using System.Net;

partial class HostingFixture {
    public HttpClient Http { get; } = new HttpClient(new HttpMessageHandlerMock());
}

class HttpMessageHandlerMock : HttpMessageHandler {

    protected override HttpResponseMessage Send(HttpRequestMessage request, CancellationToken cancellationToken) {
        string path = PathMock.GetRelativePath(request.RequestUri?.LocalPath[1..] ?? string.Empty);
        return new HttpResponseMessage(HttpStatusCode.OK) {
            Content = new StreamContent(File.OpenRead(path))
        };
    }

    protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) =>
        Task.FromResult(Send(request, cancellationToken));
}
