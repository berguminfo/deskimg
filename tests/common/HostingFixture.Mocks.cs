using System.Reflection;
using Microsoft.Extensions.Configuration;
using Deskimg;

partial class HostingFixture {

    EndpointOptions? _endpoint;

    public DateTimeOffset Toi { get; } = DateTimeOffset.Parse("2021-11-10T13:00:00Z");

    IConfigurationSection GetEndpointSection(params IEnumerable<string> resourceNames) {
        var assembly = Assembly.GetExecutingAssembly();
        var builder = new ConfigurationBuilder();
        foreach (var name in resourceNames) {
            var stream = assembly.GetManifestResourceStream(name);
            if (stream is null) {
                throw new FileNotFoundException(name);
            }

            builder.AddJsonStream(stream);
        }

        return builder.Build().GetSection(EndpointOptions.Name);
    }

    public EndpointOptions Endpoint {
        get {
            if (_endpoint == null) {
                var section = GetEndpointSection("appsettings.json", "appsettings.Tests.json");
                var logger = CreateLogger<EndpointOptions>();
#if DEBUG
                DebugOptions dbug = new() {
                    Fetch = "sample-data",
                    Sets = "sets-data"
                };
                _endpoint = new(dbug, logger, section);
#else
                _endpoint = new(logger, section);
#endif
            }

            return _endpoint;
        }
    }
}
