using Microsoft.JSInterop;
using Deskimg.Ephemeris;

internal class JSRuntimeMock : IJSRuntime {

    /// <summary>
    /// Override this method when the test needs a solar <see cref="RiseSet"/>.
    /// </summary>
    public virtual RiseSet GetSolarRiseSet() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetSolarRiseSet)}");
    }

    public virtual ICollection<AltitudeItem> GetApparentTopocentricHorizontalCoordinates() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetApparentTopocentricHorizontalCoordinates)}");
    }

    /// <summary>
    /// Override this method when the test needs a lunar <see cref="RiseSet"/>.
    /// </summary>
    public virtual RiseSet GetLunarRiseSet() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetLunarRiseSet)}");
    }

    /// <summary>
    /// Override this method when the test needs a lunar phase.
    /// </summary>
    public virtual DateTimeOffset GetLunarPhase0() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetLunarPhase0)}");
    }

    /// <summary>
    /// Override this method when the test needs a lunar phase.
    /// </summary>
    public virtual DateTimeOffset GetLunarPhase25() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetLunarPhase0)}");
    }

    /// <summary>
    /// Override this method when the test needs a lunar phase.
    /// </summary>
    public virtual DateTimeOffset GetLunarPhase50() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetLunarPhase0)}");
    }

    /// <summary>
    /// Override this method when the test needs a lunar phase.
    /// </summary>
    public virtual DateTimeOffset GetLunarPhase75() {
        throw new NotImplementedException($"{nameof(JSRuntimeMock)}.{nameof(GetLunarPhase0)}");
    }

    public ValueTask<TValue> InvokeAsync<TValue>(string identifier, object?[]? args) {
        object result = identifier switch {
            "libnova.solar.getRiseSet" => GetSolarRiseSet(),
            "libnova.solar.getApparentTopocentricHorizontalCoordinates" => GetApparentTopocentricHorizontalCoordinates(),
            "libnova.lunar.getRiseSet" => GetLunarRiseSet(),
            "libnova.lunar.getUpcomingPhase" => (args?.Length > 0 ? args[1] : int.MinValue) switch {
                0 => GetLunarPhase0(),
                25 => GetLunarPhase25(),
                50 => GetLunarPhase50(),
                75 => GetLunarPhase75(),
                _ => throw new NotSupportedException()
            },
            _ => throw new InvalidOperationException(identifier),
        };
        return ValueTask.FromResult(
            result is TValue resultT ? resultT : (TValue)Convert.ChangeType(result, typeof(TValue)));
    }

    public ValueTask<TValue> InvokeAsync<TValue>(string identifier, CancellationToken cancellationToken, object?[]? args) =>
        InvokeAsync<TValue>(identifier, args);
}
