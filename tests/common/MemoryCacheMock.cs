using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

partial class HostingFixture {
    public IMemoryCache MemoryCache { get; } = new MemoryCacheMock();
}

class MemoryCacheMock : IMemoryCache {

    public ICacheEntry CreateEntry(object key) => new CacheEntryMock(key);

    public void Dispose() { }

    public void Remove(object key) { }

    public bool TryGetValue(object key, out object? value) {
        value = null;
        return false;
    }
}

class CacheEntryMock : ICacheEntry {

    public CacheEntryMock(object key) => Key = key;

    public object Key { get; private set; }

    public object? Value { get; set; }

    public DateTimeOffset? AbsoluteExpiration { get; set; }

    public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }

    public TimeSpan? SlidingExpiration { get; set; }

    public IList<IChangeToken> ExpirationTokens { get; } = [];

    public IList<PostEvictionCallbackRegistration> PostEvictionCallbacks { get; } = [];

    public CacheItemPriority Priority { get; set; }

    public long? Size { get; set; }

    public void Dispose() { }
}
