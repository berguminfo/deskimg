using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

public partial class HostingFixture {

    // Calendar.National.NO: Used to find Calendar:NO:Flags settings
    public IConfiguration CreateConfiguration(params IEnumerable<KeyValuePair<string, string?>> initialData) {
        var builder = new ConfigurationBuilder();
        builder.AddInMemoryCollection(initialData);
        return builder.Build();
    }

    public ILogger<T> CreateLogger<T>() => new XunitLogger<T>();

    public ILogger<T> CreateLogger<T>(ITestOutputHelper output) => new XunitLogger<T>(output);
}

public class XunitLogger<T> : ILogger<T>, IDisposable {

    readonly ITestOutputHelper? _output;

    public XunitLogger() { }

    public XunitLogger(ITestOutputHelper output) => _output = output;

    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter) {
        if (!IsEnabled(logLevel)) {
            return;
        }

        _output?.WriteLine($"{logLevel}: {typeof(T).FullName}[{eventId.Id}]");
        _output?.WriteLine(formatter(state, exception));
    }

    public bool IsEnabled(LogLevel logLevel) => true;

    public IDisposable? BeginScope<TState>(TState state) where TState : notnull => this;

    public void Dispose() { }
}
