static class PathMock {

    public static string GetRelativePath(string relativePath, int depth = 5) {
        var parts = new string[] {
            Path.GetDirectoryName(typeof(PathMock).Assembly.Location) ?? string.Empty,
            string.Concat(Enumerable.Repeat($"..{Path.DirectorySeparatorChar}", depth)),
            relativePath.Replace('/', Path.DirectorySeparatorChar)
        };
        string path = Path.GetFullPath(Path.Combine(parts));
        if (path.EndsWith(Path.DirectorySeparatorChar)) {
            return path.Substring(0, path.Length - 1);
        } else {
            return path;
        }
    }
}
