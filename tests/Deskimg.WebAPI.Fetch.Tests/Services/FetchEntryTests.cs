using System.Net;
using System.Net.Http.Headers;
using Microsoft.Extensions.Caching.Hybrid;

namespace Deskimg.WebAPI.Fetch.Services.Tests;

public class FetchEntryTests {

    FetchEntry CreateFetchEntry(
        string requestUri,
        Action<HttpContentHeaders>? additionalContentHeaders = null,
        Action<HttpResponseHeaders>? additionalResponseHeaders = null) {

        HttpResponseMessage response = new(HttpStatusCode.OK) {
            RequestMessage = new(HttpMethod.Get, new Uri(requestUri)),
            Content = new ByteArrayContent(""u8.ToArray())
        };
        if (additionalContentHeaders is not null) {
            additionalContentHeaders(response.Content.Headers);
        }
        if (additionalResponseHeaders is not null) {
            additionalResponseHeaders(response.Headers);
        }

        FetchEntry entry = new();
        HybridCacheEntryOptions options = new() {
            LocalCacheExpiration = TimeSpan.FromMinutes(1)
        };
        entry.Update(response, options, null);
        return entry;
    }

    FetchEntry CreateFetchEntry(
        Action<HttpContentHeaders>? additionalContentHeaders = null,
        Action<HttpResponseHeaders>? additionalResponseHeaders = null) =>
        CreateFetchEntry("https://localhost/", additionalContentHeaders, additionalResponseHeaders);

    [Fact]
    public void TestExpirations() {
        var expected = DateTimeOffset.Now;
        var actual = CreateFetchEntry(x => x.LastModified = expected);
        // Eija: HybridCacheEntryOptions sets TTL to 1 minute, hence the expected value is now 1 minute ahead now
        expected = expected.AddMinutes(1);
        Assert.Equal(expected, actual.ExpiresAtTime);
        Assert.Equal(expected, actual.AbsoluteExpiration);
    }

    [Fact]
    public void TestContentType() {
        var expected = MediaTypeNames.Application.Octet;
        var actual = CreateFetchEntry(x => x.ContentType = new(expected));
        Assert.Equal(expected, actual.ContentType);
    }


    [Fact]
    public void TestHost() {
        Uri expected = new("https://localhost/");
        var actual = CreateFetchEntry(expected.AbsoluteUri);
        Assert.Equal(expected.Host, actual.Host);
    }

    [Theory]
    [InlineData(
        MediaTypeNames.Text.Html,
        "https://localhost/",
        "index.html")]
    [InlineData(
        MediaTypeNames.Application.Octet,
        "https://localhost/%20\t/\\x>/\\/?%20",
        "index.octet")]
    [InlineData(
        MediaTypeNames.Application.Rss,
        "https://www.nrk.no/nyheter/siste.rss",
        "siste.rss")]
    [InlineData(
        MediaTypeNames.Text.Plain,
        "https://tgftp.nws.noaa.gov/data/observations/metar/stations/ENTC.TXT",
        "ENTC.TXT")]
    [InlineData(
        MediaTypeNames.Application.Rss,
        "https://api.met.no/weatherapi/metalerts/1.1/?lang=no&lat=59.1234&lon=19.5678",
        "index.rss")]
    [InlineData(
        MediaTypeNames.Application.Json,
        "https://psapi.nrk.no/epg/nrk1,nrk2,nrk3?date=2022-10-28",
        "nrk1,nrk2,nrk3.json")]
    [InlineData(
        MediaTypeNames.Application.Rss,
        "https://api.tv2.no/articles/",
        "index.rss")]
    [InlineData(
        MediaTypeNames.Image.Png,
        "https://api.met.no/weatherapi/geosatellite/1.4/?area=europe&type=infrared&size=small",
        "index.png")]
    [InlineData(
        MediaTypeNames.Image.Jpeg,
        "https://weather.cs.uit.no/wcam0_snapshots/wcam0_latest.jpg",
        "wcam0_latest.jpg")]
    [InlineData(
        MediaTypeNames.Image.Jpeg,
        "https://www.vegvesen.no/public/webkamera/kamera?id=100104",
        "kamera.jpg")]
    [InlineData(
        MediaTypeNames.Application.Json,
        "https://kartverket.no/api/vsl/tideLevels?latitude=59.1234&longitude=19.5678&language=nb",
        "tideLevels.json")]
    [InlineData(
        MediaTypeNames.Application.Xml,
        "https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.20221028141757",
        "index.xml")]
    [InlineData(
        MediaTypeNames.Application.Json,
        "https://api.met.no/weatherapi/locationforecast/2.0/complete?lat=59.1234&lon=19.5678&altitude=-2",
        "complete.json")]
    [InlineData(
        MediaTypeNames.Image.Jpeg,
        "https://www.cdn.tv2.no/images/?imageId=15223088&panox=0&panow=100&panoh=51.020408163265&panoy=0&heighty=0&heightx=0&heightw=100&heighth=100&width=480",
        "index.jpg")]
    public void TestFileDownloadName(string contentType, string requestUri, string expected) {
        var actual = CreateFetchEntry(requestUri, x => x.ContentType = new(contentType));
        Assert.Equal(expected, actual.FileName);
    }

    [Fact]
    public void TestLastModified() {
        var expected = DateTimeOffset.Now;
        var actual = CreateFetchEntry(x => x.LastModified = expected);
        Assert.Equal(expected, actual.LastModified);
    }

    [Fact]
    public void TestETag() {
        var expected = "\"" + Guid.NewGuid().ToString("n") + "\"";
        var actual = CreateFetchEntry(null, x => x.ETag = new(expected));
        Assert.Equal(expected, actual.ETag);
    }

    [Fact]
    public void TestContentLength() {
        long expected = 0xaccaba;
        var actual = CreateFetchEntry(x => x.ContentLength = expected);
        Assert.Equal(expected, actual.ContentLength);
    }
}
