using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Deskimg.WebAPI.Fetch.Services.Tests;

using Deskimg.Extensions;
using Deskimg.WebAPI.Fetch.Options;

public class FileSystemTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    FileSystemService? _service;

    FileSystemService Service {
        get {
            if (_service == null) {
                _service = new(
                    fixture.CreateLogger<FileSystemService>(output),
                    fixture.CreateLogger<HttpResponseMessage>(output),
                    new OptionsWrapper<AppOptions>(new() {
                        OutputFormat = "{Local}/tests/fetch/{FileName}",
                        Permission = new() {
                            Allow = ["https://localhost", "https://bergum.info"]
                        }
                    }),
                    new(),
                    new NetworkService());
                _service.CreateCache();
            }

            return _service;
        }
    }

    [Theory]
    [InlineData("https://localhost:5000/")]
    public async Task TestHeadAsync(string source) {
        string base64url = DeflateBase64Url.Encode(source);
        var actual = await Service.HeadAsync(base64url);
        Assert.NotNull(actual);
        Assert.Equal(MediaTypeNames.Text.Html, actual?.ContentType);
        Assert.Equal("index.html", actual?.FileName);
        Assert.NotNull(actual?.LastModified);
        Assert.True(actual?.LastModified < DateTimeOffset.Now);
        Assert.NotNull(actual?.ETag);
        Assert.True(actual?.ContentLength > 0);
        Assert.True(actual?.ExpiresAtTime > DateTimeOffset.Now);
    }

    [Theory]
    [InlineData("https://localhost:5000/")]
    public async Task TestGetAsync(string source) {
        string base64url = DeflateBase64Url.Encode(source);
        HeaderDictionary headers = [];
        var actual = await Service.GetAsync(base64url, headers);
        Assert.NotNull(actual.Item1);
        Assert.NotNull(actual.Item2);
    }

    [Theory]
    [InlineData("https://deny.local/forbidden/")]
    public async Task TestDeniedUri(string source) {
        string base64url = DeflateBase64Url.Encode(source);
        await Assert.ThrowsAsync<RequestDeniedException>(() => Service.HeadAsync(base64url));
    }
}
