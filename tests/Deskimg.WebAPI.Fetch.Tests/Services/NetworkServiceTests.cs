namespace Deskimg.WebAPI.Fetch.Services.Tests;

public class NetworkServiceTests(
    ITestOutputHelper output) {

    readonly NetworkService service = new();

    [Fact]
    public void IsInternetAvailable() {
        bool actual = service.IsInternetAvailable;
        // Should not throw exception.
        output.WriteLine($"IsInternetAvailable result: {actual}");
    }

    [Fact]
    public void IsInternetOnMeteredConnection() {
        bool actual = service.IsInternetOnMeteredConnection;
        // Should not throw exception.
        output.WriteLine($"IsInternetOnMeteredConnection result: {actual}");
    }
}
