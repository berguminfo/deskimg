using System.Text.RegularExpressions;

namespace Deskimg.WebAPI.Fetch.Controllers.Tests;

public class BaseCalDavControllerTests {

    static Regex s_validRoute = new(BaseFetchController.ValidRoute.Replace("[[", "[").Replace("]]", "]"));

    [Theory]
    [InlineData("\x00")]
    [InlineData("\x1F")]
    [InlineData("/")]
    [InlineData("+")]
    [InlineData("\\")]
    [InlineData(":")]
    [InlineData("*")]
    [InlineData("?")]
    [InlineData("\"")]
    [InlineData("<")]
    [InlineData(">")]
    [InlineData("|")]
    public void TestInValidRoute(string input) {
        var actual = s_validRoute.Match(input);
        Assert.False(actual.Success);
    }

    [Theory]
    [InlineData('A', 'Z')]
    [InlineData('a', 'z')]
    [InlineData('0', '9')]
    [InlineData('-', '-')]
    [InlineData('_', '_')]
    public void TestValidRoute(char from, char to) {
        for (char input = from; input <= to; input++) {
            var actual = s_validRoute.Match(input.ToString());
            Assert.True(actual.Success);
        }
    }
}
