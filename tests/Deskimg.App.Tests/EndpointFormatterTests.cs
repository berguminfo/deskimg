namespace Deskimg.App.Tests;

public class EndpointFormatterTests {

    [Theory]
    [UnitTest]
    [InlineData("")]
    [InlineData("{Port}")]
    [InlineData("{Port, number}")]
    void TestFormat_Empty(string pattern) {
        EndpointFormatter endpointFormatter = new(reservedPorts: []);
        Assert.Empty(endpointFormatter.Format(pattern));
    }

    [Fact]
    [UnitTest]
    void TestFormat_RandomPort() {
        Dictionary<string, int> assignedPorts = [];
        EndpointFormatter endpointFormatter = new(assignedPorts: assignedPorts, reservedPorts: []);
        var actual1 = endpointFormatter.Format("{Port, number, test}");
        Assert.True(int.TryParse(actual1, out int _));
        var actual2 = endpointFormatter.Format("{Port, number, test}");
        Assert.Single(assignedPorts);
        Assert.Equal(actual1, actual2);
        Assert.Equal(actual1, assignedPorts["test"].ToString());
    }

    [Fact]
    [UnitTest]
    void TestFormat_AllAssigned() {
        EndpointFormatter endpointFormatter = new(new(0, 0), reservedPorts: []);
        Assert.Throws<ArgumentOutOfRangeException>(() =>
            // try 2 assigns but the range is only 1
            endpointFormatter.Format("{Port, number, test1}{Port, number, test2}"));
    }

    [Fact]
    [UnitTest]
    void TestFormat_ReAssign() {
        Dictionary<string, int> assignedPorts = new() {
            ["test"] = ushort.MaxValue
        };
        HashSet<int> reservedPorts = [ushort.MaxValue];
        EndpointFormatter endpointFormatter = new(assignedPorts: assignedPorts, reservedPorts: reservedPorts);
        endpointFormatter.Format("{Port, number, test}");
        Assert.Single(assignedPorts);
        Assert.NotEqual(ushort.MaxValue, assignedPorts["test"]);
    }
}
