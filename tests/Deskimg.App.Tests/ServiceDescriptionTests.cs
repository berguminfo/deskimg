namespace Deskimg.App.Tests;

public class ServiceDescriptionTests {

    [Fact]
    [UnitTest]
    public void TestGetProcessStartInfo() {
        EndpointFormatter endpointFormatter = new(reservedPorts: []);
        ServiceDescriptor service = new() {
            FileName = "{Platform, select, IsWindows{windows} other{_}}",
            Environment = new Dictionary<string, string> {
                ["TEST"] = "{Port, number, test}"
            },
            Arguments = ["{Port, number, test}"]
        };
        var actual = service.GetProcessStartInfo(endpointFormatter);
        Assert.NotNull(actual);
        // FileName should be expanded to FullPath
        Assert.True(actual.FileName.Length > service.FileName.Length);
        Assert.True(int.TryParse(actual.Environment["TEST"], out int _));
        Assert.Equal(actual.ArgumentList.First(), actual.Environment["TEST"]);
    }
}
