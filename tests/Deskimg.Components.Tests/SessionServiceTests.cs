using System.Globalization;
using Microsoft.Extensions.Options;

namespace Deskimg.Components.Tests;

public class SessionServiceTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly SessionService session = new(
        fixture.CreateLogger<SessionService>(output),
        fixture.Endpoint,
        new OptionsWrapper<SessionOptions>(new() {
            Sets = ["tests"],
            Language = "nn-NO",
#if DEBUG
            Dbug = new() {
                Sets = "sets-data"
            }
#endif
        }),
        new(),
        fixture.Http,
        fixture.MemoryCache);

    [Fact]
    async Task TestSetCurrentCultureAsync() {
        await session.SetCurrentCultureAsync();
        Assert.Equal("nn-NO", CultureInfo.CurrentCulture.Name);
        Assert.Equal("nn-NO", Thread.CurrentThread.CurrentCulture.Name);
        Assert.Equal("nn-NO", SR.Default.Language);
        Assert.Equal("SØNDAG", CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0].ToUpper());
    }

    [Fact]
    async Task TestGetCurrentSetAsync() {
#if DEBUG
        var currentSet = await session.GetCurrentSetAsync();
        Assert.NotNull(currentSet.Background);
        Assert.NotNull(currentSet.Index);
        Assert.Single(currentSet.Geolocation);
#endif
    }
}
