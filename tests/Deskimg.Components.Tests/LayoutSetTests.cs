using System.Net.Http.Json;

namespace Deskimg.Components.Tests;

public class LayoutSetTests : IClassFixture<HostingFixture> {

    readonly EndpointOptions _endpoint;
    readonly HttpClient _http;

    LayoutSet? _currentSet;

    async Task<LayoutSet> GetCurrentSetAsync() {
        if (_currentSet is null) {
            Uri? requestUri = _endpoint?.Sets("tests");
            Assert.NotNull(requestUri);
            _currentSet = await _http.GetFromJsonAsync<LayoutSet>(requestUri);
            Assert.NotNull(_currentSet);
        }

        return _currentSet;
    }

    public LayoutSetTests(HostingFixture fixture) {
        _endpoint = fixture.Endpoint;
        _http = fixture.Http;
    }

    [Fact]
    [UnitTest]
    public async Task TestDeserialize_Background() {
        var actual = await GetCurrentSetAsync();
        Assert.NotNull(actual.Background);
        BackgroundType background = actual.Background;
        Assert.NotNull(background.Overlay);
        Assert.Equal(HatchPattern.CheckerBoard, background.Overlay.Pattern);
        Assert.NotNull(background.Overlay);
        Assert.NotNull(background.Overlay.Opacity);
        Assert.Equal(2, background.Overlay.Opacity.Count);
        Assert.Equal(-1.2, background.Overlay.Opacity[0]);
        Assert.Equal(3.4, background.Overlay.Opacity[1]);
        Assert.Equal(2, background.Overlay.Width);
        Assert.Equal(2, background.Overlay.Height);
        Assert.NotNull(background.Composite);
        Assert.NotNull(background.Composite[0]);
        BackgroundComposite composite = background.Composite[0];
        Assert.Equal(5.6, composite.From);
        Assert.Equal(7.8, composite.To);
        Assert.NotNull(composite.Items);
        Assert.Single(composite.Items);
        Assert.Equal("Test", composite.Items[0].Source);
    }

    [Fact]
    [UnitTest]
    public async Task TestDeserialize_Index() {
        var actual = await GetCurrentSetAsync();
        Assert.NotNull(actual.Index);
        IndexType index = actual.Index[0];
        Assert.Equal("col-12", index.Class);
        Assert.NotNull(index.Component);
        Assert.Single(index.Component);
        var componentData = index.Component[0];
        Assert.NotNull(componentData);
        var test = componentData["Test"];
        Assert.NotNull(test);
        Assert.Contains("PropertyKey", test);
        Assert.Equal("NotNull", test["PropertyKey"].ToString());
    }

    [Fact]
    [UnitTest]
    public async Task TestDeserialize_Geolocation() {
        var actual = await GetCurrentSetAsync();
        var geolocation = actual.Geolocation;
        Assert.NotNull(geolocation);
        Assert.Single(geolocation);
        var geoposition = geolocation["NZSP"];
        Assert.NotNull(geoposition);
        Assert.NotNull(geoposition.CivicAddress);
        Assert.Equal("NZSP", geoposition.CivicAddress.City);
        Assert.Equal("AQ", geoposition.CivicAddress.Country);
        Assert.NotNull(geoposition.Coordinate);
        Assert.Equal(-89.989444, geoposition.Coordinate.Latitude);
        Assert.Equal(-1, geoposition.Coordinate.Longitude);
        Assert.Equal(2835, geoposition.Coordinate.Altitude);
        Assert.NotNull(geoposition.Tags);
        Assert.Contains("metar.noaa.gov?NZSP", geoposition.Tags);
    }
}
