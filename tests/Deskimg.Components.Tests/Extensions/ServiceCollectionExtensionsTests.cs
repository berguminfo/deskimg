using Microsoft.Extensions.DependencyInjection;

namespace Deskimg.Extensions.Tests;

public class ServiceCollectionExtensionsTests : IClassFixture<HostingFixture> {

    readonly ServiceCollection services = new();

    public ServiceCollectionExtensionsTests(HostingFixture fixture) {
        var configuration = fixture.CreateConfiguration(
            new KeyValuePair<string, string?>("Session:🌐", "A"),
            new KeyValuePair<string, string?>("Session:🌐", "B")
        );
        services.AddSingleton(configuration);
    }


    [Fact]
    void TestMapSessionSets() {
        services.AddSessionOptions();
        /*
        var actual = configuration.MapSessionSets();
        Assert.Equal("A", actual["Session:Sets:0"]);
        Assert.Equal("B", actual["Session:Sets:1"]);
        */
    }
}
