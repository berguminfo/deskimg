using System.Collections.Immutable;

namespace Deskimg.Extensions.Tests;

public class StyleExtensionsTests(
    ITestOutputHelper output) {

    [Fact]
    public void TestToStyleString() {
        IImmutableDictionary<string, string?> expected = new Dictionary<string, string?> {
            ["key"] = "value",
            ["empty"] = string.Empty,
            ["null"] = null
        }.ToImmutableDictionary();
        var actual = expected.ToStyleString();
        output.WriteLine(actual);
        Assert.Equal(2, actual.Count(x => x == ';'));
        Assert.Contains("key:value", actual);
        Assert.Contains("empty:", actual);
        Assert.Contains("null:", actual);
    }
}
