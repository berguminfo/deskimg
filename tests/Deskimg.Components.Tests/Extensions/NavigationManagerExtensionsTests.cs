using Microsoft.AspNetCore.Components;
using Google.OpenLocationCode;

namespace Deskimg.Extensions.Tests;

public sealed class NavigationManagerMock : NavigationManager {

    public NavigationManagerMock(string uri) {
        Initialize(uri, uri);
    }
}

public class NavigationManagerExtensionsTests {

    [Fact]
    [UnitTest]
    public void TestGetOptions_Sets_EmptyIsNull() {
        NavigationManagerMock navigation = new("https://unused/?🌐=");
        var actual = navigation.GetOptions();
        Assert.Null(actual.Sets);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Sets1() {
        NavigationManagerMock navigation = new("https://unused/?🌐=A");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.Sets);
        Assert.Single(actual.Sets);
        Assert.Contains("A", actual.Sets);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Sets2() {
        NavigationManagerMock navigation = new("https://unused/?🌐=A&🌐=B");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.Sets);
        Assert.Equal(2, actual.Sets.Count());
        Assert.Contains("A", actual.Sets);
        Assert.Contains("B", actual.Sets);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Toi_Date() {
        NavigationManagerMock navigation = new("https://unused/?t=2020-12-23");
        var actual = navigation.GetOptions();
        Assert.Equal(DateTimeOffset.Parse("2020-12-23"), actual.Toi);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Toi_DateTime() {
        NavigationManagerMock navigation = new("https://unused/?t=2020-12-23T12:34:56");
        var actual = navigation.GetOptions();
        Assert.Equal(DateTimeOffset.Parse("2020-12-23T12:34:56"), actual.Toi);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Toi_PlusDuration() {
        NavigationManagerMock navigation = new("https://unused/?t=2020-12-23&t=PT12H");
        var actual = navigation.GetOptions();
        Assert.Equal(DateTimeOffset.Parse("2020-12-23T12:00:00"), actual.Toi);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Toi_NowPlusDuration() {
        NavigationManagerMock navigation = new("https://unused/?t=PT10S");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.Toi);
        Assert.True(DateTimeOffset.Now.AddSeconds(10) >= actual.Toi.Value);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Language() {
        NavigationManagerMock navigation = new("https://unused/?lang=A");
        var actual = navigation.GetOptions();
        Assert.Equal("A", actual.Language);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Language_Short() {
        NavigationManagerMock navigation = new("https://unused/?l=A");
        var actual = navigation.GetOptions();
        Assert.Equal("A", actual.Language);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Language_Last() {
        NavigationManagerMock navigation = new("https://unused/?lang=A&lang=B");
        var actual = navigation.GetOptions();
        Assert.Equal("B", actual.Language);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_GeoURI() {
        NavigationManagerMock navigation = new("https://unused/?geo=-27.175063,78.042188");
        var actual = navigation.GetOptions();
        Assert.Equal(-27.175063, actual.Latitude);
        Assert.Equal(78.042188, actual.Longitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_GeoURI_Last() {
        NavigationManagerMock navigation = new("https://unused/?geo=-27.175063,78.042188&geo=27.175063,-78.042188");
        var actual = navigation.GetOptions();
        Assert.Equal(27.175063, actual.Latitude);
        Assert.Equal(-78.042188, actual.Longitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_GeoURI_OutOfBound() {
        NavigationManagerMock navigation = new("https://unused/?geo=-91,181");
        var actual = navigation.GetOptions();
        Assert.Null(actual.Latitude);
        Assert.Null(actual.Longitude);
    }

    [Theory]
    [UnitTest]
    [InlineData(10)]
    [InlineData(11)]
    public void TestGetOptions_PlusCode(int codeLength) {
        string code = OpenLocationCode.Encode(-27.175063, 78.042188, codeLength);
        NavigationManagerMock navigation = new($"https://unused/?geo={Uri.EscapeDataString(code)}%20Test");
        var actual = navigation.GetOptions();
        var expected = OpenLocationCode.Decode(code).Center;
        Assert.Equal(expected.Latitude, actual.Latitude);
        Assert.Equal(expected.Longitude, actual.Longitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Location() {
        NavigationManagerMock navigation = new("https://unused/?geo=NZSP");
        var actual = navigation.GetOptions();
        Assert.Equal("NZSP", actual.Location);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Latitude() {
        NavigationManagerMock navigation = new("https://unused/?Φ=-12,34");
        var actual = navigation.GetOptions();
        Assert.Equal(-12.34, actual.Latitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Latitude_OutOfBound() {
        NavigationManagerMock navigation = new("https://unused/?Φ=91");
        var actual = navigation.GetOptions();
        Assert.Null(actual.Latitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Longitude() {
        NavigationManagerMock navigation = new("https://unused/?Λ=-12,34");
        var actual = navigation.GetOptions();
        Assert.Equal(-12.34, actual.Longitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Longitude_OutOfBound() {
        NavigationManagerMock navigation = new("https://unused/?Λ=181");
        var actual = navigation.GetOptions();
        Assert.Null(actual.Longitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Altitude() {
        NavigationManagerMock navigation = new("https://unused/?z=12,34");
        var actual = navigation.GetOptions();
        Assert.Equal(12.34, actual.Altitude);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Navigation_True() {
        NavigationManagerMock navigation = new("https://unused/?nav=✓");
        var actual = navigation.GetOptions();
        Assert.True(actual.Navigation);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Navigation_False() {
        NavigationManagerMock navigation = new("https://unused/?nav=✓&nav=");
        var actual = navigation.GetOptions();
        Assert.False(actual.Navigation);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_UpdateInterval() {
        NavigationManagerMock navigation = new("https://unused/?upd=01:23:45");
        var actual = navigation.GetOptions();
        Assert.Equal(TimeSpan.Parse("01:23:45"), actual.UpdateInterval);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_BackgroundSource() {
        NavigationManagerMock navigation = new("https://unused/?img=A");
        var actual = navigation.GetOptions();
        Assert.Equal("A", actual.BackgroundSource);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_BackgroundSource_Last() {
        NavigationManagerMock navigation = new("https://unused/?img=A&img=B");
        var actual = navigation.GetOptions();
        Assert.Equal("B", actual.BackgroundSource);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_BackgroundRendering() {
        NavigationManagerMock navigation = new("https://unused/?bkg=Blob");
        var actual = navigation.GetOptions();
        Assert.Equal(RenderingMode.Blob, actual.BackgroundRendering);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_BackgroundRendering_Short() {
        NavigationManagerMock navigation = new("https://unused/?bg=Css");
        var actual = navigation.GetOptions();
        Assert.Equal(RenderingMode.Css, actual.BackgroundRendering);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_BackgroundSource_Short() {
        NavigationManagerMock navigation = new("https://unused/?img=A");
        var actual = navigation.GetOptions();
        Assert.Equal("A", actual.BackgroundSource);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_EnableNotification() {
        NavigationManagerMock navigation = new("https://unused/?nf=✓");
        var actual = navigation.GetOptions();
        Assert.True(actual.EnableNotification);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_ClipWidth() {
        NavigationManagerMock navigation = new("https://unused/?cw=1324");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.ClippingRules);
        Assert.Contains("screen and (min-width: 1324px)", actual.ClippingRules);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_ClipWidth_OutOfBound() {
        NavigationManagerMock navigation = new("https://unused/?cw=-1324");
        var actual = navigation.GetOptions();
        Assert.Null(actual.ClippingRules);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_ClipHeight() {
        NavigationManagerMock navigation = new("https://unused/?ch=1324");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.ClippingRules);
        Assert.Contains("screen and (min-height: 1324px)", actual.ClippingRules);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_ClipHeight_OutOfBound() {
        NavigationManagerMock navigation = new("https://unused/?ch=-1324");
        var actual = navigation.GetOptions();
        Assert.Null(actual.ClippingRules);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_Search() {
        NavigationManagerMock navigation = new("https://unused/?q=A");
        var actual = navigation.GetOptions();
        Assert.Equal("A", actual.Search);
    }

#if DEBUG

    [Fact]
    [UnitTest]
    public void TestGetOptions_DbugFetch() {
        NavigationManagerMock navigation = new("https://unused/?dbug:fetch=✓");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.Dbug);
        Assert.Equal("sample-data", actual.Dbug.Fetch);
    }

    [Fact]
    [UnitTest]
    public void TestGetOptions_DbugSets() {
        NavigationManagerMock navigation = new("https://unused/?dbug:sets=✓");
        var actual = navigation.GetOptions();
        Assert.NotNull(actual.Dbug);
        Assert.Equal("sets-data", actual.Dbug.Sets);
    }

#endif
}
