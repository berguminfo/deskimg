namespace Deskimg.Generators.Iso3166.Tests;

[UsesVerify]
public class Iso3166GeneratorTests {

    [Fact]
    public Task VerifySourceGenerator() {
        string source =
            """
            using Deskimg;

            namespace Namespace1.Namespace2; // generator requires namespaced types

            [Iso3166Generated]
            public class TestClass { }
            """;
        return TestHelper.Verify(new Iso3166Generator(), source);
    }
}
