using System.Runtime.CompilerServices;

namespace Deskimg.Generators.Iso3166.Tests;

public static class ModuleInitializer {

    [ModuleInitializer]
    public static void Init() => VerifySourceGenerators.Initialize();
}
