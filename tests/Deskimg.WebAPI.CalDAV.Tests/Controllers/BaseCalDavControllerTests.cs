using System.Text.RegularExpressions;

namespace Deskimg.WebAPI.CalDAV.Controllers.Tests;

public class BaseCalDavControllerTests {

    static Regex s_validRoute = new(BaseCalDavController.ValidRoute.Replace("[[", "[").Replace("]]", "]"));

    [Theory]
    [InlineData("\x00")]
    [InlineData("\x1F")]
    [InlineData("/")]
    [InlineData("\\")]
    [InlineData(":")]
    [InlineData("*")]
    [InlineData("?")]
    [InlineData("\"")]
    [InlineData("<")]
    [InlineData(">")]
    [InlineData("|")]
    public void TestInValidRoute(string input) {
        var actual = s_validRoute.Match(input);
        Assert.False(actual.Success);
    }
}
