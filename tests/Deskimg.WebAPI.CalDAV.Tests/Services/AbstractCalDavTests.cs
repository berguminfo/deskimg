//#define SKIP_NONE

using System.Net;
using System.Text;
using System.Text.Json;

namespace Deskimg.WebAPI.CalDAV.Services.Tests;

public sealed record Input(
    string[] Store,
    string Name,
    Stream Content,
    long? ContentLength,
    string? ContentType,
    string? ContentEncoding,
    string? RemoteAddress);

public abstract class AbstractCalDavTests(
    ITestOutputHelper output) {

    protected abstract AbstractCalDavRepository Service { get; }

    protected const string _content = """
        {
            "number": 1.23,
            "string": "øæå"
        }
        """;

    protected readonly Input _input = new(
        ["test"],
        "1",
        new MemoryStream(Encoding.UTF8.GetBytes(_content)),
        3,
        MediaTypeNames.Application.Octet,
        "utf-8",
        "[::1]");

    [Fact]
    [UnitTest]
    public async Task TestCRUD() {
        await TestPutAsync();
        await TestHeadAsync();
        await TestGetAsync();
        await TestPropFind();
        await TestDeleteAsync();
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestHeadAsync() {
        var actual = await Service.HeadAsync(_input.Store, _input.Name);
        Assert.NotNull(actual);
        output.WriteLine(JsonSerializer.Serialize(actual));
        Assert.Equal(_input.ContentType, actual.ContentType);
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestHeadAsync_NotFound() {
        await Assert.ThrowsAsync<FileNotFoundException>(async () => await Service.HeadAsync(_input.Store, Guid.NewGuid().ToString("N")));
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestGetAsync() {
        var actual = await Service.GetAsync(_input.Store, _input.Name) as IGetResult<string>;
        Assert.NotNull(actual);
        output.WriteLine(JsonSerializer.Serialize(actual));
        string expectedContent = Encoding.UTF8.GetString(((MemoryStream)_input.Content).ToArray());
        Assert.Equal(expectedContent, actual.Content);

    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestGetAsync_NotFound() {
        await Assert.ThrowsAsync<FileNotFoundException>(async () => await Service.GetAsync(_input.Store, Guid.NewGuid().ToString("N")));
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestPutAsync() {
        var actual = await Service.PutAsync(
            _input.Store,
            _input.Name,
            _input.Content,
            _input.ContentLength,
            _input.ContentType,
            _input.ContentEncoding,
            _input.RemoteAddress);
#if !SKIP_NONE
        Assert.Equal(HttpStatusCode.Created, actual);
#endif
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestDeleteAsync() {
        var actual = await Service.DeleteAsync(_input.Store, _input.Name);
        Assert.Equal(HttpStatusCode.OK, actual);
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestPropFind() {
        var actual = await Service.PropFind(_input.Store);
        Assert.NotNull(actual);
        output.WriteLine(JsonSerializer.Serialize(actual));
#if !SKIP_NONE
        Assert.Single(actual);
#endif
    }

#if SKIP_NONE
    [Fact]
#else
    [Fact(Skip = "Use TestCRUD or define SKIP_NONE")]
#endif
    [UnitTest]
    public async Task TestPropFind_Collections() {
        var actual = await Service.PropFind();
        Assert.NotNull(actual);
        output.WriteLine(JsonSerializer.Serialize(actual));
#if !SKIP_NONE
        Assert.Single(actual);
#endif
    }
}
