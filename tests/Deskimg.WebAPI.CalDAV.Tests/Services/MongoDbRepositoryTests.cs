// Ignore Spelling: Mongo

using System.Text;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Deskimg.WebAPI.CalDAV.Services.Tests;

using Deskimg.WebAPI.CalDAV.Options;

public class MongoDbRepositoryTests(
    ITestOutputHelper output) : AbstractCalDavTests(output) {

    MongoDbRepository? _service;

    protected override AbstractCalDavRepository Service {
        get {
            if (_service is null) {
                string connectionString = Environment.GetEnvironmentVariable("MONGODB_URI") ?? "mongodb://localhost:27017";
                _service = new(
                    new OptionsWrapper<AppOptions>(new () {
                        StorageDatabase = "deskimg_tests"
                    }),
                    new MongoClient(connectionString));
            }

            return _service;
        }
    }

    [Fact]
    [UnitTest]
    public async Task TestContentEncoding_Latin1() {
        string[] expectedStore = [nameof(MongoDbRepositoryTests)];
        string expectedName = nameof(TestContentEncoding_Latin1);
        string expectedContent = "ÆØÅæøå";
        var encoding = Encoding.GetEncoding("iso-8859-1");
        byte[] contentBytes = encoding.GetBytes(expectedContent);

        await Service.PutAsync(
            expectedStore,
            expectedName,
            new MemoryStream(contentBytes),
            contentBytes.Length,
            MediaTypeNames.Text.Plain,
            encoding.BodyName,
            "[::1]");
        var actual = await Service.GetAsync(expectedStore, expectedName) as IGetResult<string>;
        Assert.NotNull(actual);
        Assert.Equal(expectedContent, actual.Content);
#if !SKIP_NONE
        try {
            await Service.DeleteAsync(expectedStore, expectedName);
        } catch (Exception ex) {
            Assert.Fail(ex.Message);
        }
#endif
    }

    [Fact]
    [UnitTest]
    public async Task TestContentEncoding_UTF8() {
        string[] expectedStore = [nameof(MongoDbRepositoryTests)];
        string expectedName = nameof(TestContentEncoding_UTF8);
        var expectedContent = "⚡⚡⚡";
        var encoding = Encoding.UTF8;
        var contentBytes = encoding.GetBytes(expectedContent);

        await Service.PutAsync(
            expectedStore,
            expectedName,
            new MemoryStream(contentBytes),
            contentBytes.Length,
            MediaTypeNames.Text.Plain,
            encoding.BodyName,
            "[::1]");
        var actual = await Service.GetAsync(expectedStore, expectedName) as IGetResult<string>;
        Assert.NotNull(actual);
        Assert.Equal(expectedContent, actual.Content);
#if !SKIP_NONE
        try {
            await Service.DeleteAsync(expectedStore, expectedName);
        } catch (Exception ex) {
            Assert.Fail(ex.Message);
        }
#endif
    }

    [Fact]
    [UnitTest]
    public async Task TestPatchAsync() {
        await TestPutAsync();

        string expected = "⚡";
        string patchJson = $$"""
            [
                {
                    "op": "replace",
                    "patch": "/number",
                    "value": {{expected}}
                }
            ]
            """;

        //await _service.PatchAsync(_input.Store, _input.Name, patch, _input.RemoteAddress);
        var actual = await Service.GetAsync(_input.Store, _input.Name) as IGetResult<string>;
        Assert.NotNull(actual);
        //output.WriteLine(actual.Content);
        Assert.Contains(expected, actual.Content);

        await TestDeleteAsync();
    }
}
