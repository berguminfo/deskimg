using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Options;

namespace Deskimg.WebAPI.CalDAV.Services.Tests;

using Deskimg.WebAPI.CalDAV.Options;

public class FileSystemRepositoryTests(
    ITestOutputHelper output) : AbstractCalDavTests(output) {

    FileSystemRepository? _service;

    protected override AbstractCalDavRepository Service {
        get {
            if (_service is null) {
                _service = new(
                    new OptionsWrapper<AppOptions>(new () {
                        OutputFormat = AppDomain.CurrentDomain.BaseDirectory + "/storage/{FileName}"
                    }),
                    new FileExtensionContentTypeProvider());
            }

            return _service;
        }
    }
}
