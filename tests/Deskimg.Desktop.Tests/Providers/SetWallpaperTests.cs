namespace Deskimg.Desktop.Providers.Tests;

using Deskimg.Desktop.Options;

public class SetWallpaperTests {

    static readonly DirectoryInfo s_wallpaperDirectory = new(@"C:\tmp\");

    [Fact]
    [UnitTest]
    public async Task TestExec_Throws() {
        Exec provider = new();
        SetDesktopOptions options = new() {
            Provider = string.Empty,
        };
        await Assert.ThrowsAsync<InvalidOperationException>(() =>
            provider.SetWallpaperAsync(options, s_wallpaperDirectory));
    }

#if ENABLE_START_PROCESS

    [Fact]
    [UnitTest("Verified")]
    public async Task TestExec_StartProcess() {
        Exec provider = new();
        SetDesktopOptions options = new() {
            Provider = string.Empty,
            Command = "winver",
        };
        try {
            await provider.SetWallpaperAsync(options, s_wallpaperDirectory);
        } catch (Exception ex) {
            Assert.Fail(ex.Message);
        }
    }

#endif

#if WINDOWS8_0_OR_GREATER || true

    [Fact]
    //[Fact(Skip = "CsWin32: E_INVALIDARG")]
    [UnitTest]
    public async Task TestShobj_AllMonitors() {
        Shobj provider = new();
        SetDesktopOptions options = new() {
            Provider = string.Empty,
            Desktop = null,
        };
        try {
            await provider.SetWallpaperAsync(options, s_wallpaperDirectory);
        } catch (Exception ex) {
            Assert.Fail(ex.Message);
        }
    }

    [Fact]
    //[Fact(Skip = "CsWin32: E_INVALIDARG")]
    [UnitTest]
    public async Task TestShobj_Monitor0() {
        Shobj provider = new();
        SetDesktopOptions options = new() {
            Provider = string.Empty,
            Desktop = 0,
        };
        try {
            await provider.SetWallpaperAsync(options, s_wallpaperDirectory);
        } catch (Exception ex) {
            Assert.Fail(ex.Message);
        }
    }

#endif

#if WINDOWS || true
    [Fact]
    public async Task TestUser32() {
        User32 provider = new();
        SetDesktopOptions options = new() {
            Provider = string.Empty
        };
        try {
            await provider.SetWallpaperAsync(options, s_wallpaperDirectory);
        } catch (Exception ex) {
            Assert.Fail(ex.Message);
        }
    }
#endif

#if WINDOWS10_0_17763_0_OR_GREATER
    [Fact]
    public async Task TestUwpWallpaper() {
        UWP provider = new();
        await provider.SetWallpaperAsync(s_options, s_wallpaperDirectory);
    }

    [Fact]
    public async Task TestUwpLockScreen() {
        UWP provider = new();
        await provider.SetWallpaperAsync(s_options, s_wallpaperDirectory);
    }
#endif
}
