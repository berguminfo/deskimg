namespace Deskimg.Meteorology.NO.Tests;

public class YrWeatherWarningTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly YrWeatherWarning provider = new(
        fixture.CreateLogger<YrWeatherWarning>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("wrn.yr.no", string.Empty)
    };

    readonly Geoposition observer = new() {
        Coordinate = new() {
            Latitude = 70.1,
            Longitude = 19.2,
        }
    };

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_CAP1_NO() {
        await SR.InitializeAsync("nb-NO");
        await TestRunner("?cap=1", AlertKind.Ice, AlertSeverity.Red);
    }

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_CAP2_NO() {
        await SR.InitializeAsync("nb-NO");
        await TestRunner("?cap=2", AlertKind.Wind, AlertSeverity.Extreme);
    }

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_CAP1_EN() {
        await SR.InitializeAsync("en-GB");
        await TestRunner("?cap=1", AlertKind.Ice, AlertSeverity.Red);
    }

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_CAP2_EN() {
        await SR.InitializeAsync("en-GB");
        await TestRunner("?cap=2", AlertKind.Wind, AlertSeverity.Extreme);
    }

    async Task TestRunner(string query, AlertKind AlertKind, AlertSeverity alertSeverity) {
        var result = await provider.GetAlertsAsync(DateTimeOffset.Now, TimeSpan.Zero, observer);
        var actual = result.FirstOrDefault(x =>
            x.Message.Link is not null && x.Message.Link.Contains(query));
        Assert.NotNull(actual);
        Assert.False(string.IsNullOrWhiteSpace(actual.Message.Id));
        Assert.NotNull(actual.Message.Title);
        Assert.NotNull(actual.Message.Summary);
        Assert.Equal(AlertKind, actual.Kind);
        Assert.Equal(alertSeverity, actual.Severity);
    }
}
