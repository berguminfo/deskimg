namespace Deskimg.Meteorology.NO.Tests;

[LegendsInlineData(MethodName = "TestParseCondition")]
public partial class LegendsTests(
    ITestOutputHelper output) {

    void TestParseConditionRunner(string symbol) {
        var condition = YrForecast.ParseCondition(symbol);
        output.WriteLine($"{symbol}: {condition}");
        Assert.NotEqual(WeatherConditions.None, condition);
    }
}
