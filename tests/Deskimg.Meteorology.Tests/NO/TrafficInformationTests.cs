namespace Deskimg.Meteorology.NO.Tests;

public class TrafficInformationTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly TrafficInformation provider = new(
        fixture.CreateLogger<TrafficInformation>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("traffic-info.vegvesen.no", string.Empty)
    };

    readonly Geoposition observer = new() {
        Coordinate = new() {
            Latitude = 70.1,
            Longitude = 19.2,
        }
    };

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts() {
        var actual = (await provider.GetAlertsAsync(DateTimeOffset.Now, TimeSpan.Zero, observer)).First();
        Assert.NotNull(actual);
        Assert.False(string.IsNullOrWhiteSpace(actual.Message.Id));
        Assert.NotNull(actual.Message.Title);
        Assert.NotNull(actual.Message.Summary);
        Assert.Equal(AlertKind.RoadClosed, actual.Kind);
        Assert.Equal(AlertSeverity.Active, actual.Severity);
    }
}
