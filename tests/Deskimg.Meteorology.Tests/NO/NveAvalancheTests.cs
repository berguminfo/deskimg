namespace Deskimg.Meteorology.NO.Tests;

public class NvaAvalancheTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly NveAvalanche provider = new(
        fixture.CreateLogger<NveAvalanche>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("avalanche.nve.no", string.Empty)
    };

    readonly Geoposition observer = new() {
        Coordinate = new() {
            Latitude = 70.1,
            Longitude = 19.2,
        }
    };

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_NO() {
        await SR.InitializeAsync("nb-NO");

        var actual = (await provider.GetAlertsAsync(DateTimeOffset.Now, TimeSpan.Zero, observer)).FirstOrDefault();
        Assert.NotNull(actual);
        Assert.False(string.IsNullOrWhiteSpace(actual.Message.Id));
        Assert.NotNull(actual.Message.Title);
        Assert.NotNull(actual.Message.Summary);
        Assert.Equal(AlertKind.Avalanches, actual.Kind);
        Assert.Equal(AlertSeverity.Red, actual.Severity);
    }

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_EN() {
        await SR.InitializeAsync("en-GB");

        var actual = (await provider.GetAlertsAsync(DateTimeOffset.Now, TimeSpan.Zero, observer)).FirstOrDefault();
        Assert.NotNull(actual);
        Assert.False(string.IsNullOrWhiteSpace(actual.Message.Id));
        Assert.NotNull(actual.Message.Title);
        Assert.NotNull(actual.Message.Summary);
        Assert.Equal(AlertKind.Avalanches, actual.Kind);
        Assert.Equal(AlertSeverity.Red, actual.Severity);
    }
}
