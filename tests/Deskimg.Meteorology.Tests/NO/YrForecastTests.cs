namespace Deskimg.Meteorology.NO.Tests;

public class YrForecastTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly YrForecast provider = new(
        fixture.CreateLogger<YrForecast>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("forecast.yr.no", string.Empty)
    };

    readonly Geoposition observer = new() {
        Coordinate = new() {
            Latitude = 70.1,
            Longitude = 19.2,
        }
    };

    [Theory]
    [InlineData("2022-10-25")]
    [InlineData("2022-11-04")] // may throw NullPointerException
    [InlineData("2022-10-01")]
    [UnitTest]
    public async Task TestGetForecasts(string start) {
        var actual = (await provider.GetForecastsAsync(DateTimeOffset.Parse(start), TimeSpan.Zero, observer)).FirstOrDefault();
        Assert.NotNull(actual);
        Assert.Equal(typeof(IForecastProvider), actual.Provider);
        Assert.NotNull(actual?.Temperature);
        Assert.NotNull(actual?.Conditions);
        Assert.NotEqual(WeatherConditions.Unknown, actual?.Conditions);
        Assert.NotNull(actual?.UltravioletRadiation);
        Assert.NotNull(actual?.Wind);
    }
}
