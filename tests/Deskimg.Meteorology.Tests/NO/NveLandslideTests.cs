namespace Deskimg.Meteorology.NO.Tests;

public class NvaLandslideTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly NveLandslide provider = new(
        fixture.CreateLogger<NveLandslide>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("landslide.nve.no/municipality", "9999")
    };

    readonly Geoposition observer = new() {
        Coordinate = new() {
            Latitude = 70.1,
            Longitude = 19.2,
        }
    };

    [Fact]
    [UnitTest]
    public async Task TestGetAlerts_NOB() {
        await SR.InitializeAsync("nb-NO");

        var actual = (await provider.GetAlertsAsync(DateTimeOffset.Now, TimeSpan.Zero, observer)).FirstOrDefault();
        Assert.NotNull(actual);
        Assert.False(string.IsNullOrWhiteSpace(actual.Message.Id));
        Assert.NotNull(actual.Message.Title);
        Assert.NotNull(actual.Message.Summary);
        Assert.Equal(AlertKind.Landslide, actual.Kind);
        Assert.Equal(AlertSeverity.Orange, actual.Severity);
    }
}
