#if ENABLE_THIS
using Deskimg.Ephemeris;

namespace Deskimg.Meteorology.Tests;

public class WeatherElementTests : IClassFixture<HostingFixture> {

    readonly ITestOutputHelper _output;
    readonly IDictionary<string, bool> _files;

    public WeatherElementTests(ITestOutputHelper output, HostingFixture fixture) {
        _output = output;

        string basename = PathMock.GetRelativePath(Path.Join(AppContext.BaseDirectory,
                "..", "..", "..", "..", "..", ".."));
        //      Debug/bin/BASE/blazor/tests/ROOT
#pragma warning TODO_LocalResourceOption
        Uri imagesUri = new(fixture.Configuration["TODO:images"] ?? string.Empty);
        string statusImagesPath = Path.Join(basename, imagesUri.LocalPath, "status", "110");
        _files = Directory.GetFiles(statusImagesPath, "weather-*.svg").ToDictionary(item => Path.GetFileNameWithoutExtension(item), _ => false);
    }

    [Theory]
    [InlineData(ConditionType.Unknown, ConditionIntensity.None)]
    [InlineData(ConditionType.ClearSky, ConditionIntensity.Complete, ConditionThemeType.Day)]
    [InlineData(ConditionType.ClearSky, ConditionIntensity.Complete, ConditionThemeType.Night)]
    [InlineData(ConditionType.ClearSky, ConditionIntensity.Complete, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.Fair, ConditionIntensity.Complete, ConditionThemeType.Day)]
    [InlineData(ConditionType.Fair, ConditionIntensity.Complete, ConditionThemeType.Night)]
    [InlineData(ConditionType.Fair, ConditionIntensity.Complete, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.PartlyCloudy, ConditionIntensity.Complete, ConditionThemeType.Day)]
    [InlineData(ConditionType.PartlyCloudy, ConditionIntensity.Complete, ConditionThemeType.Night)]
    [InlineData(ConditionType.PartlyCloudy, ConditionIntensity.Complete, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.Cloudy, ConditionIntensity.Complete)]
    [InlineData(ConditionType.Fog, ConditionIntensity.Complete)]
    [InlineData(ConditionType.Rain, ConditionIntensity.Light)]
    [InlineData(ConditionType.Rain, ConditionIntensity.Moderate)]
    [InlineData(ConditionType.Rain, ConditionIntensity.Heavy)]
    [InlineData(ConditionType.RainAndThunder, ConditionIntensity.Light)]
    [InlineData(ConditionType.RainAndThunder, ConditionIntensity.Moderate)]
    [InlineData(ConditionType.RainAndThunder, ConditionIntensity.Heavy)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Light, ConditionThemeType.Day)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Light, ConditionThemeType.Night)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Light, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Moderate, ConditionThemeType.Day)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Moderate, ConditionThemeType.Night)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Moderate, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Heavy, ConditionThemeType.Day)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Heavy, ConditionThemeType.Night)]
    [InlineData(ConditionType.RainShowers, ConditionIntensity.Heavy, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.Day)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.Night)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.Day)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.Night)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.Day)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.Night)]
    [InlineData(ConditionType.RainShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.Sleet, ConditionIntensity.Light)]
    [InlineData(ConditionType.Sleet, ConditionIntensity.Moderate)]
    [InlineData(ConditionType.Sleet, ConditionIntensity.Heavy)]
    [InlineData(ConditionType.SleetAndThunder, ConditionIntensity.Light)]
    [InlineData(ConditionType.SleetAndThunder, ConditionIntensity.Moderate)]
    [InlineData(ConditionType.SleetAndThunder, ConditionIntensity.Heavy)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Light, ConditionThemeType.Day)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Light, ConditionThemeType.Night)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Light, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Moderate, ConditionThemeType.Day)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Moderate, ConditionThemeType.Night)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Moderate, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Heavy, ConditionThemeType.Day)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Heavy, ConditionThemeType.Night)]
    [InlineData(ConditionType.SleetShowers, ConditionIntensity.Heavy, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.Day)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.Night)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.Day)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.Night)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.Day)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.Night)]
    [InlineData(ConditionType.SleetShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.Snow, ConditionIntensity.Light)]
    [InlineData(ConditionType.Snow, ConditionIntensity.Moderate)]
    [InlineData(ConditionType.Snow, ConditionIntensity.Heavy)]
    [InlineData(ConditionType.SnowAndThunder, ConditionIntensity.Light)]
    [InlineData(ConditionType.SnowAndThunder, ConditionIntensity.Moderate)]
    [InlineData(ConditionType.SnowAndThunder, ConditionIntensity.Heavy)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Light, ConditionThemeType.Day)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Light, ConditionThemeType.Night)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Light, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Moderate, ConditionThemeType.Day)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Moderate, ConditionThemeType.Night)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Moderate, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Heavy, ConditionThemeType.Day)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Heavy, ConditionThemeType.Night)]
    [InlineData(ConditionType.SnowShowers, ConditionIntensity.Heavy, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.Day)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.Night)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Light, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.Day)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.Night)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Moderate, ConditionThemeType.PolarTwilight)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.Day)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.Night)]
    [InlineData(ConditionType.SnowShowersAndThunder, ConditionIntensity.Heavy, ConditionThemeType.PolarTwilight)]
    public void TestGetConditionImage(ConditionType condition, ConditionIntensity intensitivity, ConditionThemeType theme = ConditionThemeType.None) {
        WeatherElement weatherEntry = new() {
            Validity = new(DateTimeOffset.Parse(theme == ConditionThemeType.Night ?
                "2021-01-01T06:00:00" : "2021-01-01T12:00:00")),
            Condition = new(condition, new MeasureD((double)intensitivity, string.Empty)),
            SolarRiseSet = new() {
                BelowHorizon = theme == ConditionThemeType.PolarTwilight,
                Rise = DateTimeOffset.Parse("2021-01-01T09:00:00"),
                Set = DateTimeOffset.Parse("2021-01-01T15:00:00")
            }
        };
        string? actual = weatherEntry.GetConditionImage();
        Assert.NotNull(actual);
        Assert.Contains(actual, _files.Keys);
    }

    [Fact]
    public void TestBelowHorizonTheme() {
        WeatherElement weatherEntry = new() {
            Condition = new(ConditionType.ClearSky, new MeasureD())
        };
        RiseSet riseSet = new() {
            BelowHorizon = true
        };
        var actual = weatherEntry.GetConditionTheme(riseSet);
        Assert.Equal(ConditionThemeType.PolarTwilight, actual);
    }

    [Fact]
    public void TestAboveHorizonTheme() {
        WeatherElement weatherEntry = new() {
            Condition = new(ConditionType.ClearSky, new MeasureD())
        };
        RiseSet riseSet = new() {
            AboveHorizon = true
        };
        var actual = weatherEntry.GetConditionTheme(riseSet);
        Assert.Equal(ConditionThemeType.Day, actual);
    }

    [Fact]
    public void TestDayTheme() {
        WeatherElement weatherEntry = new() {
            Condition = new(ConditionType.ClearSky, new MeasureD()),
            Validity = new(
                DateTimeOffset.Parse("2021-01-01T12:00:00"),
                DateTimeOffset.Parse("2021-01-01T12:00:00"))
        };
        RiseSet riseSet = new() {
            Rise = DateTimeOffset.Parse("2021-01-01T08:00:00"),
            Set = DateTimeOffset.Parse("2021-01-01T16:00:00")
        };
        var actual = weatherEntry.GetConditionTheme(riseSet);
        Assert.Equal(ConditionThemeType.Day, actual);
    }

    [Fact]
    public void TestNightTheme() {
        WeatherElement weatherEntry = new() {
            Condition = new(ConditionType.ClearSky, new MeasureD()),
            Validity = new(
                DateTimeOffset.Parse("2021-01-01T18:00:00"),
                DateTimeOffset.Parse("2021-01-01T18:00:00"))
        };
        RiseSet riseSet = new() {
            Rise = DateTimeOffset.Parse("2021-01-01T08:00:00"),
            Set = DateTimeOffset.Parse("2021-01-01T16:00:00")
        };
        var actual = weatherEntry.GetConditionTheme(riseSet);
        Assert.Equal(ConditionThemeType.Night, actual);
    }

    [Theory]
    [InlineData(0.4 - 0.1)]             //  0: Calm
    [InlineData(1.5 - 0.1)]             //  1: Light air
    [InlineData(3.3 - 0.1)]             //  2: Light breeze
    [InlineData(5.5 - 0.1)]             //  3: Gentle breeze
    [InlineData(7.9 - 0.1)]             //  4: Moderate breeze
    [InlineData(10.7 - 0.1)]            //  5: Fresh breeze
    [InlineData(13.8 - 0.1)]            //  6: Strong breeze
    [InlineData(17.1 - 0.1)]            //  7: Moderate gale
    [InlineData(20.7 - 0.1)]            //  8: Fresh gale
    [InlineData(24.4 - 0.1)]            //  9: Strong gale
    [InlineData(28.4 - 0.1)]            // 10: Storm
    [InlineData(32.6 - 0.1)]            // 11: Violent storm
    [InlineData(double.MaxValue - 0.1)] // 12: Hurricane
    public void TestGetWindImage(double windSpeed) {
        WeatherElement weatherElement = new() {
            WindSpeed = new List<ElementValue<WindSpeedType>> {
                new(WindSpeedType.Sustained, new(windSpeed, "m/s"))
            }
        };
        var actual = weatherElement.GetWindImage("1rem", "1rem");
        _output.WriteLine(actual);
    }
}
#endif
