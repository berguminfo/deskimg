namespace Deskimg.Meteorology.Tests;

using Deskimg.Ephemeris;

public class AlgorithmsTests {

    const WeatherConditions TestCondition = WeatherConditions.Showers;
    const string TestTimeZone = "CET";

    static DateTimeOffset GetTestDateTime(int hours) =>
        DateTimeOffset.Parse(string.Format("2023-03-21T{0:D2}:00:00+00:00", hours));

    [Theory]
    [InlineData(13, TestCondition)]
    [InlineData(1, TestCondition | WeatherConditions.Night)]
    public void TestSkyCondition_RiseAndSet(int hours, WeatherConditions expected) {
        RiseSet solarRiseSet = new() {
            Rise = GetTestDateTime(8),
            Set = GetTestDateTime(16)
        };
        MeteorologyReading actual = new() {
            Provider = typeof(AlgorithmsTests),
            Conditions = TestCondition
        };
        Algorithms.SetVariantConditions(actual, GetTestDateTime(hours), TestTimeZone, solarRiseSet);
        Assert.Equal(expected, actual.Conditions);
    }

    [Theory]
    [InlineData(1, TestCondition)]
    public void TestSkyCondition_AboveHorizon(int hours, WeatherConditions expected) {
        RiseSet solarRiseSet = new() {
            AboveHorizon = true
        };
        MeteorologyReading actual = new() {
            Provider = typeof(AlgorithmsTests),
            Conditions = TestCondition
        };
        Algorithms.SetVariantConditions(actual, GetTestDateTime(hours), TestTimeZone, solarRiseSet);
        Assert.Equal(expected, actual.Conditions);
    }

    [Theory]
    [InlineData(7, TestCondition | WeatherConditions.Night)]
    [InlineData(8, TestCondition | WeatherConditions.PolarNight)]
    [InlineData(14, TestCondition | WeatherConditions.PolarNight)]
    [InlineData(15, TestCondition | WeatherConditions.Night)]
    public void TestSkyCondition_BelowHorizon(int hours, WeatherConditions expected) {
        RiseSet solarRiseSet = new() {
            BelowHorizon = true
        };
        MeteorologyReading actual = new() {
            Provider = typeof(AlgorithmsTests),
            Conditions = TestCondition
        };
        Algorithms.SetVariantConditions(actual, GetTestDateTime(hours), TestTimeZone, solarRiseSet);
        Assert.Equal(expected, actual.Conditions);
    }
}
