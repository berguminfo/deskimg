using System.Text;

namespace Deskimg.Meteorology.GOV.Tests;

using Deskimg.Ephemeris;

public class MetarTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    class MetarJSRuntimeMock(
        bool belowHorizon = false,
        bool aboveHorizon = false) : JSRuntimeMock {

        public override RiseSet GetSolarRiseSet() => new() {
            Rise = belowHorizon || aboveHorizon ? null : DateTimeOffset.Parse("2021-12-07T08:00:01Z"),
            Set = belowHorizon || aboveHorizon ? null : DateTimeOffset.Parse("2021-12-07T16:00:00Z"),
            BelowHorizon = belowHorizon,
            AboveHorizon = aboveHorizon,
        };
    }

    readonly Geoposition observer = new() {
        Coordinate = new() {
            Latitude = 70,
            Longitude = 19
        },
        TimeZone = "CET"
    };

    Metar GetProvider(bool belowHorizon = false, bool aboveHorizon = false) => new(
        fixture.CreateLogger<Metar>(output),
        fixture.Endpoint,
        fixture.Http,
        // NOTE: for now only this test will use this mock
        new MetarJSRuntimeMock(belowHorizon, aboveHorizon),
        fixture.MemoryCache) {
        CacheControl = new(),
        // should match a file in _development/sample-data/tgftp.nws.noaa.gov...
        Tag = new("metar.noaa.gov", "ENTC"),
    };

    [Fact]
    [UnitTest]
    public async Task TestGetObservation() {
        // fetch from _development/sample-data/tgftp.nws.noaa.gov_data_observations_metar_stations_ENTC
        // 2021/12/07 08:00
        // ENTC 070800Z 02010KT 9999 SCT023 BKN045 OVC055 04/01 Q1016 RMK WIND 2600FT 36017KT
        var provider = GetProvider();
        var actual = (await provider.GetObservationsAsync(observer)).FirstOrDefault();
        Assert.NotNull(actual);
        Assert.Equal(typeof(IObservationProvider), actual.Provider);
        Assert.Equal(DateTimeOffset.Parse("2021-12-07T08:00:00Z"), actual.ValidFrom.ToUniversalTime());
        Assert.NotEqual(DateTimeOffset.MinValue, actual.ValidTo);
        Assert.Equal(20, actual.Wind?.FromDirection?.Value);
        Assert.Equal(10, actual.Wind?.Sustained?.Value);
        Assert.Equal("kn", actual.Wind?.Sustained?.Unit);
        Assert.Equal(WeatherConditions.PartlyCloudy | WeatherConditions.Night, actual.Conditions); // NOTE: SCT overrides OVC
        Assert.Equal(4, actual.Temperature?.Air?.Value);
        Assert.Equal(1, actual.Temperature?.DewPoint?.Value);
        Assert.Equal(1016, actual.Pressure?.Atmospheric?.Value);
    }

    // PolarNight starts at 09:00 and ends at 15:00 (inclusive) local time
    // NOTE: expect time zone to be CET (+01:00) for this test
    [Theory]
    [InlineData("ENTC 250759Z 10008KT 9999 CAVOK M35/ A2816", WeatherConditions.ClearSky | WeatherConditions.PolarNight)]
    [InlineData("ENTC 251601Z 10008KT 9999 -RA M35/ A2816", WeatherConditions.Rain | WeatherConditions.Light)]
    [UnitTest]
    public async Task TestClearSky_PolarNight(string line, WeatherConditions expected) {
        string metar = "2023/12/21 00:00\r\n" + line + "\r\n";
        var stream = new MemoryStream(Encoding.ASCII.GetBytes(metar));
        var provider = GetProvider(belowHorizon: true);
        var actual = await provider.ParseMetarAsync(observer, stream);
        Assert.NotNull(actual);
        Assert.Equal(expected, actual.Conditions);
    }

    [Theory]
    [InlineData("ENTC 222350Z AUTO 23009KT 9999 FEW160/// 11/06 Q1007 RMK WIND 2600FT 17031KT")]
    [InlineData("ENTC 102350Z AUTO 00000KT 9999 -RA ///043/// 07/05 Q0993 RMK WIND 2600FT 19013KT")]
    [Bug]
    public void TestUnknownConditions(string line) {
        MeteorologyReading observation = new() {
            Provider = typeof(MetarTests),
            ValidFrom = DateTimeOffset.UtcNow
        };
        var actual = Metar.ParseMetar(observation, line);
        Assert.NotEqual(WeatherConditions.Unknown, actual.Conditions);
    }

    [Theory]
    [InlineData("NZSP 251750Z 10008KT 9999 FEW150 M35/ A2816 RMK CLN AIR 10008KT ALL WNDS GRID SDG/HDG", -35)]
    [InlineData("ENTC 131220Z 28017KT 9999 BKN033 08/04 Q1019 NOSIG RMK WIND 2600FT 25020G31KT", 8)]
    [InlineData("NZSP 281750Z 03019KT 0400 FZFG BLSN FEW100 M50/ A2792 RMK PK WND 02026/1749 CLN AIR 02022KT ALL WNDS GRID", -50)]
    [Bug]
    public void TestUnknownTemperatures(string line, double expected) {
        MeteorologyReading observation = new() {
            Provider = typeof(MetarTests),
            ValidFrom = DateTimeOffset.UtcNow
        };
        var actual = Metar.ParseMetar(observation, line);
        Assert.NotNull(actual);
        Assert.Equal("°C", actual.Temperature?.Air?.Unit);
        Assert.Equal(expected, actual.Temperature?.Air?.Value);
    }
}
