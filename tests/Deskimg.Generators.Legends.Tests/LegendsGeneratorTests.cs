namespace Deskimg.Generators.Legends.Tests;

[UsesVerify]
public class LegendsGeneratorTests {

    [Fact]
    public Task VerifySourceGenerator() {
        string source =
            """
            using Deskimg;

            namespace Namespace1.Namespace2; // generator requires namespaced types

            [LegendsInlineData(MethodName = "TestInlineData")]
            public class TestClass { }
            """;
        return TestHelper.Verify(new LegendsGenerator(), source);
    }
}
