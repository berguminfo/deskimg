using System.Runtime.CompilerServices;

namespace Deskimg.Generators.Legends.Tests;

public static class ModuleInitializer {

    [ModuleInitializer]
    public static void Init() => VerifySourceGenerators.Initialize();
}
