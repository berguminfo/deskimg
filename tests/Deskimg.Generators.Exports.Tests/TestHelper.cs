namespace Deskimg.Generators.Exports.Tests;

public static class TestHelper {

    public static Task Verify(IIncrementalGenerator generator, string source) {

        // parse the provided string into a C# syntax tree
        SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(source);

        // create references for assemblies we require
        IEnumerable<PortableExecutableReference> references = new[] {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location)
        };

        // create a roslyn compilation for the syntax tree
        CSharpCompilation compilation = CSharpCompilation.Create(
            assemblyName: "Tests",
            syntaxTrees: new[] { syntaxTree },
            references: references);

        // the GeneratorDriver is used to run our generator against a compilation
        GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

        // run the source generator!
        driver = driver.RunGenerators(compilation);

        // use verify to snapshot test the source generator output!
        return Verifier.Verify(driver).UseDirectory("Snapshots");
    }
}
