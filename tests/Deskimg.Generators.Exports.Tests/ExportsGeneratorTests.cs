namespace Deskimg.Generators.Exports.Tests;

[UsesVerify]
public class ExportsGeneratorTests {

    [Fact]
    public Task VerifySourceGenerator() {
        string source =
            """

            //
            // required types without adding dependencies for this test
            //
            namespace Deskimg.Composition {

                [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = true)]
                public sealed class ExportAttribute<T> : System.Attribute {
                    public ExportAttribute(string _) { }
                }

                [System.AttributeUsage(System.AttributeTargets.Class)]
                public sealed class AssemblyExportsGeneratedAttribute : System.Attribute { }

                public interface IExportsCollection { }
            }

            //
            // add a sample service implementing multiple interfaces
            //
            namespace Tests {

                using Deskimg.Composition;

                public interface IService { }

                [Export<IService>("test.1.1")]
                [Export<IService>("test.1.2")]
                public class ExportedService1 { }

                [Export<IService>("test.2.1")]
                public class ExportedService2 { }

                [AssemblyExportsGenerated]
                public partial class TestExports {
                    public partial IExportsCollection GetExports();
                }
            }
            """;
        return TestHelper.Verify(new ExportsGenerator(), source);
    }
}
