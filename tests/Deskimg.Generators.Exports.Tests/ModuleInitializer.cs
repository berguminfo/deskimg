using System.Runtime.CompilerServices;

namespace Deskimg.Generators.Exports.Tests;

public static class ModuleInitializer {

    [ModuleInitializer]
    public static void Init() => VerifySourceGenerators.Initialize();
}
