﻿//HintName: TestExports.g.cs
//<auto-generated/>
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

using System.Collections.Immutable; // ToImmutableDictionary

namespace Tests;

partial class TestExports : Deskimg.Composition.IAssemblyExports {

    public partial System.Collections.Immutable.IImmutableDictionary<(System.Type, string), System.Type> GetExports() =>
        new System.Collections.Generic.Dictionary<(System.Type, string), System.Type> {
                [(typeof(Tests.IService), "test.1.1")] = typeof(Tests.ExportedService1),
        [(typeof(Tests.IService), "test.1.2")] = typeof(Tests.ExportedService1),
        [(typeof(Tests.IService), "test.2.1")] = typeof(Tests.ExportedService2),

        }.ToImmutableDictionary();
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member