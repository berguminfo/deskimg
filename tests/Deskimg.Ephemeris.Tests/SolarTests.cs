namespace Deskimg.Ephemeris.Tests;

public class SolarTests {

    class SolarMock(string riseAt, string setsAt) : JSRuntimeMock {

        public override RiseSet GetSolarRiseSet() => new() {
            Rise = DateTimeOffset.Parse(riseAt),
            Set = DateTimeOffset.Parse(setsAt),
        };
    }

    // unused
    readonly Geocoordinate coords = new();

    [Fact]
    public async Task GetRiseSetTest_Mock() {
        Earth earth = new(new SolarMock("2020-12-13T12:34:56", "2020-12-13T12:34:56"));
        // https://www.timeanddate.com/sun/@40,-20
        RiseSet actual = await earth.GetRiseSetAsync<Solar>(DateTimeOffset.UtcNow, coords);
        // NOTE: No calculation, it's a JSRuntimeMock result.
        Assert.Equal("2020-12-13 12:34:56", actual.Rise?.ToString("yyyy-MM-dd HH:mm:ss"));
        Assert.Equal("2020-12-13 12:34:56", actual.Set?.ToString("yyyy-MM-dd HH:mm:ss"));
    }
}
