namespace Deskimg.Ephemeris.Tests;

public class LunarTests {

    class LunarMock(string riseAt, string setsAt) : JSRuntimeMock {

        public override RiseSet GetLunarRiseSet() => new() {
            Rise = DateTimeOffset.Parse(riseAt),
            Set = DateTimeOffset.Parse(setsAt),
        };

        public override DateTimeOffset GetLunarPhase0() {
            return DateTimeOffset.UtcNow;
        }

        public override DateTimeOffset GetLunarPhase25() {
            return DateTimeOffset.UtcNow;
        }

        public override DateTimeOffset GetLunarPhase50() {
            return DateTimeOffset.UtcNow;
        }

        public override DateTimeOffset GetLunarPhase75() {
            return DateTimeOffset.UtcNow;
        }
    }

    // unused
    readonly Geocoordinate coords = new();

    [Fact]
    public async Task TestGetRiseSet_Mock() {
        Earth earth = new(new LunarMock("2020-12-13T12:34:56", "2020-12-13T12:34:56"));
        // https://www.timeanddate.com/moon/@40,-20
        RiseSet actual = await earth.GetRiseSetAsync<Lunar>(DateTimeOffset.UtcNow, coords);
        // NOTE: No calculation, it's a JSRuntimeMock result.
        Assert.Equal("2020-12-13 12:34:56", actual.Rise?.ToString("yyyy-MM-dd HH:mm:ss"));
        Assert.Equal("2020-12-13 12:34:56", actual.Set?.ToString("yyyy-MM-dd HH:mm:ss"));
    }

    [Fact(Skip = "NotSupportedException")]
    public async Task TestGetUpcomingLunarPhase_Mock() {
        Earth earth = new(new LunarMock("2020-12-13T12:34:56", "2020-12-13T12:34:56"));
        var result = earth.GetUpcomingLunarPhaseAsync<Lunar>(DateTimeOffset.UtcNow);
        PhaseInfo[] actual = await result.ToArrayAsync();
        Assert.Equal(8, actual.Length);
    }
}
