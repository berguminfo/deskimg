namespace Deskimg.Ephemeris.Tests;

public class RiseSetTests {

    [Theory]
    [InlineData(0.50, 12)]
    [InlineData(0.75, 15)]
    [InlineData(1.00, 18)]
    [InlineData(-.75, 21)]
    [InlineData(-.50, 0)]
    [InlineData(-.25, 3)]
    [InlineData(0.00, 6)]
    [InlineData(0.25, 9)]
    // polar twilight
    [InlineData(0.50, 12, 25, 23)]
    [InlineData(0.00, 25, 25, 23)]
    [InlineData(1.00, 23, 25, 23)]
    [InlineData(-.50, 24, 25, 23)]
    public void TestGetDaylightFactor(double expected, double whenHours, double riseHours = 6, double setHours = 18) {
        var now = DateTimeOffset.Parse("2023-01-01T00:00:00+02:00");
        RiseSet riseSet = new() {
            Rise = now.AddHours(riseHours),
            Set = now.AddHours(setHours)
        };
        double actual = riseSet.GetDaylightFactor(now.AddHours(whenHours));
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(true, false, 0.5)]
    [InlineData(false, true, -0.5)]
    public void TestGetDaylightFactor_State(bool aboveHorizon, bool belowHorizon, double expected) {
        RiseSet riseSet = new() {
            AboveHorizon = aboveHorizon,
            BelowHorizon = belowHorizon
        };
        double actual = riseSet.GetDaylightFactor(DateTimeOffset.Now);
        Assert.Equal(expected, actual);
    }

    [Fact]
    [Bug]
    public void TestGetDaylightFactor_RiseNotSet() {
        RiseSet riseSet = new() {
            Rise = DateTimeOffset.Parse("2023-05-15T23:55:00+02:00"),
            Set = DateTimeOffset.Parse("2023-05-16T01:24:00+02:00")
        };
        double actual = riseSet.GetDaylightFactor(DateTimeOffset.Parse($"2023-05-15T12:00+02:00"));
        Assert.True(actual > 0);
    }
}
