namespace Deskimg.Foundation.Tests;

public class SRTests {

    readonly string[] resourceNames = typeof(SR).Assembly.GetManifestResourceNames();

    [Theory]
    [InlineData("da-DK")]
    [InlineData("en-GB")]
    [InlineData("en-US")]
    [InlineData("fi-FI")]
    [InlineData("nb-NO")]
    [InlineData("nn-NO")]
    [InlineData("sv-FI")]
    [InlineData("sv-SE")]
    public async Task TestInitialize_Exists(string cultureName) {
        var resourceName = $"Deskimg.Resources.langs.{cultureName}.json";
        var actual = resourceNames.Contains(resourceName);
        Assert.True(actual);

        await SR.InitializeAsync(cultureName);
        Assert.Equal(cultureName, SR.Default.Language);
    }

    [Fact]
    public async Task TestInitialize_Fallback() {
        await SR.InitializeAsync(string.Empty);
        Assert.Equal("en-GB", SR.Default.Language);
    }
}
