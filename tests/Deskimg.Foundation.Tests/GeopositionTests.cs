namespace Deskimg.Foundation.Tests;

public class GeopositionTests {

    [Fact]
    [UnitTest]
    public void TestToString_Coordinate() {
        Geoposition test = new();
        Assert.Equal(test.Coordinate?.ToString(), test.ToString());
    }

    [Fact]
    [UnitTest]
    public void TestToString_City() {
        Geoposition test = new() {
            CivicAddress = new() { City = "Test" }
        };
        Assert.Equal(test.CivicAddress.City, test.ToString());
    }
}
