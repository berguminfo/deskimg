namespace Deskimg.Foundation.Tests;

public class GeocoordinateTests {

    const char Degree = '\u00B0';
    const char Prime = '\u2032';
    const char DoublePrime = '\u2033';

    [Theory]
    [InlineData(69, 18, "N", "W")]
    [InlineData(69, -18, "N", "E")]
    [InlineData(-69, 18, "S", "W")]
    [InlineData(-69, -18, "S", "E")]
    [UnitTest]
    public void TestToString(double latitude, double longitude, string ns, string ew) {
        Geocoordinate test = new() {
            Latitude = latitude,
            Longitude = longitude
        };
        string expected =
            $"{Math.Abs(latitude)}{Degree}0{Prime}0{DoublePrime}{ns}" +
            " " +
            $"{Math.Abs(longitude)}{Degree}0{Prime}0{DoublePrime}{ew}";
        Assert.Equal(expected, test.ToString());
    }
}
