#if DEBUG

namespace Deskimg.Foundation.Tests;

public class DebugOptionsTests(
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly EndpointOptions endpoint = fixture.Endpoint;

    [Fact]
    public void TestFetchIsSet() {
        Assert.Equal("sample-data", endpoint.Dbug.Fetch);
    }

    [Fact]
    public void TestSetsIsSet() {
        Assert.Equal("sets-data", endpoint.Dbug.Sets);
    }
}


#endif
