using System.Text;

namespace Deskimg.Syndication.Tests;

public class SyndicationFeedTests {

    async Task<SyndicationFeed> CreateFeedAsync(string s) {
        SyndicationFeed result = new();
        await result.LoadAsync(new MemoryStream(Encoding.UTF8.GetBytes(s)));
        return result;
    }

    [Fact]
    [UnitTest]
    async Task TestLoad_Xml() {
        await Assert.ThrowsAsync<NotSupportedException>(() => CreateFeedAsync(
            """
            <?xml version="1.0"?>
            <root></root>
            """));
    }

    [Fact]
    [UnitTest]
    async Task TestLoad_Rss20_1() {
        SyndicationItem expected = new() {
            Id = string.Empty,
            Published = DateTimeOffset.Parse("Wed, 22 Nov 2023 04:41:17 GMT"),
            Title = "RSS:title",
            Summary = "RSS:summary",
            Link = "RSS:link",
            Enclosure = "RSS:enclosure"
        };
        var feed = await CreateFeedAsync(
            $$"""
            <rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
            <channel>
                <title>Siste – Siste nytt – NRK</title>
                <item>
                    <title>{{expected.Title}}</title>
                    <link>{{expected.Link}}</link>
                    <description>{{expected.Summary}}</description>
                    <category>Nyhetssenter Troms og Finnmark</category>
                    <pubDate>Wed, 22 Nov 2023 04:41:17 GMT</pubDate>
                    <dc:date>2023-11-22T04:41:17Z</dc:date>
                    <media:content medium="image" type="image/jpeg" url="{{expected.Enclosure}}">
                    </media:content>
                </item>
            </channel>
            </rss>
            """);
        TestRunner(expected, feed);
    }

    [Fact]
    [UnitTest]
    async Task TestLoad_Rss20_2() {
        SyndicationItem expected = new() {
            Id = string.Empty,
            Published = DateTimeOffset.Parse("Wed, 22 Nov 2023 05:19:02 GMT"),
            Title = "RSS:title",
            Summary = "RSS:summary",
            Link = "RSS:link",
            Enclosure = "RSS:enclosure"
        };
        var feed = await CreateFeedAsync(
            $$"""
            <rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
            <channel>
                <title>TV 2</title>
                <item>
                    <title>{{expected.Title}}</title>
                    <link>{{expected.Link}}</link>
                    <description>{{expected.Summary}}</description>
                    <enclosure url="{{expected.Enclosure}}" length="1" type="image/jpg" />
                    <category domain="section">Nyheter</category>
                    <category domain="tags">innenriks, nyheter</category>
                    <pubDate>{{expected.Published?.ToString("r")}}</pubDate>
                    <dc:date>2023-11-22T05:19:02Z</dc:date>
                </item>
            </channel>
            </rss>
            """);
        TestRunner(expected, feed);
    }

    [Fact]
    async Task TestLoad_Atom10() {
        SyndicationItem expected = new() {
            Id = string.Empty,
            Published = DateTimeOffset.Parse("2023-12-23"),
            Title = "A10:title",
            Summary = "A10:summary",
            Link = "A10:link",
            Enclosure = "A10:enclosure"
        };
        var feed = await CreateFeedAsync(
            $$"""
            <feed xmlns="http://www.w3.org/2005/Atom">
                <entry>
                    <published>{{expected.Published?.ToString("u")}}</published>
                    <title>{{expected.Title}}</title>
                    <summary>{{expected.Summary}}</summary>
                    <link rel="alternate" type="text/html" href="{{expected.Link}}"/>
                    <link rel="enclosure" type="image/jpg" href="{{expected.Enclosure}}"/>
                </entry>
            </feed>
            """);
        TestRunner(expected, feed);
    }

    void TestRunner(SyndicationItem expected, SyndicationFeed feed) {
        Assert.NotNull(feed.Items);
        if (feed.Items?.FirstOrDefault() is SyndicationItem actual) {
            Assert.Equal(expected.Published, actual.Published);
            Assert.Equal(expected.Title, actual.Title);
            Assert.Equal(expected.Summary, actual.Summary);
            Assert.Equal(expected.Link, actual.Link);
            Assert.Equal(expected.Enclosure, actual.Enclosure);
        }
    }
}
