namespace Deskimg.Extensions.Tests;

public class DeflateBase64UrlTests(
    ITestOutputHelper output) {

    [Theory]
    [InlineData("https://313fc6fb", "eJzLKCkpKLbS1zc2NE5LNktLAgAuPgUq")] // eJzLKCkpKLbS1zc2NE5LNktLAgAuPgUq
    [InlineData("https://f85214d7", "eJzLKCkpKLbS10-zMDUyNEkxBwAuEgTR")] // eJzLKCkpKLbS10+zMDUyNEkxBwAuEgTR
    [InlineData("https://6a8fe513", "eJzLKCkpKLbS1zdLtEhLNTU0BgAvMAT_")] // eJzLKCkpKLbS1zdLtEhLNTU0BgAvMAT/
    [InlineData("https://c897d7e1", "eJzLKCkpKLbS10-2sDRPMU81BAAu_AUI")] // eJzLKCkpKLbS10+2sDRPMU81BAAu/AUI
    [InlineData("https://eb274ca", "eJzLKCkpKLbS109NMjI3SU4EACqfBPQ")] // eJzLKCkpKLbS109NMjI3SU4EACqfBPQ=
    [InlineData("https://b85cb32", "eJzLKCkpKLbS10-yME1OMjYCACpIBMU")] // eJzLKCkpKLbS10+yME1OMjYCACpIBMU=
    [InlineData("https://6bbf573", "eJzLKCkpKLbS1zdLSkozNTcGACp_BMs")] // eJzLKCkpKLbS1zdLSkozNTcGACp/BMs=
    [InlineData("https://d8729c0", "eJzLKCkpKLbS10-xMDeyTDYAACl_BJ0")] // eJzLKCkpKLbS10+xMDeyTDYAACl/BJ0=
    [InlineData("https://4c70707e", "eJzLKCkpKLbS1zdJNjcAwlQALZEEzQ")] // eJzLKCkpKLbS1zdJNjcAwlQALZEEzQ==
    [InlineData("https://ed9dea", "eJzLKCkpKLbS109NsUxJTQQAJrgE-A")] // eJzLKCkpKLbS109NsUxJTQQAJrgE+A==
    [InlineData("https://1377ea", "eJzLKCkpKLbS1zc0NjdPTQQAI_wEZA")] // eJzLKCkpKLbS1zc0NjdPTQQAI/wEZA==
    [InlineData("https://%4%337b9689b", "eJzLKCkpKLbS11c1UTU2Nk-yNLOwTAIAP_4Fiw")] // eJzLKCkpKLbS11c1UTU2Nk+yNLOwTAIAP/4Fiw==
    //[Trait("output", "yaml")]
    public void TestEncode(string input, string expected) {
        string actual = DeflateBase64Url.Encode(input);
        output.WriteLine($"""
            #
            - input:    '{input}'
              expected: '{expected}'
              actual:   '{actual}'
        """);
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData("https://313fc6fb", "eJzLKCkpKLbS1zc2NE5LNktLAgAuPgUq")] // eJzLKCkpKLbS1zc2NE5LNktLAgAuPgUq
    [InlineData("https://f85214d7", "eJzLKCkpKLbS10-zMDUyNEkxBwAuEgTR")] // eJzLKCkpKLbS10+zMDUyNEkxBwAuEgTR
    [InlineData("https://6a8fe513", "eJzLKCkpKLbS1zdLtEhLNTU0BgAvMAT_")] // eJzLKCkpKLbS1zdLtEhLNTU0BgAvMAT/
    [InlineData("https://c897d7e1", "eJzLKCkpKLbS10-2sDRPMU81BAAu_AUI")] // eJzLKCkpKLbS10+2sDRPMU81BAAu/AUI
    [InlineData("https://eb274ca", "eJzLKCkpKLbS109NMjI3SU4EACqfBPQ")] // eJzLKCkpKLbS109NMjI3SU4EACqfBPQ=
    [InlineData("https://b85cb32", "eJzLKCkpKLbS10-yME1OMjYCACpIBMU")] // eJzLKCkpKLbS10+yME1OMjYCACpIBMU=
    [InlineData("https://6bbf573", "eJzLKCkpKLbS1zdLSkozNTcGACp_BMs")] // eJzLKCkpKLbS1zdLSkozNTcGACp/BMs=
    [InlineData("https://d8729c0", "eJzLKCkpKLbS10-xMDeyTDYAACl_BJ0")] // eJzLKCkpKLbS10+xMDeyTDYAACl/BJ0=
    [InlineData("https://4c70707e", "eJzLKCkpKLbS1zdJNjcAwlQALZEEzQ")] // eJzLKCkpKLbS1zdJNjcAwlQALZEEzQ==
    [InlineData("https://ed9dea", "eJzLKCkpKLbS109NsUxJTQQAJrgE-A")] // eJzLKCkpKLbS109NsUxJTQQAJrgE+A==
    [InlineData("https://1377ea", "eJzLKCkpKLbS1zc0NjdPTQQAI_wEZA")] // eJzLKCkpKLbS1zc0NjdPTQQAI/wEZA==
    [InlineData("https://%4%337b9689b", "eJzLKCkpKLbS11c1UTU2Nk-yNLOwTAIAP_4Fiw")] // eJzLKCkpKLbS11c1UTU2Nk+yNLOwTAIAP/4Fiw==
    [Trait("output", "yaml")]
    public void TestDecode(string expected, string input) {
        string actual = DeflateBase64Url.Decode(input);
        output.WriteLine($"""
            #
            - input:    '{input}'
              expected: '{expected}'
              actual:   '{actual}'
        """);
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void TestWhenDecodingInvalidData() {
        Assert.Throws<InvalidDataException>(() =>
            DeflateBase64Url.Decode("-JzLKCkpKLbS1zc2NE5LNktLAgAuPgU"));
        Assert.Throws<InvalidDataException>(() =>
            DeflateBase64Url.Decode("eJzLKCkpKLbS1zc2NE5LNktLAgAuPg-"));
    }

    [Fact]
    [Trait("output", "yaml")]
    public void TestWhenEncodingUncompressed() {
        string expected = "https://%4%337b9689b";
        string input = DeflateBase64Url.Encode(expected, false);
        string actual = DeflateBase64Url.Decode(input);
        output.WriteLine($"""
            #
            - input:    '{input}'
              expected: '{expected}'
              actual:   '{actual}'
        """);
        Assert.Equal(expected, actual);
    }
}
