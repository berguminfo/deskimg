#define SHA1

using System.Security.Cryptography;

namespace Deskimg.Extensions.Tests;

public class FetchTokenTests(
    ITestOutputHelper output) {

    [Theory]
#if SHA1
    [InlineData(35)]
#else
    [InlineData(51)]
#endif
    [Trait("output", "yaml")]
    public void TestCreate(int expected) {
        for (int i = 0; i < 10; ++i) {
            string input = FetchToken.Create(shr: 28);
            output.WriteLine($"""
                #
                - input:    '{input}'
                  expected: {expected}
                  actual:   {input.Length}
            """);
            Assert.Equal(expected, input.Length);
        }
    }

    [Theory]
#if SHA1
    [InlineData(35)]
#else
    [InlineData(51)]
#endif
    [Trait("output", "yaml")]
    public void TestCreate_Internal(int expected) {
        byte[] salt = RandomNumberGenerator.GetBytes(6);
        string saltArray = string.Join(", ", salt.Select(x => x));
        string saltArrayX2 = string.Join(", ", salt.Select(x => $"0x{x:X2}"));
        long ticks = DateTime.UtcNow.Ticks;
        int shr = 28;
        string hash = ThisAssembly.Git.Sha;
        string input = FetchToken.Create(salt, ticks, shr, hash);
        output.WriteLine($"""
                #
                - salt:     [{saltArray}] # [{saltArrayX2}] 
                  ticks:    {ticks}
                  shr:      {shr}
                  hash:     '{hash}'
                  input:    '{input}'
                  expected: {expected}
                  actual:   {input.Length}
            """);
        Assert.Equal(expected, input.Length);
    }

    [Fact]
    [Trait("output", "yaml")]
    public void TestVerify_Empty() {
        string input = string.Empty;
        bool actual = FetchToken.Verify(input);
        output.WriteLine($"""
            #
            - input:    '{input}'
              expected: False
              actual:   {actual}
        """);
        Assert.False(actual);
    }

    [Fact]
    [Trait("output", "yaml")]
    public void TestVerify_0() {
        string input = FetchToken.Create();
        bool actual = FetchToken.Verify(input);
        output.WriteLine($"""
            #
            - input:    '{input}'
              expected: True
              actual:   {actual}
        """);
        Assert.True(actual);
    }

    [Fact]
    [Trait("output", "yaml")]
    public void TestVerify_28() {
        string input = FetchToken.Create(shr: 28);
        bool actual = FetchToken.Verify(input, shr: 28);
        output.WriteLine($"""
            #
            - input:    '{input}'
              expected: True
              actual:   {actual}
        """);
        Assert.True(actual);
    }
}
