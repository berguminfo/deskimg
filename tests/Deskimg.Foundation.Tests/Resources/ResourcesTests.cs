namespace Deskimg.Resources.Tests;

public class ResourcesTests {

    [Theory]
    [InlineData("moon-above-horizon")]
    [InlineData("moon-below-horizon")]
    [InlineData("moon-rise")]
    [InlineData("moon-set")]
    [InlineData("sun-above-horizon")]
    [InlineData("sun-below-horizon")]
    [InlineData("sun-rise")]
    [InlineData("sun-set")]
    [InlineData("tide-high")]
    [InlineData("tide-low")]
    public void TestEmbeddedStatus(string name) {
        var actual = Embedded.Status(name);
        Assert.StartsWith("<svg", actual);
    }
}
