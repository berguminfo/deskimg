namespace Deskimg.Foundation.Tests;

public class EndpointOptionsTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly EndpointOptions endpoint = fixture.Endpoint;
    readonly string testKey = "test";

#if DEBUG
    [Fact]
#else
    [Fact(Skip = "will always succeed but nothing downloads")]
#endif
    public void TestFetch_GivenInvalidUri() {
        Assert.Throws<UriFormatException>(() => endpoint.Fetch(string.Empty));
    }

    [Fact]
    public void TestFetch() {
        Uri? actual = endpoint.Fetch(testKey);
        Assert.NotNull(actual);
        output.WriteLine(actual.AbsoluteUri);
        Assert.Contains("sample-data", actual.AbsoluteUri);
    }

    [Fact]
    public void TestFetch_WithQuery() {
        Uri? actual = endpoint.Fetch(testKey, new { Search = string.Empty });
        Assert.NotNull(actual);
        output.WriteLine(actual.AbsoluteUri);
        char[] invalid = { '%', '{', '}' };
        Assert.False(actual.PathAndQuery.IndexOfAny(invalid) >= 0);
    }

#if DEBUG

    [Fact]
    public void TestFetch_DiscardQuery() {
        Uri? actual = endpoint.Fetch(testKey, discardQuery: true);
        Assert.NotNull(actual);
        output.WriteLine(actual.AbsoluteUri);
        Assert.DoesNotContain("discard", actual.PathAndQuery);
    }

    [Fact]
    public void TestFetch_DiscardPath() {
        Uri? actual = endpoint.Fetch(testKey, discardPath: 1);
        Assert.NotNull(actual);
        output.WriteLine(actual.AbsoluteUri);
        Assert.DoesNotContain("discard", actual.PathAndQuery);
    }

#endif

    [Fact]
    public void TestSets() {
        Uri? actual = endpoint.Sets("test");
        Assert.NotNull(actual);
        output.WriteLine(actual.AbsoluteUri);
#if DEBUG
        Assert.EndsWith("test.json", actual.PathAndQuery);
#else
        Assert.EndsWith("/test", actual.PathAndQuery);
#endif
    }
}
