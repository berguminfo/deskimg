namespace Deskimg.Calendar.Tests;

public class VEventsCalendarTests {

    [Fact]
    [UnitTest]
    public async void TestOnce_Start() {
        VEventsCalendar calendar = [
            new() {
                Start = DateTimeOffset.Parse("2020-01-01T03:00:00+02:00")
            }
        ];
        var actual = (await calendar.SubsetAsync(
            DateTimeOffset.Parse("2020-01-01T01:00:00+01:00"), TimeSpan.FromDays(1))).ToList();
        Assert.Single(actual);
        Assert.Equal("2020-01-01T01:00:00", actual[0].When.ToUniversalTime().ToString("s"));
    }

    [Fact]
    [UnitTest]
    public async void TestOnce_StartDate() {
        VEventsCalendar calendar = [
            new() {
                StartDate = DateOnly.Parse("2020-01-01")
            }
        ];
        var actual = (await calendar.SubsetAsync(
            DateTimeOffset.Parse("2020-01-01T00:00:00-10:00"), TimeSpan.FromDays(1))).ToList();
        Assert.Single(actual);
        Assert.Equal("2020-01-01T11:00:00", actual[0].When.ToLocalTime().ToString("s"));
        Assert.Equal("2020-01-01T10:00:00", actual[0].When.ToUniversalTime().ToString("s"));
    }

    [Fact]
    [UnitTest]
    public async void TestOnce_StartTime() {
        VEventsCalendar calendar = [
            new() {
                StartDate = DateOnly.Parse("2020-01-01"),
                StartTime = TimeOnly.Parse("03:00")
            }
        ];
        var actual = (await calendar.SubsetAsync(
            DateTimeOffset.Parse("2020-01-01T00:00:00+02:00"), TimeSpan.FromDays(1))).ToList();
        Assert.Single(actual);
        Assert.Equal("2020-01-01T01:00:00", actual[0].When.ToUniversalTime().ToString("s"));
    }

    [Fact]
    [UnitTest]
    public async void TestOnce20230508() {
        VEventsCalendar calendar = [
            new() {
                StartDate = DateOnly.Parse("2023-05-08"),
                EndDate = DateOnly.Parse("2023-05-09")
            }
        ];
        var actual = (await calendar.SubsetAsync(
            DateTimeOffset.Parse("2023-05-01"), TimeSpan.FromDays(400))).ToList();
        Assert.Single(actual);
        Assert.Equal("2023-05-08T00:00:00", actual[0].When.ToLocalTime().ToString("s"));
    }

    [Fact]
    [UnitTest]
    public async void TestWeeklyByInterval2() {
        VEventsCalendar calendar = [
            new() {
                RecurrenceRule = new() {
                    Recurrence = VEventRecurrence.Weekly,
                    Interval = 2
                }
            }
        ];
        var actual = await calendar.SubsetAsync(DateTimeOffset.MinValue, TimeSpan.Zero);
        Assert.Throws<NotSupportedException>(() => actual.ToList());
    }

    [Theory]
    [InlineData("2022-12-26")]
    [InlineData("2022-12-27")]
    [InlineData("2022-12-28")]
    [InlineData("2022-12-29")]
    [InlineData("2022-12-30")]
    [InlineData("2022-12-31")]
    [InlineData("2023-01-01")]
    [UnitTest]
    public async void TestWeeklyBySundays(string rangeStart) {
        VEventsCalendar calendar = [
            new() {
                RecurrenceRule = new() {
                    Recurrence = VEventRecurrence.Weekly,
                    DaysOfWeek = DaysOfWeek.Sunday
                }
            }
        ];
        var actual = (await calendar.SubsetAsync(
            DateTimeOffset.Parse(rangeStart), TimeSpan.FromDays(30))).Take(4).ToList();
        Assert.Equal(4, actual.Count);
        IEnumerable<string> expected = [
            "2023-01-01T00:00:00",
            "2023-01-08T00:00:00",
            "2023-01-15T00:00:00",
            "2023-01-22T00:00:00"
        ];
        Assert.All(actual, item => Assert.Contains(item.When.ToLocalTime().ToString("s"), expected));
    }

    [Fact]
    [UnitTest]
    public async Task TestYearly() {
        VEventsCalendar calendar = [
            new() {
                StartDate = DateOnly.MinValue,
                RecurrenceRule = new() {
                    Recurrence = VEventRecurrence.Yearly
                }
            }
        ];
        var actual = (await calendar.SubsetAsync(
            DateTimeOffset.Parse("2023-01-01T00:00:00-12:00"), TimeSpan.FromDays(1))).ToList();
        Assert.Single(actual);
        Assert.Equal("2023-01-01T00:00:00", actual[0].When.ToLocalTime().ToString("s"));
    }

    [Theory]
    [InlineData("2023-01-02", "-1.00:00:00", 0)] // validStart > validEnd
    [InlineData("2023-01-02", "00:00:00", 1)] // validStart >= validEnd
    [InlineData("2023-01-02", "8.00:00:00", 5)]
    [InlineData("2023-01-11", "01:00:00", 0)]
    [InlineData("2023-01-01", "8.00:00:00", 4)]
    [UnitTest]
    public async void TestDaily_MWFT(string rangeStart, string rangeLength, int expected) {

        // ->  M: 2023-01-02
        // ->  W: 2023-01-04
        // ->  F: 2023-01-06
        // NOT S: 2023-01-08
        // ->  T: 2023-01-10
        // NOT W: EndDate
        // NOT F: 2023-01-13
        VEventsCalendar calendar = [
            new() {
                StartDate = DateOnly.Parse("2023-01-02"),
                EndDate = DateOnly.Parse("2023-02-01"),
                RecurrenceRule = new() {
                    Recurrence = VEventRecurrence.Daily,
                    Interval = 2,
                    DaysOfWeek = DaysOfWeek.WesternWorkweek
                }
            }
        ];
        var actual = await calendar.SubsetAsync(DateTimeOffset.Parse(rangeStart), TimeSpan.Parse(rangeLength));
        Assert.Equal(expected, actual.ToList().Count);
    }

    [Theory]
    [InlineData("2020-01-01")]
    [InlineData("0001-01-01")]
    [UnitTest]
    void TestThrowsArgumentException(string startDate) {
        try {
            var expected = DateOnly.Parse(startDate);
            DateTimeOffset actual = new(expected.ToDateTime(TimeOnly.MinValue, DateTimeKind.Utc));
            Assert.Equal(expected, DateOnly.FromDateTime(actual.Date));
        } catch (ArgumentException ex) {
            Assert.Fail(ex.Message);
        }
    }
}
