namespace Deskimg.Calendar.National.Tests;

public class SETests(
    ITestOutputHelper output) : TestRunner(output, new SE()) {

    [Fact]
    [Category("Coverage")]
    public Task CoverageTest() => GetCoverageTest("2024-01-01");

    [Theory]
    [InlineData("Nyårsdagen", "2024-01-01")]
    [InlineData("Trettondedag jul", "2024-01-06")]
    [InlineData("Konungens namnsdag", "2024-01-28")]
    [InlineData("Kronprinsessans namnsdag", "2024-03-12")]
    [InlineData("Långfredagen", "2024-03-29")]
    [InlineData("Påskdagen", "2024-03-31")]
    [InlineData("Annandag påsk", "2024-04-01")]
    [InlineData("Kristi himmelsfärdsdag", "2024-05-09")]
    [InlineData("Pingstdagen", "2024-05-19")]
    [InlineData("Konungens födelsedag", "2024-04-30")]
    [InlineData("Första maj", "2024-05-01")]
    [InlineData("Sveriges nationaldag och svenska flaggans dag", "2024-06-06")]
    [InlineData("Midsommardagen", "2024-06-22")]
    [InlineData("Kronprinsessans födelsedag", "2024-07-14")]
    [InlineData("Drottningens namnsdag", "2024-08-08")]
    [InlineData("Val till Europaparlamentet", "2024-06-09")]
    [InlineData("FN-dagen", "2024-10-24")]
    [InlineData("Alla helgons dag", "2024-11-06")]
    [InlineData("Gustav Adolfsdagen", "2024-11-06")]
    [InlineData("Nobeldagen", "2024-12-10")]
    [InlineData("Drottningens födelsedag", "2024-12-23")]
    [InlineData("Juldagen", "2024-12-25")]
    [InlineData("Annandag jul", "2024-12-26")]
    [UnitTest]
    public async Task TestSubset2024(string s, string expected) {
        var actual = await GetSubsetTest("2024-01-01", 365);
        try {
            Assert.Contains(actual, item => item.Summary == s);
        } catch (Exception) {
            output.WriteLine(s);
            throw;
        }
        try {
            Assert.Contains(actual, item => item.When == expected);
        } catch (Exception) {
            output.WriteLine(expected);
            throw;
        }
    }

    [Theory]
    [InlineData(2020, "2020-06-20")]
    [InlineData(2021, "2021-06-26")]
    [InlineData(2022, "2022-06-25")]
    [InlineData(2023, "2023-06-24")]
    [InlineData(2024, "2024-06-22")]
    [InlineData(2025, "2025-06-21")]
    [InlineData(2026, "2026-06-20")]
    [Feature]
    public async void TestMidsummerDays(int y, string expected) {
        var collection = await GetSubsetTest($"{y}-06-01", 31);
        var actual = collection.FirstOrDefault(item => item.Summary == "Midsommardagen");
        Assert.Equal(expected, actual?.When);
    }

    [Theory]
    [InlineData(2020, "2020-10-31")]
    [InlineData(2021, "2021-11-06")]
    [InlineData(2022, "2022-11-05")]
    [InlineData(2023, "2023-11-04")]
    [InlineData(2024, "2024-11-02")]
    [InlineData(2025, "2025-11-01")]
    [InlineData(2026, "2026-10-31")]
    [Feature]
    public async void TestAllSaintsDays(int y, string expected) {
        var collection = await GetSubsetTest($"{y}-10-01", 2 * 30);
        var actual = collection.FirstOrDefault(item => item.Summary == "Alla helgons dag");
        Assert.Equal(expected, actual?.When);
    }
}
