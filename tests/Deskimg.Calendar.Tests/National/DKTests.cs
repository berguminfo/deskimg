namespace Deskimg.Calendar.National.Tests;

public class DKTests(
    ITestOutputHelper output) : TestRunner(output, new DK()) {

    [Fact]
    [Category("Coverage")]
    public Task CoverageTest() => GetCoverageTest("2025-01-01");

    [Theory]
    [InlineData("Nytårsdag", "2025-01-01")]
    [InlineData("Kronprinsesse Mary", "2025-02-05")]
    [InlineData("Prinsesse Marie", "2025-02-06")]
    [InlineData("Palmesøndag", "2025-04-13")]
    [InlineData("Skærtorsdag", "2025-04-17")]
    [InlineData("Langfredag", "2025-04-18")]
    [InlineData("Påskedag", "2025-04-20")]
    [InlineData("2. påskedag", "2025-04-21")]
    // Excluded at 31-12-2024 [InlineData("Store Bededag", "2025-05-16")]
    [InlineData("Kristi Himmelfartsdag", "2025-05-29")]
    [InlineData("Pinsedag", "2025-06-08")]
    [InlineData("2. pinsedag", "2025-06-09")]
    [InlineData("Besættelsesdagen", "2025-04-09")]
    [InlineData("Dronning Margrethe II", "2025-04-16")]
    [InlineData("Prinsesse Benedikte", "2025-04-29")]
    [InlineData("Befrielsesdagen", "2025-05-05")]
    [InlineData("Kronprins Frederik", "2025-05-26")]
    [InlineData("Grundlovsdag", "2025-06-05")]
    [InlineData("Prins Joachim", "2025-06-07")]
    [InlineData("Valdemarsdag og genforeningsdag", "2025-06-15")]
    [InlineData("Grønlands nationaldag", "2025-06-21")]
    [InlineData("Færøernes nationale festdag", "2025-07-29")]
    [InlineData("Danmarks udsendte", "2025-09-05")]
    [InlineData("Juledag", "2025-12-25")]
    [InlineData("2. juledag", "2025-12-26")]
    [UnitTest]
    public async Task TestSubset2025(string s, string expected) {
        var actual = await GetSubsetTest("2025-01-01", 365);
        try {
            Assert.Contains(actual, item => item.Summary == s);
        } catch (Exception) {
            output.WriteLine(s);
            throw;
        }
        try {
            Assert.Contains(actual, item => item.When == expected);
        } catch (Exception) {
            output.WriteLine(expected);
            throw;
        }
    }

    [Theory]
    [InlineData(2023, "2023-05-05")]
    [InlineData(2024, null)]
    [Feature("Historic")]
    public async Task TestGreatPrayerDay(int y, string? expected) {
        var collection = await GetSubsetTest($"{y}-04-01", 60);
        var actual = collection.FirstOrDefault(item => item.Summary == "Store Bededag");
        Assert.Equal(expected, actual?.When);
    }
}
