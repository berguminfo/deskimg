namespace Deskimg.Calendar.National.Tests;

public abstract class TestRunner(
    ITestOutputHelper testOutputHelper,
    VEventsCalendar calendar) {

    protected readonly ITestOutputHelper output = testOutputHelper;

    protected record Result(string? Summary, string When);

    readonly Dictionary<string, ICollection<Result>> _cache = [];

    protected async Task GetCoverageTest(string start) {
        var result = await calendar.SubsetAsync(DateTimeOffset.Parse(start), TimeSpan.FromDays(365));

        // Sundays
        int count = (
            from item in result
            where item.Event.Classification == AccessClassification.Private
            orderby item.When
            select item).Count();
        Assert.InRange(count, 52, 53);

        // Holidays
        IList<SubsetResult> actual = (
            from item in result
            where item.Event.Classification == AccessClassification.Public
            orderby item.When
            select item).ToList();
        var expected = (
            from item in calendar
            where item.Classification == AccessClassification.Public
            select item).ToList();
        foreach (var item in actual) {

            // MUST have summary.
            Assert.NotNull(item.Event.Summary);

            // MUST have the country code.
            Assert.NotNull(item.Event.Location);

            // MUST have attachment if Flagday
            if (item.Event.Categories.Contains(Category.FlagDay)) {
                Assert.NotEmpty(item.Event.Attachment);
            }

            // Dump for manual inspection
            output.WriteLine("{0:yyyy-MM-dd}: {1}", item.When, item.Event.Summary);
        }
    }

    protected async ValueTask<ICollection<Result>> GetSubsetTest(string rangeStart, int rangeDays) {
        string key = $"{rangeStart}+{rangeDays}";
        if (!_cache.TryGetValue(key, out var value)) {
            var collection = await calendar.SubsetAsync(DateTimeOffset.Parse(rangeStart), TimeSpan.FromDays(rangeDays));
            value = (
                from item in collection
                select new Result(
                    item.Event.Summary,
                    item.When.ToString("yyyy-MM-dd")
                )).ToList();
            _cache[key] = value;
        }

        return value;
    }
}
