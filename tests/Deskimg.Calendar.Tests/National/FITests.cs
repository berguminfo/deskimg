namespace Deskimg.Calendar.National.Tests;

public class FITests(
    ITestOutputHelper output) : TestRunner(output, new FI()) {

    [Fact]
    [Category("Coverage")]
    public Task CoverageTest() => GetCoverageTest("2021-01-01");

    [Theory]
    [InlineData("Uudenvuodenpäivä", "2021-01-01")]
    [InlineData("Loppiainen", "2021-01-06")]
    [InlineData("Pitkäperjantai", "2021-04-02")]
    [InlineData("Pääsiäispäivä", "2021-04-04")]
    [InlineData("2. pääsiäispäivä", "2021-04-05")]
    [InlineData("Helatorstai", "2021-05-13")]
    [InlineData("Helluntaipäivä", "2021-05-23")]
    [InlineData("Juhannuspäivä", "2021-06-26")]
    [InlineData("Pyhäinpäivä", "2021-11-06")]
    [InlineData("Itsenäisyyspäivä", "2021-12-06")]
    [InlineData("Joulupäivä", "2021-12-25")]
    [InlineData("Tapaninpäivä", "2021-12-26")]
    [UnitTest]
    public async Task TestSubset2021(string s, string expected) {
        var actual = await GetSubsetTest("2021-01-01", 365);
        try {
            Assert.Contains(actual, item => item.Summary == s);
        } catch (Exception) {
            output.WriteLine(s);
            throw;
        }
        try {
            Assert.Contains(actual, item => item.When == expected);
        } catch (Exception) {
            output.WriteLine(expected);
            throw;
        }
    }
}
