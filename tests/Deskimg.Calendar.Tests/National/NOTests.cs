using Microsoft.Extensions.Configuration;

namespace Deskimg.Calendar.National.Tests;

public class NOTests(
    ITestOutputHelper output,
    HostingFixture fixture) : TestRunner(output, new NO(fixture.CreateConfiguration())),
                              IClassFixture<HostingFixture> {

    [Fact]
    [Category("Coverage")]
    public Task CoverageTest() => GetCoverageTest("2025-01-01");

    [Theory]
    [InlineData("1. nyttårsdag", "2025-01-01")]
    [InlineData("Prinsesse Ingrid Alexandra", "2025-01-21")]
    [InlineData("Samefolkets dag", "2025-02-06")]
    [InlineData("Kong Harald V", "2025-02-21")]
    [InlineData("Palmesøndag", "2025-04-13")]
    [InlineData("Skjærtorsdag", "2025-04-17")]
    [InlineData("Langfredag", "2025-04-18")]
    [InlineData("1. påskedag", "2025-04-20")]
    [InlineData("2. påskedag", "2025-04-21")]
    [InlineData("Kristi himmelfartsdag", "2025-05-29")]
    [InlineData("1. pinsedag", "2025-06-08")]
    [InlineData("2. pinsedag", "2025-06-09")]
    [InlineData("Offentlig høytidsdag", "2025-05-01")]
    [InlineData("Frigjøringsdagen 1945", "2025-05-08")]
    [InlineData("Grunnlovsdagen", "2025-05-17")]
    [InlineData("Unionsoppløsningen 1905", "2025-06-07")]
    [InlineData("Dronning Sonja", "2025-07-04")]
    [InlineData("Kronprins Haakon Magnus", "2025-07-20")]
    [InlineData("Olsokdagen", "2025-07-29")]
    [InlineData("Kronprinsesse Mette-Marit", "2025-08-19")]
    [InlineData("Stortingsvalg", "2025-09-08")]
    [InlineData("1. juledag", "2025-12-25")]
    [InlineData("2. juledag", "2025-12-26")]
    [UnitTest]
    public async Task TestSubset2025(string s, string expected) {
        var actual = await GetSubsetTest("2025-01-01", 365);
        try {
            Assert.Contains(actual, item => item.Summary == s);
        } catch (Exception) {
            output.WriteLine(s);
            throw;
        }
        try {
            Assert.Contains(actual, item => item.When == expected);
        } catch (Exception) {
            output.WriteLine(expected);
            throw;
        }
    }

    [Theory]
    [InlineData(2008, null)]
    [InlineData(2009, "2009-09-14")]
    [InlineData(2010, null)]
    [InlineData(2011, null)]
    [InlineData(2012, null)]
    [InlineData(2013, "2013-09-09")]
    [InlineData(2017, "2017-09-11")]
    [InlineData(2021, "2021-09-13")]
    [Feature]
    public async void TestElectionDays(int y, string? expected) {
        var collection = await GetSubsetTest($"{y}-09-01", 31);
        var actual = collection.FirstOrDefault(item => item.Summary == "Stortingsvalg");
        Assert.Equal(expected, actual?.When);
    }

    [Fact]
    [Feature("Historic")]
    public async void TestExcludeHistoricEvents() {
        var collection = await GetSubsetTest("1980-01-01", 365);
        var actual = collection.FirstOrDefault(item => item.Summary == "Kong Olav V");
        Assert.Null(actual);
    }

    [Fact]
    [Feature("Historic")]
    public async void TestIncludeHistoricEvents() {
        Dictionary<string, string?> initialData = new() {
            ["Calendar:NO:Flags"] = "7"
        };
        var configuration = new ConfigurationBuilder()
            .AddInMemoryCollection(initialData)
            .Build();
        NO calendar = new(configuration);
        var collection = await calendar.SubsetAsync(DateTimeOffset.Parse("1980-01-01"), TimeSpan.FromDays(365));
        var actual = collection.FirstOrDefault(item => item.Event.Summary == "Kong Olav V");
        Assert.NotNull(actual);
    }
}
