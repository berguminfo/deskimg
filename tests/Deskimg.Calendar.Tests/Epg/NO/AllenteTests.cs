// Ignore spelling: Allente

using System.Diagnostics;
using System.Globalization;
using System.Net.Http.Json;
using System.Text.Json;

namespace Deskimg.Calendar.Epg.NO.Tests;

public class AllenteTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly Allente calendar = new(
        fixture.CreateLogger<Allente>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("epg.allente.no", "0206,0361,0298") // "tv6.no", "tv3plus.no", "tv3.no"
    };

    readonly DateTimeOffset rangeStart = DateTimeOffset.Parse("2022-09-23");

    readonly TimeSpan rangeLength = TimeSpan.FromDays(3);

    [Fact]
    [UnitTest]
    [Expensive]
    public async Task TestSubset() {
        output.WriteLine("⚠️ This test should duplicate each entry 3 times (same EPG for yesterday, today, and tomorrow).");

        var actual = await calendar.SubsetAsync(rangeStart, rangeLength);
        Assert.NotNull(actual);
        // NOTE: 0298 is the allente identity for tv3.no 
        Assert.Contains("tv3.no", actual.Select(x => x.Key.Id));
        var result = actual.First();
        Assert.NotNull(result);
        foreach (var item in result.SelectMany(x => x)) {
            output.WriteLine("{0}: {1}. Categories: '{2}'. Attachments: '{3}'.",
                item.When.ToString("HH:mm"),
                item.Event.Summary,
                string.Join("; ", item.Event.Categories),
                string.Join("; ", item.Event.Attachment));
            Assert.True(rangeStart <= item.When + item.Event.Duration);
        }
    }

    [Fact(Skip = "Is benchmark test")]
    [Category("Benchmark")]
    public async Task BenchmarkSubset() {
        var bm = Stopwatch.StartNew();
        await calendar.SubsetAsync(rangeStart, rangeLength);
        bm.Stop();
        Assert.InRange(bm.ElapsedMilliseconds, 0, 100);
    }

    [Fact]
    [UnitTest]
    public async Task TestChannelId() {
        var actual = await calendar.SubsetAsync(rangeStart, rangeLength);
        Assert.NotNull(actual);
        foreach (var item in actual) {
            output.WriteLine("ChannelId: {0}", item.Key.Id);
            Assert.DoesNotMatch(@"^\d+$", item.Key.Id);
        }
    }

    [Fact]
    [UnitTest]
    public async Task TestChannelImageUrl() {
        var actual = await calendar.SubsetAsync(rangeStart, rangeLength);
        Assert.NotNull(actual);
        foreach (var item in actual) {
            Assert.NotNull(item.Key.GetImageUri());
        }
    }

    [Fact]
    [UnitTest]
    public async Task TestLookupOrdering() {
        var actual = await calendar.SubsetAsync(rangeStart, rangeLength);
        Assert.NotNull(actual);
        // expecting Tag to be "0206", "0361", "0298"
        string[] expected = ["tv6.no", "tv3plus.no", "tv3.no"];
        int i = 0;
        foreach (var grouping in actual) {
            Assert.Equal(expected[i++], grouping.Key.Id);
        }
    }

    [Fact(Skip = "Mapping is incomplete, due to undecided identifiers.")]
    [UnitTest]
    public async Task TestChannelMapping() {
        JsonSerializerOptions options = new() {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        };
        Uri? requestUri = fixture.Endpoint.Fetch(calendar.Tag.Key, new {
            Tag = calendar.Tag.Value,
            Date = rangeStart.ToLocalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)
        }, discardQuery: true);
        var response = await fixture.Http.GetFromJsonAsync<Schema.GetAllenteEpgResponse?>(
            requestUri, calendar.CacheControl, options);
        Assert.NotNull(response);
        int actual = 0;
        foreach (var item in response.Channels) {
            if (int.TryParse(item.Id, out var _)) {
                output.WriteLine("Missing mapping: {0}", item.Id);
            } else {
                actual++;
            }
        }
        Assert.Equal(response.Channels.Count, actual);
    }
}
