using System.Diagnostics;

namespace Deskimg.Calendar.Epg.NO.Tests;

public class NRKTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly NRK calendar = new(
        fixture.CreateLogger<NRK>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new("epg.nrk.no", "nrk3,nrk1,nrk2")
    };

    readonly DateTimeOffset RangeStart = DateTimeOffset.Parse("2022-09-23");

    readonly TimeSpan RangeLength = TimeSpan.FromDays(1);

    [Fact]
    [UnitTest]
    [Expensive]
    public async Task TestSubset() {
        output.WriteLine("⚠️ This test should duplicate each entry 3 times (same EPG for yesterday, today, and tomorrow).");

        var actual = await calendar.SubsetAsync(RangeStart, RangeLength);
        Assert.NotNull(actual);
        Assert.Contains("nrk1.nrk.no", actual.Select(x => x.Key.Id));
        var result = actual.First();
        Assert.NotNull(result);
        foreach (var item in result.SelectMany(x => x)) {
            output.WriteLine("{0}: {1}. Categories: '{2}'. Attachments: '{3}'.",
                item.When.ToString("HH:mm"),
                item.Event.Summary,
                string.Join("; ", item.Event.Categories),
                string.Join("; ", item.Event.Attachment));
            Assert.True(RangeStart <= item.When + item.Event.Duration);
        }
    }

    [Fact]
    [Category("Benchmark")]
    public async Task BenchmarkSubset() {
        var bm = Stopwatch.StartNew();
        await calendar.SubsetAsync(RangeStart, RangeLength);
        bm.Stop();
        Assert.InRange(bm.ElapsedMilliseconds, 0, 100);
    }

    [Fact]
    [UnitTest]
    public async Task TestChannelId() {
        var actual = await calendar.SubsetAsync(RangeStart, RangeLength);
        Assert.NotNull(actual);
        foreach (var item in actual) {
            Assert.Matches(@"[a-z\d]\w+\.nrk\.no$", item.Key.Id);
        }
    }

    [Fact]
    [UnitTest]
    public async Task TestChannelImageUrl() {
        var actual = await calendar.SubsetAsync(RangeStart, RangeLength);
        Assert.NotNull(actual);
        foreach (var item in actual) {
            Assert.NotNull(item.Key.GetImageUri(maxWidth: 300));
            Assert.Null(item.Key.GetImageUri(maxWidth: 299));
        }
    }

    [Fact]
    [UnitTest]
    public async Task TestLookupOrdering() {
        var actual = await calendar.SubsetAsync(RangeStart, RangeLength);
        Assert.NotNull(actual);
        string[] expected = ["nrk3.nrk.no", "nrk1.nrk.no", "nrk2.nrk.no"];
        int i = 0;
        foreach (var grouping in actual) {
            Assert.Equal(expected[i++], grouping.Key.Id);
        }
    }
}
