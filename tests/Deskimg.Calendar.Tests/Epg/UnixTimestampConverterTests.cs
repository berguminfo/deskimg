namespace Deskimg.Calendar.Epg.Tests;

#if DEBUG

public class UnixTimestampConverterTests {

    [Theory]
    [InlineData("/Date(1665462600000+0200)/", "2022-10-11T06:30:00")]
    [InlineData("/Date(1665462600000-0200)/", "2022-10-11T06:30:00")]
    [UnitTest]
    public void TestConvertFrom(string value, string expected) {
        DateTimeOffset? actual = UnixTimestampConverter.Convert(value);
        Assert.NotNull(actual);
        Assert.Equal(expected, actual?.ToLocalTime().ToString("s"));
    }

    [Theory]
    [InlineData("2022-10-11T06:30:00+02:00", "/Date(1665462600000+0200)/")]
    [InlineData("2022-10-11T02:30:00-02:00", "/Date(1665462600000-0200)/")]
    [UnitTest]
    public void TestConvertTo(string value, string expected) {
        string actual = UnixTimestampConverter.Convert(DateTimeOffset.Parse(value));
        Assert.Equal(expected, actual);
    }
}

#endif
