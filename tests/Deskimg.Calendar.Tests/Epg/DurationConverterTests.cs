namespace Deskimg.Calendar.Epg.Tests;

#if DEBUG

public class DurationConverterTests {

    [Theory]
    [InlineData("PT2H30M0.08S", "02:30:00.0800000")]
    [InlineData("PT3H", "03:00:00")]
    [UnitTest]
    public void TestConvertTo(string value, string expected) {
        TimeSpan actual = DurationConverter.Convert(value);
        Assert.Equal(expected, actual.ToString());
    }

    [Theory]
    [InlineData("02:30:00.0800000", "PT2H30M0.08S")]
    [InlineData("03:00:00", "PT3H")]
    [UnitTest]
    public void TestConvertFrom(string value, string expected) {
        string actual = DurationConverter.Convert(TimeSpan.Parse(value));
        Assert.Equal(expected, actual.ToString());
    }
}

#endif
