namespace Deskimg.Calendar.Specialized.Tests;

public class TideLowHighTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly TideLowHigh calendar = new(
        fixture.CreateLogger<TideLowHigh>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
    };

    // unused (construction of the requestUri)
    readonly Geoposition observer = new() {
        Coordinate = new()
    };

    [Fact]
    [UnitTest]
    public async Task TestSubset() {
        var actual = await calendar.SubsetAsync(DateTimeOffset.Now.Date, TimeSpan.FromDays(1), observer);
        // interval is outside any matching tide record
        Assert.Equal(0, actual?.Count());
    }
}
