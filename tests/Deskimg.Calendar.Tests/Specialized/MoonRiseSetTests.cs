using Deskimg.Ephemeris;

namespace Deskimg.Calendar.Specialized.Tests;

public class MoonRiseSetTests {

    class MoonRiseSetMock(string riseAt, string setsAt) : JSRuntimeMock {

        public override RiseSet GetLunarRiseSet() => new() {
            Rise = DateTimeOffset.Parse(riseAt),
            Set = DateTimeOffset.Parse(setsAt),
        };
    }

    // unused
    readonly Geoposition observer = new();

    [Fact]
    [UnitTest]
    public async Task TestSubset() {
        MoonRiseSet calendar = new(new MoonRiseSetMock("2022-11-09T03:00:00", "2022-11-09T02:00:00"));
        string[] collection = (await calendar.SubsetAsync(DateTimeOffset.Parse("2022-11-09"), TimeSpan.Zero, observer))
            .Select(x => x.When.ToLocalTime().ToString("s") ?? string.Empty).ToArray();
        Assert.Equal(2, collection.Length);
        // Moon rises.
        Assert.Contains("2022-11-09T03:00:00", collection);
        // Moon sets.
        Assert.Contains("2022-11-09T02:00:00", collection);
    }
}
