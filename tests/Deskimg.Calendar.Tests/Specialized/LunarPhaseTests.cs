namespace Deskimg.Calendar.Specialized.Tests;

public class LunarPhaseTests  {

    class LunarPhaseMock : JSRuntimeMock {
    }

    [Fact(Skip = "JSRuntimeMock don't work here")]
    [UnitTest]
    public async Task TestSubset() {
        LunarPhase calendar = new(new LunarPhaseMock());
        string[] collection = (await calendar.SubsetAsync(DateTimeOffset.Parse("2022-09-07"), TimeSpan.FromDays(30)))
            .Select(x => x.When.ToLocalTime().ToString("yyyy-MM-dd") ?? string.Empty).ToArray();
        // Ref: https://www.timeanddate.no/astronomi/maanefaser/
        // NOTE: 2022-09-10 is full moon, ensure the case has coverage
        // TODO: Try use the real libnova library instead of JSRuntimeMock.
        Assert.True(collection.Length >= 4);
        // FullMoon
        Assert.Contains("2022-09-10", collection);
        // FirstQuarter
        Assert.Contains("2022-09-17", collection);
        // NewMoon
        Assert.Contains("2022-09-25", collection);
        // LastQuarter
        Assert.Contains("2022-10-03", collection);
    }
}
