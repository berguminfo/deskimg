namespace Deskimg.Calendar.Specialized.Tests;

public class DstTests {

    readonly Dst calendar = new();

    [Fact]
    [UnitTest]
    public async Task TestSubset2022() {
        // NOTE: this test works only for the CET/CEST timezone.
        // TODO: ensure the local timezone is CET or CEST
        string[] collection = (await calendar.SubsetAsync(DateTimeOffset.Parse("2022-01-01"), TimeSpan.FromDays(365)))
            .Select(x => x.When.ToLocalTime().ToString("s") ?? string.Empty).ToArray();
        Assert.Equal(2, collection.Length);
        // CET ends. CEST starts.
        Assert.Contains("2022-03-27T03:00:00", collection);
        // CEST ends. CET starts.
        Assert.Contains("2022-10-30T02:00:00", collection);
    }
}
