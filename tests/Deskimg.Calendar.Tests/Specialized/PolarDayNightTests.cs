using Deskimg.Ephemeris;

namespace Deskimg.Calendar.Specialized.Tests;

public class PolarDayNightTests {

    class SunRiseSetMock(string riseAt, string setsAt) : JSRuntimeMock {

        public override RiseSet GetSolarRiseSet() => new() {
            Rise = DateTimeOffset.Parse(riseAt),
            Set = DateTimeOffset.Parse(setsAt),
        };
    }

    // unused
    readonly Geoposition observer = new();

    [Fact(Skip = "JSRuntimeMock don't work here")]
    [UnitTest]
    public async Task TestSubset() {
        PolarDayNight calendar = new(new SunRiseSetMock("2022-03-27T00:00:00", "2022-03-27T00:00:00"));
        var result = await calendar.SubsetAsync(DateTimeOffset.Parse("2022-01-01"), TimeSpan.FromDays(365), observer);
        var collection = result.Take(4).Select(x => x.When.ToLocalTime().ToString("s") ?? string.Empty).ToArray();

        Assert.Equal(4, collection.Length);
        // sun below horizon all day
        Assert.Contains("2022-03-27T03:00:00", collection);
        // sun rise and sets
        Assert.Contains("2022-10-30T02:00:00", collection);
        // sun above horizon all day
        Assert.Contains("2022-10-30T02:00:00", collection);
        // sun rise and sets
        Assert.Contains("2022-10-30T02:00:00", collection);
    }
}
