namespace Deskimg.Calendar.Specialized.Tests;

public class CalDAVTests(
    ITestOutputHelper output,
    HostingFixture fixture) : IClassFixture<HostingFixture> {

    readonly CalDAV calendar = new(
        fixture.CreateLogger<CalDAV>(output),
        fixture.Endpoint,
        fixture.Http,
        fixture.MemoryCache) {
        CacheControl = new(),
        Tag = new(string.Empty, string.Empty) // workaround: attributes required
    };

    readonly EndpointOptions endpoint = fixture.Endpoint;

    [Fact]
    [UnitTest]
    public async Task TestPropFind_CalDAV() {
        calendar.Tag = new(nameof(CalDAV), $"{nameof(TestPropFind_CalDAV)}/");
        var actual = await calendar.SubsetAsync(DateTimeOffset.Parse("2024-08-20"), TimeSpan.FromDays(30));
    }
}
