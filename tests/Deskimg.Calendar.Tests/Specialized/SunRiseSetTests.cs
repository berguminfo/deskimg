namespace Deskimg.Calendar.Specialized.Tests;

using Deskimg.Ephemeris;

public class SunRiseSetTests {

    class SunRiseSetMock(string riseAt, string setsAt) : JSRuntimeMock {

        public override RiseSet GetSolarRiseSet() => new() {
            Rise = DateTimeOffset.Parse(riseAt),
            Set = DateTimeOffset.Parse(setsAt),
        };
    }

    // unused
    readonly Geoposition observer = new();

    [Theory]
    [InlineData("2022-11-09", "2022-11-09T03:00:00", "2022-11-09T02:00:00")]
    [UnitTest]
    public async Task TestSubset(string when, string riseAt, string setsAt) {
        SunRiseSet calendar = new(new SunRiseSetMock(riseAt, setsAt));
        string[] collection = (await calendar.SubsetAsync(DateTimeOffset.Parse(when), TimeSpan.FromDays(1), observer))
            .Select(x => x.When.ToLocalTime().ToString("s") ?? string.Empty).ToArray();
        Assert.Equal(2, collection.Length);
        Assert.Contains(riseAt, collection);
        Assert.Contains(setsAt, collection);
    }
}
