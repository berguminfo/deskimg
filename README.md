# Deskimg <img src="docs/assets/logo.png" style="height:2em;float:right;margin-top:-1rem">

Application to visualize data collections as the desktop wallpaper.

## Prerequisites

- Install [.NET SDK](https://dotnet.microsoft.com/en-us/download) version 9.0.
- Install [Node.js](https://nodejs.org/en/download) version 18.0.0 or later.

---

👉 The visualization component uses **WebAssembly** for the rendering.

Ensure the **wasm-tools** workload has been installed.

```sh
dotnet workload restore
```

---

👉 The `./docs` directory contains articles about this project.

To build **HTML** from the **Markdown** source, install the **docfx** tool.

```sh
dotnet tool install --global docfx
```

---

👉 In order to minify JavaScripts, install the **esbuild** tool.

```sh
npm install --global esbuild
```

---

👉 In order to browse without warning, install the development certificate.

```sh
dotnet dev-certs https --trust
```

---

⚠️ Before executing `dotnet restore`, `dotnet build` commands.

The sample `nuget.config` file has added two additional package sources.

- `artifacts/packages`
  - Repository containing the source generator packages required to build the `blazor` projects.
- `.cache/nuget`
  - Repository containing the *globalPackagesFolder* for offline development.

Ensure to create the directories before restoring nuget packages
or rename/edit the `nuget.config` file.

## Getting started

1. Pack the source generators and build all components:

    pwsh:

    ```ps1
    New-Item -Force .cache\nuget,artifacts\packages | Out-Null
    .\eng\pack.ps1 && .\eng\restore.ps1 && .\eng\build.ps1
    ```

    bash:

    ```sh
    mkdir -p .cache/nuget artifacts/packages
    eng/pack.sh && eng/restore.sh && eng/build.sh
    ```

2. Run the application:

    For WPF:

    ```sh
    cd src/apps/Deskimg.App.Wpf
    dotnet run
    ```

## Features

See [Documentation](docs/index.md) for more information.

## The HTTP Query String

In addition to the `appsettings` JSON configuration files.
It's possible to override some options on the HTTP Query String.

- `🌐=⟦xs:string⟧`
  - Specifies the layout set to use, use multiple queries to override any previous set.
  - Example 1: `?🌐=highcontrast&🌐=corp&🌐=sets/@.json`.
- `t=⟦xs:dateTime ∨ xs:date ∨ xs:duration⟧`
  - Specifies the time of interest, use multiple queries to advance the current time of interest.
  - Example 1: `?t=2024-01-1T12:00:00` gives `2024-01-01 12:00:00`.
  - Example 2: `?t=2024-01-01&t=PT12H` gives `2024-01-01 12:00:00`.
  - Example 3: `?t=2024-01-01&t=-PT12H` gives `2023-12-31 12:00:00`.
- `lang=⟦xs:language⟧`
  - Specifies the preferred language in [BCP 47 Format](https://www.rfc-editor.org/info/bcp47).
  - Example 1: `?lang=en-US`.
- `geo=⟦xs:string⟧`
  -  Specifies the preferred location or request the current position if empty.
  - Example 1: `?geo=-12.3,-45.6` (GeoURI).
  - Example 2: `?geo=7JVW52GR+2V` (PlusCode).
  - Example 3: `?geo=NZSP` (Layout set name).
- `nav=` or `nav=✓`
  - Turns the navigation pane visibility on or off.
- `upd=⟦xs:time⟧`
  - Specifies the update interval or keep page static if not specified.
  - Example 1: `?upd=00:05:00`.
- `bg=⟦xs:anyURI⟧`
  - Specifies the background wallpaper URI.
  - Example 1: `?bg=https://localhost/wallpaper.webp`.
- `bgtype=⟦xs:string⟧`
  - Specifies the background wallpaper mime type.
  - Example 1: `?bgtype=image/webp`.
  - Example 2: `?bgtype=video/mp4`.
- `bgmode=⟦Dom ∨ Blob ∨ Css⟧`
  - Specifies the the background rendering mode.
  - `Dom`
    - Render using `ImageElement` for both background and overlay pictures.
  - `Blob`
    - Render using one `ImageElement` combining the background and overlay images.
  - `Css`
    - Render using one combined `blob:` image URI using a `background-image` CSS attribute.
- `theme=⟦Auto ∨ Dark ∨ Light⟧`
  - Specifies the value to set for the `data-bs-theme` attribute.
  - `Auto`
    - Chooses the `Dark` or `Light` theme based on the composed background wallpaper. Due to the nature of the calculation the background rendering is more slightly more efficient in `Css` or `Blob` mode.
  - Example 1: `?theme=Auto`.
- `nf=` or `nf=✓`
  - Turn on or off the alert notification.
- `cw=⟦xs:positiveInteger⟧` and `ch=⟦xs:positiveInteger⟧`
  - Specifies the viewport clipping rules.
  - Example 1: `?cw=4096&ch=4096`.
- `q=⟦xs:string⟧`
  - Specifies the search query.
  - Example 1: `?q=chess`.

## Offline development

By default the `.cache` directory contains cached `nuget` and `npm` packages for offline development.

To manually populate the directories, execute the following commands:

```sh
# cache nuget packages
nuget config -Set globalPackagesFolder=.cache/nuget
nuget restore

# cache npm package
cd src/blazor/libnova
npm install --cache ../../../.cache/npm-cache
cd src/blazor/storybook
npm install --cache ../../../.cache/npm-cache
cd src/webapi/spec
npm install --cache ../../../.cache/npm-cache
```

## Default Port Assignment

|             | Development | Staging     | Production  |
|-------------|-------------|-------------|-------------|
| Blazor      | 5000        | 15610       | 16610       |
| Fetch       | 5001        | 15611       | 16611       |
|             | 5002        | 15612       | 16612       |
| CalDAV      | 5003        | 15613       | 16613       |
| Settings    | 5004        | 15614       | 16614       |
| Sets        | 5005        | 15615       | 16615       |

The following list of files configures the port assignments:

- Capture
  - `src/apps/Deskimg.Capture/appsettings.Staging.json`
  - `src/apps/Deskimg.Capture/appsettings.Production.json`
- Settings
  - `src/blazor/Deskimg.Blazor.Settings/wwwroot/appsettings.Staging.json`
  - `src/blazor/Deskimg.Blazor.Settings/wwwroot/appsettings.Production.json`
- WebAssembly
  - `src/blazor/Deskimg.Blazor.WebAssembly/wwwroot/appsettings.Staging.json`
  - `src/blazor/Deskimg.Blazor.WebAssembly/wwwroot/appsettings.Production.json`

💡 The `apps` applications uses the `Deskimg.App.Hosting` component to host the
web dependencies and assigns any random ports in the 15000-15099 range.

## Setting the desktop wallpaper

1. Install the following web applications to a web server:

    - Required: `Deskimg.WebAPI.Fetch`
      - Installed using the [ASP.NET Core](https://learn.microsoft.com/en-us/dotnet/core/install/) manual.
    - Required: `Deskimg.Blazor.WebAssembly`
      - Installed as static files.
    - Optional: `Deskimg.Blazor.CalDAV`
      - Recommended and used to store the customized layout sets.
      - Installed using the [ASP.NET Core](https://learn.microsoft.com/en-us/dotnet/core/install/) manual.
    - Optional: `Deskimg.Blazor.Settings`
      - A simpel editor to edit layout sets and other settings.
      - Installed as static files.
    - Optional: `Deskimg.WebAPI.Sets`
      - Provides a repository for the default layout sets.
      - Installed using the [ASP.NET Core](https://learn.microsoft.com/en-us/dotnet/core/install/) manual.

2. Install the `Scheduled Tasks` or `crontab`.

    To generate a sample, execute the following commands:

    Scheduled Tasks

    ```ps1
    .\Deskimg.Capture\⟦framework⟧\Deskimg.Capture.exe --export capture.xml
    .\Deskimg.Desktop\⟦framework⟧\Deskimg.Desktop.exe --export desktop.xml
    ```

    crontab

    ```sh
    dotnet Deskimg.Capture/⟦framework⟧/Deskimg.Capture.dll --export capture
    dotnet Deskimg.Desktop/⟦framework⟧/Deskimg.Desktop.dll --export desktop
    ```

💡 Hint: Install the `Capture` stage about a minute before the `Desktop` stage in order to set the most recent captured wallpaper.

## Sample watch script

Sample script to open a new window having 1 tab and 3 split panes.
Each starting `dotnet watch run`.

```ps1
$env:DOTNET_USE_POLLING_FILE_WATCHER=true # https://github.com/dotnet/aspnetcore/issues/45519
$SubCommand = @(
    "nt --tabColor '#dc143c' -d `"src\blazor\Deskimg.Blazor.WebAssembly`" pwsh -noe -c dotnet watch run",
    "sp -V --tabColor '#4682b4' -d `"src\webapi\Deskimg.WebAPI.Sets`" pwsh -noe -c dotnet watch run",
    "sp -H --tabColor '#3cb371' -d `"src\webapi\Deskimg.WebAPI.Fetch`" pwsh -noe -c dotnet watch run -f net9.0-windows10.0.17763.0"
)
# The ';' character must be escaped with '`;', otherwise pwsh slurps the next wt subcommand as the next ps1 statement.
"wt " + ($SubCommand -join " ``; ") | Invoke-Expression
```

## The file structure

- `/docs`
  - Contains the documentation site.
  - Execute `docfx build` to transform the documentation markdown files to the `_site` directory.
- `/eng`
  - Contains engineering scripts and files.
- `/public`
  - Contains public published files like schemas.
- `/src`
  - Contains domain specific projects, for example:
  - `/src/analyzers`
    - Contains code generator projects.
  - `/src/apps`
    - Contains application projects.
  - `/src/blazor`
    - Contains frontend related projects.
  - `/src/webapi`
     - Contains backend related projects.
- `/tests`
  - Contains namespace specific test projects.

## Deployment

### IIS

- Install [URL Rewrite Module](https://www.iis.net/downloads/microsoft/url-rewrite).
- Install [AspNetCoreModuleV2](https://github.com/aspnet/IISIntegration).

In order to give access to IIS sites the **IUSR** and **IIS_IUSRS** should be given **Read and Execute** access.

## Some common problems and resolution

❓ After installation on IIS the site output is:

```
HTTP Error 500.19 - Internal Server Error
The requested page cannot be accessed because the related configuration data for the page is invalid.

Error Code	   0x8007000d
Config Error

More Information:
This error occurs when there is a problem reading the configuration file for the Web server or Web application.
```

💡 This problem is resolved by installing the _Deployment with IIS_ required components.
