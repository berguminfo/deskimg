# Fetch WebAPI

A Fetch request takes a cache line parameter and returns the remote resource content.

Key features include:

* Should cache remote resources, both in memory and permanent storage.
* Should delay commits.
* Should respect metered connections.
* Should use If-Modified-* headers.

## Fetch requests

A Fetch request is an HTTP request in the following formats:

> `HEAD` /api/_version_/fetch/_path-parameters_
>
> `GET` /api/_version_/fetch/_path-parameters_
>
> `PROPFIND` /api/_version_/fetch/_path-parameters_
>
> `GET` /api/1.1/fetch/

where `version` may be one of the following values:

- `1.0` to use the local file system for storage
- `1.1` to get the cache storage state

## Required path parameters

Except for the `PROPFIND` request all request is required to provide the following path parameters.

- `base64url` is the URI to fetch and cache on behalf of the client.

  👉 For more information about the Base64Url encoding scheme, see the documentation about the DeflateBase64Url class.

## Fetch examples

The following example returns the content at `http://localhost:8080/`.

```sh
curl -L -X GET '/api/1.0/fetch/aHR0cDovL2xvY2FsaG9zdDo4MDgwLw'
```

The following example list the resources available at `http://localhost:8080/`.

```sh
curl -L -X PROPFIND '/api/1.0/fetch/aHR0cDovL2xvY2FsaG9zdDo4MDgwLw'
```

## Fetch responses

A Fetch response contains the content of the remote resource. In case the remote resource can't be found or an error occurs the `FetchStatus` code indicating the error condition.

## FetchStatus

Status codes returned by the service.

- `200 OK` indicating the Fetch succeeded and the response body contains exactly the same as the remove endpoint sent.

- `403 Forbidden` indicating that the remote endpoint has been forbidden by configuration.

- `404 NotFound` indicating the remote endpoint can't find the resource.

- `408 RequestTimeout` indicating the Fetch connection remote endpoint timed out.

- `509 EnforceMeteredConnection` indicating that Fetch has been rejected, the internet is connected to metered connection.

  👉 To resolve this response either set the internet connection to non metered, or disable the enforcement by setting the `EnforceMeteredConnection` parameter to `false`.

- `500 InternalServerError` indicating the Fetch failed. For more information analyze the server log.
