# Fetch Service

## Description

The WebAPI component responsible to cache network resources.
May be deployed as a kestrel service, or any other ASP.NET deployment.

Special features include:

* Metered connection detection.
* Minimum and maximum expiration.
* System services.

## Build

```sh
dotnet watch run
dotnet publish -c Release -f net8.0-windows10.0.17763.0
dotnet publish -c Release -f net8.0
```

### Fetch Parameters

- `Fetch:Timeout`
  - The fetch timeout.
  - *Data Type: string{TimeSpan}.*
  - *Default Value: `00:00:30`.*
- `Fetch:EnforceMeteredConnection`
  - Denies fetch to any resource if the current connection is metered.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Fetch:EnforceRefresh`
  - Enforces fetch refresh.
  - *Data Type: boolean.*
  - *Default Value: `false`.*
- `Fetch:Allow`
  - If present this parameter specifies the list of endpoints allowed to fetch.
  - __NOTE:__
      For listening ports facing the public internet this parameter should
      be populated given the endpoints and resources used by `Deskimg.Blazor`.
      Otherwise the fetch service may be used by anyone for anything.
  - *Data Type: string[].*
  - *Default Value: `undefined`.*
- `Cache:StorePath`
  - The *cache store* path. Should end with `.json`.
  - *Data Type: string{MessageFormat}.*
- `Cache:OutputFormat`
  - The *cache item* output path. Should end with `{Name}`.
  - *Data Type: string{MessageFormat}.*
- `Cache:MinAge`
  - The resource minimum cache age.
  - *Data Type: string{TimeSpan}.*
  - *Default Value: `00:15`.*
- `Cache:MaxAge`
  - The resource maximum cache age.
  - *Data Type: string{TimeSpan}.*
  - *Default Value: `01:00`.*
- `Cache:CommitDelay`
  - The delay for cache commit.
  - *Data Type: string{TimeSpan}.*
  - *Default Value: `00:00:30`.*

### MessageFormat replacements

- `{Roaming}`
  - Replaced with `$env:APPDATA\Deskimg`.
- `{Local}`
  - Replaced with `$env:LOCALAPPDATA\Deskimg`.
- `{Name}`
  - Replaced with the resource hash data.
