# About Desktop App

## Description

The set desktop wallpaper component, using `Deskimg.Capture` to capture the wallpaper image.

> [!TIP]
> This component may set any desktop wallpaper image.

## Build

```sh
dotnet watch run
dotnet publish -c Release -f net8.0-windows10.0.17763.0
dotnet publish -c Release -f net8.0
```

## Install

1. Execute:

   ```sh
   Deskimg.Desktop.exe --export "Deskimg Desktop.xml"
   ```

2. Start `Task Scheduler` and import the exported task `Deskimg Desktop.xml`.

## Parameters

- `OutputFormat`
  - Specifies the output format.
  - *Data Type: string{MessageFormat}.*
- `MaxArchive`
  - Specifies the maximum number of images to archive.
  - *Data Type: number.*
- `Fetch:0:Get`
  - Specifies the source path to the wallpaper image.
  - *Data Type: string.*
- `Fetch:0:Put`
  - Specifies the desktop to set. See list of configured settings below.
  - *Data Type: string.*
- `Desktops:Exec:Command`
  - Specifies the command line to execute in order to set desktop.
  - *Data Type: string.*

### MessageFormat replacements

- `{Roaming}`
  - Replaced with `$env:APPDATA\Deskimg`.
- `{Local}`
  - Replaced with `$env:LOCALAPPDATA\Deskimg`.
- `{TimeStamp}`
  - Replaced with the current timestamp.
- `{FileName}`
  - Replaced with the reference in `Fetch:0:Put`.

### Desktops for the `Put` statement

- `Exec`
  - Sets the wallpaper using an executable.
- `UwpLockScreen`
  - Sets the wallpaper to win10 lockscreen.
- `UwpWallpaper0`
  - Sets the wallpaper to win10 (all monitors).
- `Shobj0`
  - Sets the wallpaper to win8+ desktop.
- `User32`
  - Sets the wallpaper to any windows desktop.
