# About Capture App

## Description

The web capture component, using `Deskimg.Blazor` visualization to produce an image.

> [!TIP]
> This component may capture any web content.

## Build

```sh
dotnet watch run
dotnet publish -c Release
```

## Install

1. Execute:

   ```sh
   Deskimg.Capture.exe --export "Deskimg Capture.xml"
   ```

2. Start `Task Scheduler` and import the exported task `Deskimg Capture.xml`.

## Parameters

- `OutputFormat`
  - Specifies the output format.
  - *Data Type: string{MessageFormat}.*
- `Capture:0:Get`
  - Specifies the URI to capture.
  - *Data Type: string{Uri}.*
- `Capture:0:Put`
  - Specifies the captured destination filename and image format.
  - *Data Type: string.*

### MessageFormat replacements

- `{Roaming}`
  - Replaced with `$env:APPDATA\Deskimg`.
- `{Local}`
  - Replaced with `$env:LOCALAPPDATA\Deskimg`.
- `{FileName}`
  - Replaced with the reference in `Capture:*:Put`.
