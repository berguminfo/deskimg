# Overview

## Description

The documentation site describes the application API and basic settings to get started.

## Parameters Schema

No formal schema exists, try to be readable.

### Data Type

Expect to see regular ECMAScript types:
- `boolean`, `number`, `object`, `string`

For vector like types, append `[]` to the type:
- `boolean[]`, `number[]`, `object[]`, `string[]`

Sometimes its even better to document with a CLR type from the `System` namespace:
- `string{TimeSpan}`, `string{DateTime}`, `string{DateOnly}`, `string{Uri}`
- `number{Int32}`, `number{Double}`

To indicate nullable type append the `?` character:
- `boolean?`, `number?`, `object?`, `string?`
