# About Deskimg App

## Description

Simple application to view and host the desktop image production.

## Build

```sh
dotnet watch run
dotnet publish -c Release
```

## Parameters

- `PortRange:MinInclusive`
  - Specifies the minimum random port assignment.
  - *Data Type: number.*
- `PortRange:MaxInclusive`
  - Specifies the maximum random port assignment.
  - *Data Type: number.*
- `Language`
  - Specifies the browser language.
  - *Data Type: string.*
- `Permissions:UnknownPermission`
  - Specifies a permission state.
  - *Data Type: Allow|Deny|Default.*
- `Permissions:Geolocation`
  - Specifies a permission state.
  - *Data Type: Allow|Deny|Default.*
- `Permissions:Notifications`
  - Specifies a permission state.
  - *Data Type: Allow|Deny|Default.*
- `Urls:0`
  - Specifies the home page URL.
  - *Data Type: string{MessageFormat}.*
- `Urls:1`
  - Specifies the settings page URL.
  - *Data Type: string{MessageFormat}.*
- `Urls:{number}`
  - Optional additional URLs.
  - *Data Type: string{MessageFormat}.*
- `Services:0:StartType`
  - Specifies the hosting service startup type.
  - *Data Type: Automatic|Manual|Disabled.*
- `Services:0:Environment:ASPNETCORE_URLS`
  - Specifies the hosting service URL.
  - *Data Type: string{MessageFormat}.*
- `Services:0:Environment:DOTNET_ENVIRONMENT`
  - Specifies the hosting environment.
  - *Data Type: Development|Staging|Production.*
- `Services:0:Environment:Endpoint__api__fetch`
  - Specifies the assigned hosting endpoint.
  - *Data Type: string{MessageFormat}.*
- `Services:0:Environment:Endpoint__api__sets`
  - Specifies the assigned hosting endpoint.
  - *Data Type: string{MessageFormat}.*
- `Services:0:FileName`
  - Specifies the hosting program to execute.
  - *Data Type: string{MessageFormat}.*
- `Services:0:Arguments:0`
  - Optional hosting program arguments.
  - *Data Type: string{MessageFormat}.*

### MessageFormat replacements

- `{Port, number, fetch}`
  - Replaced with the assigned hosting port.
- `{Port, number, sets}`
  - Replaced with the assigned hosting port.
- `{Port, number, settings}`
  - Replaced with the assigned hosting port.
- `{Platform, select, IsWindows{net8.0-windows10.0.17763.0} other{net8.0}}`
  - Platform replacement token. May select on `IsWindows` or `other` keywords.
