# The Backtick Trick

## Background

One of PowerShell's design goals has always been to maintain compatibility with
existing shells while enhancing the capabilities of current scripts. As a result,
PowerShell syntax closely resembles `bash` syntax in many areas, though not entirely.

The “backtick trick” refers to the practice of mixing `pwsh` (PowerShell) and `bash`
syntax within the same script.

## The Backtick Trick

- In `bash`, the combination of <kbd>Backtick</kbd> + <kbd>#</kbd> + <kbd>\\</kbd> is used for folded command substitution. In `pwsh`, however, the backtick (`) serves as the escape character.
- The `<#` ... `#>` sequence is a multi-line comment in `pwsh`, hereas it's treated as a single-line comment in `bash`.

This allows for the following script:

```sh
`                     #\
first pwsh command    #\
last pwsh command    <#\
`
first bash command
                      #>
```

## Explanation

### In `bash`

The block enclosed by <kbd>Backtick</kbd> ... <kbd>Backtick</kbd> is interpreted by `bash` as:

- Execute the commands inside the block.
- Use the <kbd>\\</kbd> at the end of the line for folded substitution.

Thus, the block is reduced to (with whitespace removed):

```sh
#first pwsh command#last pwsh command<#
```

Which is essentially a single-line comment.

Executing an empty line sets the `$?` variable to 0, indicating success.

`bash` then executes the next command, which is `first bash command`, followed by the comment `#>`.

### In `pwsh`

The first line, starting with <kbd>Backtick</kbd> + <kbd>Space</kbd> ... `#\`, is interpreted by `pwsh` as:

- Escape the following character, which in this case is a <kbd>Space</kbd>.
- Treat the rest of the line as a comment.

This effectively results in an empty line.

`pwsh` then executes the next two commands: `first pwsh command` and `last pwsh command`.

The `<#` ... `#>` block is interpreted as a multi-line comment in `pwsh`, causing the `bash` commands to be ignored.

### Notes

- The `pwsh` block must not contain any additional backticks. Use the <kbd>Backtick</kbd> + <kbd>e</kbd> escape sequence as a workaround if necessary.
- The `bash` block will set `$? -eq 0` due to the prior execution of the <kbd>Backtick</kbd>... <kbd>Backtick</kbd> command.
