# CalDAV WebAPI

A CalDAV request takes a text input and returns a CalDAV response. The input must specifiy the CalDAV path and file name.

## CalDAV requests

A CalDAV request is an HTTP request in the following formats:

> `HEAD` /api/_version_/caldav/_path_/_fileName_
>
> `GET` /api/_version_/caldav/_path_/_fileName_
>
> `PROPFIND` /api/_version_/caldav/_path_
>
> `PUT` /api/2.0/caldav/_path_/_fileName_
>
> `DELETE` /api/2.0/caldav/_path_/_fileName_

where `version` may be one of the following values:

- `1.0` to use a `IsolatedStorage` for storage
- `2.0` to use the local file system for storage

# Required path parameters

Except for the `PROPFIND` request all request is required to provide the following path parameters.

- `path` specifies the CalDAV repository. This parameter can't contain any special file system characters.

- `fileName` specifies the CAlDAV file name to retrieve. This parameter can't contain any special file system characters.

The special file system charcters denied to be routed is the regular expression matching the pattern `[\\/:"\*\?<>\|]`.

## CalDAV examples

The following example returns the iCAL calender in the group of `Public` calendars having the file name `foo.ics`.

```sh
curl -L -X GET '/api/2.0/caldav/Public/foo.ics'
```

The following example lists the calendars available in the `Public group.

```sh
curl -L -X PROPFIND '/api/2.0/caldav/Public'
```

## CalDAV responses

A CalDAV response contains the content of requested calendar. In case the resource can't be found or an error occurs the `CalDAVStatus` code indicating the error condition.

## CalDAVStatus

Status codes returned by the service.

- `200 OK` indicating the API request was successfully.

- `403 Forbidden` indicating that your request was denied, generally because:

  - The hosting credidental don't have access to the file.

- `404 NotFound` indicating that the request references a resource that can't be found.

- `500 InternalServerError` indicat