# Meteorology Component

## Description

Component to view weather observations and forecasts.

> [!TIP]
> For more information:
> - [metar.noaa.gov](../../external/metar.noaa.gov.md)
> - [forecast.yr.no](../../external/forecast.yr.no.md)

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Source`
  - The list of location identifiers. See [Geolocation](geolocation.md).
  - *Data Type: string[].*
- `CacheControl`
  - The `Cache-Control` request header.
  - *Data Type: object.*
  - *Default Value: `{ MaxAge: '00:15' }`.*
- `Take`
  - The maximum number of items to display.
  - *Data Type: number.*
  - *Default Value: `4`.*
- `DisplayTitle`
  - The title visibility.
  - *Data Type: boolean.*
  - *Default Value: `false`.*
- `DisplayWindGust`
  - The wind gust visibility.
  - *Data Type: boolean.*
  - *Default Value: `false`.*

## Weather Condition

Current weather (N/A)
Intensity        Description      Precipitation       Obscuration      Other
-  Light         MI Shallow       DZ Drizzle          BR Mist          PO Well developed dust / sand whirls
   Moderate      PR Partial       RA Rain             FG Fog           SQ Squalls
+  Heavy         BC Patches       SN Snow             FU Smoke         FC Funnel clouds inc tornadoes or waterspouts
VC Vicinity      DR Low drifting  SG Snow grains      VA Volcanic ash  SS Sandstorm
BL Blowing       IC Ice crystals  DU Widespread dust  DS Duststorm
SH Showers       PL Ice pellets   SA Sand
TS Thunderstorm  GR Hail          HZ Haze
FZ Freezing      GS Small hail    PY Spray
UP Unknown
e.g. -SHRA - Light showers of rain
TSRA - Thunderstorms and rain.

## Cloud Cover

nnn - Sky condition.
    SKC     (0/8)
    CLR     (0/8)
    FEW     (1/8, 2/8)
    SCT     (3/8, 4/8)
    BKN     (5/8, 6/8, 7/8)
    OVC     (8/8)
    VV  (8/8) - vertical visibility.
hhh - cloud height in flight level (100 feet units)
Examples:
FEW018 Few clouds at 1800 ft,
SCT030 Scattered clouds at 3000 ft,
BKN120 Broken clouds at 12,000 ft.


## Example

```json
{
  "Meteorology": {
    "Source": [
      "NZSD"
    ]
  }
}
```