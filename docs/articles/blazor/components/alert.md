# Alert Component

## Description

Component to view weather alerts.

> [!TIP]
> For more information:
> - [wrn.yr.no](../../external/wrn.yr.no.md)
> - [avalanche.nve.no](../../external/avalanche.nve.no.md)
> - [flood.nve.no](../../external/flood.nve.no.md)
> - [landslide.nve.no](../../external/landslide.nve.no.md)
> - [traffic-info.vegvesen.no?roadNumber=E6&sort=trafficImpact](https://www.vegvesen.no/trafikk)

# Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Source`
  - The location identifier. See [Geolocation](geolocation.md).
  - *Data Type: string.*
- `CacheControl`
  - The `Cache-Control` request header.
  - *Data Type: object.*
  - *Default Value: `{ MaxAge: '00:15' }`.*
- `Take`
  - The maximum number of items to display.
  - *Data Type: number.*
  - *Default Value: `4`.*

## Example

```json
{
  "Alert": {
    "Source": "NZSP"
  }
}
```
