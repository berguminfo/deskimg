# Navigation Component

## Description

Component to view the navigation bar.

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `false`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*

## Example

```json
{
  "Navigation": {
    "Visible": true
  }
}
```