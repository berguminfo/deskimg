# Container Component

## Description

Component to contain additional components.

## Parameters

- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Index`
  - The container index.
  - *Data Type: object.*

## Example [1]

```json
{
  "Container": {
    "Index": [
      {
        "Class": "col-6",
        "Component": [
          {
            "Picture": {}
          }
        ]
      }
    ]
```
