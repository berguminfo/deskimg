# Calendar Component

## Description

Component to view a event calendar.

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Source`
  - The list of event calendars. See __Example [1]__ for the list of known event calendars.
  - *Data Type: string[].*
- `Take`
  - The maximum number of items to display.
  - *Data Type: number.*
  - *Default Value: `4`.*

## Example [1]

```json
{
  "Calendar": {
    "Source": [

      // Holidays for countries (ISO-3166)
      "AI", // GB
      "AX", // FI++
      "BM", // GB
      "BV", // NO
      "DK",
      "ENG", // GB++
      "FI",
      "FK", // GB
      "FO",
      "GB",
      "GG", // GB++
      "GI", // GB
      "GL", // DK++
      "IE",
      "IM", // GB++
      "IS",
      "JE", // GB++
      "KY", // GB
      "MS", // GB
      "NIR", // GB++
      "NO",
      "PN", // GB
      "SCT", // GB++
      "SE",
      "SH", // GB
      "SJ", // NO
      "TC", // GB
      "VG", // GB
      "WLS", // ENG

      // Starting and ending of daylight-saving time
      "Dst",

      // Moon phases: new, first-quarter, full, or last-quarter phases
      "LunarPhase",

      // Polar day/night events
      "PolarDayNight",
    ]
  }
}
```