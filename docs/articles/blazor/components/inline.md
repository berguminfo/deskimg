# Inline Component

## Description

Component to display Text or HTML content.

# Parameters

- `Visible`
  - Specifies the visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Value`
  - Specifies the inline value. See FormatTokens section below.
  - *Data Type: string.*

## FormatTokens

- `{{Version}}`
  - Renders the application version i.e. `7.3.0`.
- `{{YearMonth}}`
  - Renders the current year and month i.e. `helmikuu 2025`.
- `{{Location}}`
  - Renders the current geographic position i.e. `63°24′10″N 10°25′12″W`.

## Example

```json
{
  "Inline": {
    "Style": {
      "text-transform": "uppercase"
    },
    "Value": "<p>Current date is {{YearMonth}}</p>"
  }
}
```
