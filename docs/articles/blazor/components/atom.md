# Atom Component

## Description

Component to view an `Atom` feed.

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Source`
  - The address of the source feed.
  - *Data Type: string{Uri}.*
- `Class`
  - The `class` attribute to decorate with.
  - May be one of the following:
    - `bg-warning`
    - `bg-severity-yellow`
    - `bg-severity-orange`
    - `bg-severity-red`
    - `bg-severity-extreme`
  - *Data Type: string.*
- `CacheControl`
  - The `Cache-Control` request header.
  - *Data Type: object.*
  - *Default Value: `{ MaxAge: '00:15' }`.*
- `Take`
  - The maximum number of items to display.
  - *Data Type: number.*
  - *Default Value: `4`.*

## Example

```json
{
  "Atom": {
      "ClassName": "bg-warning",
      "Source": "https://example.domain/feed.rss"
  }
}
```