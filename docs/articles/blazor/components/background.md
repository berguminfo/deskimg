# Background Component

## Description

Component to view a background composition.

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Overlay`
  - Optional background overlay pattern.
    - *Data Type: object.*
  - `Pattern`
    - *Data Type: CheckerBoard.*
  - `Opacity`
    - The overlay brush to dim the background composition.
    - *Data Type: number[]*
    - *Interval: [0, 1].*
  - `Width`
    - *Data Type: number.*
    - *Default Value: 2.*
  - `Height`
    - *Data Type: number.*
    - *Default Value: 2.*
- `Composite`
  - The list elements giving the resulting background image.
  - *Data Type: object[].*
  - `From`
    - *Data Type: number.*
    - *Interval: [-1, 1].* __[2]__
  - `To`
    - *Data Type: number.*
    - *Interval: [-1, 1].* __[2]__
  - `Images`
    - The composite images
    - *Data Type: object.*

#### [1]: Hatch pattern figure
<figure>
  <style>
    .hatch {
      font-size: 14px;
      font-weight: bold;
      table-layout: fixed;
      width: 100px;
    }
    .hatch1, .hatch2 {
      padding: 0;
      text-align: center;
      vertical-align: top;
    }
    .hatch1 {
      color:#000;
      background: rgba(255,255,255,138);
    }
    .hatch2 {
      color: #fff;
      background: rgba(0,0,0,92);
    }
  </style>
  <table class="hatch">
    <tr>
      <td class="hatch1">1</td>
      <td class="hatch2">2</td>
      <td class="hatch1">1</td>
      <td class="hatch2">2</td>
    </tr>
    <tr>
      <td class="hatch2">2</td>
      <td class="hatch1">1</td>
      <td class="hatch2">2</td>
      <td class="hatch1">1</td>
    </tr>
    <tr>
      <td class="hatch1">1</td>
      <td class="hatch2">2</td>
      <td class="hatch1">1</td>
      <td class="hatch2">2</td>
    </tr>
    <tr>
      <td class="hatch2">2</td>
      <td class="hatch1">1</td>
      <td class="hatch2">2</td>
      <td class="hatch1">1</td>
    </tr>
  </table>
</figure>
</div>

#### [2] Composite intervals

Given the values:
- _f_ = `from daylight`
- _t_ = `to daylight`
- _d_ = `current daylight`

When the alpha opacity _α_ for the composition should be:

\`alpha=(d-f)/abs(t-f)\`

## Example
```json
{
  "Background": {
    "Overlay": {
      "Pattern": "CheckerBoard",
      "Opacity": [0.54, 0.36]
    },
    "Composite": [
      {
        "From": -1.0,
        "To": 1.0,
        "Images": [
          { "Source": "{Local}/308-{1920w,4096w}-light.webp" },
          { "Source": "{Local}/308-{1920w,4096w}-dark.webp", "Opacity": 1.0 }
        ]
      }
    ]
  }
}
```

<script src="../../../assets/js/ASCIIMathML.js" defer></script>