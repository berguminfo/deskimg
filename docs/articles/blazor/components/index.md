# Index

## Description

The list of components to include.

## Parameters

- `Class`
  - The column class attribute.
  - See [Grid system documentation](https://getbootstrap.com/docs/5.3/layout/grid/).
  - *Data Type: string.*
- `Component`
  - Collection of components to include:
    - [Alert](alert.md)
    - [Atom](atom.md)
    - [Audio](audio.md)
    - [Calendar](calendar.md)
    - [Container](container.md)
    - [Epg](epg.md)
    - [Inline](inline.md)
    - [Location](location.md)
    - [Meteorology](meteorology.md)
    - [Navigation](navigation.md)
    - [Picture](picture.md)
    - [Video](video.md)
  - *Data Type: object[].*

## Examples

```json
{
  "Index": [
    {
      "Class": "col-6",
      "Component": [
        {
          "Atom": {}
        }
      ]
    },
    {
      "Class": "col-6",
      "Component": [
        {
          "Calendar": {}
        },
        {
          "Image": {}
        }
      ]
    }
  ]
}
```