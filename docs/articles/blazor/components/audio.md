# Audio Component

## Description

Component to play audio resources.

# Parameters

- `Visible`
  - Specifies the visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Title`
  - Optional title attribute.
  - *Data Type: string.*
- `Source`
  - `Type`
    - Specifies the MIME media type.
    - *Data Type: string.*
  - `Media`
    - Specifies the media query for the resource's intended media.
    - *Data Type: string.*
  - `Value`
    - Specifies the URL of the media resource.
    - *Data Type: string.*
- `AutoPlay`
  - *Data Type: boolean.*
  - *Default Value: true.*

## Example

```json
{
  "Audio": {
    "Source": {
      "Value": "https://host.domain/audio.mp3"
    }
  }
}
```
