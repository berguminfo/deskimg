# Picture Component

## Description

Component to view a picture.

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Title`
  - The image title.
  - *Data Type: string.*
- `Source`
  - The address of the image.
  - *Data Type: string.*
- `Opacity`
  - The image opacity.
  - *Data Type: number.*
  - *Default Value: `0.87`.*
  - *Interval: [0, 1].*

## Example

```json
{
  "Picture": {
    "Title": "Camera",
    "Source": "https://example.domain/camera.jpg"
  }
}
```