# Location Component

## Description

Component to view details about a location.
- The time `sun` rise and/or set.
- The time `moon` rise and/or set.
- The next time tide level is high and/or low.

> [!TIP]
> For more information:
> - <s>[tidelevels.kartverket.no](../../external/tidelevels.kartverket.no.md)</s>

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Source`
  - The location identifier. See [Geolocation](geolocation.md).
  - Empty or `null` to use current device location.
  - *Data Type: string.*
- `CacheControl`
  - The `Cache-Control` request header.
  - *Data Type: object.*
  - *Default Value: `{ MaxAge: '00:15' }`.*
- `DisplayTitle`
  - The title visibility.
  - *Data Type: boolean.*
  - *Default Value: `false`.*

## Example

```json
{
  "Location": {
    "Source": ""
  }
}
```