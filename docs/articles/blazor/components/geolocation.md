# Geographic location

## Description

This setting specifies a list of geographic locations.

## Parameters

- `CivicAddress`
  - *Data Type: object?.*
  - `City`
    - *Data Type: string.*
  - `Country`
    - *Data Type: string.*
- `Coordinate`
  - Specifies the geographic coordinates, or use the device position if null.
  - *Data Type: object?.*
  - `Latitude`
    - *Data Type: number.*
    - *Interval: [-90, 90].*
  - `Longitude`
    - *Data Type: number.*
    - *Interval: [-180, 180].*
  - `Altitude`
    - *Data Type: number.*
- `TimeZone`
    - *Data Type: string.*
    - [IANA (TZDB) time zone](https://nodatime.org/TimeZones).
- `Tags`
  - *Data Type: string[].*

## Tags: [Location](location.md)

The location can display tide levels.

- `tideforecast.kartverket.no`
  - Tide forecast for Norwegian waters. Uses the current geographic position.

## Tags: [Meteorology](meteorology.md)

The location can display forecasts and observations.

- `metar.noaa.gov?{ICAO}`
  - Observations at world wide airports.
- `forecast.yr.no`
  - Complete world wide forecast for 10 days.
- `forecast.yr.no/compact`
  - Compact world wide forecast for 10 days.

## Tags: [Alert](alert.md)

The location can display weather alerts.

- `wrn.yr.no` = ``.
  - Upcoming and ongoing weather alerts for mostly Norwegian geopositions.
- `avalanche.nve.no` = ``.
  - Upcoming and ongoing avalanche alerts for Norway (summary).
- `avalanche.nve.no/all` = ``.
  - Upcoming and ongoing avalanche alerts for Norway.
- `avalanche.nve.no/region` = `regionid`.
  - Upcoming and ongoing avalanche alerts for a Norwegian region.
- `flood.nve.no` = ``.
  - Upcoming and ongoing flood alerts for Norway (summary).
- `flood.nve.no/all` = ``.
  - Upcoming and ongoing flood alerts for Norway.
- `flood.nve.no/county` = `countyid`.
  - Upcoming and ongoing flood alerts for a Norwegian county.
- `flood.nve.no/municipality` = `municipalityid`.
  - Upcoming and ongoing flood alerts for a Norwegian municipality.
- `landslide.nve.no` = ``.
  - Upcoming and ongoing landslide alerts for Norway (summary).
- `landslide.nve.no/all` = ``.
  - Upcoming and ongoing landslide alerts for Norway.
- `landslide.nve.no/county` = `countyid`.
  - Upcoming and ongoing landslide alerts for a Norwegian county.
- `landslide.nve.no/municipality` = `municipalityid`.
  - Upcoming and ongoing landslide alerts for a Norwegian municipality.

## Example

```json
{
  "Geolocation": {
    "NZSP": {
      "CivicAddress": {
        "City": "Jack F. Paulus Skiway",
        "Country": "AQ"
      },
      "Coordinate": {
        "Latitude": -89.989444,
        "Longitude": -1,
        "Altitude": 2835
      },
      "TimeZone": "CET",
      "Tags": {
        "tideforecast.kartverket.no",
        "metar.noaa.gov?NZSP",
        "forecast.yr.no",
        "wrn.yr.no"
      }
    }
  }
}
```