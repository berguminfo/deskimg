# Epg Component

## Description

Component to view a `Electronic Program Guide`.

> [!TIP]
> For more information:
> - [epg.nrk.no](../../external/epg.nrk.no.md)

## Parameters

- `Visible`
  - The visibility state.
  - *Data Type: boolean.*
  - *Default Value: `true`.*
- `Style`
  - Optional [CSS](https://www.w3.org/Style/CSS/#specs) description object.
  - *Data Type: object.*
- `Source`
  - The EPG provider.
    - One of:
      - `epg.nrk.no?{ChannelIds}`
      - `epg.allente.no?{ChannelIds}`
  - *Data Type: string.*
- `CacheControl`
  - The `Cache-Control` request header.
  - *Data Type: object.*
  - *Default Value: `{ MaxAge: '00:15' }`.*
- `Take`
  - The maximum number of items to display.
  - *Data Type: number.*
  - *Default Value: `4`.*

## Channel Identities

### epg.nrk.no

```json
[
  {"id":"nrk1","name":"NRK1"},
  {"id":"nrk2","name":"NRK2"},
  {"id":"nrk3","name":"NRK3"},
  {"id":"nrksuper","name":"NRK Super"},
  {"id":"nrk_tegnspraak","name":"NRK Tegnspråk"}
]
```

### epg.allente.no

```json
[
  {"id":"0090","name":"NRK1 HD (T)"},
  {"id":"0288","name":"NRK2 HD (T)"},
  {"id":"0289","name":"NRK3/ NRK Super HD (T)"},
  {"id":"0187","name":"TV 2 Direkte HD (N) (T)"},
  {"id":"534","name":"TV Norge HD (T)"},
  {"id":"0298","name":"TV3 HD (N) (T)"},
  {"id":"533","name":"MAX HD (T)"},
  {"id":"0361","name":"TV3+ HD (N) (T)"},
  {"id":"0405","name":"TV 2 Zebra HD (N) (T)"},
  {"id":"0277","name":"TV 2 Livsstil HD (N) (T)"},
  {"id":"0056","name":"FEM HD (T)"},
  {"id":"0206","name":"TV6 (N) (T)"},
  {"id":"1089","name":"Heim HD (T)"},
  {"id":"457","name":"TV 2 Nyheter HD (N) (T)"},
  {"id":"0199","name":"TV 2 Sport 1 HD (N) (T)"},
  {"id":"0406","name":"TV 2 Sport 2 HD (N) (T)"},
  {"id":"532","name":"Discovery Channel HD (N) (T)"},
  {"id":"1012","name":"TLC Norge HD (T)"},
  {"id":"0316","name":"National Geographic HD (N) (T)"},
  {"id":"535","name":"VOX HD (T)"},
  {"id":"1017","name":"BBC Nordic HD (N) (T)"},
  {"id":"0358","name":"Viasat Explore HD (T)"},
  {"id":"0357","name":"Viasat History HD (T)"},
  {"id":"0356","name":"Viasat Nature HD (T)"},
  {"id":"0320","name":"V series HD (T)"},
  {"id":"0321","name":"V film premiere HD (T)"},
  {"id":"0308","name":"V film family (T)"},
  {"id":"1002","name":"SkyShowtime 1 HD (T)"},
  {"id":"1001","name":"SkyShowtime 2 HD (T)"},
  {"id":"0080","name":"MTV (Nordic) (T)"},
  {"id":"0246","name":"MTV 00s (T)"},
  {"id":"604","name":"MTV 80s (T)"},
  {"id":"1010","name":"Bilkanalen Auto Motor og Sport TV HD (N) (T)"},
  {"id":"668","name":"Horse & Country -IP"},
  {"id":"1005","name":"Animal Planet HD (T)"},
  {"id":"1006","name":"Discovery Science (T)"},
  {"id":"1011","name":"ID Investigation Discovery (N) (T)"},
  {"id":"1003","name":"HISTORY HD (N/D/F) (T)"},
  {"id":"1094","name":"Love Nature HD (T)"},
  {"id":"1004","name":"HISTORY 2 HD (T)"},
  {"id":"0086","name":"Nickelodeon (Nordic) (T)"},
  {"id":"0088","name":"Nick Jr. (T)"},
  {"id":"570","name":"Nicktoons (T)"},
  {"id":"0028","name":"Cartoon Network (T) "},
  {"id":"0017","name":"Cartoonito (T)"},
  {"id":"1034","name":"VGTV HD (T)"},
  {"id":"0033","name":"CNN (T)"},
  {"id":"0016","name":"BBC News (T)"},
  {"id":"596","name":"Sky News (T)"},
  {"id":"1008","name":"Bloomberg (T)"},
  {"id":"0032","name":"CNBC (T)"},
  {"id":"1007","name":"DW English (T)"},
  {"id":"0344","name":"Al Jazeera English -OTT"},
  {"id":"1033","name":"France 24 (T)"},
  {"id":"0121","name":"SVT1 HD (T)"},
  {"id":"0141","name":"SVT2 HD (T)"},
  {"id":"0149","name":"Kunskapskanalen HD (T)"},
  {"id":"452","name":"DR1 HD (T)"},
  {"id":"0051","name":"DR2 HD (T)"},
  {"id":"0188","name":"TV 2 HD (D) (T)"},
  {"id":"0147","name":"SVT Barn HD (T)"},
  {"id":"530","name":"Eurosport Norge HD (T)"},
  {"id":"531","name":"Eurosport 1 HD (N) (T)"},
  {"id":"1009","name":"MOTORVISION TV HD (T)"},
  {"id":"0364","name":"V sport golf HD (T)"},
  {"id":"418","name":"V sport ultra HD (T)"},
  {"id":"0271","name":"V sport+ HD (N) (T)"},
  {"id":"0365","name":"V sport 1 HD (N) (T)"},
  {"id":"608","name":"V sport 2 HD (N) (T)"},
  {"id":"609","name":"V sport 3 HD (N) (T)"},
  {"id":"0197","name":"TV 2 Sport Premium HD (N) (T)"},
  {"id":"0198","name":"TV 2 Sport Premium 2 HD (N) (T)"},
  {"id":"1091","name":"Hits HD (N) (T)"},
  {"id":"1090","name":"Stars HD (N) (T)"},
  {"id":"972","name":"SF Kanalen (T)"},
  {"id":"0299","name":"V film action HD (T)"},
  {"id":"0322","name":"V film hits HD (T)"},
  {"id":"1047","name":"V sport Premier League HD (T)"},
  {"id":"0255","name":"V sport live 1 (T)"},
  {"id":"0256","name":"V sport live 2 (T)"},
  {"id":"0257","name":"V sport live 3 (T)"},
  {"id":"0258","name":"V sport live 4 (T)"},
  {"id":"0259","name":"V sport live 5 (T)"}
]
```

## Example [1]

```json
{
  "Epg": {
    "Source": [
      "epg.nrk.no?`nrk1,nrk2,nrk3"
    ]
  }
}
```
## Example [2]

```json
{
  "Epg": {
    "Source": [
      "epg.allente.no?0298,0361,0206"
    ]
  }
}
```