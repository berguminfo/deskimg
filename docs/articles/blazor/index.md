# About Blazor Site (SPA)

## Description

Represents the `Blazor` component responsible to render the wallpaper image.
The rendering can be visualized by a web browser, or captured and rendered on the users desktop.

## Parameters

- `Background`
  - [The list of background compositions.](components/background.md)
- `Index`
  - [The list of components to include.](components/index.md)
- `Geolocation`
  - [The list of known locations.](components/geolocation.md)
- `Session:Toi`
  - The observation date and time.
  - Defaults to the current time.
  - *Data Type: string{DateTime}?.*
- `Session:Language`
  - The localization language.
  - Defaults to the first language set in browser.
  - *Date Type: string.*
- `Session:Navigation`
  - The navigation bar visibility.
  - *Data Type: boolean.*
  - *Default Value: `false`.*
- `Session:Location`
  - The current observation location.
  - Defaults to the location reported by browser.
  - *Data Type: string.*
- `Session:UpdateInterval`
  - The delay for page update.
  - *Data Type: string{TimeSpan}.*
  - *Default Value: `00:05`.*
- `dbug:fetch`
  - Debug feature to fetch sample data.
  - Unavailable in production builds.
  - *Data Type: boolean.*
  - *Default Value: `false`.*
- `dbug:sets`
  - Debug feature to sets sample data.
  - Unavailable in release builds.
  - *Data Type: boolean.*
  - *Default Value: `false`.*

## Query Parameters

For development, sometimes it's “productive” to override components using the query string.

For example the request URI:

```
https://localhost:5000/?nav=✓&lang=en-UK&t=2022-11-25T12:00&geo=ZZZZ&:Index:3:Component:1:Meteorology:Source:0=NZSP&:Index:3:Component:1:Meteorology:Source:1=ZZZZ
```

Will override the following application components:

- `nav=✓`
  - Displays the navigation bar.
- `t=2022-11-25`
  - Sets the relative date to '2022-11-25 00:00:00'.
- `t=2022-11-25T12:00`
  - Sets the relative date to '2022-11-25 12:00:00'.
- `lang=en-UK`
  - Sets the locale to 'en-UK', however not the local timezone.
- `lang=en-US`
  - Sets the locale to 'en-UK', however not the local timezone.
- `geo=ZZZZ`
  - Sets the geolocation to 'ZZZZ'.
- `:Index:3:Component:1:Meteorology:Source:0=NZSP`
  - Sets the opservations station to 'Jack F. Paulus Skiway'.
- `:Index:3:Component:1:Meteorology:Source:1=ZZZZ`
  - Sets the forecast station to 'ZZZZ'.

### Component visibility

- `:Index:1:Component:0:Epg:Visible=False`
  - Hides the _EPG_ component.
- `:Index:2:Component:0:Image:Visible=False`
  - Hides the first _Image_ component.
- `:Index:2:Component:1:Image:Visible=False`
  - Hides the second _Image_ component.
- `:Index:2:Component:2:Image:Visible=False`
  - Hides the third _Image_ component.
- `:Index:2:Component:3:Image:Visible=False`
  - Hides the fourth _Image_ component.
- `:Index:3:Component:0:Alert:Visible=False`
  - Hides the _Alert_ component.
- `:Index:3:Component:1:Meteorology:Visible=False`
  - Hides the _Meteorology_ component.
- `:Index:3:Component:2:Atom:Visible=False`
  - Hides the first _Atom_ component.
- `:Index:3:Component:3:Atom:Visible=False`
  - Hides the second _Atom_ component.
- `:Index:4:Component:0:Location:Visible=False`
  - Hides the _Location_ component.
- `:Index:4:Component:1:Calendar:Visible=False`
  - Hides the _Calendar_ component.
