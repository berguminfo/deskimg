# Blazor Options

Customize application behavior with query string parameters.

## Parameters

- `🌐={string}`
  - Sets current layout option.
- `t={date-time}`
  - Sets current time of interest.
- `geo={string}`
  - Sets the current location.
- `lang={string}`
  - Sets the current language.
- `nav=✓`
  - Sets current navigation visibility.
- `upd={duration}`
  - Sets current update interval.
- `q={string}`
  - Sets the search criteria.

## Languages

```ps1
[CultureInfo]::GetCultures(2) | Format-Table ("Name", "TwoLetterISOLanguageName", "ThreeLetterISOLanguageName", "DisplayName")
```

Some relevant cultures:
```
Name    │ ISO │ DisplayName
────────┼─────┼─────────────────────────────────────────────────
da-DK   │ dan │ Danish (Denmark)
da-GL   │ dan │ Danish (Greenland)
en-001  │ eng │ English (World)
en-150  │ eng │ English (Europe)
en-AI   │ eng │ English (Anguilla)
en-BM   │ eng │ English (Bermuda)
en-DK   │ eng │ English (Denmark)
en-FI   │ eng │ English (Finland)
en-FK   │ eng │ English (Falkland Islands)
en-GB   │ eng │ English (United Kingdom)
en-GG   │ eng │ English (Guernsey)
en-GI   │ eng │ English (Gibraltar)
en-IE   │ eng │ English (Ireland)
en-IM   │ eng │ English (Isle of Man)
en-JE   │ eng │ English (Jersey)
en-KY   │ eng │ English (Cayman Islands)
en-MS   │ eng │ English (Montserrat)
en-PN   │ eng │ English (Pitcairn Islands)
en-SE   │ eng │ English (Sweden)
en-SH   │ eng │ English (St Helena, Ascension, Tristan da Cunha)
en-TC   │ eng │ English (Turks & Caicos Islands)
en-VG   │ eng │ English (British Virgin Islands)
fi-FI   │ fin │ Finnish (Finland)
fo-DK   │ fao │ Faroese (Denmark)
fo-FO   │ fao │ Faroese (Faroe Islands)
ga-GB   │ gle │ Irish (United Kingdom)
ga-IE   │ gle │ Irish (Ireland)
gd-GB   │ gla │ Scottish Gaelic (United Kingdom)
is-IS   │ isl │ Icelandic (Iceland)
kl-GL   │ kal │ Kalaallisut (Greenland)
nb-NO   │ nob │ Norwegian Bokmål (Norway)
nb-SJ   │ nob │ Norwegian Bokmål (Svalbard & Jan Mayen)
nn-NO   │ nno │ Norwegian Nynorsk (Norway)
sv-AX   │ swe │ Swedish (Åland Islands)
sv-FI   │ swe │ Swedish (Finland)
sv-SE   │ swe │ Swedish (Sweden)
```
