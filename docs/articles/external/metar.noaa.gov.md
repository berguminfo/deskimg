# metar.noaa.gov

## Endpoints

> https://tgftp.nws.noaa.gov/data
>
> - __GET__
>   - /observations/metar/stations/`{id}`.TXT

## Path Parameters

- `{id}`
  - The ICAO identifier for the weather station. See http://www.rap.ucar.edu/weather/surface/stations.txt

## Responses

### 200 OK

```
YMML 302200Z 36023G33KT 9999 FEW018 SCT030 BKN120 22/17 Q1001
INTER 2200/0100 3000 TSRA BKN010 FEW040CB FM2200 MOD/SEV TURB BLW 5000FT
```

## Appendix A

Metar codes are of the form:
YMML 302200Z 36023G33KT 9999 FEW018 SCT030 BKN120 22/17 Q1001
INTER 2200/0100 3000 TSRA BKN010 FEW040CB FM2200 MOD/SEV TURB BLW 5000FT

This is a coded observation of the current weather, which is mainly used for
aviation. Each section has a definite format.

Station ID (YMML)
A four-character ICAO station code.
Example: YMML is Melbourne, Australia.

Time of report (302200Z)
Format: ddhhmmZ
dd - day of month.
hhmm - hour and minute of report.
Z - indicates Zulu timezone, or UT.
Example 302200Z 22:00Z on the 30th of the month.

Wind speed and direction (36023G33KT)
Format: dddkkKT
    dddkkGkkKT
    dddkkKT dddVddd
    VRBkkKT
ddd - the wind direction. 90 degrees is encoded 090.
kk - the wind speed in knots.
Gkk - if the wind is gusting, the gust speed in knots is created. (as in theis example).
dddVddd - If the wind direction is variable and the wind speed is greater than 6 knots, give the direction range. e.g. 23013KT 210V250
VRBkk - If the wind speed is variable and less than 6 knots, use VRB05KT.
Example: 36023G33KT 360 degrees heading, 23 kts speed with gusts to 33 kts.

Visibility (9999)
Format: vvvvvSM
vvvvv - visibility in statute miles. Visibility in fractional miles is encoded with a space, i.e. 2 1/4SM. Automated systems with visibilities less then 1/4 of a mile is encoded M1/4SM.
9999 - Visibility greater than maximum recorded value (e.g. 30 miles) is written as 9999

Runway visual range (N/A)
Format: Rdd/vvvvFT
    Rdd/vvvvVvvvvFT
dd - the runway identifier. e.g. R02, or possibly R02L if the left runway is reported.
vvvvFT - the reported visual range in feet.
vvvvVvvvvFT - a range of visual ranges for that runway.
M - The actual value is less than the reported value.
P - The actual value is more than the reported value.
Examples: R26L/2400FT -- Runway 26 Left has a range of 2400 ft.
R08/0400V0800FT -- Runway 08 has a visual range between 400 and 800 feet.

Current weather (N/A)
Intensity   Description     Precipitation   Obscuration     Other
-  Light    MI Shallow  DZ Drizzle  BR Mist     PO Well developed dust / sand whirls
   Moderate PR Partial  RA Rain     FG Fog      SQ Squalls
+  Heavy    BC Patches  SN Snow     FU Smoke    FC Funnel clouds inc tornadoes or waterspouts
VC Vicinity     DR Low drifting SG Snow grains      VA Volcanic ash     SS Sandstorm
BL Blowing      IC Ice crystals DU Widespread dust  DS Duststorm
SH Showers      PL Ice pellets  SA Sand
TS Thunderstorm GR Hail         HZ Haze
FZ Freezing     GS Small hail   PY Spray
UP Unknown
e.g. -SHRA - Light showers of rain
TSRA - Thunderstorms and rain.

Cloud cover (FEW018 SCT030 BKN120)
Format: nnnhhh
nnn - Sky condition.
    SKC     (0/8)
    CLR     (0/8)
    FEW     (1/8, 2/8)
    SCT     (3/8, 4/8)
    BKN     (5/8, 6/8, 7/8)
    OVC     (8/8)
    VV  (8/8) - vertical visibility.
hhh - cloud height in flight level (100 feet units)
Examples:
FEW018 Few clouds at 1800 ft,
SCT030 Scattered clouds at 3000 ft,
BKN120 Broken clouds at 12,000 ft.

Temperature (22/17)
Format: tt/dd
tt - Temperature in degrees celcius.
dd - Dewpoint in degrees celcius.
M - indicates a negative temperature or dewpoint.
Example 22/17 Temperature 22 degrees C, dewpoint 17 degrees C.

Atmospheric pressure (Q1001)
Format: Qpppp
    Ahhhh
Qpppp - Pressure in hPa (or mb)
Ahhhh - Pressure in inches of Hg multiplied by 100 (29.95 inHg is encoded as A2995).
Example Q1001 is an atmospheric pressure of 1001 hPa, 1001 millibars, which is equivalent to 29.56 inHg.

Remarks (remainder of report)
Format: RMK Additional remarks may be placed in the final body of the METAR report. The format of these remarks can be any of the following.

Volcanic eruptions
Format: Plain language.
Includes: Name of the volcano, Lat and Long or the direction and distance, date and time of the erruption, size, description of an ash cloud.

Funnel cloud
Format: Type B(hh)mm LOC
Type -  Type of funnel clound. Can be one of
    TORNADO
    FUNNEL CLOUD
    WATERSPOUT

B(hh)mm - Begining time (can also specify E(hh)mm for ending time). hh is the hour of the sighting, which is deleted if redundant. mm is the minute of the sighting.

LOC - Location or direction of movement.
Example, TORNADO B24 3 SW indicates a tornado sighted at 24 minutes past the hour, 3 miles SW of the station.

Type of automated station
Format: AO1 or AO2.
AO1 - station without a precipitation descriminator.
AO2 - station with a precipitation descriminator.

Peak wind
Format: PK WND dddff(f)/(hh)mm
ddd - wind direction
ff(f) - wind speed in kts
(hh)mm - time at which wind speed occurred.
Example: PK WND 32024/45 means a peak wind of 24 kts at 320 degrees occurred at 45 mins past the hour.

Wind shift
Format: WSHIFT (hh)mm [FROPA]
(hh)mm - The time the wind shift occurred.
FROPA - If the wind shift was the result of a frontal passage.

Visibility
Format: TWR VIS vvvvv
    SFC VIS vvvvv
    VIS lllllVuuuuu
    VIS [DIR] vvvvv
    VIS vvvvv [LOC]
TWR VIS - Tower visibility
SFC VIS - Surface visibility
VIS [DIR] - Sector visibility with additional direction (e.g. VIS NE 2 - 2 miles visibility in the NE).
VIS vvvvv [LOC] - Visibility at a second location.
vvvvv - Visibility in statute miles.

VIS lllllVuuuuu - Variable visibility.

Lightning
Format: Frequency LTG(type) [LOC]
Frequency can be one of:
    OCNL    Occasional, less than 1 flash per min
    FRQ Frequent, 1 to 6 flashes per min
    CONS    Continuous, more than 6 flashes per min
LTG(type) can by one of:
    CG  Cloud-ground
    IC  In cloud
    CC  Cloud-cloud
    CA  Cloud-air

[LOC] - location, can be either a station, or VC, DSNT for vicinity or distant.
Example, FRQ LTGICCC VC, which indicates frequent in-clound and cloud-cloud ligntning in the vicinity.

Begining end ending precipitation
Format: wwB(hh)mmE(hh)mm
ww - weather phenomonon (eg RA).
(hh)mm - begining or ending time.

Thunderstorm location
Format: TS LOC (MOV DIR)
LOC - location
DIR - direction
Example, TS NW MOV NE Thunderstorms from the NW are moving to the NE.

Hailstone size
Format: GR [size]
size - size of the hailstones in inches.

Virga
Format: VIRGA (DIR)
DIR - direction.

Variable ceiling height
Format: CIG hhhVhhh
hhh - Ceiling height in feet.

Obscurations
Format: ww [nnn]hhh
ww - weather phenomenon
[nnn] - amount of obscuration, if applicable (e.g. FEW, SCT, OVC)
hhh - height of obscuration.
Examples, FG FEW000, would be fog covering up to 2/8 of the sky at ground level.

Variable sky condition
Format: nnn(hhh) V nnn
nnn amount of obscuration (FEW, OVC etc)
hhh height of obscuration if applicable.

Siginificant cloud types
Format: type dir (mov)
Type - one of,
    CB  Cumulonimbus
    CBMAM   Cumulonimbus mammatus
    TCU Towering cumulonimbus
    ACC Altocumulus castellanus
    CLD Standing lenticular or rotor clouds
dir - Direction of cloud (N, NW, etc.)
mov - Movement of cloud if appropriate.

Ceiling height at second location
Format: CIG hhh [LOC]
hhh - Height of ceiling in flight level units (100 feet).
[LOC] - Location of ceiling if appropriate.

Pressure rising or falling rapidly
Format: PRESRR or PRESFR

Sea-level pressure
Format: SLPppp
ppp - pressure in hPa. If no pressure measurement is available, then NO is recorded.

Aircraft mishap
Format: ACFT MSHP [Plain language]

Snow increasing rapidly
Format: SNINCR [iiiii/ggggg]
iiiii - Increase in snow level in inches per hour.
ggggg - Snow level on the ground in inches.

While I was writing this, the weather at Melbourne was:

YMML 302100Z 36027G37KT 9999 VCSH FEW045 SCT050 BKN120 21/17 Q1001
RERA INTER 2100/2400 3000 TSRA BKN010 FEW040CB FM2100 MOD/SEV TURB BLW 5000FT

YMML 302200Z 36023G33KT 9999 FEW018 SCT030 BKN120 22/17 Q1001
RMK RF00.0/000.4 INTER 2200/0100 3000 TSRA BKN010 FEW040CB
FM2200 MOD/SEV TURB BLW 5000FT

YMML 302300Z 01016KT 9999 -SHRA FEWSCT025 BKN035 20/18 Q1001
RERA RMK RF00.0/000.0 INTER 2300/0200 6000 SHRA BKN012
FM2300 MOD/SEV TURB BLW 5000FT

YMML 310000Z 02018KT 9999 -RA FEW018 SCT025 OVC140 20/19 Q0999
RERA RMK RF00.2/000.6 INTER 0000/0300 6000 SHRA BKN012
FM0000 MOD/SEV TURB BLW 5000FT

YMML 310100Z 36017KT 9999 VCSH FEW016 BKN025 OVC140 22/20 Q0999
RERA RMK RF00.0/000.8 INTER 0100/0400 6000 SHRA BKN012 FM0100 MOD/SEV TURB BLW 5000FT

YMML 310126Z 36021G34KT 9999 VCSH BKN025 23/19 Q0999
INTER 0126/0426 6000 SHRA BKN012 FM0126 MOD/SEV TURB BLW 5000FT

YMML 310153Z 36027G37KT 9999 -SHRA FEW018 BKN025 BKN035 23/18 Q0999
RERA INTER 0153/0453 6000 SHRA BKN012 FM0153 MOD/SEV TURB BLW 5000FT

YMML 310214Z 35022G33KT 4000 SHRA FEW016 BKN025 BKN035 20/17 Q1000
RERA FM0214 35025G40KT 9999 SHRA SCT015 BKN030 INTER 0214/0514
6000 SHRA BKN012 FM0214 MOD/SEV TURB BLW 5000FT

Last Updated: 17 February 2003. Richard Ogley