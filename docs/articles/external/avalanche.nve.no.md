# avalanche.nve.no

Snøskredvarslingen i Norge utgir varsler etter internasjonal standard. Formålet med varslingen er å unngå tap av liv og verdier som følge av snøskred. Snøskredvarslingen i Norge utarbeider snøskredvarsler daglig som beskriver faregrad, skredproblem og utsatt terreng for hvert varslingsområde.

Bruk swagger for ytterligere dokumentasjon av apiet
https://api01.nve.no/hydrology/forecast/avalanche/v6.2.1/swagger/

## Endpoints

> https://api01.nve.no/hydrology/forecast/avalanche/v6.2.1
>
> - __GET__
>   - /api/AvalancheWarningByCoordinates/Simple/`{x}`/`{y}`/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/AvalancheWarningByRegion/Simple/`{regionid}`/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/All/`{langkey}`/`{startdate}`/`{enddate}`

## Path Parameters

- `x`
  - The latitude coordinate value.
  - *Value: (required).*
  - *Data Type: number.*
- `y`
  - The longitude coordinate value.
  - *Value: (required).*
  - *Data Type: number.*
- `langkey`
  - One of: 1 = Norwegian. 2 = English.
  - *Value: (required).*
  - *Data Type: number.*
- `startdate`
  - The start date (yyyy-MM-dd).
  - *Value: (required).*
  - *Data Type: string.*
- `enddate`
  - The end date (yyyy-MM-dd).
  - *Value: (required).*
  - *Data Type: string.*


## Responses

### 200 OK

```json
[
  {
    "RegionName": "Vest-Finnmark",
    "ValidFrom": "2014-03-13T00:00:00",
    "ValidTo": "2014-03-13T23:59:00",
    "DangerLevel": "4",
    "MainText": "Hovedbudskap (norsk)"
  },
  "...",
  "..."
]
```

## DangerLevel

- 0: Not given
- 1: Low
- 2: Moderate
- 3: Considerable
- 4: High
- 5: Extreme
