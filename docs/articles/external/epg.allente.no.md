# epg.allente.no

## Description

Returns EPG for one or more channels for a date.

## Endpoints

> https://cs-vcb.allente.no
>
> - __GET__
>   - /epg/events?date=`{Date}`&category-filter=`{Categories}`

### Path Parameters

- `{Date}`
  - EPG Date.
  - *Value: (optional).*
  - *Parameter Type: query.*
  - *Date Type: string.*
  - *Example: `2000-11-22`.*
- `{Categories}`
  - EPG categories.
    - `program`
    - `series`
    - `news`
    - `entertainment`
    - `sport`
    - `children`
    - `documentary`
    - `other`
  - *Value: (optional).*
  - *Parameter Type: query.*
  - *Date Type: string[].*

### Response (Status 200)

```json
{
  "date": "2020-01-01",
  "categories": [],
  "channels": [
    {
      "id": "1051",
      "icon": "//images.ctfassets.net/989y85n5kcxs/CQmW3A8moHB1D0mU7wbZ6/c4d05ae6ecebfa840e40917e625ad543/VSport_PL_4_Logo_RGB_pos.svg",
      "name": "V sport Premier League 4 HD (T)",
      "events": [
        {
          "id": "0623202304300400",
          "live": false,
          "time": "2020-01-01T00:00:00Z",
          "title": "Sendeopphold",
          "details": {
            "title": "Sendeopphold",
            "image": "https://cd-static.telenorcdn.net/cdepg/img/epg/1080/607/353347.jpg",
            "description": "Sendeopphold",
            "season": 0,
            "episode": 0,
            "categories": [ "other" ],
            "duration": "360"
          }
        }
      ]
    }
  ]
}
```
