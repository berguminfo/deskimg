```graphql
query {
  operationName = schedule
  variables = {
      "date": "2023-05-05"
  }
  extensions = {
      "persistedQuery": {
          "version": 1,
          "sha256Hash": "be43b9a1e15de6de1011e4a572245d21fcaa9a3b9b1816443e1c42655496f970"
      }
  }
}
```