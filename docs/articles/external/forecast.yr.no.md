# forecast.yr.no

## Description

> [!TIP]
> For more information:
> - https://api.met.no/weatherapi/locationforecast/2.0/documentation

## Endpoints

> https://api.met.no/weatherapi
>
> - __GET__
>   - /locationforecast/2.0/complete?lat=`{latitude}`&lon=`{longitude}`&altitude=`{altitude}`
>   - /locationforecast/2.0/compact?lat=`{latitude}`&lon=`{longitude}`&altitude=`{altitude}`

## Query Parameters

- `{latitude}`
  - The latitude.
- `{longitude}`
  - The longitude.
- `{altitude}`
  - The altitude.

## 200 OK

```json
{
  "properties": {
    "meta": {
      "units": {
        "air_temperature": "celcius",
        "precipitation_amount": "mm",
        "wind_from_direction": "degrees",
        "wind_speed": "m/s"
      }
    },
    "timeseries": [
      {
        "instant": {
          "details": {
            "air_temperature": -1.2,
            "wind_from_direction": 259.9,
            "wind_speed": 3.4
          }
        },
        "next_6_hours": {
          "summary": {
            "symbol_code": "rain"
          },
          "details": {
            "precipitation_amount": 4.5
          }
        }
      }
    ]
  }
}
```