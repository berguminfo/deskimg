# epg.nrk.no

## Description

Elektronisk programguide (EPG) for NRK TV og Radio.

> [!TIP]
> For more information:
> - [Open API](spec/epg.nrk.no.json)
> - https://psapi.nrk.no/documentation/redoc/epg/

## Endpoints

> https://psapi.nrk.no
>
> - __GET__
>   - /epg/`{ChannelIds}`?date=`{Date}`

## Path Parameters

- `{ChannelIds}`
  - Comma separated channel ids:
    - nrk1
    - nrk2
    - nrk3
    - nrksuper
    - nrk_tegnspraak
  - *Value: (required).*
  - *Data Type: string.*
  - *Example: `nrk1,nrk2`.*

## Query Parameters

- `{Date}`
  - The date for the EPG.
  - *Value: (optional).*
  - *Date Type: string.*
  - *Example: `date=2021-04-11`.*

## Responses

### 200 OK

```json
[
  {
    "_links": {
      "self": {
        "href": "/epg/nrk1"
      },
      "fargerik": {
        "href": "https://fargerik-psapi.nrk.no/tv/epg/channel/nrk1"
      }
    },
    "id": "epg_nrk1",
    "sourceMedium": 1,
    "channel": {
      "id": "nrk1",
      "title": "NRK1",
      "sourceMedium": 1,
      "isLive": true,
      "hasEpg": true,
      "isOndemandChannel": true,
      "isDistrictChannel": false,
      "hasDistrictChannels": true,
      "priority": 1,
      "illustration": [
        {
          "imageInfo": {
            "id": "cruot8A4cjIzmFPLq9xlZg",
            "cropInfo": {
              "x": 0,
              "y": 0,
              "width": 1,
              "height": 1
            }
          },
          "imageWidthCropInfo": "cruot8A4cjIzmFPLq9xlZg?x=0.00000000000000000&y=0.00000000000000000&w=1.00000000000000000&h=1.00000000000000000",
          "webImages": [
            {
              "imageUrl": "https://gfx.nrk.no/cruot8A4cjIzmFPLq9xlZgvx4DIyF_NY2aZvqA3Ed-RA",
              "pixelWidth": 300
            },
            {
              "imageUrl": "https://gfx.nrk.no/cruot8A4cjIzmFPLq9xlZgbp6YGtKLp7yaZvqA3Ed-RA",
              "pixelWidth": 600
            },
            {
              "imageUrl": "https://gfx.nrk.no/cruot8A4cjIzmFPLq9xlZg4y0kkoRVnP2aZvqA3Ed-RA",
              "pixelWidth": 960
            },
            {
              "imageUrl": "https://gfx.nrk.no/cruot8A4cjIzmFPLq9xlZgjzVh8NzWtVKaZvqA3Ed-RA",
              "pixelWidth": 1280
            },
            {
              "imageUrl": "https://gfx.nrk.no/cruot8A4cjIzmFPLq9xlZgZjnmaNykr9GaZvqA3Ed-RA",
              "pixelWidth": 1600
            },
            {
              "imageUrl": "https://gfx.nrk.no/cruot8A4cjIzmFPLq9xlZgODsLa7wIKq-aZvqA3Ed-RA",
              "pixelWidth": 1920
            }
          ],
          "isDefaultImage": false
        }
      ],
      "mediaAssetsLive": {
        "flashUrl": "https://psapi.nrk.no/obsoleteproperty?endpoint=epg/nrk1&property=FlashUrl&useragent=Mozilla%2f5.0",
        "hlsUrl": "https://psapi.nrk.no/obsoleteproperty?endpoint=epg/nrk1&property=HlsUrl&useragent=Mozilla%2f5.0",
        "hlsMobileUrl": "https://psapi.nrk.no/obsoleteproperty?endpoint=epg/nrk1&property=HlsMobileUrl&useragent=Mozilla%2f5.0",
        "mp3Url": "https://psapi.nrk.no/obsoleteproperty?endpoint=epg/nrk1&property=Mp3Url&useragent=Mozilla%2f5.0",
        "bufferDuration": "PT0S"
      },
      "parentChannelId": null
    },
    "date": "/Date(1672527600000+0100)/",
    "title": "nrk1",
    "description": "Elektronisk Program Guide for en gitt dag",
    "entries": [
      {
        "_links": {
          "program": {
            "href": "/programs/KOID24002421"
          }
        },
        "programId": "KOID24002421",
        "seriesId": "ett-aar-i-lofoten",
        "onDemandUsageRights": {
          "isGeoBlocked": true,
          "availableFrom": "/Date(1641358920000+0100)/",
          "availableTo": "/Date(1793487599000+0100)/",
          "hasRightsNow": true
        },
        "category": {
          "id": "dokumentar",
          "displayValue": "Dokumentar",
          "isTvCategory": true,
          "isRadioCategory": false
        },
        "reRun": true,
        "partNumber": 1,
        "seriesTitle": "Ett år i Lofoten",
        "legalAge": {
          "id": "A",
          "displayValue": "Tillatt for alle",
          "displayAge": "A"
        },
        "availability": {
          "status": "available",
          "hasLabel": false,
          "label": null
        },
        "title": "Ett år i Lofoten: Sommer og høst",
        "description": "Om sommeren er lyset på hele natta, til glede for fastboende og tilreisende. Og fram til høsten får Lofotens lam og sauer nyte friheten på beite.",
        "plannedStart": "/Date(1672553400000+0100)/",
        "actualStart": "/Date(1672553346000+0100)/",
        "duration": "PT44M12.8S",
        "relativeTimeType": 1,
        "image": {
          "imageInfo": {
            "id": "YUefkZO8e48KvLnzyzAMVQ",
            "cropInfo": {
              "x": 0,
              "y": 0.0125972014,
              "width": 1,
              "height": 0.87480557
            }
          },
          "imageWidthCropInfo": "YUefkZO8e48KvLnzyzAMVQ?x=0.00000000000000000&y=0.01259720100000000&w=1.00000000000000000&h=0.87480559900000000",
          "webImages": [
            {
              "imageUrl": "https://gfx.nrk.no/YUefkZO8e48KvLnzyzAMVQvhIJNdHsR6gya1-XFh4XJw",
              "pixelWidth": 300
            },
            {
              "imageUrl": "https://gfx.nrk.no/YUefkZO8e48KvLnzyzAMVQVNGjr6OSn9gya1-XFh4XJw",
              "pixelWidth": 600
            },
            {
              "imageUrl": "https://gfx.nrk.no/YUefkZO8e48KvLnzyzAMVQnPGRuVHCr1Mya1-XFh4XJw",
              "pixelWidth": 960
            },
            {
              "imageUrl": "https://gfx.nrk.no/YUefkZO8e48KvLnzyzAMVQeqy0vuwJy0sya1-XFh4XJw",
              "pixelWidth": 1280
            },
            {
              "imageUrl": "https://gfx.nrk.no/YUefkZO8e48KvLnzyzAMVQ6SlgaDswjv0ya1-XFh4XJw",
              "pixelWidth": 1600
            },
            {
              "imageUrl": "https://gfx.nrk.no/YUefkZO8e48KvLnzyzAMVQv35M6L7IdnEya1-XFh4XJw",
              "pixelWidth": 1920
            }
          ],
          "isDefaultImage": false
        },
        "firstTransmission": {
          "publicationDate": "2022-01-05T06:00:00+01:00",
          "channelId": "nrkw",
          "isRerun": false,
          "displayValue": "NRK TV · Onsdag 5. januar 2022 kl. 06:00"
        },
        "lastTransmission": {
          "publicationDate": "2023-01-01T07:10:00+01:00",
          "channelId": "nrk1",
          "isRerun": true,
          "displayValue": "NRK1 · Søndag 1. januar 2023 kl. 07:10"
        },
        "nextPlannedTransmission": null,
        "isTranscoded": true,
        "programUrlMetadata": "sesong-1/episode-2",
        "sourceMedium": 1,
        "reviewUrl": null,
        "type": "program",
        "mediaAssetsLive": null
      }
  }
]
```
