# epg.svt.se

## Description

Elektronisk programguide (EPG) for SVT.

## Endpoints

- https://contento.svt.se/graphql

## Query Parameters

- operationName
  - BroadcastSchedule
  - *Value: (required).*
  - *Data Type: string.*
- variables
  - JSON object
  - *Value: (required).*
  - *Data Type: string.*
  - day
    - 2023-11-16
    - *Value: (required).*
    - *Data Type: string.*
- extensions
  - JSON object
  - *Value: (required).*
  - *Data Type: string.*
  - persistedQuery
    - JSON query
    - *Value: (required).*
    - *Data Type: string.*
    - sha256Hash
      - 531c44f50daa51c11a003de3a41c0a665ee3fb46b8fecceec10212ee797eb6ce
      - *Value: (required).*
      - *Data Type: string.*
    - version
      - 1
      - *Value: (required).*
      - *Data Type: number.*

## Channel Identities

- ch-svt1
- ch-svt2
- ch-barnkanalen
- ch-kunskapskanalen"
- ch-svt24

## Responses

### 200 OK

```json
{
  "data": {
    "channels": {
      "channels": [
        {
          "id": "ch-svt1",
          "name": "SVT 1",
          "schedule": [
            {
              "descriptionRaw": "Samtal med författaren Katarina Wennstam, aktuell med ny bok, där hon skildrar fosterdrivning och kvinnors hårda villkor under slutet på 1800-talet. Det blir också matlagning med kocken Anders Samuelsson och tips om aktuella läsvärda böcker. Och ett reportage om Tonje Halvorsen som skapar kläder av säkerhetsnålar. Programledare: Reyhaneh Rouhani.",
              "name": "Go'kväll",
              "subHeading": "",
              "startTime": "05:15",
              "start": "2023-11-16T05:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407094-121AR",
              "svtId": "8WAgYZd",
              "rerunDescription": "Från SVT1 tidigare idag",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Katarina Wennstam och säkerhetsnålskläder",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8WAgYZd/gokvall/katarina-wennstam-och-sakerhetsnalsklader",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8WAgYZd",
                "parent": {
                  "svtId": "826Ea26",
                  "urls": {
                    "svtplay": "/gokvall",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135768,
                      "id": "33001838",
                      "description": "Inger Ljung Olsson - Go'kväll",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-15T18:45:00+01:00",
                "validToFormatted": "Fre 15 dec (29 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Dagens viktigaste nyheter och analyser med täta uppdateringar. Vi sänder direkt inrikes- och utrikesnyheter inklusive väder, sport, kultur, nöje och lokala nyheter. Dessutom intervjuer med aktuella gäster. Nyhetssammanfattningar varje hel- och halvtimme med start kl 06.00.",
              "name": "Morgonstudion",
              "subHeading": "",
              "startTime": "06:00",
              "start": "2023-11-16T06:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407398-229A",
              "svtId": "jQnVJDN",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 06:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jQnVJDN/morgonstudion/idag-06-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jQnVJDN",
                "parent": {
                  "svtId": "8r9wQQg",
                  "urls": {
                    "svtplay": "/morgonstudion",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133147,
                      "id": "35798005",
                      "description": "Morgonstudion",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T06:00:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Sofia Åhman leder SVT:s hemmagympapass. Denna gång fokuserar vi på core.",
              "name": "Hemmagympa med Sofia",
              "subHeading": "",
              "startTime": "10:00",
              "start": "2023-11-16T10:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406193-014A",
              "svtId": "KRJ4ApX",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Core",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KRJ4ApX/hemmagympa-med-sofia/core",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KRJ4ApX",
                "parent": {
                  "svtId": "8PvW9no",
                  "urls": {
                    "svtplay": "/hemmagympa-med-sofia",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136691,
                      "id": "26774345",
                      "description": "Hemmagympa med Sofia.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2022-11-03T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Artisten Ann-Louise Hanson och hennes dotter Josefin Glenmark har en fin relation med stor gemenskap. Men så ser det inte alltid ut i relationen mellan vuxna barn och deras föräldrar. Prästen Olle Carlsson möter ofta människor som behöver göra upp med sina föräldrar. Del 15 av 20. UR.",
              "name": "Studio 65",
              "subHeading": "Säsong 2 — Avsnitt 15: Jag och mitt vuxna barn",
              "startTime": "10:20",
              "start": "2023-11-16T10:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408841-015A",
              "svtId": "jMyv9Q5",
              "rerunDescription": "Från 15/11 i K. Även i K 18/11 och SVT2 22/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00010107020015",
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "15. Jag och mitt vuxna barn",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jMyv9Q5/studio-65/15-jag-och-mitt-vuxna-barn",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 2 — Avsnitt 15",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jMyv9Q5",
                "parent": {
                  "svtId": "jawpzV3",
                  "urls": {
                    "svtplay": "/studio-65",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1691389739,
                      "id": "50527216",
                      "description": "Kattis Ahlström, Anders Palmgren (stående) samt Ingmar Skoog - Studio 65",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-15T02:00:00+01:00",
                "validToFormatted": "Mån 17 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hollywoodstjärnan Malin Åkerman älskar att göra film i Sverige som hon mer än gärna kallar sitt hem. Farah Abadi samtalar med skådespelaren som först ville bli barnpsykolog och fortfarande brinner för sociala frågor. Möt också konstnären Sara Sjöbäck som slaktar gamla bilar för att kunna skapa sina konstverk. Bluessångaren Eric Bibb växte upp mitt i kampen för de svartas rättigheter i USA. Nu har han skrivit musiken till föreställningen Ögonblicket som spelas runtom i Västmanland.   Programledare: Farah Abadi.",
              "name": "Sverige!",
              "subHeading": "",
              "startTime": "10:50",
              "start": "2023-11-16T10:50:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406757-011A",
              "svtId": "jw2XkVY",
              "rerunDescription": "Från 11/11 i SVT1",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Malin Åkerman om kärleken till Sverige",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jw2XkVY/sverige/malin-akerman-om-karleken-till-sverige",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jw2XkVY",
                "parent": {
                  "svtId": "jdwG1L4",
                  "urls": {
                    "svtplay": "/sverige",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1692785267,
                      "id": "50591152",
                      "description": "Programledare: Farah Abadi. - Sverige!",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-10T02:00:00+01:00",
                "validToFormatted": "Tor 9 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Under 1800-talet hade hårslöjd och postischtillverkningen sin guldålder. När kort hår blev mode försvann hårkonst allt mer i glömska innan ett par entusiaster startade kurser i hårslöjd i Våmhus i slutet av 50-talet. Sedan dess håller \"hårnätet\" liv i traditionen i Våmhus i Dalarna.",
              "name": "Slöjdreportage",
              "subHeading": "Säsong 1 — Avsnitt 1: Hårkullor",
              "startTime": "11:20",
              "start": "2023-11-16T11:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1385919-001A",
              "svtId": "jRg9oPK",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Hårkullor",
                "shortDescription": "Reportage från Go'kväll",
                "urls": {
                  "svtplay": "/video/jRg9oPK/slojdreportage/1-harkullor",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jRg9oPK",
                "parent": {
                  "svtId": "K5ryn97",
                  "urls": {
                    "svtplay": "/slojdreportage",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132938,
                      "id": "36813971",
                      "description": "Slöjdreportage",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-03-28T20:50:00+02:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Läkaren Henrik utsätter sitt blod för livsfarligt ormgift. Kemisten Ulf och programledaren Robin Paulsson ordnar en lite annorlunda vinprovning och så berättar historiken Gunnar varför snus är en så stor grej för svenskarna. Medverkande: Robin Paulsson, Henrik Widegren, Jessica Abbott, Gabriella Stenberg Wieser, Gunnar Wetterberg och Jenny Nilsson. Del 7 av 8.",
              "name": "Fråga Lund",
              "subHeading": "",
              "startTime": "11:30",
              "start": "2023-11-16T11:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408340-007A",
              "svtId": "8LaVpRn",
              "rerunDescription": "Från 14/11 i SVT1. Även i SVT24 senare i natt, SVT24 17/11 och SVT1 18/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ormgift och cirkuskonster",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8LaVpRn/fraga-lund/ormgift-och-cirkuskonster",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8LaVpRn",
                "parent": {
                  "svtId": "KQYbQZD",
                  "urls": {
                    "svtplay": "/fraga-lund",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1693703749,
                      "id": "50618596",
                      "description": "Fråga Lund med programledare Robin Paulsson.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-14T02:00:00+01:00",
                "validToFormatted": "Tor 13 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Den ständiga resan genom Sverige via lokalnyheterna. Reportage, människor och möten i Sveriges mänskligaste tv-program. Programledare: Henrik Kruusval.",
              "name": "Landet runt",
              "subHeading": "",
              "startTime": "12:30",
              "start": "2023-11-16T12:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408804-011A",
              "svtId": "e5d77wd",
              "rerunDescription": "Från 12/11 i SVT1",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Sön 12 nov 02:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/e5d77wd/landet-runt/son-12-nov-02-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e5d77wd",
                "parent": {
                  "svtId": "ewkE7NN",
                  "urls": {
                    "svtplay": "/landet-runt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698151551,
                      "id": "50762072",
                      "description": "Programledare är Henrik Kruusval. - Landet runt",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Tis 12 dec (26 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vädrets makter kan han inte rå på, så när vårlossningen tvingar honom att avbryta turen bestämmer sig Jens för att genomföra den sista delen till fots. Med tungt lastad ryggsäck ger han sig ut på resans sista etapp. Och han smider redan nya planer, om att leva pälsjägarliv i Kanada... Trots att han saknar tidigare erfarenhet av att köra hundspann har äventyraren Jens Kvernmo bestämt sig för att tillsammans med sex draghundar köra den närmare 150 mil långa sträckan mellan Finnmark och hemtrakterna i Trøndelag. Del 6 av 6.",
              "name": "Jens i vildmarken",
              "subHeading": "Säsong 2 — Avsnitt 6: Sommaren är nära",
              "startTime": "13:15",
              "start": "2023-11-16T13:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1393166-006A",
              "svtId": "KyV4YBW",
              "rerunDescription": "Tidigare sänt 2021",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "6. Sommaren är nära",
                "shortDescription": "Norsk serie från 2019",
                "urls": {
                  "svtplay": "/video/KyV4YBW/jens-i-vildmarken/6-sommaren-ar-nara",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 2 — Avsnitt 6",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KyV4YBW",
                "parent": {
                  "svtId": "jv1GEQB",
                  "urls": {
                    "svtplay": "/jens-i-vildmarken",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135926,
                      "id": "31547502",
                      "description": "Äventyraren Jens Kvernmo. - Jens i vildmarken",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-13T02:00:00+01:00",
                "validToFormatted": "Fre 15 nov 2024 (12 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nicole besöker Angel för att be om hjälp med en gammal tapet hon hittat i slottets gömmor. Hon får även med sig några goda råd och väl hemma fortsätter slottsrenoveringen. Del 17 av 20: Lära från en mästare.",
              "name": "Drömslottet: Tro, hopp och murbruk",
              "subHeading": "Säsong 1 — Avsnitt 17: Lära från en mästare",
              "startTime": "13:55",
              "start": "2023-11-16T13:55:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1409175-017A",
              "svtId": "jd6Xmog",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "17. Lära från en mästare",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jd6Xmog/dromslottet-tro-hopp-och-murbruk/17-lara-fran-en-mastare",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 17",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jd6Xmog",
                "parent": {
                  "svtId": "8rQ9Enr",
                  "urls": {
                    "svtplay": "/dromslottet-tro-hopp-och-murbruk",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696412706,
                      "id": "50705804",
                      "description": "Dick och Angel i Drömslottet: Tro, hopp och murbruk.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-13T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Även om Merdø av många anses vara världens vackraste ö bor bara en endaste man där året runt. Han heter Trond Helle. I år blir dock ön avgörande för fler än bara honom. Del 5 av 5.",
              "name": "Där ingen skulle tro att någon kunde bo",
              "subHeading": "Trond Helle på Merdø",
              "startTime": "14:40",
              "start": "2023-11-16T14:40:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1405224-005A",
              "svtId": "jzXBrPr",
              "rerunDescription": "Från 20/8 i SVT1",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Trond Helle på Merdø",
                "shortDescription": "Norsk serie från 2021. Säsong 18",
                "urls": {
                  "svtplay": "/video/jzXBrPr/dar-ingen-skulle-tro-att-nagon-kunde-bo/trond-helle-pa-merdo",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jzXBrPr",
                "parent": {
                  "svtId": "KQYb2oy",
                  "urls": {
                    "svtplay": "/dar-ingen-skulle-tro-att-nagon-kunde-bo",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135976,
                      "id": "31160050",
                      "description": "Där ingen skulle tro att någon kunde bo.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-08-20T02:00:00+02:00",
                "validToFormatted": "Fre 15 nov 2024 (12 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Stjärnkocken Micke Björklund gästar Strömsö och försöker göra strömmingen så attraktiv som möjligt för en ung generation. En kritisk ungdomspanel får säga sitt. Jim gör en visselpipa och Camilla fiskskulpturer. Del 12 av 16.",
              "name": "Strömsö",
              "subHeading": "",
              "startTime": "15:20",
              "start": "2023-11-16T15:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408466-012A",
              "svtId": "8z2QZ6o",
              "rerunDescription": "Från 15/11 i SVT1. Även i SVT24 18/11 och SVT1 19/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Länge leve strömmingen!",
                "shortDescription": "Finlandssvenskt livsstilsmagasin från 2023",
                "urls": {
                  "svtplay": "/video/8z2QZ6o/stromso/lange-leve-strommingen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8z2QZ6o",
                "parent": {
                  "svtId": "KqvMkMk",
                  "urls": {
                    "svtplay": "/stromso",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135843,
                      "id": "32413779",
                      "description": "Elin Skagersten-Ström och Nicke Aldén i Strömsö.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-15T02:00:00+01:00",
                "validToFormatted": "Tor 12 dec 2024 (1 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "En dramakomedi om de medelålders kvinnorna Elisabet och Gudrun som möts av en slump och blir goda vänner. De är båda frånskilda, med vuxna barn, och nu är det meningen att de ska gå vidare i livet. I sin jakt på kärlek testar de båda lyckan på klubben Heartbreak Hotel, där det blir mycket dans och många starka drinkar. Regi: Colin Nutley. I rollerna: Helena Bergström, Maria Lundqvist, Claes Månsson, Johan Rabaeus, Erica Braun, Marie Robertson m.fl.",
              "name": "Heartbreak Hotel",
              "subHeading": "Svensk långfilm från 2006",
              "startTime": "15:50",
              "start": "2023-11-16T15:50:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1397502-001A",
              "svtId": "eopRpzy",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Heartbreak Hotel",
                "shortDescription": "Svensk långfilm från 2006",
                "urls": {
                  "svtplay": "/video/eopRpzy/heartbreak-hotel",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "eopRpzy",
                "parent": {
                  "svtId": "eDmdbqD",
                  "urls": {
                    "svtplay": "/video/eDmdbqD/heartbreak-hotel",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136157,
                      "id": "29773066",
                      "description": "Elisabeth (Helena Bergström) och Gudrun (Maria Lundqvist). - Heartbreak Hotel",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-10T02:00:00+01:00",
                "validToFormatted": "Sön 10 dec (24 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "I kväll om det hårda trycket på utbildningen av kriminalvårdare i Norrköping. Dessutom om att det väntas bli besparingar på kulturen i många regioner i Sverige. Programledare: Julia Hedlund. Sverige idag samlar och fördjupar lokala nyheter från hela landet.",
              "name": "Sverige idag",
              "subHeading": "",
              "startTime": "17:30",
              "start": "2023-11-16T17:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1409276-064A",
              "svtId": "jmydmaR",
              "rerunDescription": "Även i SVT1 senare i natt",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:30",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jmydmaR/sverige-idag/idag-17-30",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jmydmaR",
                "parent": {
                  "svtId": "e3Bz1MZ",
                  "urls": {
                    "svtplay": "/sverige-idag",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1694160942,
                      "id": "50636340",
                      "description": "Programledarna Helena Wink, Julia Hedlund och Frida Wengberg. - Sverige idag",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:30:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter från Sverige och världen.",
              "name": "Rapport",
              "subHeading": "",
              "startTime": "18:00",
              "start": "2023-11-16T18:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407224-229A",
              "svtId": "epogWbo",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 18:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/epogWbo/rapport/ikvall-18-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "epogWbo",
                "parent": {
                  "svtId": "eoVPJNz",
                  "urls": {
                    "svtplay": "/rapport",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133144,
                      "id": "35812861",
                      "description": "Rapport",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T18:00:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Kulturnyheterna",
              "subHeading": "",
              "startTime": "18:17",
              "start": "2023-11-16T18:17:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407225-194A",
              "svtId": "8oGWwqk",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 18:17",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8oGWwqk/kulturnyheterna/ikvall-18-17",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8oGWwqk",
                "parent": {
                  "svtId": "jXZVggV",
                  "urls": {
                    "svtplay": "/kulturnyheterna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1695739548,
                      "id": "50683944",
                      "description": "Ika Johannesson, Kristofer Lundström, Hannes Fossbo och Rebecca Haim. - Kulturnyheterna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T18:17:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Senaste nytt i sportvärlden.",
              "name": "Sportnytt",
              "subHeading": "",
              "startTime": "18:28",
              "start": "2023-11-16T18:28:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406771-320A",
              "svtId": "Kxp7pXq",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 18:28",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/Kxp7pXq/sportnytt/ikvall-18-28",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "Kxp7pXq",
                "parent": {
                  "svtId": "e3BzXZ2",
                  "urls": {
                    "svtplay": "/sportnytt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135762,
                      "id": "33071930",
                      "description": "Sportnytt",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T18:28:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter från SVT:s lokala redaktioner.",
              "name": "Lokala nyheter",
              "subHeading": "",
              "startTime": "18:33",
              "start": "2023-11-16T18:33:00+01:00",
              "state": "running",
              "progress": 57,
              "id": "1407226-179A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Intervju med skådespelaren Valter Skarsgård, aktuell i rollen som Börje Salming i dramaserien \"Börje - The journey of a legend\". En tittare får en ny stil med hjälpa av stylisten Rufus Kellman och frisören Jana Lynn, dessutom ett resereportage från Spaniens huvudstad Madrid tillsammans med Reyhaneh Rouhani. Programledare: Inger Ljung Olsson.",
              "name": "Go'kväll",
              "subHeading": "",
              "startTime": "18:45",
              "start": "2023-11-16T18:45:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407094-122A",
              "svtId": "ed3EAnx",
              "rerunDescription": "Även i SVT1 senare i natt",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Valter Skarsgård och  Gör om mig",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/ed3EAnx/gokvall/valter-skarsgard-och-gor-om-mig",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "ed3EAnx",
                "parent": {
                  "svtId": "826Ea26",
                  "urls": {
                    "svtplay": "/gokvall",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135768,
                      "id": "33001838",
                      "description": "Inger Ljung Olsson - Go'kväll",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T18:45:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter från Sverige och världen.",
              "name": "Rapport",
              "subHeading": "",
              "startTime": "19:30",
              "start": "2023-11-16T19:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407227-275A",
              "svtId": "jmokypB",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 19:30",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jmokypB/rapport/ikvall-19-30",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jmokypB",
                "parent": {
                  "svtId": "eoVPJNz",
                  "urls": {
                    "svtplay": "/rapport",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133144,
                      "id": "35812861",
                      "description": "Rapport",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T19:30:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter från SVT:s lokala redaktioner.",
              "name": "Lokala nyheter",
              "subHeading": "",
              "startTime": "19:55",
              "start": "2023-11-16T19:55:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407228-275A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "I flera år har Marie drömt om den här dagen - att äntligen få träffa den franska konstnärinnan Pascale Palun. Systrarna besöker hennes sagoliknande universum i centrala Avignon. Det blir ett möte som väcker känslor. Resan fortsätter via Cannes och vidare till Milano. I vintageaffären i Milano kan man både hitta fynd och en egen stil. Det händer något med en när man reser och äntligen får systrarna tid till att prata om det de annars aldrig pratar om. Del 3 av 6.",
              "name": "Husdrömmar Sicilien - vintageresan",
              "subHeading": "Säsong 1 — Avsnitt 3: \"Om jag inte haft dig\"",
              "startTime": "20:00",
              "start": "2023-11-16T20:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406762-003A",
              "svtId": "KD9GmDg",
              "rerunDescription": "Även i SVT1 18/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "3. \"Om jag inte haft dig\"",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KD9GmDg/husdrommar-sicilien-vintageresan/3-om-jag-inte-haft-dig",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 3",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KD9GmDg",
                "parent": {
                  "svtId": "89dBg2w",
                  "urls": {
                    "svtplay": "/husdrommar-sicilien-vintageresan",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696412513,
                      "id": "50705788",
                      "description": "Husdrömmar Sicilien vintageresan - Husdrömmar Sicilien - vintageresan",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Tis 4 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Lisa och Christoffer lämnar både jobb och bostad för att följa sin stora dröm. En italiensk skolbuss ska byggas om till deras nya rullande hem och möjliggöra ett friare liv och en evig sommar. Planen är att bygga om den gamla bussen under en sommar för att till hösten lämna landet mot varmare breddgrader. Del 1 av 3.",
              "name": "Husdrömmar dokumentär: Bussen till friheten",
              "subHeading": "Säsong 1 — Avsnitt 4: Bussen till friheten del 1",
              "startTime": "20:30",
              "start": "2023-11-16T20:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1403537-004A",
              "svtId": "K5RLp9p",
              "rerunDescription": "Även i SVT1 18/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "4. Bussen till friheten del 1",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/K5RLp9p/husdrommar-dokumentar/4-bussen-till-friheten-del-1",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 4",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "K5RLp9p",
                "parent": {
                  "svtId": "jwqkpAn",
                  "urls": {
                    "svtplay": "/husdrommar-dokumentar",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696341983,
                      "id": "50702656",
                      "description": "Husdrömmar dokumentär",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Tis 18 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Branden i Västmanland 2014 var den största enskilda branden i modern tid. Människor drabbades och naturen förändrades men inga djurpopulationer tog skada på allvar. I de våldsamma bränderna i Australien 2020 drabbades tre miljarder djur av lågornas framfart. Anders Lundin besöker koalaforskare och djursjukhus som försöker rädda en hotad art. Del 1 av 4.",
              "name": "I brändernas spår",
              "subHeading": "Säsong 1 — Avsnitt 1: Förgöraren och livgivaren",
              "startTime": "21:00",
              "start": "2023-11-16T21:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407159-001A",
              "svtId": "Kz2NMoM",
              "rerunDescription": "Även i SVT24 17/11, SVT24 18/11, SVT2 19/11, SVT24 19/11 och SVT1 21/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Förgöraren och livgivaren",
                "shortDescription": "Svensk naturserie från 2023",
                "urls": {
                  "svtplay": "/video/Kz2NMoM/i-brandernas-spar/1-forgoraren-och-livgivaren",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "Kz2NMoM",
                "parent": {
                  "svtId": "KXvbyM1",
                  "urls": {
                    "svtplay": "/i-brandernas-spar",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698928891,
                      "id": "50789812",
                      "description": "I brändernas spår. Svensk naturserie från 2023 med Anders Lundin.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter från SVT:s lokala redaktioner.",
              "name": "Lokala nyheter",
              "subHeading": "",
              "startTime": "21:50",
              "start": "2023-11-16T21:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407229-144A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "När alla trodde han skulle återvända till Frölunda så valde Andreas Johnson att skriva långtidskontrakt med Skellefteå där hans bror spelar. Är den förre Toronto-stjärnan lösningen på Skellefteås offensiva problem? Johanna undrar var ligans målskyttar är, Jonas sitter på svaret och riktar också en känga till coacherna i SHL. Dessutom i Overtime: Den målfarligaste NHL-svensken! I studion: Dušan Umicévić, Jonas Andersson & Johanna Dahlén.",
              "name": "Hockeykväll",
              "subHeading": "",
              "startTime": "22:00",
              "start": "2023-11-16T22:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406824-009A",
              "svtId": "8WmD1pR",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 22:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8WmD1pR/hockeykvall/ikvall-22-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8WmD1pR",
                "parent": {
                  "svtId": "jLYywQa",
                  "urls": {
                    "svtplay": "/hockeykvall",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133015,
                      "id": "36665687",
                      "description": "Jonas Andersson, Johanna Dahlén, Dusan Umicevic och Håkan Loob - Hockeykväll",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T22:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Dougs fästmö Stephanie vägrar att flytta in förrän han gjort sig av med sin avlidna frus tillhörigheter. Döstädarna rycker ut för att hjälpa Doug att sortera både sina prylar och sin hjärtesorg. Del 7 av 8: Korvstopp.",
              "name": "Den svenska konsten att döstäda",
              "subHeading": "Säsong 1 — Avsnitt 7: Korvstopp",
              "startTime": "22:30",
              "start": "2023-11-16T22:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1409171-007A",
              "svtId": "KPBrmao",
              "rerunDescription": "Även i SVT1 19/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "7. Korvstopp",
                "shortDescription": "Amerikansk serie från 2023",
                "urls": {
                  "svtplay": "/video/KPBrmao/den-svenska-konsten-att-dostada/7-korvstopp",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 7",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KPBrmao",
                "parent": {
                  "svtId": "86dJ4gq",
                  "urls": {
                    "svtplay": "/den-svenska-konsten-att-dostada",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1694096583,
                      "id": "50633264",
                      "description": "Den svenska konsten att döstäda",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-02T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter från Sverige och världen.",
              "name": "Rapport",
              "subHeading": "",
              "startTime": "23:15",
              "start": "2023-11-16T23:15:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407230-320A",
              "svtId": "j1ad1ok",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 23:15",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/j1ad1ok/rapport/ikvall-23-15",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "j1ad1ok",
                "parent": {
                  "svtId": "eoVPJNz",
                  "urls": {
                    "svtplay": "/rapport",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133144,
                      "id": "35812861",
                      "description": "Rapport",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T23:15:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Den svenska skönhetsmiljardären jagas för Sveriges största skatteskuld. Han verkar ha gjort allt för att dölja sina pengar. Men en läcka från Cypern avslöjar avancerade upplägg som han, ryska oligarker och maffiabossar använder sig av. Kan de nya spåren leda till de gömda pengarna?",
              "name": "Uppdrag granskning",
              "subHeading": "",
              "startTime": "23:20",
              "start": "2023-11-16T23:20:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407519-012A",
              "svtId": null,
              "rerunDescription": "Från 15/11 i SVT1",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "På jakt efter metaller lämnar människorna stenåldern bakom sig och ger sig ut i Europa på långa och riskfyllda resor. Föremålen och kunskaperna de tar med sig hem skapar nya möjligheter för alla som lever i vårt land. Men de leder också till våld och krig. Simon J Berger är seriens berättare. Del 2 av 10.",
              "name": "Historien om Sverige",
              "subHeading": "Säsong 1 — Avsnitt 2: Metallernas tid, ca 1700 f Kr–500 e Kr",
              "startTime": "00:20",
              "start": "2023-11-17T00:20:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1403885-002A",
              "svtId": "jLZ57xy",
              "rerunDescription": "Från 12/11 i SVT1. Även i SVT24 17/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "2. Metallernas tid, ca 1700 f Kr–500 e Kr",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jLZ57xy/historien-om-sverige/2-metallernas-tid-ca-1700-f-kr-500-e-kr",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 2",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jLZ57xy",
                "parent": {
                  "svtId": "82dR3Yx",
                  "urls": {
                    "svtplay": "/historien-om-sverige",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1697546397,
                      "id": "50741552",
                      "description": "Historien om Sverige",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Tis 11 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vilka var de bästa sakerna som kom från romarriket och hur tolkar man egentligen hällristningar som verkar visa forntida nycirkus? Och borde inte bronsåldern döpas om till ullåldern? Gäster är Johan Ling, professor i arkeologi, Jenny Larsson, professor i språk, Helena Victor, arkeolog och Nanna Olasdotter Hallberg, journalist och forntidsentusiast. Programledare är Cecilia Düringer. Del 2 av 10.",
              "name": "Historiskt eftersnack",
              "subHeading": "Säsong 1 — Avsnitt 2: De tänkte också på romarriket",
              "startTime": "01:20",
              "start": "2023-11-17T01:20:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406741-002A",
              "svtId": "jgnGBJR",
              "rerunDescription": "Från 12/11 i SVT1. Även i SVT24 17/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "2. De tänkte också på romarriket",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jgnGBJR/historiskt-eftersnack/2-de-tankte-ocksa-pa-romarriket",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 2",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jgnGBJR",
                "parent": {
                  "svtId": "egWavAV",
                  "urls": {
                    "svtplay": "/historiskt-eftersnack",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698910026,
                      "id": "50788540",
                      "description": "Programledaren Cecilia Düringer följer upp veckans avsnitt av Historien om Sverige och djupdyker i nördiga sidospår och gör oväntade spaningar tillsammans med experter och kulturpersonligheter. - Historiskt eftersnack",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Tis 11 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Linas relation med Rikard intensifieras samtidigt som Petras äktenskap med Dejan surnar. Petra får stöd och tröst av sin nya kollega. Molly och Melvin får veta att deras morfar är sjuk, men Molly har svårt att acceptera omständigheterna. Lina säger ifrån på jobbet och hamnar i konflikt med chefen. I rollerna: Michaela Thorsén, Lennart Jähkel, Ulrika Nilsson, Björn Bengtsson, Mathias Lithner, Minttu Mustakallio, Arvin Kananian, Francisco Sobrado, Sanna Persson Halápi m.fl. Del 3 av 8.",
              "name": "Hålla samman",
              "subHeading": "Säsong 1 — Avsnitt 3",
              "startTime": "01:50",
              "start": "2023-11-17T01:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1404880-003A",
              "svtId": "eWV7Pv3",
              "rerunDescription": "Från 13/11 i SVT1",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Avsnitt 3",
                "shortDescription": "Svensk dramakomediserie från 2023",
                "urls": {
                  "svtplay": "/video/eWV7Pv3/halla-samman/avsnitt-3",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 3",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eWV7Pv3",
                "parent": {
                  "svtId": "8PB2NPq",
                  "urls": {
                    "svtplay": "/halla-samman",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1694785309,
                      "id": "50655468",
                      "description": "Hålla samman",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-03T02:00:00+01:00",
                "validToFormatted": "Lör 15 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "02:20",
              "start": "2023-11-17T02:20:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "noitem-ch-svt1",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "I kväll om det hårda trycket på utbildningen av kriminalvårdare i Norrköping. Dessutom om att det väntas bli besparingar på kulturen i många regioner i Sverige. Programledare: Julia Hedlund. Sverige idag samlar och fördjupar lokala nyheter från hela landet.",
              "name": "Sverige idag",
              "subHeading": "",
              "startTime": "04:45",
              "start": "2023-11-17T04:45:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1409276-064AR",
              "svtId": "jmydmaR",
              "rerunDescription": "Från SVT1 tidigare idag",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:30",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jmydmaR/sverige-idag/idag-17-30",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jmydmaR",
                "parent": {
                  "svtId": "e3Bz1MZ",
                  "urls": {
                    "svtplay": "/sverige-idag",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1694160942,
                      "id": "50636340",
                      "description": "Programledarna Helena Wink, Julia Hedlund och Frida Wengberg. - Sverige idag",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:30:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            }
          ],
          "__typename": "Channel"
        },
        {
          "id": "ch-svt2",
          "name": "SVT 2",
          "schedule": [
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "05:00",
              "start": "2023-11-16T05:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "noitem-ch-svt2",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hur kunde ett gatubarn från Londons slum bli världens berömdaste man inom loppet av några få år? Filmen beskriver hur Chaplin startade sin karriär i Hollywood och utvecklade stumfilmens enaktare till komiska pärlor som lever än i våra dagar. Med massor av nyrestaurerade klipp ur Chaplins filmer och glimtar från hans liv bakom kameran.",
              "name": "Komiska genier - Chaplin",
              "subHeading": "Fransk dokumentär från 2014",
              "startTime": "07:30",
              "start": "2023-11-16T07:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406557-001A",
              "svtId": "jw2n67Z",
              "rerunDescription": "Från 11/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Komiska genier - Chaplin",
                "shortDescription": "Fransk dokumentär från 2014",
                "urls": {
                  "svtplay": "/video/jw2n67Z/komiska-genier-chaplin",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "jw2n67Z",
                "parent": {
                  "svtId": "KBM7Pvq",
                  "urls": {
                    "svtplay": "/video/KBM7Pvq/komiska-genier-chaplin",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1699349277,
                      "id": "50801696",
                      "description": "Charlie Chaplin - Komiska genier - Chaplin",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-10T02:00:00+01:00",
                "validToFormatted": "Ons 31 dec 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Frank har ägnat många timmar åt att samla in värphönsen och deras ägg för att ha dem samlade i en kycklingcentral. Men några gick honom förbi och nu måste han in i ladan på jakt efter dem som har kläckts utanför det trygga hägnet. Sedan ska boningshusets lerväggar förstärkas för att bättre klara stötar. Till sist ska det skäras grödor, bland annat gröna linser som är nya för året. Ett självförsörjande lantbruk som drivs på hållbart vis och med gamla metoder är drömmen för Frank och hans familj på Kastanjegården. Del 5 av 8.",
              "name": "Hundra procent bonde",
              "subHeading": "Säsong 29 — Avsnitt 5: Kycklingjakt",
              "startTime": "08:30",
              "start": "2023-11-16T08:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408445-005A",
              "svtId": "KvXNzEW",
              "rerunDescription": "Från 10/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "5. Kycklingjakt",
                "shortDescription": "Dansk serie från 2022. Säsong 29",
                "urls": {
                  "svtplay": "/video/KvXNzEW/hundra-procent-bonde/5-kycklingjakt",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 29 — Avsnitt 5",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KvXNzEW",
                "parent": {
                  "svtId": "KQYbQBn",
                  "urls": {
                    "svtplay": "/hundra-procent-bonde",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1694096160,
                      "id": "50633248",
                      "description": "Hundra procent bonde. Dansk serie från 2022, säsong 29.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-10T02:00:00+01:00",
                "validToFormatted": "Lör 9 nov 2024 (12 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "09:00: Vem ska betala priset för omsorgen om äldre? En åldrande befolkning leder till ökat tryck på välfärden i ett läge där finansieringen redan är hårt pressad. Hur ska de anhörigas obetalda omsorgsarbete finansieras? Äldre- och socialförsäkringsminister Anna Tenje (M) kommenterar en ny rapport. Arrangör: SNS. Från 24/10. * \n\n10:15: Gängkriminalitet i EU. Gängvåldet i Sverige har en tydlig koppling till Europa och behöver bekämpas på EU-nivå. EU-parlamentarikerna Heléne Fritzon (S) och Sara Skyttedal (KD), om hur kampen mot gängen ska lyckas. Programledare: Marcus Carlehed. Från 7/11. * \n\n10:35: Sällsynta metaller - stormaktspolitik och säkerhetsutmaningar. Samhällets tekniska och gröna utveckling är beroende av sällsynta metaller. Vilka strategier har stormakter som Kina, USA och EU för att säkra tillgången till dessa metaller? Och hur är situationen för människor som utvinner dem? Arrangör: Folk och Försvar. Från 2/10. ",
              "name": "SVT Forum",
              "subHeading": "",
              "startTime": "09:00",
              "start": "2023-11-16T09:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407221-189A",
              "svtId": "jawo15p",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 09:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jawo15p/forum/idag-09-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jawo15p",
                "parent": {
                  "svtId": "ewkEYGw",
                  "urls": {
                    "svtplay": "/forum",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136521,
                      "id": "28102132",
                      "description": "Forum",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T09:00:00+01:00",
                "validToFormatted": "Ons 15 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Rapport",
              "subHeading": "",
              "startTime": "12:00",
              "start": "2023-11-16T12:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407236-179A",
              "svtId": "KB3NYqB",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 12:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KB3NYqB/rapport/idag-12-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KB3NYqB",
                "parent": {
                  "svtId": "eoVPJNz",
                  "urls": {
                    "svtplay": "/rapport",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133144,
                      "id": "35812861",
                      "description": "Rapport",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T12:00:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "12:03: Extremism inom försvaret och studieförbunden. DIREKT. Interpellationsdebatter i riksdagen. Ministrar svarar på riksdagsledamöternas i förväg inlämnade, skriftliga frågor. * \n\n13:30: Forum: Politik och samhälle. * \n\n14:00: Statsministerns frågestund. DIREKT. Statsminister Ulf Kristersson (M) svarar på riksdagsledamöternas frågor direkt i kammaren. * \n\n15:10: Skogen i EU - vad behöver förändras? Från det nationella skogsprogrammets årskonferens 3/10. Huvudarrangör: Skogsvårdsstyrelsen. ",
              "name": "SVT Forum",
              "subHeading": "",
              "startTime": "12:03",
              "start": "2023-11-16T12:03:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407221-189A2",
              "svtId": "jawo15p",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 09:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jawo15p/forum/idag-09-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jawo15p",
                "parent": {
                  "svtId": "ewkEYGw",
                  "urls": {
                    "svtplay": "/forum",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136521,
                      "id": "28102132",
                      "description": "Forum",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T09:00:00+01:00",
                "validToFormatted": "Ons 15 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Rapport",
              "subHeading": "",
              "startTime": "16:00",
              "start": "2023-11-16T16:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407237-320A",
              "svtId": "jA3NgmG",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 16:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jA3NgmG/rapport/idag-16-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jA3NgmG",
                "parent": {
                  "svtId": "eoVPJNz",
                  "urls": {
                    "svtplay": "/rapport",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133144,
                      "id": "35812861",
                      "description": "Rapport",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T16:00:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hänt i dag.",
              "name": "SVT Forum",
              "subHeading": "",
              "startTime": "16:05",
              "start": "2023-11-16T16:05:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407221-189A3",
              "svtId": "jawo15p",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 09:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jawo15p/forum/idag-09-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jawo15p",
                "parent": {
                  "svtId": "ewkEYGw",
                  "urls": {
                    "svtplay": "/forum",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136521,
                      "id": "28102132",
                      "description": "Forum",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T09:00:00+01:00",
                "validToFormatted": "Ons 15 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Marjo tränar offpiståkning med Pinja. Yifeng och Juuso testar en ny fiskemetod, Manu lär sig om rävjakt med fotsnara. Följ med på vildmarksäventyr i den finska naturen! Del 9 av 11.",
              "name": "Jakttid",
              "subHeading": "I dimman",
              "startTime": "16:15",
              "start": "2023-11-16T16:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1405241-010A",
              "svtId": "jJ3Qq6n",
              "rerunDescription": "Från 12/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "I dimman",
                "shortDescription": "Finsk jaktserie från 2021. Säsong 6",
                "urls": {
                  "svtplay": "/video/jJ3Qq6n/jakttid/i-dimman",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jJ3Qq6n",
                "parent": {
                  "svtId": "eWkGRGN",
                  "urls": {
                    "svtplay": "/jakttid",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132964,
                      "id": "36810763",
                      "description": "Riitta Ahmasalo, Manu Soininmäki, Äijä-koira, Aki Huhtanen, Antti Kokko, Sukka-koira och Riikka Aaltonen. - Jakttid",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Tis 26 nov 2024 (1 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hemmamamman Linda är i fängelse för första gången och hon saknar sitt barn. Australiensiska Janes dom överklagas i hovrätten. Tiina inser att drogerna redan har kostat henne för mycket och Lilja flyttar till en annorlunda institution. Följ kvinnorna på kåken i Tavastehus. Del 5 av 8.",
              "name": "Kvinnor på kåken",
              "subHeading": "Säsong 1 — Avsnitt 5: Jag är ingen brottsling",
              "startTime": "16:45",
              "start": "2023-11-16T16:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406142-005A",
              "svtId": "KnbYZGJ",
              "rerunDescription": "Från 12/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "5. Jag är ingen brottsling",
                "shortDescription": "Finsk dokumentärserie från 2022",
                "urls": {
                  "svtplay": "/video/KnbYZGJ/kvinnor-pa-kaken/5-jag-ar-ingen-brottsling",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 5",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KnbYZGJ",
                "parent": {
                  "svtId": "jqPvLLQ",
                  "urls": {
                    "svtplay": "/kvinnor-pa-kaken",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696584296,
                      "id": "50713124",
                      "description": "Kvinnor på kåken, finsk dokumentärserie från 2022.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Mån 2 dec 2024 (1 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter på lätt svenska är ett program för alla som behöver nyheter på ett enklare språk.",
              "name": "Nyheter på lätt svenska",
              "subHeading": "",
              "startTime": "17:15",
              "start": "2023-11-16T17:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407244-179A",
              "svtId": "8oGWDPA",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:15",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8oGWDPA/nyheter-pa-latt-svenska/idag-17-15",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8oGWDPA",
                "parent": {
                  "svtId": "jpmQZ6w",
                  "urls": {
                    "svtplay": "/nyheter-pa-latt-svenska",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135540,
                      "id": "34680387",
                      "description": "Nyheter på lätt svenska",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:15:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter på teckenspråk.",
              "name": "Nyhetstecken",
              "subHeading": "",
              "startTime": "17:20",
              "start": "2023-11-16T17:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407238-229A",
              "svtId": "KnVaXrm",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:20",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KnVaXrm/nyhetstecken/idag-17-20",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KnVaXrm",
                "parent": {
                  "svtId": "egAGWQg",
                  "urls": {
                    "svtplay": "/nyhetstecken",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137384,
                      "id": "19594756",
                      "description": "Nyhetstecken",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:20:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Samiskspråkiga nyheter.",
              "name": "Oddasat",
              "subHeading": "",
              "startTime": "17:30",
              "start": "2023-11-16T17:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407239-184A",
              "svtId": "KroVMwY",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:30",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KroVMwY/oddasat/idag-17-30",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KroVMwY",
                "parent": {
                  "svtId": "826EVoV",
                  "urls": {
                    "svtplay": "/oddasat",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137993,
                      "id": "7348238",
                      "description": "Oddasat logga",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:30:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Finskspråkiga nyheter.",
              "name": "Uutiset",
              "subHeading": "",
              "startTime": "17:45",
              "start": "2023-11-16T17:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407240-184A",
              "svtId": "KroV6RD",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:45",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KroV6RD/uutiset/idag-17-45",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KroV6RD",
                "parent": {
                  "svtId": "KAkPnp5",
                  "urls": {
                    "svtplay": "/uutiset",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135468,
                      "id": "35155307",
                      "description": "Uutiset",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:45:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Dokumentär om David Grossman, en av Israels mest hyllade författare. I filmen berättar han om sin uppväxt i en arbetarklassfamilj i Jerusalem, om sin starka koppling till den judiska historien och Förintelsen och inte minst om hur sonen som inkallad i den israeliska armén dog i södra Libanon. Här talar den sörjande fadern om sin tidiga medvetenhet kring döden, som både plågade honom och drev honom till att skriva. En film om att slitas mellan ljus och mörker, familjelivet och skrivandets ensamhet. Israelisk dokumentärfilm av Adi Arbel från 2021.",
              "name": "Grossman - författare mellan krig och fred",
              "subHeading": "K special",
              "startTime": "17:55",
              "start": "2023-11-16T17:55:00+01:00",
              "state": "running",
              "progress": 82,
              "id": "1408273-001A",
              "svtId": "Kyp5pWa",
              "rerunDescription": "Från 10/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Grossman - författare mellan krig och fred",
                "shortDescription": "K special",
                "urls": {
                  "svtplay": "/video/Kyp5pWa/grossman-forfattare-mellan-krig-och-fred",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "Kyp5pWa",
                "parent": {
                  "svtId": "KBM7gJB",
                  "urls": {
                    "svtplay": "/video/KBM7gJB/grossman-forfattare-mellan-krig-och-fred",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698073009,
                      "id": "50758288",
                      "description": "Grossman - författare mellan krig och fred",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-05T02:00:00+01:00",
                "validToFormatted": "Tor 13 aug 2026 (3 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Musikjournalisten Ditte Hammar gräver i den svenska musikens historia. I serien får vi höra om bronslurar, harpospelande vikingahövdingar, snapsvisor, förbjudna trummor och musikaliska spioner. Del 1 av 10: Klingande skelett och transdans. Hur uppstod musiken?",
              "name": "Sveriges musikhistoria",
              "subHeading": "Säsong 1 — Avsnitt 1: Klingande skelett och transdans",
              "startTime": "18:50",
              "start": "2023-11-16T18:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407958-001A",
              "svtId": "jG3WyqD",
              "rerunDescription": "Från 11/11 i SVT2. Även i SVT24 6/12 och SVT1 10/12",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Klingande skelett och transdans",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jG3WyqD/sveriges-musikhistoria/1-klingande-skelett-och-transdans",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jG3WyqD",
                "parent": {
                  "svtId": "eWmbvr3",
                  "urls": {
                    "svtplay": "/sveriges-musikhistoria",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698054346,
                      "id": "50757748",
                      "description": "Sveriges musikhistoria. Musikjournalisten Ditte Hammar gräver i den svenska musikens historia. I serien får vi höra om bronslurar, harpospelande vikingahövdingar, snapsvisor, förbjudna trummor och musikaliska spioner.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-05T02:00:00+01:00",
                "validToFormatted": "Fre 9 aug 2024 (9 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Genomgår Vänsterpartiet lite av en makeover just nu? Enligt partisekreterare Aron Etzler ska vänsterns nya partiprogram göra partiet tydligare och visionerna mindre \"konstiga\". Partiet ska inte längre verka för ett utträde ur EU och ordet socialist nämns färre gånger i det nya partiprogrammet. Och det är bäddat för trubbel - redan nu har intern kritik väckts. Politikbyrån undrar om Vänsterpartiet genomgår en facelift eller en helrenovering?  I studion: Mats Knutson, inrikespolitisk kommentator, SVT, Lova Olsson, politikreporter Sveriges radio, och Fredrik Björkman, politikreporter Aftonbladet. Programledare: Aida Pourshahidi.",
              "name": "Politikbyrån",
              "subHeading": "Minsta möjliga Marx",
              "startTime": "19:00",
              "start": "2023-11-16T19:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1409105-013A",
              "svtId": "8EqERrx",
              "rerunDescription": "Från 15/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Minsta möjliga Marx",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8EqERrx/politikbyran/minsta-mojliga-marx",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8EqERrx",
                "parent": {
                  "svtId": "e7ovrxY",
                  "urls": {
                    "svtplay": "/politikbyran",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686131923,
                      "id": "38205847",
                      "description": "Programledare är Aida Pourshahidi. - Politikbyrån",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-15T19:30:00+01:00",
                "validToFormatted": "Mån 20 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tre antikexperter åker land och rike runt i jakten på gömda och glömda skatter. Jan ska värdera dödsboet efter en jägare och Jonna ställer ut på antikmarknad, men faller själv för frestelsen att handla. Del 6 av 15.",
              "name": "Skattjägarna",
              "subHeading": "",
              "startTime": "19:30",
              "start": "2023-11-16T19:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1186201-006A",
              "svtId": "e7z73LG",
              "rerunDescription": "Tidigare sänt 2011. Även i SVT2 senare i natt",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Mån 13 nov 02:00",
                "shortDescription": "Dansk programserie från 2011",
                "urls": {
                  "svtplay": "/video/e7z73LG/skattjagarna/man-13-nov-02-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e7z73LG",
                "parent": {
                  "svtId": "egAGXgY",
                  "urls": {
                    "svtplay": "/skattjagarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686129683,
                      "id": "39988326",
                      "description": "Två personer står i en antikhandel. En av dem kollar på en vas genom förstoringsglas. - Skattjägarna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-13T02:00:00+01:00",
                "validToFormatted": "Tis 12 nov 2024 (12 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Efter att ha blivit utkastad av sin adoptivfar beger sig Tom mot London. Natten före sitt bröllop rymmer Sophia för att leta upp Tom, men får sedan höra att Tom spenderat natten med Mrs Waters. Med brustet hjärta beger sig Sophia till London i jakten på ett nytt liv. Baserad på Henry Fieldings roman Tom Jones - berättelsen om ett hittebarn. I rollerna: Solly McLeod, Sophie Wilde, Hannah Waddingham, Pearl Mackie, Daniel Rigby m.fl. Del 2 av 4.",
              "name": "Den charmerande Tom Jones",
              "subHeading": "Säsong 1 — Avsnitt 2",
              "startTime": "20:00",
              "start": "2023-11-16T20:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408382-002A",
              "svtId": "jgnwJAG",
              "rerunDescription": "Även i SVT2 18/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Avsnitt 2",
                "shortDescription": "Brittisk dramaserie från 2023",
                "urls": {
                  "svtplay": "/video/jgnwJAG/den-charmerande-tom-jones/avsnitt-2",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 2",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jgnwJAG",
                "parent": {
                  "svtId": "8WAy5V4",
                  "urls": {
                    "svtplay": "/den-charmerande-tom-jones",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686130052,
                      "id": "39526339",
                      "description": "Den charmerande Tom Jones. Brittisk dramaserie från 2023.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-05-03T02:00:00+02:00",
                "validToFormatted": "Tor 1 feb 2024 (3 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "En snygging med en god portion humor, i filmer som Deadpool och Green Lantern.",
              "name": "I studion: Ryan Reynolds",
              "subHeading": "Säsong 1 — Avsnitt 8: Ryan Reynolds",
              "startTime": "20:55",
              "start": "2023-11-16T20:55:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1402788-008A",
              "svtId": "e3vGDmV",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "8. Ryan Reynolds",
                "shortDescription": "Amerikansk kortfilmsserie från 2021",
                "urls": {
                  "svtplay": "/video/e3vGDmV/i-studion/8-ryan-reynolds",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 8",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e3vGDmV",
                "parent": {
                  "svtId": "jzXPLME",
                  "urls": {
                    "svtplay": "/i-studion",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132965,
                      "id": "36810587",
                      "description": "Christoper Nolan, regissören bakom Inception, Tenet och Batman. - I studion:",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2022-06-19T13:35:00+02:00",
                "validToFormatted": "Fre 31 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "SVT:s fördjupande nyhetsprogram.",
              "name": "Aktuellt",
              "subHeading": "",
              "startTime": "21:00",
              "start": "2023-11-16T21:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407245-148A",
              "svtId": "KZAqJbo",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 21:00",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KZAqJbo/aktuellt/ikvall-21-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KZAqJbo",
                "parent": {
                  "svtId": "jpmQ9xw",
                  "urls": {
                    "svtplay": "/aktuellt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133144,
                      "id": "35812489",
                      "description": "Jon Nilsson, Nike Nylander, Cecilia Gralde och Katia Elliott. - Aktuellt",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T21:00:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Kulturnyheterna",
              "subHeading": "",
              "startTime": "21:38",
              "start": "2023-11-16T21:38:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407246-152A",
              "svtId": "KB3Nq4D",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 21:38",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KB3Nq4D/kulturnyheterna/ikvall-21-38",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KB3Nq4D",
                "parent": {
                  "svtId": "jXZVggV",
                  "urls": {
                    "svtplay": "/kulturnyheterna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1695739548,
                      "id": "50683944",
                      "description": "Ika Johannesson, Kristofer Lundström, Hannes Fossbo och Rebecca Haim. - Kulturnyheterna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T21:38:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "De senaste sportnyheterna, analyserna och fördjupningarna.",
              "name": "Sportnytt",
              "subHeading": "",
              "startTime": "21:46",
              "start": "2023-11-16T21:46:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406773-229A",
              "svtId": "KD9GWx4",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 21:46",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KD9GWx4/sportnytt/ikvall-21-46",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KD9GWx4",
                "parent": {
                  "svtId": "e3BzXZ2",
                  "urls": {
                    "svtplay": "/sportnytt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135762,
                      "id": "33071930",
                      "description": "Sportnytt",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T21:46:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Gäst i programmet är Mikael Damberg, socialdemokraternas ekonomisk-politiske talesperson. Programledare: Anders Holmberg.",
              "name": "30 minuter",
              "subHeading": "Mikael Damberg",
              "startTime": "22:00",
              "start": "2023-11-16T22:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408849-013A",
              "svtId": "ey2A32w",
              "rerunDescription": "Även i SVT2 17/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Mikael Damberg",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/ey2A32w/30-minuter/mikael-damberg",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "ey2A32w",
                "parent": {
                  "svtId": "86gk7yr",
                  "urls": {
                    "svtplay": "/30-minuter",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1691660739,
                      "id": "50545356",
                      "description": "30 minuter med Anders Holmberg.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T19:30:00+01:00",
                "validToFormatted": "Lör 8 mar 2025 (1 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hade det inte varit för Kaisa Vilhuinen hade mycket av kunskaperna om den skogsfinska kulturen i Sverige riskerat att falla i glömska. I en tid då moderniteten svepte undan mycket av gammal livsföring, var Kaisa en av få som förvaltade kunskaperna om både språk, traditioner och inte minst besvärjelser. Del 2 av 2.",
              "name": "Kvinnorna som förändrade historien - finska",
              "subHeading": "Kaisa Vilhuinen",
              "startTime": "22:30",
              "start": "2023-11-16T22:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406221-002A",
              "svtId": "epdgRpW",
              "rerunDescription": "Även i SVT2 18/11 och SVT2 22/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Kaisa Vilhuinen",
                "shortDescription": "Dokumentärserie från 2023",
                "urls": {
                  "svtplay": "/video/epdgRpW/kvinnorna-som-forandrade-historien-1/kaisa-vilhuinen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "epdgRpW",
                "parent": {
                  "svtId": "8qPppQA",
                  "urls": {
                    "svtplay": "/kvinnorna-som-forandrade-historien-1",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1697700372,
                      "id": "50747984",
                      "description": "Kvinnorna som förändrade historien",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-09T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Laura och Ida åker till Tokyo och träffar en massa trevliga personer i stadsdelen Shibuya. Många tycker att de finska och japanska språken liknar varandra. Men vad tycker japanerna själva? Del 2 av 6.",
              "name": "Go cosplay - Goes to Japan",
              "subHeading": "Dags att åka till Japan!",
              "startTime": "23:00",
              "start": "2023-11-16T23:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406984-002A",
              "svtId": "KxpZVYq",
              "rerunDescription": "Även i SVT2 19/11 och SVT2 22/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Dags att åka till Japan!",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KxpZVYq/go-cosplay-goes-to-japan/dags-att-aka-till-japan",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KxpZVYq",
                "parent": {
                  "svtId": "eQr3zEm",
                  "urls": {
                    "svtplay": "/go-cosplay-goes-to-japan",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1699008833,
                      "id": "50793556",
                      "description": "Go cosplay - Goes to Japan",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Li Fangwei har kallats världens farligaste vapenhandlare. Trots att han står överst på FBI:s lista över efterlysta brottslingar är det få som vet vem han är i verkligheten, eller ens hur han ser ut. \nTysk dokumentär från 2023.",
              "name": "Dokument utifrån: Handelsresande i död",
              "subHeading": "Dokument utifrån",
              "startTime": "23:15",
              "start": "2023-11-16T23:15:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408413-001A",
              "svtId": "eZAndLq",
              "rerunDescription": "Från 12/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Handelsresande i död",
                "shortDescription": "Dokument utifrån",
                "urls": {
                  "svtplay": "/video/eZAndLq/dokument-utifran-handelsresande-i-dod",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "eZAndLq",
                "parent": {
                  "svtId": "Kv1YzgV",
                  "urls": {
                    "svtplay": "/video/Kv1YzgV/dokument-utifran-handelsresande-i-dod",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1695213403,
                      "id": "50667408",
                      "description": "Li Fangwei har kallats världens farligaste vapenhandlare. Trots att han står överst på FBI:s lista över efterlysta brottslingar är det få som vet vem han är i verkligheten, eller ens hur han ser ut. - Handelsresande i död",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Tis 11 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Under bronsåldern domineras vår värld av tre blomstrande civilisationer. Men inom loppet av bara 50 år kollapsar de alla, en efter en. Hur är det möjligt? Följ med och nysta i den stora gåtan om bronsålderns kollaps.",
              "name": "Världens historia: Bronsålderns kollaps",
              "subHeading": "Australisk dokumentär",
              "startTime": "00:45",
              "start": "2023-11-17T00:45:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408570-001A",
              "svtId": "KMVxoG3",
              "rerunDescription": "Från 14/11 i SVT2",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Bronsålderns kollaps",
                "shortDescription": "Australisk dokumentär",
                "urls": {
                  "svtplay": "/video/KMVxoG3/varldens-historia-bronsalderns-kollaps",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "KMVxoG3",
                "parent": {
                  "svtId": "jQ72vA2",
                  "urls": {
                    "svtplay": "/video/jQ72vA2/varldens-historia-bronsalderns-kollaps",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696253044,
                      "id": "50699216",
                      "description": "Världens historia: Bronsålderns kollaps",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-14T02:00:00+01:00",
                "validToFormatted": "Tor 13 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tre antikexperter åker land och rike runt i jakten på gömda och glömda skatter. Jan ska värdera dödsboet efter en jägare och Jonna ställer ut på antikmarknad, men faller själv för frestelsen att handla. Del 6 av 15.",
              "name": "Skattjägarna",
              "subHeading": "",
              "startTime": "01:40",
              "start": "2023-11-17T01:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1186201-006A",
              "svtId": "e7z73LG",
              "rerunDescription": "Från SVT2 tidigare idag",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Mån 13 nov 02:00",
                "shortDescription": "Dansk programserie från 2011",
                "urls": {
                  "svtplay": "/video/e7z73LG/skattjagarna/man-13-nov-02-00",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e7z73LG",
                "parent": {
                  "svtId": "egAGXgY",
                  "urls": {
                    "svtplay": "/skattjagarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686129683,
                      "id": "39988326",
                      "description": "Två personer står i en antikhandel. En av dem kollar på en vas genom förstoringsglas. - Skattjägarna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-13T02:00:00+01:00",
                "validToFormatted": "Tis 12 nov 2024 (12 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "De senaste sportnyheterna, analyserna och fördjupningarna.",
              "name": "Sportnytt",
              "subHeading": "",
              "startTime": "02:10",
              "start": "2023-11-17T02:10:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406773-229AR",
              "svtId": "KD9GWx4",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 21:46",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KD9GWx4/sportnytt/ikvall-21-46",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KD9GWx4",
                "parent": {
                  "svtId": "e3BzXZ2",
                  "urls": {
                    "svtplay": "/sportnytt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135762,
                      "id": "33071930",
                      "description": "Sportnytt",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T21:46:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Nyheter på teckenspråk.",
              "name": "Nyhetstecken",
              "subHeading": "",
              "startTime": "02:25",
              "start": "2023-11-17T02:25:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407238-229AR",
              "svtId": "KnVaXrm",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Idag 17:20",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KnVaXrm/nyhetstecken/idag-17-20",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KnVaXrm",
                "parent": {
                  "svtId": "egAGWQg",
                  "urls": {
                    "svtplay": "/nyhetstecken",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137384,
                      "id": "19594756",
                      "description": "Nyhetstecken",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T17:20:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "02:35",
              "start": "2023-11-17T02:35:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "noitem-ch-svt2",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            }
          ],
          "__typename": "Channel"
        },
        {
          "id": "ch-barnkanalen",
          "name": "SVT Barn",
          "schedule": [
            {
              "descriptionRaw": "Vi möter Daisy, Igglepiggle, Makka Pakka och några till. De bor i en vacker trädgård och är med om många  upptäckter och äventyr.",
              "name": "I drömmarnas trädgård",
              "subHeading": "Igglepiggle hälsar på",
              "startTime": "05:00",
              "start": "2023-11-16T05:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1128488-049A",
              "svtId": "jv16ND1",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Igglepiggle hälsar på",
                "shortDescription": "BBC-serie för de allra minsta",
                "urls": {
                  "svtplay": "/video/jv16ND1/i-drommarnas-tradgard/igglepiggle-halsar-pa",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jv16ND1",
                "parent": {
                  "svtId": "jx3zWzX",
                  "urls": {
                    "svtplay": "/i-drommarnas-tradgard",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686131227,
                      "id": "38705431",
                      "description": "I drömmarnas trädgård",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T05:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tecknad brittisk serie om en nyfiken och vetgirig prinsessa. Om den lilla prinsessan inte får som hon vill blir hon arg.",
              "name": "Lilla prinsessan",
              "subHeading": "Avsnitt 4",
              "startTime": "05:25",
              "start": "2023-11-16T05:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1124510-004A",
              "svtId": "8p3Vb1r",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Avsnitt 4",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8p3Vb1r/lilla-prinsessan/avsnitt-4",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8p3Vb1r",
                "parent": {
                  "svtId": "8r9wX4Z",
                  "urls": {
                    "svtplay": "/lilla-prinsessan",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137877,
                      "id": "14198420",
                      "description": "Lilla prinsessan",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T05:25:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Kaninfamiljen ger sig ut på äventyr med sin kaninbuss och på vägen träffar de nya vänner.",
              "name": "Modiga kaniner",
              "subHeading": "Avsnitt 3",
              "startTime": "05:40",
              "start": "2023-11-16T05:40:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1401188-003A",
              "svtId": "Kq21VEz",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Avsnitt 3",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/Kq21VEz/modiga-kaniner/avsnitt-3",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "Kq21VEz",
                "parent": {
                  "svtId": "e3vgWd6",
                  "urls": {
                    "svtplay": "/modiga-kaniner",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135848,
                      "id": "32379155",
                      "description": "Modiga kaniner",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-10-17T05:40:00+02:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Det blir mycket lek och bus när lillebror Jack ger sig ut på upptäcktsfärd tillsammans med Hamsterkören och vännerna Gnagis, Cissi och Pingeling.",
              "name": "Min lillebror",
              "subHeading": "Jack surrar",
              "startTime": "05:45",
              "start": "2023-11-16T05:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1384368-017A",
              "svtId": "jXbrPdb",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Jack surrar",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jXbrPdb/min-lillebror/jack-surrar",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jXbrPdb",
                "parent": {
                  "svtId": "8ZkGEyw",
                  "urls": {
                    "svtplay": "/min-lillebror",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137371,
                      "id": "19866162",
                      "description": "Min lillebror",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T05:45:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hej små vänner! Undrar vad Jycke tänkt hitta på med er idag?",
              "name": "Hej Jycke",
              "subHeading": "Var försiktig-märket",
              "startTime": "05:55",
              "start": "2023-11-16T05:55:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1369541-043A",
              "svtId": "K16BAMB",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Var försiktig-märket",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/K16BAMB/hej-jycke/var-forsiktig-market",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "K16BAMB",
                "parent": {
                  "svtId": "KyWZPar",
                  "urls": {
                    "svtplay": "/hej-jycke",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137819,
                      "id": "15526525",
                      "description": "Hej Jycke",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-08-08T05:55:00+02:00",
                "validToFormatted": "Sön 24 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Greta är en glatt grymtande liten gris som bor med mamma Gris, pappa Gris och lillebror Georg. Greta tycker om att spela spel, leka och klä ut sig men det absolut bästa hon vet är att hoppa i lerpölar.",
              "name": "Greta Gris",
              "subHeading": "Den leriga festivalen",
              "startTime": "06:00",
              "start": "2023-11-16T06:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1398315-015A",
              "svtId": "emL27oG",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Den leriga festivalen",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/emL27oG/greta-gris/den-leriga-festivalen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "emL27oG",
                "parent": {
                  "svtId": "e3Bz1w5",
                  "urls": {
                    "svtplay": "/greta-gris",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135467,
                      "id": "35159763",
                      "description": "Greta Gris",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-10-12T07:10:00+02:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "När Kokos hjärta börjar glöda rycker Regnbågsstadens hjälte ut för att hjälpa stora som små.",
              "name": "Regnbågsstadens hjälte",
              "subHeading": "Stor bebis",
              "startTime": "06:05",
              "start": "2023-11-16T06:05:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374614-003A",
              "svtId": "eyBvy1j",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Stor bebis",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eyBvy1j/regnbagsstadens-hjalte/stor-bebis",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eyBvy1j",
                "parent": {
                  "svtId": "e3BzPGL",
                  "urls": {
                    "svtplay": "/regnbagsstadens-hjalte",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133122,
                      "id": "35961853",
                      "description": "Regnbågsstadens hjälte",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-10-18T17:40:00+02:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Charlie är sju år och hänger ofta ute med sin kompis Marv. Han gillar fotboll, raketer och att hitta på historier. Han gillar också sin lillasyster Lola. Det är bara en sak - hon är väldigt  bestämd och envis. Tecknad småbarnsserie från BBC där de svenska rösterna görs av Jakob Bergström, Ella Andersson, Mathilda Smedius, Benjamin Wahlgren m.fl.",
              "name": "Charlie och Lola",
              "subHeading": "",
              "startTime": "06:15",
              "start": "2023-11-16T06:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1116007-021A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hej små vänner! Undrar vad Jycke tänkt hitta på med er idag?",
              "name": "Hej Jycke",
              "subHeading": "Campingmärket",
              "startTime": "06:30",
              "start": "2023-11-16T06:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1377825-017A",
              "svtId": "eaJDN3j",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Campingmärket",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eaJDN3j/hej-jycke/campingmarket",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eaJDN3j",
                "parent": {
                  "svtId": "KyWZPar",
                  "urls": {
                    "svtplay": "/hej-jycke",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137819,
                      "id": "15526525",
                      "description": "Hej Jycke",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-10-25T18:20:00+02:00",
                "validToFormatted": "Lör 2 mar 2024 (3 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Häng med Billy och lillasyster Chrissy på äventyr i Stötfångarstad.",
              "name": "Heja! Heja! Billy Bilsson",
              "subHeading": "",
              "startTime": "06:35",
              "start": "2023-11-16T06:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407457-025A",
              "svtId": "jA3pb5L",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Dags för dejt",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jA3pb5L/heja-heja-billy-bilsson/dags-for-dejt",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jA3pb5L",
                "parent": {
                  "svtId": "KkAv23z",
                  "urls": {
                    "svtplay": "/heja-heja-billy-bilsson",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696949148,
                      "id": "50722440",
                      "description": "Heja! Heja! Billy Bilsson",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T06:35:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "De små, blå smurfarna drar ut på äventyr och tillsammans försöker de lura den elake Gargamel.",
              "name": "Smurfarna",
              "subHeading": "",
              "startTime": "06:45",
              "start": "2023-11-16T06:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408105-006A",
              "svtId": "8Pn4rEJ",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Skruvnyckelsmurfen",
                "shortDescription": "Smurfar på äventyr",
                "urls": {
                  "svtplay": "/video/8Pn4rEJ/smurfarna/skruvnyckelsmurfen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8Pn4rEJ",
                "parent": {
                  "svtId": "ePwN672",
                  "urls": {
                    "svtplay": "/smurfarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1690196898,
                      "id": "50475688",
                      "description": "Smurfarna.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T06:45:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Var än Professor Varg försöker ställa till med oreda finns Superkaninen Simon och hans gäng redo att rycka ut!",
              "name": "Superkaninen Simon",
              "subHeading": "En supertårta för superhjältar",
              "startTime": "06:55",
              "start": "2023-11-16T06:55:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1402723-042A",
              "svtId": "86E64y6",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "En supertårta för superhjältar",
                "shortDescription": "Superkaninen Simon och hans gäng är redo att rycka ut!",
                "urls": {
                  "svtplay": "/video/86E64y6/simon/en-supertarta-for-superhjaltar",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "86E64y6",
                "parent": {
                  "svtId": "KAkP1ZY",
                  "urls": {
                    "svtplay": "/simon",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686130458,
                      "id": "39241535",
                      "description": "Simon",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-05-22T06:00:00+02:00",
                "validToFormatted": "Ons 31 dec 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "När bonden inte ser på blir djuren sina riktiga jag.",
              "name": "Fåret Shaun",
              "subHeading": "Väck ej den katt som sover",
              "startTime": "07:00",
              "start": "2023-11-16T07:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1128051-032A",
              "svtId": "eYwDJNq",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Väck ej den katt som sover",
                "shortDescription": "Leranimerad humorserie",
                "urls": {
                  "svtplay": "/video/eYwDJNq/faret-shaun/vack-ej-den-katt-som-sover",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eYwDJNq",
                "parent": {
                  "svtId": "KqvMn95",
                  "urls": {
                    "svtplay": "/faret-shaun",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1695046902,
                      "id": "50660752",
                      "description": "Fåret Shaun.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T07:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Greta är en glatt grymtande liten gris som bor med mamma Gris, pappa Gris och lillebror Georg. Greta tycker om att spela spel, leka och klä ut sig men det absolut bästa hon vet är att hoppa i lerpölar.",
              "name": "Greta Gris",
              "subHeading": "Rockringar",
              "startTime": "07:10",
              "start": "2023-11-16T07:10:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1404184-014A",
              "svtId": "8ryykRL",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Rockringar",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8ryykRL/greta-gris/rockringar",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8ryykRL",
                "parent": {
                  "svtId": "e3Bz1w5",
                  "urls": {
                    "svtplay": "/greta-gris",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135467,
                      "id": "35159763",
                      "description": "Greta Gris",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T07:10:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Flugsmälla och spindelnät. På sin färd över ängar och hus flyger flugan till slut in till ett kök. Där luktar det gott! Flugan sätter sig i taket innan han flyger ner för att smaka på en potatis. Men han jagas bort och fastnar i ett spindelnät innan han räddas av en omtänksam pojke. Åke Lundqvist gestaltar flugans liv i Bisse Falks berättelse. Del 3 av 4. UR.",
              "name": "En flugas liv",
              "subHeading": "",
              "startTime": "07:15",
              "start": "2023-11-16T07:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407689-003A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009002010003",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Skräcködlor av Britt G Hallqvist. En filmad gestaltning av Britt G Hallqvists Skräcködlor. En dikt som blandar fakta och fantasi: \"För hundrafemtio miljoner år sen var jorden full av stora, hemska djur.\" Del 4 av 10. UR.",
              "name": "En bro av poesi",
              "subHeading": "",
              "startTime": "07:25",
              "start": "2023-11-16T07:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1403818-004A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00005629010004",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Elefanten Mumfie bor på en liten ö där det alltid är nära till vänner och äventyr.",
              "name": "Mumfie",
              "subHeading": "En kunglig favör",
              "startTime": "07:30",
              "start": "2023-11-16T07:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1380431-015A",
              "svtId": "jXkDMbW",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "En kunglig favör",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jXkDMbW/mumfie/en-kunglig-favor",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jXkDMbW",
                "parent": {
                  "svtId": "8MVLL6X",
                  "urls": {
                    "svtplay": "/mumfie",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132763,
                      "id": "37361525",
                      "description": "Mumfie",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T07:30:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "På natten förvandlas Oscar, Amanda och Jens till de tre pyjamashjältarna Kattpojken, Ugglis och Gecko. Tillsammans skyddar de staden mot alla skurkar.",
              "name": "Pyjamashjältarna",
              "subHeading": "Kattpojken och fjärilsbrigaden",
              "startTime": "07:40",
              "start": "2023-11-16T07:40:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1377845-011A",
              "svtId": "jNVZLD4",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Kattpojken och fjärilsbrigaden",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jNVZLD4/pyjamashjaltarna/kattpojken-och-fjarilsbrigaden",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jNVZLD4",
                "parent": {
                  "svtId": "e7PpzZk",
                  "urls": {
                    "svtplay": "/pyjamashjaltarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135431,
                      "id": "35346669",
                      "description": "Pyjamashjältarna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T07:40:00+01:00",
                "validToFormatted": "Ons 29 nov (13 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "De små, blå smurfarna drar ut på äventyr och tillsammans försöker de lura den elake Gargamel.",
              "name": "Smurfarna",
              "subHeading": "",
              "startTime": "07:50",
              "start": "2023-11-16T07:50:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1405873-038A",
              "svtId": "jMPLbqa",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Smurfarnas föreställning - del 2",
                "shortDescription": "Smurfar på äventyr",
                "urls": {
                  "svtplay": "/video/jMPLbqa/smurfarna/smurfarnas-forestallning-del-2",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jMPLbqa",
                "parent": {
                  "svtId": "ePwN672",
                  "urls": {
                    "svtplay": "/smurfarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1690196898,
                      "id": "50475688",
                      "description": "Smurfarna.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-05T17:05:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Slemfisken. Kapten Sjöbjörn och hans besättning av octonauter och vegemaler bor tillsammans på havets botten. Deras uppdrag är att utforska, hjälpa och bevara havens alla varelser.",
              "name": "Octonauterna",
              "subHeading": "Slemfisken",
              "startTime": "08:00",
              "start": "2023-11-16T08:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374573-027A",
              "svtId": "KPED4Lx",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Slemfisken",
                "shortDescription": "Animerat undervattensäventyr",
                "urls": {
                  "svtplay": "/video/KPED4Lx/octonauterna/slemfisken",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KPED4Lx",
                "parent": {
                  "svtId": "jXZVa74",
                  "urls": {
                    "svtplay": "/octonauterna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133122,
                      "id": "35960633",
                      "description": "Octonauterna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T08:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Paddington är en vänlig björn som älskar marmelad. Han har lätt för att hamna i trubbel men gör alltid sitt bästa för att ställa allt tillrätta igen. En animerad serie baserad på böckerna av Michael Bond.",
              "name": "Paddingtons äventyr",
              "subHeading": "Paddington gör en klippbok",
              "startTime": "08:15",
              "start": "2023-11-16T08:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1400523-018A",
              "svtId": "ea4LoV7",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Paddington gör en klippbok",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/ea4LoV7/paddingtons-aventyr/paddington-gor-en-klippbok",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "ea4LoV7",
                "parent": {
                  "svtId": "8Dm7r4y",
                  "urls": {
                    "svtplay": "/paddingtons-aventyr",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136199,
                      "id": "29440264",
                      "description": "Paddingtons äventyr",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2022-09-26T07:55:00+02:00",
                "validToFormatted": "Tor 7 mar 2024 (4 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Maya är inte som alla andra bin - hon bor nämligen inte i en kupa. Ängen är hennes hem och platsen där hon upplever alla sina äventyr med sina ovanliga vänner - och sin bästis Villy, som trots att han också är ett bi egentligen är en riktig latmask.",
              "name": "Biet Maya",
              "subHeading": "Drottning för en dag",
              "startTime": "08:25",
              "start": "2023-11-16T08:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1318830-058A",
              "svtId": "KBMx34q",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Drottning för en dag",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KBMx34q/biet-maya/drottning-for-en-dag",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KBMx34q",
                "parent": {
                  "svtId": "jdwGaxA",
                  "urls": {
                    "svtplay": "/biet-maya",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1694788153,
                      "id": "50655832",
                      "description": "Biet Maya.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T08:25:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Även den som är väldigt liten kan utföra stordåd!",
              "name": "Karl, den pyttelilla riddaren",
              "subHeading": "Prinsessan Gnällspik",
              "startTime": "08:35",
              "start": "2023-11-16T08:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1402480-015A",
              "svtId": "KM3PNq1",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Prinsessan Gnällspik",
                "shortDescription": "De små bästa lösning nå!",
                "urls": {
                  "svtplay": "/video/KM3PNq1/karl-den-pyttelilla-riddaren/prinsessan-gnallspik",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KM3PNq1",
                "parent": {
                  "svtId": "e6YwBzW",
                  "urls": {
                    "svtplay": "/karl-den-pyttelilla-riddaren",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135541,
                      "id": "34676175",
                      "description": "Karl, den pyttelilla riddaren",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T08:35:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tecknad brittisk serie om en nyfiken och vetgirig prinsessa. Om den lilla prinsessan inte får som hon vill blir hon arg.",
              "name": "Lilla prinsessan",
              "subHeading": "Avsnitt 5",
              "startTime": "08:50",
              "start": "2023-11-16T08:50:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1124510-005A",
              "svtId": "eQ7BXBq",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Avsnitt 5",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eQ7BXBq/lilla-prinsessan/avsnitt-5",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eQ7BXBq",
                "parent": {
                  "svtId": "8r9wX4Z",
                  "urls": {
                    "svtplay": "/lilla-prinsessan",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137877,
                      "id": "14198420",
                      "description": "Lilla prinsessan",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T08:50:00+01:00",
                "validToFormatted": "Tor 23 nov 23:59 (7 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tilde är tjejen som tillsammans med sin hund och sina vänner utforskar världen med hjälp av sina teknikprylar.",
              "name": "Tilde",
              "subHeading": "Äggstra spännande",
              "startTime": "09:00",
              "start": "2023-11-16T09:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374703-011A",
              "svtId": "eJZ22dk",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Äggstra spännande",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eJZ22dk/tilde/aggstra-spannande",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eJZ22dk",
                "parent": {
                  "svtId": "826EL3b",
                  "urls": {
                    "svtplay": "/tilde",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137472,
                      "id": "17470006",
                      "description": "Tilde~",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T09:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Ko Ni står på tur att bli byns beskyddare och måste lära sig behärska Safirdrakens krafter. Mäster Li hjälper honom med träningen och tillsammans med Goji och Mei lär sig Ko Ni att skydda byn och stoppa den elake apan Mogo från att stjäla draken och dess krafter.",
              "name": "Safirdrakens krafter",
              "subHeading": "Bollen",
              "startTime": "09:10",
              "start": "2023-11-16T09:10:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1380427-023A",
              "svtId": "eoE3QPN",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Bollen",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eoE3QPN/safirdrakens-krafter/bollen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eoE3QPN",
                "parent": {
                  "svtId": "826EM4o",
                  "urls": {
                    "svtplay": "/safirdrakens-krafter",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137382,
                      "id": "19606300",
                      "description": "Safirdrakens krafter",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T09:10:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Föräldralösa Heidi skickas till bergen för att bo hos sin farfar - en allvarlig, sorgtyngd man. Det blir en tuff omställning för Heidi som dock så småningom visar sig mer än kapabel till att klara sig i sitt nya hem. Heidi får snart flera nya vänner och upplever en rad äventyr tillsammans med dem. Del 15: Klocktornet.",
              "name": "Heidi",
              "subHeading": "Säsong 1 — Avsnitt 15: Klocktornet",
              "startTime": "09:20",
              "start": "2023-11-16T09:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1370098-015A",
              "svtId": "KMqXBvK",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "15. Klocktornet",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KMqXBvK/heidi/15-klocktornet",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 15",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KMqXBvK",
                "parent": {
                  "svtId": "KqvMYdX",
                  "urls": {
                    "svtplay": "/heidi",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135937,
                      "id": "31487334",
                      "description": "Heidi",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T09:20:00+01:00",
                "validToFormatted": "Tis 9 jan 2024 (2 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hamstersyskonen Cleo och Ozzy driver en detektivbyrå under Bernards djuraffär mitt i London. Scotland Yards detektiv McFlare är en trogen kund och tillsammans löser de riktigt kluriga fall.",
              "name": "Cleo löser fallet",
              "subHeading": "Säsong 1 — Avsnitt 9: Bläckfiskfällan",
              "startTime": "09:45",
              "start": "2023-11-16T09:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1398676-007A",
              "svtId": "8zJ7PVb",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "9. Bläckfiskfällan",
                "shortDescription": "Hamster-detektiver löser kluriga fall i London",
                "urls": {
                  "svtplay": "/video/8zJ7PVb/cleo-loser-fallet/9-blackfiskfallan",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 9",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8zJ7PVb",
                "parent": {
                  "svtId": "84oR9mE",
                  "urls": {
                    "svtplay": "/cleo-loser-fallet",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686576978,
                      "id": "50067143",
                      "description": "Cleo löser fallet.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-10-27T18:30:00+02:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Familjen Nekton bor på ubåten Aronnax och älskar äventyr. Ett av havets största mysterier är Lemurien - en försvunnen civilisation - och flera generationer Nekton har sökt efter denna förlorade värld. Nu med all ny teknik, verkar Lemurien närmare än någonsin.",
              "name": "Djupet",
              "subHeading": "Säsong 1 — Avsnitt 2: Späckhuggaren",
              "startTime": "10:05",
              "start": "2023-11-16T10:05:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1368155-002A",
              "svtId": "e52EME9",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "2. Späckhuggaren",
                "shortDescription": "Säsong 1",
                "urls": {
                  "svtplay": "/video/e52EME9/djupet/2-spackhuggaren",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 2",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e52EME9",
                "parent": {
                  "svtId": "8ZkG7z4",
                  "urls": {
                    "svtplay": "/djupet",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1685707687,
                      "id": "50026427",
                      "description": "Djupet.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T10:05:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "12-åriga Dorothy sveps iväg till landet Oz av en magisk dagbok. Det är bara början på hennes fantastiska äventyr i jakten på de goda - och en väg hem.",
              "name": "Vilse i Oz",
              "subHeading": "Säsong 1 — Avsnitt 23: Rädda Cynta",
              "startTime": "10:25",
              "start": "2023-11-16T10:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1393442-023A",
              "svtId": "jqWJ5LJ",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "23. Rädda Cynta",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jqWJ5LJ/vilse-i-oz/23-radda-cynta",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 23",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jqWJ5LJ",
                "parent": {
                  "svtId": "jE4x1n7",
                  "urls": {
                    "svtplay": "/vilse-i-oz",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133122,
                      "id": "35960813",
                      "description": "Vilse i Oz",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2022-09-09T09:25:00+02:00",
                "validToFormatted": "Ons 6 mar 2024 (4 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Buck, Tom och Vicky går på Hawkings-academy, en elitskola för genier inom högteknologi. De har byggt den unika drönaren Dronix. Ingen har sett något liknande förut - förutom Vickys far Charles, innan han försvann spårlöst. Men vad är Dronix hemlighet och varför vill så många komma åt den? Denna gång: Topphemligt.",
              "name": "Team Dronix",
              "subHeading": "Topphemligt",
              "startTime": "10:50",
              "start": "2023-11-16T10:50:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1395933-015A",
              "svtId": "jpAzbV5",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Topphemligt",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jpAzbV5/team-dronix/topphemligt",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jpAzbV5",
                "parent": {
                  "svtId": "jLZYw5x",
                  "urls": {
                    "svtplay": "/team-dronix",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135980,
                      "id": "31127974",
                      "description": "Team Dronix",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-10-21T11:10:00+02:00",
                "validToFormatted": "Sön 31 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Det är år 2162 och rymdäventyren för tvillingarna Adelaide och Sid Nova är precis på väg att börja, på riktigt!",
              "name": "Space Nova",
              "subHeading": "Äventyr i underjorden",
              "startTime": "11:10",
              "start": "2023-11-16T11:10:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1404790-007A",
              "svtId": "jLDp6zy",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Äventyr i underjorden",
                "shortDescription": "Fantastiska rymdäventyr med tvillingarna Nova!",
                "urls": {
                  "svtplay": "/video/jLDp6zy/space-nova/aventyr-i-underjorden",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jLDp6zy",
                "parent": {
                  "svtId": "8YLp3qY",
                  "urls": {
                    "svtplay": "/space-nova",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135464,
                      "id": "35175123",
                      "description": "Space Nova",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-01-08T11:00:00+01:00",
                "validToFormatted": "Fre 22 mar 2024 (4 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Spännande äventyr med fyra små dinosaurier och en jättestor Gigantosaurus.",
              "name": "Gigantosaurus",
              "subHeading": "Följ den som leder",
              "startTime": "11:35",
              "start": "2023-11-16T11:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1397525-037A",
              "svtId": "e4zWgx6",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Följ den som leder",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/e4zWgx6/gigantosaurus/folj-den-som-leder",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e4zWgx6",
                "parent": {
                  "svtId": "jABE5dz",
                  "urls": {
                    "svtplay": "/gigantosaurus",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136477,
                      "id": "28413533",
                      "description": "Gigantosaurus",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T11:35:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Andy och Harriet jobbar med dinosaurieavdelningen på Naturhistoriska museet. Vad Harriet inte vet är att Andy hittat en tidsmaskin på museet som gör att han kan träffa dinosaurier på riktigt!",
              "name": "Andys dinosaurie-äventyr",
              "subHeading": "T-Rex och pimpsten",
              "startTime": "11:45",
              "start": "2023-11-16T11:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1362099-001A",
              "svtId": "jdPzAa1",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "T-Rex och pimpsten",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jdPzAa1/andys-dinosaurieaventyr/t-rex-och-pimpsten",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jdPzAa1",
                "parent": {
                  "svtId": "8r9wQg7",
                  "urls": {
                    "svtplay": "/andys-dinosaurieaventyr",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135484,
                      "id": "35061311",
                      "description": "Andys dinosaurieäventyr",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2022-08-22T11:40:00+02:00",
                "validToFormatted": "Sön 31 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hédi och Marika är kompisar. De har varsin hund och de leker varje dag. Hédi är judisk och Marika är kristen. Men när kriget kommer får Hédi plötsligt inte leka med Marika längre. Hon får inte ens gå ut med sin älskade hund Bodri. Istället hamnar hon i ett läger där hon kämpar för att överleva. UR.",
              "name": "Historien om Bodri - romani chib/kelderash",
              "subHeading": "",
              "startTime": "12:00",
              "start": "2023-11-16T12:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1409274-001A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009632010001",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Leif har fångat spökena i en hundbajspåse och håller en seans med Kicki och Stig för att få kontakt med andra sidan. Del 16 av 24: Abissos.",
              "name": "Mysteriet på Greveholm - Grevens återkomst (romani)",
              "subHeading": "Romani — Avsnitt 16: Abissos",
              "startTime": "12:10",
              "start": "2023-11-16T12:10:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408248-016A",
              "svtId": "82onam6",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "16. Abissos",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/82onam6/mysteriet-pa-greveholm-grevens-aterkomst/16-abissos",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Romani — Avsnitt 16",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "82onam6",
                "parent": {
                  "svtId": "KqvMyAW",
                  "urls": {
                    "svtplay": "/mysteriet-pa-greveholm-grevens-aterkomst",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132475,
                      "id": "37744652",
                      "description": "Mysteriet på Greveholm - Grevens återkomst",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T06:00:00+01:00",
                "validToFormatted": "Ons 22 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Kaninfamiljen ger sig ut på äventyr med sin kaninbuss och på vägen träffar de nya vänner.",
              "name": "Modiga kaniner",
              "subHeading": "Vi delar med oss",
              "startTime": "12:25",
              "start": "2023-11-16T12:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1401186-003A",
              "svtId": "8M3RV4r",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Vi delar med oss",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8M3RV4r/modiga-kaniner/vi-delar-med-oss",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8M3RV4r",
                "parent": {
                  "svtId": "e3vgWd6",
                  "urls": {
                    "svtplay": "/modiga-kaniner",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135848,
                      "id": "32379155",
                      "description": "Modiga kaniner",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-09-17T05:40:00+02:00",
                "validToFormatted": "Mån 15 jan 2024 (2 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vad händer med alla löv i skogen, varför har fåglar fjädrar? Nyfikna kaninen Ellinor går på upptäcktsfärd med sina vänner Ari och Olivia för att hitta svaren.",
              "name": "Ellinor undrar så",
              "subHeading": "Racerfart / Tvilling-getterna",
              "startTime": "12:35",
              "start": "2023-11-16T12:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1400277-009A",
              "svtId": "KyEAp55",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Racerfart / Tvilling-getterna",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KyEAp55/ellinor-undrar-sa/racerfart-tvilling-getterna",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KyEAp55",
                "parent": {
                  "svtId": "jLZR6Dq",
                  "urls": {
                    "svtplay": "/ellinor-undrar-sa",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136030,
                      "id": "30776070",
                      "description": "Ellinor undrar så",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T12:35:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Håll i hatten - Andy ska ut på nya äventyr! Den här omgången till olika förhistoriska åldrar och arter.",
              "name": "Andys förhistoriska äventyr",
              "subHeading": "Australopithecus och redskapet",
              "startTime": "13:00",
              "start": "2023-11-16T13:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374643-024A",
              "svtId": "Kka36MP",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Australopithecus och redskapet",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/Kka36MP/andys-forhistoriska-aventyr/australopithecus-och-redskapet",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "Kka36MP",
                "parent": {
                  "svtId": "K5ryM4W",
                  "urls": {
                    "svtplay": "/andys-forhistoriska-aventyr",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137409,
                      "id": "19022106",
                      "description": "Andys förhistoriska äventyr",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T13:00:00+01:00",
                "validToFormatted": "Sön 17 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Andy och Teja är tillbaka för att ge er nya episka äventyr, den här gången med kopplingar till vatten!\nHäng med och utforska över hela världen för att hitta både kända och okända vattenlevande djur!",
              "name": "Andys akvatiska äventyr",
              "subHeading": "Andy och trollsländorna",
              "startTime": "13:15",
              "start": "2023-11-16T13:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1402925-008A",
              "svtId": "e6EyWDb",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Andy och trollsländorna",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/e6EyWDb/andys-akvatiska-aventyr/andy-och-trollslandorna",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "e6EyWDb",
                "parent": {
                  "svtId": "KyXkWx5",
                  "urls": {
                    "svtplay": "/andys-akvatiska-aventyr",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135761,
                      "id": "33081082",
                      "description": "Andys akvatiska äventyr",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2022-10-10T18:30:00+02:00",
                "validToFormatted": "Tor 21 mar 2024 (4 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Berit söker jobb som tandläkare och får i uppgift att ta hand om en alldeles egen patient. Lyckas hon laga patientens tänder eller får hon sparken? Del 10 av 12.",
              "name": "Berit söker jobb - samiska",
              "subHeading": "Tandläkare",
              "startTime": "13:30",
              "start": "2023-11-16T13:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406018-010A",
              "svtId": "eDnxGv9",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Tandläkare",
                "shortDescription": "Humorprogram med karaktären Berit",
                "urls": {
                  "svtplay": "/video/eDnxGv9/berit-soker-jobb/tandlakare",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eDnxGv9",
                "parent": {
                  "svtId": "jd6XaNb",
                  "urls": {
                    "svtplay": "/berit-soker-jobb",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696520744,
                      "id": "50710076",
                      "description": "Berit söker jobb.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-04T06:00:00+01:00",
                "validToFormatted": "Mån 10 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Det blir kaos i huset när Loke glömmer sin magiska väska hemma hos Bo och Bella och inte blir det bättre när är han går tillbaka och ytterdörren är låst och Bo och Bella har gått och lagt sig. Hur ska det här gå?",
              "name": "Kaos i huset",
              "subHeading": "Glömda väskan",
              "startTime": "13:35",
              "start": "2023-11-16T13:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406020-011A",
              "svtId": "jqbJEYg",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Glömda väskan",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jqbJEYg/kaos-i-huset/glomda-vaskan",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jqbJEYg",
                "parent": {
                  "svtId": "8zJg1qJ",
                  "urls": {
                    "svtplay": "/kaos-i-huset",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696426392,
                      "id": "50706028",
                      "description": "Kaos i huset.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-11T13:40:00+01:00",
                "validToFormatted": "Mån 10 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Det blir kaos i huset när Bo och Bella går på en fisketur. Vad får de på kroken?",
              "name": "Kaos i huset",
              "subHeading": "Fiske",
              "startTime": "13:40",
              "start": "2023-11-16T13:40:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1406020-012A",
              "svtId": "eXqxWgW",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Fiske",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eXqxWgW/kaos-i-huset/fiske",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eXqxWgW",
                "parent": {
                  "svtId": "8zJg1qJ",
                  "urls": {
                    "svtplay": "/kaos-i-huset",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696426392,
                      "id": "50706028",
                      "description": "Kaos i huset.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-11T13:45:00+01:00",
                "validToFormatted": "Mån 10 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Leif har fångat spökena i en hundbajspåse och håller en seans med Kicki och Stig för att få kontakt med andra sidan. Del 16 av 24: Abissos.",
              "name": "Mysteriet på Greveholm - Grevens återkomst (finska)",
              "subHeading": "Finska — Avsnitt 16: Abissos",
              "startTime": "13:45",
              "start": "2023-11-16T13:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1407292-016A",
              "svtId": "KZA3ryG",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "16. Abissos",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KZA3ryG/mysteriet-pa-greveholm-grevens-aterkomst/16-abissos",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Finska — Avsnitt 16",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KZA3ryG",
                "parent": {
                  "svtId": "KqvMyAW",
                  "urls": {
                    "svtplay": "/mysteriet-pa-greveholm-grevens-aterkomst",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132475,
                      "id": "37744652",
                      "description": "Mysteriet på Greveholm - Grevens återkomst",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T06:00:00+01:00",
                "validToFormatted": "Ons 22 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Lena, Anais, Angelo och Hugo delar alla samma intresse - hästar. På Lenas farfars gamla ranch sköter de sina hästar och hjälper även andra skadade djur. Lena kan inte tänka sig ett liv utan sin Mistral och fortsätter att utveckla sin förmåga som hästlyssnare men Samantha känner stor rivalitet och har svårt att låta dem vara ifred. Säsong 2.",
              "name": "Ranchen",
              "subHeading": "Säsong 2 — Avsnitt 21: Strumpan och Pakito",
              "startTime": "14:00",
              "start": "2023-11-16T14:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1373331-023A",
              "svtId": "jVaqx4e",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "21. Strumpan och Pakito",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jVaqx4e/ranchen/21-strumpan-och-pakito",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 2 — Avsnitt 21",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jVaqx4e",
                "parent": {
                  "svtId": "eoVPdgm",
                  "urls": {
                    "svtplay": "/ranchen",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686133123,
                      "id": "35960389",
                      "description": "Ranchen",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T14:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Engelsk dramaserie efter böcker av den engelska författarinnan Jill Murphy. En skola för häxor ställer stora krav på eleverna. Och stackars Mildred har svårt att hänga med eftersom hon både är höjdrädd och ofta misslyckas med sina trollformler.",
              "name": "En hopplös häxa",
              "subHeading": "Säsong 1 — Avsnitt 26: Millennium",
              "startTime": "14:20",
              "start": "2023-11-16T14:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1117876-026D",
              "svtId": "KVk5kMa",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "26. Millennium",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KVk5kMa/en-hopplos-haxa/26-millennium",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 26",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KVk5kMa",
                "parent": {
                  "svtId": "8zoyv7G",
                  "urls": {
                    "svtplay": "/en-hopplos-haxa",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135958,
                      "id": "31273286",
                      "description": "En hopplös häxa",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T14:20:00+01:00",
                "validToFormatted": "Sön 31 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "I dagens samhälle finns en ny typ av hemliga agenter, väl gömda på en plats där skurkar och banditer aldrig skulle leta. Välkomna till St. Hopes, skolan som gömmer de mest avancerade tonårsagenterna den engelska regeringen har.",
              "name": "M.I. High",
              "subHeading": "Säsong 2 — Avsnitt 10: En bländande kupp",
              "startTime": "14:45",
              "start": "2023-11-16T14:45:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1135312-010A",
              "svtId": "84aLPZL",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "10. En bländande kupp",
                "shortDescription": "Brittisk dramaserie i 13 delar, andra säsongen",
                "urls": {
                  "svtplay": "/video/84aLPZL/mi-high/10-en-blandande-kupp",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 2 — Avsnitt 10",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "84aLPZL",
                "parent": {
                  "svtId": "jpmQWGd",
                  "urls": {
                    "svtplay": "/mi-high",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135652,
                      "id": "33852586",
                      "description": "MI High",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T14:45:00+01:00",
                "validToFormatted": "Tor 21 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Trots det som hänt hans pappa drömmer Nicholas om ett äkta cirkusliv. På den prestigefyllda cirkusskolan Big Top Academy känner han sig både välkommen och motarbetad. Har han det som krävs? Vet de nåt om hans pappa? Mellan akrobatikpass, jonglering och uppträdanden hjälper Phoenix sin vän Nicholas lära sig mer om sin pappa. Denna gång: Mästarna.",
              "name": "Big Top Academy",
              "subHeading": "Säsong 1 — Avsnitt 18: Mästarna",
              "startTime": "15:10",
              "start": "2023-11-16T15:10:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1398935-018A",
              "svtId": "egqaNg7",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "18. Mästarna",
                "shortDescription": "Cirkusskola med hemligheter",
                "urls": {
                  "svtplay": "/video/egqaNg7/big-top-academy/18-mastarna",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 18",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "egqaNg7",
                "parent": {
                  "svtId": "e7ovNox",
                  "urls": {
                    "svtplay": "/big-top-academy",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135969,
                      "id": "31208170",
                      "description": "Big Top Academy",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T15:10:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "En av tidernas mest talangfulla balettdansöser hamnar i vår tid genom ett magiskt halsband. Ska hon anpassa sig till ett liv i nuet, eller vill hon hellre återvända?",
              "name": "Tidlös prinsessa",
              "subHeading": "Säsong 1 — Avsnitt 16: Högriskabel hiphop",
              "startTime": "15:35",
              "start": "2023-11-16T15:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1387559-016A",
              "svtId": "8WvQQMB",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "16. Högriskabel hiphop",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8WvQQMB/tidlos-prinsessa/16-hogriskabel-hiphop",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 16",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8WvQQMB",
                "parent": {
                  "svtId": "eopE913",
                  "urls": {
                    "svtplay": "/tidlos-prinsessa",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1687267499,
                      "id": "50178680",
                      "description": "Tidlös prinsessa",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T15:25:00+01:00",
                "validToFormatted": "Tis 26 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tonårsflickan Alex går i skolan och spelar i en orkester. När hon fyller 16 möter hon indiern Kuru och hans elefant. Han säger att hon är en prinsessa och direkt arvtagare till landet Manjipur. Ska hon bestämma sig för att bli drottning där eller att stanna i Melbourne? Svenska röster: Josephine Bornebusch, Gabriel Odenhammar, Anna Sahlin, Julia Dufvenius m.fl.",
              "name": "Elefantprinsessan",
              "subHeading": "Säsong 1 — Avsnitt 19",
              "startTime": "16:00",
              "start": "2023-11-16T16:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1132600-019A",
              "svtId": "KyVW725",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Avsnitt 19",
                "shortDescription": "Australisk serie från 2008",
                "urls": {
                  "svtplay": "/video/KyVW725/elefantprinsessan/avsnitt-19",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 19",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KyVW725",
                "parent": {
                  "svtId": "eWkGaDV",
                  "urls": {
                    "svtplay": "/elefantprinsessan",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686138080,
                      "id": "259376",
                      "description": "Elefantprinsessan",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T16:00:00+01:00",
                "validToFormatted": "Lör 23 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Darrell Rivers flyttar till internatskolan Malory Towers för att få en nystart. Gwendolyn blir avundsjuk på hur lätt Darrell skaffar sig vänner och börjar därför gräva i hennes förflutna. Det sägs att Lady Malorys ande vandrar i korridorerna om nätterna, vilket passar bra för Alicia som älskar spökhistorier. Första säsongen.",
              "name": "Malory Towers",
              "subHeading": "Säsong 1 — Avsnitt 3: Skämtet",
              "startTime": "16:25",
              "start": "2023-11-16T16:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1399095-003A",
              "svtId": "8ZDkx1q",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "3. Skämtet",
                "shortDescription": "Brittisk serie från 2020 efter böcker av Enid Blyton. Säsong 1",
                "urls": {
                  "svtplay": "/video/8ZDkx1q/malory-towers/3-skamtet",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 3",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8ZDkx1q",
                "parent": {
                  "svtId": "eozJB4G",
                  "urls": {
                    "svtplay": "/malory-towers",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135476,
                      "id": "35103107",
                      "description": "Malory Towers",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T16:25:00+01:00",
                "validToFormatted": "Lör 30 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Gluko och Lennon, två asgrymma kompisar som gör hela världen bättre! Du med! Alltså, du är ju asgrym själv bara för att du läser den här texten! Kolla programmet också! Tjohoo!",
              "name": "Super-duper-asgrymt!",
              "subHeading": "Se det snöar",
              "startTime": "16:50",
              "start": "2023-11-16T16:50:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1393441-015A",
              "svtId": "KrJ5o6E",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Se det snöar",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KrJ5o6E/super-duper-asgrymt/se-det-snoar",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KrJ5o6E",
                "parent": {
                  "svtId": "ePvR7XE",
                  "urls": {
                    "svtplay": "/super-duper-asgrymt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686136580,
                      "id": "27622545",
                      "description": "Super-duper-asgrymt!",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T16:50:00+01:00",
                "validToFormatted": "Tor 4 jan 2024 (2 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Har din bil någon gång börjat dansa, har ditt hus förvandlats till en rymdraket eller har siffrorna i din mattebok försvunnit spårlöst? Agenterna på KLURO löser problemen. Denna gång: Bollkontroll.",
              "name": "Ett fall för KLURO",
              "subHeading": "Bollkontroll",
              "startTime": "17:00",
              "start": "2023-11-16T17:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374787-027A",
              "svtId": "KkVNLNj",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Bollkontroll",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KkVNLNj/ett-fall-for-kluro/bollkontroll",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KkVNLNj",
                "parent": {
                  "svtId": "8ZkG2P6",
                  "urls": {
                    "svtplay": "/ett-fall-for-kluro",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135475,
                      "id": "35107623",
                      "description": "Ett fall för KLURO",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T17:00:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tilde är tjejen som tillsammans med sin hund och sina vänner utforskar världen med hjälp av sina teknikprylar.",
              "name": "Tilde",
              "subHeading": "Önskar du var här",
              "startTime": "17:15",
              "start": "2023-11-16T17:15:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374703-012A",
              "svtId": "8Md967n",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Önskar du var här",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/8Md967n/tilde/onskar-du-var-har",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "8Md967n",
                "parent": {
                  "svtId": "826EL3b",
                  "urls": {
                    "svtplay": "/tilde",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137472,
                      "id": "17470006",
                      "description": "Tilde~",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T17:15:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Charlie är sju år och hänger ofta ute med sin kompis Marv. Han gillar fotboll, raketer och att hitta på historier. Han gillar också sin lillasyster Lola. Det är bara en sak - hon är väldigt  bestämd och envis. Tecknad småbarnsserie från BBC där de svenska rösterna görs av Jakob Bergström, Ella Andersson, Mathilda Smedius, Benjamin Wahlgren m.fl.",
              "name": "Charlie och Lola",
              "subHeading": "",
              "startTime": "17:25",
              "start": "2023-11-16T17:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1116007-022A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Greta är en glatt grymtande liten gris som bor med mamma Gris, pappa Gris och lillebror Georg. Greta tycker om att spela spel, leka och klä ut sig men det absolut bästa hon vet är att hoppa i lerpölar.",
              "name": "Greta Gris",
              "subHeading": "Den stora kullen",
              "startTime": "17:35",
              "start": "2023-11-16T17:35:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1404185-018A",
              "svtId": "KA55DAa",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Den stora kullen",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KA55DAa/greta-gris/den-stora-kullen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KA55DAa",
                "parent": {
                  "svtId": "e3Bz1w5",
                  "urls": {
                    "svtplay": "/greta-gris",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135467,
                      "id": "35159763",
                      "description": "Greta Gris",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T17:35:00+01:00",
                "validToFormatted": "Sön 14 jan 2024 (2 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Häng med bästa kompisarna Esme och Roy - de bästa monsterbarnvaktarna i stan - när de får monsterstora och -små problem på halsen!",
              "name": "Monsterbarnvakterna",
              "subHeading": "Mysingmysteriet",
              "startTime": "17:40",
              "start": "2023-11-16T17:40:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1388339-009A",
              "svtId": "KMdy2d5",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Mysingmysteriet",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KMdy2d5/monsterbarnvakterna/mysingmysteriet",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KMdy2d5",
                "parent": {
                  "svtId": "e3k6JpR",
                  "urls": {
                    "svtplay": "/monsterbarnvakterna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686137101,
                      "id": "23508118",
                      "description": "Monsterbarnvakterna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-09-01T06:00:00+02:00",
                "validToFormatted": "Sön 31 aug 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Fåret Shaun är tillbaka med nya galna upptåg på den helt ovetande bondens gård.",
              "name": "Fåret Shaun",
              "subHeading": "Naturfilmarna",
              "startTime": "17:55",
              "start": "2023-11-16T17:55:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1355484-015A",
              "svtId": "KMdMABG",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Naturfilmarna",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KMdMABG/faret-shaun/naturfilmarna",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KMdMABG",
                "parent": {
                  "svtId": "KqvMn95",
                  "urls": {
                    "svtplay": "/faret-shaun",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1695046902,
                      "id": "50660752",
                      "description": "Fåret Shaun.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-16T17:55:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Pingvinparad. Violas pingvinparad är dagens höjdpunkt för alla på Peek Zoo. Del 20 av 26. UR.",
              "name": "Peek Zoo",
              "subHeading": "",
              "startTime": "18:00",
              "start": "2023-11-16T18:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1400135-020A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00003464010020",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vintern. Det är höst. Men Amanda längtar till vintern och snön. Hon vill att det ska bli jul, nyår och gnistrande stjärnor. Serie byggd av bilder från svensk natur i norr och söder. Del 4 av 4. UR.",
              "name": "Amanda längtar",
              "subHeading": "",
              "startTime": "18:08",
              "start": "2023-11-16T18:08:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1374539-003A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/66010433803",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Skattjägarna. Julian använder kuddar för att bygga en grotta i vardagsrummet. När den rasar ihop blir Julian arg och vill inte leka mer. Del 3 av 26. UR.",
              "name": "Brillebjörn",
              "subHeading": "",
              "startTime": "18:13",
              "start": "2023-11-16T18:13:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1396761-003A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00002402020003",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Havsöringens långa färd. En havsöring måste simma många kilometer mot strömmen, hela vägen ut ur havet och upp till en bäck, för att lägga ägg. På vägen kommer det till ett vattenfall. Kommer den att klara av att nå sitt mål? Del 31 av 40. UR.",
              "name": "Vilda djur i Norden",
              "subHeading": "",
              "startTime": "18:20",
              "start": "2023-11-16T18:20:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408811-031A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009711010031",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Den köttätande växten. I mossan står en växt med många vackra vattendroppar på. Men i verkligheten är de inte alls vattendroppar. Det är en klibbig dödlig fälla. Växten sileshår är nämligen en köttätande växt och den väntar på sin lunch. Del 32 av 40. UR.",
              "name": "Vilda djur i Norden",
              "subHeading": "",
              "startTime": "18:25",
              "start": "2023-11-16T18:25:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1408811-032A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009711010032",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "De små, blå smurfarna drar ut på äventyr och tillsammans försöker de lura den elake Gargamel.",
              "name": "Smurfarna",
              "subHeading": "",
              "startTime": "18:35",
              "start": "2023-11-16T18:35:00+01:00",
              "state": "running",
              "progress": 49,
              "id": "1405873-034A",
              "svtId": "KVBbJ7k",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Blåsningen",
                "shortDescription": "Smurfar på äventyr",
                "urls": {
                  "svtplay": "/video/KVBbJ7k/smurfarna/blasningen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KVBbJ7k",
                "parent": {
                  "svtId": "ePwN672",
                  "urls": {
                    "svtplay": "/smurfarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1690196898,
                      "id": "50475688",
                      "description": "Smurfarna.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-11-03T17:25:00+01:00",
                "validToFormatted": "Tor 30 nov (14 dagar kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hade man verkligen getter med sig ut i krig? Varför låg man inte ner och sov? Fanns enhörningar på riktigt? Hur tog sig svenska armén över Lilla Bält och vad var egentligen reduktionen? Här får vi veta mer om tiden mellan 1611 och 1718. Del 3 av 8. UR.",
              "name": "Vilken historia!",
              "subHeading": "Säsong 1 — Avsnitt 3: Stormaktstiden - trossen",
              "startTime": "18:45",
              "start": "2023-11-16T18:45:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1409400-003A",
              "svtId": "jkAvDWp",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009869010003",
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "3. Stormaktstiden - trossen",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jkAvDWp/vilken-historia/3-stormaktstiden-trossen",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 3",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jkAvDWp",
                "parent": {
                  "svtId": "K5drBbE",
                  "urls": {
                    "svtplay": "/vilken-historia",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1697197550,
                      "id": "50732824",
                      "description": "Vilken historia! En serie från UR.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Mån 17 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Skiljetecken. Hur använder man skiljetecken? Mats och Marie firar Skiljetecknens dag på torget och bjuder på både saft, punkt och kommatecken. Britt-Inger tappar luften när hon läser ett manus från en författare utan ett enda skiljetecken. Del 3 av 8. UR.",
              "name": "Skrivbolaget",
              "subHeading": "",
              "startTime": "19:00",
              "start": "2023-11-16T19:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1404594-003A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": "http://urplay.se/redirect/epn/00006105010003",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Dagens viktigaste nyheter för unga tittare.",
              "name": "Lilla Aktuellt",
              "subHeading": "",
              "startTime": "19:15",
              "start": "2023-11-16T19:15:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408736-052A",
              "svtId": "jNnNZN4",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ikväll 18:15",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jNnNZN4/lilla-aktuellt/ikvall-18-15",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jNnNZN4",
                "parent": {
                  "svtId": "jLYy6Bk",
                  "urls": {
                    "svtplay": "/lilla-aktuellt",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1700150287,
                      "id": "50827968",
                      "description": "Lilla Aktuellt",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-16T18:15:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tre syskon skannas av misstag in i ett dataspel. Där dras de in i ett äventyr fyllt av pirater och skeppsbrutna barn på jakt efter en gömd skatt. Piraterna tror att de märkliga främlingarna kommit för att lägga beslag på skatten och planerar att stoppa dem till varje pris. Kanske är den gömda skatten vägen hem, i så fall måste de hinna före piraterna. Svenska röster: Linn Ehrner, Ima Nilsson, Alexander Kaunitz, Dominique Pålsson Wiklund, Tom Ljungman, Björn Bengtsson, Allan Svensson m.fl.",
              "name": "Piratöarna",
              "subHeading": "Säsong 1 — Avsnitt 5: Den stora flykten",
              "startTime": "19:25",
              "start": "2023-11-16T19:25:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1358373-005A",
              "svtId": "K16B1np",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "5. Den stora flykten",
                "shortDescription": "Australisk äventyrsserie i 26 delar",
                "urls": {
                  "svtplay": "/video/K16B1np/piratoarna/5-den-stora-flykten",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 5",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "K16B1np",
                "parent": {
                  "svtId": "ewkEndy",
                  "urls": {
                    "svtplay": "/piratoarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686138055,
                      "id": "2095774",
                      "description": "Piratöarna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T19:25:00+01:00",
                "validToFormatted": "Lör 16 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Björnen Grizzy bor i en skogvaktarstuga där han mot sin vilja måste samsas med en stor lämmelfamilj. Detta avsnitt: Is och björnar.",
              "name": "Grizzy och lämlarna",
              "subHeading": "Is och björnar",
              "startTime": "19:50",
              "start": "2023-11-16T19:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408309-002A",
              "svtId": "eZAngz4",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Is och björnar",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/eZAngz4/grizzy-och-lamlarna/is-och-bjornar",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eZAngz4",
                "parent": {
                  "svtId": "8voLy64",
                  "urls": {
                    "svtplay": "/grizzy-och-lamlarna",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1685707712,
                      "id": "50023583",
                      "description": "Grizzy och lämlarna",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "KidsTvShow"
                },
                "validFrom": "2023-09-13T19:05:00+02:00",
                "validToFormatted": "Ons 11 feb 2026 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "20:00",
              "start": "2023-11-16T20:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "noitem-ch-barnkanalen",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Leif har fångat spökena i en hundbajspåse och håller en seans med Kicki och Stig för att få kontakt med andra sidan. Del 16 av 24: Abissos.",
              "name": "Mysteriet på Greveholm - Grevens återkomst (romani)",
              "subHeading": "Romani — Avsnitt 16: Abissos",
              "startTime": "04:00",
              "start": "2023-11-17T04:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408248-016A",
              "svtId": "82onam6",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "16. Abissos",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/82onam6/mysteriet-pa-greveholm-grevens-aterkomst/16-abissos",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Romani — Avsnitt 16",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "82onam6",
                "parent": {
                  "svtId": "KqvMyAW",
                  "urls": {
                    "svtplay": "/mysteriet-pa-greveholm-grevens-aterkomst",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132475,
                      "id": "37744652",
                      "description": "Mysteriet på Greveholm - Grevens återkomst",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T06:00:00+01:00",
                "validToFormatted": "Ons 22 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Leif har fångat spökena i en hundbajspåse och håller en seans med Kicki och Stig för att få kontakt med andra sidan. Del 16 av 24: Abissos.",
              "name": "Mysteriet på Greveholm - Grevens återkomst (finska)",
              "subHeading": "Finska — Avsnitt 16: Abissos",
              "startTime": "04:15",
              "start": "2023-11-17T04:15:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407292-016A",
              "svtId": "KZA3ryG",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "16. Abissos",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KZA3ryG/mysteriet-pa-greveholm-grevens-aterkomst/16-abissos",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Finska — Avsnitt 16",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KZA3ryG",
                "parent": {
                  "svtId": "KqvMyAW",
                  "urls": {
                    "svtplay": "/mysteriet-pa-greveholm-grevens-aterkomst",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132475,
                      "id": "37744652",
                      "description": "Mysteriet på Greveholm - Grevens återkomst",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T06:00:00+01:00",
                "validToFormatted": "Ons 22 maj 2024 (6 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vi börjar från början när de allra första människorna kommer hit. När inlandsisen smälter så kommer först djuren och sedan människorna. Vi får se hur människor lever, hur de bor, vad de äter och hur viktig hunden är för stenåldersfamiljerna. Vi får också se vilka faror som finns och hur de ser på livet efter döden.",
              "name": "Historien om Sverige med Farah - syntolkat",
              "subHeading": "Säsong 1 — Avsnitt 1: Jägare och offer",
              "startTime": "04:30",
              "start": "2023-11-17T04:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408193-001S",
              "svtId": "eoG2prg",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Jägare och offer",
                "shortDescription": "Historieprogram för barnen och hela familjen",
                "urls": {
                  "svtplay": "/video/eoG2prg/historien-om-sverige-med-farah/1-jagare-och-offer",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eoG2prg",
                "parent": {
                  "svtId": "KMyvAB1",
                  "urls": {
                    "svtplay": "/historien-om-sverige-med-farah",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1697114736,
                      "id": "50728836",
                      "description": "Historien om Sverige med Farah.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-05T02:00:00+01:00",
                "validToFormatted": "Tis 4 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vi börjar från början när de allra första människorna kommer hit. När inlandsisen smälter så kommer först djuren och sedan människorna. Vi får se hur människor lever, hur de bor, vad de äter och hur viktig hunden är för stenåldersfamiljerna. Vi får också se vilka faror som finns och hur de ser på livet efter döden.",
              "name": "Historien om Sverige med Farah - teckenspråk",
              "subHeading": "Säsong 1 — Avsnitt 1: Jägare och offer",
              "startTime": "04:40",
              "start": "2023-11-17T04:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408193-001T",
              "svtId": "eoG2prg",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Jägare och offer",
                "shortDescription": "Historieprogram för barnen och hela familjen",
                "urls": {
                  "svtplay": "/video/eoG2prg/historien-om-sverige-med-farah/1-jagare-och-offer",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eoG2prg",
                "parent": {
                  "svtId": "KMyvAB1",
                  "urls": {
                    "svtplay": "/historien-om-sverige-med-farah",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1697114736,
                      "id": "50728836",
                      "description": "Historien om Sverige med Farah.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-05T02:00:00+01:00",
                "validToFormatted": "Tis 4 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Wilma råkar läsa Samiras dagbok.",
              "name": "Tjejer - teckenspråk",
              "subHeading": "Hösten 2023 — Avsnitt 23: Wilma - Dagboken",
              "startTime": "04:50",
              "start": "2023-11-17T04:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406687-023T",
              "svtId": "84oRZpQ",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "23. Wilma - Dagboken - teckenspråkstolkat",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/84oRZpQ/tjejer/23-wilma-dagboken",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Hösten 2023 — Avsnitt 23",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "84oRZpQ",
                "parent": {
                  "svtId": "eYwG299",
                  "urls": {
                    "svtplay": "/tjejer",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1692610987,
                      "id": "50583500",
                      "description": "Tjejer.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-08T06:00:00+01:00",
                "validToFormatted": "Tor 13 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vågar Alex gå ner i den mörka tvättstugan själv?",
              "name": "Tjejer - teckenspråk",
              "subHeading": "Hösten 2023 — Avsnitt 24: Alex - Tvättstugan",
              "startTime": "04:55",
              "start": "2023-11-17T04:55:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406687-024T",
              "svtId": "KZAnGRN",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "24. Alex - Tvättstugan - teckenspråkstolkat",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/KZAnGRN/tjejer/24-alex-tvattstugan",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Hösten 2023 — Avsnitt 24",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "KZAnGRN",
                "parent": {
                  "svtId": "eYwG299",
                  "urls": {
                    "svtplay": "/tjejer",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1692610987,
                      "id": "50583500",
                      "description": "Tjejer.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-11T06:00:00+01:00",
                "validToFormatted": "Tor 13 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            }
          ],
          "__typename": "Channel"
        },
        {
          "id": "ch-kunskapskanalen",
          "name": "Kunskapskanalen",
          "schedule": [
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "05:00",
              "start": "2023-11-16T05:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "noitem-ch-kunskapskanalen",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Musik, gräl och rockdrömmar. Rockbandet Eldkvarns frontfigur Plura Jonsson intervjuas på scen av musikjournalisten Håkan Lahger, som har följt Eldkvarn från 1970-talet fram till idag och nu har skrivit en bok om bandet. UR.",
              "name": "Bokmässan 2023",
              "subHeading": "",
              "startTime": "15:00",
              "start": "2023-11-16T15:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1409645-018A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00010647010018",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Annie Lööf bakom rubrikerna. Annie Lööf var 23 år när hon äntrade den rikspolitiska scenen. Fem år senare blev hon Centerpartiets yngsta ordförande genom tiderna, och var en nyckelspelare i svensk politik. Hon har mött såväl djup respekt som hot, hat och konkreta mordplaner. Här berättar hon själv utifrån sin aktuella självbiografi. UR.",
              "name": "Bokmässan 2023",
              "subHeading": "",
              "startTime": "15:40",
              "start": "2023-11-16T15:40:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1409645-020A",
              "svtId": null,
              "rerunDescription": "Från 6/11 i K",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00010647010020",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Ingen att hålla fast vid. De bokaktuella författarna Nicolas Lunabba och Atefeh Sebdani möts i ett samtal om att växa upp i ett samhälle som misslyckas med att se de utsatta. Moderator: Kattis Ahlström, journalist. UR.",
              "name": "Bokmässan 2023",
              "subHeading": "",
              "startTime": "16:10",
              "start": "2023-11-16T16:10:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1409645-019A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00010647010019",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Bygga utan CO2. Hur bygger vi utan att släppa ut koldioxid? Höghus i massivt trä kan vara framtiden, eftersom träd lagrar koldioxid medan det växer och samtidigt ger träbostäderna en speciell charm och ett hälsosamt inomhusklimat. Dansk faktaserie från 2019. Del 2 av 4. UR.",
              "name": "Framtidens byggnader",
              "subHeading": "",
              "startTime": "17:00",
              "start": "2023-11-16T17:00:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1403142-002A",
              "svtId": null,
              "rerunDescription": "Från 10/11 i K",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": "http://urplay.se/redirect/epn/00003528010002",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Pekings nya flygplats är tänkt att bli jordens största. Den är byggd för 76 miljoner människor, men förra året passerade här hela 100 miljoner resenärer. Hur bygger man något som är så oerhört stort, men som också behöver vara säkert och effektivt? Del 12 av 16.",
              "name": "Omöjlig ingenjörskonst",
              "subHeading": "Säsong 5 — Avsnitt 12: Pekings nya flygplats",
              "startTime": "17:30",
              "start": "2023-11-16T17:30:00+01:00",
              "state": "ended",
              "progress": 100,
              "id": "1394575-012A",
              "svtId": "Kw19v67",
              "rerunDescription": "Från 12/11 i K. Även i K 17/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "12. Pekings nya flygplats",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/Kw19v67/omojlig-ingenjorskonst/12-pekings-nya-flygplats",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 5 — Avsnitt 12",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "Kw19v67",
                "parent": {
                  "svtId": "jx3zWJq",
                  "urls": {
                    "svtplay": "/omojlig-ingenjorskonst",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686132996,
                      "id": "36725751",
                      "description": "Omöjlig ingenjörskonst - U.S. Air Force F-35A Lightning II.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-12T19:25:00+01:00",
                "validToFormatted": "Mån 18 dec (1 månad kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Världsekonomin är ofta oupptäckt, äventyrlig och tilltalande för många enskilda investerare. En sak är säker - värdet på en valuta går alltid upp eller ner på bekostnad av en annan. Där en person vinner förlorar en annan likt ett nollsummespel. Så vem vinner när du förlorar? Del 2 av 3.",
              "name": "Den nya ekonomin",
              "subHeading": "Lycksökarna",
              "startTime": "18:15",
              "start": "2023-11-16T18:15:00+01:00",
              "state": "running",
              "progress": 50,
              "id": "1407810-005A",
              "svtId": "jvvVQvm",
              "rerunDescription": "Från 13/11 i K. Även i K 17/11 och K 19/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Lycksökarna",
                "shortDescription": "Nederländsk dokumentärserie från 2023",
                "urls": {
                  "svtplay": "/video/jvvVQvm/den-nya-ekonomin/lycksokarna",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jvvVQvm",
                "parent": {
                  "svtId": "e5o36Qy",
                  "urls": {
                    "svtplay": "/den-nya-ekonomin",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686130980,
                      "id": "38877755",
                      "description": "Den nya ekonomin",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-13T20:10:00+01:00",
                "validToFormatted": "Tis 18 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Londons glömda gravplats. Seriestart. Under utgrävningar inför en ny järnväg i centrala London hittas kistor med lås och inklädda i järn. Man tog till alla sätt för att stoppa gravplundrarvågen som plågade London under georgiansk tid. Vad var det egentligen vad som hände? Brittisk faktaserie från 2020. Del 1 av 3. UR.",
              "name": "Englands stora utgrävning",
              "subHeading": "",
              "startTime": "19:05",
              "start": "2023-11-16T19:05:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1405129-001A",
              "svtId": null,
              "rerunDescription": "Från 14/11 i K. Även i K 17/11 och K 18/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00004742__0001",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Under bronsåldern domineras vår värld av tre blomstrande civilisationer. Men inom loppet av bara 50 år kollapsar de alla, en efter en. Hur är det möjligt? Följ med och nysta i den stora gåtan om bronsålderns kollaps.",
              "name": "Världens historia: Bronsålderns kollaps",
              "subHeading": "Australisk dokumentär",
              "startTime": "20:05",
              "start": "2023-11-16T20:05:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408570-001A",
              "svtId": "KMVxoG3",
              "rerunDescription": "Från 14/11 i SVT2. Även i SVT2 senare i natt",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "Bronsålderns kollaps",
                "shortDescription": "Australisk dokumentär",
                "urls": {
                  "svtplay": "/video/KMVxoG3/varldens-historia-bronsalderns-kollaps",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "KMVxoG3",
                "parent": {
                  "svtId": "jQ72vA2",
                  "urls": {
                    "svtplay": "/video/jQ72vA2/varldens-historia-bronsalderns-kollaps",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696253044,
                      "id": "50699216",
                      "description": "Världens historia: Bronsålderns kollaps",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-14T02:00:00+01:00",
                "validToFormatted": "Tor 13 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Antikens riken strävade alltid efter odödlighet men förstörelsen låg alltid nära till hands. Idag kan vi fortfarande höra ekon från dess uråldriga krafter och de fortsätter att forma vår värld med sina idéer och övertygelser. Del 6 av 6.",
              "name": "Antikens riken",
              "subHeading": "Säsong 1 — Avsnitt 6: Slutet",
              "startTime": "21:00",
              "start": "2023-11-16T21:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408423-006A",
              "svtId": "eroav46",
              "rerunDescription": "Från 11/11 i K",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "6. Slutet",
                "shortDescription": "Brittisk historiedokumentär från 2023",
                "urls": {
                  "svtplay": "/video/eroav46/antikens-riken/6-slutet",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 6",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eroav46",
                "parent": {
                  "svtId": "8qPvRYR",
                  "urls": {
                    "svtplay": "/antikens-riken",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1695996942,
                      "id": "50694576",
                      "description": "Antikens riken",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-11T02:00:00+01:00",
                "validToFormatted": "Sön 4 maj 2025 (1 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Att uppfostra ett barn är svårt, oavsett om det sker i Stilla havet där bläckfiskhonan måste leta efter en säker plats att förvara sina ägg. Eller högt upp i Borneos regnskog, där orangutangmamman lär sitt barn djungelns alla vägar. Vår planets fauna är vacker och mångfacetterad, liksom sätten att uppfostra djur.",
              "name": "Naturens bästa föräldrar",
              "subHeading": "Österrikisk naturfilm från 2021",
              "startTime": "21:50",
              "start": "2023-11-16T21:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407732-001A",
              "svtId": "emow2PX",
              "rerunDescription": "Från 11/11 i K",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Naturens bästa föräldrar",
                "shortDescription": "Österrikisk naturfilm från 2021",
                "urls": {
                  "svtplay": "/video/emow2PX/naturens-basta-foraldrar",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "emow2PX",
                "parent": {
                  "svtId": "8Wv67pq",
                  "urls": {
                    "svtplay": "/video/8Wv67pq/naturens-basta-foraldrar",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698405698,
                      "id": "50773120",
                      "description": "Naturens bästa föräldrar",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2023-11-11T21:00:00+01:00",
                "validToFormatted": "Mån 10 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Niagarafloden i Kanada och USA. Vi reser nerför den vackra Niagarafloden och lär oss mer om den fascinerande historien och tekniken för denna gränsflod mellan Kanada och USA, innan vi anländer till de världsberömda Niagarafallen. Kanadensiskt reseprogram från 2020. Del 3 av 6. UR.",
              "name": "Resor längs vackra floder",
              "subHeading": "",
              "startTime": "22:40",
              "start": "2023-11-16T22:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1405622-003A",
              "svtId": null,
              "rerunDescription": "Från 12/11 i K",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00006348010003",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Ett förändrat London. Vi ser hur London byggs upp etappvis efter 1945. Barbican var ett stort byggprojekt med bio, teatrar och bostäder som började byggas 1962. Men det skulle dröja till 1982 innan stadsdelen officiellt invigdes av drottningen. Del 4 av 4. UR.",
              "name": "Städerna efter kriget",
              "subHeading": "",
              "startTime": "23:25",
              "start": "2023-11-16T23:25:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1409235-004A",
              "svtId": null,
              "rerunDescription": "Från 14/11 i K. Även i K 19/11 och K 20/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009149010004",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vatten är vår planets absolut viktigaste resurs. Men på flera platser på jorden har klimatförändningar gjort att färskvattnet riskerar att ta slut och det är ett alarmerande problem som måste hanteras nu. Dokumentären är filmad över tre år och följer dem som arbetar vid frontlinjen på tre av våra kontinenter där bristen är som värst. Tillsammans berättar de historien om hur illa läget är men också vad vi kan göra för att stoppa utvecklingen.",
              "name": "Planet under press: Dagen då vattnet tar slut",
              "subHeading": "Amerikansk dokumentär från 2021",
              "startTime": "00:20",
              "start": "2023-11-17T00:20:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1402406-001A",
              "svtId": "j4Vz7NR",
              "rerunDescription": "Tidigare sänt 2021. Även i K 19/11",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Dagen då vattnet tar slut",
                "shortDescription": "Amerikansk dokumentär från 2021",
                "urls": {
                  "svtplay": "/video/j4Vz7NR/dagen-da-vattnet-tar-slut",
                  "__typename": "Urls"
                },
                "episode": {
                  "__typename": "Single"
                },
                "__typename": "Variant",
                "videoSvtId": "j4Vz7NR",
                "parent": {
                  "svtId": "KnDkrgM",
                  "urls": {
                    "svtplay": "/video/KnDkrgM/dagen-da-vattnet-tar-slut",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1686135886,
                      "id": "31880007",
                      "description": "Vatten är vår planets absolut viktigaste resurs. Men på flera platser på jorden har klimatförändningar gjort att färskvattnet riskerar att ta slut och det är ett alarmerande problem som måste hanteras nu. - Dagen då vattnet tar slut",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "Single"
                },
                "validFrom": "2021-07-20T02:00:00+02:00",
                "validToFormatted": "Sön 31 mar 2024 (4 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan bara ses i Sverige",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "01:15",
              "start": "2023-11-17T01:15:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "noitem-ch-kunskapskanalen",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            }
          ],
          "__typename": "Channel"
        },
        {
          "id": "ch-svt24",
          "name": "SVT 24",
          "schedule": [
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "05:00",
              "start": "2023-11-16T05:00:00+01:00",
              "state": "running",
              "progress": 91,
              "id": "noitem-ch-svt24",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "I flera år har Marie drömt om den här dagen - att äntligen få träffa den franska konstnärinnan Pascale Palun. Systrarna besöker hennes sagoliknande universum i centrala Avignon. Det blir ett möte som väcker känslor. Resan fortsätter via Cannes och vidare till Milano. I vintageaffären i Milano kan man både hitta fynd och en egen stil. Det händer något med en när man reser och äntligen får systrarna tid till att prata om det de annars aldrig pratar om. Del 3 av 6.",
              "name": "Husdrömmar Sicilien - vintageresan - syntolkat",
              "subHeading": "Säsong 1 — Avsnitt 3: \"Om jag inte haft dig\"",
              "startTime": "20:00",
              "start": "2023-11-16T20:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406762-003S",
              "svtId": "Kz2NMP7",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "3. \"Om jag inte haft dig\" - syntolkat",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/Kz2NMP7/husdrommar-sicilien-vintageresan/3-om-jag-inte-haft-dig",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 3",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "Kz2NMP7",
                "parent": {
                  "svtId": "89dBg2w",
                  "urls": {
                    "svtplay": "/husdrommar-sicilien-vintageresan",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696412513,
                      "id": "50705788",
                      "description": "Husdrömmar Sicilien vintageresan - Husdrömmar Sicilien - vintageresan",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Tis 4 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Lisa och Christoffer lämnar både jobb och bostad för att följa sin stora dröm. En italiensk skolbuss ska byggas om till deras nya rullande hem och möjliggöra ett friare liv och en evig sommar. Planen är att bygga om den gamla bussen under en sommar för att till hösten lämna landet mot varmare breddgrader. Del 1 av 3.",
              "name": "Husdrömmar dokumentär: Bussen till friheten - syntolkat",
              "subHeading": "Säsong 1 — Avsnitt 4: Bussen till friheten del 1",
              "startTime": "20:30",
              "start": "2023-11-16T20:30:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1403537-004S",
              "svtId": "K5RLp9p",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "4. Bussen till friheten del 1",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/K5RLp9p/husdrommar-dokumentar/4-bussen-till-friheten-del-1",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 4",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "K5RLp9p",
                "parent": {
                  "svtId": "jwqkpAn",
                  "urls": {
                    "svtplay": "/husdrommar-dokumentar",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1696341983,
                      "id": "50702656",
                      "description": "Husdrömmar dokumentär",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Tis 18 jun 2024 (7 månader kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Branden i Västmanland 2014 var den största enskilda branden i modern tid. Människor drabbades och naturen förändrades men inga djurpopulationer tog skada på allvar. I de våldsamma bränderna i Australien 2020 drabbades tre miljarder djur av lågornas framfart. Anders Lundin besöker koalaforskare och djursjukhus som försöker rädda en hotad art. Del 1 av 4.",
              "name": "I brändernas spår - syntolkat",
              "subHeading": "Säsong 1 — Avsnitt 1: Förgöraren och livgivaren",
              "startTime": "21:00",
              "start": "2023-11-16T21:00:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1407159-001S",
              "svtId": "jak1b9R",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Förgöraren och livgivaren - syntolkat",
                "shortDescription": "Svensk naturserie från 2023",
                "urls": {
                  "svtplay": "/video/jak1b9R/i-brandernas-spar/1-forgoraren-och-livgivaren",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jak1b9R",
                "parent": {
                  "svtId": "KXvbyM1",
                  "urls": {
                    "svtplay": "/i-brandernas-spar",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698928891,
                      "id": "50789812",
                      "description": "I brändernas spår. Svensk naturserie från 2023 med Anders Lundin.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-16T02:00:00+01:00",
                "validToFormatted": "Lör 15 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Tove rapporteras försvunnen efter en utekväll. Snabbt blir två unga kvinnor som är bekanta med Tove misstänkta och efter flera veckors sökande hittas Tove död. Poliser, utredare och grannar berättar om kvällen då Tove dog och de efterföljande veckorna. Reportrarna på Vetlandaposten beskriver hur samhället påverkades och företrädare för Missing People berättar om hur alla slöt upp i sökandet efter Tove. Del 1 av 3.",
              "name": "I brottets spår: Fallet Tove - teckenspråkstolkat",
              "subHeading": "Säsong 1 — Avsnitt 1: Brottet",
              "startTime": "21:50",
              "start": "2023-11-16T21:50:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408527-001T",
              "svtId": "eyp54w7",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "surround51",
              "usingFallbackImage": false,
              "item": {
                "name": "1. Brottet - teckenspråkstolkat",
                "shortDescription": "Svensk dokumentärserie från 2023",
                "urls": {
                  "svtplay": "/video/eyp54w7/i-brottets-spar-fallet-tove/1-brottet",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "Säsong 1 — Avsnitt 1",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "eyp54w7",
                "parent": {
                  "svtId": "KrQbLL3",
                  "urls": {
                    "svtplay": "/i-brottets-spar-fallet-tove",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1698219285,
                      "id": "50765052",
                      "description": "Tove rapporteras försvunnen efter en utekväll. Snabbt blir två unga kvinnor som är bekanta med Tove misstänkta och efter flera veckors sökande hittas Tove död. - I brottets spår: Fallet Tove",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvSeries"
                },
                "validFrom": "2023-11-12T02:00:00+01:00",
                "validToFormatted": "Tis 25 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Seriestart. Artisten Viktor Frisk fick sin bipolära diagnos efter en traumatisk upplevelse under en stökig period i livet. Vi får följa med Viktor på hans resa för att förstå sig själv. Del 1 av 4. UR.",
              "name": "Min ångest och jag - teckenspråkstolkat",
              "subHeading": "",
              "startTime": "22:40",
              "start": "2023-11-16T22:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1409275-001A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00009778010001SL",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "När idrotten försvinner. För Torsten innebar flytten från Stockholm till Backe ett avsmalnat utbud av idrotter att välja på. Coronapandemin påverkade många föreningar och aktiva på ett drastiskt sätt. Vi gör nedslag runt om i landet för att fånga upp röster och lärdomar. Del 6 av 6. UR.",
              "name": "Idrottens himmel och helvete - teckenspråkstolkat",
              "subHeading": "",
              "startTime": "23:10",
              "start": "2023-11-16T23:10:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1404397-006A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": "http://urplay.se/redirect/epn/00006220040006SL",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Vetenskap och religion. Kyrkan hade en ohotad position både hos människa och samhälle fram till att vetenskapen började ta plats på 1600-talet. Men vad hände när det man tidigare litat blint på inte längre var den enda sanningen? Del 3 av 8. UR.",
              "name": "En annan sida av historien - teckenspråkstolkat",
              "subHeading": "",
              "startTime": "23:40",
              "start": "2023-11-16T23:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1405326-003A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00005241010003SL",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Dialog. Babi träffar ungdomar som genom dialog skapat kunskap och förståelse för andras olikheter. På en fritidsgård i Gällivare såg några ungdomar till att sprida kunskap om hbtq-personer och arrangera stadens första pride-parad. I Trollhättan bestämde sig några ungdomar för att bli ambassadörer och genom att träffa pensionärer få slut på trakasserierna mot dem. Del 3 av 6. UR.",
              "name": "Demokrati helt enkelt - teckenspråkstolkat",
              "subHeading": "",
              "startTime": "23:55",
              "start": "2023-11-16T23:55:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406523-003A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": false,
              "urPlayLink": "http://urplay.se/redirect/epn/00006647010003SL",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Hur kan vi minska stress? Tonåringars stress och prestationskrav bara ökar. Vad är det egentligen som stressar våra barn och hur kan vi hjälpa dem att hitta en bra balans? Även vuxna kan behöva lära sig att hantera sin egen stress så att den inte överförs på barnen. Del 2 av 5. UR.",
              "name": "Våra tonåringar - teckenspråkstolkat",
              "subHeading": "",
              "startTime": "00:10",
              "start": "2023-11-17T00:10:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1406263-002A",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": "http://urplay.se/redirect/epn/00006281010002SL",
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "Läkaren Henrik utsätter sitt blod för livsfarligt ormgift. Kemisten Ulf och programledaren Robin Paulsson ordnar en lite annorlunda vinprovning och så berättar historiken Gunnar varför snus är en så stor grej för svenskarna. Medverkande: Robin Paulsson, Henrik Widegren, Jessica Abbott, Gabriella Stenberg Wieser, Gunnar Wetterberg och Jenny Nilsson. Del 7 av 8.",
              "name": "Fråga Lund - teckenspråkstolkat",
              "subHeading": "",
              "startTime": "00:40",
              "start": "2023-11-17T00:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "1408340-007T",
              "svtId": "jb3Q7XD",
              "rerunDescription": "",
              "isBroadcastIntermission": false,
              "highDefinition": true,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": false,
              "item": {
                "name": "Ormgift och cirkuskonster - teckenspråkstolkat",
                "shortDescription": "",
                "urls": {
                  "svtplay": "/video/jb3Q7XD/fraga-lund/ormgift-och-cirkuskonster",
                  "__typename": "Urls"
                },
                "episode": {
                  "positionInSeason": "",
                  "__typename": "Episode"
                },
                "__typename": "Variant",
                "videoSvtId": "jb3Q7XD",
                "parent": {
                  "svtId": "KQYbQZD",
                  "urls": {
                    "svtplay": "/fraga-lund",
                    "__typename": "Urls"
                  },
                  "images": {
                    "wide": {
                      "changed": 1693703749,
                      "id": "50618596",
                      "description": "Fråga Lund med programledare Robin Paulsson.",
                      "__typename": "Image"
                    },
                    "__typename": "Images"
                  },
                  "__typename": "TvShow"
                },
                "validFrom": "2023-11-14T02:00:00+01:00",
                "validToFormatted": "Tor 13 nov 2025 (2 år kvar)",
                "restrictions": {
                  "onlyAvailableInSwedenFormatted": "Kan ses i hela världen",
                  "__typename": "Restrictions"
                }
              },
              "__typename": "BroadcastItem"
            },
            {
              "descriptionRaw": "",
              "name": "Sändningsuppehåll",
              "subHeading": "",
              "startTime": "01:40",
              "start": "2023-11-17T01:40:00+01:00",
              "state": "upcoming",
              "progress": 0,
              "id": "noitem-ch-svt24",
              "svtId": null,
              "rerunDescription": "",
              "isBroadcastIntermission": true,
              "highDefinition": false,
              "urPlayLink": null,
              "broadcastAudio": "unknown",
              "usingFallbackImage": true,
              "item": null,
              "__typename": "BroadcastItem"
            }
          ],
          "__typename": "Channel"
        }
      ],
      "__typename": "Channels"
    }
  }
}
```