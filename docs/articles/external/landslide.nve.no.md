# landslide.nve.no

NVE utarbeider daglig jordskredvarsler som beskriver aktsomhetsnivå og skredtype på regionalt nivå (fylkesnivå, ev. en eller flere kommuner). Formålet er å unngå tap av liv og verdier som følge av jord-, sørpe- og flomskred. Jordskredvarslingen for Norge ble operativt i 2013.

Bruk swagger for ytterligere dokumentasjon av apiet
https://api01.nve.no/hydrology/forecast/landslide/v1.0.8/swagger/ui/index

## Endpoints

> https://api01.nve.no/hydrology/forecast/landslide/v1.0.8
>
> - __GET__
>   - /api/Warning/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/All/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/County/`{county}`/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/Municipality/`{municipality}`/`{langkey}`/`{startdate}`/`{enddate}`

## Path Parameters

- `{county}`
- `{municipality}`
  - [The norwegian county/municipality number.](https://api01.nve.no/hydrology/forecast/flood/v1.0.8/api/Region/)
  - *Value: (required).*
  - *Parameter Type: path.*
  - *Data Type: string.*
- `{langkey}`
  - One of: 1 = Norwegian. 2 = English.
  - *Value: (required).*
  - *Parameter Type: path.*
  - *Data Type: number.*
- `{startdate}`
  - The start date (yyyy-MM-dd).
  - *Value: (required).*
  - *Parameter Type: path.*
  - *Data Type: string.*
- `{enddate}`
  - The end date (yyyy-MM-dd).
  - *Value: (required).*
  - *Parameter Type: path.*
  - *Data Type: string.*

## Responses

### 200 OK

```json
[
  {
    "CountyList": [
      {
        "Id": 20,
        "Name": "Finnmark"
      }
    ],
    "MunicipalityList": [
      {
        "Id": 2030,
        "Name": "Sør-Varanger"
      }
    ],
    "ActivityLevel": 3,
    "ValidFrom": "22/12/2017 07:00:00",
    "ValidTo": "23/12/2017 06:59:00",
    "DangerType": 6,
    "DangerTypeName": "jord-, sørpe- og flomskredfare",
    "MainText": "Varsel om jord-, sørpe- og flomskredfare, oransje nivå for deler av Finnmark, grunnet mye regn og snøsmelting",
    "CauseList": [
      {
        "Id": 1,
        "Name": "Regn"
      }
    ]
  }
]
```

## ActivityLevel

- 0: Ikke vurdert
- 1: Liten
- 2: Moderat
- 3: Betydelig
- 4: Stor

## Cause

- 0: Ikke gitt
- 1: Regn
- 2: Intens regn (bygenedbør)
- 3: Snøsmelting
- 4: Isgang
- 5: Frost og is
- 6: Vannmetning (i jord)
- 7: Dambrudd/jøkulhlaup