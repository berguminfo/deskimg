# wrn.yr.no

## Description

Delivers weather alerts in CAP format plus an RSS 2.0 feed of such.

Documentation (in Norwegian) of the MetAlerts service and warnings in CAP format may be found in the MET report Samordning av farevarsler ved MET og NVE (20-2017).

The Farevarselplan (in Norwegian) describes the criteria for issuing alerts.

Also see the instructions on how to use the RSS feed (in Norwegian).

> [!TIP]
> For more information:
> - https://api.met.no/weatherapi/metalerts/1.1/documentation
> - [MET-report_20-2017.pdf](MET-report_20-2017.pdf)

## Usage

Use the RSS feed to get a list of CAP warnings, optionally filtered by query parameters.

Do not download each CAP file more than once, and certainly not every time you download the RSS feed! (this is liable to getting you throttled). Once issued the CAP file never changes, so it should be downloaded only once and stored locally.

## Endpoints

> https://api.met.no/weatherapi
>
> - __GET__
>   - /metalerts/1.1/?`{Parameters...}`

## Query Parameters

- `none`
  - Unless the cap parameter is given, we return an RSS 2.0 feed of alerts.
- `cap`
  - Retrieve CAP XML message with given guid (Note: From 1.0 this is the CAP identifier, not a date!).
- `lang`
  - Output language, currently "no" or "en" for Norwegian and English, respectively.
- `event`
  - Filter by CAP eventType, legal values are: blowingSnow, forestFire, gale, ice, icing, polarLow, rain, rainFlood, snow, stormSurge, wind.
  - You can find matching icons (SVG and PNG) for all events on the Yr warning icons repo.
- `incidentName`
  - Filter by incident.
- `geographicDomain`
  - Filter by land or sea.
- `county (fylke)`
  - Filter by Norwegian county number where available (only land areas, not gale warnings etc). The county number is defined as always having 2 digits, so the counties Østfold to Aust-Agder must be zero prefixed. Currently one single county allowed per request.
  - Note that the list of counties is undergoing several changes in the period 2018-2020. For an updated list, please see the list from Kartverket.
- `municipality (kommune)`
  - The CAP files also contain codes for the municipalities affected. The authorative source for these can be found at Kartverket. Note that many of these got new codes on Jan 1st, 2020.
- `lat, lon`
  - Coordinates for geographical search (returns all alerts covered by the search location).
  - Note: This feature is experimental, and may be changed without prior notice!
- `period`
  - This is used to get historical alerts from the archive, which is segmented by monthly intervals. Use the ISO format for months, on the form YYYY-MM. Periods outside the archive timespan will return error.
  - Legal values are: 2019-01, 2019-02, 2019-03, 2019-04, 2019-05, 2019-06, 2019-07, 2019-08, 2019-09, 2019-10, 2019-11, 2019-12, 2020-01, 2020-02, 2020-03, 2020-04, 2020-05, 2020-06, 2020-07, 2020-08, 2020-09, 2020-10, 2020-11, 2020-12, 2021-01, 2021-02.
  - Note: This feature is experimental, and may be changed without prior notice!
- `show`
  - Alerts to be listed, either "all" to include expired/updated alerts (up to one month past), or nothing to list only current alerts. If period is used, "all" is automatically enabled.
- `sort`
  - Sort RSS items on field (in ascending order).- The following fields are available:
    - area
    - event
    - incidentName
    - t_published
    - t_onset
    - type

## Examples

- https://api.met.no/weatherapi/metalerts/1.1/
- https://api.met.no/weatherapi/metalerts/1.1?lang=en
- https://api.met.no/weatherapi/metalerts/1.1?show=all
- https://api.met.no/weatherapi/metalerts/1.1/?county=42 (show alerts for Agder county)
- https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.170912185028.1114 (non-functional example - check RSS feed above for working URLs)

## Severity Table

<img src="MET_CAP-Severity_2016.png" />