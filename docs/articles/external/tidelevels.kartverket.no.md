# tidelevels.kartverket.no

## Overview

Gets tide levels from a given position.

## Endpoints

> https://kartverket.no
>
> - __GET__
>   - /api/vsl/tideLevels?latitude=`{latitude}`&longitude=`{longitude}`&language=nb

## Query Parameters

- `{latitude}`
  - The latitude coordinate.
  - *Value: (required).*
  - *Data Type: number.*
  - *Example: `-12.56789`.*
- `{longitude}`
  - The longitude coordinate.
  - *Value: (required).*
  - *Data Type: number.*
  - *Example: `-12.56789`.*

## Responses

### 200 OK

```json
{
    "result": {
        "nextHighTide": {
            "dateTime": "date-time"
        },
        "nextLowTide": {
            "dateTime": "date-time"
        }
    }
}
```