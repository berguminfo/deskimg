# flood.nve.no

## Overview

NVE har ansvaret for den nasjonale flomvarslingstjenesten som overvåker den hydrologiske tilstanden i vassdrag over hele landet, og vurderer faren for flom for de nærmeste dagene på regionalt nivå (fylkesnivå, ev. en eller flere kommuner). Formålet er å unngå tap av liv og verdier som følge av flom.

Bruk swagger for ytterligere dokumentasjon av apiet
https://api01.nve.no/hydrology/forecast/flood/v1.0.8/swagger/ui/index

## Endpoints

> https://api01.nve.no/hydrology/forecast/flood/v1.0.8
>
> - __GET__
>   - /api/Warning/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/All/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/County/`{county}`/`{langkey}`/`{startdate}`/`{enddate}`
>   - /api/Warning/Municipality/`{municipality}`/`{langkey}`/`{startdate}`/`{enddate}`

## Path Parameters

- `{county}`
- `{municipality}`
  - [The norwegian county/municipality number.](https://api01.nve.no/hydrology/forecast/flood/v1.0.8/api/Region/)
  - *Value: (required).*
  - *Data Type: string.*
- `{langkey}`
  - One of: 1 = Norwegian. 2 = English.
  - *Value: (required).*
  - *Data Type: number.*
- `{startdate}`
  - The start date (yyyy-MM-dd).
  - *Value: (required).*
  - *Data Type: string.*
- `{enddate}`
  - The end date (yyyy-MM-dd).
  - *Value: (required).*
  - *Data Type: string.*

## Responses

### 200 OK

```json
[
  {
    "Area": "Sør-Varanger",
    "CountyList": [
      {
        "HighestActivityLevel": "4",
        "Id": "20",
        "Name": "Finnmark",
      }
    ],
    "MunicipalityList": [
      {
        "Id": "2030",
        "Name": "Sør-Varanger"
      }
    ],
    "ActivityLevel": "4",
    "LevelMeaningText": "Ekstrem situasjon som forekommer svært sjelden, krever tett oppfølging og kan medføre store skader. Rødt nivå er det høyeste av våre varslingsnivåer.",
    "ValidFrom": "2017-12-22T07:00:00",
    "ValidTo": "2017-12-23T06:59:00",
    "DangerTypeName": "Flomfare",
    "MainText": "Varsel om flom, rødt nivå, for Sør-Varanger, pga. mye regn og snøsmelting",
    "WarningText": "Det ventes mye nedbør fra fredag kveld til lørdag kveld. Mesteparten av nedbøren vil falle i form av regn. Midtre strøk er mest utsatt, men plasseringen av de største nedbørmengdene er usikker.",
    "CauseList": [
      {
        "Id": "1",
        "Name": "Regn"
      }
    ]
  }
]
```

## ActivityLevel

- 0: Ikke vurdert
- 1: Liten
- 2: Moderat
- 3: Betydelig
- 4: Stor

## Cause

- 0: Ikke gitt
- 1: Regn
- 2: Intens regn (bygenedbør)
- 3: Snøsmelting
- 4: Isgang
- 5: Frost og is
- 6: Vannmetning (i jord)
- 7: Dambrudd/jøkulhlaup
