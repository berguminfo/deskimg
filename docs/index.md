<img src="assets/images/release-7.6.webp" style="float: right; height: 300px">

# Deskimg v7.6

C#v13, WASM, Generators, Notifications, MemoryCache, esbuild, Capture, Live Wallpaper and more.

<a href="CHANGELOG.md" class="btn btn-primary" style="margin-top: 2rem">Read more</a>
