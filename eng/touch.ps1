param (
    [Parameter(Position = 0, Mandatory)] $Path,
    [Parameter(Position = 1)] $Date,
    $Version
)

if ($null -eq $Version) {
    $Version = Split-Path $Path -Leaf
}
if ($null -eq $Date) {
    $Date = "{0:d}" -f (Get-Date)
}

$v = [Version]::Parse($Version)
$TimeStamp = Get-Date -Date $Date -Hour $v.Major -Minute $v.Minor -Second $v.Build

function Touch($item) {
    $item.CreationTime = $TimeStamp
    $item.LastWriteTime = $TimeStamp
    $item = gi $item.FullName
    echo "$($item.FullName.Substring((Get-Location).Path.Length + 1)): $("{0:s}" -f $item.CreationTime)"
}

gi $Path | % { Touch $_ }
gci -r $Path | % { Touch $_ }
