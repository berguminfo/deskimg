param (
    [Parameter(Position = 0)] $Target = $null,
    $OutputPath = ".dist",
    $SourcePath = "artifacts",
    $Sets = ("bundle", "wpf"),
    $TargetFramework = "net9.0",
    [string[]] $Exclude = ("appsettings.Staging.json"),
    [switch] $DryRun
)

$OutputPath = Join-Path $PSScriptRoot ".." $OutputPath

$Collections = @{
    "bundle" = @{
        Destination = "deskimg-bundle-{0}.$TargetFramework.zip";
        Files       = (
            "Deskimg.Blazor.Settings",
            "Deskimg.Blazor.WebAssembly",
            "Deskimg.Capture",
            "Deskimg.Desktop",
            "Deskimg.WebAPI.CalDAV",
            "Deskimg.WebAPI.Fetch",
            "Deskimg.WebAPI.Sets"
        )
    };
    "wpf"    = @{
        Destination = "Deskimg.App.Wpf-{0}.$TargetFramework-windows10.0.17763.0.zip";
        Files       = (
            "Deskimg.App.Hosting",
            "Deskimg.App.Wpf",
            "Deskimg.Blazor.Settings",
            "Deskimg.Blazor.WebAssembly",
            "Deskimg.WebAPI.CalDAV",
            "Deskimg.WebAPI.Fetch",
            "Deskimg.WebAPI.Sets"
        )
    };
}
if ($null -eq $Target) {
    $Target =
    Get-ChildItem $SourcePath -Directory |
    Where-Object { $_.Attributes.ToString() -NotLike "*Reparse*" } |
    Sort-Object LastWriteTime -Descending -Top 1
}
else {
    $Target = Join-Path $SourcePath $Target
}

function Invoke-Exclude() {
    $Result = @()
    gci -r $Exclude | % {
        $Item = @{
            Key   = [IO.Path]::GetTempFileName();
            Value = $_
        }
        Write-Host "Exclude: $($Item.Value)"
        if ($DryRun) {
            cp $Item.Value $Item.Key -Force
        }
        else {
            mv $Item.Value $Item.Key -Force
        }

        $Result += $Item
    }
    return $Result
}

function Invoke-Restore($Temp) {
    $Temp | % {
        Write-Host "Restore: $($_.Value)"
        if ($DryRun) {
            cp $_.Key $_.Value -Force
        }
        else {
            mv $_.Key $_.Value -Force
        }
    }
}

function Invoke-Compress($Set) {
    $DestinationPath = Join-Path $OutputPath ($Set.Destination -f (Split-Path -Leaf $Target))
    Compress-Archive -Force -Path $Set.Files -DestinationPath $DestinationPath
}

Push-Location
cd $Target
$Temp = Invoke-Exclude
$Sets | % {
    Invoke-Compress $Collections[$_]
}
if ($Temp) {
    Invoke-Restore $Temp
}
Pop-Location
