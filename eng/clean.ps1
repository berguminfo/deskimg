echo 'clean: rm -r bin,obj,node_modules,@typespec'
gci -di -r | ? name -match "^(bin|obj|node_modules|@typespec)$" | rm -r -force
