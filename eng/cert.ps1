# Source: https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/

# https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/eng/bash_pwsh.md
`                                                                    #\
param(                                                               #\
    [Parameter(Position = 0)] $DOMAIN = "localhost"                  #\
)                                                                   <#\
`
DOMAIN=${1:-localhost}
                                                                     #>

echo "Generate signed certificate for domain '$DOMAIN'."
echo ""

mkdir -p .certs/

`                                                                    #\
if (-not (Test-Path -PathType Leaf ".certs/ca.crt")) {              <#\
`
if [ ! -f .certs/ca.crt ]; then
                                                                     #>
    echo "⌛ ca.key"
    openssl genrsa -des3 -out .certs/ca.key 2048

    echo "⌛ ca.crt"
    openssl req -x509 -new -nodes -key .certs/ca.key -sha256 -days 1825 -out .certs/ca.crt

`                                                                    #\
}                                                                   <#\
`
fi
                                                                     #>

echo "⌛ $DOMAIN.key"
openssl genrsa -out .certs/$DOMAIN.key 2048

echo "⌛ $DOMAIN.csr"
openssl req -new -key .certs/$DOMAIN.key -out .certs/$DOMAIN.csr

echo "⌛ $DOMAIN.ext"
echo "" > .certs/$DOMAIN.ext
echo "authorityKeyIdentifier=keyid,issuer" >> .certs/$DOMAIN.ext
echo "basicConstraints=CA:FALSE" >> .certs/$DOMAIN.ext
echo "keyUsage=digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment" >> .certs/$DOMAIN.ext
echo "subjectAltName=@alt_names" >> .certs/$DOMAIN.ext
echo "[alt_names]" >> .certs/$DOMAIN.ext
echo "DNS.1=$DOMAIN" >> .certs/$DOMAIN.ext

echo "⌛ $DOMAIN.crt"
openssl x509 -req -in .certs/$DOMAIN.csr -CA .certs/ca.crt -CAkey .certs/ca.key -CAcreateserial -out .certs/$DOMAIN.crt -days 825 -sha256 -extfile .certs/$DOMAIN.ext

echo "⌛ $DOMAIN.pfx"
openssl pkcs12 -export -out .certs/$DOMAIN.pfx -inkey .certs/$DOMAIN.key -in .certs/$DOMAIN.crt
