# https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/eng/bash_pwsh.md
`                                                                    #\
$BEL = [char]0x07                                                    #\
$ESC = [char]0x1b                                                    #\
$ST = "$ESC\"                                                        #\
$OSC9 = "$ESC]9"                                                     #\
Write-Host -NoNewline "$OSC9;4;0;$ST"                                #\
$Platform = [Environment]::Is64BitOperatingSystem ? "x64" : "x86"    #\
$AdditionalOptions = $args                                          <#\
`
Platform=x64 # unused
AdditionalOptions=$@
                                                                     #>
dotnet build --no-restore -p:Platform=$Platform $AdditionalOptions
`                                                                    #\
if (!$?) { Write-Host -NoNewline "$OSC9;4;2;100;$ST$BEL" }           #\
`
