<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" media-type="text/plain" />

  <xsl:variable name="CRLF"><xsl:text>&#xD;&#xA;</xsl:text></xsl:variable>

  <xsl:template match="/" xmlns:t="http://microsoft.com/schemas/VisualStudio/TeamTest/2010">
    <xsl:for-each select="//t:TestDefinitions/t:UnitTest/t:TestMethod[not(@name=preceding::t:TestMethod/@name)]">
      <xsl:variable name="target"><xsl:value-of select="concat(@className, '.', @name)" /></xsl:variable>
      <xsl:value-of select="concat($target, ':', $CRLF)" />
      <xsl:for-each select="//t:UnitTestResult[starts-with(@testName, $target)]">
        <xsl:value-of select="concat(t:Output/t:StdOut, $CRLF)" />
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
