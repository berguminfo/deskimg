# https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/eng/bash_pwsh.md
`                                                                    #\
$BEL = [char]0x07                                                    #\
$ESC = [char]0x1b                                                    #\
$ST = "$ESC\"                                                        #\
$OSC9 = "$ESC]9"                                                     #\
$Platform = [Environment]::Is64BitOperatingSystem ? "x64" : "x86"    #\
$AdditionalOptions = $args                                           #\
Write-Host -NoNewline "$OSC9;3;0;$ST"                               <#\
`
Platform=x64 # unused
AdditionalOptions=$@
                                                                     #>
dotnet msbuild -t:PublishAll -p:Configuration=Release -p:Platform=$Platform $AdditionalOptions
#dotnet publish -c Release src/blazor/Deskimg.Blazor.WebAssembly/Deskimg.Blazor.WebAssembly.csproj -p:PublishTargetFramework=browser
`                                                                    #\
if (!$?) {                                                           #\
    Write-Host -NoNewline "$OSC9;4;2;100;$ST$BEL"                    #\
} else {                                                             #\
    Write-Host -NoNewline "$OSC9;4;0;$ST"                            #\
}                                                                    #\
`
