// mkreadme.js. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

const fs = require("node:fs");

function type(prop, name, schema) {
    if (prop === undefined) {
        throw new Error(`Argument 'prop' is undefined having name '${name}'`);
    }

    if (typeof(prop.$ref) === "string") {
        const path = prop.$ref.split("/").slice(1);
        const ref = path.reduce((accumulator, currentValue) =>
            (accumulator && accumulator[currentValue] !== undefined) ? accumulator[currentValue] : undefined, schema);
        return type(ref, path.slice(-1)[0], schema);
    } else
    if (Array.isArray(prop.enum)) {
        return [name, name, prop.enum.join(", ")];
    } else
    if (prop.type === "array") {
        const tmp = type(prop.items, name, schema);
        return [`${tmp[0]}[]`, `${tmp[1]}[]`];
    } else
    if (prop.type === "boolean") {
        return ["bool", "Boolean"];
    } else
    if (prop.type === "number" && prop.maximum !== undefined) {
        return ["float", "Float"];
    } else
    if (prop.type === "number") {
        return ["double", "Double"];
    } else
    if (prop.type === "integer" && prop.minimum !== undefined && prop.maximum !== undefined) {
        return ["ushort", "UInt16"];
    } else
    if (prop.type === "integer" && prop.maximum !== undefined) {
        return ["short", "Int16"];
    } else
    if (prop.type === "integer" && prop.minimum !== undefined) {
        return ["uint", "UInt32"];
    } else
    if (prop.type === "integer") {
        return ["int", "Int32"];
    } else
    if (prop.type === "string" && prop.format && prop.format === "time") {
        return ["TimeSpan", "TimeSpan"];
    } else
    if (prop.type === "string") {
        return ["string", "String"];
    } else {
        throw new Error("Missing type mapping.", { cause: prop });
    }
}

function parse(path) {
    const doc = [];
    const schema = JSON.parse(fs.readFileSync(path, "utf8"));

    // # Title
    doc.push(`# ${schema.title}`);
    doc.push("");

    // ## Description
    doc.push(schema.description);
    doc.push("");

    // ## Syntax
    doc.push("## Syntax");
    doc.push("");
    doc.push("```sh");
    doc.push(schema.title);

    function syntax(properties, prefix = "") {

        // [--Name] <Type>
        for (const [name, prop] of Object.entries(properties)) {
            const fullName = prefix.length > 0 ? `${prefix}:${name}` : name;

            if (prop.type === "object") {
                if (!prop.properties) {
                    throw new Error(`The node named '${fullName}' missing a properties object.`);
                }
                syntax(prop.properties, fullName);
            } else
            if (prop.type === "array" && prop.items.type === "object") {
                syntax(prop.items.properties, `${fullName}:0`);
            } else {
                doc.push(`  [--${fullName} <${type(prop, name, schema)[0]}>]`);
            }

        }
    }

    syntax(schema.properties);

    doc.push("```");
    doc.push("");
    doc.push("👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)");
    doc.push("");

    // ## Examples
    if (Array.isArray(schema.examples)) {
        doc.push("## Examples");
        doc.push("");
        for (let i = 0; i < schema.examples.length; ++i) {
            const example = schema.examples[i];
            doc.push(`### Example ${i + 1}: ${example.title}`);
            doc.push("");
            if (typeof(example.description) === "string") {
                doc.push(example.description);
                doc.push("");
            }
            doc.push("```sh");
            doc.push(example.command);
            doc.push("```");
            doc.push("");
        }
    }

    // ## Parameters
    doc.push("## Parameters");
    doc.push("");

    function parameters(properties, required, prefix = "") {
        for (const [name, prop] of Object.entries(properties)) {
            const fullName = prefix.length > 0 ? `${prefix}:${name}` : name;

            if (prop.type === "object") {
                if (!prop.properties) {
                    throw new Error(`The node named '${fullName}' missing a properties object.`);
                }
                parameters(prop.properties, prop.required || [], fullName);
            } else
            if (prop.type === "array" && prop.items.type === "object") {
                parameters(prop.items.properties, prop.items.required || [], `${fullName}:0`);
            } else {

                // ### --Type
                doc.push(`### --${fullName}`);
                doc.push("");
                if (typeof(prop.description) === "string") {
                    doc.push(prop.description.replace(/\t/g, "  "));
                    doc.push("");
                }
                if (prop.$defs && Array.isArray(prop.$defs.MessageFormat)) {
                    doc.push("The following substitutions are available for use.");
                    doc.push("");
                    for (const line of prop.$defs.MessageFormat) {
                        doc.push(line);
                    }
                    doc.push("");
                }

                const typeValue = type(prop, name, schema);
                const table = [
                    ["", ""],
                    ["-", "-"]
                ];

                // | Type: | Type |
                table.push(["Type:", typeValue[1]]);
                // | Accepted values: | Values |
                if (typeValue.length > 2) {
                    table.push(["Accepted values:", typeValue[2]]);
                }
                // | Default value: | Value |
                if (prop.default !== undefined) {
                    let defaultValue = prop.default.toString();
                    defaultValue = defaultValue[0].toUpperCase() + defaultValue.slice(1);
                    table.push(["Default value:", defaultValue]);
                }
                // | Required: | True, False |
                table.push(["Required:", required.includes(name) ? "True" : "False"])

                for (const row of table) {
                    const padString = row[0] === "-" ? "-" : " ";
                    doc.push(`| ${row[0].padEnd(16, padString)} | ${row[1].padEnd(57, padString)} |`);
                }
                doc.push("");
            }
        }
    }

    parameters(schema.properties, schema.required || []);

    return doc.join("\n");
}

try {
    let outputPath = "README.md";
    if (process.argv.length > 2) {
        outputPath = process.argv[2];
    }
    let schemaPath = "appsettings.schema.json";
    if (process.argv.length > 3) {
        schemaPath = process.argv[3];
    }

    const readme = parse(schemaPath);
    if (outputPath === "-") {
        console.log(readme);
    } else {
        fs.writeFileSync(outputPath, readme);
        console.log(`mkreadme: saved '${outputPath}'`);
    }
} catch (e) {
    console.error(e);
}
