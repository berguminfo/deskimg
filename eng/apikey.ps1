# script to create text to embed into `M:FetchToken.Create` documentation

# table header
"/// | SHR | Ticks      | Interval |"
"/// |-----|------------|----------|"
# table body
(24..35) | % {
    $t = [TimeSpan]::new([Math]::Pow(2, $_) - 1)
    "/// | $_  | {0:x9}ₓ | {1:mm\mss\s}   |" -f $t.Ticks, $t
}
