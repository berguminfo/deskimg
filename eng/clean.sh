#!/usr/bin/env sh
echo 'clean: rm -r bin,obj,node_modules,@typespec'
find . -iname bin -print0 | xargs -0 rm -rf
find . -iname obj -print0 | xargs -0 rm -rf
find . -iname node_modules -print0 | xargs -0 rm -rf
find . -iname @typespec -print0 | xargs -0 rm -rf
