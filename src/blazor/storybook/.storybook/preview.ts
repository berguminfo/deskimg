import type { Preview } from "@storybook/react";
import { withThemeByDataAttribute } from "@storybook/addon-themes";
import "../../Deskimg.Blazor.Components/wwwroot/lib/bootstrap/css/bootstrap.min.css";
import "../../Deskimg.Blazor.Components/wwwroot/css/site.css";

const preview: Preview = {
    parameters: {
        backgrounds: {
            default: "dark",
        },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/i,
            },
        },
    },
};

export const decorators = [
    withThemeByDataAttribute({
        themes: {
            light: "light",
            dark: "dark",
        },
        defaultTheme: "dark",
        attributeName: "data-bs-theme",
    }),
];

export default preview;
