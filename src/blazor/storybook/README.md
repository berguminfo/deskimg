
# Single (') or Double (") quote

The issue is that TypeScript allows both ' and " to quote strings.
Nothing special about that since JavaScript take this practice but not Java.
The most important thing for all project is for developers to agree on an
common coding style. In the project lifetime this style should never change.
In case that is to be, another project should be created.

Some comments collected:
- React developers prefer single quote.
- TypeScript developers prefer double quote, ref: [Use double quotes for strings](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines).
- C# requires double quotes and single quotes for a `char`.
- JSON requires double quotes.

Hence for this project the agreement is to use double (") quotes for strings!

# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
  },
}
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
