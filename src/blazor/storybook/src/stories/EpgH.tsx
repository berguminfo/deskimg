import "../../../Deskimg.Blazor.Components/Components/EpgH.razor.css";
import Icon1 from "../../../Deskimg.Blazor.Foundation/Resources/Epg/nrk1.nrk.no.svg?react";
import Icon2 from "../../../Deskimg.Blazor.Foundation/Resources/Epg/nrk2.nrk.no.svg?react";

interface EpgProps {
    channels: {
        icon: number,
        events: {
            when: Date,
            duration: number,
            summary?: string | undefined,
            description?: string | undefined,
            attachment?: string | undefined,
            category?: string[] | undefined,
        }[]
    }[];
}

type Rect = { id: number, x: number, y: number, width: number, height: number };

export const EpgH = ({
    channels,
}: EpgProps) => {

    const timeFormat = new Intl.DateTimeFormat("no-NB", {
        hour: "2-digit",
        minute: "2-digit",
    });

    const start = Date.parse("2024-04-17T18:00:00");
    const resolution = 15 * 1000; // number of milliseconds per pixel

    function move(rect: Rect, event: { when: Date, duration: number }, originX = 80) {
        rect.id++;
        rect.x = originX + (event.when.valueOf() - start.valueOf()) / resolution;
        rect.width = event.duration / resolution;
        if (rect.x < originX) {
            rect.width -= (Math.abs(rect.x) + originX);
            rect.x = originX;
        }
    }

    function getIcon(n: number) {
        switch (n) {
            case 1: return <Icon1 />;
            case 2: return <Icon2 />;
        }
    }

    const content = [];
    const rect = { id: 0, x: 0, y: 0, width: 0, height: 40 };
    for (const channel of channels) {
        content.push(
            <g key={`icon__${rect.id}`} transform={`translate(0 ${rect.y + 10})`}>
                {getIcon(channel.icon)}
            </g>
        );
        for (const event of channel.events) {
            move(rect, event);
            content.push(
                <g key={`event__${rect.id}`}>
                    <g clipPath={`url(#clip__${rect.id})`}>
                        <rect x={rect.x} y={rect.y} width={rect.width} height={rect.height} stroke="var(--bs-border-color)" fill="transparent" />
                        <text x={rect.x} y={rect.y + (rect.height / 2)}>
                            <tspan dy="7" fill="var(--bs-body-color)">{timeFormat.format(event.when)} {event.summary}</tspan>
                        </text>
                    </g>
                    <clipPath id={`clip__${rect.id}`}>
                        <rect x={rect.x} y={rect.y} width={rect.width} height={rect.height} />
                    </clipPath>
                </g>
            );
        }

        // move to next line
        rect.x = 0;
        rect.y += rect.height;
    }
    return (
        <div className="component epg-component">
            <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
                {content}
            </svg>
        </div>
    );
};
