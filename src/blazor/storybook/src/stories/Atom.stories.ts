import type { Meta, StoryObj } from "@storybook/react";
import { Atom } from "./Atom";

const meta = {
    title: "Components/Atom",
    component: Atom,
    tags: ["autodocs"],
    args: {
        publishDate: "2024-04-16T12:34:56",
        title: "Telefonsalgselskap drev «uredelig»",
        summary: "HMS Vakten har tatt hundrevis av kunder til forliksrådet. Frank Ove Nilsen nektet å la seg presse eller betale for tjeneste han ikke fikk, og vant frem med sitt søksmål."
    },
} satisfies Meta<typeof Atom>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Collapsed: Story = {
    args: {
        expanded: false,
    }
};

export const Expanded: Story = {
    args: {
        expanded: true,
    }
};
