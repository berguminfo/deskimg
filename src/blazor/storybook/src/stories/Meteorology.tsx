import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/Meteorology.razor.css";

interface MeteorologyProps {
    type: string;
    conditionImageStyle?: CSSProperties;
    windImageStyle?: CSSProperties;
    style?: CSSProperties | undefined;
    validity?: string[] | undefined;
    temperature?: number[] | undefined;
    condition?: string[] | undefined;
    ultravioletIndexSeverity?: string | undefined;
    ultravioletIndexTitle?: string | undefined;
    precipitation?: number[] | undefined;
    windDirection?: number[] | undefined;
    windSpeed?: number[] | undefined;
}

export const Meteorology = ({
    type,
    conditionImageStyle = {
        "width": "2em",
        "height": "2em"
    },
    windImageStyle = {
        "width": "1.5em",
        "height": "1.5em"
    },
    style = undefined,
    validity = undefined,
    temperature = undefined,
    condition = undefined,
    ultravioletIndexSeverity = undefined,
    ultravioletIndexTitle = undefined,
    precipitation = undefined,
    windDirection = undefined,
    windSpeed = undefined,
}: MeteorologyProps) => {

    function createWindArrow(direction: number) {
        return `
            <svg viewBox="0 0 24 24" width="${windImageStyle.width}" height="${windImageStyle.height}" xmlns="http://www.w3.org/2000/svg">
                <g transform="rotate(${direction} 10.5 10.5)">
                    <path d="M11.53 3l-.941 12.857L7 15l5.001 6L17 15l-3.587.857L12.471 3h-.941z" clip-rule="evenodd" fill-rule="evenodd" fill="currentcolor"></path>
                </g>
            </svg>`;
    }

    return (
        <div className="component meteorology-component" style={style}>
            <table className="table table-sm">
                <tbody>
                    <tr>
                        {validity && validity.map(element =>
                            <td className={`${type} when shadowed`}>
                                {element}
                            </td>
                        )}
                    </tr>
                    <tr>
                        {temperature && temperature.map(element =>
                            <td className={`${type} temperature shadowed`}>
                                <span title="{element}">
                                    {element}°
                                </span>
                            </td>
                        )}
                    </tr>
                    <tr>
                        {condition && condition.map(element =>
                            <td className={`${type} conditionImage`}>
                                <div className={ultravioletIndexSeverity}>
                                    <img src={`/images/weather/310/${element}.svg`}
                                        title={ultravioletIndexTitle}
                                        alt={element}
                                        style={conditionImageStyle} />
                                </div>
                            </td>
                        )}
                    </tr>
                    <tr>
                        {precipitation && precipitation.map(element =>
                            <td className={`{type} precipitation shadowed`}>
                                <span title="{element}">
                                    {element > 0 ? element : ""}
                                </span>
                            </td>
                        )}
                    </tr>
                    <tr>
                        {windDirection && windDirection.map(element =>
                            <td className={`${type} windImage`}>
                                <span title="@(directionDisplay)"
                                    dangerouslySetInnerHTML={{ __html: createWindArrow(element) }} />
                            </td>
                        )}
                    </tr>
                    <tr>
                        {windSpeed && windSpeed.map(element =>
                            <td className={`${type} windSpeed shadowed`}>
                                <span className={element >= 18.0 ? "fw-bold" : ""}>
                                    {element}
                                </span>
                            </td>
                        )}
                    </tr>
                </tbody>
            </table>
        </div >
    );
};
