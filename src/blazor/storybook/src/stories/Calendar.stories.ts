import type { Meta, StoryObj } from "@storybook/react";
import { Calendar } from "./Calendar";

const meta = {
    title: "Components/Calendar",
    component: Calendar,
    tags: ["autodocs"],
    args: {
    },
} satisfies Meta<typeof Calendar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const First: Story = {
    args: {
        summary: "Prinsesse Noe"
    }
};
