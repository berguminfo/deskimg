interface ErrorPageProps {
    statusCode: number;
}

export const ErrorPage = ({
    statusCode,
}: ErrorPageProps) => {
    return (
        <a className="btn btn-link" href={`/${statusCode}.html`} target="_blank">
            Click to open '/{statusCode}.html'
        </a>
    );
};
