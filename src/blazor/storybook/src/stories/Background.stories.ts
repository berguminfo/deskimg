import type { Meta, StoryObj } from "@storybook/react";
import { Background } from "./Background";

const meta = {
    title: "Components/Background",
    component: Background,
    tags: ["autodocs"],
    args: {
    },
} satisfies Meta<typeof Background>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Combine1: Story = {
    args: {
        items: [
            {
                source: "408-1920w-light.webp",
                opacity: 1.0
            }
        ]
    }
};

export const Combine2: Story = {
    args: {
        items: [
            {
                source: "408-1920w-light.webp",
                opacity: 1.0
            },
            {
                source: "408-1920w-dark.webp",
                opacity: 0.5
            }
        ]
    }
};

export const Combine3: Story = {
    args: {
        items: [
            {
                source: "408-1920w-dark.webp",
                opacity: 1.0
            }
        ]
    }
};

export const Video: Story = {
    args: {
        items: [
            {
                source: "bkg/video.mp4",
                type: "video/mp4",
                opacity: 1.0
            }
        ]
    }
};
