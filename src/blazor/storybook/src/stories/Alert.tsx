import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/Alert.razor.css";

interface AlertProps {
    expanded: boolean;
    collapseId?: string;
    style?: CSSProperties | undefined;
    severity?: string | undefined;
    symbol?: string | undefined;
    validity?: string | undefined;
    title?: string | undefined;
    summary?: string | undefined;
}

export const Alert = ({
    expanded,
    collapseId = "wrn__001",
    style = undefined,
    severity = undefined,
    symbol = undefined,
    title = undefined,
    validity = undefined,
    summary = undefined,
}: AlertProps) => {
    return (
        <div className={`item bg-severity-${severity}`} style={style}>
            <button className="btn btn-link shadowed" data-bs-toggle="collapse"
                data-bs-target={`.${collapseId}`} aria-expanded={expanded}>
                {symbol &&
                    <img className="enclosure" src={`/images/warning/001/${symbol}.svg`} />
                }
                {title &&
                    <span className="title">
                        {title}
                    </span>
                }
            </button>
            <div className={`${collapseId} collapse ${expanded && "show" || ""}`}>
                <div className="summary shadowed">
                    <div>
                        <time dateTime={validity}>
                            {validity}
                        </time>
                    </div>
                    <div>
                        {summary}
                    </div>
                </div>
            </div>
        </div>
    );
};
