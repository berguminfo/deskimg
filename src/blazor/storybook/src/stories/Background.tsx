type CompositeItem = {
    source?: string;
    type?: string;
    opacity?: number;
};

interface BackgroundProps {
    items?: CompositeItem[];
}

export const Background = ({
    items = [],
}: BackgroundProps) => {
    const content = [];
    for (const item of items) {
        if (typeof item.type === "string" && item.type.startsWith("video")) {
            content.push(
                <video className="background-item" autoPlay loop playsInline muted>
                    <source src={`/caldav/${item.source}`} type="video/mp4" />
                </video>
            );
        } else {
            content.push(
                <picture>
                    <source srcSet={`/images/background/${item.source}`} />
                    <img className="background-item"
                        src={`/images/background/${item.source}`}
                        style={{ opacity: item.opacity }} />
                </picture>
            );
        }
    }
    return (
        [content]
    );
};
