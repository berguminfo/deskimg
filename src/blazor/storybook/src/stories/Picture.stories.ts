import type { Meta, StoryObj } from "@storybook/react";
import { Picture } from "./Picture";

const meta = {
    title: "Components/Picture",
    component: Picture,
    tags: ["autodocs"],
} satisfies Meta<typeof Picture>;

export default meta;
type Story = StoryObj<typeof meta>;

export const First: Story = {
    args: {
        source: "/sample-data/gfx.nrk.no_eJxFy00OwiAQQOETdYafguJOPYIL15MWCwkOBAgJtzcu.jpg",
        title: "Title",
    }
};
