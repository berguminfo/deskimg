import type { Meta, StoryObj } from "@storybook/react";
import { Inline } from "./Inline";

const meta = {
    title: "Components/Inline",
    component: Inline,
    tags: ["autodocs"],
    args: {
    },
} satisfies Meta<typeof Inline>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Text: Story = {
    args: {
        value: "Text: {{Version}}",
    }
};

export const Markup: Story = {
    args: {
        value: "<b>Markup: {{YearMonth}}</b>",
    }
};
