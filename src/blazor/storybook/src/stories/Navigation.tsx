import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/Navigation.razor.css";
import X from "../../../Deskimg.Blazor.Foundation/Resources/Action/x.svg?react";
import ChevronLeft from "../../../Deskimg.Blazor.Foundation/Resources/Action/chevron-left.svg?react";
import ChevronRight from "../../../Deskimg.Blazor.Foundation/Resources/Action/chevron-right.svg?react";
import CalendarTime from "../../../Deskimg.Blazor.Foundation/Resources/Action/calendar-time.svg?react";
import World from "../../../Deskimg.Blazor.Foundation/Resources/Action/world.svg?react";
import Search from "../../../Deskimg.Blazor.Foundation/Resources/Action/search.svg?react";

interface NavigationProps {
    modal: boolean;
    navbarId?: string;
    modalId?: string;
    durationDataListId?: string;
    group1Id?: string;
    group2Id?: string;
    style?: CSSProperties | undefined;
    realTimeClock?: boolean;
    clippingButtons?: string[] | undefined;
}

export const Navigation = ({
    modal,
    navbarId = "nav__001",
    modalId = "nav__002",
    durationDataListId = "lst__001",
    group1Id = "grp__001",
    group2Id = "grp__002",
    style = undefined,
    realTimeClock = true,
    clippingButtons = ["sm", "md", "lg", "xl", "∞"],
}: NavigationProps) => {

    function durationChanged() { }
    function changeToiClicked() { }
    function nowClicked() { }
    function clearClicked() { }
    function openModal() { }
    function closeModal() { }
    function useMarkedClicked() { }
    function toggleMapVisibility() { }
    function displayMapClicked() {

    }

    if (!modal) {
        return (
            <nav className="component navigate-component navbar navbar-expand-sm" style={style}>
                <div className="container-fluid p-0">
                    <button className="navbar-toggler mb-1" type="button" data-bs-toggle="collapse"
                        data-bs-target={`#${navbarId}`}
                        aria-controls={navbarId} aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <ul id={navbarId} className="navbar-nav collapse">
                        <li className="nav-item mb-1 d-sm-none">
                            <div className="input-group">
                                <input className="form-control"
                                    onInput={durationChanged}
                                    list={durationDataListId} />
                                <button className="btn btn-outline-light icon-link px-1"
                                    title="Backward"
                                    onClick={changeToiClicked}>
                                    <ChevronLeft />
                                </button>
                                <button className="btn btn-outline-light icon-link px-1"
                                    title="Forward"
                                    onClick={changeToiClicked}>
                                    <ChevronRight />
                                </button>
                            </div>
                        </li>
                    </ul>
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav me-auto mb-0 mb-lg-0">
                            <li className="nav-item mx-1">
                                <button className={`btn btn-outline-success ${realTimeClock && "active"}`}
                                    onClick={nowClicked}>
                                    Now
                                </button>
                            </li>
                            <li className="nav-item mx-2">
                                <div className="input-group">
                                    <span className="input-group-text">
                                        Duration
                                    </span>
                                    <input className="form-control form-control-internal"
                                        onInput={durationChanged}
                                        list={durationDataListId} />
                                    <button className="btn btn-outline-light btn-internal icon-link px-1"
                                        onClick={clearClicked}>
                                        <X />
                                    </button>
                                    <button className="btn btn-outline-light icon-link px-2"
                                        title="Backward"
                                        onClick={changeToiClicked}>
                                        <ChevronLeft />
                                    </button>
                                    <button className="btn btn-outline-light icon-link px-2"
                                        title="Forward"
                                        onClick={changeToiClicked}>
                                        <ChevronRight />
                                    </button>
                                </div>
                            </li>
                        </ul>
                        <div className="d-flex">
                            <div className="input-group">
                                <button className="btn btn-outline-secondary text-nowrap me-2" data-bs-toggle="modal"
                                    data-bs-target={`#${modalId}`}
                                    title="DetailsTitle"
                                    onClick={openModal}>
                                    <div className="icon-link">
                                        <CalendarTime />
                                        <div className="d-none d-md-block">
                                            31.12.2020
                                            14:55
                                        </div>
                                    </div>
                                    <div className="vr mx-1"></div>
                                    <div className="icon-link">
                                        <World />
                                        <div className="d-none d-lg-block">
                                            69°18′15″N 19°22′44″W
                                        </div>
                                    </div>
                                </button>
                            </div>
                            <div className="jusify-content-right" style={{ maxWidth: "20rem" }}>
                                <form className="d-flex" role="search">
                                    <div className="input-group flex-nowrap">
                                        <div className="input-group-text px-2">
                                            <Search />
                                        </div>
                                        <input name="q" className="form-control me-1" type="search" aria-label="Search"
                                            placeholder="Search" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <button className="btn btn-close ms-2" aria-label="Close"
                            title="@SR.Default.Modal.Close"></button>
                    </div>
                </div>
            </nav>
        );
    } else {
        /*
        return (
            <div className="_modal _fade">
                <div className="modal-dialog modal-dialog-scrollable">
                modalh
                </div>

            </div>
        );
        */
        return (
            <div id={modalId} className="__modal __fade" tabIndex={-1} aria-hidden={true}>
                <div className="modal-dialog modal-dialog-scrollable">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5">
                                DetailsTitle
                            </h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div data-toggle="@_group1">
                                <div className="row">
                                    <div className="col">
                                        <label className="col-form-label" htmlFor="date" id="date-label">
                                            Date
                                        </label>
                                        <input autoFocus={true} id="date" className="form-control" type="date" aria-describedby="date-label"
                                        />
                                    </div>
                                    <div className="col">
                                        <label className="col-form-label" htmlFor="time" id="time-label">
                                            Time
                                        </label>
                                        <input id="time" className="form-control" type="time" aria-describedby="time-label" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <label className="col-form-label" htmlFor="latitude" id="latitude-label">
                                            Latitude
                                        </label>
                                        <input id="latitude" className="form-control" type="number" min="-90" max="90" step="1" aria-describedby="latitude-label" />
                                    </div>
                                    <div className="col">
                                        <label className="col-form-label" htmlFor="longitude" id="longitude-label">
                                            Longitude
                                        </label>
                                        <input id="longitude" className="form-control" type="number" min="-180" max="180" step="1" aria-describedby="longitude-label" />
                                    </div>
                                    <div className="col-auto align-self-end mb-1">
                                        <button className="btn btn-outline-light btn-sm py-1 px-2"
                                                onClick={displayMapClicked}>
                                            <img src="/lib/leaflet/images/marker-icon.png" style={{ "height": "1rem" }} />
                                        </button>
                                    </div>
                                </div>
                                <hr />
                                <div className="btn-group" role="group">
                                    {clippingButtons && clippingButtons.map(element =>
                                        <button className="btn btn-outline-info @active" aria-describedby={`${element}-label`}
                                                onClick={() => { }}>
                                        {element}
                                    </button>
                                    )}
                                </div>
                            </div>
                            <div data-toggle="@_group2" className="d-none">
                                <div id="@_mapId" style={{ "height": "20rem" }}></div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <div data-toggle={group1Id}>
                                <button type="submit" className="btn btn-primary" data-bs-dismiss="modal"
                                        onClick={closeModal}>
                                    Close
                                </button>
                            </div>
                            <div data-toggle={group2Id} className="d-none">
                                <button className="btn btn-success"
                                        onClick={useMarkedClicked}>
                                    Use
                                </button>
                                <button className="btn btn-warning ms-2"
                                        onClick={toggleMapVisibility}>
                                    Back
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};
