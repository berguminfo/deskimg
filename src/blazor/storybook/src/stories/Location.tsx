import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/Location.razor.css";
import SunRise from "../../../Deskimg.Blazor.Foundation/Resources/Status/sun-rise.svg?react";
import SunSet from "../../../Deskimg.Blazor.Foundation/Resources/Status/sun-set.svg?react";
import MoonAboveHorizon from "../../../Deskimg.Blazor.Foundation/Resources/Status/moon-above-horizon.svg?react";
import TideHigh from "../../../Deskimg.Blazor.Foundation/Resources/Status/tide-high.svg?react";
import TideLow from "../../../Deskimg.Blazor.Foundation/Resources/Status/tide-low.svg?react";

interface LocationProps {
    collapseId?: string;
    style?: CSSProperties | undefined;
    viewData?: { icon: any, value?: string }[][] | undefined;
}

export const Location = ({
    collapseId = "loc__001",
    style = undefined,
    viewData = [
        [{ icon: <SunRise />, value: "08:23" }, { icon: <SunSet />, value: "20:19" }],
        [{ icon: <MoonAboveHorizon /> }],
        [{ icon: <TideHigh />, value: "10:33" }, { icon: <TideLow />, value: "13:01" }]
    ],
}: LocationProps) => {
    return (
        <div className="component location-component" style={style}>
            {viewData && viewData.map(row =>
                <div className="d-flex flex-row align ">
                    {row.map(element =>
                        <button className="btn btn-link icon-link w-50 shadowed" data-bs-toggle="collapse"
                                data-bs-target={`.${collapseId}`}>
                            {element.icon}
                            {element.value &&
                                <time dateTime={element.value}>
                                    {element.value}
                                </time>
                            }
                        </button>
                    )}
                </div>
                /*
                <div id="@collapse" className="chart collapse">
                    <div id="@chart" style={{"height": "6rem"}} />
                </div>
                */
        )}
        </div>
    );
};
