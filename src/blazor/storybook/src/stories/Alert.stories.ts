import type { Meta, StoryObj } from "@storybook/react";
import { Alert } from "./Alert";

const meta = {
    title: "Components/Alert",
    component: Alert,
    tags: ["autodocs"],
    args: {
        validity: "2021-01-21T05:00:00+00:00",
        title: "Ekstremværet Frank: Sterk ising på skip, oransje nivå.",
        summary: "Lokalt moderat til sterk ising er ventet på båter i kyst- og fjordstrøkene.",
    },
} satisfies Meta<typeof Alert>;

export default meta;

type Story = StoryObj<typeof meta>;

const extremeProps = {
    severity: "extreme",
    symbol: "extreme",
};

export const ExtremeCollapsed: Story = {
    args: {
        expanded: false,
        ...extremeProps,
    }
};

export const ExtremeExpanded: Story = {
    args: {
        expanded: true,
        ...extremeProps,
    }
};
