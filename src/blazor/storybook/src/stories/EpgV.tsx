import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/EpgV.razor.css";
import Icon from "../../../Deskimg.Blazor.Foundation/Resources/Epg/nrk1.nrk.no.svg?react";

interface EpgProps {
    expanded: boolean;
    collapseId?: string;
    style?: CSSProperties | undefined;
    when?: string | undefined;
    duration?: string | undefined;
    summary?: string | undefined;
    description?: string | undefined;
    attachment?: string | undefined;
    category?: string[] | undefined;
}

export const EpgV = ({
    expanded,
    collapseId = "epg__001",
    style = undefined,
    when = undefined,
    duration = undefined,
    summary = undefined,
    description = undefined,
    attachment = undefined,
    category = undefined,
}: EpgProps) => {
    return (
        <div className="component epg-component" style={style}>
            <div className="channel shadowed">
                <Icon />
            </div>
            <div className="programmes">
                <div className="item">
                    <button className="btn btn-link shadowed" data-bs-toggle="collapse"
                        data-bs-target={`.${collapseId}`} aria-expanded={expanded}>
                        <div className="title">
                            <time dateTime={when}>
                                <span>
                                    {when}
                                </span>
                                {duration &&
                                    <span className={`${collapseId} collapse ${expanded && "show" || ""}`}>
                                        <span className="mx-1">-</span>
                                        <span>
                                            {duration}
                                        </span>
                                    </span>
                                }
                            </time>
                            <span>
                                {summary}
                            </span>
                        </div>
                    </button>
                    <div className={`${collapseId} collapse ${expanded && "show" || ""}`}>
                        <img className="enclosure" src={attachment} />
                        <div className="summary shadowed">
                            {description}
                            {category &&
                                <p className="category">
                                    {category.map(element => <kbd>{element}</kbd>)}
                                </p>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
