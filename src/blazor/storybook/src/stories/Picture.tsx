import "../../../Deskimg.Blazor.Components/Components/Picture.razor.css";

interface PictureProps {
    opacity?: number;
    source?: string | undefined;
    title?: string | undefined;
}

export const Picture = ({
    opacity = 1.0,
    source = undefined,
    title = undefined,
}: PictureProps) => {
    return (
        <div className="component picture-component" style={{ opacity: opacity }}>
            <a href={source} target="_blank">
                <img alt={title} title={title} className="" src={source} />
            </a>
        </div>
    );
};
