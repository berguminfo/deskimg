import type { Meta, StoryObj } from "@storybook/react";
import { EpgH } from "./EpgH";

const seconds = (n: number) => n * 1000;
const minutes = (n: number) => n * seconds(60);
const hours = (n: number) => n * minutes(60);

const meta = {
    title: "Components/EpgH",
    component: EpgH,
    tags: ["autodocs"],
    args: {
        channels: [
            {
                icon: 1,
                events: [
                    {
                        when: new Date(Date.parse("2024-04-17T18:00:00")),
                        duration: minutes(30),
                        summary: "Dagsnytt 18 Dagens viktigste debatter",
                        description: "Dagens viktigste debatter, intervjuer og kommentarer direkte fra radiostudio.",
                        attachment: "/sample-data/gfx.nrk.no_iyE4am67ZursSlh8BZt9VQL4KWhNahFKtOCYyceTpkUw.jpg",
                        category: ["Nyheter"],
                    },
                    {
                        when: new Date(Date.parse("2024-04-17T18:30:00")),
                        duration: hours(1) + minutes(15),
                        summary: "Dagsnytt 18 Dagens viktigste debatter",
                        description: "Dagens viktigste debatter, intervjuer og kommentarer direkte fra radiostudio.",
                        attachment: "/sample-data/gfx.nrk.no_iyE4am67ZursSlh8BZt9VQL4KWhNahFKtOCYyceTpkUw.jpg",
                        category: ["Nyheter"],
                    }
                ],
            },
            {
                icon: 2,
                events: [
                    {
                        when: new Date(Date.parse("2024-04-17T17:00:00")),
                        duration: hours(1) + minutes(45),
                        summary: "Dagsnytt 18 Dagens viktigste debatter",
                        description: "Dagens viktigste debatter, intervjuer og kommentarer direkte fra radiostudio.",
                        attachment: "/sample-data/gfx.nrk.no_iyE4am67ZursSlh8BZt9VQL4KWhNahFKtOCYyceTpkUw.jpg",
                        category: ["Nyheter"],
                    },
                    {
                        when: new Date(Date.parse("2024-04-17T18:50:00")),
                        duration: hours(1) + minutes(15),
                        summary: "Dagsnytt 18 Dagens viktigste debatter",
                        description: "Dagens viktigste debatter, intervjuer og kommentarer direkte fra radiostudio.",
                        attachment: "/sample-data/gfx.nrk.no_iyE4am67ZursSlh8BZt9VQL4KWhNahFKtOCYyceTpkUw.jpg",
                        category: ["Nyheter"],
                    }
                ],
            },
        ],
    },
} satisfies Meta<typeof EpgH>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Collapsed: Story = {
    args: {
        //expanded: false,
    }
};

export const Expanded: Story = {
    args: {
        //expanded: true,
    }
};
