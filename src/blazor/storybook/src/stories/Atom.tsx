import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/Atom.razor.css";
import ExternalLink from "../../../Deskimg.Blazor.Foundation/Resources/Action/external-link.svg?react";

interface AtomProps {
    expanded: boolean;
    collapseId?: string;
    style?: CSSProperties | undefined;
    enclosure?: string | undefined,
    publishDate?: string | undefined,
    title?: string | undefined;
    summary?: string | undefined;
    link?: string | undefined;
}

export const Atom = ({
    expanded,
    collapseId = "rss__001",
    style = undefined,
    enclosure = undefined,
    publishDate = undefined,
    title = undefined,
    summary = undefined,
    link = undefined,
}: AtomProps) => {
    return (
        <div className="component atom-component" style={style}>
            <button className="btn btn-link shadowed" data-bs-toggle="collapse"
                data-bs-target={`.${collapseId}`} aria-expanded={expanded}>
                <div className={`${collapseId} collapse ${expanded && "show" || ""}`}>
                    {enclosure &&
                        <img className="enclosure" src={enclosure} />
                    }
                    <div className="title">
                        {publishDate &&
                            <time dateTime={publishDate}>
                                {publishDate}
                            </time>
                        }
                        {title}
                    </div>
                    <div className="summary">
                        {summary}
                        {link &&
                            <a className="icon-link ms-1" href="#" tabIndex={-1}>
                                <ExternalLink />
                            </a>
                        }
                    </div>
                </div>
            </button>
        </div >
    );
};
