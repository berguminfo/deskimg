import type { Meta, StoryObj } from "@storybook/react";
import { Location } from "./Location";

const meta = {
    title: "Components/Location",
    component: Location,
    tags: ["autodocs"],
} satisfies Meta<typeof Location>;

export default meta;
type Story = StoryObj<typeof meta>;

export const First: Story = {
    args: {
    }
};
