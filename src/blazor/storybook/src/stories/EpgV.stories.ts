import type { Meta, StoryObj } from "@storybook/react";
import { EpgV } from "./EpgV";

const meta = {
    title: "Components/EpgV",
    component: EpgV,
    tags: ["autodocs"],
    args: {
        when: "18:00",
        duration: "18:59",
        summary: "Dagsnytt 18",
        description: "Dagens viktigste debatter, intervjuer og kommentarer direkte fra radiostudio.",
        attachment: "/sample-data/gfx.nrk.no_iyE4am67ZursSlh8BZt9VQL4KWhNahFKtOCYyceTpkUw.jpg",
        category: ["Nyheter"],
    },
} satisfies Meta<typeof EpgV>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Collapsed: Story = {
    args: {
        expanded: false,
    }
};

export const Expanded: Story = {
    args: {
        expanded: true,
    }
};
