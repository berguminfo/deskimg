import type { Meta, StoryObj } from "@storybook/react";
import { ErrorPage } from "./ErrorPage";

const meta = {
    title: "Pages/Error",
    component: ErrorPage,
    tags: ["autodocs"],
    args: {
    },
} satisfies Meta<typeof ErrorPage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const NotFound: Story = {
    args: {
        statusCode: 404,
    }
};
