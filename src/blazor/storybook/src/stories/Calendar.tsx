import { CSSProperties } from "react";
import "../../../Deskimg.Blazor.Components/Components/Calendar.razor.css";

interface CalendarProps {
    style?: CSSProperties | undefined;
    summary?: string | undefined;
}

export const Calendar = ({
    style = undefined,
    summary = undefined,
}: CalendarProps) => {
    const monthYearFormat = new Intl.DateTimeFormat("no-NB", {
        month: "long",
        year: "numeric",
    });
    const dayFormat = new Intl.DateTimeFormat("no-NB", {
        weekday: "narrow",
    });

    const content = [];
    let previousMonth = 0;
    let previousWeek = 14;
    for (let i = 0; i < 10; ++i) {
        const date = new Date(Date.parse("2024-03-25") + (i * 60 * 60 * 24 * 1000));
        const bgClass = "";
        const bgStyle = {
            "background-size": "50%",
            "width": "100%",
        };
        const holidayClass = date.getDay() === 0 ? "" : "";
        if (previousMonth != date.getMonth()) {
            content.push(
                <tr>
                    <td className="yearmonth shadowed" colSpan={4}>
                        <time dateTime="1900-10-10">
                            {monthYearFormat.format(date)}
                        </time>
                    </td>
                </tr>);
            previousMonth = date.getMonth();
        }
        content.push(
            <tr>
                <td className={`daydate text-nowrap shadowed ${bgClass} ${holidayClass}`}>
                    <span>{dayFormat.format(date)}</span>
                    <span className="ms-1">{date.getDate()}</span>
                </td>
                <td className={`events shadowed ${bgClass}`} style={bgStyle}>
                    <div>
                        {date.getDay() === 1 ?
                            <div className="week" title="{previousWeek}">
                                {previousWeek++}
                            </div> : ""
                        }
                        <div className="attachment d-flex flex-row">
                            <div className="me-2">
                                <img src="/images/intl/flag-no.svg" alt="flag-no" />
                            </div>
                            {summary &&
                                <div className="@summaryClass shadowed">
                                    {summary}
                                </div>
                            }
                        </div>
                    </div>
                </td>
            </tr>);
    }

    return (
        <div className="component calendar-component" style={style}>
            <table className="table">
                <tbody>
                    {content}
                </tbody>
            </table>
        </div>
    );
};
