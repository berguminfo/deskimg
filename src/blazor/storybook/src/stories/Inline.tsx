import { CSSProperties } from "react";

interface InlineProps {
    style?: CSSProperties | undefined;
    value?: string | undefined;
}

export const Inline = ({
    style = undefined,
    value = undefined,
}: InlineProps) => {
    if (value && value.length >= 2 && value.length >= 2 &&
        value.charAt(0) === '<' && value.charAt(value.length - 1) === '>') {
        return (
            <div className="component inline-component" style={style}
                dangerouslySetInnerHTML={{ __html: value }} />
        );
    } else {
        return (
            <div className="component inline-component" style={style}>
                {value}
            </div>
        );
    }
};
