import type { Meta, StoryObj } from "@storybook/react";
import { Navigation } from "./Navigation";

const meta = {
    title: "Components/Navigation",
    component: Navigation,
    tags: ["autodocs"],
    args: {
        style: {
            color: "white",
        }
    },
} satisfies Meta<typeof Navigation>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Nav: Story = {
    args: {
        modal: false,
    }
};

export const Modal: Story = {
    args: {
        modal: true,
    }
};
