import type { Meta, StoryObj } from "@storybook/react";
import { Meteorology } from "./Meteorology";

const meta = {
    title: "Components/Meteorology",
    component: Meteorology,
    tags: ["autodocs"],
} satisfies Meta<typeof Meteorology>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Observation: Story = {
    args: {
        type: "observation",
        validity: ["12:34"],
        temperature: [5],
        condition: ["-ra"],
        precipitation: [0.2],
    }
};

export const Forecast: Story = {
    args: {
        type: "forecast",
        validity: ["12:00", "12:30", "13:00"],
        temperature: [-1, 0, 34],
        condition: ["+ra", "clr", "rasnsh_n"],
        precipitation: [1.2, 0, 0.2],
        windDirection: [99, 129, 244],
        windSpeed: [0, 18, 5],
    }
};
