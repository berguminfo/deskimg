// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA2227 // Collection properties should be read only
// Ignore Spelling: Geoposition Geocoordinate

using System.Globalization;

namespace Deskimg;

/// <summary>
/// Represents the position of the concerned device at a given time.
/// </summary>
public class Geoposition {

    /// <summary>
    /// Gets or sets the position civic address.
    /// </summary>
    public CivicAddress? CivicAddress { get; set; }

    /// <summary>
    /// Gets or set the position coordinate.
    /// </summary>
    public Geocoordinate? Coordinate { get; set; }

    /// <summary>
    /// Gets or sets the position time zone identifier or alias.
    /// </summary>
    public string? TimeZone { get; set; }

    /// <summary>
    /// Gets or sets the position tags.
    /// </summary>
    public IList<string> Tags { get; set; } = [];

    /// <summary>
    /// Gets a enumerator splitting each tag on the '?' character.
    /// </summary>
    /// <returns>The tags enumerator.</returns>
    public IEnumerable<KeyValuePair<string, object?>> GetTagsEnumerator() {
        foreach (var item in Tags) {
            int index = item.IndexOf('?', StringComparison.Ordinal);
            yield return index >= 0 ?
                new(item[..index], item[(index + 1)..]) :
                new(item, null);
        }
    }

    /// <inheritdoc/>
    public override string? ToString() => CivicAddress?.City ?? Coordinate?.ToString();
}

/// <summary>
/// Represents a civic address.
/// </summary>
public class CivicAddress {

    /// <summary>
    /// Gets or sets the unknown indicator.
    /// </summary>
    public bool IsUnknown { get; set; }

    /// <summary>
    /// Gets or sets the city part.
    /// </summary>
    public string? City { get; set; }

    /// <summary>
    /// Gets or sets the country part.
    /// </summary>
    public string? Country { get; set; }
}

/// <summary>
/// The list of supported CRS.
/// </summary>
public enum CoordinateReferenceSystem {

    /// <summary>
    /// None or unknown CRS.
    /// </summary>
    Unknown,

    /// <summary>
    /// The World Geodetic System (1966 revision).
    /// </summary>
    WGS66,

    /// <summary>
    /// The World Geodetic System (1972 revision).
    /// </summary>
    WGS72,

    /// <summary>
    /// The World Geodetic System (1984 revision).
    /// </summary>
    WGS84
}

/// <summary>
/// Represents a geodetic coordinate.
/// </summary>
public class Geocoordinate : IEquatable<Geocoordinate> {

    double _latitude, _longitude;

    /// <summary>
    /// Gets or sets the coordinate CRS.
    /// </summary>
    public CoordinateReferenceSystem CRS { get; set; } = CoordinateReferenceSystem.WGS84;

    /// <summary>
    /// Gets or sets the latitude of the position in decimal degrees.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">The provided value was out of range.</exception>
    public double Latitude {
        get => _latitude;
        set {
            if (value is < (-90.0) or > 90.0) {
                throw new ArgumentOutOfRangeException(nameof(Latitude), Latitude, "Expected value to be between -90 and 90");
            }

            _latitude = value;
        }
    }

    /// <summary>
    /// Gets or sets the longitude of the position in decimal degrees.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">The provided value was out of range.</exception>
    public double Longitude {
        get => _longitude;
        set {
            if (value is < (-180.0) or > 180.0) {
                throw new ArgumentOutOfRangeException(nameof(Longitude), Longitude, "Expected value to be between -180 and 180");
            }

            _longitude = value;
        }
    }

    /// <summary>
    /// Gets or sets the altitude of the position in meters, relative to sea level.
    /// 
    /// This value is null if the implementation cannot provide this data.
    /// </summary>
    public double? Altitude { get; set; }

    /// <summary>
    /// Gets or sets the accuracy, with a 95% confidence level, of the Latitude and Longitude expressed in meters.
    /// </summary>
    public double? Accuracy { get; set; }

    /// <summary>
    /// Gets or sets the accuracy, with a 95% confidence level, of the Altitude expressed in meters.
    /// </summary>
    public double? AltitudeAccuracy { get; set; }

    static string ToAngle(double value, string positive, string negative) {
        var degrees = Math.Abs(value);
        var d = (int)Math.Floor(degrees);
        var m = (int)Math.Floor((degrees - d) * 60.0);
        var s = (int)Math.Floor((degrees - d - (m / 60.0)) * 3600.0);
        string suffix = string.Empty;
        if (value > double.Epsilon) suffix = positive;
        if (value < -double.Epsilon) suffix = negative;
        return $"{d}°{m}′{s}″{suffix}";
    }

    /// <inheritdoc/>
    public override string? ToString() =>
        double.IsNaN(Latitude) || double.IsNaN(Longitude) ?
            base.ToString() :
            string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                ToAngle(Latitude, "N", "S"),
                ToAngle(Longitude, "W", "E")
            );

    #region IEquatable`1

    /// <inheritdoc/>
    public override int GetHashCode() => Latitude.GetHashCode() ^ Longitude.GetHashCode();

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is Geocoordinate other && Equals(other);

    /// <inheritdoc/>
    public bool Equals(Geocoordinate? other) => GetHashCode() == other?.GetHashCode();

    /// <inheritdoc cref="Equals(Geocoordinate)"/>
    public static bool operator ==(Geocoordinate left, Geocoordinate right) => left.Equals(right);

    /// <inheritdoc cref="Equals(Geocoordinate)"/>
    public static bool operator !=(Geocoordinate left, Geocoordinate right) => !(left == right);

    #endregion
}

#pragma warning restore CA2227 // Collection properties should be read only
