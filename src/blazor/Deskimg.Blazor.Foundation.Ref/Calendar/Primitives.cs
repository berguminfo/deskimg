// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public enum AccessClassification {
    Public,
    Private,
    Confidential
}

public static class VEventStatus {
    public const ushort Tentative = 0x01;
    public const ushort Confirmed = 0x02;
    public const ushort Canceled = 0x03;
}

public static class VTodoStatus {
    public const ushort NeedsAction = 0x11;
    public const ushort Completed = 0x12;
    public const ushort InProcess = 0x13;
    public const ushort Canceled = 0x14;
}

public static class VJournalStatus {
    public const ushort Draft = 0x21;
    public const ushort Final = 0x22;
    public const ushort Canceled = 0x23;
}

public enum VEventRecurrence {
    Once,
    Daily,
    Weekly,
    Monthly,
    Yearly,
    Easter
}

public enum WeekOfMonth {
    None,
    First,
    Second,
    Third,
    Fourth,
    Last
}

[Flags]
public enum DaysOfWeek {
    None = 0,
    Sunday = 1 << DayOfWeek.Sunday,
    Monday = 1 << DayOfWeek.Monday,
    Tuesday = 1 << DayOfWeek.Tuesday,
    Wednesday = 1 << DayOfWeek.Wednesday,
    Thursday = 1 << DayOfWeek.Thursday,
    Friday = 1 << DayOfWeek.Friday,
    Saturday = 1 << DayOfWeek.Saturday,
    WesternWorkweek = Monday | Tuesday | Wednesday | Thursday | Friday
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
