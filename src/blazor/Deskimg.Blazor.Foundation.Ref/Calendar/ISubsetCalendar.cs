// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

/// <summary>
/// Represents the result of one Subset iteration.
/// </summary>
/// <param name="When">The <see cref="DateTimeOffset"/> the iteration occur.</param>
/// <param name="Event">The matching <see cref="IVEvent"/>.</param>
/// <param name="Occurrences">The number of iterations found so far.</param>
public record SubsetResult(DateTimeOffset When, IVEvent Event, int Occurrences = 1);

/// <summary>
/// Represents a subset calendar.
/// </summary>
public interface ISubsetCalendar {

    /// <summary>
    /// Enumerates relevant events asynchronously.
    /// </summary>
    /// <param name="rangeStart">The interest range start.</param>
    /// <param name="rangeLength">The interest range length.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>Enumeration of events matching the criteria.</returns>
    Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default);
}

/// <summary>
/// Represents a subset calendar.
/// </summary>
public interface ISubsetCalendar2 {

    /// <summary>
    /// Enumerates relevant events asynchronously.
    /// </summary>
    /// <param name="rangeStart">The interest range start.</param>
    /// <param name="rangeLength">The interest range length.</param>
    /// <param name="observer">The observer coordinates.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>Enumeration of events matching the criteria.</returns>
    Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default);
}
