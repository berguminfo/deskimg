// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

/// <summary>
/// Represents a <see href="https://tools.ietf.org/html/rfc5545">RFC5545 event.</see>.
/// </summary>
public interface IVEvent {

    /// <summary>
    /// This property defines the access classification for a
    /// calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.3">3.8.1.3.  CLASS</see>
    /// <example>CLASS:PUBLIC</example>
    AccessClassification Classification => AccessClassification.Public;

    /// <summary>
    /// This property defines a short summary or subject for the
    /// calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.12">3.8.1.12.  SUMMARY</see>
    /// <example>SUMMARY:Department Party</example>
    string Summary { get; }

    /// <summary>
    /// This property defines the intended venue for the activity
    /// defined by a calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.7">3.8.1.7.  LOCATION</see>
    /// <example>LOCATION:Conference Room - F123\, Bldg. 002</example>
    string? Location => null;

    /// <summary>
    /// This property provides a more complete description of the
    /// calendar component than that provided by the "SUMMARY" property.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.5">3.8.1.5.  DESCRIPTION</see>
    /// <example>DESCRIPTION:Meeting to provide technical review for "Phoenix" design.</example>
    string? Description { get; }

    /// <summary>
    /// This property specifies a positive duration of time.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.2.5">3.8.2.5.  DURATION</see>
    /// <example>DURATION:PT1H0M0S</example>
    TimeSpan? Duration { get; }

    /// <summary>
    /// This property provides the capability to associate a
    /// document object with a calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.1">3.8.1.1.  ATTACH</see>
    /// <example>ATTACH;FMTTYPE=application/postscript:ftp://example.com/pub/reports/r-960812.ps</example>
    HashSet<string> Attachment => [];

    /// <summary>
    /// This property defines the categories for a calendar
    /// component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.2">3.8.1.2.  CATEGORIES</see>
    /// <example>CATEGORIES:APPOINTMENT,EDUCATION</example>
    /// <example>CATEGORIES:MEETING</example>
    HashSet<object> Categories => [];

    /// <summary>
    /// This property defines a Uniform Resource Locator (URL)
    /// associated with the iCalendar object.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.6">3.8.4.6.  URL</see>
    /// <example>URL:http://example.com/pub/calendars/jsmith/mytime.ics</example>
    public Uri? Url => null;

    /// <summary>
    /// This property defines the persistent, globally unique
    /// identifier for the calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.7">3.8.4.7.  URL</see>
    /// <remarks>
    /// The property MUST be specified in the "VEVENT",
    /// "VTODO", "VJOURNAL", or "VFREEBUSY" calendar components.
    /// </remarks>
    /// <example>UID:19960401T080045Z-4000F192713-0052@example.com</example>
    string UniqueId { get; }
}
