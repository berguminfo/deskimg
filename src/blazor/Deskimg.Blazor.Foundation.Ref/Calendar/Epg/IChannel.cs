// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.Epg;

/// <summary>
/// Represents a EPG subset calendar.
/// </summary>
public interface IChannel {

    /// <summary>
    /// Gets the channel identifier.
    /// </summary>
    string Id { get; }

    /// <summary>
    /// Gets the channel title.
    /// </summary>
    string Title { get; }

    /// <summary>
    /// Gets the channel image URI.
    /// </summary>
    /// <param name="minWidth">The minimum width of the image.</param>
    /// <param name="maxWidth">The maximum width of the image.</param>
    /// <param name="minHeight">The minimum height of the image.</param>
    /// <param name="maxHeight">The maximum height of the image.</param>
    public Uri? GetImageUri(
        uint minWidth = uint.MinValue, uint maxWidth = uint.MaxValue,
        uint minHeight = uint.MinValue, uint maxHeight = uint.MaxValue);
}
