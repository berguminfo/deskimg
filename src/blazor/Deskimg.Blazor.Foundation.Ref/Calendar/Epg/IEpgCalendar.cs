// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.Epg;

/// <summary>
/// Represents a EPG subset calendar.
/// </summary>
public interface IEpgCalendar {

    /// <summary>
    /// Enumerates relevant events asynchronously.
    /// </summary>
    /// <param name="rangeStart">The interest range start.</param>
    /// <param name="rangeLength">The interest range length.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>Enumeration of events matching the criteria.</returns>
    Task<ILookup<IChannel, IEnumerable<SubsetResult>>?> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default);
}
