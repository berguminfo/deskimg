// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Composition;

/// <summary>
/// Specifies that a type, property, field, or method provides a particular export.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
public sealed class ExportAttribute<T>(string? contractName) : Attribute {

    /// <summary>
    /// Gets the contract name to export the type or member under.
    /// </summary>
    /// <remarks>
    /// A <see cref="string"/> containing the contract name to export the type or member
    /// marked with this attribute, under. The default value is an empty string ("").
    /// </remarks>
    public string? ContractName { get; } = contractName;

    /// <summary>
    /// Get the contract type that is exported by the member that this attribute is attached to.
    /// </summary>
    /// <remarks>
    /// A <see cref="Type"/> of the export that is be provided. The default value is
    /// <see langword="null"/> which means that the type will be obtained by looking at the type on
    /// the member that this export is attached to.
    /// </remarks>
    public Type ContractType { get; } = typeof(T);

    /// <inheritdoc/>
    public override int GetHashCode() =>
        ContractType.GetHashCode() ^ (ContractName ?? ContractType.Name).GetHashCode(StringComparison.Ordinal);
}
