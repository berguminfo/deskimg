// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Composition;

using System.Net.Http.Headers;

/// <summary>
/// Interface used to set properties.
/// </summary>
public interface ICanCacheControl {

    /// <summary>
    /// Gets or sets the <see cref="CacheControlHeaderValue"/>.
    /// </summary>
    CacheControlHeaderValue CacheControl { get; set; }
}
