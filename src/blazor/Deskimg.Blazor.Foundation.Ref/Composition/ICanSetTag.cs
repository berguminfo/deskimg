// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Composition;

/// <summary>
/// Interface used to set properties.
/// </summary>
public interface ICanSetTag {

    /// <summary>
    /// Gets or sets the value of <see cref="Geoposition.Tags"/>.
    /// </summary>
    KeyValuePair<string, object?> Tag { get; set; }
}
