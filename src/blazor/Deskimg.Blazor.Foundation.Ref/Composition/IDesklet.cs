// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Composition;

#pragma warning disable CA1040 // Avoid empty interfaces

/// <summary>
/// Interface to represent desklets.
/// </summary>
public interface IDesklet { }

#pragma warning restore CA1040 // Avoid empty interfaces
