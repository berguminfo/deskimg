// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Composition;

/// <summary>
/// Instructs the Deskimg.Generators.Exports source generator to generate
/// an implementation of <see cref="IAssemblyExports"/>.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public sealed class AssemblyExportsGeneratedAttribute : Attribute { }
