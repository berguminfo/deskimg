// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Collections.Immutable;

namespace Deskimg.Composition;

/// <summary>
/// Represents an interface to add assembly exports.
/// </summary>
public interface IAssemblyExports {

    /// <summary>
    /// Gets the assembly exports.
    /// </summary>
    IImmutableDictionary<(Type ContractType, string ContractName), Type> GetExports();
}
