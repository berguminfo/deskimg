// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace Deskimg;

/// <summary>
/// Represents supported DataType annotations.
/// </summary>
public static class CustomDataType {
    public const string Style = nameof(Style);
    public const string AlertSource = nameof(AlertSource);
    public const string AtomSource = nameof(AtomSource);
    public const string CalendarSource = nameof(CalendarSource);
    public const string EpgSource = nameof(EpgSource);
    public const string InlineSource = nameof(InlineSource);
    public const string LocationSource = nameof(LocationSource);
    public const string MeteorologySource = nameof(MeteorologySource);
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
