// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

/// <summary>
/// Represents the <see cref="MeasureD"/> format info.
/// </summary>
public static class MeasureFormatInfo {

    static readonly Dictionary<string, string> s_symbols = new() {
        ["°C"] = "℃",
        ["°F"] = "℉",
        ["m/s"] = "㎧",
        ["in"] = "″"
    };

    static readonly Dictionary<string, Dictionary<string, Func<double, double>>> s_unitConverters = new() {

        // temperature in Celsius
        ["°C"] = new() {
            ["°C"] = x => x,
            ["K"] = x => x - 273.15,
            ["°F"] = x => x * (9.0 / 5.0) + 32.0
        },

        // temperature in Fahrenheit
        ["°F"] = new() {
            ["°F"] = x => x,
            ["K"] = x => x * (9.0 / 5.0) - 459.67,
            ["°C"] = x => (x - 32.0) * 5.0 / 9.0
        },

        // temperature in kelvin
        ["K"] = new() {
            ["K"] = x => x,
            ["°C"] = x => x + 273.15,
            ["°F"] = x => (x + 459.67) * (5.0 / 9.0)
        },

        // speed in meter per second
        ["m/s"] = new() {
            ["m/s"] = x => x,
            ["km/h"] = x => x * (3600.0 / 1000.0),
            ["kn"] = x => x * (3600.0 / 1852.0),
            ["mph"] = x => x / 0.44704
        },

        // speed in knot
        ["kn"] = new() {
            ["m/s"] = x => x * (1852.0 / 3600.0),
            ["km/h"] = x => x * 1.852,
            ["kn"] = x => x,
            ["mph"] = x => x * (1.852 / 1.609344)
        },

        // speed in kilometers per hour
        ["km/h"] = new() {
            ["m/s"] = x => x * (1000.0 / 3600.0),
            ["km/h"] = x => x,
            ["kn"] = x => x / 1.852,
            ["mph"] = x => x / 1.609344
        },

        // speed in miles per hour
        ["mph"] = new() {
            ["m/s"] = x => x * 0.44704,
            ["km/h"] = x => x * 1.609344,
            ["kn"] = x => x * (1.609344 / 1.852),
            ["mph"] = x => x
        },

        // pressure in hectors pascal
        ["hPa"] = new() {
            ["hPa"] = x => x
        },

        // pressure in inch of mercury
        ["inHg"] = new() {
            ["hPa"] = x => x * (3386.389 / 100.0)
        },

        // length in millimeters
        ["mm"] = new() {
            ["mm"] = x => x,
            ["in"] = x => x / 25.4
        },

        // length in inch
        ["in"] = new() {
            ["mm"] = x => x * 25.4
        }
    };

    /// <summary>
    /// Gets the unit converter.
    /// </summary>
    /// <param name="fromUnit">The source unit.</param>
    /// <param name="toUnit">The destination unit.</param>
    /// <returns>The converter to convert <paramref name="fromUnit"/> to <paramref name="toUnit"/> or null.</returns>
    public static Func<double, double>? GetUnitConverter(string fromUnit, string toUnit) =>
        s_unitConverters.TryGetValue(fromUnit, out var fromUnitConverts) &&
        fromUnitConverts.TryGetValue(toUnit, out var converter) ? converter : null;

    /// <summary>
    /// Gets the unit symbol for the given unit.
    /// </summary>
    /// <param name="unit">The unit name.</param>
    /// <returns>The Unicode unit symbol or <paramref name="unit"/>.</returns>
    public static string? GetSymbol(string? unit) =>
        s_symbols.TryGetValue(unit ?? string.Empty, out string? symbol) ? symbol : unit;
}
