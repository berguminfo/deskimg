// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

/// <summary>
/// Interface to represent sessions.
/// </summary>
public interface ISession {

    /// <summary>
    /// Gets the current time of interest.
    /// </summary>
    DateTimeOffset Toi { get; }
}
