// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

/// <summary>
/// The <see cref="double"/> representation in units.
/// </summary>
public readonly struct MeasureD(double value, string unit) : IFormattable, IEquatable<MeasureD> {

    /// <summary>
    /// The <see cref="double"/> value.
    /// </summary>
    public double Value { get; init; } = value;

    /// <summary>
    /// The <see cref="MeasureD"/> unit.
    /// </summary>
    public string Unit { get; init; } = unit;

    /// <summary>
    /// Gets the unit symbol.
    /// </summary>
    public string? Symbol => MeasureFormatInfo.GetSymbol(Unit);

    /// <summary>
    /// Converts the measurement to another unit.
    /// </summary>
    /// <param name="toUnit">The unit to convert to.</param>
    /// <exception cref="ArgumentException">The <paramref name="toUnit"/> has no converter.</exception>
    /// <returns>The converted measurement.</returns>
    public MeasureD Convert(string toUnit) {
        if (string.Equals(toUnit, Unit, StringComparison.Ordinal)) {
            return this;
        }

        var converter = MeasureFormatInfo.GetUnitConverter(Unit, toUnit);
        if (converter is null) {
            throw new ArgumentException($"No such converer. From: {Unit}. To: {toUnit}.", nameof(toUnit));
        } else {
            return new MeasureD(converter(Value), toUnit);
        }
    }

    /// <inheritdoc/>
    public override string ToString() => $"{Value}<{Unit}>";

    /// <inheritdoc/>
    public string ToString(string? format, IFormatProvider? formatProvider) {
        if (formatProvider is null) {
            return ToString();
        }

        string? unit = MeasureFormatInfo.GetSymbol(Unit);
        return string.Format(formatProvider, "{0} {1}", Value.ToString(format, formatProvider), unit);
    }

    #region IEquatable

    /// <inheritdoc/>
    public override int GetHashCode() => Value.GetHashCode() ^ Unit.GetHashCode(StringComparison.Ordinal);

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is MeasureD other && Equals(other);

    /// <inheritdoc/>
    public bool Equals(MeasureD other) => GetHashCode() == other.GetHashCode();

    /// <inheritdoc cref="Equals(MeasureD)"/>
    public static bool operator ==(MeasureD left, MeasureD right) => left.Equals(right);

    /// <inheritdoc cref="Equals(MeasureD)"/>
    public static bool operator !=(MeasureD left, MeasureD right) => !left.Equals(right);

    #endregion
}
