// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Syndication;

/// <summary>
/// Represents the syndication items published by RSS or Atom feeds.
/// </summary>
public class SyndicationItem {

    /// <summary>
    /// Gets or sets the atom:id value.
    /// </summary>
    public required string Id { get; set; }

    /// <summary>
    /// Gets or sets the atom:published value.
    /// </summary>
    public DateTimeOffset? Published { get; set; }

    /// <summary>
    /// Gets or sets the atom:title value.
    /// </summary>
    public string? Title { get; set; }

    /// <summary>
    /// Gets or sets the atom:summary value.
    /// </summary>
    public string? Summary { get; set; }

    /// <summary>
    /// Gets or sets the first atom:link element.
    /// </summary>
    public string? Link { get; set; }

    /// <summary>
    /// Gets or sets the extension:enclusure element or attribute.
    /// </summary>
    public string? Enclosure { get; set; }
}
