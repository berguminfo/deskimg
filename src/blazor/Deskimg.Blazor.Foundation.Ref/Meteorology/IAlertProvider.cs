// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <summary>
/// Represents a weather warning provider.
/// </summary>
public interface IAlertProvider {

    /// <summary>
    /// Enumerates weather warnings asynchronously.
    /// </summary>
    /// <param name="rangeStart">The interest range start.</param>
    /// <param name="rangeLength">The interest range length.</param>
    /// <param name="observer">The observer coordinates.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The enumeration of <see cref="IAlertMessage"/> objects.</returns>
    Task<IEnumerable<IAlertMessage>> GetAlertsAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default);
}
