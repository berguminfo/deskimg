// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

using Deskimg.Syndication;

/// <summary>
/// Represents a weather alert message.
/// </summary>
public interface IAlertMessage {

    /// <summary>
    /// Gets the location.
    /// </summary>
    public string? Location { get; }

    /// <summary>
    /// Gets the valid from dateTime.
    /// </summary>
    DateTimeOffset ValidFrom { get; }

    /// <summary>
    /// Gets the valid to dateTime.
    /// </summary>
    DateTimeOffset ValidTo { get; }

    /// <summary>
    /// Gets the alert kind.
    /// </summary>
    AlertKind Kind { get; }

    /// <summary>
    /// Gets the alert severity.
    /// </summary>
    AlertSeverity Severity { get; }

    /// <summary>
    /// Gets the alert message.
    /// </summary>
    SyndicationItem Message { get; }
}
