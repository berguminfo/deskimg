// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <summary>
/// Represents a weather forecast provider.
/// </summary>
public interface IForecastProvider {

    /// <summary>
    /// Enumerates weather forecasts asynchronously.
    /// </summary>
    /// <param name="rangeStart">The interest range start.</param>
    /// <param name="rangeLength">The interest range length.</param>
    /// <param name="observer">The observer coordinates.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The enumeration of sensor readings.</returns>
    Task<IEnumerable<IMeteorologyReading>> GetForecastsAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default);
}
