// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

#pragma warning disable CA1815 // Override equals and operator equals on value types
#pragma warning disable CA1008

/// <summary>
/// Represent a meteorology sensor reading.
/// </summary>
public struct WindReading {

    /// <summary>
    /// 
    /// </summary>
    public MeasureD? Sustained { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public MeasureD? Gust { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public MeasureD? Average { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool VariableDirections { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public MeasureD? FromDirection { get; set; }
}

#pragma warning restore CA1008
#pragma warning restore CA1815 // Override equals and operator equals on value types
