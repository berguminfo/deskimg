// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

/// <summary>
/// Represent common alert kind.
/// </summary>
// NOTE:
// - keep in sync with the 'wwwroot/images/status/warning-*.svg' files
// - in case no mapping exists, just use the 'Generic' symbol
// - in case severity is EXTREME, the symbol should be 'Extreme' regardless of eventCode
public enum AlertKind {
    None,

    // YrWeatherWarning, NveAvalanche, NveFlood, NveLandslide
    Generic,
    Extreme,
    Avalanches,
    DrivingConditions,
    Duststorm,
    Flood,
    ForestFire,
    Ice,
    Landslide,
    Lightning,
    PolarLow,
    Rain,
    RainFlood,
    Sandstorm,
    Snow,
    StormSurge,
    VulcanicEruption,
    Wind,

    // TrafficInformation
    RoadClosed,
    RiskOfRoadClosed,
    ConvoyDriving,
    RiskOfRoadConvoy,
    Warning,
    RoadWork,
    RoadClosedForWinter
}

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
