// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

#pragma warning disable CA1815 // Override equals and operator equals on value types

/// <summary>
/// Represent a meteorology sensor reading.
/// </summary>
public struct PressureReading {

    /// <summary>
    /// Gets or sets the atmospheric pressure.
    /// </summary>
    public MeasureD? Atmospheric { get; set; }
}

#pragma warning restore CA1815 // Override equals and operator equals on value types
