// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json.Serialization;

namespace Deskimg.Meteorology;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

/// <summary>
/// Represents common alert severities.
/// </summary>
// NOTE: keep in sync with site.css # .bg-severity-* definitions
[JsonConverter(typeof(JsonStringEnumConverter<AlertSeverity>))]
public enum AlertSeverity {
    None,
    Green,
    Yellow,
    Orange,
    Red,
    Extreme,
    Inactive,
    Active
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
