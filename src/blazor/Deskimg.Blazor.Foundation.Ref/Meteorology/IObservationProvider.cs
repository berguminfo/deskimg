// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <summary>
/// Represents a weather observation provider.
/// </summary>
public interface IObservationProvider {

    /// <summary>
    /// Enumerates weather observations asynchronously.
    /// </summary>
    /// <param name="observer">The observer coordinates.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The enumeration of <see cref="IMeteorologyReading"/> objects.</returns>
    Task<IEnumerable<IMeteorologyReading>> GetObservationsAsync(
        Geoposition observer,
        CancellationToken cancellationToken = default);
}
