// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <summary>
/// Represents meteorology reading.
/// </summary>
public interface IMeteorologyReading {

    /// <summary>
    /// Gets the readings provider <see cref="Type"/>.
    /// </summary>
    public Type Provider { get; }

    /// <summary>
    /// Gets the reading location.
    /// </summary>
    public string? Location { get; }

    /// <summary>
    /// Gets the reading valid from dateTime.
    /// </summary>
    public DateTimeOffset ValidFrom { get; }

    /// <summary>
    /// Gets the reading valid to dateTime.
    /// </summary>
    public DateTimeOffset ValidTo { get; }

    /// <summary>
    /// Gets the temperature sensor readings.
    /// </summary>
    public TemperatureReading? Temperature { get; }

    /// <summary>
    /// Gets the precipitation sensor readings.
    /// </summary>
    public PrecipitationReading? Precipitation { get; }

    /// <summary>
    /// Gets the wind sensor readings.
    /// </summary>
    public WindReading? Wind { get; set; }

    /// <summary>
    /// Gets the pressure sensor readings.
    /// </summary>
    public PressureReading? Pressure { get; }

    /// <summary>
    /// Gets the humidity sensor readings.
    /// </summary>
    public HumidityReading? Humidity { get; }

    /// <summary>
    /// Gets the ultraviolet radiation sensor readings.
    /// </summary>
    public UltravioletRadiationReading? UltravioletRadiation { get; }

    /// <summary>
    /// Gets the weather condition calculation.
    /// </summary>
    public WeatherConditions? Conditions { get; }
}
