// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

#pragma warning disable CA1815 // Override equals and operator equals on value types

/// <summary>
/// Represent a meteorology sensor reading.
/// </summary>
public struct PrecipitationReading {

    /// <summary>
    /// Gets or sets the liquid precipitation.
    /// </summary>
    public MeasureD? Liquid { get; set; }

    /// <summary>
    /// Gets or sets the snow depth.
    /// </summary>
    public MeasureD? SnowDepth { get; set; }

    /// <summary>
    /// Gets or sets the ice thickness.
    /// </summary>
    public MeasureD? IceThickness { get; set; }

    /// <summary>
    /// Gets or sets the minimum amount of precipitation in a interval.
    /// </summary>
    public MeasureD? IntervalMinimum { get; set; }

    /// <summary>
    /// Gets or sets the maximum amount of precipitation in a interval.
    /// </summary>
    public MeasureD? IntervalMaximum { get; set; }

    /// <summary>
    /// Gets or sets the interval length.
    /// </summary>
    public TimeSpan? IntervalLength { get; set; }
}

#pragma warning restore CA1815 // Override equals and operator equals on value types
