// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.ComponentModel.DataAnnotations;

namespace Deskimg.Meteorology;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

/// <summary>
/// Represents weather conditions.
/// </summary>
// NOTE: the conditions is encoded using the following bit encoding:
// ┌────────┬────────┬────────┬───────┬──────┬───┐
// │ 30..29 │ 28..24 │ 23..16 │ 15..8 │ 5..1 │ 0 │
// ├────────┼────────┼────────┼───────┼──────┼───┤
// │ _n     │ fg     │ -      │ ra    │ clr  │   │
// │ _p     │        │ +      │ sn    │ few  │   │
// │        │        │ sh     │       │ sct  │   │
// │        │        │ ts     │       │ bkn  │   │
// │        │        │        │       │ ovc  │   │
// └────────┴────────┴────────┴───────┴──────┴───┘
[Flags]
#pragma warning disable CA1008 // Enums should have zero value
public enum WeatherConditions {
#pragma warning restore CA1008 // Enums should have zero value
    None = 0,

    [Display(ShortName = "up")]
    Unknown = None,

    // sky condition
    [Display(ShortName = "clr")]
    ClearSky = 1 << SkyCondition.ClearSky,

    [Display(ShortName = "few")]
    Fair = 1 << SkyCondition.Fair,

    [Display(ShortName = "sct")]
    PartlyCloudy = 1 << SkyCondition.PartlyCloudy,

    [Display(ShortName = "bkn")]
    MostlyCloudy = 1 << SkyCondition.MostlyCloudy,

    [Display(ShortName = "ovc")]
    Cloudy = 1 << SkyCondition.Cloudy,

    // intensity
    [Display(ShortName = "-")]
    Light = 1 << Intensity.Light,

    [Display(ShortName = "+")]
    Heavy = 1 << Intensity.Heavy,

    [Display(ShortName = "sh")]
    Showers = 1 << Intensity.Showers,

    [Display(ShortName = "ts")]
    Thunderstorm = 1 << Intensity.Thunderstorm,


    // precipitation
    [Display(ShortName = "ra")]
    Rain = 1 << Precipitation.Rain,

    [Display(ShortName = "sn")]
    Snow = 1 << Precipitation.Snow,

    // obscuration
    [Display(ShortName = "fg")]
    Fog = 1 << Obscuration.Fog,

    // symbol variant
    [Display(ShortName = "_n")]
    Night = 1 << SymbolVariant.Night,

    [Display(ShortName = "_p")]
    PolarNight = 1 << SymbolVariant.PolarNight,
}

enum SkyCondition {
    None = 0,

    [Display(ShortName = "clr", Name = "clear")]
    ClearSky = 1,

    [Display(ShortName = "few", Name = "few clouds")]
    Fair = 2,

    [Display(ShortName = "sct", Name = "scattered")]
    PartlyCloudy = 3,

    [Display(ShortName = "bkn", Name = "broken")]
    MostlyCloudy = 4,

    [Display(ShortName = "ovc", Name = "overcast")]
    Cloudy = 5
}

enum Intensity {
    None = 0,

    [Display(ShortName = "-", Name = "light intensity")]
    Light = 8,

    [Display(ShortName = "")]
    Moderate = 9,

    [Display(ShortName = "+", Name = "heavy intensity")]
    Heavy = 10,

    [Display(ShortName = "vc", Name = "in the vicinity")]
    Vicinity = 11,

    [Display(ShortName = "bl", Name = "blowing")]
    Blowing = 12,

    [Display(ShortName = "sh", Name = "shower(s)")]
    Showers = 13,

    [Display(ShortName = "ts", Name = "thunderstorm")]
    Thunderstorm = 14,

    [Display(ShortName = "fz", Name = "freezing")]
    Freezing = 15
}

enum Precipitation {
    None = 0,

    [Display(ShortName = "up", Name = "unknown precipitation")]
    Unknown = None,

    [Display(ShortName = "dz", Name = "drizzle")]
    Drizzle = 16,

    [Display(ShortName = "ra", Name = "rain")]
    Rain = 17,

    [Display(ShortName = "sn", Name = "snow")]
    Snow = 18,

    [Display(ShortName = "sg", Name = "snow grains")]
    SnowGrains = 19,

    [Display(ShortName = "du", Name = "widespread dust")]
    WidespreadDust = 20,

    [Display(ShortName = "sa", Name = "sand")]
    Sand = 21,

    [Display(ShortName = "hz", Name = "haze")]
    Haze = 22,

    [Display(ShortName = "py", Name = "spray")]
    Spray = 23
}

public enum Obscuration {
    None = 0,

    [Display(ShortName = "br", Name = "mist")]
    Mist = 24,

    [Display(ShortName = "fg", Name = "fog")]
    Fog = 25,

    [Display(ShortName = "fu", Name = "smoke")]
    Smoke = 26,

    [Display(ShortName = "va", Name = "volcanic ash")]
    VolcanicAsh = 27,

    [Display(ShortName = "ds", Name = "duststorm")]
    Duststorm = 28
}

enum SymbolVariant {
    None = 0,

    [Display(ShortName = "_n")]
    Night = 29,

    [Display(ShortName = "_p")]
    PolarNight = 30
}

public enum PrecipitationTypeDescription {
    None = 0,

    [Display(ShortName = "mi", Name = "shallow")]
    Shallow = 32,

    [Display(ShortName = "pr", Name = "partial")]
    Partial = 33,

    [Display(ShortName = "bc", Name = "patches")]
    Patches = 34,

    [Display(ShortName = "dr", Name = "low drifting")]
    LowDrifting = 35,

    [Display(ShortName = "ic", Name = "ice crystals, in-cloud lightning")]
    IceCrystals = 36,

    [Display(ShortName = "pl", Name = "ice pellets")]
    IcePellets = 37,

    [Display(ShortName = "gr", Name = "hail")]
    Hail = 38,

    [Display(ShortName = "gs", Name = "small hail and/or snow pellets")]
    SmallHail = 39
}

public enum OtherObscuration {
    None = 0,

    [Display(ShortName = "po", Name = "dust/sand whirls (dust devils)")]
    DustWhirls = 48,

    [Display(ShortName = "sq", Name = "squalls")]
    Squalls = 49,

    [Display(ShortName = "fc", Name = "funnel cloud")]
    FunnelCloud = 50,
    Tornadoes = FunnelCloud,
    Waterspouts = FunnelCloud,

    [Display(ShortName = "ss", Name = "sandstorm")]
    Sandstorm = 51
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
