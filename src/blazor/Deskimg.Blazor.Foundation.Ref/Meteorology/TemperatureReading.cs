// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

#pragma warning disable CA1815 // Override equals and operator equals on value types

/// <summary>
/// Represent a meteorology sensor reading.
/// </summary>
public struct TemperatureReading {

    /// <summary>
    /// Gets or sets the air temperature.
    /// </summary>
    public MeasureD? Air { get; set; }

    /// <summary>
    /// Gets or sets the dew point.
    /// </summary>
    public MeasureD? DewPoint { get; set; }

    /// <summary>
    /// Gets or sets the apparent temperature.
    /// </summary>
    public MeasureD? Apparent { get; set; }

    /// <summary>
    /// Gets or sets the minimum temperature in a interval.
    /// </summary>
    public MeasureD? IntervalMinimum { get; set; }

    /// <summary>
    /// Gets or sets the maximum temperature in a interval.
    /// </summary>
    public MeasureD? IntervalMaximum { get; set; }

    /// <summary>
    /// Gets or sets the interval length.
    /// </summary>
    public TimeSpan? IntervalLength { get; set; }
}

#pragma warning restore CA1815 // Override equals and operator equals on value types
