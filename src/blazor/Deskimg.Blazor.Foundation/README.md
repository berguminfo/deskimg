# Deskimg.Foundation

Main class library implementing core functionality.

- About `Composition`, `Extensions`.
  - Repository core features required by most assemblies.
- About `Calendar`, `Ephemeris`, `Meteorology`.
  - Independent features witch may be moved to another assembly?
- About `Resources`.
  - Repository wide. However main consumer would be `Deskimg.Components`.