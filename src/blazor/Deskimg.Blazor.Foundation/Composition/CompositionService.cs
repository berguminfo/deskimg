// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Composition;

/// <summary>
/// This class exposes the composition singleton service.
/// </summary>
public partial class CompositionService {

    /// <summary>
    /// Gets the map of composition exports.
    /// </summary>
    public Dictionary<(Type ContractType, string ContractName), Type> Exports { get; } = [];

    /// <summary>
    /// Gets the exported service having
    /// contract type <typeparamref name="T"/> and <paramref name="contractName"/>.
    /// </summary>
    /// <typeparam name="T">The contract <see cref="Type"/>.</typeparam>
    /// <param name="contractName">The contract name.</param>
    /// <returns>The service type or null if not found.</returns>
    public Type? GetExport<T>(string contractName) => GetExport(typeof(T), contractName);

    /// <summary>
    /// Gets the exported service having
    /// <paramref name="contractType"/> and <paramref name="contractName"/>.
    /// </summary>
    /// <param name="contractType">The contract <see cref="Type"/>.</param>
    /// <param name="contractName">The contract name.</param>
    /// <returns>The service type or null if not found.</returns>
    public Type? GetExport(Type contractType, string contractName) =>
        Exports.TryGetValue((contractType, contractName), out var implementorType) ?
            implementorType : null;
}
