// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Reflection;
using System.Runtime.CompilerServices;

namespace Deskimg.Composition;

/// <summary>
/// Represents a CalDAV plugins loader.
/// </summary>
/// <remarks>
/// The PublishTrimmed msbuild property may trim away features required by plugins.
/// <para/>
/// In order to resolve the issue, the plugin may be linked to the Blazor application.
/// Or use the PublishTargetFramework=browser msbuild property.
/// </remarks>
public sealed class CalDAVExports(
    ILogger<CalDAVExports> logger,
    EndpointOptions endpoint,
    HttpClient http,
    CompositionService compositionService) {

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
#if DEBUG
        MaxAge = TimeSpan.Zero
#else
        MaxAge = TimeSpan.FromMinutes(5)
#endif
    };

    /// <summary>
    /// Loads all exported compositions from the CalDAV WebAPI repository.
    /// </summary>
    /// <param name="cancellationToken">The optional abort token.</param>
    /// <returns>The async result.</returns>
    public async Task PropFindAsync(CancellationToken cancellationToken = default) {
        try {
            await foreach (var assemblyExports in GetAssemblyExportsAsync(cancellationToken)) {
                foreach (var export in assemblyExports.GetExports()) {
                    compositionService.Exports.Add(export.Key, export.Value);
                }
            }
        } catch (Exception ex) {
            logger.FailedLoadingPlugins(ex);
        }
    }

    async IAsyncEnumerable<IAssemblyExports> GetAssemblyExportsAsync([EnumeratorCancellation] CancellationToken cancellationToken) {
        Uri? requestUri = endpoint.Plugins();
        if (requestUri is not null) {

            logger.FetchStarting(requestUri, CacheControl);
            var response = await http.PropFindAsync(
                requestUri,
                CacheControl,
                cancellationToken);

            foreach (var item in response) {
                if (MediaTypeNames.Application.XPlugin != item.ContentType) {
                    continue;
                }

                // TODO: Cache-Control
                logger.FetchStarting(requestUri);
                byte[] rawAssembly = await http.GetByteArrayAsync(
                    item.Source,
                    cancellationToken);

                AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
                Assembly assembly = AppDomain.CurrentDomain.Load(rawAssembly);
                var types =
                    from t in assembly.GetExportedTypes()
                    where t.GetInterfaces().Any(x => x.UnderlyingSystemType == typeof(IAssemblyExports))
                    select t;
                foreach (var t in types) {
                    if (Activator.CreateInstance(t) is IAssemblyExports assemblyExports) {
                        yield return assemblyExports;
                    }
                }
            }
        }
    }

    Assembly? CurrentDomain_AssemblyResolve(object? sender, ResolveEventArgs args) {
        // maybe fix: plugins should be contained in one assembly?
        return null;
    }
}
