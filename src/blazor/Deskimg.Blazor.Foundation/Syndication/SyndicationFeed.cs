// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Xml.Linq;

namespace Deskimg.Syndication;

/// <summary>
/// Represents a top-level feed object, &lt;feed&gt; in Atom 1.0 and &lt;rss&gt; in RSS 2.0.
/// </summary>
public class SyndicationFeed {

    static readonly XNamespace s_a10 = "http://www.w3.org/2005/Atom";
    static readonly XNamespace s_media = "http://search.yahoo.com/mrss/";

    /// <summary>
    /// Gets or sets the feed items.
    /// </summary>
    public IEnumerable<SyndicationItem>? Items { get; set; }

    /// <summary>
    /// Loads a syndication feed from the specified stream.
    /// </summary>
    /// <param name="stream">The <see cref="Stream"/> to load the feed from.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    /// <exception cref="NotSupportedException"></exception>
    public async Task LoadAsync(Stream stream, CancellationToken cancellationToken = default) {
        XDocument document = await XDocument.LoadAsync(stream, LoadOptions.None, cancellationToken);
        Items = document.Root?.Name.LocalName switch {
            "rss" => ParseRss20(document),
            "feed" => ParseAtom10(document),
            _ => throw new NotSupportedException($"Unknown root element '{document.Root?.Name}'.")
        };
    }

    static IEnumerable<SyndicationItem> ParseAtom10(XDocument document) =>
        from item in document.Descendants(s_a10 + "entry")
        select new SyndicationItem {
            Id = item.Element(s_a10 + "id")?.Value ?? Guid.NewGuid().ToString("n"),
            Published = DateTimeOffset.Parse(item.Element(s_a10 + "published")?.Value ?? string.Empty, CultureInfo.InvariantCulture),
            Title = item.Element(s_a10 + "title")?.Value,
            Summary = item.Element(s_a10 + "summary")?.Value,
            Link = (
                from link in item.Elements(s_a10 + "link")
                where (link.Attribute("rel")?.Value ?? string.Empty) == "alternate"
                select link
            ).FirstOrDefault()?.Attribute("href")?.Value,
            Enclosure = (
                from link in item.Elements(s_a10 + "link")
                where (link.Attribute("rel")?.Value ?? string.Empty) == "enclosure"
                select link
            ).FirstOrDefault()?.Attribute("href")?.Value
        };

    static IEnumerable<SyndicationItem> ParseRss20(XDocument document) =>
        from item in document.Descendants("item")
        select new SyndicationItem {
            Id = item.Element("guid")?.Value ?? Guid.NewGuid().ToString("n"),
            Published = DateTimeOffset.Parse(item.Element("pubDate")?.Value ?? string.Empty, CultureInfo.InvariantCulture),
            Title = item.Element("title")?.Value,
            Summary = item.Element("description")?.Value,
            Link = item.Element("link")?.Value,
            Enclosure = item.Element("enclosure")?.Attribute("url")?.Value ?? (
                from content in item.Elements(s_media + "content")
                where (content.Attribute("medium")?.Value ?? string.Empty) == "image"
                select content
            ).FirstOrDefault()?.Attribute("url")?.Value
        };
}
