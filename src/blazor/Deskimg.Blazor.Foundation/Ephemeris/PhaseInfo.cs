// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Ephemeris;

/// <summary>
/// The known moon phases in mille (0..1000).
/// </summary>
public enum LunarPhaseM {

    /// <summary>🌑</summary>
    New = 0,

    /// <summary>🌒</summary>
    WaxingCrescent = 125,

    /// <summary>🌓</summary>
    FirstQuarter = 250,

    /// <summary>🌔</summary>
    WaxingGibbous = 375,

    /// <summary>🌕</summary>
    Full = 500,

    /// <summary>🌖</summary>
    WaningGibbous = 625,

    /// <summary>🌗</summary>
    LastQuarter = 750,

    /// <summary>🌘</summary>
    WaningCrescent = 875
}

/// <summary>
/// Represents the moon phase info.
/// </summary>
public struct PhaseInfo : IEquatable<PhaseInfo> {

    /// <summary>
    /// Gets the date and time on the event.
    /// </summary>
    public DateTimeOffset When { get; init; }

    /// <summary>
    /// Gets the lunar phase.
    /// </summary>
    public LunarPhaseM Phase { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="PhaseInfo"/> class.
    /// </summary>
    public PhaseInfo() { }

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    /// <param name="when">The <see cref="When"/> property.</param>
    /// <param name="phase">The <see cref="Phase"/> property.</param>
    public PhaseInfo(DateTimeOffset when, LunarPhaseM phase) => (When, Phase) = (when, phase);

    #region IEquatable

    /// <inheritdoc/>
    public override int GetHashCode() => Phase.GetHashCode() ^ When.GetHashCode();

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is PhaseInfo other && Equals(other);

    /// <inheritdoc/>
    public bool Equals(PhaseInfo other) => GetHashCode() == other.GetHashCode();

    /// <inheritdoc cref="Equals(PhaseInfo)"/>
    public static bool operator ==(PhaseInfo left, PhaseInfo right) => left.Equals(right);

    /// <inheritdoc cref="Equals(PhaseInfo)"/>
    public static bool operator !=(PhaseInfo left, PhaseInfo right) => !(left == right);

    #endregion
}
