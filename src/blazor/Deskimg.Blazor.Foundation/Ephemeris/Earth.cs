// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using NodaTime;

namespace Deskimg.Ephemeris;

/// <summary>
/// Represents the <see cref="ICelestialBody" /> of earth.
/// </summary>
public class Earth(IJSRuntime js) : ICelestialBody {

    /// <inheritdoc/>
    public ValueTask<RiseSet> GetRiseSetAsync<T>(
        DateTimeOffset when,
        Geocoordinate coords,
        CancellationToken cancellationToken = default) where T : ICelestialBody => js.InvokeAsync<RiseSet>(
            $"libnova.{typeof(T).Name.ToLowerInvariant()}.getRiseSet",
            cancellationToken,
            Instant.FromDateTimeOffset(when).ToJulianDate(), coords.Latitude, coords.Longitude);

    /// <inheritdoc/>
    public ValueTask<ICollection<AltitudeItem>> GetApparentTopocentricHorizontalCoordinatesAsync<T>(
        DateTimeOffset when,
        Geocoordinate coords,
        double incr,
        double until,
        CancellationToken cancellationToken) where T : ICelestialBody => js.InvokeAsync<ICollection<AltitudeItem>>(
            $"libnova.{typeof(T).Name.ToLowerInvariant()}.getApparentTopocentricHorizontalCoordinates",
            cancellationToken,
            Instant.FromDateTimeOffset(when).ToJulianDate(), coords.Latitude, coords.Longitude, incr, until);

    /// <inheritdoc/>
    public ValueTask<ICollection<AltitudeItem>> GetApparentTopocentricHorizontalCoordinatesAsync(
        Type celestialBody,
        DateTimeOffset when,
        Geocoordinate observer,
        double incr,
        double until,
        CancellationToken cancellationToken = default) => celestialBody switch {
            Type when celestialBody == typeof(Solar) =>
                GetApparentTopocentricHorizontalCoordinatesAsync<Solar>(when, observer, incr, until, cancellationToken),
            Type when celestialBody == typeof(Lunar) =>
                GetApparentTopocentricHorizontalCoordinatesAsync<Lunar>(when, observer, incr, until, cancellationToken),
            _ => throw new ArgumentOutOfRangeException(nameof(celestialBody))
        };

    /// <inheritdoc cref="ICelestialBody.GetUpcomingLunarPhaseAsync"/>
    public async IAsyncEnumerable<PhaseInfo> GetUpcomingLunarPhaseAsync<T>(
        DateTimeOffset when,
        [EnumeratorCancellation] CancellationToken cancellationToken = default) where T : ICelestialBody {

        double jd = Instant.FromDateTimeOffset(when).ToJulianDate();
        for (int phase = 0; phase <= 750; phase += 250) {
            var next = await js.InvokeAsync<DateTimeOffset>("libnova.lunar.getUpcomingPhase", cancellationToken,
                jd, phase);
            yield return new PhaseInfo(next, (LunarPhaseM)phase);
        }
    }
}
