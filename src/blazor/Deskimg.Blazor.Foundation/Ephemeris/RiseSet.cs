// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Ephemeris;

/// <summary>
/// Represents a apparent topocentric horizontal coordinate.
/// </summary>
/// <param name="X">The x-axis value.</param>
/// <param name="Y">The y-axis value.</param>
public record struct AltitudeItem(DateTimeOffset X, double Y);

/// <summary>
/// Represent celestial body rise and set information.
/// </summary>
public class RiseSet {

    //const double SetFactor = -1.0;
    const double MidnightFactor = -0.5;
    //const double RiseFactor = 0.0;
    const double NoonFactor = 0.5;
    //const double SetFactor2 = 1.0;

    /// <summary>
    /// Gets or sets when rise event occur.
    /// </summary>
    public DateTimeOffset? Rise { get; init; }

    /// <summary>
    /// Gets or sets when set event occur.
    /// </summary>
    public DateTimeOffset? Set { get; init; }

    /// <summary>
    /// Gets or sets above horizon all day indicator.
    /// </summary>
    public bool AboveHorizon { get; init; }

    /// <summary>
    /// Gets or sets below horizon all day indicator.
    /// </summary>
    public bool BelowHorizon { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="RiseSet"/> class.
    /// </summary>
    /// <remarks>
    /// Used primarily during serialization.
    /// </remarks>
    public RiseSet() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="RiseSet"/> class.
    /// </summary>
    /// <param name="rise">The <see cref="Rise"/> property.</param>
    /// <param name="set">The <see cref="Set"/> property.</param>
    public RiseSet(DateTimeOffset rise, DateTimeOffset set) => (Rise, Set) = (rise, set);

    /// <summary>
    /// Initializes a new instance of the <see cref="RiseSet"/> class.
    /// </summary>
    /// <param name="aboveHorizon">The <see cref="AboveHorizon"/> property.</param>
    /// <param name="belowHorizon">The <see cref="BelowHorizon"/> property.</param>
    public RiseSet(bool aboveHorizon = false, bool belowHorizon = false) {
        if (!aboveHorizon && !belowHorizon) {
            throw new ArgumentException($"One of ${nameof(aboveHorizon)} or ${belowHorizon} must be set");
        }

        AboveHorizon = aboveHorizon;
        BelowHorizon = belowHorizon;
    }


    /// <summary>
    /// Gets the daylight factor.
    /// </summary>
    /// <param name="when">The time of interest.</param>
    /// <returns>
    /// <list type="bullet">
    /// <item>Set = -1.0</item>
    /// <item>Midnight = -0.5</item>
    /// <item>Rise = 0.0</item>
    /// <item>Noon = 0.5</item>
    /// <item>Set = 1.0</item>
    /// </list>
    /// </returns>
    public double GetDaylightFactor(DateTimeOffset when) {

        // midnight sun or the day the sun don't set
        if (AboveHorizon || (Rise is not null && Set is null)) {
            return NoonFactor;
        } else

        // polar night or the day the sun don't rise
        if (BelowHorizon || (Set is not null && Rise is null)) {
            return MidnightFactor;
        } else

        // somehow one of rise or set is null but state is Unknown or RiseAndSet (should not be)
        if (Rise is null || Set is null) {
            return when.ToLocalTime().Hour > 6 && when.ToLocalTime().Hour < 18 ? NoonFactor : MidnightFactor;

            // both rise and set is not null
        } else {
            double whenTicks = when.UtcTicks;
            double riseTicks = Rise.Value.UtcTicks;
            double setTicks = Set.Value.UtcTicks;

            // polartwilight, Set or Rise occurs on the next day
            DateTime whenLocalDate = when.ToLocalTime().Date;
            DateTime riseLocalDate = Rise.Value.ToLocalTime().Date;
            DateTime setLocalDate = Set.Value.ToLocalTime().Date;
            double direction = whenLocalDate != setLocalDate || whenLocalDate != riseLocalDate ? -1.0 : 1.0;

            direction = 1;

            // daytime, Now is between rise and set
            if (riseTicks <= whenTicks && whenTicks < setTicks) {
                return direction * (whenTicks - riseTicks) / (setTicks - riseTicks);
            } else

            // night waning, Now is after set
            if (whenTicks >= setTicks) {
                return direction * (-1.0f + ((whenTicks - setTicks) / (riseTicks + TimeSpan.FromDays(1).Ticks - setTicks)));
            } else

            // night waxing, Now is before rise
            if (whenTicks < riseTicks) {
                return direction * (whenTicks - riseTicks) / (riseTicks + TimeSpan.FromDays(1).Ticks - setTicks);
            } else {
                throw new InvalidOperationException();
            }
        }
    }
}
