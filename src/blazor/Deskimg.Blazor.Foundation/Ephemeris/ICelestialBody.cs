// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Ephemeris;

/// <summary>
/// Represents a celestial body.
/// </summary>
public interface ICelestialBody {

    /// <summary>
    /// Gets the celestial body rise and set information.
    /// </summary>
    /// <typeparam name="T">An <see cref="ICelestialBody"/>.</typeparam>
    /// <param name="when">The time of interest.</param>
    /// <param name="observer">The observer coordinates.</param>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns>The rise and set information.</returns>
    /// <exception cref="NotSupportedException">The <see cref="ICelestialBody"/> don't support this operation.</exception>
    ValueTask<RiseSet> GetRiseSetAsync<T>(
        DateTimeOffset when,
        Geoposition observer,
        CancellationToken cancellationToken = default) where T : ICelestialBody =>
            throw new NotSupportedException(nameof(GetRiseSetAsync));

    /// <summary>
    /// Gets the celestial body apparent topocentric horizontal coordinates.
    /// </summary>
    /// <typeparam name="T">An <see cref="ICelestialBody"/>.</typeparam>
    /// <param name="when">The time of interest.</param>
    /// <param name="observer">The observer coordinates.</param>
    /// <param name="incr">The number of julian date units to increment each step.</param>
    /// <param name="until">The number of julia date units to step until.</param>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns></returns>
    /// <exception cref="NotSupportedException">The <see cref="ICelestialBody"/> don't support this operation.</exception>
    ValueTask<ICollection<AltitudeItem>> GetApparentTopocentricHorizontalCoordinatesAsync<T>(
        DateTimeOffset when,
        Geoposition observer,
        double incr,
        double until,
        CancellationToken cancellationToken = default) where T : ICelestialBody =>
            throw new NotSupportedException(nameof(GetApparentTopocentricHorizontalCoordinatesAsync));

    /// <summary>
    /// Gets the next lunar phases.
    /// </summary>
    /// <typeparam name="T">An <see cref="ICelestialBody"/>.</typeparam>
    /// <param name="when">The time of interest.</param>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns>The lunar phases.</returns>
    /// <exception cref="NotSupportedException">The <see cref="ICelestialBody"/> don't support this operation.</exception>
    IAsyncEnumerable<PhaseInfo> GetUpcomingLunarPhaseAsync<T>(
        DateTimeOffset when,
        CancellationToken cancellationToken = default) where T : ICelestialBody =>
            throw new NotSupportedException(nameof(GetUpcomingLunarPhaseAsync));
}
