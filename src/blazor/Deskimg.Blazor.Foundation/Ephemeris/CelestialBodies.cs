// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Ephemeris;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public sealed class Lunar : ICelestialBody { }

public sealed class Solar : ICelestialBody { }

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
