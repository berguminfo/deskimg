// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Reflection;
using System.Text.Json;

namespace Deskimg;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public sealed record Strings(
    string Language,
    string TemperatureUnit,
    string PrecipitationUnit,
    string SpeedUnit,
    string PressureUnit,
    ImmutableArray<string> LongCardinalDirections,
    ImmutableArray<string> BeaufortDesignation,
    string VariableWindFormat,
    string CardinalWindFormat,
    string UltravioletIndexFormat,
    string? YearMonthPattern,
    ImmutableArray<string>? MonthNames,
    ImmutableArray<string>? ShortestDayNames,
    string WeekNumberFormat,
    string WeatherWarningFormat,
    string Avalanche,
    ImmutableDictionary<string, object> EndpointOptions,
    ModalStrings Modal,
    NavigationStrings Navigation
);

public sealed record ModalStrings(
    string Ok,
    string Cancel,
    string Close,
    string Use,
    string Back
);

public sealed record NavigationStrings(
    string DetailsTitle,
    string Now,
    string Duration,
    ImmutableList<string> DurationFormat,
    string Backward,
    string Forward,
    string Search,
    string Date,
    string Time,
    string Latitude,
    string Longitude
);

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

/// <summary>
/// This class exposes the application string resources embedded as JSON files.
/// </summary>
/// <remarks>
/// Put the JSON string resources in the 'Resources' directory.
/// Use the Embedded Resource build tool in order embed the strings.
/// Each resource should be named using RFC-3282 language codes.
/// </remarks>
public static class SR {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    static Strings? s_strings;

    /// <summary>
    /// Gets the default <see cref="Strings"/> resource.
    /// </summary>
    public static Strings Default {
        get {
            if (s_strings is null) {
                throw new InvalidOperationException();
            }

            return s_strings;
        }
    }

    /// <see cref="InitializeAsync(string, string, CancellationToken)"/>
    public static Task InitializeAsync(string cultureName, CancellationToken cancellationToken = default) =>
        InitializeAsync(cultureName, "en-GB", cancellationToken);

    /// <summary>
    /// Initializes string resources in the given culture.
    /// </summary>
    /// <param name="cultureName">The culture name.</param>
    /// <param name="fallbackCultureName">The fallback culture name.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> that can be used to cancel the read operation.</param>
    /// <exception cref="JsonException">
    /// The JSON is invalid,
    /// <see cref="Strings"/> is not compatible with the JSON,
    /// or when there is remaining data in the Stream.
    /// </exception>
    /// <exception cref="NotSupportedException">
    /// There is no compatible <see cref="System.Text.Json.Serialization.JsonConverter"/>
    /// for <see cref="Strings"/> or its serialize able members.
    /// </exception>
    public static async Task InitializeAsync(string cultureName, string fallbackCultureName, CancellationToken cancellationToken = default) {

        async Task initializeAsyncInternal(string name) {
            var resourceName = $"Deskimg.Resources.langs.{name}.json";
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName) ??
                throw new FileNotFoundException(resourceName);
            s_strings = await JsonSerializer.DeserializeAsync<Strings>(stream, s_sourceGenOptions, cancellationToken);
        }

        try {
            await initializeAsyncInternal(cultureName);
        } catch (Exception) {
            await initializeAsyncInternal(fallbackCultureName);
        }
    }
}
