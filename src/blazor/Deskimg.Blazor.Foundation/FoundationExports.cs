// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.


namespace Deskimg;

/// <summary>
/// Represents this assembly composition exports.
/// </summary>
[AssemblyExportsGenerated]
public partial class FoundationExports : IAssemblyExports {

    /// <inheritdoc cref="IAssemblyExports.GetExports"/>
    public partial IImmutableDictionary<(Type, string), Type> GetExports();
}
