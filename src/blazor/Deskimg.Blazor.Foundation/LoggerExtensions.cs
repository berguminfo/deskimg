// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net;

namespace Deskimg;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public static partial class LoggerExtensions {

    [LoggerMessage(
        EventId = 10,
        Level = LogLevel.Debug,
        Message = "Fetching the resource at '{Input}'. Cache-Control: {CacheControl}."
    )]
    static partial void FetchStartingInternal(this ILogger logger, string input, CacheControlHeaderValue? cacheControl = null);

    public static void FetchStarting(this ILogger logger, string input, CacheControlHeaderValue? cacheControl = null) =>
        FetchStartingInternal(logger, input, cacheControl);

    public static void FetchStarting(this ILogger logger, Uri requestUri, CacheControlHeaderValue? cacheControl = null) =>
        FetchStartingInternal(logger, requestUri.LocalPath, cacheControl);

    [LoggerMessage(
        EventId = 11,
        Level = LogLevel.Error,
        Message = "Failed fetching the resource at '{Input}'. Status: {Status}."
    )]
    static partial void FetchFailedInternal(this ILogger logger, Exception exception, string input, HttpStatusCode? status);

    public static void FetchFailed(this ILogger logger, Exception exception, string input, HttpStatusCode? status) =>
        FetchFailedInternal(logger, exception, input, status);

    public static void FetchFailed(this ILogger logger, Exception exception, Uri requestUri, HttpStatusCode? status) =>
        FetchFailedInternal(logger, exception, requestUri.AbsoluteUri, status);

    [LoggerMessage(
        EventId = 12,
        Level = LogLevel.Warning,
        Message = "Failed deserialize JSON response at '{Input}'. Line: {Line} | Position: {Position}."
    )]
    static partial void DeserializeJsonFailedInternal(this ILogger logger, Exception exception, string input, long? line, long? position);

    public static void DeserializeJsonFailed(this ILogger logger, Exception exception, string input, long? line, long? position) =>
        DeserializeJsonFailedInternal(logger, exception, input, line, position);

    public static void DeserializeJsonFailed(this ILogger logger, Exception exception, Uri requestUri, long? line, long? position) =>
        DeserializeJsonFailedInternal(logger, exception, requestUri.AbsoluteUri, line, position);

    [LoggerMessage(
        EventId = 13,
        Level = LogLevel.Warning,
        Message = "Failed deserialize XML response at '{Input}'. Line: {Line} | Position: {Position}."
    )]
    static partial void DeserializeXmlFailedInternal(this ILogger logger, Exception exception, string input, long? line, long? position);

    public static void DeserializeXmlFailed(this ILogger logger, Exception exception, string input, long? line, long? position) =>
        DeserializeXmlFailedInternal(logger, exception, input, line, position);

    public static void DeserializeXmlFailed(this ILogger logger, Exception exception, Uri requestUri, long? line, long? position) =>
        DeserializeXmlFailedInternal(logger, exception, requestUri.AbsoluteUri, line, position);

    [LoggerMessage(
        EventId = 14,
        Level = LogLevel.Warning,
        Message = "Failed parse METAR response at '{Input}'."
    )]
    static partial void ParseMetarFailedInternal(this ILogger logger, Exception exception, string input);

    public static void ParseMetarFailed(this ILogger logger, Exception exception, string input) =>
        ParseMetarFailedInternal(logger, exception, input);

    public static void ParseMetarFailed(this ILogger logger, Exception exception, Uri requestUri) =>
        ParseMetarFailedInternal(logger, exception, requestUri.AbsoluteUri);

    [LoggerMessage(
        EventId = 15,
        Level = LogLevel.Debug,
        Message = "METAR response '{Line}'."
    )]
    public static partial void FetchMetarResponse(this ILogger logger, string? line);

    [LoggerMessage(
        EventId = 16,
        Level = LogLevel.Warning,
        Message = "Failed parse cap document. Path: cap:info[cap:language='{Language}']."
    )]
    public static partial void ParseCapFailed(this ILogger logger, string? language);

    [LoggerMessage(
        EventId = 17,
        Level = LogLevel.Error,
        Message = "Failed loading plugins."
    )]
    public static partial void FailedLoadingPlugins(this ILogger logger, Exception ex);

    [LoggerMessage(
        EventId = 18,
        Level = LogLevel.Error,
        Message = "The Endpoint section '{key}' has not been configurated."
    )]
    public static partial void BadEndpoint(this ILogger logger, string key);
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
