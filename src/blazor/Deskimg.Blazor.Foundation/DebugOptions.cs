// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if DEBUG

namespace Deskimg;

/// <summary>
/// Represents the Session:Dbug <see cref="IConfigurationSection"/>.
/// </summary>
public class DebugOptions {

    /// <summary>
    /// The <see cref="IConfiguration.GetSection"/> name.
    /// </summary>
    public const string Name = "Session:Dbug";

    /// <summary>
    /// Gets or sets the fetch.api endpoint key.
    /// </summary>
    public string? Fetch { get; set; }

    /// <summary>
    /// Gets or sets the sets.api endpoint key.
    /// </summary>
    public string? Sets { get; set; }

    /// <summary>
    /// Gets or sets the caldav.api endpoint key.
    /// </summary>
    public string? CalDAV { get; set; }
}

#endif
