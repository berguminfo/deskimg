// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public enum Category {
    None,

    // RFC-5445
    Appointment,
    Education,
    Meeting,

    // holidays
    Holiday,
    NationalHoliday,
    LocalHoliday,
    FederalHoliday,
    BankHoliday,
    LocalBankHoliday,
    FlagDay,
    LocalFlagDay,
    Season,
    ClockChange,
    Observance,
    ImportantObservance,
    CommonObservance,
    MajorChristian,
    MoreChristian,
    JewishHoliday,
    MajorJewish,
    MoreJewish,
    MajorMuslim,
    MajorHinduism,
    MajorOrthodox
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
