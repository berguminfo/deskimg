// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Collections.ObjectModel;

namespace Deskimg.Calendar;

/// <summary>
/// Represents a <see cref="ISubsetCalendar"/> having multiple <see cref="VEvent"/> items.
/// </summary>
public class VEventsCalendar : Collection<VEvent>, ISubsetCalendar {

    /// <inheritdoc/>
    public virtual Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default) => Task.FromResult(Subset(rangeStart, rangeLength));

    /// <see cref="SubsetAsync"/>
    protected virtual IEnumerable<SubsetResult> Subset(DateTimeOffset rangeStart, TimeSpan rangeLength) {
        if (Items.Count == 0) {
            yield break;
        }

        foreach (VEvent item in Items) {

            // mono: System.NullReferenceException?
            if (item is null) {
                continue;
            }

            DateTimeOffset start =
                // choose Start
                item.Start is not null ?
                    item.Start.Value :
                // choose StartDate + StartTime
                // TODO: correct for local time zone
                // NOTE: start may be MinValue here
                item.StartDate is not null ?
                    new(item.StartDate.Value.ToDateTime(item.StartTime ?? TimeOnly.MinValue, DateTimeKind.Utc)) :
                // no start date; start at rangeStart
                rangeStart;

            DateTimeOffset end =
                // choose End
                item.End is not null ?
                    item.End.Value :
                // choose Start + Duration
                item.Start is not null && item.Duration is not null ?
                    item.Start.Value + item.Duration.Value :
                // choose EndDate + EndTime
                item.EndDate is not null ?
                    new(item.EndDate.Value.ToDateTime(item.EndTime ?? TimeOnly.MaxValue, DateTimeKind.Utc)) :
                // not end, stop at rangeStart + rangeLength
                rangeStart + rangeLength;

            // check if the event may occur
            bool validStart = start <= (rangeStart + rangeLength);
            bool validEnd = end >= rangeStart;
            if (!validStart || !validEnd) {
                continue;
            }

            if (item.RecurrenceRule.Recurrence == VEventRecurrence.Once) {

                // yield the event if inside query interval
                if (start.Contains(rangeStart, rangeLength)) {
                    yield return new(start, item, 1);
                }
            } else {

                // get recurrence rules
                var setRules = item.GetSetRules(start);
                var repeatRules = item.GetRepeatRules();
                int occurrences = item.GetOccurrences(start, end);

                // enter yield loop
                DateTimeOffset tryWhen;
                DateTimeOffset nextWhen = item.RecurrenceRule.Recurrence == VEventRecurrence.Daily ?
                    start : rangeStart;
                DateTimeOffset rangeEnd = rangeStart + rangeLength;
                while (nextWhen <= rangeEnd) {

                    // prevent dead-loop:
                    // - in case both setRules and repeatRules fails to advance nextWhen,
                    //   the loop will never end and spins forever (or stack overflow).
                    tryWhen = nextWhen;

                    // apply set:
                    // - should advance nextWhen to the next matching date, or possible leave as is
                    nextWhen = setRules(nextWhen);
                    if (nextWhen.Contains(rangeStart, rangeLength)) {
                        yield return new(nextWhen, item, occurrences);
                    } else {
                        // IMPORTANT:
                        // - the loop may very well not jet have reached a valid nextWhen,
                        //   hence don't break here, let loop continue until endInterval
                        // NOTE: about VEventsCalendar
                        // - ensure to apply .Take before .ToList, .ToArray, ...
                    }

                    // apply repeat:
                    // - should advance nextWhen by "jumping" over unwanted matches (like each second year rules)
                    nextWhen = repeatRules(nextWhen);
                    ++occurrences;

                    // detect any possible dead-loop
                    if (tryWhen == nextWhen) {
                        break;
                    }
                }
            }
        }
    }
}
