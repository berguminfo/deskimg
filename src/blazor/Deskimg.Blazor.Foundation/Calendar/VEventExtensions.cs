// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.ComponentModel;
using NodaTime;

namespace Deskimg.Calendar;

using Rule = Func<DateTimeOffset, DateTimeOffset>;

/// <summary>
/// Extensions methods for working with <see cref="RecurrenceRule"/>.
/// </summary>
public static class VEventExtensions {

    /// <summary>
    /// Get the recurrence set rules.
    /// </summary>
    /// <param name="vevent">The <see cref="RecurrenceRule"/>.</param>
    /// <param name="start">The <see cref="DateTimeOffset"/> iteration start.</param>
    /// <returns>The set rules.</returns>
    /// <exception cref="InvalidEnumArgumentException">Unknown <see cref="RecurrenceRule"/>.</exception>
    public static Rule GetSetRules(this VEvent vevent, DateTimeOffset start) {
        RecurrenceRule rule = vevent.RecurrenceRule;

        switch (rule.Recurrence) {

            case VEventRecurrence.Daily:
                return dateTime => dateTime;

            case VEventRecurrence.Weekly:
                return dateTime => dateTime.Next(WeekOfMonth.None, rule.DaysOfWeek);

            case VEventRecurrence.Monthly:
                if (rule.WeekOfMonth == WeekOfMonth.None && rule.DaysOfWeek == DaysOfWeek.None) {
                    return dateTime => dateTime.Next(rule.WeekOfMonth, rule.DaysOfWeek);
                } else
                if ((vevent.Start?.Day ?? vevent.StartDate?.Day) is int monthlyDay) {
                    return dateTime => dateTime.Date(dateTime.Month, monthlyDay);
                } else {
                    throw new InvalidOperationException($"{nameof(DateTime.Day)} is null");
                }

            case VEventRecurrence.Yearly:
                if ((vevent.Start?.Month ?? vevent.StartDate?.Month) is int yearlyMonth &&
                    (vevent.Start?.Day ?? vevent.StartDate?.Day) is int yearlyDay) {

                    if (rule.WeekOfMonth == WeekOfMonth.None && rule.DaysOfWeek == DaysOfWeek.None) {
                        return dateTime => dateTime.Date(yearlyMonth, yearlyDay);
                    } else {
                        return dateTime => dateTime
                            .AddYears(rule.Interval <= 1 ? 0 : (dateTime.Year - start.Year) % rule.Interval)
                            .Date(yearlyMonth, yearlyDay)
                            .Next(rule.WeekOfMonth, rule.DaysOfWeek);
                    }
                } else {
                    throw new InvalidOperationException($"{nameof(DateTime.Month)} or {nameof(DateTime.Day)} is null");
                }

            case VEventRecurrence.Easter:
                return dateTime => dateTime.Easter().AddDays(rule.Interval);

            default:
                throw new InvalidEnumArgumentException();
        }
    }

    /// <summary>
    /// Get the recurrence repeat rules.
    /// </summary>
    /// <param name="vevent">The <see cref="RecurrenceRule"/>.</param>
    /// <returns>The repeat rules.</returns>
    /// <exception cref="NotSupportedException">Weekly recurrence can't have Interval > 1.</exception>
    /// <exception cref="InvalidEnumArgumentException">Unknown <see cref="RecurrenceRule"/>.</exception>
    public static Rule GetRepeatRules(this VEvent vevent) {
        RecurrenceRule rule = vevent.RecurrenceRule;

        switch (rule.Recurrence) {

            case VEventRecurrence.Daily:
                return dateTime => dateTime.AddDays(rule.Interval);

            case VEventRecurrence.Weekly:
                // - next SetRules may match a day "in this week", hence Interval > 1
                //   would require multiple RepeatRules depending on the week SetRules set.
                // - for now this feature should be unsupported
                if (rule.Interval > 1) {
                    throw new NotSupportedException("Interval > 1");
                }

                return dateTime => dateTime.AddDays(rule.Interval);

            case VEventRecurrence.Monthly:
                return dateTime => dateTime.AddMonths(rule.Interval);

            case VEventRecurrence.Yearly:
                return dateTime => dateTime.AddYears(rule.Interval);

            case VEventRecurrence.Easter:
                return dateTime => dateTime.AddYears(1);

            default:
                throw new InvalidEnumArgumentException();
        }
    }

    /// <summary>
    /// Gets the number of occurrences between <paramref name="start"/> and <paramref name="end"/>.
    /// </summary>
    /// <param name="vevent">The <see cref="VEvent"/>.</param>
    /// <param name="start">The interval start.</param>
    /// <param name="end">The interval end.</param>
    /// <returns>The value.</returns>
    /// <exception cref="InvalidOperationException">Interval -le 0.</exception>
    public static int GetOccurrences(this VEvent vevent, DateTimeOffset start, DateTimeOffset end) {
        RecurrenceRule rule = vevent.RecurrenceRule;

        // prevent division by zero
        if (rule.Recurrence != VEventRecurrence.Easter && rule.Interval == 0) {
            throw new InvalidOperationException("Interval is 0");
        }

        switch (rule.Recurrence) {

            case VEventRecurrence.Daily:
                return (Convert.ToInt32(Instant.FromDateTimeOffset(end).ToJulianDate()) -
                        Convert.ToInt32(Instant.FromDateTimeOffset(start).ToJulianDate())) / rule.Interval;

            case VEventRecurrence.Weekly:
            case VEventRecurrence.Monthly:
                int months =
                    (((end.Year - start.Year) * rule.Calendar.GetMonthsInYear(start.Year)) +
                       end.Month -
                       start.Month) / rule.Interval;
                return rule.Recurrence != VEventRecurrence.Weekly ?
                    months :
                    months / DateTimeOffsetExtensions.DaysInWeek;

            case VEventRecurrence.Yearly:
                return (end.Year - start.Year) / rule.Interval;

            case VEventRecurrence.Easter:
                return end.Year - start.Year;

            default:
                throw new InvalidOperationException();
        }
    }
}
