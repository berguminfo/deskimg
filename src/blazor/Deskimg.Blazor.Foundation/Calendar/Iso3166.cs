// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

/// <summary>
/// Represents the ISO-3166 language codes.
/// </summary>
[Iso3166Generated]
public partial class Iso3166 { }
