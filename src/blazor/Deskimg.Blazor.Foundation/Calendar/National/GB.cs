// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Great Britain.
/// </summary>
[Export<ISubsetCalendar>(nameof(GB))]
public class GB : VEventsCalendar {

    /// <inheritdoc/>
    protected virtual Iso3166 Country => Iso3166.GB;

    /// <inheritdoc/>
    protected virtual string IntlImage => "flag-gb";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public GB() {
        Add(new() {
            Summary = "Sunday",
            Location = Country.ThreeLetterISORegionName,
            Classification = AccessClassification.Private,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Weekly,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });
        Add(new() {
            Summary = "New Year's Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 1, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Good Friday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = -2
            }
        });
        Add(new() {
            Summary = "Easter Sunday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 0
            }
        });
        Add(new() {
            Summary = "Early May Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 5, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.First,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "Spring Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 5, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.Last,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "Christmas Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 25),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Boxing Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 26),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Bank Holiday for the Coronation of King Charles III",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(2023, 5, 8),
            EndDate = new(2023, 5, 9)
        });
    }
}
