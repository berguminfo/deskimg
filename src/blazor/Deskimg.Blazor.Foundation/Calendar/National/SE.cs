// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Sweden.
/// </summary>
/// <remarks>
/// <list type="bullet">
/// <item><see href="https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-1989253-om-allmanna-helgdagar_sfs-1989-253"/></item>
/// <item><see href="https://lagen.nu/1982:270"/></item>
/// </list>
/// </remarks>
[Export<ISubsetCalendar>(nameof(SE))]
public class SE : VEventsCalendar {

    /// <inheritdoc/>
    protected virtual Iso3166 Country => Iso3166.SE;

    /// <inheritdoc/>
    protected virtual string IntlImage => "flag-se";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public SE() {
        Add(new() {
            Summary = "Söndag",
            Location = Country.ThreeLetterISORegionName,
            Classification = AccessClassification.Private,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Weekly,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });

        AddYearly();
        AddByEaster();
    }

    void AddYearly() {
        Add(new() {
            Summary = "Nyårsdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 1, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Trettondedag jul",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 1, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Konungens namnsdag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 1, 28),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Kronprinsessans namnsdag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 3, 12),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Konungens födelsedag", // Valborgsmässoafton
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 4, 30),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Första maj", // Valborg
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 5, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Veterandagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(2008, 5, 29),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Sveriges nationaldag och svenska flaggans dag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 6, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Val till Europaparlamentet",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(2024, 6, 1), // must match interval
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                Interval = 5,
                WeekOfMonth = WeekOfMonth.Second,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });
        Add(new() {
            Summary = "Midsommardagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 6, 20),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                DaysOfWeek = DaysOfWeek.Saturday
            }
        });
        Add(new() {
            Summary = "Kronprinsessans födelsedag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 7, 14),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Drottningens namnsdag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 8, 8),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "FN-dagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 10, 24),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Alla helgons dag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 10, 31),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                DaysOfWeek = DaysOfWeek.Saturday
            }
        });
        Add(new() {
            Summary = "Gustav Adolfsdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 11, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Nobeldagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 12, 10),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Drottningens födelsedag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 12, 23),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Juldagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 12, 25),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Annandag jul",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 26),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    void AddByEaster() {
        Add(new() {
            Summary = "Långfredagen",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = -2
            }
        });
        Add(new() {
            Summary = "Påskdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 0
            }
        });
        Add(new() {
            Summary = "Annandag påsk",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        Add(new() {
            Summary = "Kristi himmelsfärdsdag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 39
            }
        });
        Add(new() {
            Summary = "Pingstdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 49
            }
        });
    }
}
