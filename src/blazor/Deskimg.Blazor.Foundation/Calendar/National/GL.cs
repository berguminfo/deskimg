// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Greenland.
/// </summary>
/// <remarks>
/// <list type="bullet">
/// <item><see href="https://da.wikipedia.org/wiki/Flagdag"/></item>
/// </list>
/// </remarks>
[Export<ISubsetCalendar>(nameof(GL))]
public class GL : DK {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.GL;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-gl";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public GL() {
        Add(new() {
            Summary = "Hellig 3 kongers dag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 1, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Ullortuneq",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1, 6, 21),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
