// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Pitcairn Islands (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(PN))]
public class PN : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.PN;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-pn";
}
