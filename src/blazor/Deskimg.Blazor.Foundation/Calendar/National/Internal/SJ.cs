// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Norway (Svalbard, Bjørnøya, Jan Mayen).
/// </summary>
[Export<ISubsetCalendar>(nameof(SJ))]
public class SJ : NO {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.SJ;

    /// <inheritdoc/>
    public SJ(IConfiguration configuration) : base(configuration) { }
}
