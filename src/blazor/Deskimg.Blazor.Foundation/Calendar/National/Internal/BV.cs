// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Norway (Bouvetøya, Peter I øy).
/// </summary>
[Export<ISubsetCalendar>(nameof(BV))]
public class BV : NO {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.BV;

    /// <inheritdoc/>
    public BV(IConfiguration configuration) : base(configuration) { }
}
