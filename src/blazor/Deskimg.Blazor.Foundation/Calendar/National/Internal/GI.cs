// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Gibraltar (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(GI))]
public class GI : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.GI;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-gi";
}
