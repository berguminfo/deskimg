// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for British Virgin Islands (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(VG))]
public class VG : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.VG;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-vg";
}
