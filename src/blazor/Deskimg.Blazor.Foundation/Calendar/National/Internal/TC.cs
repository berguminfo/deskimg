// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Turks and Caicos Islands (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(TC))]
public class TC : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.TC;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-tc";
}
