// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Anguilla (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(AI))]
public class AI : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.AI;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-ai";
}
