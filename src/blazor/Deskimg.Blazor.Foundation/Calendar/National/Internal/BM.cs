// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Bermuda (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(BM))]
public class BM : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.BM;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-bm";
}
