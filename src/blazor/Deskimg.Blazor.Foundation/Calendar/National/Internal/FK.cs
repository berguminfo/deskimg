// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Falkland Islands (Great Britain Overseas territory).
/// </summary>
[Export<ISubsetCalendar>(nameof(FK))]
public class FK : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.FK;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-fk";
}
