// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Isle of Man (Great Britain Crown dependency).
/// </summary>
[Export<ISubsetCalendar>(nameof(IM))]
public class IM : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.IM;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-im";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public IM() {
        Add(new() {
            Summary = "Easter Monday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        //T. T. Bank Holiday
        Add(new() {
            Summary = "T. T. Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 6, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.First,
                DaysOfWeek = DaysOfWeek.Friday
            }
        });
        Add(new() {
            Summary = "Tynwald Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 7, 5),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
