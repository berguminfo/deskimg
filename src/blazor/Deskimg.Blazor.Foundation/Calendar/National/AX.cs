// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Åland.
/// </summary>
[Export<ISubsetCalendar>(nameof(AX))]
public class AX : FI_SV {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.AX;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-ax";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public AX() {

        // a) den 30 mars, dagen för högtidlighållande av Ålands demilitarisering och neutralisering,
        // b) den sista söndagen i april, Ålands flaggas dag,
        // c) den 9 juni, Ålands självstyrelsedag. (1999/16)
        Add(new() {
            Summary = "Ålands demilitarisering och neutralisering",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1921, 3, 30),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Ålands flaggas dag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1954, 4, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.Last,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });
        Add(new() {
            Summary = "Ålands självstyrelsedag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1922, 6, 9),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
