// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Ireland.
/// </summary>
/// <remarks>
/// <list type="bullet">
/// <item><see href="https://www.timeanddate.com/holidays/ireland"/></item>
/// </list>
/// </remarks>
[Export<ISubsetCalendar>(nameof(IE))]
public class IE : VEventsCalendar {

    /// <inheritdoc/>
    protected virtual Iso3166 Country => Iso3166.IE;

    /// <inheritdoc/>
    protected virtual string IntlImage => "flag-ie";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public IE() {
        Add(new() {
            Summary = "Sunday",
            Location = Country.ThreeLetterISORegionName,
            Classification = AccessClassification.Private,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Weekly,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });
        Add(new() {
            Summary = "New Year's Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 1, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "St Patrick's Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 3, 17),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "St. Brigid’s Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 2, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Easter Sunday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 0
            }
        });
        Add(new() {
            Summary = "Easter Monday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        Add(new() {
            Summary = "May Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 5, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.First,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "June Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 6, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.First,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "August Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 8, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.First,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "October Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 10, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.Last,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "Christmas Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 25),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "St. Stephen's Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 26),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
