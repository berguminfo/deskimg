// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Finland.
/// </summary>
[Export<ISubsetCalendar>(nameof(FI_SV))]
public class FI_SV : VEventsCalendar {

    /// <inheritdoc/>
    protected virtual Iso3166 Country => Iso3166.FI;

    /// <inheritdoc/>
    protected virtual string IntlImage => "flag-fi";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public FI_SV() {
        Add(new() {
            Summary = "Söndag",
            Location = Country.ThreeLetterISORegionName,
            Classification = AccessClassification.Private,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Weekly,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });

        AddYearly();
        AddByEaster();
    }

    void AddYearly() {
        Add(new() {
            Summary = "Nyårsdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 1, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Trettondedag jul",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 1, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Midsommardagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 6, 20),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                DaysOfWeek = DaysOfWeek.Saturday
            }
        });
        Add(new() {
            Summary = "Alla helgons dag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 10, 31),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                DaysOfWeek = DaysOfWeek.Saturday
            }
        });
        Add(new() {
            Summary = "Självständighetsdag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 12, 6),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Juldagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1, 12, 25),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Annandag jul",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 26),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    void AddByEaster() {
        Add(new() {
            Summary = "Långfredagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = -2
            }
        });
        Add(new() {
            Summary = "Påskdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 0
            }
        });
        Add(new() {
            Summary = "Annandag påsk",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        Add(new() {
            Summary = "Kristi himmelsfärdsdag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 39
            }
        });
        Add(new() {
            Summary = "Pingstdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 49
            }
        });
    }
}
