// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Norway.
/// </summary>
/// <remarks>
/// <list type="bullet">
/// <item><see href="https://lovdata.no/dokument/LTI/forskrift/2004-12-03-1542">Based on FOR-2004-12-03.</see></item>
/// <item>Some events has been ratified in previous documents, and some has the title property modified.</item>
/// </list>
/// </remarks>
[Export<ISubsetCalendar>(nameof(NO))]
public class NO : VEventsCalendar {

    [Flags]
    enum AddFlags {
        Yearly = 1,
        Easter = 2,
        Historic = 4
    }

    const AddFlags DefaultAddFlags = AddFlags.Yearly | AddFlags.Easter;

    /// <inheritdoc/>
    protected virtual Iso3166 Country => Iso3166.NO;

    /// <inheritdoc/>
    protected virtual string IntlImage => "flag-no";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public NO(IConfiguration configuration) {
        Add(new() {
            Summary = "Søndag",
            Location = Country.ThreeLetterISORegionName,
            Classification = AccessClassification.Private,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Weekly,
                DaysOfWeek = DaysOfWeek.Sunday
            }
        });

        AddFlags flags = configuration.GetValue("Calendar:NO:Flags", DefaultAddFlags);
        if (flags.HasFlag(AddFlags.Yearly)) AddYearly();
        if (flags.HasFlag(AddFlags.Easter)) AddByEaster();
        if (flags.HasFlag(AddFlags.Historic)) AddHistoric();
    }

    void AddYearly() {
        Add(new() {
            Summary = "1. nyttårsdag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday }, // NOTE: should be 'Holiday' [1927..2004] (FOR-2004-12-03)
            StartDate = new(1, 1, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Prinsesse Ingrid Alexandra", // b.d. 2004-01-21
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(2005, 1, 21), // FOR-2004-12-03
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Samefolkets dag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(2004, 2, 6), // FOR-2003-12-05
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Kong Harald V", // b.d. 1937-02-21
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1938, 2, 21), // FOR-1937-12-03
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Offentlig høytidsdag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1935, 5, 1), // FOR-1935-04-26
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Frigjøringsdagen 1945",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1962, 5, 8), // FOR-1962-04-27
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Grunnlovsdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday },
            StartDate = new(1815, 5, 17),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Unionsoppløsningen 1905",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1906, 6, 7),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Dronning Sonja", // b.d. 1937-07-04
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1969, 7, 4), // FOR-1968-11-08
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Kronprins Haakon Magnus", // b.d. 1973-07-20
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1974, 7, 20), // FOR-1973-08-10
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Olsokdagen",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1928, 7, 29), // FOR-1928-03-24
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Kronprinsesse Mette-Marit", // b.d. 1973-08-19
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(2002, 8, 19), // FOR-2001-12-07
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            // NOTE: this event may have many exceptions
            Summary = "Stortingsvalg", // 2021-09-13 hence 2021 % 1937 = 84
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1937, 9, 1), // FOR-1936-10-23
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                Interval = 4,
                WeekOfMonth = WeekOfMonth.Second,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "1. juledag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday }, // NOTE: should be 'Holiday' [1927..2003] (FOR-2004-12-03)
            StartDate = new(1, 12, 25),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "2. juledag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 12, 26),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    void AddByEaster() {
        Add(new() {
            Summary = "Palmesøndag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = -7
            }
        });
        Add(new() {
            Summary = "Skjærtorsdag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = -3
            }
        });
        Add(new() {
            Summary = "Langfredag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = -2
            }
        });
        Add(new() {
            Summary = "1. påskedag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday }, // NOTE: Should be 'Holiday' [1927..2003] (FOR-2004-12-03)
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 0
            }
        });
        Add(new() {
            Summary = "2. påskedag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        Add(new() {
            Summary = "Kristi himmelfartsdag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 39
            }
        });
        Add(new() {
            Summary = "1. pinsedag",
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay, Category.Holiday }, // NOTE: should be 'Holiday' [1927..2003] (FOR-2004-12-03)
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 49
            }
        });
        Add(new() {
            Summary = "2. pinsedag",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 50
            }
        });
    }

    ///
    public void AddHistoric() {
        AddHistoric1927();
        AddHistoric1928();
        AddHistoric1931();
        AddHistoric1932();
        AddHistoric1971();
    }

    ///
    public void AddHistoric1927() {
        Add(new() {
            Summary = "Dronning Maud", // b.d. 1869-11-26
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1927, 11, 26), // FOR-1927-10-21
            EndDate = new(1938, 11, 20), // N/A
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Kong Haakon VII", // b.d. 1872-08-03
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1928, 8, 3), // FOR-1927-10-21
            EndDate = new(2004, 12, 3), // FOR-2004-12-03
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
        Add(new() {
            Summary = "Kong Olav V", // b.d. 1903-07-02
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1928, 7, 2), // FOR-1927-10-21
            EndDate = new(2004, 12, 3), // FOR-2004-12-03
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    ///
    public void AddHistoric1928() {
        Add(new() {
            Summary = "Kronprinsesse Märtha", // b.d. 1901-03-28
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1928, 3, 28), // FOR-1928-3-24
            EndDate = new(2004, 12, 3), // defacto FOR-2004-12-03
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    ///
    public void AddHistoric1931() {
        Add(new() {
            Summary = "Prinsesse Ragnhild Alexandra", // b.d. 1930-06-09
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1931, 6, 9), // FOR-1931-02-06
            EndDate = new(1951, 9, 11), // FOR-1951-09-11
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    ///
    public void AddHistoric1932() {
        Add(new() {
            Summary = "Prinsesse Astrid, fru Ferner", // b.d. 1932-02-12
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1933, 2, 12), // FOR-1932-12-16
            EndDate = new(1961, 1, 27), // FOR-1961-01-27
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }

    ///
    public void AddHistoric1971() {
        Add(new() {
            Summary = "Prinsesse Märtha Louise", // b.d. 1971-09-22
            Location = Country.ThreeLetterISORegionName,
            Attachment = { IntlImage },
            Categories = { Category.FlagDay },
            StartDate = new(1972, 9, 22), // FOR-1971-10-22
            EndDate = new(2004, 12, 3), // FOR-2004-12-03
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
