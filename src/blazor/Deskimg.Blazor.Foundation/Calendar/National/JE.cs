// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Jersey (Great Britain Crown dependency).
/// </summary>
[Export<ISubsetCalendar>(nameof(JE))]
public class JE : GB {

    /// <inheritdoc/>
    protected override Iso3166 Country => Iso3166.JE;

    /// <inheritdoc/>
    protected override string IntlImage => "flag-je";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public JE() {
        Add(new() {
            Summary = "Easter Monday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        Add(new() {
            Summary = "Liberation Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 5, 9),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
