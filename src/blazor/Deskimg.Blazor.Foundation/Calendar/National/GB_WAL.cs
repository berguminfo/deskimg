// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Wales (Great Britain Constituent country).
/// </summary>
[Export<ISubsetCalendar>(nameof(GB_WAL))]
public class GB_WAL : GB_ENG { }
