// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for England (Great Britain Constituent country).
/// </summary>
[Export<ISubsetCalendar>(nameof(GB_ENG))]
public class GB_ENG : GB {

    /// <inheritdoc/>
    protected override string IntlImage => "flag-eng";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public GB_ENG() {
        Add(new() {
            Summary = "Easter Monday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Easter,
                Interval = 1
            }
        });
        Add(new() {
            Summary = "Summer Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 8, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.Last,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
    }
}
