// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.National;

/// <summary>
/// Represents the holidays calendar for Scotland (Great Britain Constituent country).
/// </summary>
[Export<ISubsetCalendar>(nameof(GB_SCT))]
public class GB_SCT : GB {

    /// <inheritdoc/>
    protected override string IntlImage => "flag-sct";

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public GB_SCT() {
        Add(new() {
            Summary = "New Year's Day", // TODO: may not occur on Saturday or Sunday
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 1, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                DaysOfWeek = DaysOfWeek.WesternWorkweek
            }
        });
        Add(new() {
            Summary = "2nd January", // TODO: may not occur on Saturday or Sunday
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 1, 2),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                DaysOfWeek = DaysOfWeek.WesternWorkweek
            }
        });
        Add(new() {
            Summary = "Summer Bank Holiday",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 8, 1),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly,
                WeekOfMonth = WeekOfMonth.First,
                DaysOfWeek = DaysOfWeek.Monday
            }
        });
        Add(new() {
            Summary = "St. Andrew's Day",
            Location = Country.ThreeLetterISORegionName,
            Categories = { Category.Holiday },
            StartDate = new(1, 11, 30),
            RecurrenceRule = new() {
                Recurrence = VEventRecurrence.Yearly
            }
        });
    }
}
