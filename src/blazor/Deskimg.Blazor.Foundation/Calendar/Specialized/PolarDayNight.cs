// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using System.Runtime.Versioning;

namespace Deskimg.Calendar.Specialized;

using Deskimg.Ephemeris;

/// <summary>
/// Represents the Polar day/night events calendar.
/// </summary>
[Export<ISubsetCalendar2>(nameof(PolarDayNight))]
public class PolarDayNight(
    IJSRuntime js) : ISubsetCalendar2 {

    /// <inheritdoc/>
    public async Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        if (OperatingSystem.IsBrowser() && observer is not null) {
            var result = Subset(rangeStart, rangeLength, observer, cancellationToken);
            return await result.ToListAsync(cancellationToken);
        } else {
            return [];
        }

    }

    [SupportedOSPlatform("browser")]
    async IAsyncEnumerable<SubsetResult> Subset(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        [EnumeratorCancellation] CancellationToken cancellationToken) {

        if (observer.Coordinate is not null) {
            Earth earth = new(js);
            int previousState = 0;
            DateTimeOffset rangeEnd = rangeStart + rangeLength;
            for (int index = -1; index < 366; ++index) {
                DateTimeOffset when = rangeStart.AddDays(index);

                // break iteration then going outside requested interval
                if (when > rangeEnd) {
                    yield break;
                }

                // find current event state
                var utc = new DateTimeOffset(when.DateTime, TimeSpan.Zero);
                RiseSet riseSet = await earth.GetRiseSetAsync<Solar>(utc, observer.Coordinate, cancellationToken);
                int state = GetState(aboveHorizon: riseSet.AboveHorizon, belowHorizon: riseSet.BelowHorizon);
                int transition = GetTransition(previousState, state);

                // found a transition
                if (s_symbolMapping.TryGetValue(transition, out string? symbol)) {
                    yield return new(
                        when,
                        new VEvent {
                            Attachment = { symbol },
                            Categories = { Category.Observance }
                        });
                }

                // try next state
                previousState = state;
            }
        }
    }

    static int GetState(bool aboveHorizon = false, bool belowHorizon = false) =>
        Convert.ToInt32(aboveHorizon) << 1 | Convert.ToInt32(belowHorizon);

    static int GetTransition(int fromState, int toState) =>
        fromState << 8 | toState;

    static readonly Dictionary<int, string> s_symbolMapping = new() {
        // polar day start
        [GetTransition(GetState(), GetState(aboveHorizon: true))] = "polar-day-begin",
        // polar day end
        [GetTransition(GetState(aboveHorizon: true), GetState())] = "polar-day-end",
        // polar night start
        [GetTransition(GetState(), GetState(belowHorizon: true))] = "polar-night-begin",
        // polar night end
        [GetTransition(GetState(belowHorizon: true), GetState())] = "polar-night-end"
    };
}
