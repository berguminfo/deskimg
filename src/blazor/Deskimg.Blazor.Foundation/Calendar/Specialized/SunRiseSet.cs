// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.Specialized;

using Deskimg.Ephemeris;

/// <summary>
/// Represents the <see cref="Solar"/> rise/set calendar.
/// </summary>
[Export<ISubsetCalendar2>(nameof(SunRiseSet))]
public class SunRiseSet(IJSRuntime js) : RiseSetCalendar<Solar>(js) { }
