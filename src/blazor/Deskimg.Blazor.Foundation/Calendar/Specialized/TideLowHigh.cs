// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Net.Http.Json;
using System.Text.Json;

namespace Deskimg.Calendar.Specialized;

/// <summary>
/// Represents the kartverket.no tide forecast <see cref="ISubsetCalendar"/> events calendar.
/// </summary>
[Export<ISubsetCalendar2>(nameof(TideLowHigh))]
public class TideLowHigh(
    ILogger<TideLowHigh> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : ISubsetCalendar2, ICanCacheControl {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public KeyValuePair<string, string> Tag { get; } = new("tideforecast.kartverket.no", string.Empty);

    /// <inheritdoc/>
    public async Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        IList<SubsetResult>? result = null;
        if (observer?.Coordinate is not null) {
            var startDate = rangeStart.ToUniversalTime().Date;
            var endDate = (rangeStart + rangeLength).ToUniversalTime().Date;
            Uri? requestUri = endpoint.Fetch(Tag.Key, new {
                Latitude = observer.Coordinate.Latitude,
                Longitude = observer.Coordinate.Longitude,
                Start = startDate.ToString("s"),
                End = endDate.ToString("s")
            }, discardQuery: true);
            if (requestUri is not null) {
                try {
                    result = await cache.GetOrCreateAsync(requestUri, async entry => {
                        entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                        logger.FetchStarting(requestUri, CacheControl);
                        var response = await http.GetFromJsonAsync<Schema.GetKvTideForecastResponse?>(
                            requestUri,
                            CacheControl,
                            s_sourceGenOptions,
                            cancellationToken);

                        return Subset(rangeStart, rangeLength, response).ToList();
                    });
                } catch (OperationCanceledException) {
                } catch (HttpRequestException ex) {
                    logger.FetchFailed(ex, requestUri, ex.StatusCode);
                } catch (JsonException ex) {
                    logger.DeserializeJsonFailed(ex, requestUri, ex.LineNumber, ex.BytePositionInLine);
                }
            }
        }

        return result ?? [];
    }

    static IEnumerable<SubsetResult> Subset(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Schema.GetKvTideForecastResponse? response) {

        if (response?.Result?.Forecasts is null) {
            yield break;
        }

        var forecasts =
            from item in response.Result.Forecasts
            where item.DateTime.Contains(rangeStart, rangeLength)
            // NOTE: expect the result to be in order
            select item;
        foreach (var item in forecasts) {
            yield return new(
                item.DateTime,
                new VEvent {
                    Summary = item.DateTime.ToString("t", CultureInfo.CurrentCulture),
                    Attachment = { $"tide-{item.Status}".ToLowerInvariant() },
                    Categories = { Category.Observance, typeof(TideLowHigh) }
                }); ;
        }
    }
}
