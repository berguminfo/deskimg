// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see href="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/tidelevels.kartverket.no.md"/>

using System.Text.Json.Serialization;

namespace Deskimg.Calendar.Specialized.Schema;

record GetKvTideForecastResponse(KvResultObject Result);

record KvResultObject(IImmutableList<KvForecastObject>? Forecasts);

[JsonConverter(typeof(JsonStringEnumConverter<KvForecastStatus>))]
enum KvForecastStatus {
    Low,
    High,
}

record KvForecastObject(KvForecastStatus Status, KvMeasurementObject Measurement, DateTimeOffset DateTime);

record KvMeasurementObject(double Value, string Unit);
