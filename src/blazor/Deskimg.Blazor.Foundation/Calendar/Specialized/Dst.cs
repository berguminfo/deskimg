// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using NodaTime;

namespace Deskimg.Calendar.Specialized;

/// <summary>
/// Represents the Daylight Saving Time (DST) change events calendar.
/// </summary>
[Export<ISubsetCalendar>(nameof(Dst))]
public class Dst : ISubsetCalendar {

    /// <inheritdoc/>
    public Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default) => Task.FromResult(Subset(rangeStart, rangeLength));

    static IEnumerable<SubsetResult> Subset(DateTimeOffset rangeStart, TimeSpan rangeLength) {
        if (TimeZoneInfo.Local.SupportsDaylightSavingTime) {
            Interval interval = new(
                Instant.FromDateTimeOffset(rangeStart),
                Instant.FromDateTimeOffset(rangeStart + rangeLength)
            );

            var timeZone = DateTimeZoneProviders.Bcl.GetSystemDefault();

            foreach (var zoneInterval in timeZone.GetZoneIntervals(interval)) {
                if (zoneInterval.HasStart) {
                    if (interval.Contains(zoneInterval.Start)) {
                        yield return new(
                            zoneInterval.Start.ToDateTimeOffset(),
                            new VEvent {
                                Summary = zoneInterval.Name,
                                Categories = { Category.ClockChange }
                            }
                        );
                    }
                }
            }
        }
    }
}
