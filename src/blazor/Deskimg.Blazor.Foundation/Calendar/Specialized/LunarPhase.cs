// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar.Specialized;

using Deskimg.Ephemeris;

/// <summary>
/// Represents the Lunar Phases <see cref="LunarPhase"/> events calendar.
/// </summary>
[Export<ISubsetCalendar>(nameof(LunarPhase))]
public class LunarPhase(IJSRuntime js) : ISubsetCalendar {

    /// <inheritdoc/>
    public async Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default) {

        if (!OperatingSystem.IsBrowser()) {
            return [];
        }

        Earth earth = new(js);
        Dictionary<DateTimeOffset, SubsetResult> result = [];

        // the intermediate phases last one-quarter of a synodic month, or 7.38 days
        // hence in order to catch all events, go back 30 days but go forward max 29 days
        DateTimeOffset rangeEnd = rangeStart + rangeLength;
        for (DateTimeOffset when = rangeStart.AddDays(-30); when < rangeEnd; when = when.AddDays(29)) {
            var iter = earth.GetUpcomingLunarPhaseAsync<Lunar>(when, cancellationToken);

            // do add each distinct event without LINQ; saves 3+ iterations of the same collection
            foreach (var item in await iter.ToListAsync(cancellationToken)) {
                if (!result.ContainsKey(item.When)) {
                    result[item.When] = new(
                        item.When,
                        new VEvent {
                            Attachment = { s_symbolMapping[item.Phase] },
                            Categories = { Category.Observance }
                        });
                }
            }
        }

        return result.Values;
    }

    static readonly Dictionary<LunarPhaseM, string> s_symbolMapping = new() {
        [LunarPhaseM.New] = "moon-new",
        [LunarPhaseM.FirstQuarter] = "moon-first-quarter",
        [LunarPhaseM.Full] = "moon-full",
        [LunarPhaseM.LastQuarter] = "moon-last-quarter"
    };
}
