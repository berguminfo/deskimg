// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Xml;

namespace Deskimg.Calendar.Specialized;

using Ical = Ical.Net;

/// <summary>
/// Represents the RFC-5545 (iCalendar) events calendar.
/// </summary>
[Export<ISubsetCalendar>(nameof(CalDAV))]
public class CalDAV(
    ILogger<CalDAV> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : VEventsCalendar, ICanCacheControl, ICanSetTag {

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public override async Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default) {

        if ((Tag.Key ?? string.Empty) == nameof(CalDAV) && Tag.Value is not null) {
            Uri? requestUri = null;
            if (Uri.TryCreate(Tag.Value.ToString(), UriKind.Absolute, out requestUri)) {

                // it's a absolute URI, use fetch.api
                requestUri = endpoint.Fetch(requestUri.AbsoluteUri);
            } else
            if (Tag.Value.ToString() is string fileName) {

                // otherwise it's a CalDAV reference
                requestUri = endpoint.CalDAV(fileName);
            }

            if (requestUri is not null) {
                await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    // remove any previous assignments
                    Clear();

                    if (requestUri.LocalPath.EndsWith('/')) {
                        await PropFindAsync(requestUri, cancellationToken);
                    } else {
                        await GetAsync(requestUri, cancellationToken);
                    }

                    return true;
                });
            }
        }

        // create the subset
        return Subset(rangeStart, rangeLength);
    }

    async Task PropFindAsync(Uri requestUri, CancellationToken cancellationToken) {
        try {
            logger.FetchStarting(requestUri, CacheControl);
            var response = await http.PropFindAsync(
                requestUri,
                CacheControl,
                cancellationToken);

            foreach (var item in response) {
                await GetAsync(item.Source, cancellationToken);
            }
        } catch (OperationCanceledException) {
        } catch (HttpRequestException ex) {
            logger.FetchFailed(ex, requestUri, ex.StatusCode);
        } catch (XmlException ex) {
            logger.DeserializeXmlFailed(ex, requestUri, ex.LineNumber, ex.LinePosition);
        }
    }

    async Task GetAsync(Uri requestUri, CancellationToken cancellationToken) {
        try {
            logger.FetchStarting(requestUri, CacheControl);
            using Stream stream = await http.GetStreamAsync(
                requestUri,
                CacheControl,
                cancellationToken);

            var collection = Ical.CalendarCollection.Load(stream);
            AddEvents(collection);
        } catch (OperationCanceledException) {
        } catch (HttpRequestException ex) {
            logger.FetchFailed(ex, requestUri, ex.StatusCode);
        }
    }

    void AddEvents(Ical.CalendarCollection icalCollection) {
        foreach (var icalCalendar in icalCollection) {
            foreach (var icalEvent in icalCalendar.Events) {
                var _calendarEventStart = icalEvent.Start.AsDateTimeOffset;
                if (!icalEvent.Start.HasDate) {

                    // all VEvents is required to have at least a start date
                    continue;
                }

                VEvent vevent = new() {
                    Summary = icalEvent.Summary,
                    Description = icalEvent.Description,
                    Start = icalEvent.DtStart.AsDateTimeOffset,
                    End = icalEvent.DtEnd.AsDateTimeOffset,
                    Duration = icalEvent.Duration
                };
                if (icalEvent.RecurrenceRules.Count > 0) {
                    var recurrenceRule = icalEvent.RecurrenceRules[0];
                    RecurrenceRule rule = new() {
                        Interval = recurrenceRule.Interval
                    };

                    switch (recurrenceRule.Frequency) {
                        case Ical.FrequencyType.Daily:
                            rule.Recurrence = VEventRecurrence.Daily;
                            rule.Interval = recurrenceRule.Interval;
                            break;

                        case Ical.FrequencyType.Weekly:
                            rule.Recurrence = VEventRecurrence.Weekly;
                            int daysOfWeek = 0;
                            for (var i = 0; i < recurrenceRule.ByDay.Count; ++i) {
                                daysOfWeek |= 1 << (int)recurrenceRule.ByDay[i].DayOfWeek;
                            }
                            rule.DaysOfWeek = (DaysOfWeek)daysOfWeek;
                            break;

                        case Ical.FrequencyType.Monthly:
                            /* TODO:
                            foreach (var byMonthDay in recurrenceRule.ByMonthDay) {
                                vevent.RecurrenceRule.Day = byMonthDay;
                            }
                            vevent.RecurrenceRule.Unit = vevent.RecurrenceRule.Day > 0 ?
                                RecurrenceUnit.MonthlyOnDay :
                                RecurrenceUnit.Monthly;
                            */
                            rule.Recurrence = VEventRecurrence.Monthly;
                            break;

                        case Ical.FrequencyType.Yearly:
                            rule.Recurrence = VEventRecurrence.Yearly;
                            // TODO: vevent.RecurrenceRule.Month = vevent.__TODO__StartTime.Value.Month;
                            // TODO: vevent.RecurrenceRule.Day = vevent.__TODO__StartTime.Value.Day;
                            break;

                        default:
                            throw new InvalidOperationException();
                    }

                    vevent.RecurrenceRule = rule;
                }

                Add(vevent);
            }
        }
    }
}
