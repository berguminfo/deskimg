// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Runtime.CompilerServices;

namespace Deskimg.Calendar.Specialized;

using Deskimg.Ephemeris;

/// <summary>
/// Represents the rise/set calendar.
/// </summary>
public class RiseSetCalendar<TCelestialBody>(
    IJSRuntime js) : ISubsetCalendar2 where TCelestialBody : ICelestialBody {

    /// <inheritdoc/>
    public async Task<IEnumerable<SubsetResult>> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        if (observer?.Coordinate is not null) {
            var result = Subset(rangeStart, rangeLength, observer.Coordinate, cancellationToken);
            return await result.ToListAsync(cancellationToken);
        } else {
            return [];
        }
    }

    async IAsyncEnumerable<SubsetResult> Subset(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geocoordinate observer,
        [EnumeratorCancellation] CancellationToken cancellationToken) {

        Earth earth = new(js);
        DateTime end = (rangeStart + rangeLength).DateTime;
        for (DateTime when = rangeStart.DateTime; when < end; when = when.AddDays(1)) {
            var utc = new DateTimeOffset(when, TimeSpan.Zero);
            RiseSet riseSet = await earth.GetRiseSetAsync<TCelestialBody>(utc, observer, cancellationToken);
            HashSet<object> categories = [
                Category.Observance,
                typeof(TCelestialBody)
            ];

            // add altitude graph:
            // * start at -12 hours
            // * step forward one hour
            // * until reaching one julian day (24 steps)
            var altitude = await earth.GetApparentTopocentricHorizontalCoordinatesAsync<TCelestialBody>(
                when.AddHours(-12), observer, 1d / 24d, 1d, cancellationToken);
            if (altitude is not null) {
                categories.Add(altitude);
            }

            // find current event state
            if (riseSet.AboveHorizon) {
                yield return new(
                    when,
                    new VEvent {
                        Attachment = { s_symbolMapping[typeof(TCelestialBody)][Symbol.AboveHorizon] },
                        Categories = categories
                    });
            } else
            if (riseSet.BelowHorizon) {
                yield return new(
                    when,
                    new VEvent {
                        Attachment = { s_symbolMapping[typeof(TCelestialBody)][Symbol.BelowHorizon] },
                        Categories = categories
                    });
            } else {
                if (riseSet.Rise != null) {
                    yield return new(
                        riseSet.Rise.Value,
                        new VEvent {
                            Summary = riseSet.Rise?.ToString("t", CultureInfo.InvariantCulture) ?? string.Empty,
                            Attachment = { s_symbolMapping[typeof(TCelestialBody)][Symbol.Rise] },
                            Categories = categories
                        });
                }
                if (riseSet.Set != null) {
                    yield return new(
                        riseSet.Set.Value,
                        new VEvent {
                            Summary = riseSet.Set?.ToString("t", CultureInfo.InvariantCulture) ?? string.Empty,
                            Attachment = { s_symbolMapping[typeof(TCelestialBody)][Symbol.Set] },
                            Categories = categories
                        });
                }
            }
        }
    }

    enum Symbol {
        Rise,
        Set,
        AboveHorizon,
        BelowHorizon
    }

    static readonly Dictionary<Type, Dictionary<Symbol, string>> s_symbolMapping = new() {
        [typeof(Solar)] = new() {
            [Symbol.Rise] = "sun-rise",
            [Symbol.Set] = "sun-set",
            [Symbol.AboveHorizon] = "sun-above-horizon",
            [Symbol.BelowHorizon] = "sun-below-horizon",
        },
        [typeof(Lunar)] = new() {
            [Symbol.Rise] = "moon-rise",
            [Symbol.Set] = "moon-set",
            [Symbol.AboveHorizon] = "moon-above-horizon",
            [Symbol.BelowHorizon] = "moon-below-horizon",
        },
    };
}
