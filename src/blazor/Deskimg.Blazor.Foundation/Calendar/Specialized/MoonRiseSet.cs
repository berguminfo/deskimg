// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Deskimg.Ephemeris;

namespace Deskimg.Calendar.Specialized;

/// <summary>
/// Represents the <see cref="Lunar"/> rise/set calendar.
/// </summary>
[Export<ISubsetCalendar2>(nameof(MoonRiseSet))]
public class MoonRiseSet(IJSRuntime js) : RiseSetCalendar<Lunar>(js) { }
