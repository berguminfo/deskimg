// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Deskimg.Calendar;

using GregorianCalendar = System.Globalization.GregorianCalendar;

/// <inheritdoc cref="IVEvent"/>
public class VEvent : IVEvent {

    /// <summary>
    /// This property defines the calendar scale used for the
    /// calendar information specified in the iCalendar object.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.7.1">3.7.1.  CALSCALE</see>
    /// <example>CALSCALE:GREGORIAN</example>
    [JsonPropertyName("CALSCALE")]
    public Type CalendarScale { get; set; } = typeof(GregorianCalendar);

    /// <summary>
    /// This property defines the iCalendar object method
    /// associated with the calendar object.
    /// </summary>
    /// <remarks>
    /// When used in a MIME message entity, the value of this
    /// property MUST be the same as the Content-Type "method" parameter
    /// value. If either the "METHOD" property or the Content-Type
    /// "method" parameter is specified, then the other MUST also be
    /// specified.
    /// </remarks>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.7.2">3.7.2.  METHOD</see>
    /// <example>METHOD:REQUEST</example>
    [JsonPropertyName("METHOD")]
    public string? Method { get; set; }

    /// <summary>
    /// This property specifies the identifier for the product that
    /// created the iCalendar object.
    /// </summary>
    /// <remarks>
    /// The property MUST be specified once in an iCalendar object.
    /// </remarks>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.7.3">3.7.3.  PRODID</see>
    /// <example>PRODID:-//ABC Corporation//NONSGML My Product//EN</example>
    [JsonPropertyName("PRODID")]
    public string ProductIdentifier { get; set; } = "-//BERGUM INFO//DESKIMG//NO";

    /// <summary>
    /// This property specifies the identifier corresponding to the
    /// highest version number or the minimum and maximum range of the
    /// iCalendar specification that is required in order to interpret the
    /// iCalendar object.
    /// </summary>
    /// <remarks>
    /// This property MUST be specified once in an iCalendar object.
    /// </remarks>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.7.4">3.7.4.  VERSION</see>
    /// <example>VERSION:2.0</example>
    [JsonPropertyName("VERSION")]
    public string Version { get; set; } = "2.0";

    /// <summary>
    /// This property provides the capability to associate a
    /// document object with a calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.1">3.8.1.1.  ATTACH</see>
    /// <example>ATTACH;FMTTYPE=application/postscript:ftp://example.com/pub/reports/r-960812.ps</example>
    [JsonPropertyName("ATTACH")]
    public HashSet<string> Attachment { get; init; } = [];

    /// <summary>
    /// This property defines the categories for a calendar
    /// component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.2">3.8.1.2.  CATEGORIES</see>
    /// <example>CATEGORIES:APPOINTMENT,EDUCATION</example>
    /// <example>CATEGORIES:MEETING</example>
    [JsonPropertyName("CATEGORIES")]
    public HashSet<object> Categories { get; init; } = [];

    /// <summary>
    /// This property defines the access classification for a
    /// calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.3">3.8.1.3.  CLASS</see>
    /// <example>CLASS:PUBLIC</example>
    [JsonPropertyName("CLASS")]
    public AccessClassification Classification { get; set; } = AccessClassification.Public;

    /// <summary>
    /// This property specifies non-processing information intended
    /// to provide a comment to the calendar user.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.4">3.8.1.4.  COMMENT</see>
    /// <example>COMMENT:The meeting really needs to include both ourselves and the customer.</example>
    [JsonPropertyName("COMMENT")]
    public string? Comment { get; set; }

    /// <summary>
    /// This property provides a more complete description of the
    /// calendar component than that provided by the "SUMMARY" property.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.5">3.8.1.5.  DESCRIPTION</see>
    /// <example>DESCRIPTION:Meeting to provide technical review for "Phoenix" design.</example>
    [JsonPropertyName("DESCRIPTION")]
    public string? Description { get; set; }

    /// <summary>
    /// This property specifies information related to the global
    /// position for the activity specified by a calendar component.
    /// </summary>
    /// <remarks>
    /// The value MUST be two SEMICOLON-separated FLOAT values.
    /// </remarks>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.6">3.8.1.6.  GEO</see>
    /// <example>GEO:37.386013;-122.082932</example>
    [JsonPropertyName("GEO")]
    public Geoposition? GeographicPosition { get; set; }

    /// <summary>
    /// This property defines the intended venue for the activity
    /// defined by a calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.7">3.8.1.7.  LOCATION</see>
    /// <example>LOCATION:Conference Room - F123\, Bldg. 002</example>
    [JsonPropertyName("LOCATION")]
    public string? Location { get; set; }

    /// <summary>
    /// This property defines the relative priority for a calendar
    /// component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.9">3.8.1.9.  PRIORITY</see>
    /// <example>PRIORITY:1</example>
    [Range(1, 9)]
    [JsonPropertyName("PRIORITY")]
    public ushort Priority { get; set; } = 5;

    /// <summary>
    /// This property defines the equipment or resources
    /// anticipated for an activity specified by a calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.10">3.8.1.10.  RESOURCES</see>
    /// <example>RESOURCES:EASEL,PROJECTOR,VCR</example>
    [JsonPropertyName("RESOURCES")]
    public IEnumerable<string>? Resources { get; set; }

    /// <summary>
    /// This property defines the overall status or confirmation
    /// for the calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.11">3.8.1.11.  STATUS</see>
    /// <example>STATUS:TENTATIVE</example>
    [JsonPropertyName("STATUS")]
    public ushort Status { get; set; }

    /// <summary>
    /// This property defines a short summary or subject for the
    /// calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.1.12">3.8.1.12.  SUMMARY</see>
    /// <example>SUMMARY:Department Party</example>
    [JsonPropertyName("SUMMARY")]
    public string Summary { get; set; } = string.Empty;

    /// <summary>
    /// This property specifies the date and time that a calendar
    /// component ends.
    /// </summary>
    /// <remarks>
    /// The value type of this property MUST be the same as the "DTSTART" property, and
    /// its value MUST be later in time than the value of the "DTSTART"
    /// property.Furthermore, this property MUST be specified as a date
    /// with local time if and only if the "DTSTART" property is also
    /// specified as a date with local time.
    /// </remarks>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.2.2">3.8.2.2.  DTEND</see>
    /// <example>DTEND:19960401T150000Z</example>
    [JsonPropertyName("DTEND")]
    public DateTimeOffset? End { get; set; }

    /// <inheritdoc cref="End"/>
    [JsonIgnore]
    public DateOnly? EndDate { get; set; }

    /// <inheritdoc cref="End"/>
    [JsonIgnore]
    public TimeOnly? EndTime { get; set; }

    /// <summary>
    /// This property specifies when the calendar component begins.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.2.4">3.8.2.4.  DTSTART</see>
    /// <example>DTSTART:19980118T073000Z</example>
    [JsonPropertyName("DTSTART")]
    public DateTimeOffset? Start { get; set; }

    /// <see cref="Start"/>
    [JsonIgnore]
    public DateOnly? StartDate { get; set; }

    /// <see cref="Start"/>
    [JsonIgnore]
    public TimeOnly? StartTime { get; set; }

    /// <see cref="Start"/>
    [JsonIgnore]
    public bool AllDay { get; set; }

    /// <summary>
    /// This property specifies a positive duration of time.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.2.5">3.8.2.5.  DURATION</see>
    /// <example>DURATION:PT1H0M0S</example>
    [JsonPropertyName("DURATION")]
    public TimeSpan? Duration { get; set; }

    /// <summary>
    /// This property defines whether or not an event is
    /// transparent to busy time searches.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.2.7">3.8.2.7.  TRANSP</see>
    /// <example>TRANSP:TRANSPARENT</example>
    [JsonPropertyName("TRANSP")]
    public bool Transparency { get; set; }

    /// <summary>
    /// This property is used to represent contact information or
    /// alternately a reference to contact information associated with the
    /// calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.2">3.8.4.2.  CONTACT</see>
    /// <example>CONTACT:Jim Dolittle\, ABC Industries\, +1-919-555-1234</example>
    [JsonPropertyName("CONTACT")]
    public string? Contact { get; set; }

    /// <summary>
    /// This property is used in conjunction with the "UID" and
    /// "SEQUENCE" properties to identify a specific instance of a
    /// recurring "VEVENT", "VTODO", or "VJOURNAL" calendar component.
    /// The property value is the original value of the "DTSTART" property
    /// of the recurrence instance.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.4">3.8.4.4.  RECURRENCE-ID</see>
    /// <example>RECURRENCE-ID;VALUE=DATE:19960401</example>
    [JsonPropertyName("RECURRENCE-ID")]
    public object? RecurrenceId { get; set; }

    /// <summary>
    /// This property is used to represent a relationship or
    /// reference between one calendar component and another.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.5">3.8.4.5.  RELATED-TO</see>
    /// <example>RELATED-TO:jsmith.part7.19960817T083000.xyzMail@example.com</example>
    [JsonPropertyName("RELATED-TO")]
    public string? RelatedTo { get; set; }

    /// <summary>
    /// This property defines a Uniform Resource Locator (URL)
    /// associated with the iCalendar object.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.6">3.8.4.6.  URL</see>
    /// <example>URL:http://example.com/pub/calendars/jsmith/mytime.ics</example>
    [JsonPropertyName("URL")]
    public Uri? Url { get; set; }

    /// <summary>
    /// This property defines the persistent, globally unique
    /// identifier for the calendar component.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.4.7">3.8.4.7.  URL</see>
    /// <remarks>
    /// The property MUST be specified in the "VEVENT",
    /// "VTODO", "VJOURNAL", or "VFREEBUSY" calendar components.
    /// </remarks>
    /// <example>UID:19960401T080045Z-4000F192713-0052@example.com</example>
    [JsonPropertyName("UID")]
    public string UniqueId { get; set; } = Guid.NewGuid().ToString("n");

    /// <summary>
    /// This property defines the list of DATE-TIME exceptions for
    /// recurring events, to-dos, journal entries, or time zone
    /// definitions.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.5.1">3.8.5.1.  EXDATE</see>
    /// <example>EXDATE:19960402T010000Z,19960403T010000Z,19960404T010000Z</example>
    [JsonPropertyName("EXDATE")]
    public IEnumerable<DateTimeOffset>? ExceptionDateTimes { get; set; }

    /// <summary>
    /// This property defines the list of DATE-TIME values for
    /// recurring events, to-dos, journal entries, or time zone
    /// definitions.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.5.2">3.8.5.2.  RDATE</see>
    /// <example>RDATE:19970714T123000Z</example>
    [JsonPropertyName("RDATE")]
    public IEnumerable<DateTimeOffset>? RecurrenceDateTimes { get; set; }

    /// <summary>
    /// This property defines a rule or repeating pattern for
    /// recurring events, to-dos, journal entries, or time zone
    /// definitions.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.5.3">3.8.5.3.  RRULE</see>
    /// <example>RRULE:FREQ=YEARLY;INTERVAL=2;COUNT=10;BYMONTH=1,2,3</example>
    [JsonPropertyName("RRULE")]
    public RecurrenceRule RecurrenceRule { get; set; }

    /// <summary>
    /// This property specifies the date and time that the calendar
    /// information was created by the calendar user agent in the calendar
    /// store.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.7.1">3.8.7.1.  CREATED</see>
    /// <remarks>
    /// The value MUST be specified in the UTC time format.
    /// </remarks>
    /// <example>CREATED:19960329T133000Z</example>
    [JsonPropertyName("CREATED")]
    public DateTimeOffset? Created { get; set; }

    /// <summary>
    /// In the case of an iCalendar object that specifies a
    /// "METHOD" property, this property specifies the date and time that
    /// the instance of the iCalendar object was created. In the case of
    /// an iCalendar object that doesn't specify a "METHOD" property, this
    /// property specifies the date and time that the information
    /// associated with the calendar component was last revised in the
    /// calendar store.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.7.2">3.8.7.2.  DTSTAMP</see>
    /// <remarks>
    /// The value MUST be specified in the UTC time format.
    /// </remarks>
    /// <example>DTSTAMP:19971210T080000Z</example>
    [JsonPropertyName("DTSTAMP")]
    public DateTimeOffset TimeStamp { get; set; } = DateTimeOffset.UtcNow;

    /// <summary>
    /// This property specifies the date and time that the
    /// information associated with the calendar component was last
    /// revised in the calendar store.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.7.3">3.8.7.3.  LAST-MODIFIED</see>
    /// <remarks>
    /// The property value MUST be specified in the UTC time format.
    /// </remarks>
    /// <example>LAST-MODIFIED:19960817T133000Z</example>
    [JsonPropertyName("LAST-MODIFIED")]
    public DateTimeOffset? LastModified { get; set; }

    /// <summary>
    /// This property defines the revision sequence number of the
    /// calendar component within a sequence of revisions.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.7.4">3.8.7.4.  SEQUENCE</see>
    /// <example>SEQUENCE:0</example>
    [JsonPropertyName("SEQUENCE")]
    public ulong Sequence { get; set; }

    /// <summary>
    /// This property defines the status code returned for a
    /// scheduling request.
    /// </summary>
    /// <see href="https://www.rfc-editor.org/rfc/rfc5545#section-3.8.8.3">3.8.8.3.  REQUEST-STATUS</see>
    /// <example> REQUEST-STATUS:2.0;Success</example>
    [JsonPropertyName("REQUEST-STATUS")]
    public string? RequestStatus { get; set; }
}
