// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

/// <summary>
/// Extension methods for working with <see cref="DateTimeOffset" />.
/// </summary>
/// <remarks>
/// <see cref="DateTimeOffset"/> exceptions for details about errors.
/// </remarks>
public static class DateTimeOffsetExtensions {

    /// <summary>
    /// Gets the number of days per week.
    /// </summary>
    public static int DaysInWeek { get; } = Enum.GetValues<DayOfWeek>().Length;

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> at the specified <paramref name="month"/> and <paramref name="day"/>.
    /// </summary>
    /// <remarks>
    /// In case the month don't contain day exactly, the last day in month is returned.
    /// </remarks>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <param name="month">The month to set.</param>
    /// <param name="day">The day to set.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset Date(this DateTimeOffset dateTime, int month, int day) {
        int daysInMonth = DateTime.DaysInMonth(dateTime.Year, month);
        if (day > daysInMonth) {
            // choose the LastDay strategy in this case
            day = daysInMonth;
        }
        return new DateTimeOffset(new DateTime(dateTime.Year, month, day), dateTime.Offset);
    }

    /// <summary>
    /// Gets the next <see cref="DateTimeOffset"/> given <paramref name="weekOfMonth"/> and <paramref name="daysOfWeek"/>.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <param name="weekOfMonth">The next <see cref="WeekOfMonth"/> to find.</param>
    /// <param name="daysOfWeek">The next <see cref="DaysOfWeek"/> to find.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset Next(this DateTimeOffset dateTime, WeekOfMonth weekOfMonth, DaysOfWeek daysOfWeek) {

        // find next day of week
        DayOfWeek dayOfWeek = dateTime.DayOfWeek;

        // TODO: if daysOfMonth contains multiple days the result will vary depending on witch day set in dateTime, leading to unpredictable results...

        if (daysOfWeek != DaysOfWeek.None) {
            for (var i = 0; i < DaysInWeek; ++i) {
                int tryDay = ((int)dateTime.DayOfWeek + i) % DaysInWeek;
                int flag = 1 << tryDay;
                if (((int)daysOfWeek & flag) == flag) {
                    dayOfWeek = (DayOfWeek)tryDay;
                    break;
                }
            }
        }

        // find day of month
        switch (weekOfMonth) {
            case WeekOfMonth.None:
                return dateTime.Next(dayOfWeek);
            case WeekOfMonth.Last:
                return dateTime.Last(dayOfWeek);
        }

        int weeks = (int)weekOfMonth - 1;
        return dateTime.First(dayOfWeek).AddDays(weeks * DaysInWeek);
    }

    /// <summary>
    /// Gets the next <see cref="DateTimeOffset"/> easter day.
    /// </summary>
    /// <remarks>
    /// Based on anonymous Gregorian algorithm.
    /// </remarks>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset Easter(this DateTimeOffset dateTime) {
        int Y = dateTime.Year;
        int a = Y % 19;
        int b = Y / 100;
        int c = Y % 100;
        int d = b / 4;
        int e = b % 4;
        int f = (b + 8) / 25;
        int g = (b - f + 1) / 3;
        int h = (19 * a + b - d - g + 15) % 30;
        int i = c / 4;
        int k = c % 4;
        int L = (32 + 2 * e + 2 * i - h - k) % 7;
        int m = (a + 11 * h + 22 * L) / 451;
        int month = (h + L - 7 * m + 114) / 31;
        int day = ((h + L - 7 * m + 114) % 31) + 1;
        return new DateTimeOffset(new DateTime(Y, month, day), dateTime.Offset);
    }

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> of the first day in the current month.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    static DateTimeOffset First(this DateTimeOffset dateTime) {
        return dateTime.AddDays(1 - dateTime.Day);
    }

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> of the first <paramref name="dayOfWeek"/> in the current month.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <param name="dayOfWeek">The <see cref="DayOfWeek"/>.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    static DateTimeOffset First(this DateTimeOffset dateTime, DayOfWeek dayOfWeek) {
        DateTimeOffset first = dateTime.First();
        if (first.DayOfWeek != dayOfWeek) {
            first = first.Next(dayOfWeek);
        }

        return first;
    }

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> of the last day in the current month.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    static DateTimeOffset Last(this DateTimeOffset dateTime) {
        int daysInMonth = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
        return dateTime.First().AddDays(daysInMonth - 1);
    }

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> of the last <paramref name="dayOfWeek"/> in the current month.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <param name="dayOfWeek">The <see cref="DayOfWeek"/>.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    static DateTimeOffset Last(this DateTimeOffset dateTime, DayOfWeek dayOfWeek) {
        DateTimeOffset last = dateTime.Last();
        return last.AddDays(Math.Abs(dayOfWeek - last.DayOfWeek) * -1);
    }

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> of the next <paramref name="dayOfWeek"/> in the current month.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <param name="dayOfWeek">The <see cref="DayOfWeek"/>.</param>
    /// <returns>The <see cref="DateTimeOffset"/>.</returns>
    static DateTimeOffset Next(this DateTimeOffset dateTime, DayOfWeek dayOfWeek) {
        int offsetDays = dayOfWeek - dateTime.DayOfWeek;
        if (offsetDays < 0) {
            offsetDays += DaysInWeek;
        }

        return dateTime.AddDays(offsetDays);
    }

    /// <summary>
    /// Checks if the given dateTime is within the provided range.
    /// </summary>
    /// <param name="dateTime">The <see cref="DateTimeOffset"/>.</param>
    /// <param name="rangeStart">The interval range start.</param>
    /// <param name="rangeLength">The interval range length.</param>
    /// <returns>true if within interval; otherwise false.</returns>
    public static bool Contains(this DateTimeOffset dateTime, DateTimeOffset rangeStart, TimeSpan rangeLength) {
        return rangeStart <= dateTime && dateTime <= rangeStart + rangeLength;
    }
}
