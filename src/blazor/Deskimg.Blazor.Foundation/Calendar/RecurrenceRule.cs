// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Calendar;

using AbstractCalendar = System.Globalization.Calendar;
using GregorianCalendar = System.Globalization.GregorianCalendar;

/// <summary>
/// Represents a recurrence rule.
/// </summary>
public struct RecurrenceRule : IEquatable<RecurrenceRule> {

    /// <summary>
    /// Initializes a new instance of the <see cref="RecurrenceRule"/> class.
    /// </summary>
    public RecurrenceRule() {
        Calendar = new GregorianCalendar();
        Interval = 1;
    }

    /// <summary>
    /// Gets or sets the <see cref="System.Globalization.Calendar"/>.
    /// Default: GregorianCalendar.
    /// </summary>
    public AbstractCalendar Calendar { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="VEventRecurrence" />.
    /// </summary>
    public VEventRecurrence Recurrence { get; set; }

    /// <summary>
    /// Gets or sets the recurrence interval.
    /// Default: 1.
    /// </summary>
    public int Interval { get; set; }

    /// <summary>
    /// Gets or sets the recurrence <see cref="Calendar.WeekOfMonth"/> rules.
    /// </summary>
    public WeekOfMonth WeekOfMonth { get; set; }

    /// <summary>
    /// Gets or sets the recurrence <see cref="Calendar.DaysOfWeek"/> rules.
    /// </summary>
    public DaysOfWeek DaysOfWeek { get; set; }

    #region IEquatable

    /// <inheritdoc/>
    public override int GetHashCode() =>
        Recurrence.GetHashCode() ^ Interval.GetHashCode() ^ WeekOfMonth.GetHashCode() ^ DaysOfWeek.GetHashCode();

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is RecurrenceRule other && Equals(other);

    /// <inheritdoc/>
    public bool Equals(RecurrenceRule other) => GetHashCode() == other.GetHashCode();

    /// <see cref="Equals(RecurrenceRule)"/>
    public static bool operator ==(RecurrenceRule left, RecurrenceRule right) => left.Equals(right);

    /// <see cref="Equals(RecurrenceRule)"/>
    public static bool operator !=(RecurrenceRule left, RecurrenceRule right) => !(left.Equals(right));

    #endregion
}
