// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml;

namespace Deskimg.Calendar.Epg;

/// <inheritdoc/>
public class DurationConverter : JsonConverter<TimeSpan?> {

    /// <inheritdoc/>
    public override TimeSpan? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) =>
        Convert(reader.GetString() ?? string.Empty);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal static TimeSpan Convert(string value) => XmlConvert.ToTimeSpan(value);

    /// <inheritdoc/>
    public override void Write(Utf8JsonWriter writer, TimeSpan? value, JsonSerializerOptions options) {
        if (value is not null) {
            writer.WriteStringValue(Convert(value.Value));
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal static string Convert(TimeSpan value) => XmlConvert.ToString(value);
}
