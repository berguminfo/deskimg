// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see href="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/epg.nrk.no.md"/>

using System.Text.Json.Serialization;

namespace Deskimg.Calendar.Epg.NO.Schema;

record EPGWithCompoundSchema(NrkChannelSchema Channel, IImmutableList<EPGEntrySchema> Entries);

sealed class NrkChannelSchema : IChannel, IEquatable<NrkChannelSchema> {

    /// <inheritdoc cref="IChannel.Id"/>
    [JsonPropertyName("id")]
    public required string OriginId { get; set; }

    /// <inheritdoc/>
    public required string Title { get; set; }

    public required ImmutableList<CdnImageSchema> Illustration { get; set; }

    #region IChannel

    [JsonIgnore]
    public string Id => $"{OriginId}.nrk.no";

    /// <inheritdoc cref="IChannel.GetImageUri"/>
    public Uri? GetImageUri(uint minWidth, uint maxWidth, uint minHeight, uint maxHeight) {
        string? uriString = (
            from illustration in Illustration
            from webImage in illustration.WebImages
            where minWidth <= webImage.PixelWidth && webImage.PixelWidth <= maxWidth
            select webImage.ImageUrl).FirstOrDefault();

        Uri? result = null;
        Uri.TryCreate(uriString, UriKind.Absolute, out result);
        return result;
    }

    #endregion

    #region IEquatable

    /// <inheritdoc/>
    public override int GetHashCode() => Id?.GetHashCode(StringComparison.Ordinal) ?? base.GetHashCode();

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is NrkChannelSchema other && Equals(other);

    /// <inheritdoc/>
    public bool Equals(NrkChannelSchema? other) => GetHashCode() == other?.GetHashCode();

    /// <inheritdoc cref="Equals(NrkChannelSchema)"/>
    public static bool operator ==(NrkChannelSchema left, NrkChannelSchema right) => left.Equals(right);

    /// <inheritdoc cref="Equals(NrkChannelSchema)"/>
    public static bool operator !=(NrkChannelSchema left, NrkChannelSchema right) => !(left == right);

    #endregion
}

sealed class EPGEntrySchema : IVEvent {

    public required string Title { get; set; }

    /// <see cref="IVEvent.Description"/>
    public required string Description { get; set; }

    [JsonConverter(typeof(UnixTimestampConverter))]
    public required DateTimeOffset? PlannedStart { get; set; } // NOTE: should be specified as not required

    [JsonConverter(typeof(UnixTimestampConverter))]
    public required DateTimeOffset? ActualStart { get; set; } // NOTE: should be specified as not required

    /// <see cref="IVEvent.Duration"/>
    [JsonConverter(typeof(DurationConverter))]
    public TimeSpan? Duration { get; set; } // NOTE: should be specified as not required

    public string? ProgramId { get; set; }

    public CdnImageSchema? Image { get; set; } // NOTE: should be specified as not required

    public CategorySchema? Category { get; set; } // NOTE: should be specified as not required

    #region IVEvent

    [JsonIgnore]
    public string UniqueId => ProgramId ?? Guid.NewGuid().ToString("n");

    [JsonIgnore]
    public string Summary => Title;

    [JsonIgnore]
    public HashSet<string> Attachment {
        get {
            HashSet<string> result = [];
            if (Image is not null) {
                var imageUrl = (
                    from item in Image.WebImages
                    orderby item.PixelWidth
                    select item.ImageUrl
                ).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(imageUrl)) {
                    result.Add(imageUrl);
                }
            }

            return result;
        }
    }

    [JsonIgnore]
    public HashSet<object> Categories => Category is null ? [] : [Category.DisplayValue];

    #endregion
}

record CdnImageSchema(IImmutableList<WebImageSchema> WebImages);

record WebImageSchema(string ImageUrl, uint PixelWidth);

record CategorySchema(string DisplayValue, bool IsTvCategory, bool IsRadioCategory);
