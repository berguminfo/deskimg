// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see href="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/epg.nrk.no.md"/>

using System.Globalization;
using System.Text.Json.Serialization;

namespace Deskimg.Calendar.Epg.NO.Schema;

record GetAllenteEpgResponse(string Date, ImmutableList<AllenteChannelSchema> Channels);

sealed class AllenteChannelSchema : IChannel, IEquatable<AllenteChannelSchema> {

    /// <inheritdoc cref="IChannel.Id"/>
    [JsonPropertyName("id")]
    public required string OriginId { get; set; }

    public required string Icon { get; set; }

    public required string Name { get; set; }

    public required ImmutableList<AllenteEventSchema> Events { get; set; }

    #region IChannel

    [JsonIgnore]
    public string Id => s_mapping.TryGetValue(OriginId, out var value) ? value : OriginId;

    static readonly Dictionary<string, string> s_mapping = new() {
        // ordering similar to tvguide.vg.no
        ["0090"] = "nrk1.nrk.no",
        ["0187"] = "direkte.tv2.no",
        ["534"] = "tvnorge.no",
        ["0288"] = "nrk2.nrk.no",
        ["457"] = "nyheter.tv2.no",
        ["0405"] = "zebra.tv2.no",
        ["0289"] = "nrk3.nrk.no",
        ["533"] = "rex.no",
        ["0361"] = "tv3plus.no",
        ["0298"] = "tv3.no",
        ["0056"] = "fem.no",
        ["1034"] = "vgtv.no",
        ["1012"] = "tlc.com",
        ["531"] = "eurosport1.sport",
        ["0365"] = "sport1.v.sport",
        ["0316"] = "natgeo.docs",
        ["1003"] = "history.docs",
        ["0277"] = "livsstil.tv2.no",
        ["0206"] = "tv6.no",
        ["535"] = "vox.no",
        ["0199"] = "sport1.tv2.no",
        ["0406"] = "sport2.tv2.no",
        // animal-planet.docs
        ["1004"] = "history2.docs",
        //["0344"] = "al-jazeera.info",
        // earth.bbc.uk
        ["1017"] = "nordic.bbc.uk",
        ["0016"] = "news.bbc.uk",
        ["1008"] = "bloomberg.info",
        // cartoonito
        // ["0028"] = "cartoon-network.?",
        // cbs-reality
        // first.c-more.film
        // hits.c-more.film
        // series.c-more.film
        // stars.c-more.film
        // ["0032"] = "cnbc.?",
        ["0033"] = "cnn.info",
        ["532"] = "discovery.docs",
        // discovery-science
        // disneychannel
        // disney-junior
        ["452"] = "dr1.dr.dk",
        ["0051"] = "dr2.dr.dk",
        ["530"] = "eurosportn.sport",
        // extreme-sports-channel
        ["1011"] = "id.docs",
        ["0080"] = "mtv.music",
        // wild.natgeo.docs
        // ["0086"] = "nickelodeon.?",
        // ["0088"] = "nickjr.?",
        // ["570"] = "nicktoons.?",
        // norway-live
        ["972"] = "sf-kanalen.tv4.se",
        ["596"] = "news.sky.info",
        ["0121"] = "svt1.svt.se",
        ["0141"] = "svt2.svt.se",
        // travel-channel
        ["0188"] = "tv2.dk",
        // ["0197"] = "premium.tv2.no",
        // ["0198"] = "premium2.tv2.no",
        // tv3.dk
        // tv3.se
        // tv4.se
        // tv6.se
        // tv8.se
        // ["0299"] = "action.v.film",
        // ["0308"] = "family.v.film",
        // ["0322"] = "hits.v.film",
        //["0321"] = "premiere.v.film",
        // ["0358"] = "explore.viasat.docs",
        // ["0357"] = "history.viasat.docs",
        // ["0356"] = "nature.viasat.docs",
        // visjon-norge
        // ["0320"] = "series.v.film",
        ["0271"] = "sportplus.v.sport",
        ["608"] = "sport2.v.sport",
        ["609"] = "sport3.v.sport",
        ["0364"] = "golf.v.sport",

        /*
        ["0149"] = "svtk.svt.se",
        ["418"] = "ultra.v.sport",
        ["1089"] = "heim",
        ["1002"] = "SkyShowtime 1",
        ["1001"] = "SkyShowtime 2",
        ["0246"] = "00s.mtv.music",
        ["604"] = "80s.mtv.music",
        ["668"] = "Horse & Country",
        ["1097"] = "Mezzo Live",
        ["1094"] = "Love Nature HD",
        ["1096"] = "Moonbug",
        ["1007"] = "DW English",
        ["0147"] = "barn.svt.se",
        ["1091"] = "Hits HD ",
        ["1090"] = "Stars HD",
        ["1101"] = "Dizi",
        ["1047"] = "premier-league.v.sport",
        ["1048"] = "premier-league1.v.sport",
        ["1049"] = "premier-league2.v.sport",
        ["1050"] = "premier-league3.v.sport",
        ["1051"] = "premier-league4.v.sport",
        ["0255"] = "live1.v.sport",
        ["0256"] = "live2.v.sport",
        ["0257"] = "live3.v.sport",
        ["0258"] = "live4.v.sport",
        ["0259"] = "live5.v.sport",
        */
    };


    [JsonIgnore]
    public string Title => Name;

    public Uri? GetImageUri(uint minWidth, uint maxWidth, uint minHeight, uint maxHeight) {
        string uriString = Icon.StartsWith("//", StringComparison.Ordinal) ?
            $"https:{Icon}" :
            Icon;
        if (Uri.TryCreate(uriString, UriKind.Absolute, out var result)) {
            return result;
        } else {
            return null;
        }
    }

    #endregion

    #region IEquatable

    /// <inheritdoc/>
    public override int GetHashCode() => Id?.GetHashCode(StringComparison.Ordinal) ?? base.GetHashCode();

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is AllenteChannelSchema other && Equals(other);

    /// <inheritdoc/>
    public bool Equals(AllenteChannelSchema? other) => GetHashCode() == other?.GetHashCode();

    /// <inheritdoc cref="Equals(AllenteChannelSchema)"/>
    public static bool operator ==(AllenteChannelSchema left, AllenteChannelSchema right) => left.Equals(right);

    /// <inheritdoc cref="Equals(AllenteChannelSchema)"/>
    public static bool operator !=(AllenteChannelSchema left, AllenteChannelSchema right) => !(left == right);

    #endregion
}

sealed class AllenteEventSchema : IVEvent {

    public required string Id { get; set; }

    public bool Live { get; set; }

    public required DateTimeOffset Time { get; set; }

    public required string Title { get; set; }

    public required AllenteEventDetailsSchema Details { get; set; }

    #region IVEvent

    [JsonIgnore]
    public string UniqueId => Id;

    [JsonIgnore]
    public string Summary => Title;

    [JsonIgnore]
    public string? Description => Details.Description;

    [JsonIgnore]
    public TimeSpan? Duration => TimeSpan.FromMinutes(Convert.ToInt32(Details.Duration, CultureInfo.InvariantCulture));

    [JsonIgnore]
    public HashSet<string> Attachment => string.IsNullOrWhiteSpace(Details.Image) ? [] : [Details.Image];

    [JsonIgnore]
    public HashSet<object> Categories {
        get {
            HashSet<object> result = [Details.Categories];
            if (Details.Episode > 0) {
                result.Add($"{Details.Season}/{Details.Episode}");
            }

            return result;
        }
    }

    #endregion
}

record AllenteEventDetailsSchema(
    string Title,
    string Image,
    string Description,
    int Season,
    int Episode,
    ImmutableList<string> Categories,
    string Duration);
