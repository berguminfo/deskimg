// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Net.Http.Json;
using System.Text.Json;

namespace Deskimg.Calendar.Epg.NO;

/// <summary>
/// Represents the epg.nrk.no <see cref="IEpgCalendar"/>.
/// </summary>
[Export<IEpgCalendar>("epg.nrk.no")]
public class NRK(
    ILogger<NRK> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IEpgCalendar, ICanCacheControl, ICanSetTag {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public async Task<ILookup<IChannel, IEnumerable<SubsetResult>>?> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default) {

        ILookup<IChannel, IEnumerable<SubsetResult>>? result = null;

        Uri? requestUri = endpoint.Fetch(Tag.Key, new {
            Tag = Tag.Value,
            Date = rangeStart.ToLocalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)
        });
        if (requestUri is not null) {

            try {
                result = await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    // NOTE: in order to show continuous list, fetch previous, this and next day
                    // FIXME: try find out the exact time the EPG moves to next or previous day
                    var set = Tag.Value?.ToString()?.Split(',').ToList() ?? [];
                    var orderBy = set.ToDictionary(x => x, x => set.IndexOf(x));
                    List<Schema.EPGWithCompoundSchema> intermediateResult = [];
                    for (int incr = -1; incr <= 1; ++incr) {
                        requestUri = endpoint.Fetch(Tag.Key, new {
                            Tag = Tag.Value,
                            // TODO: convert to UTC+1 instead of local-time
                            Date = rangeStart.AddDays(incr).ToLocalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)
                        }, discardQuery: true);

#pragma warning disable CS8604 // Converting null literal or possible null value to non-nullable type. NOTE: false positive we has already checked requestUri.

                        logger.FetchStarting(requestUri, CacheControl);

#pragma warning restore CS8604 // Converting null literal or possible null value to non-nullable type.

                        var response = await http.GetFromJsonAsync<IList<Schema.EPGWithCompoundSchema>?>(
                            requestUri,
                            CacheControl,
                            s_sourceGenOptions,
                            cancellationToken);

                        if (response is not null) {
                            intermediateResult.AddRange(response);
                        }
                    }

                    return intermediateResult.OrderBy(x => orderBy[x.Channel.OriginId]).ToLookup(
                        x => (IChannel)x.Channel,
                        x => Subset(x.Entries).ToList().AsEnumerable());
                });
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, Tag.Key, ex.StatusCode);
            } catch (JsonException ex) {
                logger.DeserializeJsonFailed(ex, Tag.Key, ex.LineNumber, ex.BytePositionInLine);
            }
        }

        return result;
    }

    static IEnumerable<SubsetResult> Subset(IEnumerable<Schema.EPGEntrySchema> itr) {
        foreach (var item in itr) {
            DateTimeOffset when = item.PlannedStart ?? item.ActualStart ?? DateTimeOffset.MinValue;
            yield return new(when, item);
        }
    }
}
