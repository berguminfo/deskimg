// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// Ignore Spelling: Allente

using System.Globalization;
using System.Text.Json;

namespace Deskimg.Calendar.Epg.NO;

/// <summary>
/// Represents the epg.allente.no <see cref="IEpgCalendar"/>.
/// </summary>
[Export<IEpgCalendar>("epg.allente.no")]
public class Allente(
    ILogger<Allente> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IEpgCalendar, ICanCacheControl, ICanSetTag {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public async Task<ILookup<IChannel, IEnumerable<SubsetResult>>?> SubsetAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        CancellationToken cancellationToken = default) {

        ILookup<IChannel, IEnumerable<SubsetResult>>? result = null;

        Uri? requestUri = endpoint.Fetch(Tag.Key, new {
            Tag = Tag.Value,
            Date = rangeStart.ToLocalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)
        });
        if (requestUri is not null) {
            try {
                result = await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    // NOTE: in order to show continuous list, fetch previous, this and next day
                    // FIXME: try find out the exact time the EPG moves to next or previous day
                    var tags = Tag.Value?.ToString()?.Split(',').ToList() ?? [];
                    var channelOrdering = tags.Distinct().ToDictionary(x => x, x => tags.IndexOf(x));
                    List<Schema.AllenteChannelSchema> intermediateResult = [];

                    for (int incr = -1; incr <= 1; ++incr) {
                        requestUri = endpoint.Fetch(Tag.Key, new {
                            // NOTE: this parameter is ignored by the server, but required to invalidate the cache
                            Tag = Tag.Value,
                            // TODO: convert to UTC+1 instead of local-time
                            Date = rangeStart.AddDays(incr).ToLocalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)
                        }, discardQuery: true);

#pragma warning disable CS8604 // Converting null literal or possible null value to non-nullable type. NOTE: false positive we has already checked requestUri.

                        logger.FetchStarting(requestUri, CacheControl);

#pragma warning restore CS8604 // Converting null literal or possible null value to non-nullable type.

                        var response = await http.GetFromJsonAsync<Schema.GetAllenteEpgResponse?>(
                            requestUri,
                            CacheControl,
                            s_sourceGenOptions,
                            cancellationToken);

                        if (response is not null) {
                            intermediateResult.AddRange(response.Channels.Where(x => channelOrdering.ContainsKey(x.OriginId)));
                        }
                    }

                    return intermediateResult.OrderBy(x => channelOrdering[x.OriginId]).ToLookup(
                        item => (IChannel)item,
                        item => Subset(item.Events).ToList().AsEnumerable());
                });
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, Tag.Key, ex.StatusCode);
            } catch (JsonException ex) {
                logger.DeserializeJsonFailed(ex, Tag.Key, ex.LineNumber, ex.BytePositionInLine);
            }
        }

        return result;
    }

    static IEnumerable<SubsetResult> Subset(IEnumerable<Schema.AllenteEventSchema> itr) {
        foreach (var item in itr) {
            yield return new(item.Time, item);
        }
    }
}
