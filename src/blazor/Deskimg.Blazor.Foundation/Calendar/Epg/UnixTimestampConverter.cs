// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace Deskimg.Calendar.Epg;

/// <inheritdoc/>
public class UnixTimestampConverter : JsonConverter<DateTimeOffset?> {

    static readonly Regex s_pattern = new(@"/Date\((\d+)([\+-]\d{2})(\d{2})\)/");

    /// <inheritdoc/>
    public override DateTimeOffset? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) =>
        Convert(reader.GetString() ?? string.Empty);

    internal static DateTimeOffset? Convert(string value) {
        var match = s_pattern.Match(value);
        if (!match.Success) {
            return null;
        }

        var milliseconds = long.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
        // TODO: var offsetHours = int.Parse(match.Groups[2].Value);
        // TODO: var offsetMinutes = int.Parse(match.Groups[3].Value);

        return DateTimeOffset.FromUnixTimeMilliseconds(milliseconds);
    }

    /// <inheritdoc/>
    public override void Write(Utf8JsonWriter writer, DateTimeOffset? value, JsonSerializerOptions options) {
        if (value is not null) {
            writer.WriteStringValue(Convert(value.Value));
        }
    }

    internal static string Convert(DateTimeOffset value) {
        string milliseconds = value.ToUnixTimeMilliseconds().ToString("D", CultureInfo.InvariantCulture);
        string offsetSign = value.Offset.Hours >= 0 ? "+" : "-";
        string offsetHours = Math.Abs(value.Offset.Hours).ToString("D2", CultureInfo.InvariantCulture);
        string offsetMinutes = value.Offset.Minutes.ToString("D2", CultureInfo.InvariantCulture);

        return $"/Date({milliseconds}{offsetSign}{offsetHours}{offsetMinutes})/";
    }
}
