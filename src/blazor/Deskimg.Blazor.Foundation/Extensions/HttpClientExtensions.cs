// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text.Json;
using System.Xml.Linq;

namespace System.Net.Http;

using Deskimg.Extensions;

/// <summary>
/// Represents the 'PROPFIND' response record.
/// </summary>
/// <param name="Source">The source <see cref="Uri"/>.</param>
/// <param name="Name">The optional propstat element 'displayname'.</param>
/// <param name="ContentType">The optional propstat element 'getcontenttype'.</param>
/// <param name="LastModified">The optional propstat element 'getlastmodified'.</param>
public record PropFindResponse(
    Uri Source,
    string? Name = null,
    string? ContentType = null,
    DateTimeOffset? LastModified = null);

/// <summary>
/// Represents additional extension methods for <see cref="HttpClient"/>.
/// </summary>
public static class HttpClientExtensions {

    const string DavNamespace = "DAV:";

    /// <inheritdoc cref="HttpClient.GetStreamAsync(Uri?)"/>
    public static Task<Stream> GetStreamAsync(
        this HttpClient http,
        Uri? requestUri,
        CacheControlHeaderValue cacheControl,
        CancellationToken cancellationToken = default) =>

        http.GetStreamAsync(requestUri, cacheControl, [], cancellationToken);

    /// <inheritdoc cref="HttpClient.GetStreamAsync(Uri?)"/>
    public static Task<Stream> GetStreamAsync(
        this HttpClient http,
        Uri? requestUri,
        CacheControlHeaderValue cacheControl,
        IEnumerable<KeyValuePair<string, string>> headers,
        CancellationToken cancellationToken = default) {

        using HttpRequestMessage request = new(HttpMethod.Get, requestUri);
        request.AddHeaders(cacheControl, headers);
        return http.GetStreamAsync(request, cancellationToken);
    }

    /// <inheritdoc cref="HttpClient.GetStreamAsync(Uri?)"/>
    public static async Task<Stream> GetStreamAsync(
        this HttpClient http,
        HttpRequestMessage request,
        CancellationToken cancellationToken = default) {

        var response = await http.SendAsync(request, cancellationToken);
        response.EnsureSuccessStatusCode();
        return await response.Content.ReadAsStreamAsync(cancellationToken);
    }

    /// <inheritdoc cref="HttpClient.GetAsync(Uri?)"/>
    public static Task<TValue?> GetFromJsonAsync<TValue>(
        this HttpClient http,
        Uri? requestUri,
        CacheControlHeaderValue cacheControl,
        JsonSerializerOptions? options = null,
        CancellationToken cancellationToken = default) =>

        http.GetFromJsonAsync<TValue>(requestUri, cacheControl, [], options, cancellationToken);

    /// <inheritdoc cref="HttpClient.GetAsync(Uri?)"/>
    public static async Task<TValue?> GetFromJsonAsync<TValue>(
        this HttpClient http,
        Uri? requestUri,
        CacheControlHeaderValue cacheControl,
        IEnumerable<KeyValuePair<string, string>> headers,
        JsonSerializerOptions? options = null,
        CancellationToken cancellationToken = default) {

        using var stream = await http.GetStreamAsync(requestUri, cacheControl, headers, cancellationToken);
        return await JsonSerializer.DeserializeAsync<TValue>(stream, options, cancellationToken);
    }

    /// <inheritdoc cref="HttpClient.GetAsync(Uri?)"/>
    public static Task<IEnumerable<PropFindResponse>> PropFindAsync(
        this HttpClient http,
        Uri? requestUri,
        CacheControlHeaderValue cacheControl,
        CancellationToken cancellationToken = default) =>

        http.PropFindAsync(requestUri, cacheControl, [], cancellationToken);

    /// <inheritdoc cref="HttpClient.GetAsync(Uri?)"/>
    public static async Task<IEnumerable<PropFindResponse>> PropFindAsync(
        this HttpClient http,
        Uri? requestUri,
        CacheControlHeaderValue cacheControl,
        IEnumerable<KeyValuePair<string, string>> headers,
        CancellationToken cancellationToken = default) {

        using HttpRequestMessage request = new(new("PROPFIND"), requestUri);
        request.AddHeaders(cacheControl, headers);
        HttpResponseMessage response = await http.SendAsync(request, cancellationToken);
        response.EnsureSuccessStatusCode();

        using var stream = await response.Content.ReadAsStreamAsync(cancellationToken);
        var document = await XDocument.LoadAsync(stream, LoadOptions.None, cancellationToken);
        XNamespace d = DavNamespace;
        return
            from element in document.Descendants(d + "response")
            let propstat = element.Element(d + "propstat")
            let prop = propstat.Element(d + "prop")
            select new PropFindResponse(
                new Uri(element?.Element(d + "href")?.Value ?? string.Empty),
                prop.Element(d + "displayname")?.Value,
                prop.Element(d + "getcontenttype")?.Value,
                DateTimeOffset.Parse(prop.Element(d + "getlastmodified")?.Value ?? string.Empty, CultureInfo.InvariantCulture));
    }
}
