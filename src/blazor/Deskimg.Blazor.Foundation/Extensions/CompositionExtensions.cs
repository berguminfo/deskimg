// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Microsoft.Extensions.DependencyInjection;

using Deskimg.Composition;

/// <summary>
/// Represents additional extension methods for <see cref="IServiceCollection"/>.
/// </summary>
public static class CompositionExtensions {

    /// <summary>
    /// Extends <see cref="IServiceCollection"/> with composition exports.
    /// </summary>
    public static IServiceCollection AddCompositions(this IServiceCollection services, params IAssemblyExports[] args) {

        // register singleton
        CompositionService compositionService = new();
        services.AddSingleton(compositionService);

        // register exports, for now all exports is transient but some may in future be scoped
        foreach (var assemblyExport in args) {
            foreach (var item in assemblyExport.GetExports()) {
                compositionService.Exports[item.Key] = item.Value;
                services.AddTransient(item.Value);
            }
        }

        return services;
    }
}
