// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace System.Net.Http;

using Deskimg.Extensions;

/// <summary>
/// Represents additional extension methods for <see cref="HttpRequestMessage"/>.
/// </summary>
public static class HttpRequestMessageExtensions {

    /// <summary>
    /// Adds additional HTTP headers.
    /// </summary>
    /// <param name="request">The request message.</param>
    public static void AddHeaders(
        this HttpRequestMessage request) {

        request.Headers.Add(FetchToken.HeaderKey, FetchToken.Create());
    }

    /// <summary>
    /// Adds additional HTTP headers.
    /// </summary>
    /// <param name="request">The request message.</param>
    /// <param name="cacheControl">The required cache control header</param>
    public static void AddHeaders(
        this HttpRequestMessage request,
        CacheControlHeaderValue cacheControl) {

        request.AddHeaders();
        request.Headers.CacheControl = cacheControl;
    }

    /// <summary>
    /// Adds additional HTTP headers.
    /// </summary>
    /// <param name="request">The request message.</param>
    /// <param name="cacheControl">The required cache control header</param>
    /// <param name="headers">Additional headers.</param>
    public static void AddHeaders(
        this HttpRequestMessage request,
        CacheControlHeaderValue cacheControl,
        IEnumerable<KeyValuePair<string, string>> headers) {

        request.AddHeaders(cacheControl);
        foreach (var item in headers) {
            request.Headers.Add(item.Key, item.Value);
        }
    }
}
