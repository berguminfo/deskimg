// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json.Serialization;
using Deskimg.Calendar.Epg.NO.Schema;
using Deskimg.Calendar.Specialized.Schema;
using Deskimg.Meteorology.NO.Schema;

namespace Deskimg;

[JsonSourceGenerationOptions]
[JsonSerializable(typeof(Strings))]
[JsonSerializable(typeof(GetAllenteEpgResponse))]
[JsonSerializable(typeof(IList<EPGWithCompoundSchema>))]
[JsonSerializable(typeof(GetKvTideForecastResponse))]
[JsonSerializable(typeof(IList<NveAvalancheWarningSimple>))]
[JsonSerializable(typeof(IList<NveFloodWarning>))]
[JsonSerializable(typeof(IList<NveLandslideWarning>))]
[JsonSerializable(typeof(GetTrafficInformationResponse))]
[JsonSerializable(typeof(GetYrForecastResponse))]
internal partial class JsonSchemaSourceGeneration : JsonSerializerContext { }
