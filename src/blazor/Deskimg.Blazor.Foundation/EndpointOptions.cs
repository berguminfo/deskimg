// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// Ignore Spelling: dbug, Plugins

using System.Text.RegularExpressions;

namespace Deskimg;

using Deskimg.Extensions;

/// <summary>
/// Represents the Endpoint <see cref="IConfigurationSection"/>.
/// </summary>
public sealed partial class EndpointOptions(
#if DEBUG
    DebugOptions dbug,
#endif
    ILogger<EndpointOptions> logger,
    IConfigurationSection section
) {
#if DEBUG
    ///
    public DebugOptions Dbug => dbug;
#endif

    /// <summary>
    /// Specifies the <see cref="IConfiguration.GetSection"/> name.
    /// </summary>
    public const string Name = "Endpoint";

    #region Fetch API

    /// <summary>
    /// Gets the fetch URI for the current configuration.
    /// </summary>
    /// <param name="key">The endpoint key.</param>
    /// <param name="data">Optional endpoint query values.</param>
    /// <param name="apiKey">Optional endpoint API key.</param>
    /// <param name="discardQuery">DEBUG: discard the query section.</param>
    /// <param name="discardPath">DEBUG: discard the number of path sections.</param>
    /// <returns>The URI.</returns>
    /// <exception cref="ArgumentNullException">Configuration error.</exception>
    public Uri? Fetch(string key, object? data, bool apiKey = false, bool discardQuery = false, int discardPath = 0) {
        string value = section[key] ?? string.Empty;
        string endpoint = data is null ? value : MessageFormatter.Format(value, data);
        return Fetch(endpoint, apiKey, discardQuery, discardPath);
    }

    /// <summary>
    /// Gets the fetch URI for the current configuration.
    /// </summary>
    /// <param name="input">The input URI.</param>
    /// <param name="apiKey">Optional endpoint API key.</param>
    /// <param name="discardQuery">DEBUG: discard the query section.</param>
    /// <param name="discardPath">DEBUG: discard the number of path sections.</param>
    /// <param name="key">The endpoint key.</param>
    /// <returns>The URI.</returns>
    /// <exception cref="ArgumentNullException">Configuration error.</exception>
    public Uri? Fetch(string input, bool apiKey = false, bool discardQuery = false, int discardPath = 0, string key = "fetch.api") {
#if DEBUG
        if (!string.IsNullOrWhiteSpace(dbug.Fetch)) {
            key = dbug.Fetch;
        }
        if (key == "sample-data") {
            return SampleData(input, discardQuery, discardPath);
        }
#endif
        string path = DeflateBase64Url.Encode(input);
        if (apiKey) {
            path += $"?{FetchToken.QueryKey}={FetchToken.Create()}";
        }

        Uri? requestUri = section.Combine(key, path);
        if (requestUri is null) {
            logger.BadEndpoint(key);
        }

        return requestUri;
    }

    /// <summary>
    /// Gets the fetch URI for the current configuration.
    /// </summary>
    /// <param name="key">The endpoint key.</param>
    /// <returns>The URI.</returns>
    /// <exception cref="ArgumentNullException">Configuration error.</exception>
    public Uri? Index(string key = "fetch.api") {
        Uri? requestUri;
        Uri.TryCreate(section.GetValue<string?>(key), UriKind.Absolute, out requestUri);
        if (requestUri is null) {
            logger.BadEndpoint(key);
        }

        return requestUri;
    }

#if DEBUG
    // escape "special" filesystem characters to "_" (POSIX, NTFS)
    [GeneratedRegex(@"[\\\*\?\|/:""<>]", RegexOptions.Singleline)]
    private static partial Regex EscapeTargetPattern();

    const string EscapeTargetReplacement = "_";

    Uri? SampleData(string input, bool discardQuery, int discardPath) {
        Uri inputUri = new(input);
        string target = discardQuery ? inputUri.AbsolutePath : inputUri.PathAndQuery;
        if (discardPath > 0) {
            int index = target.Length;
            for (int i = 0; i < discardPath; ++i) {
                index = target.LastIndexOf('/', index - 1);
                if (index < 0) {
                    break;
                }
            }
            target = target[..index];
        }

        target = EscapeTargetPattern().Replace(target, EscapeTargetReplacement);
        target = inputUri.IdnHost + target;

        return section.Combine("sample-data", target);
    }
#endif

    #endregion

    #region Sets API

    /// <summary>
    /// Gets the sets URI for the current configuration.
    /// </summary>
    /// <param name="id">The sets identifier.</param>
    /// <param name="key">The endpoint key.</param>
    /// <returns>The URI.</returns>
    /// <exception cref="InvalidOperationException">Configuration error.</exception>
    public Uri? Sets(string id, string key = "sets.api") {
#if DEBUG
        if (!string.IsNullOrWhiteSpace(dbug.Sets)) {
            key = dbug.Sets;
        }
        if (key == "sets-data") {
            id += ".json";
        }
#endif
        Uri? requestUri = section.Combine(key, id);
        if (requestUri is null) {
            logger.BadEndpoint(key);
        }

        return requestUri;
    }

    #endregion

    #region CalDAV API

    /// <summary>
    /// Gets the caldav.api/{fileName} URI.
    /// </summary>
    /// <returns>The URI.</returns>
    public Uri? CalDAV(string fileName, string key = "caldav.api") {
#if DEBUG
        if (!string.IsNullOrWhiteSpace(dbug.CalDAV)) {
            key = dbug.CalDAV;
        }
#endif
        Uri? requestUri = section.Combine(key, fileName);
        if (requestUri is null) {
            logger.BadEndpoint(key);
        }

        return requestUri;
    }

    /// <summary>
    /// Gets the caldav.api/plugins URI.
    /// </summary>
    /// <returns>The URI.</returns>
    public Uri? Plugins(string key = "caldav.api") => CalDAV("plugins", key);

    #endregion
}

/// <summary>
/// Represents extension methods for the <see cref="IConfiguration"/> interface.
/// </summary>
public static class ConfigurationExtensions {

    /// <summary>
    /// Gets the API URI for the current configuration.
    /// </summary>
    /// <param name="section">The endpoint configuration section.</param>
    /// <param name="key">The API service key.</param>
    /// <param name="path">The service resource identifier.</param>
    /// <returns>The URI.</returns>
    public static Uri? Combine(this IConfigurationSection section, string key, string path) {
        string? basename = section.GetValue<string?>(key);
        return !string.IsNullOrEmpty(basename) && Uri.TryCreate(basename + path, UriKind.Absolute, out var result) ? result : null;
    }
}
