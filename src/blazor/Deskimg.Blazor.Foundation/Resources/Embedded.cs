// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using System.Text;

namespace Deskimg.Resources;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public static class Embedded {

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Action(string name, string type = "svg") => Get(nameof(Action), name, type);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Epg(string name, string type = "svg") => Get(nameof(Epg), name, type);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Intl(string name, string type = "svg") => Get(nameof(Intl), name, type);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Status(string name, string type = "svg") => Get(nameof(Status), name, type);

    static string Get(string basename, string name, string type) {
        StringBuilder sb = new(17 + basename.Length + name.Length + type.Length);
        sb.AppendJoin(string.Empty, "Deskimg.Resources.", basename, ".", name, ".", type);
        string resourceName = sb.ToString();

        // NOTE: throw and terminate on error
        var stream = typeof(Embedded).Assembly.GetManifestResourceStream(resourceName);
        if (stream is null) {
            throw new FileNotFoundException(sb.ToString());
        }

        using var reader = new StreamReader(stream);
        return reader.ReadToEnd();
    }
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
