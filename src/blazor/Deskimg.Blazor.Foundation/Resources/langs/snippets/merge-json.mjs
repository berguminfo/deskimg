import fsPromises from "node:fs/promises";

// usage
if (process.argv.length < 4) {
    console.error("Usage: node merge-json.mjs <in:path>... <out:path>");
    process.exit(-1);
}

// read all inputs
const tasks = [];
for (const inpath of process.argv.slice(2, -1)) {
    tasks.push(new Promise(async resolve => {
        console.log(`reading input file '${inpath}'`);

        const content = await fsPromises.readFile(inpath, { encoding: "utf8" });
        const obj = JSON.parse(content);
        resolve(obj);
    }));
}

// merge each input object into result
await Promise.all(tasks);

let result = {};
for (const promise of tasks) {
    const source = await promise;
    Object.assign(result, source);
}

// write result object to output
const outpath = process.argv.slice(-1)[0];
console.log(`writing merged json to '${outpath}'`);
await fsPromises.writeFile(outpath, JSON.stringify(result, null, 2));
