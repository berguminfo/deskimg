// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <summary>
/// Utilities for the ultraviolet index.
/// </summary>
/// <see href="https://en.wikipedia.org/wiki/Ultraviolet_index"/>
public static class UltravioletIndex {

    /// <summary>
    /// Gets the ultraviolet index severity according to scale.
    /// </summary>
    /// <param name="ultravioletIndex">The measured ultraviolet index.</param>
    /// <returns>The ultraviolet index severity.</returns>
    public static string GetSeverity(double? ultravioletIndex) {
        if (ultravioletIndex is not null) {
            foreach (var item in s_ultravioletIndexSeverity) {
                if (ultravioletIndex.Value < item.Key) {
                    return item.Value;
                }
            }
        }

        return string.Empty;
    }

    // @see https://no.wikipedia.org/wiki/UV-indeks
    static readonly Dictionary<double, string> s_ultravioletIndexSeverity = new() {
        [3.0] = "--bg-severity-green", // skipping the green background
        [6.0] = "bg-severity-yellow",
        [8.0] = "bg-severity-orange",
        [11.0] = "bg-severity-red",
        [double.MaxValue] = "bg-severity-extreme"
    };
}
