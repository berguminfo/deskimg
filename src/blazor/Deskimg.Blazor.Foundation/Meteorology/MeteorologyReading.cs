// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <inheritdoc cref="IMeteorologyReading"/>
public partial class MeteorologyReading : IMeteorologyReading {

    /// <inheritdoc/>
    public required Type Provider { get; init; }

    /// <inheritdoc/>
    public string? Location { get; set; }

    /// <inheritdoc/>
    public DateTimeOffset ValidFrom { get; set; }

    /// <inheritdoc/>
    public DateTimeOffset ValidTo { get; set; }

    /// <inheritdoc/>
    public TemperatureReading? Temperature { get; set; }

    /// <inheritdoc/>
    public PrecipitationReading? Precipitation { get; set; }

    /// <inheritdoc/>
    public WindReading? Wind { get; set; }

    /// <inheritdoc/>
    public PressureReading? Pressure { get; set; }

    /// <inheritdoc/>
    public HumidityReading? Humidity { get; set; }

    /// <inheritdoc/>
    public UltravioletRadiationReading? UltravioletRadiation { get; set; }

    /// <inheritdoc/>
    public WeatherConditions? Conditions { get; set; }
}
