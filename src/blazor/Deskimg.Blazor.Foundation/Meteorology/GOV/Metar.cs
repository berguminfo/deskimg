// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Deskimg.Meteorology.GOV;

using Deskimg.Ephemeris;
using TokenHandler = Action<GroupCollection, MeteorologyReading>;

/// <summary>
/// Represents the metar.noaa.gov <see cref="IObservationProvider"/>.
/// </summary>
/// <remarks>
/// <list type="bullet">
/// <item><see href="http://www.weather.cod.edu/notes/metar.html"/></item>
/// <item><see href="http://www.ofcm.gov/fmh-1/fmh1.htm"/></item>
/// </list>
/// </remarks>
[Export<IObservationProvider>("metar.noaa.gov")]
public partial class Metar(
    ILogger<Metar> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IJSRuntime js,
    IMemoryCache cache) : IObservationProvider, ICanCacheControl, ICanSetTag {

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public async Task<IEnumerable<IMeteorologyReading>> GetObservationsAsync(
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        Uri? requestUri = endpoint.Fetch(Tag.Key, new {
            Id = Tag.Value
        });

        IMeteorologyReading[]? result = null;
        if (requestUri is not null) {
            try {
                result = await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    logger.FetchStarting(requestUri, CacheControl);
                    using var stream = await http.GetStreamAsync(
                        requestUri,
                        CacheControl,
                        cancellationToken);

                    return stream is null ? null : new IMeteorologyReading[] {
                    await ParseMetarAsync(observer, stream, cancellationToken)
                };
                });
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, requestUri, ex.StatusCode);
            } catch (Exception ex) {
                logger.ParseMetarFailed(ex, requestUri);
            }
        }

        return result ?? [];
    }

    internal async ValueTask<MeteorologyReading> ParseMetarAsync(Geoposition observer, Stream stream, CancellationToken cancellationToken = default) {
        MeteorologyReading observation = new() {
            Provider = typeof(IObservationProvider),
            Location = observer.ToString()
        };

        using StreamReader reader = new(stream, Encoding.ASCII);
        while (!reader.EndOfStream) {

            // first check for date-time line
            string? line = await reader.ReadLineAsync(cancellationToken);
            if (string.IsNullOrWhiteSpace(line)) {
                continue;
            }

            if (DateTimeOffset.TryParseExact(line, "yyyy/MM/dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out var dateTime)) {

                observation.ValidFrom = dateTime;
                observation.ValidTo = dateTime.Add(CacheControl.MaxAge ?? TimeSpan.Zero);

                // expect next line to be the METAR line
                line = await reader.ReadLineAsync(cancellationToken);
            }

            // apply METAR tokens
            if (line is not null) {
                logger.FetchMetarResponse(line);

                // update observation with this METAR line
                ParseMetar(observation, line);

                // optional set variant weather condition flags (Night, PolarTwilight)
                if (observation.Conditions is not null &&
                    observer.Coordinate is not null &&
                    observer.TimeZone is not null) {

                    Earth earth = new(js);
                    RiseSet solarRiseSet = await earth.GetRiseSetAsync<Solar>(observation.ValidFrom, observer.Coordinate, cancellationToken);
                    Algorithms.SetVariantConditions(
                        observation,
                        observation.ValidFrom,
                        observer.TimeZone,
                        solarRiseSet);
                }
            }
        }

        return observation;
    }

    internal static MeteorologyReading ParseMetar(MeteorologyReading observation, string line) {

        // remove RMK (remark) section
        int rmkIndex = line.IndexOf(" RMK", StringComparison.Ordinal);
        if (rmkIndex > 0) line = line[..(rmkIndex + 1)];
        //                                          ^--- add ending space character
        // ensure line ends with space character, required for token patterns
        if (line.EndsWith(' ')) line += ' ';

        foreach ((Regex pattern, TokenHandler handler) in s_tokens) {
            foreach (Match match in pattern.Matches(line)) {
                handler(match.Groups, observation);
            }
        }

        return observation;
    }

    // line patterns

    static readonly Dictionary<string, string> s_unit = new() {
        ["MPS"] = "m/s",
        ["KT"] = "kn",
        ["KMH"] = "km/h",
        ["Q"] = "hPa",
        ["A"] = "inHg"
    };

    static readonly Dictionary<string, WeatherConditions> s_skyConditionMapping = new() {
        ["FEW"] = WeatherConditions.Fair,         // 1/8, 2/8, 3/8
        ["SCT"] = WeatherConditions.PartlyCloudy, // 3/8, 4/8
        ["BKN"] = WeatherConditions.MostlyCloudy, // 5/8, 6/8, 7/8
        ["OVC"] = WeatherConditions.Cloudy,       // 8/8
        ["VV"] = WeatherConditions.Cloudy         // 8/8
    };

    // NOTE: keys must sort by least match first (i.e. SHRASN may match RA, SN, SHRA, and SHRASN)
    static readonly Dictionary<string, WeatherConditions> s_precipitationMapping = new() {
        ["SHTSRASN"] = WeatherConditions.Rain | WeatherConditions.Snow | WeatherConditions.Showers | WeatherConditions.Thunderstorm,
        ["SHTSRA"] = WeatherConditions.Rain | WeatherConditions.Showers | WeatherConditions.Thunderstorm,
        ["SHTSSN"] = WeatherConditions.Snow | WeatherConditions.Showers | WeatherConditions.Thunderstorm,
        ["SHRASN"] = WeatherConditions.Rain | WeatherConditions.Snow | WeatherConditions.Showers,
        ["SHRA"] = WeatherConditions.Rain | WeatherConditions.Showers,
        ["SHSN"] = WeatherConditions.Snow | WeatherConditions.Showers,
        ["TSRASN"] = WeatherConditions.Rain | WeatherConditions.Snow | WeatherConditions.Thunderstorm,
        ["TSRA"] = WeatherConditions.Rain | WeatherConditions.Thunderstorm,
        ["TSSN"] = WeatherConditions.Snow | WeatherConditions.Thunderstorm,
        ["RASN"] = WeatherConditions.Rain | WeatherConditions.Snow,
        ["RA"] = WeatherConditions.Rain,
        ["SN"] = WeatherConditions.Snow,
        ["SG"] = WeatherConditions.Snow,
        ["FG"] = WeatherConditions.Fog
    };

    static readonly Dictionary<string, WeatherConditions> s_intensityMapping = new() {
        ["+"] = WeatherConditions.Heavy,
        ["-"] = WeatherConditions.Light
    };

    [GeneratedRegex(@"(\d{2})(\d{2})(\d{2})Z\s", RegexOptions.Singleline)]
    private static partial Regex TimePattern { get; }

    [GeneratedRegex(@"\s(CAVOK|SKC|CLR|NSC|NCD)\s", RegexOptions.Singleline)]
    private static partial Regex ClearSkyPattern { get; }

    [GeneratedRegex(@"\s(FEW|SCT|BKN|OVC|VV)\d{3}[\s/]", RegexOptions.Singleline | RegexOptions.RightToLeft)]
    private static partial Regex SkyConditionPattern { get; }

    [GeneratedRegex(@"\s(\+|-|FZ)?(SH)?(TS)?(FG|RA|RASN|SN|SG)\s", RegexOptions.Singleline)]
    //                    ^ NOTE: matches only the most common codes (having mapping)
    private static partial Regex PrecipitationPattern { get; }

    [GeneratedRegex(@"\s(\d{3}|VRB)(\d+)(G\d+)?(MPS|KT|KMH)\s", RegexOptions.Singleline)]
    private static partial Regex WindPattern { get; }

    [GeneratedRegex(@"\s(M?\d+)/(M?\d*)\s", RegexOptions.Singleline)]
    private static partial Regex TemperaturePattern { get; }

    [GeneratedRegex(@"\s([QA])(\d{4})\s", RegexOptions.Singleline)]
    private static partial Regex PressurePattern { get; }

    // METAR tokens to execute
    static readonly List<(Regex, TokenHandler)> s_tokens = [
        (TimePattern, TimeToken),
        (ClearSkyPattern, ClearSkyToken),
        (SkyConditionPattern, SkyConditionToken),
        (PrecipitationPattern, PrecipitationToken),
        (WindPattern, WindToken),
        (TemperaturePattern, TemperatureToken),
        (PressurePattern, PressureToken)
    ];

    static void TimeToken(GroupCollection groups, MeteorologyReading observation) {
        if (int.TryParse(groups[1].Value, out var d) &&
            int.TryParse(groups[2].Value, out var h) &&
            int.TryParse(groups[3].Value, out var m)) {

            // set exact time of observation
            DateTimeOffset nowZ = observation.ValidFrom.ToUniversalTime();
            observation.ValidFrom = new(nowZ.Year, nowZ.Month, d, h, m, 0, TimeSpan.Zero);
        }
    }

    static void ClearSkyToken(GroupCollection groups, MeteorologyReading observation) =>
        observation.Conditions = WeatherConditions.ClearSky;

    static void SkyConditionToken(GroupCollection groups, MeteorologyReading observation) {
        if (s_skyConditionMapping.TryGetValue(groups[1].Value, out var condition)) {
            observation.Conditions = condition;
        }
    }

    static void PrecipitationToken(GroupCollection groups, MeteorologyReading observation) {
        var buffer = new StringBuilder();
        // append any SH match
        buffer.Append(groups[2].Value);
        // append any TS match
        buffer.Append(groups[3].Value);
        // append any FG|RA|RASN|SN|SG match
        buffer.Append(groups[4].Value);

        // set weather condition
        if (s_precipitationMapping.TryGetValue(buffer.ToString(), out var condition)) {
            if (s_intensityMapping.TryGetValue(groups[1].Value, out var intensity)) {
                condition |= intensity;
            }

            observation.Conditions = condition;
        }
    }

    static void WindToken(GroupCollection groups, MeteorologyReading observation) {
        WindReading result = new();

        // set wind direction
        if (groups[1].Value.Equals("VRB", StringComparison.Ordinal)) {
            result.VariableDirections = true;
        } else
        if (double.TryParse(groups[1].Value, out var windDirection)) {
            result.FromDirection = new(windDirection, "°");
        }

        // set wind speeds
        if (s_unit.TryGetValue(groups[4].Value, out string? windSpeedUnit)) {

            // sustained value
            if (double.TryParse(groups[2].Value, out var sustainedSpeed)) {
                result.Sustained = new(sustainedSpeed, windSpeedUnit);
            }

            // gust value
            if (groups[3].Value.StartsWith('G') && double.TryParse(groups[3].Value.AsSpan(1), out var gustSpeed)) {
                result.Gust = new(gustSpeed, windSpeedUnit);
            } else {
                gustSpeed = sustainedSpeed;
            }

            // average value
            double averageSpeed = (sustainedSpeed + gustSpeed) / 2.0;
            result.Average = new(averageSpeed, windSpeedUnit);

        }

        // update observation
        if (result.VariableDirections || result.FromDirection is not null) {
            observation.Wind = result;
        }
    }

    static void TemperatureToken(GroupCollection groups, MeteorologyReading observation) {
        static MeasureD? ParseTemperature(string value) {

            // gets span index with x mod 3
            // => if 'M': span starts at 1 otherwise 0
            // gets temperature multiplier with x - 2
            // => if 'M': multiply with -1 otherwise 1

            int start = value.StartsWith('M') ? 1 : 3;
            if (double.TryParse(value.AsSpan(start % 3), out var temperature)) {
                temperature *= start - 2;
                return new(temperature, "°C");
            } else {
                return null;
            }
        }

        // update observation
        TemperatureReading result = new() {
            Air = ParseTemperature(groups[1].Value),
            DewPoint = ParseTemperature(groups[2].Value),
        };
        if (result.Air is not null || result.DewPoint is not null) {
            observation.Temperature = result;
        }
    }

    static void PressureToken(GroupCollection groups, MeteorologyReading observation) {

        // set atmospheric pressure
        if (s_unit.TryGetValue(groups[1].Value, out string? pressureUnit) &&
            double.TryParse(groups[2].Value, out var pressure)) {

            // sample: A3006 = 30.06 inHg
            if (pressureUnit.Equals("inHg", StringComparison.Ordinal)) {
                pressure /= 100.0;
            }

            observation.Pressure = new() {
                Atmospheric = new(pressure, pressureUnit)
            };
        } else {
            throw new NotSupportedException($"Unknown pressure unit: {groups[1].Value}");
        }
    }
}
