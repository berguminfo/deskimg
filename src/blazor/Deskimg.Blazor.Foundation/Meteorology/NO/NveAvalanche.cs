// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text.Json;

namespace Deskimg.Meteorology.NO;

/// <summary>
/// Represents the avalanche.nve.no <see cref="IAlertProvider"/>.
/// </summary>
[Export<IAlertProvider>("avalanche.nve.no")]
[Export<IAlertProvider>("avalanche.nve.no/all")]
[Export<IAlertProvider>("avalanche.nve.no/region")]
public class NveAvalanche(
    ILogger<NveAvalanche> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IAlertProvider, ICanCacheControl, ICanSetTag {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public async Task<IEnumerable<IAlertMessage>> GetAlertsAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        IList<IAlertMessage>? result = null;

        if (observer?.Coordinate is not null) {
            Uri? requestUri = endpoint.Fetch(Tag.Key, new {
                Latitude = observer.Coordinate.Latitude,
                Longitude = observer.Coordinate.Longitude,
                Language = SR.Default.EndpointOptions[Tag.Key],
                Start = rangeStart.Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                End = (rangeStart + rangeLength).Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) // required
            }, discardPath: 5);

            if (requestUri is not null) {
                try {
                    result = await cache.GetOrCreateAsync(requestUri, async entry => {
                        entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                        logger.FetchStarting(requestUri, CacheControl);
                        var response = await http.GetFromJsonAsync<IList<Schema.NveAvalancheWarningSimple>>(
                            requestUri,
                            CacheControl,
                            s_sourceGenOptions,
                            cancellationToken);

                        return Subset(observer.ToString(), response).ToList();
                    });
                } catch (OperationCanceledException) {
                } catch (HttpRequestException ex) {
                    logger.FetchFailed(ex, requestUri, ex.StatusCode);
                } catch (JsonException ex) {
                    logger.DeserializeJsonFailed(ex, requestUri, ex.LineNumber, ex.BytePositionInLine);
                }
            }
        }

        return result ?? [];
    }

    static IEnumerable<IAlertMessage> Subset(string? location, IEnumerable<Schema.NveAvalancheWarningSimple>? itr) {
        if (itr is null) {
            yield break;
        }

        // set geolocation and yield
        foreach (var item in itr) {
            if (item.Severity != AlertSeverity.None) {
                item.Location = location;
                yield return item;
            }
        }
    }
}
