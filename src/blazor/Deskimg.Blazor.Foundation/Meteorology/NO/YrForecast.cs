// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net.Http.Json;
using System.Text.RegularExpressions;
using System.Text.Json;

namespace Deskimg.Meteorology.NO;

/// <summary>
/// Represents the forecast.yr.no <see cref="IForecastProvider"/>.
/// </summary>
[Export<IForecastProvider>("forecast.yr.no")]
[Export<IForecastProvider>("forecast.yr.no/compact")]
public class YrForecast(
    ILogger<YrForecast> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IForecastProvider, ICanCacheControl, ICanSetTag {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public async Task<IEnumerable<IMeteorologyReading>> GetForecastsAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        IEnumerable<MeteorologyReading>? forecast = null;

        if (observer?.Coordinate is not null) {
            Uri? requestUri = endpoint.Fetch(Tag.Key, new {
                Latitude = observer.Coordinate.Latitude,
                Longitude = observer.Coordinate.Longitude,
                Altitude = observer.Coordinate.Altitude ?? 0.0
            }, discardQuery: true);
            if (requestUri is not null) {
                try {
                    forecast = await cache.GetOrCreateAsync($"{requestUri}", async entry => {
                        entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                        logger.FetchStarting(requestUri, CacheControl);
                        var response = await http.GetFromJsonAsync<Schema.GetYrForecastResponse>(
                            requestUri,
                            CacheControl,
                            s_sourceGenOptions,
                            cancellationToken);

                        return response is null ? null : ParseForecastResponse(observer, response);
                    });
                } catch (OperationCanceledException) {
                } catch (HttpRequestException ex) {
                    logger.FetchFailed(ex, requestUri, ex.StatusCode);
                } catch (JsonException ex) {
                    logger.DeserializeJsonFailed(ex, requestUri, ex.LineNumber, ex.BytePositionInLine);
                }
            }
        }

        return forecast ?? [];
    }

    static IEnumerable<MeteorologyReading> ParseForecastResponse(Geoposition observer, Schema.GetYrForecastResponse response) {
        var units = response.Properties.Meta.Units;
        IEnumerable<MeteorologyReading> result =
            from item in response.Properties.Timeseries
            let details = item.Data.Instant?.Details
            let next = item.Data.NextHour ?? item.Data.Next6Hours ?? item.Data.Next12Hours
            select new MeteorologyReading {
                Provider = typeof(IForecastProvider),
                Location = observer.ToString(),
                ValidFrom = item.Time,
                ValidTo = item.Time.AddHours(1),
                Temperature = details is null ? null : new() {
                    Air = new(details.AirTemperature, s_unitMapping[units.AirTemperature])
                },
                Conditions = next is null ? null : ParseCondition(next.Summary.SymbolCode),
                Precipitation = next?.Details is null ? null : new() {
                    Liquid = new(next.Details.PrecipitationAmount, s_unitMapping[units.PrecipitationAmount]),
                    IntervalMinimum = new(next.Details.PrecipitationAmountMin, s_unitMapping[units.PrecipitationAmount]),
                    IntervalMaximum = new(next.Details.PrecipitationAmountMax, s_unitMapping[units.PrecipitationAmount])
                },
                UltravioletRadiation = details is null ? null : new() {
                    Index = details.UltravioletIndexClearSky
                },
                Wind = details is null ? null : new() {
                    FromDirection = new(details.WindFromDirection, s_unitMapping[units.WindFromDirection]),
                    Sustained = new(details.WindSpeed, s_unitMapping[units.WindSpeed]),
                    Gust = new(details.WindSpeedOfGust ?? double.NaN, s_unitMapping[units.WindSpeed])
                }
            };

        return result;
    }

    internal static WeatherConditions ParseCondition(string symbol) {

        // condition is the sum of all matching condition mappings
        WeatherConditions result = (WeatherConditions)s_conditionMapping.Sum(x =>
            (long)(Regex.Match(symbol, x.Key).Success ? x.Value : WeatherConditions.None));
        return result;
    }

    static readonly Dictionary<string, string> s_unitMapping = new() {
        ["celsius"] = "°C",
        ["mm"] = "mm",
        ["degrees"] = "°",
        ["m/s"] = "m/s"
    };

    static readonly Dictionary<string, WeatherConditions> s_conditionMapping = new() {
        ["^clearsky"] = WeatherConditions.ClearSky,
        ["^fair"] = WeatherConditions.Fair,
        ["^partlycloudy"] = WeatherConditions.PartlyCloudy,
        ["^cloudy$"] = WeatherConditions.Cloudy,
        ["^fog$"] = WeatherConditions.Fog,
        ["^light"] = WeatherConditions.Light,
        ["^heavy"] = WeatherConditions.Heavy,
        ["rain"] = WeatherConditions.Rain,
        ["snow"] = WeatherConditions.Snow,
        ["sleet"] = WeatherConditions.Rain | WeatherConditions.Snow,
        ["showers"] = WeatherConditions.Showers,
        ["thunder"] = WeatherConditions.Thunderstorm,
        ["_night$"] = WeatherConditions.Night,
        ["_polartwilight$"] = WeatherConditions.PolarNight,
    };
}
