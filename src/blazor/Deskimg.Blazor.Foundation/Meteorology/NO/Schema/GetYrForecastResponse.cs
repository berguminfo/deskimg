// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see href="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/forecast.yr.no.md"/>

using System.Text.Json.Serialization;

namespace Deskimg.Meteorology.NO.Schema;

record GetYrForecastResponse(YrPropertiesObject Properties);

record YrPropertiesObject(YrMetaObject Meta, IImmutableList<YrTimeserieObject> Timeseries);

record YrMetaObject(DateTimeOffset UpdatedAt, YrUnitsObject Units);

record YrUnitsObject(
    string AirPressureAtSeaLevel,
    string AirTemperature,
    string CloudAreaFraction,
    string PrecipitationAmount,
    string RelativeHumidity,
    string WindFromDirection,
    string WindSpeed
);

record YrTimeserieObject(DateTimeOffset Time, YrTimeserieDataObject Data);

sealed class YrTimeserieDataObject {

    public YrInstantObject? Instant { get; set; }

    [JsonPropertyName("next_1_hours")]
    public YrNextNHoursObject? NextHour { get; set; }

    [JsonPropertyName("next_6_hours")]
    public YrNextNHoursObject? Next6Hours { get; set; }

    [JsonPropertyName("next_12_hours")]
    public YrNextNHoursObject? Next12Hours { get; set; }
}


record YrInstantObject(YrInstantDetailsObject? Details);

record YrInstantDetailsObject(
    double AirPressureAtSeaLevel,
    double AirTemperature,
    double CloudAreaFraction,
    double CloudAreaFractionHigh,
    double CloudAreaFractionLow,
    double CloudAreaFractionMedium,
    double DewPointTemperature,
    double RelativeHumidity,
    double WindFromDirection,
    double WindSpeed,
    double? WindSpeedOfGust,
    double? UltravioletIndexClearSky
);

record YrNextNHoursObject(YrNextSummaryObject Summary, YrNextNHoursDetailsObject Details);

record YrNextSummaryObject(string SymbolCode);

record YrNextNHoursDetailsObject(
    double PrecipitationAmount,
    double PrecipitationAmountMax,
    double PrecipitationAmountMin,
    double ProbabilityOfPrecipitation);
