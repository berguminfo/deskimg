// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see ref="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/landslide.nve.no.md"/>

using System.Text.Json.Serialization;

namespace Deskimg.Meteorology.NO.Schema;

using Deskimg.Syndication;

sealed class NveLandslideWarning : IAlertMessage {

    public required string Id { get; set; }

    public string? ActivityLevel { get; set; }

    public string? MainText { get; set; }

    public DateTimeOffset ValidFrom { get; set; }

    public DateTimeOffset ValidTo { get; set; }

    public string? DangerTypeName { get; set; }

    public IImmutableList<CauseObject>? CauseList { get; set; }

    public IImmutableList<CountyObject>? CountyList { get; set; }

    public IImmutableList<MunicipalityObject>? MunicipalityList { get; set; }

    #region IAlertMessage

    [JsonIgnore]
    public string? Location { get; set; }

    [JsonIgnore]
    public AlertSeverity Severity {
        get {
            if (ActivityLevel is string activityLevel && s_severityMapping.TryGetValue(activityLevel, out var severity)) {
                return severity;
            } else {
                return AlertSeverity.None;
            }
        }
    }

    [JsonIgnore]
    public AlertKind Kind {
        get {
            if (Severity == AlertSeverity.Extreme) {
                return AlertKind.Extreme;
            } else
            if (CauseList?.Count > 0 && s_causeMapping.TryGetValue(CauseList[0].Id, out AlertKind value)) {
                return value;
            } else {
                return AlertKind.Landslide;
            }
        }
    }

    [JsonIgnore]
    public SyndicationItem Message {
        get {
            try {
                return new() {
                    Id = $"landslide.nve.no/{Id}",
                    Title = MessageFormatter.Format(SR.Default.WeatherWarningFormat, new {
                        Future = ValidFrom.Date > DateTimeOffset.Now.Date.AddDays(1).AddSeconds(-1) ? 1 : 0,
                        Severity = (int)Severity,
                        Title = DangerTypeName ?? string.Empty,
                        Location = (MunicipalityList?[0].Name) ?? (CountyList?[0].Name) ?? string.Empty
                    }),
                    Summary = MainText
                };
            } catch (MessageFormatterException ex) {
                return new() {
                    Id = $"landslide.nve.no/{Id}",
                    Title = $"{nameof(NveLandslideWarning)}: {ex.Message}. Severity: {Severity}."
                };
            }
        }
    }

    #endregion

    static readonly Dictionary<string, AlertSeverity> s_severityMapping = new() {
        //["1"] = WarningSeverity.Green,
        ["2"] = AlertSeverity.Yellow,
        ["3"] = AlertSeverity.Orange,
        ["4"] = AlertSeverity.Red
    };

    static readonly Dictionary<int, AlertKind> s_causeMapping = new() {
        [0] = AlertKind.Generic,   // Ikke gitt
        [1] = AlertKind.Landslide, // Regn
        [2] = AlertKind.Landslide, // Intens regn (bygenedbør)
        [3] = AlertKind.Landslide, // Snøsmelting
        [4] = AlertKind.Landslide, // Isgang
        [5] = AlertKind.Landslide, // Frost og is
        [6] = AlertKind.Landslide, // Vannmetning (i jord)
        [7] = AlertKind.Landslide  // Dambrudd/jøkulhlaup
    };
}
