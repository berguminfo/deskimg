// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see href="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/traffic-info.vegvesen.no.md"/>

using System.Text.Json.Serialization;

namespace Deskimg.Meteorology.NO.Schema;

using Deskimg.Syndication;

record GetTrafficInformationResponse(IImmutableList<TrafficMessage> TrafficMessages);

sealed class TrafficMessage : IAlertMessage {

    public required string Id { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter<AlertKind>))]
    public AlertKind Type { get; set; }

    public string? DescriptionOfLocation { get; set; }

    public string? DescriptionOfTrafficMessage { get; set; }

    public bool IsActiveNow { get; set; }

    public DateTimeOffset StartTime { get; set; }

    public DateTimeOffset EstimatedEndTime { get; set; }

    #region IWarningMessage

    [JsonIgnore]
    public string? Location { get; set; }

    [JsonIgnore]
    public DateTimeOffset ValidFrom => StartTime;

    [JsonIgnore]
    public DateTimeOffset ValidTo => EstimatedEndTime;

    [JsonIgnore]
    public AlertKind Kind => Type;

    [JsonIgnore]
    public AlertSeverity Severity => IsActiveNow ? AlertSeverity.Active : AlertSeverity.Inactive;

    [JsonIgnore]
    public SyndicationItem Message => new() {
        Id = $"traffic-info.vegvesen.no/{Id}",
        Title = DescriptionOfLocation,
        Summary = DescriptionOfTrafficMessage?.Replace('|', '\n')
    };

    #endregion
}
