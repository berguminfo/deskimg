// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace Deskimg.Meteorology.NO.Schema;

public record CauseObject(int Id, string Name);

public record CountyObject(string Id, string Name);

public record MunicipalityObject(string Id, string Name);

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
