// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// <see ref="https://gitlab.com/berguminfo/deskimg/-/blob/main/docs/articles/external/avalance.nve.no.md"/>

using System.Text.Json.Serialization;

namespace Deskimg.Meteorology.NO.Schema;

using Deskimg.Syndication;

sealed class NveAvalancheWarningSimple : IAlertMessage {

    public required int RegId { get; set; }

    public string? RegionName { get; set; }

    public string? DangerLevel { get; set; }

    public string? MainText { get; set; }

    public DateTimeOffset ValidFrom { get; set; }

    public DateTimeOffset ValidTo { get; set; }

    #region IAlertMessage

    [JsonIgnore]
    public string? Location { get; set; }

    [JsonIgnore]
    public AlertSeverity Severity {
        get {
            if (DangerLevel is string dangerLevel && s_severityMapping.TryGetValue(dangerLevel, out var severity)) {
                return severity;
            } else {
                return AlertSeverity.None;
            }
        }
    }

    [JsonIgnore]
    public AlertKind Kind =>
        Severity == AlertSeverity.Extreme ? AlertKind.Extreme : AlertKind.Avalanches;

    [JsonIgnore]
    public SyndicationItem Message {
        get {
            try {
                return new() {
                    Id = $"avalanche.nve.no/{RegId}",
                    Title = MessageFormatter.Format(SR.Default.WeatherWarningFormat, new {
                        Future = ValidFrom.Date > DateTimeOffset.Now.Date.AddDays(1).AddSeconds(-1) ? 1 : 0,
                        Severity = (int)Severity,
                        Title = SR.Default.Avalanche,
                        Location = RegionName
                    }),
                    Summary = MainText
                };
            } catch (MessageFormatterException ex) {
                Console.WriteLine("{0}. Pattern: {1}. Severity: {2}.", ex.Message, SR.Default.WeatherWarningFormat, (int)Severity);
                return new() {
                    Id = $"avalanche.nve.no/{RegId}",
                    Title = $"{nameof(NveAvalancheWarningSimple)}: {ex.Message}. Severity: {Severity}."
                };
            }
        }
    }

    #endregion

    static readonly Dictionary<string, AlertSeverity> s_severityMapping = new() {
        //["1"] = WarningSeverity.Green,
        //["2"] = WarningSeverity.Yellow,
        ["3"] = AlertSeverity.Orange,
        ["4"] = AlertSeverity.Red,
        ["5"] = AlertSeverity.Extreme
    };
}
