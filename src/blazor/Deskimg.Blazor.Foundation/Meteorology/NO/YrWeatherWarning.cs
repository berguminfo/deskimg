// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Deskimg.Meteorology.NO;

using Deskimg.Syndication;

/// <summary>
/// Represents the wrn.yr.no <see cref="IAlertProvider"/>.
/// </summary>
[Export<IAlertProvider>("wrn.yr.no")]
public class YrWeatherWarning(
    ILogger<YrWeatherWarning> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IAlertProvider, ICanCacheControl, ICanSetTag {

    const string NamespaceURI = "urn:oasis:names:tc:emergency:cap:1.2";

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    /// <remarks>
    /// <see href="https://api.met.no/weatherapi/metalerts/1.1/documentation"/>.
    /// <para>
    /// Do not download each CAP file more than once, and certainly not every time
    /// you download the RSS feed! (this is liable to getting you throttled).
    /// </para>
    /// <para>
    /// Once issued the CAP file never changes, so it should be downloaded only once
    /// and stored locally.
    /// </para>
    /// </remarks>
    public CacheControlHeaderValue CapCacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromDays(7)
    };

    /// <inheritdoc/>
    public async Task<IEnumerable<IAlertMessage>> GetAlertsAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        IEnumerable<IAlertMessage>? result = null;

        if (observer?.Coordinate is not null) {
            Uri? requestUri = endpoint.Fetch(Tag.Key, new {
                // - cap:language and request query differ in language identifier
                // - request expects 'no' or 'en'
                // - cap:language results in 'no or 'en-GB'
                Language = SR.Default.EndpointOptions[Tag.Key],
                Latitude = observer.Coordinate.Latitude,
                Longitude = observer.Coordinate.Longitude
            }, discardQuery: true);
            if (requestUri is not null) {
                try {
                    result = await cache.GetOrCreateAsync(requestUri, async entry => {
                        entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                        logger.FetchStarting(requestUri, CacheControl);
                        using var stream = await http.GetStreamAsync(
                            requestUri,
                            CacheControl,
                            cancellationToken);

                        SyndicationFeed feed = new();
                        await feed.LoadAsync(stream, cancellationToken);
                        if (feed.Items is not null) {
                            var result = FetchCapWarningsAsync(observer, feed.Items, cancellationToken);
                            return await result.ToListAsync(cancellationToken);
                        } else {
                            return null;
                        }
                    });
                } catch (OperationCanceledException) {
                } catch (HttpRequestException ex) {
                    logger.FetchFailed(ex, requestUri, ex.StatusCode);
                } catch (XmlException ex) {
                    logger.DeserializeXmlFailed(ex, requestUri, ex.LineNumber, ex.LinePosition);
                }
            }
        }

        return result ?? [];
    }

    async IAsyncEnumerable<IAlertMessage> FetchCapWarningsAsync(
        Geoposition observer,
        IEnumerable<SyndicationItem> itr,
        [EnumeratorCancellation] CancellationToken cancellationToken) {

        foreach (var item in itr) {
            if (!string.IsNullOrEmpty(item.Link)) {
                Uri? requestUri = endpoint.Fetch(item.Link);
                if (requestUri is not null) {
                    logger.FetchStarting(requestUri);
                    using var stream = await http.GetStreamAsync(
                        requestUri,
                        CapCacheControl,
                        cancellationToken);

                    if (stream is not null) {
                        var document = await XDocument.LoadAsync(stream, LoadOptions.None, cancellationToken);

                        AlertMessage result = new() {
                            Location = observer.ToString(),
                            Message = item
                        };
                        ParseCap12(document, result);
                        yield return result;
                    }
                }
            }
        }
    }

    void ParseCap12(XDocument document, AlertMessage message) {
        var nameTable = new NameTable();
        var namespaceManager = new XmlNamespaceManager(nameTable);
        namespaceManager.AddNamespace("cap", NamespaceURI);

        // override with cap:info elements
        var infoElement = document.XPathSelectElement($"//cap:info[cap:language='{SR.Default.EndpointOptions[$"{Tag.Key}/cap:language"]}']", namespaceManager);
        if (infoElement is null) {
            logger.ParseCapFailed(SR.Default.EndpointOptions[Tag.Key].ToString());
            return;
        }

        // Title is cap:headline
        string? headlineValue = infoElement.XPathSelectElement("cap:headline", namespaceManager)?.Value;
        if (headlineValue is not null) {

            // take first statement
            int index = headlineValue.IndexOf(',', StringComparison.Ordinal);
            message.Message.Title = headlineValue.Substring(0, index > 0 ? index : headlineValue.Length);
        }

        // Summary is cap:description
        string? descriptionValue = infoElement.XPathSelectElement("cap:description", namespaceManager)?.Value;
        if (descriptionValue is not null) {
            message.Message.Summary = descriptionValue;
        }

        // ValidityStart is <s>cap:effective</s> 2024-10-29: it's -> cap:onset
        // ValidityEnd is cap:expires
        string? effectiveValue = infoElement.XPathSelectElement("cap:onset", namespaceManager)?.Value;
        if (DateTimeOffset.TryParse(effectiveValue, out var validityStart)) {
            message.ValidFrom = validityStart;

            string? expiresValue = infoElement.XPathSelectElement("cap:expires", namespaceManager)?.Value;
            if (DateTimeOffset.TryParse(expiresValue, out var validityEnd)) {
                message.ValidTo = validityEnd;
            }
        }

        // Severity is cap:severity
        string? severityValue = infoElement.XPathSelectElement("cap:severity", namespaceManager)?.Value;
        if (s_severityMapping.TryGetValue(severityValue?.ToUpperInvariant() ?? string.Empty, out var severity)) {
            message.Severity = severity;

            // look for bg-severity-extreme
            if (message.Severity == AlertSeverity.Red &&
                infoElement.XPathSelectElement("cap:parameter[cap:valueName='incidentName']/cap:value", namespaceManager)?.Value is string _) {
                message.Severity = AlertSeverity.Extreme;
            }
        }

        // Kind is cap:eventCode
        string? eventCode = infoElement.XPathSelectElement("cap:eventCode[cap:valueName='eventType']/cap:value", namespaceManager)?.Value;
        if (s_eventCodeMapping.TryGetValue(eventCode?.ToUpperInvariant() ?? string.Empty, out var value)) {
            message.Kind = value;
        } else {
            message.Kind = AlertKind.Generic;
        }
    }

    // MET-report 20-2017 pg. 19 (Table 5.2)
    static readonly Dictionary<string, AlertSeverity> s_severityMapping = new() {
        ["MINOR"] = AlertSeverity.Green,
        ["MODERATE"] = AlertSeverity.Yellow,
        ["SEVERE"] = AlertSeverity.Orange,
        ["EXTREME"] = AlertSeverity.Red
    };

    // MET-report 20-2017 pg. 13 (Table 4)
    // https://api.met.no/weatherapi/metalerts/1.1/documentation
    static readonly Dictionary<string, AlertKind> s_eventCodeMapping = new() {
        ["AVALANCHES"] = AlertKind.Avalanches,
        ["BLOWINGSNOW"] = AlertKind.Snow,
        ["DRIVINGCONDITIONS"] = AlertKind.DrivingConditions,
        ["FLOOD"] = AlertKind.Flood,
        ["FORESTFIRE"] = AlertKind.ForestFire,
        ["GALE"] = AlertKind.Wind,
        ["GENERIC"] = AlertKind.Generic,
        ["ICE"] = AlertKind.Ice,
        ["ICING"] = AlertKind.Ice,
        ["LANDSLIDE"] = AlertKind.Landslide,
        ["LIGHTNING"] = AlertKind.Lightning,
        ["POLARLOW"] = AlertKind.PolarLow,
        ["RAIN"] = AlertKind.Rain,
        ["RAINFLOOD"] = AlertKind.RainFlood,
        ["SNOW"] = AlertKind.Snow,
        ["STORMSURGE"] = AlertKind.StormSurge,
        ["WIND"] = AlertKind.Wind
    };
}
