// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json;

namespace Deskimg.Meteorology.NO;

/// <summary>
/// Represents the traffic-info.vegvesen.no <see cref="IAlertProvider"/>.
/// </summary>
[Export<IAlertProvider>("traffic-info.vegvesen.no")]
public class TrafficInformation(
    ILogger<TrafficInformation> logger,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IAlertProvider, ICanCacheControl, ICanSetTag {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <inheritdoc/>
    public required CacheControlHeaderValue CacheControl { get; set; }

    /// <inheritdoc/>
    public required KeyValuePair<string, object?> Tag { get; set; }

    /// <inheritdoc/>
    public async Task<IEnumerable<IAlertMessage>> GetAlertsAsync(
        DateTimeOffset rangeStart,
        TimeSpan rangeLength,
        Geoposition observer,
        CancellationToken cancellationToken = default) {

        IList<IAlertMessage>? result = null;

        Uri? requestUri = endpoint.Fetch(Tag.Key, new {
            Query = Tag.Value
        }, discardQuery: true);
        if (requestUri is not null) {
            try {
                result = await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    logger.FetchStarting(requestUri, CacheControl);
                    var response = await http.GetFromJsonAsync<Schema.GetTrafficInformationResponse>(
                        requestUri,
                        CacheControl, [
                            // required forward headers (the 'X--' headers):
                            // - Accept
                            // - X-System-ID
                            new("X--Accept", "application/vnd.svv.v1+json; charset=utf-8"),
                            new("X--X-System-ID", "vvtraf")
                        ],
                        s_sourceGenOptions,
                        cancellationToken);

                    return Subset(observer.ToString(), response?.TrafficMessages).ToList();
                });
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, requestUri, ex.StatusCode);
            } catch (JsonException ex) {
                logger.DeserializeJsonFailed(ex, requestUri, ex.LineNumber, ex.BytePositionInLine);
            }
        }

        return result ?? [];
    }

    static IEnumerable<IAlertMessage> Subset(string? location, IEnumerable<Schema.TrafficMessage>? itr) {
        if (itr is null) {
            yield break;
        }

        // set observer and yield
        foreach (var item in itr) {
            item.Location = location;
            yield return item;
        }
    }
}
