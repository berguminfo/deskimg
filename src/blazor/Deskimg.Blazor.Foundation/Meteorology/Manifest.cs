// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net.Http.Json;
using System.Text;

namespace Deskimg.Meteorology;

using GetManifestResult = IImmutableDictionary<WeatherConditions, string>;

/// <summary>
/// Represents the weather condition manifest.
/// </summary>
public sealed class Manifest(
    HttpClient http) {

    /// <summary>
    /// Gets the weather manifest from the given resource.
    /// </summary>
    /// <param name="requestUri">The resource URI.</param>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns>The async result.</returns>
    public Task<GetManifestResult?> GetAsync(Uri requestUri, CancellationToken cancellationToken) {
        if (requestUri.LocalPath.EndsWith(".json", StringComparison.OrdinalIgnoreCase)) {
            return GetFromJsonAsync(requestUri, cancellationToken);
        } else {
            return GetFromBinaryAsync(requestUri, cancellationToken);
        }
    }

    Task<GetManifestResult?> GetFromJsonAsync(Uri requestUri, CancellationToken cancellationToken) {
        return http.GetFromJsonAsync<GetManifestResult>(requestUri, cancellationToken);
    }

    async Task<GetManifestResult?> GetFromBinaryAsync(Uri requestUri, CancellationToken cancellationToken) {
        Dictionary<WeatherConditions, string> result = [];
        using var stream = await http.GetStreamAsync(requestUri, cancellationToken);
        using (BinaryReader binaryReader = new(stream, Encoding.UTF8)) {
            if (binaryReader.ReadInt16() != 0x4C44) {
                throw new InvalidDataException("Invalid manifest signature, expected 'DL'.");
            }

            switch (binaryReader.ReadInt16()) {

                // v7.7
                case 0x0707:
                    while (binaryReader.PeekChar() != -1) {
                        var key = binaryReader.ReadInt32();
                        var value = binaryReader.ReadString();
                        result[(WeatherConditions)key] = value;
                    }
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }

        return result.ToImmutableDictionary();
    }
}
