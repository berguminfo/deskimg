// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

using Deskimg.Syndication;

/// <summary>
/// Represents the default implementation of <see cref="IAlertMessage"/>.
/// </summary>
public class AlertMessage : IAlertMessage {

    /// <inheritdoc/>
    public required string? Location { get; set; }

    /// <inheritdoc/>
    public DateTimeOffset ValidFrom { get; set; }

    /// <inheritdoc/>
    public DateTimeOffset ValidTo { get; set; }

    /// <inheritdoc/>
    public AlertKind Kind { get; set; } = AlertKind.None;

    /// <inheritdoc/>
    public AlertSeverity Severity { get; set; } = AlertSeverity.None;

    /// <inheritdoc/>
    public required SyndicationItem Message { get; set; }
}
