// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using NodaTime;

namespace Deskimg.Meteorology;

using Deskimg.Ephemeris;

/// <summary>
/// Represents some meteorology algorithms.
/// </summary>
public static class Algorithms {

    const WeatherConditions VariantWeatherSymbols =
        WeatherConditions.ClearSky |
        WeatherConditions.Fair |
        WeatherConditions.PartlyCloudy |
        WeatherConditions.MostlyCloudy |
        WeatherConditions.Showers;

    static readonly TimeOnly s_polarNightVariantStarts = TimeOnly.FromTimeSpan(TimeSpan.FromHours(9));
    static readonly TimeOnly s_polarNightVariantEnds = TimeOnly.FromTimeSpan(TimeSpan.FromHours(15));

    /// <summary>
    /// Sets the variant weather conditions.
    /// </summary>
    /// <param name="observation">Contains the weather condition with or without variant flags.</param>
    /// <param name="when">The observation time.</param>
    /// <param name="timeZoneId">The observation time zone.</param>
    /// <param name="solarRiseSet">The observation solar rise and set information.</param>
    public static void SetVariantConditions(
        MeteorologyReading observation,
        DateTimeOffset when,
        string timeZoneId,
        RiseSet solarRiseSet) {

        if ((observation.Conditions & VariantWeatherSymbols) == observation.Conditions &&
            DateTimeZoneProviders.Tzdb.GetZoneOrNull(timeZoneId) is DateTimeZone zone) {

            LocalDateTime localInstant =
                Instant.FromDateTimeOffset(when)
                .InZone(zone)
                .LocalDateTime;
            if (solarRiseSet.AboveHorizon) {
                // ConditionType is in correct state, keep as is and let branch be taken
            } else
            if (solarRiseSet.BelowHorizon) {
                TimeOnly time = TimeOnly.FromDateTime(localInstant.ToDateTimeUnspecified());
                if (time < s_polarNightVariantStarts || time > s_polarNightVariantEnds) {
                    observation.Conditions |= WeatherConditions.Night;
                } else {
                    observation.Conditions |= WeatherConditions.PolarNight;
                }
            } else
            if (when < solarRiseSet.Rise || when > solarRiseSet.Set) {
                observation.Conditions |= WeatherConditions.Night;
            }
        }
    }

    internal static WeatherConditions GetConditionFromPrecipitation(MeasureD precipitation, MeasureD temperature) {
        if (precipitation.Value > 0) {
            return temperature.Convert("°C").Value switch {
                <= 0.5 => WeatherConditions.Snow,
                > 1.5 => WeatherConditions.Rain,
                _ => WeatherConditions.Snow | WeatherConditions.Rain
            };
        } else {
            return WeatherConditions.None;
        }
    }
}
