// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Xml.Linq;

namespace Deskimg.Meteorology;

/// <summary>
/// Represents additional extension methods for <see cref="WindReading"/>.
/// </summary>
public static class WindSymbol {

    const string SvgNamespace = "http://www.w3.org/2000/svg";

    const float StrokeWidth = 1.5f;

    const string Stroke = "currentcolor";

    const string StrokeOpacity = "0.87";

    const int Width = 22;

    const int Height = 22;

    const float X = (Width / 2.0f) - (18.0f / 2.0f);

    const float Y = (Height / 2.0f) - (18.0f / 2.0f);

    /// <summary>
    /// Gets the wind direction image using wind direction.
    /// </summary>
    /// <param name="wind">The wind speed and direction.</param>
    /// <param name="width">The image width.</param>
    /// <param name="height">The image height.</param>
    /// <returns>The wind direction SVG image.</returns>
    public static string CreateWindArrow(this WindReading wind, string width, string height) {
        int index = 0;
        if (!wind.VariableDirections && wind.Sustained is not null) {
            index = BeaufortScale.GetIndex(wind.Sustained.Value);
        }

        XNamespace n = SvgNamespace;
        XElement g = new(n + "g",
            new XAttribute("transform", string.Format(CultureInfo.InvariantCulture,
                "rotate({0} {1} {2})",
                wind.FromDirection?.Value ?? 0.0, X + 8.5f, Y + 8.5f))
        );
        if (index > 0) {
            g.Add(new XElement(n + "path",
                new XAttribute("d", "M11.53 3l-.941 12.857L7 15l5.001 6L17 15l-3.587.857L12.471 3h-.941z"),
                new XAttribute("clip-rule", "evenodd"),
                new XAttribute("fill-rule", "evenodd"),
                new XAttribute("fill", "currentcolor")));
        }

        // SVG document
        XDocument document = new(new XElement(n + "svg",
            new XAttribute("viewBox", "0 0 24 24"),
            new XAttribute("width", width),
            new XAttribute("height", height),
            g));

        return document.ToString();
    }

    /// <summary>
    /// Gets the wind direction image using wind direction and the Beaufort scale.
    /// </summary>
    /// <param name="wind">The wind speed and direction.</param>
    /// <param name="width">The image width.</param>
    /// <param name="height">The image height.</param>
    /// <returns>The wind direction SVG image.</returns>
    public static string CreateBeaufortArrow(this WindReading wind, string width, string height) {
        int index = 0;
        if (!wind.VariableDirections && wind.Sustained is not null) {
            index = BeaufortScale.GetIndex(wind.Sustained.Value);
        }

        // outer group element
        XNamespace n = SvgNamespace;
        XElement g = new(n + "g",
            new XAttribute("transform", string.Format(CultureInfo.InvariantCulture,
                "rotate({0} {1} {2})",
                wind.FromDirection?.Value ?? 0.0, X + 8.5f, Y + 8.5f))
        );

        if (index > 0) {
            g.Add(

                // arrow head
                new XElement(n + "polygon",
                    new XAttribute("fill", Stroke),
                    new XAttribute("fill-opacity", StrokeOpacity),
                    new XAttribute("points", string.Format(CultureInfo.InvariantCulture,
                        "{0},{1} {2},{3} {4},{5}",
                        X + 12.5f, Y + 12.5f,   // upper right
                        X + 8.5f, Y + 17.0f,    // lower center
                        X + 4.5f, Y + 12.5f))), // upper left

                // arrow line
                new XElement(n + "polygon",
                    new XAttribute("fill", Stroke),
                    new XAttribute("fill-opacity", StrokeOpacity),
                    new XAttribute("points", string.Format(CultureInfo.InvariantCulture,
                        "{0},{1} {2},{3} {4},{5} {6},{7}",
                        X + 8.5 - (StrokeWidth / 2), Y + 0.0f,     // upper left
                        X + 8.5 - (StrokeWidth / 2), Y + 12.5f,    // lower left
                        X + 8.5 + (StrokeWidth / 2), Y + 12.5f,    // lower right
                        X + 8.5 + (StrokeWidth / 2), Y + 0.0f)))); // upper right
        }

        DrawBeafortSymbol(index,

            // circle
            () => {
                g.Add(new XElement(n + "ellipse",
                    new XAttribute("stroke", Stroke),
                    new XAttribute("stroke-opacity", StrokeOpacity),
                    new XAttribute("stroke-width", StrokeWidth),
                    new XAttribute("fill-opacity", "0.0"), // transparent
                    new XAttribute("cx", X + 8.5f),
                    new XAttribute("cy", Y + 8.5f),
                    new XAttribute("rx", 6.5f),
                    new XAttribute("ry", 6.5f)));
            },

            // triangle
            line => {
                g.Add(new XElement(n + "polygon",
                    new XAttribute("fill", Stroke),
                    new XAttribute("fill-opacity", StrokeOpacity),
                    new XAttribute("points", string.Format(CultureInfo.InvariantCulture,
                        "{0},{1} {2},{3} {4},{5}",
                        X + 8.5f + (StrokeWidth / 2), Y + 0.0f,     // upper left
                        X + 8.5f + 6.5f, Y + 2.0f,                  // center right
                        X + 8.5f + (StrokeWidth / 2), Y + 4.0f)))); // lower left
            },

            // line
            (line, half) => {
                float deltaY = line * (StrokeWidth + 0.5f);
                float width = 6.5f - (StrokeWidth / 2);
                if (half) {
                    width /= 2.0f;
                }

                g.Add(new XElement(n + "polygon",
                    new XAttribute("fill", Stroke),
                    new XAttribute("fill-opacity", StrokeOpacity),
                    new XAttribute("points", string.Format(CultureInfo.InvariantCulture,
                        "{0},{1} {2},{3} {4},{5} {6},{7}",
                        X + 8.5f + (StrokeWidth / 2), Y + deltaY,                       // upper left
                        X + 8.5f + (StrokeWidth / 2) + width, Y + deltaY,               // upper right
                        X + 8.5f + (StrokeWidth / 2) + width, Y + deltaY + StrokeWidth, // lower right
                        X + 8.5f + (StrokeWidth / 2), Y + deltaY + StrokeWidth))));     // lower left
            });

        // SVG document
        XDocument document = new(new XElement(n + "svg",
            new XAttribute("viewBox", string.Format(CultureInfo.InvariantCulture, "0 0 {0} {1}", Width, Height)),
            new XAttribute("width", width),
            new XAttribute("height", height),
            g));

        return document.ToString();
    }

    static void DrawBeafortSymbol(int index, Action circle, Action<int> triangle, Action<int, bool> line) {
        Bits pattern = s_beafortPatterns[index];
        if ((pattern & Bits.Circle) != 0) {
            circle();
        }

        for (int bit = 0; bit < 5; ++bit) {
            if (((int)pattern & 1 << bit) != 0) {
                bool half = (bit == 4 || ((int)pattern & 1 << bit + 1) == 0) && (pattern & Bits.Half) != 0;
                if (bit == 0 && (pattern & Bits.Triangle) != 0) {
                    triangle(bit);
                } else {
                    line(bit, half);
                }
            }
        }
    }

    [Flags]
    enum Bits : byte {
        Zero = 0b_0000_0000,
        Line1 = 0b_0000_0001,
        Line2 = 0b_0000_0010,
        Line3 = 0b_0000_0100,
        Line4 = 0b_0000_1000,
        Line5 = 0b_0001_0000,
        Half = 0b_0010_0000,     // last Line1...Line5 has half length
        Triangle = 0b_0100_0000, // Line1 is triangle instead of line
        Circle = 0b_1000_0000    // first draw the circle
    }

    static readonly Bits[] s_beafortPatterns = {
        Bits.Circle,                                                                //  0: Calm
        Bits.Zero,                                                                  //  1: Light air
        Bits.Line2 | Bits.Half,                                                     //  2: Light breeze
        Bits.Line1,                                                                 //  3: Gentle breeze
        Bits.Line1 | Bits.Line2 | Bits.Half,                                        //  4: Moderate breeze
        Bits.Line1 | Bits.Line2,                                                    //  5: Fresh breeze
        Bits.Line1 | Bits.Line2 | Bits.Line3 | Bits.Half,                           //  6: Strong breeze
        Bits.Line1 | Bits.Line2 | Bits.Line3,                                       //  7: Moderate gale
        Bits.Line1 | Bits.Line2 | Bits.Line3 | Bits.Line4 | Bits.Half,              //  8: Fresh gale
        Bits.Line1 | Bits.Line2 | Bits.Line3 | Bits.Line4 | Bits.Line5 | Bits.Half, //  9: Strong gale
        Bits.Line1 | Bits.Triangle,                                                 // 10: Storm
        Bits.Line1 | Bits.Line4 | Bits.Triangle,                                    // 11: Violent storm
        Bits.Line1 | Bits.Line4 | Bits.Line5 | Bits.Triangle                        // 12: Hurricane
    };
}
