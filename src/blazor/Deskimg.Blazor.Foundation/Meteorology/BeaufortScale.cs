// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Meteorology;

/// <summary>
/// Utilities for the beaufort scale.
/// </summary>
public static class BeaufortScale {

    /// <summary>
    /// <see href="https://en.wikipedia.org/wiki/Beaufort_scale"/>
    /// </summary>
    public enum BeaufortUnitType {

        /// <summary>
        /// The 1946 variant.
        /// </summary>
        Extended1946,

        /// <summary>
        /// The NMI-2013 variant.
        /// </summary>
        NMI2013,

        /// <summary>
        /// The 2015 variant.
        /// </summary>
        Modern2015
    }

    /// <summary>
    /// Gets the beaufort index.
    /// </summary>
    /// <param name="windSpeed">The wind speed.</param>
    /// <param name="type">The unit type. Default: Modern2015.</param>
    /// <returns>The beaufort index.</returns>
    public static int GetIndex(MeasureD windSpeed, BeaufortUnitType type = BeaufortUnitType.Modern2015) =>
        type switch {
            BeaufortUnitType.Extended1946 => GetIndexExtended1946(windSpeed),
            BeaufortUnitType.NMI2013 => GetIndexModern(windSpeed, s_beaufortScaleNMI2013),
            BeaufortUnitType.Modern2015 => GetIndexModern(windSpeed, s_beaufortScaleModern2015),
            _ => throw new InvalidOperationException(),
        };

    /// <summary>
    /// Gets the beaufort number according to the 1946 definition.
    /// </summary>
    /// <remarks>>
    /// According to http://oceanworld.tamu.edu/resources/ocng_textbook/chapter04/chapter04_04.htm
    /// Given:
    ///   v = wind speed, v >= 0
    ///   B = beaufort index
    /// Then:
    ///   v = 0.836 × B^(3/2)
    /// Hence:
    ///   B = 25 × 2^(2/3) × v^(2/3) / 209^(2/3)
    ///   B=25\times{2}^{\frac{2}{3}}\times{\frac{{v}^{\frac{2}{3}}}{{209}^{\frac{2}{3}}}}
    /// </remarks>
    /// <param name="windSpeed">The wind speed.</param>
    /// <returns>The beaufort index.</returns>
    static int GetIndexExtended1946(MeasureD windSpeed) {
        double value = windSpeed.Convert("m/s").Value;
        if (value < 0) {
            throw new ArgumentOutOfRangeException(nameof(windSpeed), "windSpeed < 0");
        }

        int result = (int)Math.Round(
            (25.0 * Math.Pow(2.0, 2.0 / 3.0) *
            Math.Pow(value, 2.0 / 3.0)) / Math.Pow(209.0, 2.0 / 3.0));

        return result > 12 ? 12 : result;
    }

    /// <summary>
    /// Gets the beaufort index according to a scale.
    /// </summary>
    /// <param name="windSpeed">The wind speed.</param>
    /// <param name="scale">The lookup scale.</param>
    /// <returns>The beaufort index.</returns>
    static int GetIndexModern(MeasureD windSpeed, double[] scale) {
        var value = windSpeed.Convert("m/s").Value;
        if (value < 0) {
            throw new ArgumentException("windSpeed < 0");
        }

        for (var i = 0; i < scale.Length; ++i) {
            if (value <= scale[i]) return i;
        }

        throw new InvalidOperationException(); // impossible
    }

    // measure unit: <m/s>
    static readonly double[] s_beaufortScaleNMI2013 = {
        0.2,            //  0: Calm
        1.5,            //  1: Light air
        3.3,            //  2: Light breeze
        5.4,            //  3: Gentle breeze
        7.9,            //  4: Moderate breeze
        10.7,           //  5: Fresh breeze
        13.8,           //  6: Strong breeze
        17.1,           //  7: Moderate gale
        20.7,           //  8: Fresh gale
        24.4,           //  9: Strong gale
        28.4,           // 10: Storm
        32.6,           // 11: Violent storm
        double.MaxValue // 12: Hurricane
    };

    // measure unit: <m/s>
    static readonly double[] s_beaufortScaleModern2015 = {
        0.4,            //  0: Calm
        1.5,            //  1: Light air
        3.3,            //  2: Light breeze
        5.5,            //  3: Gentle breeze
        7.9,            //  4: Moderate breeze
        10.7,           //  5: Fresh breeze
        13.8,           //  6: Strong breeze
        17.1,           //  7: Moderate gale
        20.7,           //  8: Fresh gale
        24.4,           //  9: Strong gale
        28.4,           // 10: Storm
        32.6,           // 11: Violent storm
        double.MaxValue // 12: Hurricane
    };
}
