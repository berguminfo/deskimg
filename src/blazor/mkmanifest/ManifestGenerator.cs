// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Deskimg.WeatherSymbolsManifest;

using Deskimg.Meteorology;

sealed class ManifestGenerator {

    readonly IDictionary<string, WeatherConditions> _abbreviationMapping;

    public required FileInfo Table { get; init; }

    public ManifestGenerator() {
        _abbreviationMapping = GetEnumShortName<WeatherConditions>();
    }

    /// <summary>
    /// Gets the enum short name dictionary.
    /// </summary>
    /// <typeparam name="T">Specifies the enum type.</typeparam>
    /// <returns>{ [DisplayAttribute].ShortName: GetRawConstantValue(), ... }</returns>
    static Dictionary<string, T> GetEnumShortName<T>() where T : Enum {
        var enumType = typeof(T);
        var enumTypeFields = enumType.GetFields(BindingFlags.Public | BindingFlags.Static);
        return new Dictionary<string, T>(
            from fieldInfo in enumTypeFields
            let key = fieldInfo.GetCustomAttributes<DisplayAttribute>(false).FirstOrDefault()?.ShortName
            let value = fieldInfo.GetRawConstantValue()
            where key is not null && value is not null
            select new KeyValuePair<string, T>(key, (T)value));
    }

    /// <summary>
    /// Parses the weather condition abbreviations into the similar <see cref="WeatherConditions"/>.
    /// </summary>
    /// <param name="input">The input abbreviations.</param>
    /// <returns>The sum of all WeatherConditionReadings matching a weather condition abbreviation.</returns>
    public WeatherConditions ParseAbbreviations(string input) =>
        (WeatherConditions)_abbreviationMapping.Sum(x =>
            input.Contains(x.Key, StringComparison.OrdinalIgnoreCase) ? (int)x.Value : 0);

    /// <summary>
    /// Gets the weather condition alternatives.
    /// </summary>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns>Weather condition alternatives.</returns>
    /// <exception cref="FileNotFoundException">Failed to load the alternatives table.</exception>
    async ValueTask<Dictionary<WeatherConditions, WeatherConditions>> GetWeatherConditionReadingAlternativesAsync(CancellationToken cancellationToken) {
        using var stream = Table.OpenRead();
        if (stream is null) {
            throw new FileNotFoundException(Table.FullName);
        }

        Dictionary<WeatherConditions, WeatherConditions> result = [];

        using var reader = new StreamReader(stream, Encoding.UTF8);
        while (!reader.EndOfStream) {
            cancellationToken.ThrowIfCancellationRequested();

            string? line = await reader.ReadLineAsync(cancellationToken);
            if (line is not null && !line.TrimStart().StartsWith('#')) {
                string[] cols = Regex.Split(line, @"\s+");
                if (cols.Length >= 4) {
                    result[ParseAbbreviations(cols[2])] = ParseAbbreviations(cols[3]);
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Generates the weather symbols manifest.
    /// </summary>
    /// <param name="presentSymbols">The present symbols.</param>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns>The generated manifest.</returns>
    public async ValueTask<IDictionary<WeatherConditions, string>> GenerateManifestAsync(
        IEnumerable<FileInfo> presentSymbols, CancellationToken cancellationToken) {

        // find all available weather symbols
        Dictionary<WeatherConditions, string> result = presentSymbols.ToDictionary(
            x => ParseAbbreviations(Path.GetFileNameWithoutExtension(x.Name)),
            x => x.Name
        );

        // next append all missing symbols given the alternatives table
        var alternatives = await GetWeatherConditionReadingAlternativesAsync(cancellationToken);
        if (result.Count < alternatives.Count) {
            foreach (var item in alternatives) {
                cancellationToken.ThrowIfCancellationRequested();

                if (!result.ContainsKey(item.Key)) {
                    result[item.Key] = WalkAlternatives(item.Key, result, alternatives);
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Walks the alternatives table to find a matching <see cref="WeatherConditions"/>.
    /// </summary>
    /// <param name="WeatherConditionReading">The condition type to find.</param>
    /// <param name="presents">The present weather symbols.</param>
    /// <param name="alternatives">The alternatives table.</param>
    /// <returns>The matching <see cref="WeatherConditions"/>.</returns>
    /// <exception cref="InvalidOperationException">Nothing found.</exception>
    static string WalkAlternatives(
        WeatherConditions WeatherConditionReading,
        IDictionary<WeatherConditions, string> presents,
        IDictionary<WeatherConditions, WeatherConditions> alternatives) {

        if (presents.TryGetValue(WeatherConditionReading, out var presentValue)) {
            return presentValue;
        } else
        if (alternatives.TryGetValue(WeatherConditionReading, out var alternativeValue)) {
            return WalkAlternatives(alternativeValue, presents, alternatives);
        } else {
            throw new InvalidOperationException($"No symbol found matching '{WeatherConditionReading}'");
        }
    }
}
