// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.CommandLine;
using System.Text;
using System.Text.Json;
using System.Text.Encodings.Web;
using Deskimg.WeatherSymbolsManifest;

var tableOption = new Option<FileInfo>(
    name: "--table",
    description: "specifies the table file",
    getDefaultValue: () => new("table.prn"));
var inputOption = new Option<DirectoryInfo>(
    name: "--in",
    description: "read from directory",
    getDefaultValue: () => new("."));
var outputOption = new Option<FileInfo?>(
    name: "--out",
    description: "write to file instead of stdout");
var filterOption = new Option<string>(
    name: "--filter",
    description: "search pattern",
    getDefaultValue: () => "*.svg");
var binaryOption = new Option<bool>(
    name: "--binary",
    description: "format as binary");
var binaryFullOption = new Option<bool>(
    name: "--binary-full",
    description: "format as binary including name of resource");

var rootCommand = new RootCommand("generates the weather symbols manifest") {
    tableOption,
    inputOption,
    outputOption,
    filterOption,
    binaryOption,
    binaryFullOption
};

rootCommand.SetHandler(async (table, input, output, filter, binary, binaryFull) => {
    ManifestGenerator template = new() {
        Table = table
    };
    var presentSymbols = input.EnumerateFiles(filter, SearchOption.TopDirectoryOnly);
    var manifest = await template.GenerateManifestAsync(presentSymbols, default);

    if (output is not null && (binary || binaryFull)) {
        using var stream = File.Create(output.FullName);
        using (BinaryWriter binaryWriter = new(stream, Encoding.UTF8)) {
            int signature =
                Convert.ToInt32(ThisAssembly.Git.BaseVersion.Minor, null) << 24 |
                Convert.ToInt32(ThisAssembly.Git.BaseVersion.Major, null) << 16 |
                Convert.ToInt32('L') << 8 | // Little endian
                Convert.ToInt32(nameof(Deskimg)[0]);
            binaryWriter.Write(signature);
            foreach (var item in manifest) {
                binaryWriter.Write((int)item.Key);
                if (binaryFull) {
                    binaryWriter.Write(Path.GetFileNameWithoutExtension(item.Value));
                }
            }
        }
    } else
    if (output is not null) {
        var contents = JsonSerializer.Serialize(manifest.ToDictionary(x => (int)x.Key, x => x.Value), new JsonSerializerOptions {
            Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
            WriteIndented = true
        });

        await File.WriteAllTextAsync(output.FullName, contents);
    } else {
        Console.WriteLine("{0,-54} {1}", "Name", "Value");
        Console.WriteLine("{0,-54} {1}", "----", "-----");
        foreach (var item in manifest.OrderBy(x => x.Key)) {
            Console.WriteLine("{0,-54} {1}", item.Key, item.Value);
        }
    }
}, tableOption, inputOption, outputOption, filterOption, binaryOption, binaryFullOption);

await rootCommand.InvokeAsync(args);
