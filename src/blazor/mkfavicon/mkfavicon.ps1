`                                                                    #\
$SOURCE = $args[0]                                                   #\
mkdir obj -ErrorAction SilentlyContinue                             <#\
`
SOURCE=$1
mkdir -p obj
                                                                     #>

magick $SOURCE -resize 128x128 obj/favicon80.png
magick $SOURCE -resize 32x32 obj/favicon20.png
magick $SOURCE -resize 16x16 obj/favicon10.png
magick obj/favicon80.png obj/favicon20.png obj/favicon10.png obj/favicon.ico
