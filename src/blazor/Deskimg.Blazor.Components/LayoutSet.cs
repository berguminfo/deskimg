// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA2227 // Collection properties should be read only
// Ignore Spelling: Geolocation

using System.Text.Json;
using System.Text.Json.Serialization;

namespace Deskimg;

/// <summary>
/// Represents the LayoutSet.schema.json.
/// </summary>
public class LayoutSet {

    const string Schema1 = "https://gitlab.com/berguminfo/deskimg/-/raw/main/public/LayoutSet.1.0.schema.json";

    /// <summary>
    /// Gets or sets the JSON schema identifier.
    /// </summary>
    [JsonPropertyName("$schema")]
    public string? Schema { get; set; } = Schema1;

    /// <summary>
    /// Gets or sets the name of this set.
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Gets or sets the description of this set.
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets the <code>Background</code> parameters.
    /// </summary>
    public BackgroundType? Background { get; set; }

    /// <summary>
    /// Gets or sets the <code>Index</code> parameters.
    /// </summary>
    public IList<IndexType>? Index { get; set; }

#if SETS_2_0
    IList<(string Class, IList<(string ContractName, JsonElement Parameters)>)>? __Index { get; set; }
#endif

    /// <summary>
    /// Gets or sets the collection of known <code>Geoposition</code>.
    /// </summary>
    // NOTE: MUST be a mutable collection type (i.e. IDictionary`2)
    // NOTE: MUST be a replaceable SetsModel property (for deserialize sets.json)
    public IDictionary<string, Geoposition> Geolocation { get; set; } = new Dictionary<string, Geoposition>();
}

/// <summary>
/// Represents the <code>Background</code> component parameters.
/// </summary>
public sealed class BackgroundType {

    /// Gets or sets a hint about the document element theme.
    public DocumentTheme Theme { get; set; }

    /// <summary>
    /// Gets or sets the background overlay options.
    /// </summary>
    public BackgroundOverlay? Overlay { get; set; }

    /// <summary>
    /// Gets or sets the background composite options.
    /// </summary>
    public IList<BackgroundComposite>? Composite { get; set; }
}

/// <summary>
/// Represent the known data-bs-theme attribute values.
/// </summary>
[JsonConverter(typeof(JsonStringEnumConverter<DocumentTheme>))]
public enum DocumentTheme {

    /// <summary>
    /// Don't set any data-bs-theme attribute.
    /// </summary>
    None,

    /// <summary>
    /// Choose the data-bs-theme attribute to "dark" or "light" depending on the lightness
    /// of the composed background image.
    /// </summary>
    Auto,

    /// <summary>
    /// Sets the data-bs-theme attribute to "light".
    /// </summary>
    Light,

    /// <summary>
    /// Sets the data-bs-theme attribute to "dark".
    /// </summary>
    Dark
}

/// <summary>
/// Represents the background overlay options.
/// </summary>
public sealed class BackgroundOverlay {

    /// <summary>
    /// Gets or sets the overlay pattern.
    /// </summary>
    public HatchPattern Pattern { get; set; }

    /// <summary>
    /// Gets or sets the pattern opacity options.
    /// </summary>
    /// <remarks>
    /// This collection should always be not null and have 2 items.
    /// </remarks>
    public IList<double>? Opacity { get; set; }

    /// <summary>
    /// Gets or sets the pattern "pixel" width.
    /// </summary>
    public int Width { get; set; } = 2;

    /// <summary>
    /// Gets or sets the pattern "pixel" height.
    /// </summary>
    public int Height { get; set; } = 2;
}

/// <summary>
/// Represents known hatch patterns.
/// </summary>
[JsonConverter(typeof(JsonStringEnumConverter<HatchPattern>))]
public enum HatchPattern {

    /// <summary>
    /// No pattern should be applied.
    /// </summary>
    None,

    /// <summary>
    /// Overlays the background with a checker board pattern.
    /// </summary>
    /// <remarks>
    /// The pattern is like:
    /// ┌───┬───┐
    /// │ 0 │ 1 │
    /// ├───┼───┤
    /// │ 1 │ 0 │
    /// └───┴───┘
    /// 0 is the first color in <see cref="BackgroundOverlay.Opacity"/> and 1 the second color,
    /// and the pattern is repeated for the entire canvas.
    /// </remarks>
    CheckerBoard
}

/// <summary>
/// Represents the background composite options.
/// </summary>
public sealed class BackgroundComposite {

    /// <summary>
    /// Gets or sets the composition starting index.
    /// </summary>
    public double From { get; set; } = -1.0;

    /// <summary>
    /// Gets or sets the composition ending index.
    /// </summary>
    public double To { get; set; } = 1.0;

    /// <summary>
    /// Gets or sets composition content.
    /// </summary>
    public IList<CompositeItem>? Items { get; set; }
}

/// <summary>
/// Represents a composition item.
/// </summary>
public sealed class CompositeItem {

    /// <summary>
    /// Gets or sets the source replacement <see cref="MessageFormatter"/>.
    /// </summary>
    /// <remarks>
    /// Updated by the Background component before rendering, using the replacement patterns:
    /// <list type="bullet">
    /// <item><code>{Local}</code> replaced with the value of the <code>Href:Background</code> appsetting.</item>
    /// <item><code>{MaxWidth}</code> replaced with the best matching viewport with.</item>
    /// <item><code>{CalDAV}</code> replaced with the value of the <code>Endpoint:caldav.api</code> appsetting.</item>
    /// </list>
    /// </remarks>
    public string? Source { get; set; }

    /// <summary>
    /// Gets or sets the composition content type.
    /// </summary>
    public string? Type { get; set; }

    /// <summary>
    /// Gets or sets the composition media query.
    /// </summary>
    public string? Media { get; set; }

    /// <summary>
    /// Gets or sets the rendering opacity.
    /// </summary>
    public double? Opacity { get; set; }
}

/// <summary>
/// Represents a index item.
/// </summary>
public sealed class IndexType {

    /// <summary>
    /// Gets or sets the CSS 'class' attribute.
    /// </summary>
    public string? Class { get; set; }

    /// <summary>
    /// Gets or sets the index components.
    /// </summary>
    public IList<IDictionary<string, IDictionary<string, JsonElement>>>? Component { get; set; }
}

#pragma warning restore CA2227 // Collection properties should be read only
