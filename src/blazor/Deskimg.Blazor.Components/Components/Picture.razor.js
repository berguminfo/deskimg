// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

/**
 * Sets the matching element image source from remote resource.
 *
 * @param {string} selectors
 * @param {string} requestUri
 * @returns {PromiseLike<HtmlImageElement, Error>}
 */
export function setPictureBlob(selectors, requestUri) {
    return new Promise(async (resolve, reject) => {
        try {
            const image = document.querySelector(selectors);
            const response = await fetch(requestUri);
            const arrayBuffer = await response.arrayBuffer();
            const blob = new Blob([arrayBuffer]);
            const objectURL = URL.createObjectURL(blob);

            image.onload = () => {
                URL.revokeObjectURL(objectURL);
                image.classList.remove("d-none");
                resolve(image);
            };
            image.onerror = reject;

            image.src = objectURL;
        } catch (e) {
            reject(e);
        }
    });
}
