// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

import * as echarts from "../lib/echarts/echarts.esm.min.js";

/**
 * Sets the chart dataset.
 *
 * @param {string} selectors - The chart selectors.
 * @param {Float64Array} x - The local hours.
 * @param {Float64Array} y - The altitude degrees.
 */
export function chart(selectors, x, y) {
    const element = document.querySelector(selectors);
    if (element) {
        const option = {
            backgroundColor: "transparent",
            textStyle: {
                fontFamily: "sans-serif"
            },
            grid: {
                left: 40,
                top: 10,
                right: 10,
                bottom: 30
            },
            xAxis: {
                type: "category",
                data: Array.from(x)
            },
            yAxis: {
                type: "value",
                axisLabel: {
                    formatter: "{value}°"
                },
                splitNumber: 1
            },
            series: [
                {
                    data: Array.from(y),
                    type: "line",
                    smooth: true,
                    symbol: "none"
                }
            ]
        };

        let chart = echarts.getInstanceByDom(element);
        if (!chart) {
            chart = echarts.init(element, "dark", { renderer: "svg" });
            window.addEventListener("resize", () => chart.resize());
        }

        console.log("option", option);
        chart.setOption(option);
    }
}
