// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

/**
 * Posts a interop message
 *
 * @param {string} message
 */
export function postInteropMessage(message) {
    if (window.CefSharp && CefSharp.PostMessage) {
        CefSharp.PostMessage(message);
    }
}

/**
 * Sets the data-bs-theme attribute to the given theme.
 *
 * @param {string} theme
 */
export function setDocumentTheme(theme, attributeName = "data-bs-theme", selectors = "[data-background=css]") {
    const element = document.querySelector(selectors);
    if (element) {
        element.setAttribute(attributeName, theme);
        console.debug(`${attributeName} is`, element.getAttribute(attributeName));
    }
}
