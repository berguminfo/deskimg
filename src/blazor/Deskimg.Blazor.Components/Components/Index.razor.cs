// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;
using System.Text;
using Microsoft.JSInterop; // InvokeVoidAsync extension method

namespace Deskimg.Components;

/// <summary>
/// Represents the <c>@IDX</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Index))]
partial class Index(
    SessionService session,
    IJSRuntime js) : IDesklet, IDisposable {

    /// <summary>
    /// Gets the current set.
    /// </summary>
    protected LayoutSet? CurrentSet { get; set; }

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            try {
                string ascii = "JWMgIF9fX18gICAgICAgICAgICBfICAgIF8KIHwgIF8gXCAgX19fICBfX198IHwgXyhfKV8gX18gX19fICAgX18gXwogfCB8IHwgfC8gXyBcLyBfX3wgfC8gLyB8ICdfIGAgXyBcIC8gX2AgfAogfCB8X3wgfCAgX18vXF9fIFwgICA8fCB8IHwgfCB8IHwgfCAoX3wgfAogfF9fX18vIFxfX198fF9fXy9ffFxfXF98X3wgfF98IHxffFxfXywgfAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxfX18vCg==";
                await js.InvokeVoidAsync("console.info", Encoding.ASCII.GetString(Convert.FromBase64String(ascii)) + ThisAssembly.Git.InformationalVersion, "font:monospace;white-space:pre");
            } catch (Microsoft.JSInterop.JSException) { }
            await JSInterop.ImportModuleAsync();
        }

        CurrentSet = await session.GetCurrentSetAsync();

        // set document element data-bs-theme attribute if hinted by the set
        if (OperatingSystem.IsBrowser() && session.Theme is not null) {
            switch (session.Theme) {
                case DocumentTheme.Light:
                case DocumentTheme.Dark:
#pragma warning disable CA1308 // Normalize strings to uppercase
                    JSInterop.SetDocumentTheme(session.Theme.Value.ToString().ToLowerInvariant());
#pragma warning restore CA1308 // Normalize strings to uppercase
                    break;
            }
        }

        // - components should raise the OnAfterRender event to the SessionService
        // - forward the message to the CaptureWorker to trigger the final capturing stage
        if (OperatingSystem.IsBrowser()) {
            session.OnAfterRender += Session_OnAfterRender;
        }
    }

    /// <inheritdoc/>
    public void Dispose() {
        if (OperatingSystem.IsBrowser()) {
            session.OnAfterRender -= Session_OnAfterRender;
        }
    }

    void Session_OnAfterRender(object? sender, bool firstRender) {
        if (OperatingSystem.IsBrowser()) {
            JSInterop.PostInteropMessage($"Session_OnAfterRender,{sender?.GetType().Name}");
        }
    }

    [SupportedOSPlatform("browser")]
    static partial class JSInterop {

        const string ModuleName = "Components.Index";

        internal static Task<JSObject> ImportModuleAsync() =>
            JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/Components/Index.razor.js");

        [JSImport("postInteropMessage", ModuleName)]
        internal static partial void PostInteropMessage(string message);

        [JSImport("setDocumentTheme", ModuleName)]
        internal static partial void SetDocumentTheme(string value);
    }
}
