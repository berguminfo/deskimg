// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

using Deskimg.Calendar;

/// <summary>
/// Represents the <c>@CAL</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Calendar))]
partial class Calendar(
    ILogger<Calendar> logger,
    SessionService session,
    IOptions<HyperlinkOptions> linkOptions,
    IServiceProvider serviceProvider,
    CompositionService compositionService) : IDesklet, IDisposable {

    readonly List<object> _workers = [];

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected IList<SubsetResult>? ViewData { get; private set; }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.CalendarSource)]
    public IImmutableList<string> Source { get; set; } = ImmutableList<string>.Empty;

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <summary>
    /// Gets or sets the maximum number of visible items.
    /// </summary>
    [Parameter]
    public int Take { get; set; } = 4;

    /// <summary>
    /// Gets or sets the title visibility.
    /// </summary>
    [Parameter]
    public bool DisplayTitle { get; set; }

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        var intermediateWorkers =
            from item in Source
            let contract = item.Split('?', 2)
            let implementorType = (
                compositionService.GetExport<ISubsetCalendar>(contract[0]),
                compositionService.GetExport<ISubsetCalendar2>(contract[0])
            )
            where
                implementorType.Item1 is not null ||
                implementorType.Item2 is not null
            select new {
                Provider = serviceProvider.GetService(implementorType.Item1 ?? implementorType.Item2),
                Tag = new KeyValuePair<string, object?>(
                    contract[0],
                    contract.Length > 1 ? contract[1] : string.Empty)
            };
        _workers.Clear();
        foreach (var item in intermediateWorkers) {
            if (item.Provider is ICanCacheControl canCacheControl) {
                canCacheControl.CacheControl = CacheControl;
            }
            if (item.Provider is ICanSetTag canSetTag) {
                canSetTag.Tag = item.Tag;
            }

            logger.WorkerAdded(item.Provider.GetType().Name);
            _workers.Add(item.Provider);
        }

        await ExecuteAsync(session);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {

        // NOTE:
        //  - expect the number of events to be less than 10
        //  - in case the number of events was huge per day,
        //    it’s possible better to use <see cref=”IDictionary”/> instead

        var rangeStart = new DateTimeOffset(session.Toi.Date, session.Toi.Offset);
        var rangeLength = TimeSpan.FromDays(Take + 1);

        List<SubsetResult> data = [];

        foreach (var worker in _workers) {
            IEnumerable<SubsetResult>? result = null;
            if (worker is ISubsetCalendar subsetCalendar) {
                result = await subsetCalendar.SubsetAsync(rangeStart, rangeLength, cancellationToken);
            } else
            if (worker is ISubsetCalendar2 subsetCalendar2) {
                var observer = await session.GetLocationAsync(cancellationToken);
                if (observer is not null) {
                    result = await subsetCalendar2.SubsetAsync(rangeStart, rangeLength, observer, cancellationToken);
                }
            }

            if (result is not null) {
                data.AddRange(result.Take(Take));
                logger.InvokeCompleted(worker.GetType().Name, rangeStart, rangeLength, data.Count);
            }
        }

        ViewData = data;
        StateHasChanged();
    }
}
