// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

/// <summary>
/// Represents the view port media query.
/// </summary>
partial class ViewPort(SessionService session) {

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.ClippingRulesChanged += (_, _) => StateHasChanged();
    }
}
