// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

/**
 * Creates a checked hatch pattern.
 *
 * @param {number} opacity1
 * @param {number} opacity2
 * @param {number} dw
 * @param {number} dh
 * @param {number | null} width
 * @param {number | null} height
 * @returns {string}
 */
export async function createCheckerBoard(opacity1, opacity2, dw, dh, width = null, height = null) {
    if (!width) width = dw << 1;
    if (!height) height = dh << 1;

    const canvas = new OffscreenCanvas(width, height);
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (let y = 0; y < canvas.height; y += (dh << 1)) {
        for (let x = 0; x < canvas.width; x += (dw << 1)) {
            ctx.globalAlpha = opacity1;
            ctx.fillRect(x, y, dw, dh);
            ctx.fillRect(x + dw, y + dh, dw, dh);
            ctx.globalAlpha = opacity2;
            ctx.fillRect(x + dw, y, dw, dh);
            ctx.fillRect(x, y + dh, dw, dh);
        }
    }

    const blob = await canvas.convertToBlob({ type: "image/png" });
    return URL.createObjectURL(blob);
}

/**
 * Creates an image element loader.
 *
 * @param {string} url - The source url.
 * @returns {Promise<HtmlImageElement, Error>}
 */
function imageLoader(url) {
    return new Promise((resolve, reject) => {
        const image = new Image();
        image.onload = () => resolve(image);
        image.onerror = reject;
        image.crossOrigin = "anonymous";
        image.src = url;
    });
}

/**
 * Represents the composite image class.
 */
class CompositeImage {

    /**
     * @private
     * @type {Map<string, HtmlImageElement>}
     */
    _imageCache = new Map();

    /**
     * Sets the overlay image.
     *
     * @param {string} url
     */
    async setOverlay(url) {
        this._overlay = await imageLoader(url);
    }

    /**
     * Gets the background blob URL.
     *
    * @returns {Promise<string, Error>}
    */
    async getBlob(type) {
        if (!this._canvas.convertToBlob) {
            throw new Error("Canvas.convertToBlob don't exists.");
        }

        const blob = await this._canvas.convertToBlob({ type: type });
        return URL.createObjectURL(blob);
    }

    /**
     * Composite from provided images and opacity.
     *
     * @param {images: string[]} images
     * @param {opacity: number[]} opacity
     * @returns {PromiseLike<void>}
     */
    async composite(images, opacity) {

        // always update imageCache (morning->day; day; day->night; night; night->morning)
        const tasks = [];
        for (let i = 0; i < images.length; ++i) {
            if (!this._imageCache.has(images[i])) {
                tasks.push(imageLoader(images[i]).then(x => this._imageCache.set(images[i], x)))
            }
        }

        // wait to load all images before continue
        if (tasks.length > 0) {
            await Promise.all(tasks);
        }

        // create and preserve OfflineCanvas
        if (!this._canvas) {
            const width = [...this._imageCache.values()].reduce((previousValue, currentValue) =>
                previousValue > currentValue.width ? previousValue : currentValue.width, 0);
            const height = [...this._imageCache.values()].reduce((previousValue, currentValue) =>
                previousValue > currentValue.height ? previousValue : currentValue.height, 0);
            this._canvas = new OffscreenCanvas(width, height);
        }

        const ctx = this._canvas.getContext("2d");
        ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);

        // draw images
        for (let i = 0; i < images.length; ++i) {
            ctx.save();
            ctx.globalAlpha = opacity[i];
            ctx.drawImage(this._imageCache.get(images[i]), 0, 0);
            ctx.restore();
        }

        // draw overlay
        if (this._overlay) {
            ctx.save();
            ctx.fillStyle = ctx.createPattern(this._overlay, "repeat");
            ctx.fillRect(0, 0, this._canvas.width, this._canvas.height);
            ctx.restore();
        }
    }

    /**
     * Calculates the luminance (brightness) of the composed canvas.
     *
     * @param {number} sampleSize
     * @returns {number}
     */
    getLuminance(sampleSize = 128) {
        if (!this._canvas) {
            throw new Error("Call 'composite' to create the Canvas before trying 'getDocumentTheme'.");
        }

        const ctx = this._canvas.getContext("2d");
        let sum = 0;
        let count = 0;
        for (let sy = 0; sy < this._canvas.height; sy += sampleSize) {
            for (let sx = 0; sx < this._canvas.width; sx += sampleSize) {
                const sw = sx + sampleSize > this._canvas.width ? sx + sampleSize - this._canvas.width : sampleSize;
                const sh = sy + sampleSize > this._canvas.height ? sy + sampleSize - this._canvas.height : sampleSize;
                const data = ctx.getImageData(sx, sy, sw, sh).data;

                // take measurement on the center pixel on each square, witch is at:
                // (((sh/2)*sw)−(sw/2))*4 = 2*sw*(sh-1)
                const i = 2 * sw * (sh - 1);
                let r = data[i];
                let g = data[i + 1];
                let b = data[i + 2];

                // adjust rgb
                r *= 0.2126;
                g *= 0.7152;
                b *= 0.0722;

                // calculate average components
                sum += r + g + b;
                count++;

                //console.debug(count, { len: data.length, i: i, rgb: `${Math.floor(r)},${Math.floor(g)},${Math.floor(b)}`, sum: sum, avg: sum / (count * 0xff) });
            }
        }

        // luminance is the average of all samples in the range [0..1]
        return sum / (count * 0xff);
    }
}

const backgroundObject = new CompositeImage();

/**
 * @see CompositeImage
 */
export async function setBackgroundOverlay(url) {
    await backgroundObject.setOverlay(url);
}

/**
 * @see CompositeImage
 */
export async function composite(images, opacity) {
    await backgroundObject.composite(images, opacity);
}

/**
 * @see CompositeImage
 */
export async function getBackgroundBlob(type = "image/jpeg") {
    return await backgroundObject.getBlob(type);
}

/**
 * Sets the matching element backgroundImage.
 *
 * @param {string} selectors
 * @param {string} url
 */
export function setBackgroundImage(url, selectors = "[data-background=css]") {
    const element = document.querySelector(selectors);
    if (element) {
        element.style.backgroundImage = `url(${url})`;
    }
}

const DEFAULT_THEMES = {
    dark: [0.0, 0.55],
    light: [0.55, 1.0]
};

/**
 * @see CompositeImage
 */
export function setDocumentTheme() {
    const luminance = backgroundObject.getLuminance();
    console.debug("luminance is", luminance);
    for (const [theme, range] of Object.entries(DEFAULT_THEMES)) {
        if (range[0] <= luminance && luminance <= range[1]) {
            setDocumentThemeInternal(theme);
            break;
        }
    }
}

/**
 * Sets the data-bs-theme attribute to the given theme.
 *
 * @param {string} theme
 */
export function setDocumentThemeInternal(theme, attributeName = "data-bs-theme", selectors = "[data-background=css]") {
    const element = document.querySelector(selectors);
    if (element) {
        element.setAttribute(attributeName, theme);
        console.debug(`${attributeName} is`, element.getAttribute(attributeName));
    }
}
