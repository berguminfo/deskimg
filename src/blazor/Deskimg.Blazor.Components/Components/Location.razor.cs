// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;

namespace Deskimg.Components;

using Deskimg.Calendar;
using Deskimg.Ephemeris;

/// <summary>
/// Represents the <c>@LOC</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Location))]
partial class Location(
    ILogger<Location> logger,
    SessionService session,
    IServiceProvider serviceProvider,
    CompositionService compositionService) : IDesklet, IDisposable {

    readonly List<ISubsetCalendar2> _workers = [];

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected (string? Title, IList<IEnumerable<SubsetResult>> Data)? ViewData { get; private set; }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.LocationSource)]
    public IImmutableList<string> Source { get; set; } = ImmutableList<string>.Empty;

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <summary>
    /// Gets or sets the maximum number of visible items.
    /// </summary>
    [Parameter]
    public int Take { get; set; } = 2;

    /// <summary>
    /// Gets or sets the title visibility.
    /// </summary>
    [Parameter]
    public bool DisplayTitle { get; set; }

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            await JSInterop.ImportModuleAsync();
        }

        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        var intermediateWorkers =
            from contractName in Source
            let implementorType = compositionService.GetExport<ISubsetCalendar2>(contractName)
            where implementorType is not null
            select serviceProvider.GetService(implementorType) as ISubsetCalendar2;
        _workers.Clear();
        foreach (var item in intermediateWorkers) {
            if (item is ICanCacheControl canCacheControl) {
                canCacheControl.CacheControl = CacheControl;
            }

            logger.WorkerAdded(item.GetType().Name);
            _workers.Add(item);
        }

        await ExecuteAsync(session);
    }

    void DetailsClicked(string selectors, IEnumerable<AltitudeItem>? details) {
        if (details is not null && OperatingSystem.IsBrowser()) {
            List<double> x = [];
            List<double> y = [];
            foreach (var item in details) {
                x.Add(item.X.ToLocalTime().Hour);
                y.Add(item.Y);
            }
            try {
                JSInterop.Chart($"#{selectors}", [.. x], [.. y]);
            } catch (JSException ex) {
                logger.InvokeFailed(ex);
            }
        }
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        var observer = await session.GetLocationAsync(cancellationToken);
        if (_workers.Count > 0 && observer is not null) {
            try {
                var rangeStart = session.Toi;
                var rangeLength = TimeSpan.FromDays(1);

                List<IEnumerable<SubsetResult>> data = [];

                foreach (var worker in _workers) {
                    IEnumerable<SubsetResult> result =
                        await worker.SubsetAsync(rangeStart, rangeLength, observer, cancellationToken);
                    data.Add(result.OrderBy(x => x.When).Take(Take)); ;
                    logger.InvokeCompleted(worker.GetType().Name, rangeStart, rangeLength, string.Join("; ", result.Select(x => x.When.ToLocalTime().ToString("t", CultureInfo.CurrentCulture))));
                }

                ViewData = (observer.ToString(), data);
                StateHasChanged();
            } catch (OperationCanceledException) {
            } catch (Microsoft.JSInterop.JSException ex) {

                // may come from C:Earth
                logger.InvokeFailed(ex);
            }
        }
    }
}

[SupportedOSPlatform("browser")]
static partial class JSInterop {

    const string ModuleName = "Components.Location";

    internal static Task<JSObject> ImportModuleAsync() =>
        JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/Components/Location.razor.js");

    [JSImport("chart", ModuleName)]
    internal static partial void Chart(string selectors, double[] x, double[] y);
}
