// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA2227 // Collection properties should be read only

using System.Text.Json;

namespace Deskimg.Components;

/// <summary>
/// Represents the <c>@CNT</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Container))]
partial class Container(
    ILogger<Container> logger,
    CompositionService compositionService) : IDesklet {

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected IDictionary<object, (Type ImplementorType, IDictionary<string, object?> Parameters)>? ViewData { get; private set; }

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="IndexType"/> collection.
    /// </summary>
    [Parameter]
    public IList<IndexType>? Index { get; set; }

    /// <inheritdoc/>
    protected override void OnParametersSet() {
        if (Index is not null) {
            try {
                ViewData = new Dictionary<object, (Type, IDictionary<string, object?>)>(
                    from item in Index
                    from componentDictionary in item?.Component ?? []
                    from component in componentDictionary
                    let implementorType = compositionService.GetExport<IDesklet>(component.Key)
                    where implementorType is not null
                    select new KeyValuePair<object, (Type, IDictionary<string, object?>)>(
                        component,
                        (implementorType, GetParameters(implementorType, component.Value))));
            } catch (NullReferenceException) {
                // NOTE: JsonSerializer may assign null to preferred non-nullable types
                //       Silently catch and ignore this "badly" formatted JSON
            }
        }
    }

    Dictionary<string, object?> GetParameters(Type implementorType, IDictionary<string, JsonElement> parameterValues) {
        Dictionary<string, object?> parameters = [];
        foreach (var item in parameterValues) {
            var pi = implementorType.GetProperty(item.Key);
            if (pi is not null) {
                try {
                    object? value = item.Value.Deserialize(pi.PropertyType);
                    parameters[item.Key] = value;
                    logger.SetParameterCompleted(implementorType.Name, item.Key, value);
                } catch (JsonException ex) {
                    logger.SetParameterFailed(ex, implementorType.Name, item.Key, pi.PropertyType.Name);
                }
            }
        }

        return parameters;
    }
}

#pragma warning restore CA2227 // Collection properties should be read only
