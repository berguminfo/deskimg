// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net.Http.Json;
using System.Text.Json;

namespace Deskimg.Components;

using Deskimg.Meteorology;

/// <summary>
/// Represents a image style object.
/// </summary>
public sealed class ImageStyle {

    /// <summary>
    /// Specifies the preferred image style.
    /// </summary>
    public string Theme { get; set; } = string.Empty;

    /// <summary>
    /// Specifies the preferred image width.
    /// </summary>
    public string Width { get; set; } = "1em";

    /// <summary>
    /// Specifies the preferred image height.
    /// </summary>
    public string Height { get; set; } = "1em";
}

/// <summary>
/// Represents the <c>@MET</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Meteorology))]
partial class Meteorology(
#if DEBUG
    EndpointOptions endpoint,
#endif
    ILogger<Meteorology> logger,
    SessionService session,
    IOptions<HyperlinkOptions> linkOptions,
    IServiceProvider serviceProvider,
    CompositionService compositionService,
    HttpClient http) : IDesklet, IDisposable {

    readonly List<(object Provider, Geoposition Geoposition)> _workers = [];
    IImmutableDictionary<WeatherConditions, string>? _weatherManifest;

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected IList<IMeteorologyReading>? ViewData { get; private set; }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.MeteorologySource)]
    // NOTE: choose one attribute, both will to [DataType(nameof(IObservationProvider)]
    public IImmutableList<string> Source { get; set; } = ImmutableList<string>.Empty;

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <summary>
    /// Gets or sets the maximum number of visible items.
    /// </summary>
    [Parameter]
    public int Take { get; set; } = 4;

    /// <summary>
    /// Gets or sets the title visibility.
    /// </summary>
    [Parameter]
    public bool DisplayTitle { get; set; }

    /// <summary>
    /// Gets or sets the wind gust visibility.
    /// </summary>
    [Parameter]
    public bool DisplayWindGust { get; set; } = true;

    /// <summary>
    /// Gets or sets the wind speed considered to be severe in m/s.
    /// </summary>
    [Parameter]
    public double SevereWindAboveInMPS { get; set; } = 18.0;

    /// <summary>
    /// Gets or sets the severe wind CSS class.
    /// </summary>
    [Parameter]
    public string SevereWindCssClass { get; set; } = "fw-bold";

    /// <summary>
    /// Gets or sets the weather condition image style.
    /// </summary>
    [Parameter]
    public ImageStyle ConditionImage { get; set; } = new();

    /// <summary>
    /// Gets or sets the wind image style.
    /// </summary>
    [Parameter]
    public ImageStyle WindImage { get; set; } = new();

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        var currentSet = await session.GetCurrentSetAsync(default);

        // initialize worker providers
        _workers.Clear();
        foreach (var identifier in Source) {
            if (currentSet.Geolocation.TryGetValue(identifier, out var geoposition)) {
                var intermediateWorkers =
                    from item in geoposition.GetTagsEnumerator()
                    let implementorType = (
                        compositionService.GetExport<IObservationProvider>(item.Key),
                        compositionService.GetExport<IForecastProvider>(item.Key)
                    )
                    where
                        implementorType.Item1 is not null ||
                        implementorType.Item2 is not null
                    select new {
                        Provider = serviceProvider.GetService(implementorType.Item1 ?? implementorType.Item2),
                        Tag = item
                    };
                foreach (var item in intermediateWorkers) {
                    if (item.Provider is ICanCacheControl canCacheControl) {
                        canCacheControl.CacheControl = CacheControl;
                    }
                    if (item.Provider is ICanSetTag canSetTag) {
                        canSetTag.Tag = item.Tag;
                    }

                    _workers.Add((item.Provider, geoposition));
                    logger.WorkerAdded(item.Provider.GetType().Name);
                }
            }
        }

        // initialize weather symbols manifest
        string manifestUri = linkOptions.Value.GetWeatherManifest(ConditionImage.Theme);
        try {
            if (manifestUri.EndsWith(".json", StringComparison.OrdinalIgnoreCase)) {
                logger.FetchStarting(manifestUri, CacheControl);
                _weatherManifest = await http.GetFromJsonAsync<IImmutableDictionary<WeatherConditions, string>>(
                    manifestUri);
            } else {
                logger.ManifestFormatNotSupported(manifestUri);
            }
        } catch (HttpRequestException ex) {
            logger.FetchFailed(ex, manifestUri, ex.StatusCode);
        } catch (JsonException ex) {
            logger.DeserializeJsonFailed(ex, manifestUri, ex.LineNumber, ex.BytePositionInLine);
        }

        await ExecuteAsync(session);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        if (_workers.Count > 0) {
            try {
                var rangeStart = session.Toi;
                var rangeLength = TimeSpan.Zero;
#if DEBUG
                if (endpoint.Dbug.Fetch?.Length == 1) {
                    rangeStart = DateTimeOffset.Parse("2023-04-22T05:00:00", System.Globalization.CultureInfo.InvariantCulture);
                }
#endif

                List<IMeteorologyReading> data = [];

                foreach (var worker in _workers) {
                    var observer = worker.Geoposition.Coordinate is not null ?
                          worker.Geoposition : await session.GetLocationAsync(cancellationToken);
                    if (observer is not null) {
                        IEnumerable<IMeteorologyReading>? result = null;
                        if (worker.Provider is IObservationProvider observationProvider) {
                            result = await observationProvider.GetObservationsAsync(observer, cancellationToken);
                        } else
                        if (worker.Provider is IForecastProvider forecastProvider) {
                            result = (
                                from item in await forecastProvider.GetForecastsAsync(rangeStart, rangeLength, observer, cancellationToken)
                                where item.ValidTo >= rangeStart
                                select item
                            ).Take(Take);
                        }

                        if (result is not null) {
                            data.AddRange(result);
                            logger.InvokeCompleted(worker.GetType().Name, observer, data.Count);
                        }
                    }
                }

                ViewData = data;
                StateHasChanged();
            } catch (OperationCanceledException) {
            } catch (Microsoft.JSInterop.JSException ex) {

                // may come from C:Earth
                logger.InvokeFailed(ex);
            }
        }
    }
}
