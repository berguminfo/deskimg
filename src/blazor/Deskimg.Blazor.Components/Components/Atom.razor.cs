// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Xml;
using Microsoft.JSInterop;

namespace Deskimg.Components;

using Deskimg.Syndication;

/// <summary>
/// Represents the <c>@RSS</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Atom))]
partial class Atom(
    ILogger<Atom> logger,
    SessionService session,
    EndpointOptions endpoint,
    HttpClient http,
    IJSRuntime js,
    IMemoryCache cache) : IDesklet, IDisposable {

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected IList<SyndicationItem>? ViewData { get; private set; }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.AtomSource)]
    public string? Source { get; set; }

    /// <summary>
    /// Gets or sets the <code>class</code> attributes.
    /// </summary>
    [Parameter]
    public string? Class { get; set; }

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <summary>
    /// Gets or sets the maximum number of visible items.
    /// </summary>
    [Parameter]
    public int Take { get; set; } = 4;

    async void ItemClicked(SyndicationItem item) =>
        await js.InvokeVoidAsync("window.open", item.Link, "_blank");

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.SearchChanged += SearchChanged;
        session.OnUpdate += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.SearchChanged -= SearchChanged;
        session.OnUpdate -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        if (!string.IsNullOrWhiteSpace(Source)) {
            await ExecuteAsync(session);
        }
    }

    void SearchChanged(object? sender, EventArgs e) => StateHasChanged();

    IEnumerable<SyndicationItem> Filter(IList<SyndicationItem> feed) {
        if (string.IsNullOrWhiteSpace(session.Search)) {
            return feed;
        } else {
            string[] query = session.Search.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            return
                from item in feed
                from criteria in query
                where
                    (item.Title?.Contains(criteria, StringComparison.CurrentCultureIgnoreCase) ?? true) ||
                    (item.Summary?.Contains(criteria, StringComparison.CurrentCultureIgnoreCase) ?? true)
                select item;
        }
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        if (!string.IsNullOrWhiteSpace(Source) && endpoint.Fetch(Source) is Uri requestUri) {
            SyndicationFeed? result = null;
            try {
                result = await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    logger.FetchStarting(requestUri, CacheControl);
                    using Stream stream = await http.GetStreamAsync(
                        requestUri,
                        CacheControl,
                        cancellationToken);

                    SyndicationFeed feed = new();
                    await feed.LoadAsync(stream, cancellationToken);
                    return feed;
                });
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, requestUri, ex.StatusCode);
            } catch (XmlException ex) {
                logger.DeserializeXmlFailed(ex, requestUri, ex.LineNumber, ex.LinePosition);
            } catch (FormatException ex) {
                logger.FormatDateTimeFailed(ex);
            }

            if (result?.Items is not null) {
                ViewData = result.Items.Take(Take).ToList();
                logger.InvokeCompleted(ViewData.Count);
                StateHasChanged();
            }
        }
    }
}
