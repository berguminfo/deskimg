// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;

namespace Deskimg.Components;

/// <summary>
/// Represents the <c>@PIC</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Picture))]
partial class Picture(
    ILogger<Picture> logger,
    SessionService session,
    EndpointOptions endpoint) : IDesklet, IDisposable {

    readonly string _imageId = $"img__{Guid.NewGuid():n}";

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>title</code> attribute.
    /// </summary>
    [Parameter]
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    public string? Source { get; set; }

    /// <summary>
    /// Gets or sets the <code>opacity</code> CSS property.
    /// </summary>
    [Parameter]
    public double Opacity { get; set; } = 0.87;

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            await JSInterop.ImportModuleAsync();
        }

        session.OnUpdate += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.OnUpdate -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        await ExecuteAsync(session);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        if (!string.IsNullOrWhiteSpace(Source) && endpoint.Fetch(Source, apiKey: true) is Uri requestUri) {
            try {
                if (OperatingSystem.IsBrowser()) {

                    logger.FetchStarting(requestUri, CacheControl);
                    await JSInterop.SetPictureBlob($"#{_imageId}", requestUri.AbsoluteUri);
                    logger.SetPictureBlobCompleted(_imageId);

                }
            } catch (JSException ex) {
                logger.InvokeFailed(ex);
            }
        }
    }

    [SupportedOSPlatform("browser")]
    static partial class JSInterop {

        const string ModuleName = "Components.Picture";

        internal static Task<JSObject> ImportModuleAsync() =>
            JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/Components/Picture.razor.js");

        [JSImport("setPictureBlob", ModuleName)]
        internal static partial Task SetPictureBlob(string selectors, string requestUri);
    }
}
