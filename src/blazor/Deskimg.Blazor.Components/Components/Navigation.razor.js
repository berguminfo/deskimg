// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// LeafletMapWrapper reference
let _mapWrapper = null;

// JSInterop.MarkerChanged binding
let _markerChanged = null;

/**
 * @see Geoposition.ToString
 */
function stringifyGeocoordinate(latitude, longitude) {
    function toAngle(value, positive, negative) {
        const degrees = Math.abs(value);
        const d = Math.floor(degrees);
        const m = Math.floor((degrees - d) * 60.0);
        const s = Math.floor((degrees - d - (m / 60.0)) * 3600.0);
        let suffix = "";
        if (value > Number.EPSILON) suffix = positive;
        if (value < -Number.EPSILON) suffix = negative;
        return `${d}°${m}′${s}″${suffix}`;
    }

    return `<b>${toAngle(latitude, "N", "S")} ${toAngle(longitude, "W", "E")}</b>`;
}

/**
 * Represent a leaflet map wrapper.
 */
class LeafletMapWrapper {

    /**
     * Initializes a new instance of this object.
     *
     * @param {string} id
     * @param {number} latitude
     * @param {number} longitude
     * @param {number} zoom
     */
    constructor(id, latitude, longitude, zoom) {

        // call leaflet initializer with current location as center view
        this.map = L.map(id).setView([latitude, longitude], zoom);

        // use OpenStreetMap tiles, see [Tile Usage Policy](https://operations.osmfoundation.org/policies/tiles/)
        L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(this.map);

        // add a marker at current position
        this.marker = L.marker([latitude, longitude]).addTo(this.map);

        // add a circle at current position
        this.circle = L.circle([latitude, longitude], {
            color: "white",
            opacity: 0.54,
            fillColor: "#4285f4",
            fillOpacity: 0.87,
            radius: 500
        }).addTo(this.map);

        // update popup
        this._updatePopup(latitude, longitude);

        // add a map click handler
        this.map.on("click", e => {

            // move the marker, keep longitude without wrapping in order to follow the map view
            this.marker.setLatLng(e.latlng);

            // let marker have a popup displaying the marked coordinate
            const latlng = this.map.wrapLatLng(e.latlng);
            this.marker.bindPopup(stringifyGeocoordinate(latlng.lat, latlng.lng)).openPopup();

            // notify the JSInterop wrapper
            if (_markerChanged) {
                _markerChanged(latlng.lat, latlng.lng);
            }
        });
    }

    /**
     * Moves map view, initial circle, marker and popup to another location.
     *
     * @param {number} latitude
     * @param {number} longitude
     **/
    moveTo(latitude, longitude) {
        const latlng = { lat: latitude, lng: longitude };
        this.map.panTo(latlng);
        this.circle.setLatLng(latlng)
        this.marker.setLatLng(latlng);
        this._updatePopup(latitude, longitude);
    }

    _updatePopup(latitude, longitude) {
        this.marker.bindPopup(stringifyGeocoordinate(latitude, longitude)).openPopup();
    }
}

/**
 * Helper function to toggle classList members having a data-toggle attribute.
 *
 * @param {string} selectors
 * @param {string} className
 */
export function toggleAll(selectors, className) {
    for (const key of selectors) {
        for (const element of document.querySelectorAll(`[data-toggle=${key}]`)) {
            element.classList.toggle(className);
        }
    }
}

/**
 * Binds JSInterop exported functions.
 *
 * @returns {PromiseLike<void>}
 */
export async function bind() {
    if (globalThis && globalThis.getDotnetRuntime) {
        const { getAssemblyExports } = await globalThis.getDotnetRuntime(0);
        const exports = await getAssemblyExports("Deskimg.Blazor.Components.dll");
        _markerChanged = exports.Deskimg.Components.Navigation.JSInterop.MarkerChanged;
    } else {
        _markerChanged = (latitude, longitude) => {
            console.info(`marker changed to [${latitude}, ${longitude}]`);
        };
    }
}

/**
 * @see LeafletMapWrapper
 */
 export function showMap(id, latitude, longitude, zoom) {
    if (!_mapWrapper) {
        _mapWrapper = new LeafletMapWrapper(id, latitude, longitude, zoom);
    } else {
        _mapWrapper.moveTo(latitude, longitude);
    }
}
