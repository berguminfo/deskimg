// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

using Deskimg.Calendar;
using Deskimg.Calendar.Epg;

internal sealed record Point(float X, float Y);

internal sealed record Thickness(float Left, float Top, float Right, float Bottom);

/// <summary>
/// Represents the <c>@EPG-H</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(EpgH))]
partial class EpgH : Epg, IDesklet {

    sealed class Rect {
        public float X { get; set; } // NOTE: check if a fixed number may be better here (like servo Au)
        public float Y { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
    }

    /// <summary>
    /// Gets the canvas starting time.
    /// This is the closest "time" matching a <see cref="TimeUnit"/> cell.
    /// </summary>
    protected DateTimeOffset StartTime {
        get {
            // store current toi in local variable before doing the calculation on this value
            var toi = session.Toi;
            return toi.AddTicks(toi.Ticks % TimeUnit.Ticks * -1);
        }
    }

    /// <summary>
    /// Gets the canvas ending time.
    /// This is the StartTime + (Take * TimeUnit).
    /// </summary>
    protected DateTimeOffset EndTime =>
        StartTime.AddTicks(TimeUnit.Ticks * Take);

    /// <summary>
    /// Gets or sets origin position.
    /// </summary>
    internal Point Origin { get; set; } = new(70, 20);

    /// <summary>
    /// Gets or set the cell padding.
    /// </summary>
    internal Thickness CellPadding { get; set; } = new(2, 0, 2, 0);

    /// <summary>
    /// Gets or set the icon padding.
    /// </summary>
    internal Thickness IconPadding { get; set; } = new(4, 6, 4, 6);

    /// <summary>
    /// Gets or sets the number of "time" (i.e. seconds) per app_unit (i.e. pixel).
    /// </summary>
    TimeSpan Resolution { get; set; } = TimeSpan.FromSeconds(20);

    /// <summary>
    /// Gets or sets the number of "time" (i.e. seconds) per header cell.
    /// </summary>
    TimeSpan TimeUnit { get; set; } = TimeSpan.FromMinutes(30);

    void Move(Rect rect, DateTimeOffset when, TimeSpan? duration) {
        rect.X = (float)(when.ToLocalTime().Ticks - StartTime.ToLocalTime().Ticks) / (float)Resolution.Ticks;
        rect.Width = (float)(duration?.Ticks ?? 0) / (float)Resolution.Ticks;
        if (rect.X < 0) {
            rect.Width -= (Math.Abs(rect.X));
            rect.X = 0;
        }
    }

    void Move(Rect rect, SubsetResult item) => Move(rect, item.When, item.Event.Duration);

    /// <summary>
    /// Gets the filtered result of the given <see cref="IGrouping{TKey, TElement}"/>.
    /// </summary>
    /// <param name="grouping">The grouping.</param>
    /// <returns>The filtered result.</returns>
    protected IEnumerable<SubsetResult> Filter(IGrouping<IChannel, IEnumerable<SubsetResult>> grouping) {
        string[] query = string.IsNullOrWhiteSpace(session.Search) ?
            [string.Empty] :
            session.Search.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
        return grouping
            .SelectMany(x => x)
            .OrderBy(x => x.When)
            .Where(x =>
                ((x.When + x.Event.Duration) >= StartTime) &&
                (x.When < EndTime) &&
                query.FirstOrDefault(criteria =>
                    (x.Event.Summary?.Contains(criteria, StringComparison.CurrentCultureIgnoreCase) ?? true) ||
                    (x.Event.Description?.Contains(criteria, StringComparison.CurrentCultureIgnoreCase) ?? true)
                ) is not null
            )
            .Select(x => x);
    }
}
