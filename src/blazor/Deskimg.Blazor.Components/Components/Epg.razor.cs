// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

using Deskimg.Calendar;
using Deskimg.Calendar.Epg;
using Deskimg.Resources;

/// <summary>
/// Represents the <c>@EPG</c> <see cref="IDesklet"/>.
/// </summary>
partial class Epg : IDisposable {

    readonly List<IEpgCalendar> _workers = [];

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected ILookup<IChannel, IEnumerable<SubsetResult>>? ViewData { get; private set; }

    /// <summary>
    /// Gets the embedded resource names.
    /// </summary>
    protected HashSet<string>? EmbeddedResourceNames { get; private set; }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.EpgSource)]
    public IImmutableList<string> Source { get; set; } = ImmutableList<string>.Empty;

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <summary>
    /// Gets or sets the maximum number of visible items.
    /// </summary>
    [Parameter]
    public int Take { get; set; } = 4;

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.ToiChanged += ExecuteAsync;
        session.SearchChanged += SearchChanged;

        // initialize embedded EPG resources
        EmbeddedResourceNames =
            (from item in typeof(Embedded).Assembly.GetManifestResourceNames()
             where item.StartsWith("Deskimg.Resources.Epg", StringComparison.Ordinal)
             select item).ToHashSet();
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiChanged += ExecuteAsync;
        session.SearchChanged -= SearchChanged;
    }

    void SearchChanged(object? sender, EventArgs e) => StateHasChanged();

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        if (Source is not null) {
            var intermediateWorkers =
                from item in Source
                let contract = item.Split('?', 2)
                let implementorType = compositionService.GetExport<IEpgCalendar>(contract[0])
                where implementorType is not null
                select new {
                    Provider = serviceProvider.GetService(implementorType) as IEpgCalendar,
                    Tag = new KeyValuePair<string, object?>(
                        contract[0],
                        contract.Length > 1 ? contract[1] : string.Empty)
                };
            _workers.Clear();
            foreach (var item in intermediateWorkers) {
                if (item.Provider is ICanCacheControl canCacheControl) {
                    canCacheControl.CacheControl = CacheControl;
                }
                if (item.Provider is ICanSetTag canSetTag) {
                    canSetTag.Tag = item.Tag;
                }

                logger.WorkerAdded(item.Provider.GetType().Name);
                _workers.Add(item.Provider);
            }
        }

        await ExecuteAsync(session);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
#if DEBUG
        if (endpoint.Dbug.Fetch?.Length == 1 &&
            session.Toi.ToLocalTime().Date != DateTimeOffset.Now.ToLocalTime().Date) {
#pragma warning disable CA1303 // Do not pass literals as localized parameters
            Console.WriteLine("*** REM: set both '&dbug:fetch=✓' AND '&t=' ***");
#pragma warning restore CA1303 // Do not pass literals as localized parameters
        }
#endif

        if (_workers.Count > 0) {
            try {
                var rangeStart = session.Toi;
                var rangeLength = TimeSpan.Zero;

                foreach (var worker in _workers) {
                    var result = await worker.SubsetAsync(rangeStart, rangeLength, cancellationToken);
                    logger.InvokeCompleted(worker.GetType().Name, rangeStart, rangeLength, result?.Count ?? 0);

                    ViewData = result;

                    break; // TODO: allow multiple workers
                }

                StateHasChanged();

            } catch (OperationCanceledException) { }
        }
    }
}
