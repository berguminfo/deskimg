// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text.RegularExpressions;

namespace Deskimg.Components;

/// <summary>
/// Represents the <c>@TXT</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Inline))]
partial class Inline(
    SessionService session) : IDesklet {

    [GeneratedRegex(@"\{\{(\w+):?(.*)?\}\}")]
    private static partial Regex ReplacementPattern { get; }

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected string? ViewData { get; set; }

    bool IsMarkupString() {
        if (!string.IsNullOrWhiteSpace(ViewData) && ViewData.Length >= 2 &&
            ViewData[0] == '<' && ViewData[^1] == '>') {
#if WITHOUT_MARKUP && !WITH_MARKUP
            return false;
#else
            return true;
#endif
        } else {
            return false;
        }
    }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the value.
    /// </summary>
    /// <remarks>
    /// This value will be evaluated as a <see cref="MarkupString"/> in case the
    /// first character is '&lt;' and the last character is '&gt;'.
    /// <para/>
    /// Use HTML entities (&amp;lt;, &amp;gt;,) or the ' ' space character to escape.
    /// </remarks>
    [Parameter]
    [DataType(CustomDataType.InlineSource)]
    public string? Value { get; set; }

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override Task OnParametersSetAsync() => ExecuteAsync(session);

    Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        ViewData = ReplacementPattern.Replace(Value?.Trim() ?? string.Empty, m =>
            GetReplacement(m.Groups[1].Value, m.Groups[2].Value));
        StateHasChanged();
        return Task.CompletedTask;
    }

    string GetReplacement(string key, string? format) =>
        key.ToUpperInvariant() switch {
            "VERSION" => ThisAssembly.Git.InformationalVersion,
            "NOW" => session.Toi.ToLocalTime().ToString(format, CultureInfo.CurrentCulture),
            _ => key
        };
}
