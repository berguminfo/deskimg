// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Runtime.InteropServices.JavaScript;
using System.Text.RegularExpressions;
using System.Timers;
using Timer = System.Timers.Timer;
using System.Runtime.Versioning;

namespace Deskimg.Components;


/// <summary>
/// Represents the <c>@NAV</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Navigation))]
partial class Navigation(
    ILogger<Navigation> logger,
    SessionService session) : IDesklet, IDisposable {

    readonly string _navbarId = $"nav__{Guid.NewGuid():n}";

    readonly string _modalId = $"nav__{Guid.NewGuid():n}";

    readonly string _mapId = $"nav__{Guid.NewGuid():n}";

    readonly string _group1 = $"nav__{Guid.NewGuid():n}";

    readonly string _group2 = $"nav__{Guid.NewGuid():n}";

    static readonly TimeSpan DelayedAction = TimeSpan.FromMilliseconds(250);

    /// <summary>
    /// Gets the current location.
    /// </summary>
    protected Geoposition? CurrentLocation { get; set; }

    #region Toi

    void NowClicked() {
        session.RealTimeClock = true;
        session.Toi = DateTimeOffset.Now;
    }

    ElementReference _durationElement;

    async void ClearClicked() {
        _durationValue = string.Empty;
        await _durationElement.FocusAsync();
    }

    // data binding members

    [GeneratedRegex(@"^([\d]+[\.,]?\d*)([HDWMY]?)", RegexOptions.IgnoreCase)]
    private static partial Regex DurationPattern();

    // set "1H" as initial duration value
    string _durationValue = MessageFormatter.Format(SR.Default.Navigation.DurationFormat.First(), new {
        Value = 1
    });

    // result of matching DurationValue with DurationPattern
    MeasureD? _duration;

    string DurationValue {
        get => _durationValue;
        set {
            _durationValue = value;
            _duration = null;
        }
    }

    void DurationChanged(ChangeEventArgs e) {
        if (e.Value?.ToString() is string s) {
            DurationValue = s;
        }
    }

    // duration members

    readonly string _durationDataListId = $"lst__{Guid.NewGuid():n}";
    readonly List<string> _durationDataList = [];

    // populates the duration data list members out of the event handler
    List<string> GetDurationDataList() {
        _durationDataList.Clear();
        Match m = DurationPattern().Match(DurationValue);
        if (m.Success && double.TryParse(m.Groups[1].Value.Replace(',', '.'), out var value)) {
            foreach (var pattern in SR.Default.Navigation.DurationFormat) {
                _durationDataList.Add(MessageFormatter.Format(pattern, new {
                    Value = value,
                }));
            }
        }

        return _durationDataList;
    }

    // browse Forward and Backward event handlers

    int _changeToiDirection;
    int _changeToiMultiplier;

    readonly Timer _changeToiTimer = new() {
        AutoReset = false
    };

    void ChangeToiClicked(int direction) {
        if (_duration is null) {
            Match m = DurationPattern().Match(DurationValue);
            if (m.Success && double.TryParse(m.Groups[1].Value.Replace(',', '.'), out var value)) {
                _duration = new(value, m.Groups[2].Value);
            }
        }
        if (_duration is not null) {
            _changeToiTimer.Stop();
            _changeToiDirection = direction;
            _changeToiTimer.Interval = DelayedAction.TotalMilliseconds;
            _changeToiMultiplier++;
            _changeToiTimer.Start();
        }
    }

    // the "DelayedAction" handler should update session.Toi

    void ChangeToiElapsed(object? sender, ElapsedEventArgs e) {
        if (_duration is not null) {
            double value = _duration.Value.Value * _changeToiDirection * _changeToiMultiplier;
            DateTimeOffset toi = session.Toi;
            switch (_duration.Value.Unit[0]) {
                case 'H':
                    toi = toi.AddHours(value);
                    break;
                case 'D':
                    toi = toi.AddDays(value);
                    break;
                case 'W':
                    toi = toi.AddDays(7 * value);
                    break;
                case 'M':
                    toi = toi.AddMonths(Convert.ToInt32(Math.Round(value)));
                    break;
                case 'Y':
                    toi = toi.AddYears(Convert.ToInt32(Math.Round(value)));
                    break;
            }

            // update session clock
            session.RealTimeClock = false;
            session.Toi = toi;
        }

        // clear this iteration
        _changeToiMultiplier = 0;
        _changeToiDirection = 0;
    }

    #endregion

    #region Search

    string _search = string.Empty;

    Timer _searchChangedTimer = new() {
        AutoReset = false
    };

    void SearchChanged(ChangeEventArgs e) {
        _searchChangedTimer.Stop();
        _search = e.Value?.ToString() ?? string.Empty;
        _searchChangedTimer.Interval = DelayedAction.TotalMilliseconds;
        _searchChangedTimer.Start();
    }

    void SearchChangedTimerElapsed(object? sender, ElapsedEventArgs e) {
        session.Search = _search;
    }

    #endregion

    #region Modal

    static readonly (string Text, int? Value)[] ClippingButtons =
        [("sm", 576), ("md", 768), ("lg", 992), ("xl", 1200), ("∞", null)];

    sealed class ModalModel {

        public DateTime? Date { get; set; }

        public DateTime? Time { get; set; }

        public string? Latitude { get; set; }

        public string? Longitude { get; set; }

        public int? MinWidth { get; set; }

        public DateTimeOffset? GetDateTime() {
            if (Date is not null && Time is not null) {
                var date = DateOnly.FromDateTime(Date.Value);
                var time = TimeOnly.FromDateTime(Time.Value);
                return new DateTimeOffset(date.ToDateTime(time));
            } else {
                return null;
            }
        }

        public Geocoordinate? GetCoordinates() {
            bool latitudeValid = double.TryParse(
                Latitude?.Replace(',', '.'), CultureInfo.InvariantCulture, out var latitude);
            latitudeValid &= latitude >= -90 && latitude <= 90;

            bool longitudeValid = double.TryParse(
                Longitude?.Replace(',', '.'), CultureInfo.InvariantCulture, out var longitude);
            longitudeValid &= longitude >= -180 && longitude <= 180;

            if (latitudeValid && longitudeValid) {
                return new() {
                    Latitude = latitude,
                    Longitude = longitude
                };
            } else {
                return null;
            }
        }
    }

    readonly ModalModel _modalView = new();

    void OpenModal() {
        _modalView.Date = session.Toi.ToLocalTime().DateTime;
        _modalView.Time = session.Toi.ToLocalTime().DateTime;
        if (CurrentLocation?.Coordinate is Geocoordinate coords) {
            _modalView.Latitude = coords.Latitude.ToString(CultureInfo.InvariantCulture);
            _modalView.Longitude = coords.Longitude.ToString(CultureInfo.InvariantCulture);
        }

        if (session.ClippingRules is not null) {
            foreach (var item in session.ClippingRules) {
                var m = Regex.Match(item, @"screen\s.*min-width:\s*(\d+)", RegexOptions.IgnoreCase);
                if (m.Success) {
                    _modalView.MinWidth = int.Parse(m.Groups[1].Value, CultureInfo.InvariantCulture);
                }
            }
        }
    }

    void CloseModal() {
        List<string> clippingRules = [];
        if (_modalView.MinWidth is int minWidth) {
            clippingRules.Add($"screen and (min-width: {minWidth}px)");
        }
        session.ClippingRules = clippingRules;

        if (_modalView.GetCoordinates() is Geocoordinate coords) {
            session.Latitude = coords.Latitude;
            session.Longitude = coords.Longitude;
        }

        if (_modalView.GetDateTime() is DateTimeOffset nextToi) {
            session.RealTimeClock = false;
            session.Toi = nextToi;
        }
    }

    void ToggleMapVisibility() {
        if (OperatingSystem.IsBrowser()) {
            try {
                JSInterop.ToggleAll([_group1, _group2], "d-none");
            } catch (JSException ex) {
                logger.InvokeFailed(ex);
            }
        }
    }

    void ResetTimeClicked() {
        var now = DateTime.Now.ToLocalTime();
        _modalView.Date = now;
        _modalView.Time = now;
    }

    void DisplayMapClicked() {
        if (OperatingSystem.IsBrowser()) {
            ToggleMapVisibility();
            var coords = _modalView.GetCoordinates();
            try {
                JSInterop.ShowMap(_mapId, coords?.Latitude ?? 0.0, coords?.Longitude ?? 0.0, 10);
            } catch (JSException ex) {
                logger.InvokeFailed(ex);
            }
        }
    }

    void UseMarkedClicked() {
        if (OperatingSystem.IsBrowser()) {
            _modalView.Latitude = JSInterop.MarkerCoordinate.Latitude.ToString(CultureInfo.InvariantCulture);
            _modalView.Longitude = JSInterop.MarkerCoordinate.Longitude.ToString(CultureInfo.InvariantCulture);
        }
        ToggleMapVisibility();
    }

    #endregion

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; }

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <see href="https://learn.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose"/>
    protected virtual void Dispose(bool disposing) {
        if (!_disposed) {
            if (disposing) {
                session.ToiOrLocationChanged -= ExecuteAsync;
                _changeToiTimer.Dispose();
                _searchChangedTimer.Dispose();
            }

            _disposed = true;
        }
    }

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            await JSInterop.ImportModuleAsync();
            try {
                await JSInterop.Bind();
            } catch (JSException ex) {
                logger.InvokeFailed(ex);
            }
        }

        // initialize delayed actions
        _changeToiTimer.Elapsed += ChangeToiElapsed;
        _search = session.Search;
        _searchChangedTimer.Elapsed += SearchChangedTimerElapsed;

        // initialize Session events
        session.ToiOrLocationChanged += ExecuteAsync;

        await ExecuteAsync(session);
    }

    bool _disposed;

    /// <inheritdoc/>
    protected override void OnParametersSet() {
        Visible = Visible || session.Navigation;
    }

    void MarkerChanged(object? sender, Geocoordinate coords) {
        _modalView.Latitude = coords.Latitude.ToString(CultureInfo.InvariantCulture);
        _modalView.Longitude = coords.Longitude.ToString(CultureInfo.InvariantCulture);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        CurrentLocation = await session.GetLocationAsync(cancellationToken);
        StateHasChanged();
    }

    [SupportedOSPlatform("browser")]
    static partial class JSInterop {

        internal static Geocoordinate MarkerCoordinate { get; } = new();

        const string ModuleName = "Components.Navigation";

        internal static Task<JSObject> ImportModuleAsync() =>
            JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/Components/Navigation.razor.js");

        [JSImport("bind", ModuleName)]
        internal static partial Task Bind();

        [JSImport("toggleAll", ModuleName)]
        internal static partial void ToggleAll(string[] selectors, string className);

        [JSImport("showMap", ModuleName)]
        internal static partial void ShowMap(string id, double latitude, double longitude, int zoom);

        [JSExport]
        internal static void MarkerChanged(double latitude, double longitude) {
            MarkerCoordinate.Latitude = latitude;
            MarkerCoordinate.Longitude = longitude;
        }
    }
}
