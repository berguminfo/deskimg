// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

using Deskimg.Calendar;
using Deskimg.Calendar.Epg;

/// <summary>
/// Represents the <c>@EPG-V</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(EpgV))]
partial class EpgV : Epg, IDesklet {

    bool _showImages;

    void CollapseClicked(EventArgs e) => _showImages = true;

    /// <summary>
    /// Gets the filtered result of the given <see cref="IGrouping{TKey, TElement}"/>.
    /// </summary>
    /// <param name="grouping">The grouping.</param>
    /// <returns>The filtered result.</returns>
    protected IEnumerable<SubsetResult> Filter(IGrouping<IChannel, IEnumerable<SubsetResult>> grouping) {
        string[] query = string.IsNullOrWhiteSpace(session.Search) ?
            [string.Empty] :
            session.Search.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
        return grouping
            .SelectMany(x => x)
            .OrderBy(x => x.When)
            .Where(x =>
                ((x.When + x.Event.Duration) >= session.Toi) &&
                query.FirstOrDefault(criteria =>
                    (x.Event.Summary?.Contains(criteria, StringComparison.CurrentCultureIgnoreCase) ?? true) ||
                    (x.Event.Description?.Contains(criteria, StringComparison.CurrentCultureIgnoreCase) ?? true)
                ) is not null
            )
            .Select(x => x)
            .Take(Take);
    }
}
