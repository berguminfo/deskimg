// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;

namespace Deskimg.Components;

using Deskimg.Meteorology;

/// <summary>
/// Represents the <c>@WRN</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Alert))]
partial class Alert(
    ILogger<Alert> logger,
    SessionService session,
    IOptions<HyperlinkOptions> linkOptions,
    IServiceProvider serviceProvider,
    CompositionService compositionService) : IDesklet, IDisposable {

    readonly List<(IAlertProvider Provider, Geoposition Geoposition)> _workers = [];

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected IList<IAlertMessage>? ViewData { get; private set; }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.AlertSource)]
    public string? Source { get; set; }

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <summary>
    /// Gets or sets the maximum number of visible items.
    /// </summary>
    [Parameter]
    public int Take { get; set; } = 4;

    /// <summary>
    /// Gets or sets the warning symbol theme.
    /// </summary>
    /// currently not a [Parameter]
    public string Theme { get; set; } = "001";

    /// <summary>
    /// Specifies if notifications should be supported.
    /// </summary>
    [Parameter]
    public AlertSeverity Notify { get; set; } = AlertSeverity.Inactive;

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            await JSInterop.ImportModuleAsync();
        }

        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    void AddNotification(string tag, string? title, string? icon) {
        if (OperatingSystem.IsBrowser() && session.EnableNotification) {
            try {
                JSInterop.Notify(tag, title, icon);
                logger.NotifyCompleted(tag, title);
            } catch (JSException ex) {
                logger.InvokeFailed(ex);
            }
        }
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {

        // initialize worker providers
        if (Source is not null) {
            var currentSet = await session.GetCurrentSetAsync(default);
            if (currentSet.Geolocation.TryGetValue(Source, out var geoposition)) {
                var intermediateWorkers =
                    from item in geoposition.GetTagsEnumerator()
                    let implementorType = compositionService.GetExport<IAlertProvider>(item.Key)
                    where implementorType is not null
                    select new {
                        Provider = serviceProvider.GetService(implementorType) as IAlertProvider,
                        Tag = item
                    };
                _workers.Clear();
                foreach (var item in intermediateWorkers) {
                    if (item.Provider is ICanCacheControl canCacheControl) {
                        canCacheControl.CacheControl = CacheControl;
                    }
                    if (item.Provider is ICanSetTag canSetTag) {
                        canSetTag.Tag = item.Tag;
                    }

                    logger.WorkerAdded(item.Provider.GetType().Name);
                    _workers.Add((item.Provider, geoposition));
                }
            }
        }

        await ExecuteAsync(session);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        if (_workers.Count > 0) {
            try {
                var rangeStart = session.Toi;
                var rangeLength = TimeSpan.FromDays(Take);

                List<IAlertMessage> data = [];
                foreach (var worker in _workers) {
                    var result = await worker.Provider.GetAlertsAsync(rangeStart, rangeLength, worker.Geoposition, cancellationToken);
                    data.AddRange(result.Take(Take));
                    logger.InvokeCompleted(worker.GetType().Name, worker.Geoposition, data.Count);
                }

                ViewData = data;
                StateHasChanged();
            } catch (OperationCanceledException) { }
        }
    }

    [SupportedOSPlatform("browser")]
    static partial class JSInterop {

        const string ModuleName = "Components.Alert";

        internal static Task<JSObject> ImportModuleAsync() =>
            JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/Components/Alert.razor.js");

        [JSImport("notify", ModuleName)]
        internal static partial void Notify(string tag, string? title, string? icon);
    }
}
