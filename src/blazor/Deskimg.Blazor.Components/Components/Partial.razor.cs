// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

/// <summary>
/// Represents the <c>@TXT</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Partial))]
partial class Partial(
    ILogger<Partial> logger,
    SessionService session,
    EndpointOptions endpoint,
    HttpClient http,
    IMemoryCache cache) : IDesklet, IDisposable {

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected string? ViewData { get; set; }

    bool IsMarkupString() {
        if (!string.IsNullOrWhiteSpace(ViewData) && ViewData.Length >= 2 &&
            ViewData[0] == '<' && ViewData[^1] == '>') {
#if WITHOUT_MARKUP && !WITH_MARKUP
            return false;
#else
            return true;
#endif
        } else {
            return false;
        }
    }

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>style</code> attribute.
    /// </summary>
    [Parameter]
    [DataType(CustomDataType.Style)]
    public IImmutableDictionary<string, string>? Style { get; set; }

    /// <summary>
    /// Gets or sets the data source.
    /// </summary>
    [Parameter]
    public string? Source { get; set; }

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override void OnInitialized() {
        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override Task OnParametersSetAsync() => ExecuteAsync(session);

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        if (!string.IsNullOrWhiteSpace(Source) && endpoint.Fetch(Source) is Uri requestUri) {
            string? result = null;
            try {
                result = await cache.GetOrCreateAsync(requestUri, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    logger.FetchStarting(requestUri, CacheControl);
                    using var stream = await http.GetStreamAsync(
                        requestUri,
                        CacheControl,
                        cancellationToken);

                    using StreamReader reader = new(stream);
                    return (await reader.ReadToEndAsync(cancellationToken)).Trim();
                });
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, requestUri, ex.StatusCode);
            }

            if (result is not null) {
                ViewData = result;
                StateHasChanged();
            }
        }
    }
}
