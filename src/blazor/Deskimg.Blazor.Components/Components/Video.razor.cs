// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Components;

/// <summary>
/// Represents the <c>@VID</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Video))]
partial class Video(
    SessionService session) : IDesklet {

    /// <summary>
    /// Gets or sets the component visibility.
    /// </summary>
    [Parameter]
    public bool Visible { get; set; } = true;

    /// <summary>
    /// Gets or sets the <code>title</code> attribute.
    /// </summary>
    [Parameter]
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL of the media resource.
    /// </summary>
    [Parameter]
    public string? Source { get; set; }

    /// <summary>
    /// Gets or sets the MIME media type.
    /// </summary>
    [Parameter]
    public string? Type { get; set; }

    /// <summary>
    /// Gets or sets the media query for the resource's intended media.
    /// </summary>
    [Parameter]
    public string? Media { get; set; }

    /// <summary>
    /// A Boolean attribute: if specified, the audio will automatically begin playback
    /// as soon as it can do so, without waiting for the entire audio file to finish downloading.
    /// </summary>
    public bool AutoPlay { get; set; } = true;

    /// <summary>
    /// A Boolean attribute: if specified, the audio player will automatically seek
    /// back to the start upon reaching the end of the audio.
    /// </summary>
    public bool Loop { get; set; }

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);
}
