// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

const exclusionMap = new Map();

/**
 * Sends a notification message to the user using the Notifications API or cefNotification API.
 *
 * @param {string} tag
 * @param {string} title
 * @param {string} icon
 */
export async function notify(tag, title, icon) {
    // the WebView2 component don't track tag and renotify properties
    // giving a new notification for each update
    if (!exclusionMap.has(tag)) {
        let permission = window.Notification && Notification.permission;
        if (permission === "default") {
            permission = await Notification.requestPermission();
        }

        if (permission === "granted") {
            new Notification(title, {
                tag: tag,
                icon: icon,
                renotify: false,
                vibrate: [200, 100, 200]
            });
            exclusionMap.set(tag, new Date());
        }
    }
}
