// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;

namespace Deskimg.Components;

using Deskimg.Ephemeris;

/// <summary>
/// Represents the <c>@BKG</c> <see cref="IDesklet"/>.
/// </summary>
[Export<IDesklet>(nameof(Background))]
partial class Background(
    ILogger<Background> logger,
    SessionService session,
    EndpointOptions endpoint,
    IOptions<HyperlinkOptions> linkOptions,
    Microsoft.JSInterop.IJSRuntime js) : IDesklet, IDisposable {

    /// <summary>
    /// Gets the background overlay blob URI.
    /// </summary>
    protected string? OverlayBlob { get; private set; }

    /// <summary>
    /// Gets the component view data.
    /// </summary>
    protected IList<CompositeItem>? ViewData { get; private set; }

#pragma warning disable BL0007 // Component parameters should be auto properties

    DocumentTheme _theme;

    /// <summary>
    /// Reference to CurrentSet.Background.Theme or SessionService.Theme.
    /// </summary>
    /// <remarks>
    /// Index.razor sets this parameter during initialization.
    /// </remarks>
    [Parameter]
    public DocumentTheme Theme {
        get => session.Theme ?? _theme;
        set => _theme = value;
    }

#pragma warning restore BL0007 // Component parameters should be auto properties

    /// <summary>
    /// Reference to CurrentSet.Background.Overlay.
    /// </summary>
    /// <remarks>
    /// Index.razor sets this parameter during initialization.
    /// </remarks>
    [Parameter]
    public BackgroundOverlay? Overlay { get; set; }

    /// <summary>
    /// Reference to CurrentSet.Background.Composite.
    /// </summary>
    /// <remarks>
    /// Index.razor sets this parameter during initialization.
    /// </remarks>
    [Parameter]
    public IEnumerable<BackgroundComposite>? Composite { get; set; }

    MessageFormatter? _customMessageFormatter;

    MessageFormatter CustomMessageFormatter {
        get {
            if (_customMessageFormatter is null) {
                CustomValueFormatters customValueFormatter = new() {
                    Number = (CultureInfo _, object? value, string? style, out string? formatted) => {
                        if (int.TryParse(value?.ToString(), out int number) && style is not null) {
                            int[] available = (
                                from item in style.Split(';')
                                let x = int.Parse(item, CultureInfo.InvariantCulture)
                                orderby x descending
                                select x).ToArray();
                            int bestMatch = available.FirstOrDefault(x => x <= number);
                            if (bestMatch == default && available.Length > 0) {
                                bestMatch = available.Last();
                            }

                            formatted = bestMatch.ToString(CultureInfo.InvariantCulture);
                            return true;
                        } else {
                            formatted = null;
                            return false;
                        }
                    }
                };
                _customMessageFormatter = new(customValueFormatter: customValueFormatter);
            }

            return _customMessageFormatter;
        }
    }

    /// <inheritdoc/>
    protected override void OnAfterRender(bool firstRender) => session.RaiseOnAfterRender(this, firstRender);

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            await JSInterop.ImportModuleAsync();
        }

        session.ToiOrLocationChanged += ExecuteAsync;
    }

    /// <inheritdoc/>
    public void Dispose() {
        session.ToiOrLocationChanged -= ExecuteAsync;
    }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync() {
        if (OperatingSystem.IsBrowser()) {

            // create the overlay blob if overlay has been requested
            if (Overlay is not null && Overlay.Opacity?.Count >= 2) {
                try {
                    if (Overlay.Pattern == HatchPattern.CheckerBoard && Overlay.Opacity?.Count >= 2) {
                        string blob = await JSInterop.CreateCheckerBoard(
                            Overlay.Opacity[0],
                            Overlay.Opacity[1],
                            Overlay.Width,
                            Overlay.Height);
                        logger.HatchPatternCreated(Overlay.Pattern.ToString(), Overlay.Opacity[0], Overlay.Opacity[1], Overlay.Width, Overlay.Height);

                        // redirect the blob depending on rendering mode
                        switch (session.BackgroundRendering) {

                            // Dom rendering adds the overlay as an ImageElement
                            case RenderingMode.Dom:
                                OverlayBlob = blob;
                                break;

                            // Blob and Css rendering overlay as part of the composition
                            case RenderingMode.Blob:
                            case RenderingMode.Css:
                                await JSInterop.SetBackgroundOverlay(blob);
                                logger.SetBackgroundOverlayCompleted(blob);
                                break;

                            default:
                                throw new ArgumentOutOfRangeException(nameof(session.BackgroundRendering), session.BackgroundRendering, "Invalid value.");
                        }
                    }
                } catch (JSException ex) {
                    logger.InvokeFailed(ex);
                }
            }
        }

        await ExecuteAsync(session);
    }

    async Task ExecuteAsync(SessionService session, CancellationToken cancellationToken = default) {
        try {
            List<CompositeItem>? result = null;

            // session.BackgroundSource overrides CurrentSet.Background
            if (!string.IsNullOrWhiteSpace(session.BackgroundSource) && endpoint.Fetch(session.BackgroundSource, apiKey: true) is Uri requestUri) {

                logger.FetchStarting(requestUri);
                result = [new() {
                    Source = requestUri.AbsoluteUri,
                    Type = session.BackgroundType,
                    Opacity = 1.0
                }];

                // otherwise CurrentSet.Background is a composition
            } else {
                double daylightFactor = 1.0;
                var observer = await session.GetLocationAsync(cancellationToken);
                if (OperatingSystem.IsBrowser() && observer?.Coordinate is Geocoordinate coords) {
                    Earth earth = new(js);
                    DateTimeOffset utc = new(session.Toi.DateTime, TimeSpan.Zero);
                    RiseSet solarRiseSet = await earth.GetRiseSetAsync<Solar>(utc, coords, cancellationToken);
                    daylightFactor = solarRiseSet.GetDaylightFactor(session.Toi);
                }

                // find the best matching composition given the calculated daylightFactor
                BackgroundComposite? compositeItem = Composite?.FirstOrDefault(x => x.From <= daylightFactor && daylightFactor <= x.To);
                if (compositeItem?.Items is not null) {
                    result = (
                        from item in compositeItem.Items
                        where !string.IsNullOrWhiteSpace(item.Source)
                        select new CompositeItem {
                            Source =
                                CustomMessageFormatter.FormatMessage(item.Source ?? string.Empty, new {
                                    Local = linkOptions.Value.GetBackgroundLink(),
                                    MaxWidth = 0, // TODO: find screen width
                                    CalDAV = endpoint.CalDAV(string.Empty),
                                    ApiKey = $"{FetchToken.QueryKey}={FetchToken.Create()}"
                                }),
                            Type = item.Type,
                            Opacity = GetOpacity(
                                daylightFactor,
                                compositeItem.From,
                                compositeItem.To,
                                item.Opacity)
                        }).ToList();
                }
            }
            if (result is not null) {
                switch (session.BackgroundRendering) {

                    // Dom updates ViewData and change state
                    case RenderingMode.Dom:
                        ViewData = result;
                        if (OperatingSystem.IsBrowser() && Theme == DocumentTheme.Auto) {
                            await JSInterop.Composite(result);
                            JSInterop.SetDocumentTheme();
                        }

                        StateHasChanged();
                        logger.SetBackgroundImageCompleted();
                        break;

                    // Blob invokes composite, gets the blob as ViewData and change state
                    case RenderingMode.Blob:
                        if (OperatingSystem.IsBrowser()) {
                            await JSInterop.Composite(result);
                            if (Theme == DocumentTheme.Auto) {
                                JSInterop.SetDocumentTheme();
                            }

                            ViewData = [new() {
                                Source = await JSInterop.GetBackgroundBlob(),
                                Opacity = 1.0
                            }];
                            StateHasChanged();
                            logger.SetBackgroundImageCompleted();
                        }
                        break;

                    // Css invokes composite and sets the background-image blob URI
                    case RenderingMode.Css:
                        if (OperatingSystem.IsBrowser()) {
                            await JSInterop.Composite(result);
                            if (Theme == DocumentTheme.Auto) {
                                JSInterop.SetDocumentTheme();
                            }

                            JSInterop.SetBackgroundImage(await JSInterop.GetBackgroundBlob());
                            logger.SetBackgroundImageCompleted();
                        }
                        break;

                    default:
                        throw new InvalidOperationException();
                }
            }
        } catch (OperationCanceledException) {
        } catch (Microsoft.JSInterop.JSException ex) {

            // may come from C:Earth
            logger.InvokeFailed(ex);
        } catch (JSException ex) {

            // may come from C:JSInterop
            logger.InvokeFailed(ex);
        }
    }

    static double GetOpacity(double daylightFactor, double fromDaylightFactor, double toDaylightFactor, double? opacity) {
        if (opacity is null) {
            return 1.0;
        }

        double Δ = Math.Abs(toDaylightFactor - fromDaylightFactor);
        double α = Δ <= double.Epsilon ? 0.0 : (daylightFactor - fromDaylightFactor) / Δ;
        if (α <= double.Epsilon) α = 0.0;
        if (α > 1.0) α = 1.0;

        return opacity > 0 ? α : 1.0 - α;
    }

    [SupportedOSPlatform("browser")]
    static partial class JSInterop {

        const string ModuleName = "Components.Background";

        internal static Task<JSObject> ImportModuleAsync() =>
            JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/Components/Background.razor.js");

        [JSImport("createCheckerBoard", ModuleName)]
        internal static partial Task<string> CreateCheckerBoard(double opacity1, double opacity2, int dw, int dh);

        [JSImport("setBackgroundOverlay", ModuleName)]
        internal static partial Task SetBackgroundOverlay(string url);

        [JSImport("composite", ModuleName)]
        internal static partial Task Composite(string[] images, double[] opacity);

        internal static Task Composite(IList<CompositeItem> composites) {
            static bool isImage(CompositeItem x) {
                return
                    !string.IsNullOrWhiteSpace(x.Source) && (
                        x.Type is null ||
                        x.Type.StartsWith("image", StringComparison.OrdinalIgnoreCase));
            }

            string[] images = composites.Where(isImage).Select(x => x.Source ?? string.Empty).ToArray(); // NOTE: Source is not null here due to the predicate
            double[] opacity = composites.Where(isImage).Select(x => x.Opacity ?? 1.0).ToArray();

            return Composite(images, opacity);
        }

        [JSImport("getBackgroundBlob", ModuleName)]
        internal static partial Task<string> GetBackgroundBlob();

        [JSImport("setBackgroundImage", ModuleName)]
        internal static partial void SetBackgroundImage(string url);

        [JSImport("setDocumentTheme", ModuleName)]
        internal static partial void SetDocumentTheme();
    }
}
