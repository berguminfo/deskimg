// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

using Deskimg.Meteorology;

/// <summary>
/// Represents source generated <see cref="ILogger"/> extensions.
/// </summary>
public static partial class LoggerExtensions {

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 11,
        Level = LogLevel.Critical,
        Message = "Failed to initialize required service."
    )]
    public static partial void InitializeServicesFailed(this ILogger logger, Exception exception);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 21,
        Level = LogLevel.Debug,
        Message = "Adding worker with name '{Name}'."
    )]
    public static partial void WorkerAdded(this ILogger logger, string name);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 22,
        Level = LogLevel.Debug,
        Message = "Invoke completed successfully."
    )]
    public static partial void InvokeCompleted(this ILogger logger);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 23,
        Level = LogLevel.Debug,
        Message = "Invoke completed successfully. Take: {Take}."
    )]
    public static partial void InvokeCompleted(this ILogger logger, int take);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 24,
        Level = LogLevel.Debug,
        Message = "Invoke completed successfully for '{Type}'. Location: '{Location}' | Take: {Take}."
    )]
    public static partial void InvokeCompleted(this ILogger logger, string type, Geoposition? location, int take);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 25,
        Level = LogLevel.Debug,
        Message = "Invoke completed successfully for '{Type}'. Start: '{Start:u}' | Length: '{Length}' | Take: {Take}."
    )]
    public static partial void InvokeCompleted(this ILogger logger, string type, DateTimeOffset start, TimeSpan length, int take);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 26,
        Level = LogLevel.Debug,
        Message = "Invoke completed successfully for '{Type}'. Start: '{Start:u}' | Length: '{Length}' | Result: {Result}."
    )]
    public static partial void InvokeCompleted(this ILogger logger, string type, DateTimeOffset start, TimeSpan length, string result);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 27,
        Level = LogLevel.Error,
        Message = "Invoke failed."
    )]
    public static partial void InvokeFailed(this ILogger logger, Exception exception);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 30,
        Level = LogLevel.Debug,
        Message = "Parameter changed. Session:{Name}: '{ToValue}' | Was: '{FromValue}'."
    )]
    public static partial void SessionChanged(this ILogger logger, string name, object? toValue, object? fromValue);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 31,
        Level = LogLevel.Warning,
        Message = "Unknown culture '{Name}'."
    )]
    public static partial void CultureNotFound(this ILogger logger, string name);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 33,
        Level = LogLevel.Information,
        Message = "Language changed. Culture: '{Name}' | Strings: '{StringsLanguage}'."
    )]
    public static partial void LanguageChanged(this ILogger logger, string name, string stringsLanguage);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 34,
        Level = LogLevel.Debug,
        Message = "Scheduler starting. Repeat: '{Interval}'."
    )]
    public static partial void SchedulerStarted(this ILogger logger, TimeSpan interval);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 35,
        Level = LogLevel.Debug,
        Message = "Scheduler stopped."
    )]
    public static partial void SchedulerStopped(this ILogger logger);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 36,
        Level = LogLevel.Information,
        Message = "Update location failed."
    )]
    public static partial void UpdateLocationFailed(this ILogger logger, Exception exception);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 40,
        Level = LogLevel.Warning,
        Message = "Unknown configuration section '{Name}'."
    )]
    public static partial void ConfigurationSectionNotFound(this ILogger logger, string name);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 40,
        Level = LogLevel.Debug,
        Message = "Set parameter completed. Component: {Component} | Parameter: {Parameter} | Value: '{Value}'."
    )]
    public static partial void SetParameterCompleted(this ILogger logger, string component, string parameter, object? value);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 41,
        Level = LogLevel.Error,
        Message = "Set parameter failed. Component: {Component}: | Parameter: {Parameter} | Type: {Type}."
    )]
    public static partial void SetParameterFailed(this ILogger logger, Exception exception, string component, string parameter, string type);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 42,
        Level = LogLevel.Warning,
        Message = "Unknown parameter '{Name}'. Type: '{Type}'."
    )]
    public static partial void ParameterNotFound(this ILogger logger, string type, string name);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 50,
        Level = LogLevel.Warning,
        Message = "Date and/or time arithmetic failed, offending value '{DateTime}'."
    )]
    public static partial void DateTimeArithmeticFailed(this ILogger logger, Exception exception, DateTimeOffset dateTime);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 51,
        Level = LogLevel.Warning,
        Message = "Date and/or time format failed."
    )]
    public static partial void FormatDateTimeFailed(this ILogger logger, Exception exceptions);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 60,
        Level = LogLevel.Debug,
        Message = "Created composite at daylight factor {DaylightFactor}. Images: '{Images}' | Opacity: {Opacity}."
    )]
    public static partial void CompositeCreated(this ILogger logger, double daylightFactor, string images, string opacity);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 61,
        Level = LogLevel.Debug,
        Message = "Created hatch pattern '{Identifier}'. Opacity: {Opacity1}..{Opacity2} | Dimension: {Width}x{Height}."
    )]
    public static partial void HatchPatternCreated(this ILogger logger, string identifier, double opacity1, double opacity2, int width, int height);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 62,
        Level = LogLevel.Debug,
        Message = "'deskimg.background.setOverlay' completed successfully. Blob: '{Blob}'."
    )]
    public static partial void SetBackgroundOverlayCompleted(this ILogger logger, string blob);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 63,
        Level = LogLevel.Debug,
        Message = "'deskimg.background.update' completed successfully. Blob: '{Blob}'."
    )]
    public static partial void UpdateBackgroundCompleted(this ILogger logger, string blob);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 64,
        Level = LogLevel.Debug,
        Message = "'deskimg.helper.setPictureBlob' completed successfully. Id: '{Id}'."
    )]
    public static partial void SetPictureBlobCompleted(this ILogger logger, string id);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 65,
        Level = LogLevel.Debug,
        Message = "'deskimg.helper.setBackgroundImage' completed successfully."
    )]
    public static partial void SetBackgroundImageCompleted(this ILogger logger);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 66,
        Level = LogLevel.Warning,
        Message = "Unsupported weather symbols manifest format '{Input}'."
    )]
    public static partial void ManifestFormatNotSupported(this ILogger logger, string input);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 67,
        Level = LogLevel.Warning,
        Message = "Ignoring mismatched weather symbol '{Condition}'."
    )]
    public static partial void ManifestMismatch(this ILogger logger, WeatherConditions? condition);

    /// <see cref="ILogger"/>
    [LoggerMessage(
        EventId = 68,
        Level = LogLevel.Debug,
        Message = "Notification completed successfully. Tag: {Tag} | Title: {Title}."
    )]
    public static partial void NotifyCompleted(this ILogger logger, string tag, string? title);
}
