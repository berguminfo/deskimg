// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// deserializers sets the entire collection
#pragma warning disable CA2227 // Collection properties should be read only

using System.Text.Json.Serialization;

namespace Deskimg;

/// <summary>
/// Represents the known rendering modes.
/// </summary>
[JsonConverter(typeof(JsonStringEnumConverter<RenderingMode>))]
public enum RenderingMode {

    /// <summary>
    /// Render using <code>ImageElement</code> for both background and overlay pictures.
    /// </summary>
    Dom,

    /// <summary>
    /// Render using one <code>ImageElement</code> combining the background and overlay pictures.
    /// </summary>
    Blob,

    /// <summary>
    /// Render the <see cref="Blob"/> image using a <code>background-image</code> CSS property.
    /// </summary>
    Css
}

/// <summary>
/// Represents the Session configuration section.
/// </summary>
public sealed class SessionOptions {

    /// <summary>
    /// The configuration section name.
    /// </summary>
    public const string Name = "Session";

    /// <summary>
    /// Gets or sets the layout sets.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>🌐={xs:string}</code>.
    /// <para/>
    /// The current policy is to override sets specified by previous loaded sets.
    /// </remarks>
    /// <example>
    /// The QueryString: <code>🌐=corp&amp;🌐=308&amp;🌐=TOS</code>.
    /// <para/>
    /// May after loading set the current set to be:
    /// <list type="bullet">
    /// <item><see cref="LayoutSet.Index"/> from the <code>corp</code> set.</item>
    /// <item><see cref="LayoutSet.Background"/> from the <code>308</code> set.</item>
    /// <item><see cref="LayoutSet.Geolocation"/> from the <code>TOS</code> set.</item>
    /// </list>
    /// </example>
    public IEnumerable<string>? Sets { get; internal set; }

    /// <summary>
    /// Gets or sets the time of interest.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>t={xs:dateTime | xs:date | xs:duration}</code>.
    /// <para/>
    /// The <code>duration-string</code> should be specified using the XML-Schema xs:duration specification.
    /// </remarks>
    /// <example>
    /// The QueryString: <code>t=2024-01-01&amp;t=PT12H</code>.
    /// <para/>
    /// Should after loading set <see cref="SessionService.Toi"/> to be:
    /// <code>2024-01-01T12:00:00</code>.
    /// </example>
    public DateTimeOffset? Toi { get; set; }

    /// <summary>
    /// Gets or sets the preferred language or choose the default browser language if not specified.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>lang={xs:language}</code>.
    /// </remarks>
    public string? Language { get; set; }

    /// <summary>
    /// Gets or sets the preferred location or request the current position if not specified.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>geo={xs:string}</code>.
    /// </remarks>
    public string? Location { get; set; }

    /// <summary>
    /// Gets or sets the current latitude.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>geo={xs:decimal},{xs:decimal}</code>.
    /// <para/>
    /// In order to set the current location the <see cref="Longitude"/> must also be specified.
    /// </remarks>
    /// <example>
    /// The QueryString: <code>geo=-12.5,34.5</code>.
    /// <para/>
    /// Should after loading set <see cref="SessionService.GetLocationAsync"/> to be:
    /// <code>12°30′0″S 34°30′0″E</code>
    /// </example>
    public double? Latitude { get; set; }

    /// <summary>
    /// Gets or sets the current longitude.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>geo={xs:decimal},{xs:decimal}</code>.
    /// <para/>
    /// In order to set the current location the <see cref="Latitude"/> must also be specified.
    /// </remarks>
    /// <example>
    /// <see cref="Latitude"/>.
    /// </example>
    public double? Longitude { get; set; }

    /// <summary>
    /// Gets or sets the current altitude.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>z={xs:decimal}</code>.
    /// </remarks>
    public double? Altitude { get; set; }

    /// <summary>
    /// Gets or sets the navigation pane visibility.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>nav=</code> or <code>nav=✓</code>.
    /// </remarks>
    public bool? Navigation { get; set; }

    /// <summary>
    /// Gets or sets the update interval or keep static if not specified.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>upd={xs:time}</code>.
    /// </remarks>
    public TimeSpan? UpdateInterval { get; set; }

    /// <summary>
    /// Gets or sets the background source URI.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>bg={xs:anyURI}</code>.
    /// </remarks>
    public string? BackgroundSource { get; set; }

    /// <summary>
    /// Gets or sets the background mime type.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>bgtype={xs:string}</code>.
    /// </remarks>
    public string? BackgroundType { get; set; }

    /// <summary>
    /// Gets or sets the background rendering mode.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>bgmode={Dom | Blob | Css}</code>.
    /// </remarks>
    public RenderingMode? BackgroundRendering { get; set; }

    /// <summary>
    /// Gets or sets the value to set the data-bs-theme attribute.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>theme={Auto | Dark | Light}</code>
    /// </remarks>
    public DocumentTheme? Theme { get; set; }

    /// <summary>
    /// Gets or sets alert notification visibility.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>nf=</code> or <code>nf=✓</code>.
    /// </remarks>
    public bool? EnableNotification { get; set; }

    /// <summary>
    /// Gets or sets the viewport clipping rules.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>cw={xs:positiveInteger}</code> and <code>ch={xs:positiveInteger}</code>.
    /// </remarks>
    public IList<string>? ClippingRules { get; set; }

    /// <summary>
    /// Gets or sets the search query.
    /// </summary>
    /// <remarks>
    /// QueryString: <code>q={xs:string}</code>.
    /// </remarks>
    public string? Search { get; set; }

#if DEBUG
    ///
    public DebugOptions? Dbug { get; set; }
#endif

}

#pragma warning restore CA2227 // Collection properties should be read only
