// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Extensions;

/// <summary>
/// Represents additional extension methods for component CSS style.
/// </summary>
public static class StyleExtensions {

    /// <summary>
    /// Gets the style attribute value for <see cref="IImmutableDictionary{TKey, TValue}"/>
    /// </summary>
    /// <param name="collection">The collection having styles.</param>
    /// <returns>A string similar to <code>property1:value1;property2:value2</code></returns>
    public static string ToStyleString<TKey, TValue>(this IImmutableDictionary<TKey, TValue> collection) =>
        string.Join(';', from item in collection select $"{item.Key}:{item.Value}");
}
