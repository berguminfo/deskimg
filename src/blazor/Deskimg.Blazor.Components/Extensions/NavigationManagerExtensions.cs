// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using Google.OpenLocationCode;

namespace Deskimg.Extensions;

/// <summary>
/// Represents additional extension methods for <see cref="NavigationManager"/>.
/// </summary>
public static partial class NavigationManagerExtensions {

    // The maximum value for latitude in degrees.
    private const int LatitudeMax = 90;

    // The maximum value for longitude in degrees.
    private const int LongitudeMax = 180;

    [GeneratedRegex(@"^(-?\d+\.?\d*)[,\s]+(-?\d+\.?\d*)")]
    private static partial Regex GeouriPattern { get; }

    [GeneratedRegex(@"^([\+23456789CFGHJMPQRVWX]+)")]
    private static partial Regex CodePattern { get; }

    /// <summary>
    /// Gets the <see cref="SessionOptions"/> from parsing the extension object query string.
    /// </summary>
    /// <param name="navigation">The extension object.</param>
    /// <returns>The result of parsing the query string.</returns>
    public static SessionOptions GetOptions(this NavigationManager navigation) {
        SessionOptions options = new();
        int index = navigation.Uri.IndexOf('?', StringComparison.Ordinal);
        if (index > 0) {
            var queries = QueryHelpers.ParseQuery(navigation.Uri.AsSpan()[index..].ToString());
            foreach (var item in queries) {
                if (!StringValues.IsNullOrEmpty(item.Value) && s_handlers.TryGetValue(item.Key.ToUpperInvariant(), out var handler)) {
                    handler(options, item.Value);
                }
            }
        }

        return options;
    }

    static readonly Dictionary<string, Action<SessionOptions, StringValues>> s_handlers = new() {
        // NOTE: ensure all keys is in UPPERCASE
        ["🌐"] = (options, values) => {
            options.Sets = values;
        },
        ["SET"] = (options, values) => {
            options.Sets = values;
        },
        ["T"] = (options, values) => {
            foreach (string? value in values) {
                if (DateTimeOffset.TryParse(value, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal, out var toi)) {
                    options.Toi = toi;
                } else
                if (value is not null) {
                    try {
                        TimeSpan duration = XmlConvert.ToTimeSpan(value);
                        options.Toi = (options.Toi ?? DateTimeOffset.Now).Add(duration);
                    } catch (FormatException) {
                    }
                }
            }
        },
        ["LANG"] = (options, values) => {
            options.Language = values.Last();
        },
        ["L"] = (options, values) => {
            options.Language = values.Last();
        },
        ["GEO"] = (options, values) => {
            Match m;
            string value = values.Last() ?? string.Empty;

            // check if it's a GeoURI i.e. '-12.3,-45.6'
            m = GeouriPattern.Match(value);
            if (m.Success) {
                if (double.TryParse(m.Groups[1].Value, CultureInfo.InvariantCulture, out var latitude) && -LatitudeMax <= latitude && latitude <= LatitudeMax) {
                    options.Latitude = latitude;
                }
                if (double.TryParse(m.Groups[2].Value, CultureInfo.InvariantCulture, out var longitude) && -LongitudeMax <= longitude && longitude <= LongitudeMax) {
                    options.Longitude = longitude;
                }
            } else {

                // check if it's a PlusCode i.e. '7JVW52GR+2V Location'
                m = CodePattern.Match(value);
                if (m.Success && OpenLocationCode.IsValid(m.Groups[1].Value)) {

                    var codeArea = OpenLocationCode.Decode(m.Groups[1].Value);
                    options.Latitude = codeArea.Center.Latitude;
                    options.Longitude = codeArea.Center.Longitude;
                } else {

                    // otherwise it must be a Geolocation key
                    options.Location = value;
                }
            }
        },
        // Julia: \phi
        ["Φ"] = (options, values) => {
            if (double.TryParse(values.Last()?.Replace(',', '.'), CultureInfo.InvariantCulture, out var latitude) && -LatitudeMax <= latitude && latitude <= LatitudeMax) {
                options.Latitude = latitude;
            }
        },
        // Julia: \lambda
        ["Λ"] = (options, values) => {
            if (double.TryParse(values.Last()?.Replace(',', '.'), CultureInfo.InvariantCulture, out var longitude) && -LongitudeMax <= longitude && longitude <= LongitudeMax) {
                options.Longitude = longitude;
            }
        },
        ["Z"] = (options, values) => {
            if (double.TryParse(values.Last()?.Replace(',', '.'), CultureInfo.InvariantCulture, out var altitude)) {
                options.Altitude = altitude;
            }
        },
        ["NAV"] = (options, values) => {
            options.Navigation = values.Last()?.Length > 0;
        },
        ["UPD"] = (options, values) => {
            if (TimeSpan.TryParse(values.Last(), out TimeSpan updateInterval)) {
                options.UpdateInterval = updateInterval;
            }
        },
        ["BG"] = (options, values) => {
            options.BackgroundSource = values.Last();
        },
        ["BGTYPE"] = (options, values) => {
            options.BackgroundType = values.Last();
        },
        ["BGMODE"] = (options, values) => {
            if (Enum.TryParse<RenderingMode>(values.Last(), out var backgroundRendering)) {
                options.BackgroundRendering = backgroundRendering;
            }
        },
        ["BKG"] = (options, values) => {
            if (Enum.TryParse<RenderingMode>(values.Last(), out var backgroundRendering)) {
                options.BackgroundRendering = backgroundRendering;
            }
        },
        ["THEME"] = (options, values) => {
            if (Enum.TryParse<DocumentTheme>(values.Last(), out var theme)) {
                options.Theme = theme;
            }
        },
        ["NF"] = (options, values) => {
            options.EnableNotification = values.Last()?.Length > 0;
        },
        ["CW"] = (options, values) => {
            if (uint.TryParse(values.Last(), out var minWidth)) {
                if (options.ClippingRules is null) options.ClippingRules = [];
                options.ClippingRules.Add($"screen and (min-width: {minWidth}px)");
            }
        },
        ["CH"] = (options, values) => {
            if (uint.TryParse(values.Last(), out var minHeight)) {
                if (options.ClippingRules is null) options.ClippingRules = [];
                options.ClippingRules.Add($"screen and (min-height: {minHeight}px)");
            }
        },
        ["Q"] = (options, values) => {
            options.Search = string.Join(' ', values.ToArray() ?? []);
        },
#if DEBUG
        ["DBUG:FETCH"] = (options, values) => {
            if (options.Dbug is null) options.Dbug = new();
            if (values.Last()?.Length > 1) {
                options.Dbug.Fetch = $"fetch.api:{values.Last()}";
            } else
            if (values.Last()?.Length == 1) {
                options.Dbug.Fetch = "sample-data";
            }
        },
        ["DBUG:SETS"] = (options, values) => {
            if (options.Dbug is null) options.Dbug = new();
            if (values.Last()?.Length > 1) {
                options.Dbug.Sets = $"sets.api:{values.Last()}";
            } else
            if (values.Last()?.Length == 1) {
                options.Dbug.Sets = "sets-data";
            }
        },
        ["DBUG:CALDAV"] = (options, values) => {
            if (options.Dbug is null) options.Dbug = new();
            if (values.Last()?.Length > 1) {
                options.Dbug.CalDAV = $"caldav.api:{values.Last()}";
            }
        },
#endif
    };
}
