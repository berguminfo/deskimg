// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net;

namespace Deskimg.Extensions;

/// <summary>
/// Represents additional extension methods for the Hosting namespace.
/// </summary>
public static class ConfigurationExtensions {

    /// <summary>
    /// Loads the 'hosting.json' configuration.
    /// </summary>
    public static async Task<IConfigurationBuilder> LoadHostingConfigurationAsync(this IConfigurationBuilder configurationBuilder, string baseAddress, string hostingFile = "hosting.json") {
        try {
            using HttpClient http = new() {
                BaseAddress = new Uri(baseAddress),
                DefaultRequestVersion = HttpVersion.Version30,
                DefaultVersionPolicy = HttpVersionPolicy.RequestVersionOrLower
            };
            using var response = await http.GetAsync(new Uri(hostingFile, UriKind.Relative));
            using var stream = await response.Content.ReadAsStreamAsync();
            configurationBuilder.AddJsonStream(stream);
        } catch (Exception) {
        }

        return configurationBuilder;
    }
}
