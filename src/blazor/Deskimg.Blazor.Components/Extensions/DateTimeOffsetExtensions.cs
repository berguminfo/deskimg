// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Text;

namespace Deskimg.Extensions;

/// <summary>
/// Represents additional extension methods for <see cref="DateTimeOffset"/>.
/// </summary>
public static class DateTimeOffsetExtensions {

    /// <summary>
    /// Converts the current <see cref="DateTimeOffset"/> value to relative time representation.
    /// </summary>
    /// <remarks>
    /// Expected result:
    /// SPAN element +
    ///
    /// TIME element having:
    /// <list type="bullet">
    /// <item>@datetime: value.ToString("u")</item>
    /// <item>value.ToString("t")</item>
    /// </list>
    ///   
    /// SUB element having:
    /// <list type="bullet">
    /// <item>Empty in case the number of days from Now is 0.</item>
    /// <item>Ellipsis in case the number of days from Now is greater than 100 or lower than -100.</item>
    /// <item>Otherwise +/- the number of days from now.</item>
    /// </list>
    /// </remarks>
    /// <param name="when">The <see cref="DateTimeOffset"/> instance this method extends.</param>
    /// <param name="provider">An object that supplies culture-specific formatting information.</param>
    /// <returns>The markup string representation.</returns>
    public static MarkupString ToRelativeTimeString(this DateTimeOffset when, IFormatProvider? provider = null) {
        if (provider is null) {
            provider = CultureInfo.CurrentCulture;
        }

        StringBuilder sb = new();
        double days = (when.ToLocalTime().Date - DateTimeOffset.Now.Date).TotalDays;

        sb.Append("<span>");

        // Add <time> element.
        sb.AppendFormat(provider, @"<time datetime=""{0:u}"">{0:t}</time>", when.ToLocalTime());

        // Add possible <sub> element.
        //      ⟨∞, -100⟧ → `-…`
        //      ⟨-100, -1 → `-𝑥`
        //      ⟦100, ∞⟩ → `…`
        //      ⟦2, 100⟩ → `+𝑥`
        //      ⟦1, 2⟩ → `+`

        // NOTE: Branch ordering is significant.
        NumberFormatInfo numberFormat =
            provider?.GetFormat(days.GetType()) as NumberFormatInfo ??
            CultureInfo.InvariantCulture.NumberFormat;
        switch (days) {
            case <= -100.0:
                sb.AppendFormat(provider, "<sub>{0}…</sub>", numberFormat.NegativeSign);
                break;
            case <= -1.0:
                sb.AppendFormat(provider, "<sub>{0:F0}</sub>", days);
                break;
            case >= 100.0:
                sb.Append("<sub>…</sub>");
                break;
            case >= 2.0:
                sb.AppendFormat(provider, "<sub>{0}{1:F0}</sub>", numberFormat.PositiveSign, days);
                break;
            case >= 1.0:
                sb.AppendFormat(provider, "<sub>{0}</sub>", numberFormat.PositiveSign);
                break;
        }

        sb.Append("</span>");
        return (MarkupString)sb.ToString();
    }
}
