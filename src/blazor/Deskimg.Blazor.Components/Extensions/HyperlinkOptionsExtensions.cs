// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

/// <summary>
/// Represents additional extension methods for <see cref="HyperlinkOptions"/>.
/// </summary>
public static class HyperlinkOptionsExtensions {

    /// <see cref="HyperlinkOptions.Background"/>
    public static string GetBackgroundLink(this HyperlinkOptions link) =>
        MessageFormatter.Format(link.Background, new {
            Images = link.Images
        });

    /// <see cref="HyperlinkOptions.Intl"/>
    public static string GetIntlImage(this HyperlinkOptions link, string name) =>
        MessageFormatter.Format(link.Intl, new {
            Images = link.Images,
            Name = name
        });

    /// <see cref="HyperlinkOptions.Status"/>
    public static string GetStatusImage(this HyperlinkOptions link, string name) =>
        MessageFormatter.Format(link.Status, new {
            Images = link.Images,
            Name = name
        });

    /// <see cref="HyperlinkOptions.Warning"/>
    public static string GetWarningSymbol(this HyperlinkOptions link, string theme, string name) =>
        MessageFormatter.Format(link.Warning, new {
            Images = link.Images,
            Theme = theme,
            Name = name
        });

    /// <see cref="HyperlinkOptions.Weather"/>
    public static string GetWeatherSymbol(this HyperlinkOptions link, string theme, string name) =>
        MessageFormatter.Format(link.Weather, new {
            Images = link.Images,
            Theme = theme,
            Name = name
        });

    /// <see cref="HyperlinkOptions.WeatherManifest"/>
    public static string GetWeatherManifest(this HyperlinkOptions link, string theme) =>
        MessageFormatter.Format(link.WeatherManifest, new {
            Images = link.Images,
            Theme = theme
        });
}
