// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Deskimg;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
/// Represents additional extension methods for <see cref="IServiceCollection"/>.
/// </summary>
public static class ServiceCollectionExtensions {

    /// <summary>
    /// Extends <see cref="IServiceCollection"/> with <see cref="SessionOptions"/> options.
    /// </summary>
    public static IServiceCollection AddSessionOptions(this IServiceCollection services) {
        return services.AddScoped<IOptions<SessionOptions>>(serviceProvider => {
            var configuration = serviceProvider.GetRequiredService<IConfiguration>();
            var sessionSection = configuration.GetSection(SessionOptions.Name);
            SessionOptions options = sessionSection.Get<SessionOptions>() ?? new();
            options.Sets = sessionSection.GetSection("🌐").Get<IEnumerable<string>>();
            return new OptionsWrapper<SessionOptions>(options);
        });
    }

    /// <summary>
    /// Extends <see cref="IServiceCollection"/> with <see cref="NavigationManagerExtensions"/> options.
    /// </summary>
    public static IServiceCollection AddNavigationOptions(this IServiceCollection services) {
        services.AddKeyedScoped(nameof(NavigationManager), (sp, _) => sp.GetRequiredService<NavigationManager>().GetOptions());
        return services;
    }

    /// <summary>
    /// Extends <see cref="IServiceCollection"/> with <see cref="EndpointOptions"/> options.
    /// </summary>
    public static IServiceCollection AddEndpointOptions(this IServiceCollection services) {
        services.AddScoped(static sp => {
            var section = sp.GetRequiredService<IConfiguration>().GetSection(EndpointOptions.Name);
            var logger = sp.GetRequiredService<ILogger<EndpointOptions>>();
#if DEBUG
            var dbug =
                sp.GetRequiredKeyedService<SessionOptions>(nameof(NavigationManager)).Dbug ??
                sp.GetRequiredService<IOptions<SessionOptions>>().Value.Dbug ??
                new();
            return new EndpointOptions(dbug, logger, section);
#else
            return new EndpointOptions(logger, section);
#endif
        });
        return services;
    }
}
