// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

/// <summary>
/// Represents a implementation of <see cref="ISession"/>.
/// </summary>
public class SessionImplementationFactory(SessionService session) : ISession {

    /// <inheritdoc cref="ISession.Toi"/>
    public DateTimeOffset Toi => session.Toi;
}
