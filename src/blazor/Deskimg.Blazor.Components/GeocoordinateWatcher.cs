// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;

namespace Deskimg;

/// <summary>
/// Represents the geolocation watcher.
/// </summary>
[SupportedOSPlatform("browser")]
public static partial class GeocoordinateWatcher {

    const string ModuleName = "Interop";

    /// <summary>
    /// Gets the current position.
    /// </summary>
    public static Geoposition? Position { get; private set; }

    /// <summary>
    /// Starts the geolocation watcher.
    /// </summary>
    /// <returns>The operation task.</returns>
    public static async Task WatchAsync() {
        await ImportModuleAsync();
        await WatchAsyncInternal();
    }

    internal static Task<JSObject> ImportModuleAsync() =>
        JSHost.ImportAsync(ModuleName, "../_content/Deskimg.Blazor.Components/js/interop.js");

    [JSImport("watch", ModuleName)]
    internal static partial Task WatchAsyncInternal();

    [JSExport]
    internal static void OnPositionChanged(double latitude, double longitude, double altitude, double altitudeAccuracy) {
        if (Position is null || Position.Coordinate is null) {
            Position = new() {
                Coordinate = new()
            };
        }

        Position.Coordinate.Latitude = latitude;
        Position.Coordinate.Longitude = longitude;
        Position.Coordinate.Altitude = altitude;
        Position.Coordinate.AltitudeAccuracy = altitudeAccuracy;
    }
}
