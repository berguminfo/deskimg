// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// FIXME: without FORCED_ABORT, browse hangs the UI thread, but don't freeze
#define FORCED_ABORT

using System.Globalization;
using System.Net.Http.Json;
using System.Text.Json;
using Timer = System.Timers.Timer;
using Microsoft.Extensions.DependencyInjection;
using System.Runtime.Versioning;

namespace Deskimg;

/// <summary>
/// Represents the current active session.
/// </summary>
/// <remarks>
/// This service requires the <see cref="NavigationManager"/> to be initialized.
/// </remarks>
public partial class SessionService(
    ILogger<SessionService> logger,
    EndpointOptions endpoint,
    IOptions<SessionOptions> sessionOptions,
    [FromKeyedServices(nameof(NavigationManager))] SessionOptions navigationOptions,
    HttpClient http,
    IMemoryCache cache) : ISession, IDisposable {

    Timer? _updateTimer;
    Geoposition? _sessionLocation;
    readonly AsyncEventHandler<SessionService> _toiChangedEvent = new();
    readonly AsyncEventHandler<SessionService> _toiOrLocationChangedEvent = new();
    readonly AsyncEventHandler<SessionService> _onUpdateEvent = new();

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    /// <summary>
    /// Signaled when <see cref="Toi"/> has changed.
    /// </summary>
    public event AsyncEvent<SessionService> ToiChanged {
        add => _toiChangedEvent.Register(value);
        remove => _toiChangedEvent.Unregister(value);
    }

    /// <summary>
    /// Signaled when <see cref="Toi"/> or <see cref="GetLocationAsync"/> has changed.
    /// </summary>
    public event AsyncEvent<SessionService> ToiOrLocationChanged {
        add => _toiOrLocationChangedEvent.Register(value);
        remove => _toiOrLocationChangedEvent.Unregister(value);
    }

    /// <summary>
    /// Signaled when the component should update.
    /// </summary>
    ///
    public event AsyncEvent<SessionService> OnUpdate {
        add => _onUpdateEvent.Register(value);
        remove => _onUpdateEvent.Unregister(value);
    }

    /// <summary>
    /// Signaled when <see cref="RaiseOnAfterRender"/> has been called.
    /// </summary>
    public event EventHandler<bool>? OnAfterRender;

    /// <summary>
    /// Signaled when <see cref="Search"/> has changed.
    /// </summary>
    public event EventHandler? SearchChanged;

    /// <summary>
    /// Signaled when <see cref="ClippingRules"/> has changed.
    /// </summary>
    public event EventHandler? ClippingRulesChanged;

    /// <summary>
    /// Raises a <see cref="OnAfterRender"/> event.
    /// </summary>
    /// <param name="sender">The event sender.</param>
    /// <param name="firstRender">The event arguments.</param>
    public void RaiseOnAfterRender(object? sender, bool firstRender) {
        OnAfterRender?.Invoke(sender, firstRender);
    }

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromHours(6)
    };

    /// <inheritdoc cref="SessionOptions.Sets"/>
    public IEnumerable<string>? Sets => navigationOptions.Sets ?? sessionOptions.Value.Sets;

    /// <summary>
    /// Turns the real time clock on or off.
    /// </summary>
    public bool RealTimeClock { get; set; } = (navigationOptions.Toi ?? sessionOptions.Value.Toi) is null;

    /// <inheritdoc cref="SessionOptions.Toi"/>
    public DateTimeOffset Toi {
        get => navigationOptions.Toi ?? sessionOptions.Value.Toi ?? DateTimeOffset.Now;
        set {
            logger.SessionChanged(nameof(Toi), value, Toi);
            navigationOptions.Toi = value;
            _toiChangedEvent.InvokeAsync(this, GetNextAbortToken(nameof(_toiChangedEvent)));
            _toiOrLocationChangedEvent.InvokeAsync(this, GetNextAbortToken(nameof(_toiOrLocationChangedEvent)));
        }
    }

    /// <inheritdoc cref="SessionOptions.Language"/>
    public string? Language => navigationOptions.Language ?? sessionOptions.Value.Language;

    /// <inheritdoc cref="SessionOptions.Location"/>
    public string? Location => navigationOptions.Location ?? sessionOptions.Value.Location;

    /// <inheritdoc cref="SessionOptions.Latitude"/>
    /// <remarks>
    /// Expect clients setting this property to also set <see cref="Toi"/> triggering the <see cref="ToiOrLocationChanged"/> event.
    /// </remarks>
    public double? Latitude {
        get => navigationOptions.Latitude ?? sessionOptions.Value.Latitude;
        set {
            logger.SessionChanged(nameof(Latitude), value, Latitude);
            navigationOptions.Latitude = value;

            // clear cache
            _sessionLocation = null;
        }
    }

    /// <inheritdoc cref="SessionOptions.Longitude"/>
    /// <remarks>
    /// Expect clients setting this property to also set <see cref="Toi"/> triggering the <see cref="ToiOrLocationChanged"/> event.
    /// </remarks>
    public double? Longitude {
        get => navigationOptions.Longitude ?? sessionOptions.Value.Longitude;
        set {
            logger.SessionChanged(nameof(Longitude), value, Longitude);
            navigationOptions.Longitude = value;

            // clear cache
            _sessionLocation = null;
        }
    }

    /// <inheritdoc cref="SessionOptions.Altitude"/>
    public double? Altitude {
        get => navigationOptions.Altitude ?? sessionOptions.Value.Altitude;
        set {
            logger.SessionChanged(nameof(Altitude), value, Altitude);
            navigationOptions.Altitude = value;

            // clear cache
            _sessionLocation = null;
        }
    }

    /// <inheritdoc cref="SessionOptions.Navigation"/>
    public bool Navigation => navigationOptions.Navigation ?? sessionOptions.Value.Navigation ?? false;

    /// <inheritdoc cref="SessionOptions.UpdateInterval"/>
    public TimeSpan? UpdateInterval => navigationOptions.UpdateInterval ?? sessionOptions.Value.UpdateInterval;

    /// <inheritdoc cref="SessionOptions.BackgroundSource"/>
    public string? BackgroundSource => navigationOptions.BackgroundSource ?? sessionOptions.Value.BackgroundSource;

    /// <inheritdoc cref="SessionOptions.BackgroundType"/>
    public string? BackgroundType => navigationOptions.BackgroundType ?? sessionOptions.Value.BackgroundType;

    /// <inheritdoc cref="SessionOptions.BackgroundRendering"/>
    public RenderingMode BackgroundRendering => navigationOptions.BackgroundRendering ?? sessionOptions.Value.BackgroundRendering ?? RenderingMode.Dom;

    /// <inheritdoc cref="SessionOptions.Theme"/>
    public DocumentTheme? Theme => navigationOptions.Theme ?? sessionOptions.Value.Theme;

    /// <inheritdoc cref="EnableNotification"/>
    public bool EnableNotification => navigationOptions.EnableNotification ?? sessionOptions.Value.EnableNotification ?? false;

    /// <inheritdoc cref="SessionOptions.ClippingRules"/>
    public IList<string>? ClippingRules {
        get => navigationOptions.ClippingRules ?? sessionOptions.Value.ClippingRules;
        internal set {
            logger.SessionChanged(nameof(ClippingRules), value, string.Join("; ", ClippingRules ?? []));
            navigationOptions.ClippingRules = value;
            ClippingRulesChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    /// <inheritdoc cref="SessionOptions.Search"/>
    public string Search {
        get => navigationOptions.Search ?? sessionOptions.Value.Search ?? string.Empty;
        set {
            logger.SessionChanged(nameof(Search), value, Search);
            navigationOptions.Search = value;
            SearchChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Called from the start page to set <see cref="Thread.CurrentCulture"/> from the
    /// specified <see cref="Language"/> and starts a timer to trigger <see cref="OnUpdate"/> events.
    /// </summary>
    /// <returns>A <see cref="Task"/> representing any asynchronous operation.</returns>
    public async Task InitializeAsync() {
        await SetCurrentCultureAsync();
        if (OperatingSystem.IsBrowser()) {
            StartUpdateTimer();
        }
    }

    /// <inheritdoc/>
    public void Dispose() {
        foreach (var cancellationToken in _lastAbortTokenSource.Values) {
            cancellationToken.Cancel();
            cancellationToken.Dispose();
        }

        logger.SchedulerStopped();
        _updateTimer?.Dispose();
    }


    readonly Dictionary<string, CancellationTokenSource> _lastAbortTokenSource = [];

    CancellationToken GetNextAbortToken(string name) {

        // cancel ongoing work
#if FORCED_ABORT
        if (_lastAbortTokenSource.TryGetValue(name, out var cancellationToken)) {
            cancellationToken.Cancel();
        }
#endif

        // create new token for work to be done
        _lastAbortTokenSource[name] = new();

        return _lastAbortTokenSource[name].Token;
    }

    /// <summary>
    /// Sets the current thread <see cref="CultureInfo"/> and loads the string
    /// resource for the given <see cref="Language"/>.
    /// </summary>
    /// <param name="cancellationToken">The abort token</param>
    /// <returns>The async result.</returns>
    public async Task SetCurrentCultureAsync(CancellationToken cancellationToken = default) {
        CultureInfo? culture = null;

        // language may be set in query string contains something like '?lang=nb-NO'
        if (Language?.Length >= 5) {
            try {
                culture = CultureInfo.CreateSpecificCulture(Language);
            } catch (CultureNotFoundException) {
                logger.CultureNotFound(Language);
            }
        }

        // otherwise clone the Accept-Language or default culture
        if (culture is null) {
            try {
                // NOTE: needs to create a new culture in order to clone
                culture = (CultureInfo)CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.Name).Clone();
            } catch (CultureNotFoundException) {

                // unable to clone the current culture, bail out
                logger.CultureNotFound(CultureInfo.CurrentCulture.Name);
                return;
            }
        }

        // initialize the strings resource to get DateTimeFormatInfo
        await SR.InitializeAsync(culture.Name, cancellationToken);

        // populate the DateTimeFormat properties
        var dtf = culture.DateTimeFormat;
        if (!string.IsNullOrWhiteSpace(SR.Default.YearMonthPattern)) {
            dtf.YearMonthPattern = SR.Default.YearMonthPattern;
        }
        if (SR.Default.MonthNames is not null) {
            dtf.MonthNames = [.. SR.Default.MonthNames.Value];
        }
        if (SR.Default.ShortestDayNames is not null) {
            dtf.ShortestDayNames = SR.Default.ShortestDayNames.Value
                .Select(x => x[0].ToString()).ToArray();
        }

        // apply the custom culture globally
        CultureInfo.DefaultThreadCurrentCulture = culture;
        CultureInfo.DefaultThreadCurrentUICulture = culture;
        CultureInfo.CurrentCulture = culture;
        CultureInfo.CurrentUICulture = culture;

        logger.LanguageChanged(culture.Name, SR.Default.Language);
    }

    /// <summary>
    /// Starts the update timer given the current set <see cref="UpdateInterval"/>.
    /// </summary>
    /// <remarks>
    /// For each elapsed interval the <see cref="UpdateTimer_Elapsed"/> will be invoked.
    /// </remarks>
    [SupportedOSPlatform("browser")]
    public void StartUpdateTimer() {
        if (UpdateInterval > TimeSpan.Zero) {
            _updateTimer = new(UpdateInterval.Value);
            _updateTimer.Elapsed += UpdateTimer_Elapsed;
            _updateTimer.Start();
            logger.SchedulerStarted(UpdateInterval.Value);
        }
    }

    void UpdateTimer_Elapsed(object? sender, EventArgs e) {
        DateTimeOffset? nextToi = null;

        // current time must have changed
        if (RealTimeClock) {
            nextToi = DateTimeOffset.Now;
        } else
        if (UpdateInterval is not null) {
            nextToi = Toi.Add(UpdateInterval.Value);
        }

        // trigger events: ToiChanged, ToiOrLocationChanged, OnUpdate
        if (nextToi is DateTimeOffset value) {
            Toi = value;
            _onUpdateEvent.InvokeAsync(this, GetNextAbortToken(nameof(OnUpdate)));
        }
    }

    string CacheKey => string.Join('\t', Sets ?? []);

    /// <summary>
    /// Signaled then the current set has changed.
    /// </summary>
    public event EventHandler? CurrentSetChanged;

    /// <inheritdoc cref="CurrentSetChanged"/>
    public void RaiseCurrentSetChanged(object sender) {
        if (cache.TryGetValue(CacheKey, out var _)) {
            CurrentSetChanged?.Invoke(sender, EventArgs.Empty);
        }
    }

    // at most one thread should create the CurrentSet at the time,
    // otherwise multiple CurrentSet exists leading to unexpected behavior
    static readonly SemaphoreSlim _createCurrentSetLock = new(1, 1);

    /// <summary>
    /// Gets the current set.
    /// </summary>
    /// <param name="cancellationToken">The abort token.</param>
    /// <returns>A <see cref="Task"/> representing any asynchronous operation.</returns>
    public async Task<LayoutSet> GetCurrentSetAsync(CancellationToken cancellationToken = default) {
        LayoutSet? result = null;
        if (Sets is not null) {

            // wait on any thread currently working on creating the one and only CurrentSet
            await _createCurrentSetLock.WaitAsync(cancellationToken);

            try {
                result = await cache.GetOrCreateAsync(CacheKey, async entry => {
                    entry.AbsoluteExpirationRelativeToNow = CacheControl.MaxAge;

                    LayoutSet currentSet = new();
                    foreach (var item in Sets) {

                        // fetch from remote and local locations
                        Uri? requestUri;
                        if (!Uri.TryCreate(item, UriKind.Absolute, out requestUri)) {
                            if (item.Contains('/', StringComparison.Ordinal)) {
                                requestUri = endpoint.CalDAV(item);
                            } else {
                                requestUri = endpoint.Sets(item);
                            }
                        }

                        if (requestUri is not null) {
                            try {
                                logger.FetchStarting(requestUri);
                                var response = await http.GetFromJsonAsync<LayoutSet>(
                                    requestUri,
                                    CacheControl,
                                    s_sourceGenOptions,
                                    cancellationToken);

                                if (response is not null) {

                                    // override Background
                                    if (response.Background is not null) {
                                        currentSet.Background = response.Background;
                                    }

                                    // override Index
                                    if (response.Index is not null) {
                                        currentSet.Index = response.Index;
                                    }

                                    // override Geolocation
                                    if (response.Geolocation.Count > 0) {
                                        currentSet.Geolocation.Clear();
                                        foreach (var geolocation in response.Geolocation) {
                                            currentSet.Geolocation[geolocation.Key] = geolocation.Value;
                                        }
                                    }
                                }
                            } catch (OperationCanceledException) {
                            } catch (HttpRequestException) {
                                // NOTE: polyfills.ts will log this exception
                            } catch (JsonException ex) {
                                logger.DeserializeJsonFailed(ex, requestUri, ex.LineNumber, ex.BytePositionInLine);
                            }
                        }
                    }

                    return currentSet;
                });
            } finally {
                _createCurrentSetLock.Release();
            }
        }

        return result ?? new();
    }

    /// <summary>
    /// Gets the current location.
    /// </summary>
    /// <param name="cancellationToken">The abort token</param>
    /// <returns>A <see cref="Task"/> representing any asynchronous operation.</returns>
    public async Task<Geoposition?> GetLocationAsync(CancellationToken cancellationToken = default) {
        Geoposition? result = null;

        // first choose user provided coordinates or secondly the geolocation place
        if (Latitude is not null && Longitude is not null) {
            if (_sessionLocation is null) {
                _sessionLocation = new() {
                    Coordinate = new() {
                        Latitude = Latitude.Value,
                        Longitude = Longitude.Value,
                        Altitude = Altitude
                    }
                };
            }

            result = _sessionLocation;
        } else
        if (!string.IsNullOrWhiteSpace(Location)) {
            var currentSet = await GetCurrentSetAsync(cancellationToken);
            if (currentSet.Geolocation.TryGetValue(Location, out var value)) {
                result = value;
            }
        }

        // fallback to geolocation sensor
        if (result is null && OperatingSystem.IsBrowser()) {
            result = GeocoordinateWatcher.Position;
        }

        return result;
    }
}
