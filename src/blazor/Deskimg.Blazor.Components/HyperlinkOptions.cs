// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg;

/// <summary>
/// Represents the Session:Href <see cref="IConfigurationSection"/>.
/// </summary>
public sealed class HyperlinkOptions {

    /// <summary>
    /// The <see cref="IConfiguration.GetSection"/> name.
    /// </summary>
    public const string Name = "Href";

    /// <summary>
    /// Gets or sets the images base href.
    /// </summary>
    public required string Images { get; set; }

    /// <summary>
    /// Gets or set the background image hyper-link.
    /// </summary>
    public required string Background { get; set; }

    /// <summary>
    /// Gets or sets the intl image message format template.
    /// </summary>
    public required string Intl { get; set; }

    /// <summary>
    /// Gets or sets the status image message format template.
    /// </summary>
    public required string Status { get; set; }

    /// <summary>
    /// Gets or sets the warning symbol message format template.
    /// </summary>
    public required string Warning { get; set; }

    /// <summary>
    /// Gets or sets the weather symbol message format template.
    /// </summary>
    public required string Weather { get; set; }

    /// <summary>
    /// Gets or sets the weather manifest message format template.
    /// </summary>
    public required string WeatherManifest { get; set; }
}
