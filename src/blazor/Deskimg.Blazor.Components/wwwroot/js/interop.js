// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

/**
 * Starts the geolocation watcher.
 */
export async function watch() {
    const { getAssemblyExports } = await globalThis.getDotnetRuntime(0);
    const exports = await getAssemblyExports("Deskimg.Blazor.Components.dll");
    const geopositionChanged = exports.Deskimg.GeocoordinateWatcher.OnPositionChanged;

    navigator.geolocation.watchPosition(
        async position => {
            const coords = position.coords;
            geopositionChanged(coords.latitude, coords.longitude, coords.altitude, coords.altitudeAccuracy);
        },
        positionError => console.error("geoposition error", positionError)
    );
}
