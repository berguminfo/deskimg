// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

async function autostart() {
    let blazorOptions = {};
    const response = await fetch("hosting.json");
    if (response.ok) {
        const hosting = await response.json();
        if (hosting) {
            if (typeof (hosting["register-service-worker"]) === "boolean" && hosting["register-service-worker"]) {
                navigator.serviceWorker.register("service-worker.js");
            }
            if (typeof (hosting["blazor-options"]) === "object") {
                blazorOptions = hosting["blazor-options"];
            }
        }
    }
    Blazor.start(blazorOptions);
}

autostart();
