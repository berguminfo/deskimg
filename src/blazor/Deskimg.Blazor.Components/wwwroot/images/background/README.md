### Production of the 408 theme

- Download the images from https://github.com/GNOME/gnome-backgrounds/.
- Convert the source 4096x4096 __JXL__ to __WEBP__ with [ImageMagick](https://www.imagemagick.org/).
```sh
magick convert {dark-file-name}.jxl 408-4096w-dark.webp
magick convert {light-file-name}.jxl 408-4096w-light.webp
magick convert {dark-file-name}.jxl -resize 1096x1096 408-1096w-dark.webp
magick convert {light-file-name}.jxl -resize 1096x1096 408-1096w-light.webp
```
- Optionaly adjust the dark images to be more redish similar to the `308` theme:
  - Hue filter should be 100°.
  - Chrome filter should be -20%.
  - Lightness filter should be 20%.
