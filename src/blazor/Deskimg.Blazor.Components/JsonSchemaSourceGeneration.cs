// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json.Serialization;

namespace Deskimg;

[JsonSourceGenerationOptions]
[JsonSerializable(typeof(LayoutSet))]
internal partial class JsonSchemaSourceGeneration : JsonSerializerContext { }
