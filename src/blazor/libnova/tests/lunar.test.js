import { describe, it, test } from "node:test";
import assert from "node:assert";
import { createTimeOfInterest } from "astronomy-bundle/time/index.js";
import { lunar } from "../src/index.js";
import { $ } from "./testfixture.js";

describe("LunarCelestialBody", () => {

    // ref.: https://www.timeanddate.com/sun/@69.5,18.5?month=3&year=2023

    const latitude = 69.5;
    const longitude = 18.5;

    describe("getRiseSet at 2023-03-10", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 3, 10);
        const actual = await lunar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return rise and set", () => {
            assert.equal($(actual.Rise), "2023-03-10T21:19");
            assert.equal($(actual.Set), "2023-03-10T05:37");
            assert.equal(actual.AboveHorizon, false);
            assert.equal(actual.BelowHorizon, false);
        });
    });

    describe("getRiseSet at 2023-01-14", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 1, 14);
        const actual = await lunar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return set only", () => {
            assert.equal(actual.Rise, null);
            assert.equal($(actual.Set), "2023-01-14T09:25");
            assert.equal(actual.BelowHorizon, false);
            assert.equal(actual.AboveHorizon, false);
        });
    });

    describe("getRiseSet at 2023-01-07", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 1, 7);
        const actual = await lunar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return up all day", () => {
            assert.equal(actual.Rise, null);
            assert.equal(actual.Set, null);
            assert.equal(actual.BelowHorizon, false);
            assert.equal(actual.AboveHorizon, true);
        });
    });

    describe("getRiseSet at 2023-01-21", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 1, 21);
        const actual = await lunar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return down all day", () => {
            assert.equal(actual.Rise, null);
            assert.equal(actual.Set, null);
            assert.equal(actual.BelowHorizon, true);
            assert.equal(actual.AboveHorizon, false);
        });
    });

    describe("getApparentTopocentricHorizontalCoordinates for 1 julian day", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 12, 22);
        const actual = await lunar.getApparentTopocentricHorizontalCoordinates(toi.getJulianDay(), latitude, longitude, 2 / 24, 1);
        it("should return 12 element having 2/24 step", async () => {
            assert.equal(12, actual.length);
        });
    });

    describe("getTimeOfInterestOfUpcomingPhase at 2023-12-11", () => {
        const toi = createTimeOfInterest.fromTime(2023, 12, 11);
        test("test upcoming new moon", async () => {
            const actual = await lunar.getUpcomingPhase(toi.getJulianDay(), 0);
            assert.equal($(actual, 10), "2023-12-12");
        });
        test("test upcoming first quarter", async () => {
            const actual = await lunar.getUpcomingPhase(toi.getJulianDay(), 250);
            assert.equal($(actual, 10), "2023-12-19");
        });
        test("test upcoming full moon", async () => {
            const actual = await lunar.getUpcomingPhase(toi.getJulianDay(), 500);
            assert.equal($(actual, 10), "2023-12-27");
        });
        test("test upcoming third quarter", async () => {
            const actual = await lunar.getUpcomingPhase(toi.getJulianDay(), 750);
            assert.equal($(actual, 10), "2024-01-04");
        });
    });
});
