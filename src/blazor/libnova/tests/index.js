import { readdir } from 'node:fs/promises';

for (const test of await readdir(import.meta.dirname)) {
    if (test.endsWith(".test.js")) {
        await import(`./${test}`);
    }
}
