export function $(date, length = 16) {
    return date.toISOString().substring(0, length);
}
