import { describe, it } from "node:test";
import assert from "node:assert";
import { createTimeOfInterest } from "astronomy-bundle/time/index.js";
import { solar } from "../src/index.js";
import { $ } from "./testfixture.js";

describe("SolarCelestialBody", () => {

    // ref.: https://www.timeanddate.com/sun/@69.5,18.5?month=3&year=2023

    const latitude = 69.5;
    const longitude = 18.5;

    describe("getRiseSet at march equinox", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 3, 20);
        const actual = await solar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return rise and set", () => {
            assert.equal($(actual.Rise), "2023-03-20T04:47");
            assert.equal($(actual.Set), "2023-03-20T17:02");
            assert.equal(actual.BelowHorizon, false);
            assert.equal(actual.AboveHorizon, false);
        });
    });

    describe("getRiseSet at june solstice", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 6, 21);
        const actual = await solar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return up all day", () => {
            assert.equal(actual.Rise, null);
            assert.equal(actual.Set, null);
            assert.equal(actual.BelowHorizon, false);
            assert.equal(actual.AboveHorizon, true);
        });
    });

    describe("getRiseSet at september equinox", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 9, 23);
        const actual = await solar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return rise and set", () => {
            assert.equal($(actual.Rise), "2023-09-23T04:28");
            assert.equal($(actual.Set), "2023-09-23T16:46");
            assert.equal(actual.BelowHorizon, false);
            assert.equal(actual.AboveHorizon, false);
        });
    });

    describe("getRiseSet at december solstice", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 12, 22);
        const actual = await solar.getRiseSet(toi.getJulianDay(), latitude, longitude);
        it("should return down all day at december solstice", async () => {
            assert.equal(actual.Rise, null);
            assert.equal(actual.Set, null);
            assert.equal(actual.BelowHorizon, true);
            assert.equal(actual.AboveHorizon, false);
        });
    });

    describe("getApparentTopocentricHorizontalCoordinates for 1 julian day", async () => {
        const toi = createTimeOfInterest.fromTime(2023, 12, 22);
        const actual = await solar.getApparentTopocentricHorizontalCoordinates(toi.getJulianDay(), latitude, longitude, 2 / 24, 1);
        it("should return 12 element having 2/24 step", async () => {
            assert.equal(12, actual.length);
        });
    });
});

