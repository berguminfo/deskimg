// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

import { createTimeOfInterest } from "astronomy-bundle/time/index.js";

/**
 * Gets the celestial body rise and set information.
 *
 * @param {Function} createCelestialBody - Function to create the celestial body.
 * @param {number} jd - The time of interest.
 * @param {Location} observer - The observer coordinates.
 * @param {number?} standardAltitude - The standard altitude.
 * @return {PromiseLike<RiseSet>}
 * @type {{
 *   Rise: Date | null,
 *   Set: Date | null,
 *   AboveHorizon: boolean,
 *   BelowHorizon: boolean
 * }} RiseSet
*/
export async function getRiseSet(createCelestialBody, jd, observer, standardAltitude) {
    const result = {
        Rise: null,
        Set: null,
        AboveHorizon: false,
        BelowHorizon: false
    };

    // calculate the accurate rise and set time.
    const toi = createTimeOfInterest.fromJulianDay(jd);
    const celestialBody = createCelestialBody(toi);
    try {
        const rise = await celestialBody.getRise(observer, standardAltitude);
        result.Rise = rise.getDate();
    } catch {
    }
    try {
        const set = await celestialBody.getSet(observer, standardAltitude);
        result.Set = set.getDate();
    } catch {
    }

    // find out if down or up all day applies
    if (!result.Rise && !result.Set) {
        const collection = await getApparentTopocentricHorizontalCoordinates(createCelestialBody, jd - 0.5, observer, 2 / 24, 1);
        const altitudes = collection.map(element => element.Y);
        const minAltitude = Math.min(...altitudes);
        const maxAltitude = Math.max(...altitudes);
        result.AboveHorizon = Math.abs(minAltitude) < Math.abs(maxAltitude);
        result.BelowHorizon = !result.AboveHorizon;
    }

    return result;
}

/**
 * Gets the celestial body apparent topocentric horizontal coordinates.
 *
 * @param {Function} createCelestialBody - Function to create the celestial body.
 * @param {number} jd - The time of interest.
 * @param {Location} observer - The observer coordinates.
 * @param {number} incr - The number of julian date units to increment each step.
 * @param {number} until - The number of julia date units to step until.
 * @return {PromiseLike<AltitudeItem[]>}
 * @type {{
*   X: Date,
*   Y: number
* }} AltitudeItem
*/
export async function getApparentTopocentricHorizontalCoordinates(createCelestialBody, jd, observer, incr, until) {
    const result = [];

    // dead-loop guard
    if (incr > 0) {
        for (let i = 0; i < until; i += incr) {
            const toi = createTimeOfInterest.fromJulianDay(jd + i);
            const celestialBody = createCelestialBody(toi);
            const coord = await celestialBody.getApparentTopocentricHorizontalCoordinates(observer);
            result.push({
                X: toi.getDate(),
                Y: coord.altitude
            });
        }
    }

    return result;
}
