// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

import { SolarCelestialBody } from "./solar.js";
import { LunarCelestialBody } from "./lunar.js";

export const solar = new SolarCelestialBody();
export const lunar = new LunarCelestialBody();
