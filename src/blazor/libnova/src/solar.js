// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

import { createSun } from "astronomy-bundle/sun/index.js";
import { STANDARD_ALTITUDE_SUN_UPPER_LIMB_REFRACTION } from "astronomy-bundle/constants/standardAltitude.js";
import { getRiseSet, getApparentTopocentricHorizontalCoordinates } from "./getRiseSet.js";

/**
 * Represents the solar celestial body.
 */
export class SolarCelestialBody {

    /**
     * @see getRiseSet.
     */
    getRiseSet(jd, latitude, longitude, standardAltitude = undefined) {
        return getRiseSet(createSun, jd, { lat: latitude, lon: longitude }, standardAltitude || STANDARD_ALTITUDE_SUN_UPPER_LIMB_REFRACTION);
    }

    /**
     * @see getApparentTopocentricHorizontalCoordinates
     */
    getApparentTopocentricHorizontalCoordinates(jd, latitude, longitude, incr, until) {
        return getApparentTopocentricHorizontalCoordinates(createSun, jd, { lat: latitude, lon: longitude }, incr, until);
    }
}
