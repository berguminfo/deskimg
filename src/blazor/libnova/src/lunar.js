// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

import { createTimeOfInterest } from "astronomy-bundle/time/index.js";
import { createMoon } from "astronomy-bundle/moon/index.js";
import { moonPhaseCalc } from "astronomy-bundle/utils/index.js";
import { STANDARD_ALTITUDE_MOON_CENTER_REFRACTION } from "astronomy-bundle/constants/standardAltitude.js";
import { getRiseSet, getApparentTopocentricHorizontalCoordinates } from "./getRiseSet.js";

/**
 * Represents the lunar celestial body.
 */
export class LunarCelestialBody {

    /**
     * @see getRiseSet.
     */
    getRiseSet(jd, latitude, longitude, standardAltitude = undefined) {
        return getRiseSet(createMoon, jd, { lat: latitude, lon: longitude }, standardAltitude || STANDARD_ALTITUDE_MOON_CENTER_REFRACTION);
    }

    /**
     * @see getApparentTopocentricHorizontalCoordinates
     */
    getApparentTopocentricHorizontalCoordinates(jd, latitude, longitude, incr, until) {
        return getApparentTopocentricHorizontalCoordinates(createMoon, jd, { lat: latitude, lon: longitude }, incr, until);
    }

    /**
     * Gets the upcoming moon phase.
     *
     * @param {number} jd - The time of interest.
     * @param {number} lunarPhase - 0 | 250 | 500 | 750
     * @return {Date}
     */
    getUpcomingPhase(jd, lunarPhase) {
        const toi = createTimeOfInterest.fromJulianDay(jd);
        const decimalYear = toi.getDecimalYear();
        const upcomingPhase = moonPhaseCalc.getTimeOfInterestOfUpcomingPhase(decimalYear, lunarPhase / 1000);
        return upcomingPhase.getDate();
    }

    /**
     * Gets the upcoming phase fraction.
     *
     * @param {number} jd - The time of interest.
     * @return {PromiseLike<number>} - 0 | 250 | 500 | 1000.
     */
    async getUpcomingPhaseFraction(jd) {
        const toi = createTimeOfInterest.fromJulianDay(jd);
        const body = createMoon(toi);
        const k = await body.getIlluminatedFraction() * 1000;
        return ((Math.floor((k / 250)) * 250) + 250) % 1000;
    }
}
