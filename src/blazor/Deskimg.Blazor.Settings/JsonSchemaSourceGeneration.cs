// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json.Serialization;

namespace Deskimg.Blazor.Settings;

[JsonSourceGenerationOptions]
[JsonSerializable(typeof(Strings))]
internal sealed partial class JsonSchemaSourceGeneration : JsonSerializerContext { }
