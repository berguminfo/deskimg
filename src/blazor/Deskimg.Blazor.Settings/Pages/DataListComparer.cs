// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Blazor.Settings.Pages;

sealed class DataListComparer<T>(IList<T> ordering) : IComparer<T> {

    public int Compare(T? x, T? y) {

        // if x or y is null, , leave them in original relative order
        if (x is null || y is null) {
            return 0;
        }

        int indexX = ordering.IndexOf(x);
        int indexY = ordering.IndexOf(y);

        // if both x and y exist in list, compare their indexes
        if (indexX != -1 && indexY != -1) {
            return indexX.CompareTo(indexY);
        }
        // if only x exists in list, it should come before y
        else if (indexX != -1) {
            return -1;
        }
        // if only y exists in list, it should come before x
        else if (indexY != -1) {
            return 1;
        }
        // if neither x nor y exists in list, leave them in original relative order
        else {
            return 0;
        }
    }
}
