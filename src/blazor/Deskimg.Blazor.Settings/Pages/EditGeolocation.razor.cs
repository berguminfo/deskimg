// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Collections.Immutable;

namespace Deskimg.Blazor.Settings.Pages;

using Deskimg.Composition;
using Deskimg.Meteorology;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class EditGeolocation(
    NavigationManager navigation,
    SessionService session,
    CompositionService composition) {

    // may be created on initialization
    Geoposition? _newItem;

    /// <summary>
    /// Gets or sets the current set.
    /// </summary>
    protected LayoutSet? CurrentSet { get; set; }

    /// <summary>
    /// Gets or sets the Id portion of the route.
    /// </summary>
    [Parameter]
    public string? Id { get; set; }

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        navigation.LocationChanged += (_, _) => {
            // clear previous iteration
            _newItem = null;
            StateHasChanged();
        };
        CurrentSet = await session.GetCurrentSetAsync();
    }

    (Geoposition, bool) GetGeoposition() {
        if (CurrentSet is not null && CurrentSet.Geolocation.TryGetValue(Id ?? string.Empty, out var geoposition)) {
            return (geoposition, false);
        } else {
            // create at most once
            _newItem = _newItem ?? new() {
                CivicAddress = new(),
                Coordinate = new()
            };
            return (_newItem, true);
        }
    }

    #region DataList Members

    Type[] s_contractTypes = [
        typeof(IAlertProvider),
        typeof(IObservationProvider),
        typeof(IForecastProvider)
    ];

    IEnumerable<string> TagsDataList =>
        from item in composition.Exports
        where s_contractTypes.Contains(item.Key.ContractType)
        orderby item.Key.ContractName
        select item.Key.ContractName;

    #endregion

    #region Heading Members

    void AddClicked() {
        if (CurrentSet is not null &&
            !string.IsNullOrWhiteSpace(Id) &&
            _newItem is not null &&
            !CurrentSet.Geolocation.ContainsKey(Id)) {

            // add and navigate to the edit page
            CurrentSet.Geolocation[Id] = _newItem;
            navigation.NavigateTo(navigation.GeopositionPage(Id));
        }
    }

    void RemoveClicked() {
        if (CurrentSet is not null && !string.IsNullOrWhiteSpace(Id)) {

            // remove and navigate home
            if (CurrentSet.Geolocation.Remove(Id)) {
                navigation.NavigateTo("/");
            }
        }
    }

    #endregion
}
