// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Blazor.Settings.Pages;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class EditBackground(
    NavigationManager navigation,
    SessionService session) {

    /// <summary>
    /// Gets or sets the current set.
    /// </summary>
    protected LayoutSet? CurrentSet { get; set; }

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        navigation.LocationChanged += (_, _) => StateHasChanged();
        CurrentSet = await session.GetCurrentSetAsync();
    }

    #region Overlay Members

    double _opacity1, _opacity2;

    BackgroundOverlay? GetOverlay() {
        BackgroundOverlay? overlay = CurrentSet?.Background?.Overlay;
        if (overlay is not null) {
            _opacity1 = overlay.Opacity?.FirstOrDefault() ?? 0.0;
            _opacity2 = overlay.Opacity?.Skip(1).FirstOrDefault() ?? 0.0;
        }
        return overlay;
    }

    void OnOpacityChanged() {
        if (CurrentSet?.Background?.Overlay is not null) {
            CurrentSet.Background.Overlay.Opacity = [_opacity1, _opacity2];
        }
    }

    double Opacity1 {
        get => _opacity1;
        set {
            _opacity1 = value;
            OnOpacityChanged();
        }
    }

    double Opacity2 {
        get => _opacity2;
        set {
            _opacity2 = value;
            OnOpacityChanged();
        }
    }

    #endregion

    #region Composite Members

    IEnumerable<BackgroundComposite> Composite =>
        CurrentSet?.Background?.Composite ?? [];

    #endregion
}
