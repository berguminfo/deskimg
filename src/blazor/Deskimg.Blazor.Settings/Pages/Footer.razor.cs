// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net;
using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Json;
using System.Timers;
using Timer = System.Timers.Timer;

namespace Deskimg.Blazor.Settings.Pages;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class Footer(
    NavigationManager navigation,
    ILogger<Footer> logger,
    SessionService session,
    EndpointOptions endpoint,
    HttpClient http) : IDisposable {

    string? SaveTo => session.Sets?.LastOrDefault(x => x.Contains('/', StringComparison.Ordinal));

    /// <inheritdoc cref="ComponentBase.OnInitializedAsync"/>
    protected override async Task OnInitializedAsync() {
        if (OperatingSystem.IsBrowser()) {
            await JSInterop.ImportModuleAsync();
        }

        // initialize delayed actions
        _fadeOutTimer.Elapsed += FadeOutTimerElapsed;
    }

    #region FadeOut Timer

    static readonly TimeSpan DelayedAction = TimeSpan.FromSeconds(5);

    Timer _fadeOutTimer = new() {
        AutoReset = false
    };

    void FadeOutTimerElapsed(object? sender, ElapsedEventArgs e) => CloseNotification();

    bool _disposed;

    /// <inheritdoc/>
    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <see href="https://learn.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose"/>
    protected virtual void Dispose(bool disposing) {
        if (!_disposed) {
            if (disposing) {
                _fadeOutTimer.Dispose();
            }

            _disposed = true;
        }
    }

    #endregion

    #region Notification Members

    sealed record Notification(string? Message, string? CssClass, string? Confirm = null);

    Notification? _notification;

    void ShowNotification(string message, int fadeIn, string cssClass = "text-body bg-body-tertiary", string? confirm = null) {
        _notification = new(message, $"{cssClass} fadeIn-{fadeIn}", confirm);
        StateHasChanged();

        _fadeOutTimer.Stop();
        _fadeOutTimer.Interval = DelayedAction.TotalMilliseconds;
        _fadeOutTimer.Start();
    }

    void CloseNotification() {
        _notification = new(_notification?.Message, "fadeOut");
        StateHasChanged();
    }

    #endregion

    #region Click Event Handlers

    enum MessageBoxResult {
        None = 0,
        OK = 1,
        Cancel = 2,
        Yes = 6,
        No = 7
    }

    static readonly JsonSerializerOptions s_serializerOptions = new() {
        WriteIndented = true,
        PropertyNamingPolicy = null
    };

    async void OkClicked() {
        Uri? requestUri = SaveTo is not null ? endpoint.CalDAV(SaveTo) : null;
        if (requestUri is not null) {
            try {
                // NOTE: use the same serializer as the /export page
                var currentSet = await session.GetCurrentSetAsync(default);
                string body = JsonSerializer.Serialize(currentSet, s_serializerOptions);
                using var content = new StringContent(body, Encoding.UTF8, MediaTypeNames.Application.Json);

                using HttpRequestMessage request = new(HttpMethod.Put, requestUri);
                request.AddHeaders();
                request.Content = content;
                var response = await http.SendAsync(request);
                response.EnsureSuccessStatusCode();
                ShowNotification(SR.Default.Footer.SaveCompleted, fadeIn: 2, "text-bg-success");

                // navigate to first tab, and refresh
                if (OperatingSystem.IsBrowser()) {
                    JSInterop.PostInteropMessage((int)MessageBoxResult.OK);
                }
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, requestUri, ex.StatusCode);
                ShowNotification(ex.Message, fadeIn: 1, "text-bg-danger");
            }
        }
    }

    void CancelClicked() {
        CloseNotification();
        if (OperatingSystem.IsBrowser()) {
            JSInterop.PostInteropMessage((int)MessageBoxResult.Cancel);
        }
    }

    void ResetClicked() {
        ShowNotification(SR.Default.Footer.ConfirmReset, fadeIn: 1, "text-bg-warning", confirm: nameof(ResetClicked));
    }

    async void ConfirmClicked(string confirmation) {
        if (confirmation == nameof(ResetClicked)) {
            Uri? requestUri = SaveTo is not null ? endpoint.CalDAV(SaveTo) : null;
            if (requestUri is not null) {
                try {
                    using HttpRequestMessage request = new(HttpMethod.Delete, requestUri);
                    request.AddHeaders();
                    var response = await http.SendAsync(request);
                    response.EnsureSuccessStatusCode();
                    CloseNotification();

                    // navigate home
                    navigation.NavigateTo("/", true);
                } catch (HttpRequestException ex) {
                    logger.FetchFailed(ex, requestUri, ex.StatusCode);
                    if (ex.StatusCode == HttpStatusCode.NotFound) {
                        ShowNotification(SR.Default.Footer.ResetIgnored, fadeIn: 2);
                    } else {
                        ShowNotification(ex.Message, fadeIn: 1, "text-bg-danger");
                    }
                }
            }
        }
    }

    #endregion

    [SupportedOSPlatform("browser")]
    static partial class JSInterop {

        const string ModuleName = "Settings.Footer";

        internal static Task<JSObject> ImportModuleAsync() =>
            JSHost.ImportAsync(ModuleName, "../js/interop.js");

        [JSImport("postInteropMessage", ModuleName)]
        internal static partial void PostInteropMessage(int message);
    }
}
