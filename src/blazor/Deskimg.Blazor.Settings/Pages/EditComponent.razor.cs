// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.ComponentModel.DataAnnotations;
using System.Text.Json;

namespace Deskimg.Blazor.Settings.Pages;

using Deskimg.Composition;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class EditComponent(
    NavigationManager navigation,
    IConfiguration configuration,
    SessionService session,
    CompositionService compositionService) {

    sealed record ComponentType(string Name, IDictionary<string, JsonElement> Parameters);

    /// <summary>
    /// Gets or sets the current set.
    /// </summary>
    protected LayoutSet? CurrentSet { get; set; }

    /// <summary>
    /// Gets or sets the GridArea portion of the route.
    /// </summary>
    [Parameter]
    public int GridArea { get; set; }

    /// <summary>
    /// Gets or sets the ComponentIndex portion of the route.
    /// </summary>
    [Parameter]
    public int ComponentIndex { get; set; }

    /// <summary>
    /// Gets or sets the ContainerArea portion of the route.
    /// </summary>
    [Parameter]
    public int? ContainerGridArea { get; set; }

    /// <summary>
    /// Gets or sets the ContainerComponent portion of the route.
    /// </summary>
    [Parameter]
    public int? ContainerComponentIndex { get; set; }

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        navigation.LocationChanged += (_, _) => StateHasChanged();
        CurrentSet = await session.GetCurrentSetAsync(default);
    }

    static IndexType? GetIndex(LayoutSet currentSet, int gridArea) {
        IndexType? result = null;
        if (currentSet.Index is not null &&
            gridArea >= 0 && gridArea < currentSet.Index.Count) {

            // index is in collection
            result = currentSet.Index[gridArea];
        }

        return result;
    }

    // NOTE: this method should return null instead of empty collection due to routing logic
    static IList<IndexType>? GetComponentIndex(ComponentType component) {
        IList<IndexType>? result = null;
        if (component.Parameters.TryGetValue(nameof(Components.Container.Index), out var indexElement) &&
            JsonSerializer.Deserialize<IList<IndexType>>(indexElement) is IList<IndexType> index) {

            // index is the collection
            result = index;
        }

        return result;
    }

    static IndexType? GetComponentIndex(ComponentType component, int gridArea) {
        IndexType? result = null;
        if (GetComponentIndex(component) is IList<IndexType> index &&
            gridArea >= 0 &&
            gridArea < index.Count) {

            // index is in collection
            // NOTE: this index can't be modified directly, it's a component parameter
            result = index[gridArea];
        }

        return result;
    }

    static ComponentType? GetComponent(IndexType index, int componentIndex) {
        ComponentType? result = null;
        if (index.Component is not null &&
            componentIndex >= 0 &&
            componentIndex < index.Component.Count) {

            // component is in collection
            var keyValue = index.Component[componentIndex].FirstOrDefault();
            result = new(keyValue.Key, keyValue.Value);
        }

        return result;
    }

    IndexType? GetIndex() {
        IndexType? result = null;
        if (CurrentSet is LayoutSet currentSet &&
            GetIndex(currentSet, GridArea) is IndexType index) {

            // index found
            result = index;
        }

        return result;
    }

    ComponentType? GetComponent() {
        ComponentType? result = null;
        if (GetIndex() is IndexType index &&
            GetComponent(index, ComponentIndex) is ComponentType component) {

            if (component.Name == nameof(Components.Container) &&
                ContainerGridArea is int containerGridArea &&
                ContainerComponentIndex is int containerComponentIndex) {

                // component found
                // NOTE: this component can't be modified directly, it must be serialized
                result =
                    GetComponentIndex(component, containerGridArea) is IndexType containerIndex &&
                    GetComponent(containerIndex, containerComponentIndex) is ComponentType containerComponent ? containerComponent : null;
            } else {

                // component found and can be modified directly
                result = component;
            }
        }

        return result;
    }

    Dictionary<string, string> GetImplementorParameters(ComponentType component) {
        Dictionary<string, string> result = [];
        var implementorType = compositionService.GetExport<IDesklet>(component.Name);
        if (implementorType is not null) {
            foreach (var pi in implementorType.GetProperties()) {

                // ensure this property is a ParameterAttribute property
                if (pi.GetCustomAttributes(true).OfType<ParameterAttribute>().FirstOrDefault() is not null) {
                    string? customDataType = pi.GetCustomAttributes(true)
                        .OfType<DataTypeAttribute>().FirstOrDefault()?.CustomDataType;
                    string? propertyType = pi.PropertyType.FullName;

                    result[pi.Name] = customDataType ?? propertyType ?? string.Empty;
                }
            }
        }

        return result;
    }

    #region DataList Members

    IEnumerable<string>? GetStringSetDataList(string key) =>
        configuration.GetSection($"DataList:{key}").Get<IList<string>>();

    IDictionary<string, string[]?>? GetStringMapDataList(string key) =>
        configuration.GetSection($"DataList:{key}").Get<IDictionary<string, string[]?>>();

    IDictionary<string, string>? GetEpgSourceDataList(string key = "EpgSource") =>
        configuration.GetSection($"DataList:{key}").Get<IDictionary<string, string>>();

    IEnumerable<string> GetGeopositionDataList() =>
        from item in CurrentSet?.Geolocation
        orderby item.Key
        select item.Key;

    IEnumerable<string> GetExportsDataList(params Type[] include) =>
        from item in compositionService.Exports
        where include.Contains(item.Key.ContractType)
        orderby item.Key.ContractName
        select item.Key.ContractName;

    IEnumerable<string> GetExportsDataList(string key, params Type[] include) {
        var exports = GetExportsDataList(include);

        // if configuration has key, let initial exports be sorted on this list
        // - expect configuration sections to be a IList<T>
        if (GetStringSetDataList(key) is IList<string> ordering) {
            var copy = exports.ToList();
            copy.Sort(new DataListComparer<string>(ordering));
            exports = copy;
        }

        return exports;
    }

    #endregion

    #region Heading Members

    void RemoveClicked() {
        if (GetIndex() is IndexType index &&
            index.Component is not null &&
            ComponentIndex >= 0 &&
            ComponentIndex < index.Component.Count &&
            ContainerGridArea is null &&
            ContainerComponentIndex is null) {

            // remove and navigate home
            index.Component.RemoveAt(ComponentIndex);
            navigation.NavigateTo("/");
        }
    }

    void MoveClicked(int direction) {
        int srcIndex = ComponentIndex;
        int dstIndex = srcIndex + direction;
        if (GetIndex() is IndexType index &&
            index.Component is not null &&
            dstIndex >= 0 &&
            dstIndex < index.Component.Count &&
            ContainerGridArea is null &&
            ContainerComponentIndex is null) {

            // swap srcIndex and dstIndex and navigate to new url
            (index.Component[dstIndex], index.Component[srcIndex]) = (index.Component[srcIndex], index.Component[dstIndex]);
            navigation.NavigateTo(navigation.ComponentPage(GridArea, dstIndex));
        }
    }

    #endregion

}
