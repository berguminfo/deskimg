// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json;

namespace Deskimg.Blazor.Settings.Pages;

using Deskimg.Composition;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class EditIndex(
    IConfiguration configuration,
    NavigationManager navigation,
    SessionService session,
    CompositionService compositionService) {

    sealed record Item(int Index, string Title);

    // may be created on initialization
    IndexType? _newItem;

    /// <summary>
    /// Gets or sets the current set.
    /// </summary>
    protected LayoutSet? CurrentSet { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="IndexType.Component"/> witch might be added. 
    /// </summary>
    protected string? BindValue { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="IndexType.Class"/> value.
    /// </summary>
    protected string? Class {
        get => GetIndex()?.Class;
        set {
            if (GetIndex() is IndexType index) {
                // set value and notify the sidebar
                index.Class = value;
                session.RaiseCurrentSetChanged(this);
            }
        }
    }

    /// <summary>
    /// Gets or sets the GridArea portion of the route.
    /// </summary>
    [Parameter]
    public int? GridArea { get; set; }

    /// <summary>
    /// Gets or sets the ComponentIndex portion of the route.
    /// </summary>
    [Parameter]
    public int? ComponentIndex { get; set; }

    /// <summary>
    /// Gets or sets the ContainerGridArea portion of the route.
    /// </summary>
    [Parameter]
    public int? ContainerGridArea { get; set; }

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        navigation.LocationChanged += (_, _) => {
            BindValue = null;
            StateHasChanged();
        };
        CurrentSet = await session.GetCurrentSetAsync();
    }

    IndexType? GetIndex() {
        if (CurrentSet?.Index is not null &&
            GridArea >= 0 &&
            GridArea < CurrentSet.Index.Count) {

            return CurrentSet.Index[GridArea.Value];
        } else
        if (CurrentSet?.Index is not null &&
            GridArea >= CurrentSet.Index.Count) {

            // create at most once
            _newItem ??= new() { Component = [] };
            return _newItem;
        } else {
            return null;
        }
    }

    List<Item> GetComponents() {
        List<Item> result = [];
        if (GetIndex() is IndexType index && index.Component is not null) {
            for (int i = 0; i < index.Component.Count; ++i) {
                result.Add(new(i, index.Component[i].FirstOrDefault().Key));
            }
        }

        return result;
    }

    IEnumerable<string> GetDeskletDataList(params Type[] exclude) =>
        from item in compositionService.Exports
        where
            item.Key.ContractType == typeof(IDesklet) &&
            !exclude.Contains(item.Value)
        orderby item.Key.ContractName
        select item.Key.ContractName;

    #region Heading Members

    void AddClicked() {
        if (CurrentSet?.Index is not null &&
            !string.IsNullOrEmpty(Class) &&
            _newItem is not null) {

            // add and navigate to the edit page
            CurrentSet.Index.Add(_newItem);
            _newItem = null;
            navigation.NavigateTo(navigation.IndexPage(CurrentSet.Index.Count - 1));
        }
    }

    void RemoveClicked() {
        if (CurrentSet?.Index is not null &&
            GridArea >= 0 &&
            GridArea < CurrentSet.Index.Count) {

            // remove and navigate home
            CurrentSet.Index.RemoveAt(GridArea.Value);
            navigation.NavigateTo("/");
        }
    }

    void MoveClicked(int direction) {
        if (GridArea is not null) {
            int srcIndex = GridArea.Value;
            int dstIndex = srcIndex + direction;
            if (CurrentSet?.Index is not null &&
                srcIndex >= 0 && srcIndex < CurrentSet.Index.Count &&
                dstIndex >= 0 && dstIndex < CurrentSet.Index.Count) {

                // swap srcIndex and dstIndex and navigate to the edit page
                (CurrentSet.Index[dstIndex], CurrentSet.Index[srcIndex]) = (CurrentSet.Index[srcIndex], CurrentSet.Index[dstIndex]);
                navigation.NavigateTo(navigation.IndexPage(dstIndex));
            }
        }
    }

    #endregion

    #region Component Members

    static Dictionary<string, object?> GetSectionAsDictionary(IConfigurationSection section) =>
        section.GetChildren().ToDictionary(
            x => x.Key,
            x => x.GetChildren().Any() ? (object)GetSectionAsDictionary(x) : x.Value);

    void AddComponentClicked() {
        if (!string.IsNullOrWhiteSpace(BindValue) &&
            GetIndex() is IndexType index &&
            index.Component is not null) {

            // create the component parameters with initial values
            Dictionary<string, JsonElement> parameters = [];
            var implementorType = compositionService.GetExport<IDesklet>(BindValue);
            if (implementorType is not null) {
                foreach (var pi in implementorType.GetProperties()) {
                    // ensure this property is a ParameterAttribute property
                    if (pi.GetCustomAttributes(true).OfType<ParameterAttribute>().FirstOrDefault() is not null) {
                        string key = $"InitialValue:{BindValue}:{pi.Name}";

                        // try get as boolean, number or string
                        object? initialValue = configuration.GetValue(pi.PropertyType, key);

                        // if no match, try get as object
                        if (initialValue is null) {
                            Dictionary<string, object?> obj = GetSectionAsDictionary(configuration.GetSection(key));
                            if (obj.Count > 0) {
                                initialValue = obj;
                            }
                        }

                        // skip null and undefined
                        if (initialValue is not null) {
                            parameters[pi.Name] = JsonSerializer.SerializeToElement(initialValue);
                        }
                    }
                }
            }

            // add and notify the Sidebar
            index.Component.Add(new Dictionary<string, IDictionary<string, JsonElement>> {
                [BindValue] = parameters
            });
            session.RaiseCurrentSetChanged(this);

            // clear the new value select element
            BindValue = null;
        }
    }

    void RemoveComponentClicked(Item item) {
        if (GetIndex() is IndexType index &&
            index.Component is not null &&
            item.Index >= 0 &&
            item.Index < index.Component.Count) {

            // remove and notify <see cref="Sidebar"/>
            index.Component.RemoveAt(item.Index);
            session.RaiseCurrentSetChanged(this);
        }
    }

    void MoveComponentClicked(Item item, int direction) {
        int srcIndex = item.Index;
        int dstIndex = srcIndex + direction;
        if (GetIndex() is IndexType index &&
            index.Component is not null &&
            srcIndex >= 0 && srcIndex < index.Component.Count &&
            dstIndex >= 0 && dstIndex < index.Component.Count) {

            // swap srcIndex and dstIndex and notify <see cref="Sidebar"/>
            var tmp = index.Component[srcIndex];
            index.Component[srcIndex] = index.Component[dstIndex];
            index.Component[dstIndex] = tmp;
            session.RaiseCurrentSetChanged(this);
        }
    }

    #endregion
}
