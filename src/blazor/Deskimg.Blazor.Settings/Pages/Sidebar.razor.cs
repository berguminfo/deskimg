// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Blazor.Settings.Pages;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class Sidebar(
    NavigationManager navigation,
    SessionService session) {

    /// <summary>
    /// Gets or sets the current set.
    /// </summary>
    protected LayoutSet? CurrentSet { get; set; }

    /// <summary>
    /// Gets the HSL color given the provided index.
    /// </summary>
    /// <param name="index">The index.</param>
    /// <returns>The HSL color.</returns>
    public static string GetIndexColor(int index) =>
        $"hsla({180 + (index * 30)} 50% 50% / var(--semi-opaque))";

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {

        // set initial tab
        if (navigation.IsGeopositionPage()) {
            _activeTab = 1;
        } else
        if (navigation.IsIndexPage()) {
            _activeTab = 2;
        }

        // update the selected item
        navigation.LocationChanged += (_, _) => StateHasChanged();

        // EditIndex may change the CurrentSet
        session.CurrentSetChanged += (_, _) => StateHasChanged();

        CurrentSet = await session.GetCurrentSetAsync();
    }

    #region Tab Members

    int _activeTab;

    void TabClicked(int n) => _activeTab = n;

    string ActiveTab(int n) => n == _activeTab ? "active" : string.Empty;

    #endregion

    #region Nav Members

    string ActiveLink(string target) {
        string uri = "/" + Uri.UnescapeDataString(navigation.ToBaseRelativePath(navigation.Uri)).Trim();
        string result = uri.StartsWith(target, StringComparison.CurrentCultureIgnoreCase) ?
            "active" : string.Empty;
        return result;
    }

    #endregion
}
