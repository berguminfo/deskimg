// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;

namespace Deskimg.Blazor.Settings.Pages;

using Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class Cache(
    ILogger<Index> logger,
    EndpointOptions endpoint,
    HttpClient http) {

    IDictionary<string, FetchEntry>? ViewData { get; set; }

    /// <summary>
    /// Gets or sets the fetch <code>Cache-Control</code> strategy.
    /// </summary>
    [Parameter]
    public CacheControlHeaderValue CacheControl { get; set; } = new() {
        MaxAge = TimeSpan.FromMinutes(15)
    };

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        Uri? requestUri = endpoint.Index("fetch.api/cache");
        if (requestUri is not null) {
            try {
                logger.FetchStarting(requestUri);
                ViewData = await http.GetFromJsonAsync<IDictionary<string, FetchEntry>>(requestUri, CacheControl);
            } catch (OperationCanceledException) {
            } catch (HttpRequestException ex) {
                logger.FetchFailed(ex, requestUri, ex.StatusCode);
            } catch (JsonException ex) {
                logger.DeserializeJsonFailed(ex, requestUri, ex.LineNumber, ex.BytePositionInLine);
            }
        }
    }
}
