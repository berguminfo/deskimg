// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json;

namespace Deskimg.Blazor.Settings.Pages;

/// <summary>
/// Represents the razor page code-behind partial class.
/// </summary>
partial class Export(
    NavigationManager navigation,
    SessionService session) {

    /// <summary>
    /// Gets or sets the component view data.
    /// </summary>
    protected string? ViewData { get; set; }

    static readonly JsonSerializerOptions s_serializerOptions = new() {
        WriteIndented = true,
        PropertyNamingPolicy = null
    };

    /// <inheritdoc/>
    protected override async Task OnInitializedAsync() {
        navigation.LocationChanged += (_, _) => StateHasChanged();
        // TODO: not needed? session.CurrentSetChanged += (_, _) => StateHasChanged();
        ViewData = await GetExportAsync();
    }

    async Task<string> GetExportAsync(CancellationToken cancellationToken = default) {
        var currentSet = await session.GetCurrentSetAsync(cancellationToken);
        return JsonSerializer.Serialize(currentSet, s_serializerOptions);
    }
}
