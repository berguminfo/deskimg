// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.RegularExpressions;

namespace Deskimg.Blazor.Settings.Pages;

static partial class NavigationManagerExtensions {

    static string Combine<T>(params IEnumerable<T> values) =>
        "-/" + string.Join('/', values);

    public static string PreviewPage(this NavigationManager navigate) =>
        navigate.ToAbsoluteUri(Combine<string>("preview")).LocalPath;

    public static string ExportPage(this NavigationManager navigate) =>
        navigate.ToAbsoluteUri(Combine<string>("export")).LocalPath;

    public static string CachePage(this NavigationManager navigate) =>
        navigate.ToAbsoluteUri(Combine<string>("cache")).LocalPath;

    public static string BackgroundPage(this NavigationManager navigate) =>
        navigate.ToAbsoluteUri(Combine<string>("bk")).LocalPath;

    public static string ComponentPage(this NavigationManager navigate, params IEnumerable<int> path) =>
        navigate.ToAbsoluteUri(Combine(path)).LocalPath;

    public static string GeopositionPage(this NavigationManager navigate, string? id = null) =>
        navigate.ToAbsoluteUri(Combine("geo", id)).LocalPath;

    [GeneratedRegex("^/-/geo/.+$")]
    static partial Regex IsGeopositionPagePattern { get; }

    public static bool IsGeopositionPage(this NavigationManager navigate) =>
        IsGeopositionPagePattern.Match(navigate.Uri).Success;

    public static string IndexPage(this NavigationManager navigate, params IEnumerable<int> path) =>
        navigate.ToAbsoluteUri(Combine(path)).LocalPath;

    [GeneratedRegex(@"^/-/\d+$")]
    static partial Regex IsIndexPagePattern { get; }

    public static bool IsIndexPage(this NavigationManager navigate) =>
        IsIndexPagePattern.Match(navigate.Uri).Success;
}
