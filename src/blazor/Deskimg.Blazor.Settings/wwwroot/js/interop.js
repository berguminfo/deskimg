// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

export function postInteropMessage(message) {
    if (chrome && chrome.webview && chrome.webview.postMessage) {
        chrome.webview.postMessage({ message });
    } else {
        console.debug("postInteropMessage", message);
    }
}
