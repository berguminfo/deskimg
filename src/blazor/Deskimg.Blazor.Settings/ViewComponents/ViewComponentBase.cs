// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA1515 // IComponent should be public

using System.Text.Json;

namespace Deskimg.Blazor.Settings.ViewComponents;

/// <summary>
/// Represents the abstract base class for view components.
/// </summary>
/// <typeparam name="TValue">Specifies the <see cref="Type"/> of the <see cref="Value"/> property.</typeparam>
public abstract class ViewComponentBase<TValue> : ComponentBase {

    /// <summary>
    /// Gets or sets a collection of additional attributes that will be applied to the created element.
    /// </summary>
    [Parameter(CaptureUnmatchedValues = true)]
    public IReadOnlyDictionary<string, object>? AdditionalAttributes { get; set; }


    /// <summary>
    /// Gets or sets the parameters dictionary.
    /// </summary>
    [Parameter]
    public IDictionary<string, JsonElement>? Parameters { get; init; }

    /// <summary>
    /// Gets or sets the parameter key.
    /// </summary>
    [Parameter]
    public string? Key { get; set; }

    /// <summary>
    /// Gets or sets the component value.
    /// </summary>
    [Parameter]
    public TValue? Value { get; set; }

    /// <summary>
    /// Gets or sets a callback that updates the bound value.
    /// </summary>
    [Parameter]
    public EventCallback<TValue> ValueChanged { get; set; }

    /// <summary>
    /// Gets or sets the current component value.
    /// </summary>
    protected virtual TValue? CurrentValue {
        get {
            if (Parameters is not null && Key is not null && Parameters.TryGetValue(Key, out var value)) {
                var obj = value.Deserialize<TValue>();
                return obj ?? default;
            } else {
                return Value;
            }
        }
        set {
            if (Parameters is not null && Key is not null) {
                Parameters[Key] = JsonSerializer.SerializeToElement(value);
#if DEBUG
#pragma warning disable CA1303 // Do not pass literals as localized parameters
                Console.WriteLine("CurrentValue: {0}", Parameters[Key]);
#pragma warning restore CA1303 // Do not pass literals as localized parameters
#endif
            } else {
                Value = value;
                ValueChanged.InvokeAsync(Value);
#if DEBUG
#pragma warning disable CA1303 // Do not pass literals as localized parameters
                Console.WriteLine("CurrentValue: {0}", Value);
#pragma warning restore CA1303 // Do not pass literals as localized parameters
#endif
            }

        }
    }
}

#pragma warning restore CA1515 // IComponent should be public
