// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Reflection;
using System.Text.Json;

namespace Deskimg.Blazor.Settings;

internal sealed partial class Strings {
    public required string Title { get; set; }
    public string PageTitle(string title) => string.Concat(Title, " › ", title);
    public required ModalStrings Modal { get; set; }
    public required CacheStrings Cache { get; set; }
    public required ExportString Export { get; set; }
    public required FooterStrings Footer { get; set; }
    public required SidebarStrings Sidebar { get; set; }
    public required PreviewStrings Preview { get; set; }
    public required IDictionary<string, string> Label { get; set; }
    public required IDictionary<string, string> Component { get; set; }
    public string LabelString(string? name) =>
        Label.TryGetValue(name ?? string.Empty, out var result) ? result : $"{name}";
    public string ComponentString(string? name) =>
        Component.TryGetValue(name ?? string.Empty, out var result) ? result : $"{name}";
}

internal sealed record ModalStrings(
    string Add,
    string Cancel,
    string Confirm,
    string More,
    string MoveDown,
    string MoveUp,
    string Remove,
    string Reset,
    string Save
);

internal sealed record CacheStrings(
    string Title,
    string Host,
    string FileName,
    string ExpiresAtTime,
    string AbsoluteExpiration
);

internal sealed record ExportString(
    string Title
);

internal sealed record FooterStrings(
    string ConfirmReset,
    string ResetIgnored,
    string SaveCompleted
);

internal sealed record PreviewStrings(
    string Title
);

internal sealed record SidebarStrings(
    string Components,
    string Locations,
    string Layout
);

/// <summary>
/// This class exposes the application string resources embedded as JSON files.
/// </summary>
/// <remarks>
/// Put the JSON string resources in the 'Resources' directory.
/// Use the Embedded Resource build tool in order embed the strings.
/// Each resource should be named using RFC-3282 language codes.
/// </remarks>
internal static class SR {

    static readonly JsonSerializerOptions s_sourceGenOptions = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = JsonSchemaSourceGeneration.Default
    };

    static Strings? s_strings;

    /// <summary>
    /// Gets the default <see cref="Strings"/> resource.
    /// </summary>
    public static Strings Default {
        get {
            if (s_strings is null) {
                throw new InvalidOperationException();
            }

            return s_strings;
        }
    }

    /// <see cref="InitializeAsync(string, string, CancellationToken)"/>
    public static Task InitializeAsync(string cultureName, CancellationToken cancellationToken = default) =>
        InitializeAsync(cultureName, "en", cancellationToken);

    /// <summary>
    /// Initializes string resources in the given culture.
    /// </summary>
    /// <param name="cultureName">The culture name.</param>
    /// <param name="fallbackCultureName">The fallback culture name.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> that can be used to cancel the read operation.</param>
    /// <exception cref="JsonException">
    /// The JSON is invalid,
    /// <see cref="Strings"/> is not compatible with the JSON,
    /// or when there is remaining data in the Stream.
    /// </exception>
    /// <exception cref="NotSupportedException">
    /// There is no compatible <see cref="System.Text.Json.Serialization.JsonConverter"/>
    /// for <see cref="Strings"/> or its serialize able members.
    /// </exception>
    public static async Task InitializeAsync(string cultureName, string fallbackCultureName, CancellationToken cancellationToken = default) {

        async Task initializeAsyncInternal(string name) {
            var resourceName = $"Deskimg.Blazor.Settings.Resources.langs.{name}.json";
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName) ??
                throw new FileNotFoundException(resourceName);

            s_strings = await JsonSerializer.DeserializeAsync<Strings>(stream, s_sourceGenOptions, cancellationToken);
        }

        try {
            await initializeAsyncInternal(cultureName);
        } catch (Exception) {
            await initializeAsyncInternal(fallbackCultureName);
        }
    }
}
