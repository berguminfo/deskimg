param (
    [Parameter(Position = 0)]
    $Path = (Join-Path "obj" "epg.nrk.no.json"),
    [Parameter(Position = 1)]
    $DestinationFile = (Join-Path ".." "wwwroot" "epg" "epg.nrk.no.json"),
    [switch] $Fetch = $false,
    $UrlFormat = "https://psapi.nrk.no/epg/nrk1,nrk2,nrk3,nrksuper,nrk_tegnspraak,p1pluss,p2,p3,p13,mp3,alltid_nyheter,radio_super,klassisk,sapmi,jazz,folkemusikk,sport?date={0}"
)

function Invoke-Fetch {
    $url = $UrlFormat -f (Get-Date -Format "yyyy-MM-dd")
    mkdir (Split-Path -Parent $Path) -ErrorAction Ignore
    curl $url -o $Path
}

function Invoke-Parse {
    $result = @{}
    Get-Content $Path | ConvertFrom-Json | % {
        $result["epg.nrk.no?$($_.channel.id)"] = $_.channel.title
    }
    ConvertTo-Json $result
}

if ($Fetch) {
    Invoke-Fetch
}

Invoke-Parse
