param (
    [Parameter(Position = 0)]
    $Path = (Join-Path "obj" "epg.allente.no.json"),
    [Parameter(Position = 1)]
    $DestinationFile = (Join-Path ".." "wwwroot" "epg" "epg.allente.no.json"),
    [switch] $Fetch = $false,
    $UrlFormat = "https://cs-vcb.allente.no/epg/events?date={0}"
)

function Invoke-Fetch {
    $url = $UrlFormat -f (Get-Date -Format "yyyy-MM-dd")
    mkdir (Split-Path -Parent $Path) -ErrorAction Ignore
    curl $url -o $Path
}

function Invoke-Parse {
    $result = @{}
    Get-Content $Path | ConvertFrom-Json | % {
        $_.channels | % {
            $result["epg.allente.no?$($_.id)"] = $_.name
        }
    }
    $result
}

if ($Fetch) {
    Invoke-Fetch
}

Invoke-Parse | ConvertTo-Json
