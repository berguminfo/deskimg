// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Deskimg;
using Deskimg.Extensions;
using Deskimg.Blazor.Settings;
using SR = Deskimg.Blazor.Settings.SR;
#if ENABLE_PLUGINS
using Deskimg.Composition;
#endif

var builder = WebAssemblyHostBuilder.CreateDefault(args);

// bind razor components to HTML elements
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

// set LogLevel from IConfiguration
var logLevel = builder.Configuration.GetSection("Logging:LogLevel:Default").Get<LogLevel?>();
if (logLevel is not null) {
    builder.Logging.SetMinimumLevel(logLevel.Value);
}
builder.Logging
    .AddConfiguration(builder.Configuration)
    .AddFilter("Microsoft.AspNetCore.Components.RenderTree.*", LogLevel.None);

// apply additional configuration
await builder.Configuration.LoadHostingConfigurationAsync(builder.HostEnvironment.BaseAddress);

// add services to the container
builder.Services.AddLocalization();
builder.Services.AddMemoryCache();
builder.Services.AddSingleton(_ => new HttpClient {
    BaseAddress = new Uri(builder.HostEnvironment.BaseAddress),
    DefaultRequestVersion = HttpVersion.Version30,
    DefaultVersionPolicy = HttpVersionPolicy.RequestVersionOrLower
});
builder.Services.Configure<HyperlinkOptions>(builder.Configuration.GetSection(HyperlinkOptions.Name));
builder.Services.AddSessionOptions();
builder.Services.AddNavigationOptions();
builder.Services.AddEndpointOptions();
builder.Services.AddKeyedScoped<SessionService>(nameof(ISession));
SessionService? session = null;
builder.Services.AddScoped(sp => session ??= sp.GetRequiredKeyedService<SessionService>(nameof(ISession)));
builder.Services.AddScoped<ISession, SessionImplementationFactory>();
builder.Services.AddCompositions(new FoundationExports(), new ComponentsExports());
#if ENABLE_PLUGINS
builder.Services.AddScoped<CalDAVExports>();
#endif

// setup window.globalThis
if (OperatingSystem.IsBrowser()) {
    await GeocoordinateWatcher.WatchAsync();
}

// initialize session and load plugins
var app = builder.Build();
using (var scope = app.Services.CreateScope()) {
    await scope.ServiceProvider.GetRequiredService<SessionService>().InitializeAsync();
    await SR.InitializeAsync(Thread.CurrentThread.CurrentCulture.Name);
#if ENABLE_PLUGINS
    // NOTE: the -p:PublishTrimmed option may trim away required code for the plugin
    await scope.ServiceProvider.GetRequiredService<CalDAVExports>().PropFindAsync();
#endif

    // NOTE: SessionService is in this scope and disposing will stop the update timer
    await app.RunAsync();
}

