// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using System.Text;

namespace Deskimg.Blazor.Settings.Resources;

static class Embedded {

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Action(string name, string type = "svg") =>
        Get(nameof(Action), name, type) ?? string.Empty;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Illustration(string name, int size = 20, string type = "svg") =>
        Get(nameof(Illustration), $"{name}-{size}", type) ??
        Get(nameof(Illustration), $"fallback-{size}", type) ?? string.Empty;

    static string? Get(string basename, string name, string type) {
        StringBuilder sb = new(17 + basename.Length + name.Length + type.Length);
        sb.AppendJoin(string.Empty, "Deskimg.Blazor.Settings.Resources.", basename, ".", name, ".", type);
        string resourceName = sb.ToString();

        // NOTE: throw and terminate on error
        var stream = typeof(Embedded).Assembly.GetManifestResourceStream(resourceName);
        if (stream is not null) {
            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        } else {
            return null;
        }
    }
}
