using System.Collections.Immutable;

namespace Desklet.Plugin;

[AssemblyExportsGenerated]
public partial class PluginExports : IAssemblyExports {

    public partial IImmutableDictionary<(Type, string), Type> GetExports();
}
