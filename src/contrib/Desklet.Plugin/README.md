# Desklet.Plugin

A simple plugin sample.

Plugins is expected to be light weight having no dependencies
except the `Deskimg.Foundation.Ref` and `Deskimg.Generators.Exports` packages.

This sample injects the [QRCoder](https://github.com/codebude/QRCoder) source to
generate the QRCode.

## Install

Copy the plugin dll to the CalDAV `OutputFormat` directory.

```ps1
%APPDATA%/Deskimg/storage/plugins
```

## Usage

```json
{
  "QRCode": {
    "Source": "https://bergum.info",
    "PixelsPerModule": 1,
    "DarkColor": "currentcolor",
    "LightColor": "transparent",
    "ECC": "L"
  }
}
```