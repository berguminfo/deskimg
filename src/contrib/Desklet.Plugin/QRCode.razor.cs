using System.Text.Json.Serialization;

namespace Desklet.Plugin;

using QRCoder;

[Export<IDesklet>(nameof(QRCode))]
partial class QRCode(
    ILogger<QRCode> logger,
    ISession session) : IDesklet {

    protected string? ViewData { get; set; }

    [Parameter]
    public string? Value { get; set; }

    [Parameter]
    public int PixelsPerModule { get; set; } = 1;

    [Parameter]
    public string DarkColor { get; set; } = "currentcolor";

    [Parameter]
    public string LightColor { get; set; } = "transparent";

    [Parameter]
    [JsonConverter(typeof(JsonStringEnumConverter<QRCodeGenerator.ECCLevel>))]
    public QRCodeGenerator.ECCLevel ECC { get; set; } = QRCodeGenerator.ECCLevel.L;

    protected override void OnParametersSet() {
        if (!string.IsNullOrWhiteSpace(Value)) {
            ViewData = SvgQRCodeHelper.GetQRCode(Value, PixelsPerModule, DarkColor, LightColor, ECC);
            logger.LogDebug($"QRCode generated at: {session.Toi.ToString("s")}");
        }
    }
}
