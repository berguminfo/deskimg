// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA1515, CA1852

partial class ThisAssembly {
    public static partial class Git {
        public const string InformationalVersion =
            $"{BaseVersion.Major}.{BaseVersion.Minor}.{BaseVersion.Patch}{SemVer.DashLabel}";
    }
}

#pragma warning restore CA1515, CA1852
