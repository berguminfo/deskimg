// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA1515, CS1591, CA1034, CA1724

using Mime = System.Net.Mime.MediaTypeNames;

namespace Deskimg;

/// <summary>
/// Represents commonly used MIME types extending <see cref="System.Net.Mime.MediaTypeNames"/>.
/// </summary>
public static class MediaTypeNames {

    public static class Application {
        public const string Atom = "application/atom+xml";
        public const string Json = Mime.Application.Json;
        public const string JsonPatchJson = "application/json-patch+json";
        public const string Octet = Mime.Application.Octet;
        public const string ProblemJson = Mime.Application.ProblemJson;
        public const string Rss = "application/rss+xml";
        public const string Xml = Mime.Application.Xml;
        public const string XPlugin = "application/x.plugin";
    }

    public static class Image {
        public const string Jpeg = Mime.Image.Jpeg;
        public const string Png = Mime.Image.Png;
        public const string Webp = Mime.Image.Webp;
    }

    public static class Text {
        public const string Calendar = "text/calendar";
        public const string Plain = Mime.Text.Plain;
        public const string Html = Mime.Text.Html;
        public const string Xml = Mime.Text.Xml;
    }
}
