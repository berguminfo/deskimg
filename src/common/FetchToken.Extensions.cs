// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Http;

namespace Deskimg.Extensions;

/// <summary>
/// Represents a class to create and verify fetch tokens.
/// </summary>
internal static class HttpRequestExtensions {

    internal static bool HasValidFetchToken(this HttpRequest request) {
        if (request.Headers.TryGetValue(FetchToken.HeaderKey, out var headerToken)) {
            return FetchToken.Verify(headerToken);
        } else
        if (request.Query.TryGetValue(FetchToken.QueryKey, out var queryToken)) {
            return FetchToken.Verify(queryToken);
        } else {
            return false;
        }
    }
}
