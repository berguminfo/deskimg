// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

#pragma warning disable CA1515

using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Microsoft.AspNetCore.Mvc;

/// <summary>
/// Identifies an action that supports the HTTP PROPFIND method.
/// </summary>
public sealed class HttpPropFindAttribute : HttpMethodAttribute {

    static readonly IEnumerable<string> s_supportedMethods = ["PROPFIND"];

    /// <summary>
    /// Creates a new <see cref="HttpPropFindAttribute"/>.
    /// </summary>
    public HttpPropFindAttribute() : base(s_supportedMethods) { }

    /// <summary>
    /// Creates a new <see cref="HttpPropFindAttribute"/> with the given route template.
    /// </summary>
    /// <param name="template">The route template. May not be null.</param>
    public HttpPropFindAttribute([StringSyntax("Route")] string template)
        : base(s_supportedMethods, template) {
        ArgumentNullException.ThrowIfNull(template);
    }
}

/// <summary>
/// Identifies an action that supports the HTTP PROPPATCH method.
/// </summary>
public sealed class HttpPropPatchAttribute : HttpMethodAttribute {

    static readonly IEnumerable<string> s_supportedMethods = ["PROPPATCH"];

    /// <summary>
    /// Creates a new <see cref="HttpPropPatchAttribute"/>.
    /// </summary>
    public HttpPropPatchAttribute() : base(s_supportedMethods) { }

    /// <summary>
    /// Creates a new <see cref="HttpPropPatchAttribute"/> with the given route template.
    /// </summary>
    /// <param name="template">The route template. May not be null.</param>
    public HttpPropPatchAttribute([StringSyntax("Route")] string template)
        : base(s_supportedMethods, template) {
        ArgumentNullException.ThrowIfNull(template);
    }
}

/// <summary>
/// Identifies an action that supports the HTTP COPY method.
/// </summary>
public sealed class HttpCopyAttribute : HttpMethodAttribute {

    static readonly IEnumerable<string> s_supportedMethods = ["COPY"];

    /// <summary>
    /// Creates a new <see cref="HttpCopyAttribute"/>.
    /// </summary>
    public HttpCopyAttribute() : base(s_supportedMethods) { }

    /// <summary>
    /// Creates a new <see cref="HttpCopyAttribute"/> with the given route template.
    /// </summary>
    /// <param name="template">The route template. May not be null.</param>
    public HttpCopyAttribute([StringSyntax("Route")] string template)
        : base(s_supportedMethods, template) {
        ArgumentNullException.ThrowIfNull(template);
    }
}

/// <summary>
/// Identifies an action that supports the HTTP MOVE method.
/// </summary>
public sealed class HttpMoveAttribute : HttpMethodAttribute {

    static readonly IEnumerable<string> s_supportedMethods = ["MOVE"];

    /// <summary>
    /// Creates a new <see cref="HttpMoveAttribute"/>.
    /// </summary>
    public HttpMoveAttribute() : base(s_supportedMethods) { }

    /// <summary>
    /// Creates a new <see cref="HttpMoveAttribute"/> with the given route template.
    /// </summary>
    /// <param name="template">The route template. May not be null.</param>
    public HttpMoveAttribute([StringSyntax("Route")] string template)
        : base(s_supportedMethods, template) {
        ArgumentNullException.ThrowIfNull(template);
    }
}
