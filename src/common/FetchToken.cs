// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if NET9_0_OR_GREATER
#define USE_BUFFER_BASE64URL
#endif

#define SHA1

using System.Buffers.Binary;
using System.Security.Cryptography;
#if USE_BUFFER_BASE64URL
using System.Buffers.Text;
#endif

namespace Deskimg.Extensions;

/// <summary>
/// Represents a class to create and verify fetch tokens.
/// </summary>
public static class FetchToken {

    const int DefaultSaltSize = 6;

    /// <summary>
    /// Specifies the HTTP header key holding the API key.
    /// </summary>
    public const string HeaderKey = "x-api-key";

    /// <summary>
    /// Specifies the HTTP query string key holding the API key.
    /// </summary>
    public const string QueryKey = "api_key";

    /// <summary>
    /// Creates a fetch token using random salt, current clock and the git object id.
    /// </summary>
    /// <remarks>
    /// The validity interval is <see cref="DateTime.Ticks"/> divided by 2ᵈ-1
    /// (where ᵈ is <paramref name="shr"/>).
    ///
    /// Use this <c>pwsh</c> script to print available shift right options.
    /// <code>
    /// (24..35) | % {
    ///     $t = [TimeSpan]::new([Math]::Pow(2, $_) - 1)
    ///     "| $_ | {0:x9}₁₆ | {1:mm\mss\s} |" -f $t.Ticks, $t
    /// }
    /// </code>
    /// <code>
    /// | SHR | Ticks      | Interval |
    /// |-----|------------|----------|
    /// | 24  | 000ffffffₓ | 00m01s   |
    /// | 25  | 001ffffffₓ | 00m03s   |
    /// | 26  | 003ffffffₓ | 00m06s   |
    /// | 27  | 007ffffffₓ | 00m13s   |
    /// | 28  | 00fffffffₓ | 00m26s   |
    /// | 29  | 01fffffffₓ | 00m53s   | * default
    /// | 30  | 03fffffffₓ | 01m47s   |
    /// | 31  | 07fffffffₓ | 03m34s   |
    /// | 32  | 0ffffffffₓ | 07m09s   |
    /// | 33  | 1ffffffffₓ | 14m18s   |
    /// | 34  | 3ffffffffₓ | 28m37s   |
    /// | 35  | 7ffffffffₓ | 57m15s   |
    /// </code>
    /// </remarks>
    /// <param name="shr">Specifies the validity divide (shift right).</param>
    /// <returns>A (for now) valid fetch token.</returns>
    public static string Create(int shr = 29) => Create(null, shr);

    internal static string Create(byte[]? salt, int shr) {
        salt ??= RandomNumberGenerator.GetBytes(DefaultSaltSize);
        return Create(salt, DateTime.UtcNow.Ticks, shr, ThisAssembly.Git.Sha);
    }

    internal static string Create(byte[] salt, long ticks, int shr, string hash) {
        byte[] buffer = new byte[salt.Length + 4 + (hash.Length / 2)];
        Span<byte> saltSpan = new(buffer, 0, salt.Length);
        Span<byte> ticksSpan = new(buffer, salt.Length, 4);
        Span<byte> hashSpan = new(buffer, salt.Length + 4, hash.Length / 2);

        // salt
        salt.CopyTo(saltSpan);

        // clock
        if (shr > 0) {
            BinaryPrimitives.WriteInt32BigEndian(ticksSpan, (int)(ticks >> shr));
        }

        // sha
        Convert.FromHexString(hash, hashSpan, out _, out _);
#if SHA1
#pragma warning disable CA5350 // Do Not Use Weak Cryptographic Algorithms
        // "IVXavssL" + "oTFo6ZTbsReGZ2CcThSyrSXDkNk="
        byte[] digest = SHA1.HashData(buffer);
#pragma warning restore CA5350 // Do Not Use Weak Cryptographic Algorithms
#else
        byte[] digest = SHA256.HashData(buffer);
#endif
#if USE_BUFFER_BASE64URL
        return Base64Url.EncodeToString(saltSpan) + Base64Url.EncodeToString(digest);
#else
        return Convert.ToBase64String(buffer, 0, ticksSpan.Length) + Convert.ToBase64String(digest);
#endif
    }

    /// <summary>
    /// Verifies that the provided token is a valid fetch token.
    /// </summary>
    /// <param name="token">The token challenge.</param>
    /// <param name="shr">Specifies the validity divide (shift right).</param>
    /// <returns>True if the token is valid otherwise false.</returns>
    public static bool Verify(string? token, int shr = 29) =>
        Verify(token, DateTime.UtcNow.Ticks, shr, ThisAssembly.Git.Sha);

    internal static bool Verify(string? token, long ticks, int shr, string hash) {
        if (token?.Length >= DefaultSaltSize + 2) {
            byte[] salt = new byte[DefaultSaltSize];
#if USE_BUFFER_BASE64URL
            bool success = Base64Url.TryDecodeFromChars(token.ToCharArray(0, DefaultSaltSize + 2), salt, out var bytesWritten);
#else
            bool success = Convert.TryFromBase64String(token[..8], salt, out var bytesWritten);
#endif
            if (success && bytesWritten == salt.Length) {
                return token.Equals(Create(salt, ticks, shr, hash), StringComparison.Ordinal);
            }
        }

        return false;
    }
}
