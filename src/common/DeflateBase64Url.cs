// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA1515

#if NET9_0_OR_GREATER
#define USE_BUFFER_BASE64URL
#endif

using System.Buffers.Binary;
using System.IO.Compression;
using System.Text;
#if USE_BUFFER_BASE64URL
using System.Buffers.Text;
#else
using Microsoft.AspNetCore.WebUtilities;
#endif

namespace Deskimg.Extensions;

/// <summary>
/// Helper class to encode and decode RFC-1950 DEFLATE strings.
/// </summary>
internal static class DeflateBase64Url {

    static readonly Encoding s_encoding = Encoding.UTF8;

    // CMF (Compression Method and Flags)
    //
    // bits 0 to 3  CM     Compression method
    // bits 4 to 7  CINFO  Compression info
    //
    // CM = 8 denotes the "deflate" compression method
    // CINFO = 7 denotes 32K window size
    const byte CMF = 0x78;

    // FLG (FLaGs)
    //
    // bits 0 to 4  FCHECK  (check bits for CMF and FLG)
    // bit  5       FDICT   (preset dictionary)
    // bits 6 to 7  FLEVEL  (compression level)
    const byte FLG_FLEVEL0 = 0b00_0_00001;
    const byte FLG_FLEVEL1 = 0b01_0_11110; // 5Eₓ
    const byte FLG_FLEVEL2 = 0b10_0_11100; // 9Cₓ
    const byte FLG_FLEVEL3 = 0b11_0_11010; // DAₓ

    static uint Adler32(ReadOnlySpan<byte> bytes) {
        const uint mod = 0xfff1;
        uint a = 1, b = 0;
        foreach (byte c in bytes) {
            a = (a + c) % mod;
            b = (b + a) % mod;
        }
        return (b << 16) | a;
    }

    /// <summary>
    /// Encodes and inflates supplied data into Base64 and replaces any URL
    /// encodable characters into non-URL encodable characters.
    /// </summary>
    /// <remarks>
    /// <para>
    /// About RFC-1950 version 3.3.
    /// </para>
    /// <para>
    /// This method should be compatible with <see href="https://developer.mozilla.org/en-US/docs/Web/API/CompressionStream" />.
    /// Hence the output has the ZLIB prefix and ADLER-32 suffix.
    /// </para>
    /// </remarks>
    /// <param name="data">Data to be encoded.</param>
    /// <param name="compress">True to compress the data using the "deflate" algorithm.</param>
    /// <returns>Base64 encoded string modified with non-URL encodable characters.</returns>
    public static string Encode(string data, bool compress = true) {
        byte[] bytes = s_encoding.GetBytes(data);

        // buffer is ZLIB-header + bytes ∨ inflate-raw + ADLER-32
        MemoryStream buffer = new();
        buffer.WriteByte(CMF);

        // write compressed or raw data to buffer
        if (compress) {
            MemoryStream compressedBuffer = new(bytes.Length);
            using (DeflateStream compressor = new(compressedBuffer, CompressionMode.Compress)) {
                compressor.Write(bytes, 0, bytes.Length);
            }

            buffer.WriteByte(FLG_FLEVEL2);
            buffer.Write(compressedBuffer.ToArray());
        } else {
            buffer.WriteByte(FLG_FLEVEL0);
            buffer.Write(bytes);
        }

        // write ADLER-32 checksum to buffer
        byte[] checksum = new byte[sizeof(uint)];
        BinaryPrimitives.WriteUInt32BigEndian(checksum, Adler32(bytes));
        buffer.Write(checksum, 0, checksum.Length);

#if USE_BUFFER_BASE64URL
        return Base64Url.EncodeToString(buffer.ToArray());
#else
        return Base64UrlTextEncoder.Encode(buffer.ToArray());
#endif
    }

    /// <summary>
    /// Decodes and deflates supplied string by replacing the non-URL encodable
    /// characters with URL encodable characters and then decodes the Base64 string.
    /// </summary>
    /// <param name="source">The string to be decoded.</param>
    /// <returns>The decoded data.</returns>
    /// <exception cref="InvalidDataException">The supplied text has not been encoded with <see cref="Encode"/>.</exception>
    public static string Decode(string source) {
#if USE_BUFFER_BASE64URL
        byte[] bytes = Base64Url.DecodeFromChars(source);
#else
        byte[] bytes = Base64UrlTextEncoder.Decode(source);
#endif

        // check length and header
        bool valid =
            bytes.Length >= 2 + sizeof(uint) &&
            bytes[0] == CMF && (
                bytes[1] == FLG_FLEVEL0 ||
                bytes[1] == FLG_FLEVEL1 ||
                bytes[1] == FLG_FLEVEL2 ||
                bytes[1] == FLG_FLEVEL3);
        if (!valid) {
            throw new InvalidDataException($"Invalid ZLIB header: {source}");
        }

        // get decompressed or raw data
        ReadOnlySpan<byte> data = new(bytes, 2, bytes.Length - 2 - sizeof(uint));
        if (bytes[1] != FLG_FLEVEL0) {
            // NOTE: FLG_FLEVEL is unused in code, only informal
            MemoryStream decompressedBuffer = new();
            using (DeflateStream decompressor = new(new MemoryStream(data.ToArray()), CompressionMode.Decompress)) {
                decompressor.CopyTo(decompressedBuffer);
            }

            data = decompressedBuffer.ToArray();
        }

        // check checksum (ADLER-32)
        uint dataChecksum = Adler32(data);
        uint bytesChecksum = BinaryPrimitives.ReadUInt32BigEndian(new ReadOnlySpan<byte>(bytes, bytes.Length - sizeof(uint), sizeof(uint)));
        if (dataChecksum != bytesChecksum) {
            throw new InvalidDataException($"Invalid checksum. Expected checksum is {bytesChecksum} and actual checksum is {dataChecksum}");
        }

        return s_encoding.GetString(data);
    }
}
