// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;

// NOTE: this class should be in the default namespace

/// <summary>
/// Represents some common commands.
/// </summary>
static class CommonCommands {

    /// <summary>
    /// Executes the --export command.
    /// </summary>
    /// <returns>True if the command has been executed otherwise false.</returns>
    public static async Task<bool> IsExportCommand(string[] args) {
        bool result = false;
        if (args.Length >= 2) {
            if (args[0].Equals("--export", StringComparison.OrdinalIgnoreCase)) {
                if (OperatingSystem.IsWindows()) {
                    args[0] = "--export-windows";
                } else {
                    args[0] = "--export-crontab";
                }
            }

            if (args[0].Equals("--export-windows", StringComparison.OrdinalIgnoreCase)) {
                await ExportScheduledTaskAsync(args[1]);
                result = true;
            } else
            if (args[0].Equals("--export-crontab", StringComparison.OrdinalIgnoreCase)) {
                await ExportCrontab(args[1]);
                result = true;
            }
        }

        return result;
    }

    static async Task ExportScheduledTaskAsync(string fileName) {
        string resourceName = $"{Assembly.GetExecutingAssembly().GetName().Name}.Resources.ScheduledTask.xml";
        using var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        if (stream is not null) {
            var doc = await XDocument.LoadAsync(stream, LoadOptions.None, default);
            XNamespace t = "http://schemas.microsoft.com/windows/2004/02/mit/task";
            var element = doc.Element(t + "Task")?.Element(t + "Actions")?.Element(t + "Exec")?.Element(t + "WorkingDirectory");
            if (element is not null) {
                element.Value = AppDomain.CurrentDomain.BaseDirectory;
            }
            using var writer = XmlWriter.Create(fileName, new() {
                Encoding = Encoding.Unicode
            });
            doc.WriteTo(writer);
        }
    }

    static async Task ExportCrontab(string fileName) {
        string resourceName = $"{Assembly.GetExecutingAssembly().GetName().Name}.Resources.crontab";
        using var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        if (stream is not null) {
            using StreamReader reader = new(stream);
            string template = await reader.ReadToEndAsync();
            template = template.Replace("{Path}", AppDomain.CurrentDomain.BaseDirectory, StringComparison.Ordinal);
            await File.WriteAllTextAsync(fileName, template);
        }
    }
}
