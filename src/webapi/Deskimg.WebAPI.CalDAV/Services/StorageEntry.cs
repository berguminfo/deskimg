// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// Ignore Spelling: Utc

#if V3_0

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Deskimg.WebAPI.CalDAV.Services;

public sealed class StorageEntry : IGetResult<string> {
    public ObjectId Id { get; set; }
    public required long EntryHash { get; set; }
    public required string Store { get; set; }
    public required string Name { get; set; }
    public string? ContentType { get; set; }
    public long ContentLength { get; set; }
    public required long CreationTime { get; set; }
    public string? CreatedFrom { get; set; }
    public long LastWriteTime { get; set; }
    public string? ModifiedFrom { get; set; }
    public int Revision { get; set; }
    public required string Content { get; set; }

    [BsonIgnore]
    public DateTimeOffset LastModified => DateTimeOffset.FromUnixTimeMilliseconds(LastWriteTime);
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

#endif
