// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// Ignore Spelling: Mongo

#if V3_1 && MONGODB

using System.Text.Json;
using Microsoft.AspNetCore.JsonPatch;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Deskimg.WebAPI.CalDAV.Services;

partial class MongoDbRepository {

    /// <summary>
    /// Applies the provided JsonPatch the item in the given store and having the given name.
    /// </summary>
    /// <param name="store">The store of the item to put.</param>
    /// <param name="name">The name of the item to put.</param>
    /// <param name="patchDoc">The JsonPatch document to apply.</param>
    /// <param name="remoteAddress">Optional the remote IP address of the client.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    public async Task<bool> PatchAsync(
        string[] store,
        string name,
        JsonPatchDocument<LayoutSet> patchDoc,
        string? remoteAddress,
        CancellationToken cancellationToken = default) {

        var collection = await GetStorageCollectionAsync();
        StorageEntry document = await (
            from item in collection.AsQueryable()
            where item.EntryHash == GetEntryHash(store, name)
            select item).FirstOrDefaultAsync(cancellationToken);
        if (document is null) {
            throw new FileNotFoundException();
        }

        // update meta data
        document.LastWriteTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        document.ModifiedFrom = remoteAddress;
        document.Revision++;

        // set content
        var content = JsonSerializer.Deserialize<LayoutSet>(document.Content);
        if (content is null) {
            throw new InvalidDataException();
        }

        patchDoc.ApplyTo(content);
        document.Content = JsonSerializer.Serialize(content);
        document.ContentLength = document.Content.Length;

        var replaceResult = await collection.ReplaceOneAsync(x => x.Id == document.Id, document, s_replaceOptions, cancellationToken);
        return replaceResult.IsAcknowledged && replaceResult.MatchedCount == 1;
    }
}

#endif
