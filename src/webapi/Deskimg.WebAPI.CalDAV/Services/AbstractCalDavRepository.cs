// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net;

namespace Deskimg.WebAPI.CalDAV.Services;

/// <summary>
/// Represents the abstract CalDAV repository.
/// </summary>
public abstract class AbstractCalDavRepository {

    /// <summary>
    /// Tests the existence of the item in the given store and having the given name.
    /// </summary>
    /// <param name="store">The store of the item to get the meta data.</param>
    /// <param name="name">The name of the item to get the meta data.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The item <see cref="IGetResult"/> if found.</returns>
    /// <exception cref="FileNotFoundException">The given item was not found.</exception>
    public abstract Task<IGetResult?> HeadAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default);

    /// <summary>
    /// Gets the item in the given store and having the given name.
    /// </summary>
    /// <param name="store">The store of the item to get.</param>
    /// <param name="name">The name of the item to get.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The item <see cref="IGetResult"/> if found.</returns>
    /// <exception cref="FileNotFoundException">The given item was not found.</exception>
    public abstract Task<IGetResult?> GetAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default);

    /// <summary>
    /// Adds or replaces the item in the given store and having the given name.
    /// </summary>
    /// <param name="store">The store of the item to put.</param>
    /// <param name="name">The name of the item to put.</param>
    /// <param name="content">The content to store.</param>
    /// <param name="contentLength">Optional a hint about the length of the content.</param>
    /// <param name="contentType">Optional a hint about the type of the content.</param>
    /// <param name="contentEncoding">Optional a hint about the encoding of the content.</param>
    /// <param name="remoteAddress">Optional the remote IP address of the client.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The status code of the operation.</returns>
    public abstract Task<HttpStatusCode> PutAsync(
        IEnumerable<string> store,
        string name,
        Stream content,
        long? contentLength,
        string? contentType,
        string? contentEncoding,
        string? remoteAddress,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Removes the item in the given store and having the given name.
    /// </summary>
    /// <param name="store">The store of the item to delete.</param>
    /// <param name="name">The name of the item to delete.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The status code of the operation.</returns>
    public abstract Task<HttpStatusCode> DeleteAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default);

    /// <summary>
    /// Enumerates all stores.
    /// </summary>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The item enumeration.</returns>
    public abstract Task<IEnumerable<string>> PropFind(CancellationToken cancellationToken = default);

    /// <summary>
    /// Enumerates all items in the given store.
    /// </summary>
    /// <param name="store">The store of the items to list.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The item enumeration.</returns>
    public abstract Task<IEnumerable<IGetResult>> PropFind(IEnumerable<string> store, CancellationToken cancellationToken = default);
}
