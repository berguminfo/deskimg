// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// Ignore Spelling: Mongo

#if V3_0 && MONGODB

using System.IO.Hashing;
using System.Runtime.CompilerServices;
using System.Text;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Deskimg.WebAPI.CalDAV.Services;

using System.Net;
using Deskimg.WebAPI.CalDAV.Options;

/// <summary>
/// Represents a CalDAV service using MongoDB for storage.
/// </summary>
public sealed partial class MongoDbRepository(
    IOptions<AppOptions> appOptions,
    MongoClient client) : AbstractCalDavRepository {

    readonly AppOptions options = appOptions.Value;

    const char SeparatorCharacter = '/';

    static readonly InsertOneOptions s_insertOptions = new() {
        BypassDocumentValidation = true
    };
    static readonly ReplaceOptions s_replaceOptions = new() {
        BypassDocumentValidation = true
    };

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static string GetStore(params IEnumerable<string> store) =>
        string.Join(SeparatorCharacter, store);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static long GetEntryHash(IEnumerable<string> store, string name) {
        unchecked {
            // NOTE: python xxhash module don't support XXH3 (slightly faster)
            return (long)XxHash64.HashToUInt64(Encoding.UTF8.GetBytes(GetStore(store) + SeparatorCharacter + name));
        }
    }

    async Task<IMongoCollection<StorageEntry>> GetStorageCollectionAsync() {
        var collection = client
            .GetDatabase(options.StorageDatabase)
            .GetCollection<StorageEntry>($"{nameof(StorageEntry)}_66c6573cbe35b22334b3cb7e");
        var indexModel = new CreateIndexModel<StorageEntry>(Builders<StorageEntry>.IndexKeys.Ascending(x => x.EntryHash));
        await collection.Indexes.CreateOneAsync(indexModel);
        return collection;
    }

    /// <inheritdoc/>
    public override async Task<IGetResult?> HeadAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        var collection = await GetStorageCollectionAsync();
        var result = await (
            from item in collection.AsQueryable()
            where item.EntryHash == GetEntryHash(store, name)
            select new GetResult {
                LastModified = DateTimeOffset.FromUnixTimeMilliseconds(item.LastWriteTime),
                ContentType = item.ContentType,
            }).FirstOrDefaultAsync(cancellationToken);
        return result;
    }

    /// <inheritdoc/>
    public override async Task<IGetResult?> GetAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        var collection = await GetStorageCollectionAsync();
        var result = await (
            from item in collection.AsQueryable()
            where item.EntryHash == GetEntryHash(store, name)
            select item).FirstOrDefaultAsync(cancellationToken);
        return result;
    }

    /// <inheritdoc/>
    public override async Task<HttpStatusCode> PutAsync(
        IEnumerable<string> store,
        string name,
        Stream content,
        long? contentLength,
        string? contentType,
        string? contentEncoding,
        string? remoteAddress,
        CancellationToken cancellationToken = default) {

        var collection = await GetStorageCollectionAsync();
        StorageEntry document = await (
            from item in collection.AsQueryable()
            where item.EntryHash == GetEntryHash(store, name)
            select item).FirstOrDefaultAsync(cancellationToken);
        bool created = document is null;

        // if not found, create a new document
        document ??= new() {
            EntryHash = GetEntryHash(store, name),
            Store = GetStore(store),
            Name = name,
            CreationTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
            CreatedFrom = remoteAddress,
            Content = string.Empty
        };

        // update meta data
        document.ContentType = contentType;
        document.LastWriteTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        document.ModifiedFrom = remoteAddress;
        document.Revision++;

        // set content
        MemoryStream buffer = new();
        if (contentLength is not null) {
            buffer.Capacity = Convert.ToInt32(contentLength.Value);
        }

        await content.CopyToAsync(buffer, cancellationToken);
        Encoding encoding = Encoding.GetEncoding(contentEncoding ?? "utf-8");
        document.Content = encoding.GetString(buffer.ToArray());
        document.ContentLength = document.Content.Length;

        if (created) {
            await collection.InsertOneAsync(document, s_insertOptions, cancellationToken);
            return HttpStatusCode.Created;
        } else {
            await collection.ReplaceOneAsync(x => x.Id == document.Id, document, s_replaceOptions, cancellationToken);
            return HttpStatusCode.OK;
        }
    }

    /// <inheritdoc/>
    public override async Task<HttpStatusCode> DeleteAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        var collection = await GetStorageCollectionAsync();
        var result = await (
            from item in collection.AsQueryable()
            where item.EntryHash == GetEntryHash(store, name)
            select new {
                item.Id
            }).FirstOrDefaultAsync(cancellationToken);
        if (result is not null) {
            await collection.DeleteOneAsync(x => x.Id == result.Id, cancellationToken);
            return HttpStatusCode.OK;
        } else {
            return HttpStatusCode.NotFound;
        }
    }

    /// <inheritdoc/>
    public override async Task<IEnumerable<string>> PropFind(CancellationToken cancellationToken = default) {
        var collection = await GetStorageCollectionAsync();
        var result =
            from item in collection.AsQueryable()
            select item.Store;
        return result.Distinct().ToEnumerable(cancellationToken);
    }

    /// <inheritdoc/>
    public override async Task<IEnumerable<IGetResult>> PropFind(IEnumerable<string> store, CancellationToken cancellationToken = default) {
        var collection = await GetStorageCollectionAsync();
        var result =
            from item in collection.AsQueryable()
            where item.Store == GetStore(store)
            select item;
        return result.ToEnumerable(cancellationToken);
    }
}

#endif
