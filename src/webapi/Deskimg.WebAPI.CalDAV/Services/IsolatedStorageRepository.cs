// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_0

using System.IO.IsolatedStorage;
using System.Net;
using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.StaticFiles;

namespace Deskimg.WebAPI.CalDAV.Services;

/// <summary>
/// Represents a CalDavService using the isolated storage.
/// </summary>
public sealed class IsolatedStorageRepository(
    FileExtensionContentTypeProvider contentTypeProvider) : AbstractCalDavRepository, IDisposable {

    IsolatedStorageFile? _storage;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static string GetStoragePath(params IEnumerable<string> store) =>
        GetStoragePath(store, string.Empty);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static string GetStoragePath(IEnumerable<string> store, string name) =>
        Path.Combine(string.Join('_', store), name);

    IsolatedStorageFile Storage {
        get {
            if (_storage is null) {
                var scope = IsolatedStorageScope.Roaming | IsolatedStorageScope.Assembly;
                _storage = IsolatedStorageFile.GetStore(scope, nameof(Deskimg));
                if (!_storage.DirectoryExists(nameof(CalDAV))) {
                    _storage.CreateDirectory(nameof(CalDAV));
                }
            }

            return _storage;
        }
    }

    /// <inheritdoc/>
    public void Dispose() {
        _storage?.Dispose();
        _storage = null;
    }

    /// <inheritdoc/>
    public override Task<IGetResult?> HeadAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        string path = GetStoragePath(store, name);
        return
            Task.FromResult<IGetResult?>(!Storage.FileExists(path) ? null : new GetResult {
                ContentType = contentTypeProvider.GetContentType(path),
                LastModified = Storage.GetLastWriteTime(path),
            });
    }

    /// <inheritdoc/>
    public override async Task<IGetResult?> GetAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        string path = GetStoragePath(store, name);
        if (Storage.FileExists(path)) {

            // copy the content to a memory buffer
            MemoryStream buffer = new();
            using var stream = Storage.OpenFile(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            await stream.CopyToAsync(buffer, cancellationToken);

            return new GetResult<byte[]> {
                ContentType = contentTypeProvider.GetContentType(path),
                LastModified = Storage.GetLastWriteTime(path),
                Content = buffer.ToArray(),
            };
        } else {
            return null;
        }
    }

    /// <inheritdoc/>
    public override Task<HttpStatusCode> PutAsync(
        IEnumerable<string> store,
        string name,
        Stream content,
        long? contentLength,
        string? contentType,
        string? contentEncoding,
        string? remoteAddress,
        CancellationToken cancellationToken = default) {
        throw new NotSupportedException();
    }

    /// <inheritdoc/>
    public override Task<HttpStatusCode> DeleteAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        throw new NotSupportedException();
    }

    /// <inheritdoc/>
    public override Task<IEnumerable<string>> PropFind(CancellationToken cancellationToken = default) {
        string path = GetStoragePath();
        IEnumerable<string> result =
            from item in Storage.GetDirectoryNames(path)
            select item;
        return Task.FromResult(result);
    }

    /// <inheritdoc/>
    public override Task<IEnumerable<IGetResult>> PropFind(IEnumerable<string> store, CancellationToken cancellationToken = default) {
        string path = GetStoragePath(store);
        IEnumerable<IGetResult> result =
            !Storage.DirectoryExists(path) ? [] :
                from item in Storage.GetFileNames(path)
                select new GetResult {
                    Name = item,
                    LastModified = Storage.GetLastWriteTime(Path.Combine(path, item)),
                    ContentType = contentTypeProvider.GetContentType(item),
                };
        return Task.FromResult(result);
    }
}

#endif
