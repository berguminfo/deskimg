// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V2_0

using Microsoft.AspNetCore.StaticFiles;

namespace Deskimg.WebAPI.CalDAV.Services;

using System.Net;
using Deskimg.WebAPI.CalDAV.Options;

/// <summary>
/// Represents a CalDavService using the file system for storage.
/// </summary>
public sealed class FileSystemRepository(
    IOptions<AppOptions> appOptions,
    FileExtensionContentTypeProvider contentTypeProvider) : AbstractCalDavRepository {

    readonly AppOptions options = appOptions.Value;

    static readonly string s_roamingPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
        nameof(Deskimg));
    static readonly string s_localPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        nameof(Deskimg));

    string GetPhysicalPath(IEnumerable<string> store, string name) {
        var data = new {
            Roaming = s_roamingPath,
            Local = s_localPath,
            FileName = Path.Combine(string.Join('_', store), name)
        };
        try {
            return MessageFormatter.Format(options.OutputFormat, data);
        } catch (ArgumentNullException) {
            throw new MessageFormatterException($"Format: {options.OutputFormat}. Roaming: {data.Roaming}. Local: {data.Local}. FileName: {data.FileName}.");
        }
    }

    string GetPhysicalPath(IEnumerable<string> store) => GetPhysicalPath(store, string.Empty);

    /// <inheritdoc/>
    public override Task<IGetResult?> HeadAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        FileInfo physicalFile = new(GetPhysicalPath(store, name));
        return
            Task.FromResult<IGetResult?>(!physicalFile.Exists ? null : new GetResult {
                ContentType = contentTypeProvider.GetContentType(physicalFile.Name),
                LastModified = physicalFile.LastWriteTimeUtc,
            });
    }

    /// <inheritdoc/>
    public override Task<IGetResult?> GetAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        FileInfo physicalFile = new(GetPhysicalPath(store, name));
        return Task.FromResult<IGetResult?>(!physicalFile.Exists ? null : new GetResult<FileInfo> {
            ContentType = contentTypeProvider.GetContentType(physicalFile.Name),
            LastModified = physicalFile.LastWriteTimeUtc,
            Content = physicalFile,
        });
    }

    /// <inheritdoc/>
    public override async Task<HttpStatusCode> PutAsync(
        IEnumerable<string> store,
        string name,
        Stream content,
        long? contentLength,
        string? contentType,
        string? contentEncoding,
        string? remoteAddress,
        CancellationToken cancellationToken = default) {

        FileInfo physicalFile = new(GetPhysicalPath(store, name));
        if (physicalFile.Directory is not null && !physicalFile.Directory.Exists) {
            physicalFile.Directory.Create();
        }

        var statusCode = physicalFile.Exists ? HttpStatusCode.OK : HttpStatusCode.Created;
        using var stream = physicalFile.Create();
        await content.CopyToAsync(stream, cancellationToken);

        return statusCode;
    }

    /// <inheritdoc/>
    public override Task<HttpStatusCode> DeleteAsync(IEnumerable<string> store, string name, CancellationToken cancellationToken = default) {
        FileInfo physicalFile = new(GetPhysicalPath(store, name));
        if (physicalFile.Exists) {

            // remove the existing file by renaming it to the backup file
            FileInfo backupFile = new(physicalFile.FullName + ".bak");
            if (backupFile.Exists) {
                backupFile.Delete();
            }

            physicalFile.MoveTo(backupFile.FullName);
            return Task.FromResult(HttpStatusCode.OK);
        } else {
            return Task.FromResult(HttpStatusCode.NotFound);
        }
    }

    /// <inheritdoc/>
    public override Task<IEnumerable<string>> PropFind(CancellationToken cancellationToken = default) {
        DirectoryInfo physicalDirectory = new(GetPhysicalPath([]));
        IEnumerable<string> result =
            !physicalDirectory.Exists ? [] :
            from item in physicalDirectory.GetDirectories()
            select item.Name;
        return Task.FromResult(result);
    }

    /// <inheritdoc/>
    public override Task<IEnumerable<IGetResult>> PropFind(IEnumerable<string> store, CancellationToken cancellationToken = default) {
        DirectoryInfo physicalDirectory = new(GetPhysicalPath(store));
        IEnumerable<IGetResult> result =
            !physicalDirectory.Exists ? [] :
            from item in physicalDirectory.GetFiles()
            select new GetResult {
                Name = item.Name,
                ContentType = contentTypeProvider.GetContentType(item.Name),
                ContentLength = item.Length,
                LastModified = item.LastWriteTimeUtc,
            };
        return Task.FromResult(result);
    }
}

#endif
