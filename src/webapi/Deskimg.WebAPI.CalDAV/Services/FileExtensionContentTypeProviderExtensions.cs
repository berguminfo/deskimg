// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.StaticFiles;

namespace Deskimg.WebAPI.CalDAV.Services;

/// <summary>
/// Represents extension methods to <see cref="FileExtensionContentTypeProvider"/>.
/// </summary>
public static class FileExtensionContentTypeProviderExtensions {

    /// <see cref="FileExtensionContentTypeProvider.TryGetContentType"/>
    public static string GetContentType(this FileExtensionContentTypeProvider contentTypeProvider, string fileName) {
        return contentTypeProvider.TryGetContentType(fileName, out string? contentType) ?
            contentType : MediaTypeNames.Application.Octet;
    }
}
