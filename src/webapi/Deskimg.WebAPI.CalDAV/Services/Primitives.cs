// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.CalDAV.Services;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public interface IGetResult {
    public string? Name { get; }
    public string? ContentType { get; }
    public long ContentLength { get; }
    public DateTimeOffset LastModified { get; }
}

public interface IGetResult<T> : IGetResult {
    public T Content { get; }
}

public class GetResult : IGetResult {
    public string? Name { get; set; }
    public string? ContentType { get; set; }
    public long ContentLength { get; set; }
    public DateTimeOffset LastModified { get; set; }
}

public class GetResult<T> : GetResult, IGetResult<T> {
    public required T Content { get; set; }
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
