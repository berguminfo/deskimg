// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.CalDAV.Options;

/// <summary>
/// Represents the root appsettings options.
/// </summary>
public sealed class AppOptions {

#if V2_0

    /// <summary>
    /// Specifies the <see cref="MessageFormatter"/> output format.
    /// </summary>
    public string OutputFormat { get; set; } = string.Empty;

#endif
#if V3_0

    /// <summary>
    /// Specifies the storage database name.
    /// </summary>
    public string StorageDatabase { get; set; } = "caldav";

#endif

    /// <inheritdoc cref="Deskimg.Extensions.FetchToken.Create(int)"/>
    public int ApiKey { get; set; } = 29;
}
