// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.CalDAV;

static partial class LoggerExtensions {

    [LoggerMessage(
        EventId = 200,
        Level = LogLevel.Trace,
        Message = "GET: Store is '{Store}'. Name is '{Name}'."
    )]
    public static partial void TraceGet(this ILogger logger, string store, string name);

    [LoggerMessage(
        EventId = 201,
        Level = LogLevel.Information,
        Message = "PUT: Store is '{Store}'. Name is '{Name}'. Created is {Created}."
    )]
    public static partial void TracePut(this ILogger logger, string store, string name, bool created);

    [LoggerMessage(
        EventId = 202,
        Level = LogLevel.Information,
        Message = "DELETE: Store is '{Store}'. Name is '{Name}'."
    )]
    public static partial void TraceDelete(this ILogger logger, string store, string name);

    [LoggerMessage(
        EventId = 207,
        Level = LogLevel.Trace,
        Message = "PROPFIND: Store is '{Store}'."
    )]
    public static partial void TracePropFind(this ILogger logger, string store);

    [LoggerMessage(
        EventId = 403,
        Level = LogLevel.Debug,
        Message = "Forbidden: Store is '{Store}'. Name is '{Name}'. See exception details for more information."
    )]
    public static partial void Forbidden(this ILogger logger, Exception ex, string? store = null, string? name = null);

    [LoggerMessage(
        EventId = 404,
        Level = LogLevel.Debug,
        Message = "NotFound: Store is '{Store}'. Name is '{Name}'."
    )]
    public static partial void NotFound(this ILogger logger, string store, string? name = null);

    [LoggerMessage(
        EventId = 500,
        Level = LogLevel.Error,
        Message = "Error fetching the resource. Store is '{Store}'. Name is '{Name}'. See exception details for more information."
    )]
    public static partial void Problem(this ILogger logger, Exception ex, string? store = null, string? name = null);
}
