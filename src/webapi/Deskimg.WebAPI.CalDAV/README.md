# Deskimg.WebAPI.CalDAV

WebAPI service for [CalDAV (RFC 4791)](https://datatracker.ietf.org/doc/html/rfc4791).

## Syntax

```sh
Deskimg.WebAPI.CalDAV
  [--OutputFormat <string>]
  [--StorageDatabase <string>]
  [--ApiKey <int>]
  [--Logging:LogLevel:Deskimg <LogLevel>]
  [--Logging:LogLevel:System.Net.Http.HttpResponseMessage <LogLevel>]
```

👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)

## Parameters

### --OutputFormat

**2.0:** Specifies the output format for filesystem storage.

The following substitutions are available for use.

- `{Local}`
  - Replaced with the value of `SpecialFolder.LocalApplicationData` + `Deskimg`.
- `{Roaming}`
  - Replaced with the value of `SpecialFolder.ApplicationData` + `Deskimg`.
- `{FileName}`
  - The file name of the fetch source.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --StorageDatabase

**3.0:** Specifies the MongoDB storage database.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Default value:   | Caldav                                                    |
| Required:        | False                                                     |

### --ApiKey

Optionally turn fetch token authentication on or off.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Int32                                                     |
| Default value:   | 29                                                        |
| Required:        | False                                                     |

### --Logging:LogLevel:Deskimg

Specifies the logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Information                                               |
| Required:        | False                                                     |

### --Logging:LogLevel:System.Net.Http.HttpResponseMessage

Specifies the HTTP logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Warning                                                   |
| Required:        | False                                                     |
