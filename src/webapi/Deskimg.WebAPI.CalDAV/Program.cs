// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.StaticFiles;
#if V3_0
using MongoDB.Driver;
#endif
using Deskimg.WebAPI.CalDAV.Options;
using Deskimg.WebAPI.CalDAV.Services;

var builder = WebApplication.CreateBuilder(args);

// add services to the container

FileExtensionContentTypeProvider contentTypeProvider = new();
var mimeMap = builder.Configuration.GetSection("mimeMap").Get<IDictionary<string, string>>();
if (mimeMap is not null) {
    foreach (var item in mimeMap) {
        contentTypeProvider.Mappings.Add(item.Key, item.Value);
    }
}

const string CorsPolicyName = "allow-any";
builder.Services.AddCors(options => {
    options.AddPolicy(CorsPolicyName, policy => {
        policy.AllowAnyOrigin();
        policy.AllowAnyMethod();
        policy.AllowAnyHeader();
    });
});
builder.Services.AddSingleton(contentTypeProvider);
builder.Services.AddControllers();
builder.Services.AddProblemDetails();
builder.Services.AddApiVersioning(options => {
    options.ReportApiVersions = true;
}).AddMvc();
builder.Services.Configure<AppOptions>(builder.Configuration);

// add API services
#if V1_0
builder.Services.AddSingleton<IsolatedStorageRepository>();
#endif
#if V2_0
builder.Services.AddSingleton<FileSystemRepository>();
#endif
#if V3_0
builder.Services.AddSingleton<MongoDbRepository>();
#endif

// add the MongoClient singleton
#if V3_0
string connectionString = Environment.GetEnvironmentVariable("MONGODB_URI") ?? "mongodb://localhost:27017";
builder.Services.AddSingleton(_ => new MongoClient(connectionString));
#endif

// learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();
app.Use((context, next) => {
    context.Response.Headers.AltSvc = $"h3=\":{context.Connection.LocalPort}\"";
    return next(context);
});
app.Use(async (context, nextMiddleware) => {
    context.Response.Headers["x-system-id"] = $"{nameof(Deskimg.WebAPI.CalDAV)}/{ThisAssembly.Git.InformationalVersion}";
    await nextMiddleware();
});

// configure the HTTP request pipeline

if (!app.Environment.IsProduction()) {
    const string name = "Deskimg.WebAPI.CalDAV";
    const string url = "../@typespec/openapi3/";
    app.UseSwaggerUI(options => {
        options.DocumentTitle = name;
        options.ConfigObject.Urls = [
#if V3_1
            new() { Url = $"{url}openapi.3.1.yaml", Name = $"{name} (3.1)" },
#endif
#if V3_0
            new() { Url = $"{url}openapi.3.0.yaml", Name = $"{name} (3.0)" },
#endif
#if V2_0
            new() { Url = $"{url}openapi.2.0.yaml", Name = $"{name} (2.0)" },
#endif
#if V1_0
            new() { Url = $"{url}openapi.1.0.yaml", Name = $"{name} (1.0)" },
#endif
        ];
    });
}

app.UseHttpsRedirection();
app.UseCors(CorsPolicyName);
app.UseDefaultFiles();
app.UseStaticFiles(new StaticFileOptions {
    ContentTypeProvider = contentTypeProvider
});
app.MapControllers();

await app.RunAsync();
