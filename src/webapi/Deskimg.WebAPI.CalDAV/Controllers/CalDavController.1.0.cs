// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_0

using Microsoft.AspNetCore.Mvc;
using Deskimg.WebAPI.CalDAV.Services;

namespace Deskimg.WebAPI.CalDAV.Controllers.V1;

using Deskimg.WebAPI.CalDAV.Options;

/// <summary>
/// Represents the <code>/api/1.0/caldav/</code> endpoint.
/// </summary>
/// <remarks>
/// Used to test the C:Deksimg.Blazor.Calendar.Spesialized.CalDAV calendar.
/// Put iCalendar files under '$HOME\AppData\Local\IsolatedStorage\{domain}\{machine}\{user}\AssemFiles'.
/// </remarks>
[ApiVersion(1.0)]
[Route("api/{version:apiVersion}/caldav")]
[ApiController]
public sealed class CalDavController(
    ILogger<CalDavController> logger,
    IOptions<AppOptions> appOptions,
    IsolatedStorageRepository service) : BaseCalDavController(logger, appOptions, service) {

    /// <inheritdoc/>
    [HttpHead($"{{store:regex({ValidRoute})}}/{{name:regex({ValidRoute})}}")]
    public new Task<IActionResult> Head(string store, string name) => base.Head(store, name);

    /// <inheritdoc/>
    [HttpGet($"{{store:regex({ValidRoute})}}/{{name:regex({ValidRoute})}}")]
    [Produces(MediaTypeNames.Text.Calendar, MediaTypeNames.Application.Json)]
    public new Task<IActionResult> Get(string store, string name) => base.Get(store, name);

    /// <inheritdoc/>
    [HttpPropFind]
    [Produces(MediaTypeNames.Text.Xml)]
    public new Task<IActionResult> PropFind() => base.PropFind();

    /// <inheritdoc/>
    [HttpPropFind($"{{store:regex({ValidRoute})}}")]
    [Produces(MediaTypeNames.Text.Xml)]
    public new Task<IActionResult> PropFind(string store) => base.PropFind(store);
}

#endif
