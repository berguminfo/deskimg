// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA1308 // Normalize strings to uppercase

using System.Globalization;
using System.Text;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Deskimg.WebAPI.CalDAV.Services;

namespace Deskimg.WebAPI.CalDAV.Controllers;

using Deskimg.Extensions;
using Deskimg.WebAPI.CalDAV.Options;

/// <summary>
/// Represents the base class for all CalDAV controllers.
/// </summary>
public abstract class BaseCalDavController(
    ILogger logger,
    IOptions<AppOptions> appOptions,
    AbstractCalDavRepository service) : Controller {

    readonly AppOptions options = appOptions.Value;

    /// <summary>
    /// Specifies the valid route regular expression.
    /// </summary>
    internal const string ValidRoute = """^[[^\x00-\x1F\\/:"\*\?<>\|]]+$""";

    const string DavNamespace = "DAV:";

    void SetResponseHeaders(IGetResult result) {
        Response.Headers.LastModified = result.LastModified.ToString("r");
        Response.Headers.ContentType = result.ContentType;
    }

    /// <summary>
    /// Handles 'OPTIONS' requests.
    /// </summary>
    [HttpOptions]
    public IActionResult Options() {
        Response.Headers.Allow = "OPTIONS";
        Response.Headers["Public"] = Response.Headers.Allow;
        return Ok();
    }

    /// <summary>
    /// Handles 'OPTIONS' requests.
    /// </summary>
    [HttpOptions("{path}")]
    public IActionResult Options(string path) {
        Response.Headers.Allow = "OPTIONS, PROPFIND, PROPPATCH";
        Response.Headers["Public"] = Response.Headers.Allow;
        Response.Headers["DAV"] = "1";
        return Ok();
    }

    /// <summary>
    /// Handles 'OPTIONS' requests.
    /// </summary>
    [HttpOptions("{store}/{name}")]
    public IActionResult Options(string store, string name) {
        Response.Headers.Allow = "OPTIONS, HEAD, GET, PUT, DELETE, COPY, MOVE";
        Response.Headers["Public"] = Response.Headers.Allow;
        return Ok();
    }

    /// <inheritdoc cref="HttpHeadAttribute"/>
    protected async Task<IActionResult> Head(string store, string name) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var result = await service.HeadAsync([store], name, HttpContext.RequestAborted);
            if (result is null) {
                logger.NotFound(store, name);
                return NotFound();
            }

            SetResponseHeaders(result);
            return Ok();

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (SystemException ex) {
            logger.Forbidden(ex, store, name);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex, store, name);
            return Problem(ex.Message);
        }
    }

    /// <inheritdoc cref="HttpGetAttribute"/>
    protected async Task<IActionResult> Get(string store, string name) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var result = await service.GetAsync([store], name, HttpContext.RequestAborted);
            if (result is null) {
                logger.NotFound(store, name);
                return NotFound();
            }

            // API 1.0
            if (result is IGetResult<byte[]> byteArrayResult) {
                return File(
                    byteArrayResult.Content,
                    result.ContentType ?? MediaTypeNames.Application.Octet,
                    result.LastModified,
                    new($@"""{result.LastModified.GetHashCode().ToString("x", CultureInfo.InvariantCulture)}"""));
            } else
            // API 2.0
            if (result is IGetResult<FileInfo> fileInfoResult) {
                return PhysicalFile(
                    fileInfoResult.Content.FullName,
                    result.ContentType ?? MediaTypeNames.Application.Octet,
                    result.LastModified,
                    new($@"""{result.LastModified.GetHashCode().ToString("x", CultureInfo.InvariantCulture)}"""));
            } else
            // API 3.0
            if (result is IGetResult<string> stringResult) {
                SetResponseHeaders(result);
                return Content(stringResult.Content);
            } else {
                return Problem($"Unknown type '{result.GetType()}'. Analyze {nameof(BaseCalDavController)} to resolve.");
            }

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (SystemException ex) {
            logger.Forbidden(ex, store, name);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex, store, name);
            return Problem(ex.Message);
        }
    }

    /// <inheritdoc cref="HttpPutAttribute"/>
    protected async Task<IActionResult> Put(string store, string name) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var statusCode = await service.PutAsync(
                [store],
                name,
                Request.Body,
                Request.Headers.ContentLength,
                Request.Headers.ContentType.FirstOrDefault(),
                Request.Headers.ContentEncoding.FirstOrDefault(),
                HttpContext.Connection.RemoteIpAddress?.ToString(),
                HttpContext.RequestAborted);
            return StatusCode((int)statusCode);

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (SystemException ex) {
            logger.Forbidden(ex, store, name);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex, store, name);
            return Problem(ex.Message);
        }
    }

    /// <inheritdoc cref="HttpDeleteAttribute"/>
    protected async Task<IActionResult> Delete(string store, string name) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var statusCode = await service.DeleteAsync([store], name, HttpContext.RequestAborted);
            return StatusCode((int)statusCode);

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (FileNotFoundException ex) {
            logger.NotFound(store, name);
            return NotFound(ex);
        } catch (SystemException ex) {
            logger.Forbidden(ex, store, name);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex, store, name);
            return Problem(ex.Message);
        }
    }

    /// <inheritdoc cref="HttpPropFindAttribute"/>
    protected async Task<IActionResult> PropFind() {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var collection = await service.PropFind(HttpContext.RequestAborted);
            string href = new UriBuilder(Request.Scheme, Request.Host.Host, Request.Host.Port ?? -1, Request.Path.Add("/")).Uri.AbsoluteUri;
            XNamespace d = DavNamespace;
            XDocument document = new(
                new XDeclaration("1.0", "utf-8", null),
                new XElement(d + "multistatus",
                    from item in collection
                    select
                        new XElement(d + "response",
                            new XElement(d + "href", href + item),
                            new XElement(d + "propstat",
                                new XElement(d + "status", "HTTP/1.1 200 OK"),
                                new XElement(d + "prop",
                                    new XElement(d + "displayname", item),
                                    new XElement(d + "getcontentlength", 0),
                                    new XElement(d + "getcontenttype", string.Empty),
                                    new XElement(d + "getlastmodified", DateTimeOffset.UtcNow.ToString("r")),
                                    new XElement(d + "resourcetype",
                                        new XElement(d + "collection")),
                                    new XElement(d + "supportedlock", string.Empty)
                                )
                            )
                        )
                )
            );

            var response = Content(document.ToString(), MediaTypeNames.Text.Xml, Encoding.UTF8);
            response.StatusCode = StatusCodes.Status207MultiStatus;
            return response;

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (SystemException ex) {
            logger.Forbidden(ex);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex);
            return Problem(ex.Message);
        }
    }

    /// <inheritdoc cref="HttpPropFindAttribute"/>
    protected async Task<IActionResult> PropFind(string store) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var collection = await service.PropFind([store], HttpContext.RequestAborted);
            string href = new UriBuilder(Request.Scheme, Request.Host.Host, Request.Host.Port ?? -1, Request.Path.Add("/")).Uri.AbsoluteUri;
            XNamespace d = DavNamespace;
            XDocument document = new(
                new XDeclaration("1.0", "utf-8", null),
                new XElement(d + "multistatus",
                    from item in collection
                    select
                        new XElement(d + "response",
                            new XElement(d + "href", href + item.Name),
                            new XElement(d + "propstat",
                                new XElement(d + "status", "HTTP/1.1 200 OK"),
                                new XElement(d + "prop",
                                    new XElement(d + "displayname", item.Name),
                                    new XElement(d + "getcontentlength", item.ContentLength),
                                    new XElement(d + "getcontenttype", item.ContentType),
                                    new XElement(d + "getlastmodified", item.LastModified.ToString("r")),
                                    new XElement(d + "resourcetype", string.Empty),
                                    new XElement(d + "supportedlock", string.Empty)
                                )
                            )
                        )
                )
            );

            var response = Content(document.ToString(), MediaTypeNames.Text.Xml, Encoding.UTF8);
            response.StatusCode = StatusCodes.Status207MultiStatus;
            return response;

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (FileNotFoundException ex) {
            logger.NotFound(store);
            return NotFound(ex);
        } catch (SystemException ex) {
            logger.Forbidden(ex);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex);
            return Problem(ex.Message);
        }
    }
}

#pragma warning restore CA1308 // Normalize strings to uppercase
