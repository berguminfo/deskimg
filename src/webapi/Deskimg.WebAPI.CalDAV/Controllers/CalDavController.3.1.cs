// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V3_1

using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Deskimg.WebAPI.CalDAV.Controllers.V3;

using Deskimg.Extensions;
using Deskimg.WebAPI.CalDAV.Options;

partial class CalDavController {

    readonly AppOptions options = appOptions30.Value;

    /// <inheritdoc cref="HttpPatchAttribute"/>
    [ApiVersion(3.1)]
    [HttpPatch($"{{store:regex(^sets$)}}/{{name:regex({ValidRoute})}}")]
    [Consumes(MediaTypeNames.Application.JsonPatchJson)]
    public async Task<IActionResult> Patch(string store, string name, [FromBody] JsonPatchDocument<LayoutSet> patchDoc) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var modified = await service.PatchAsync(
                [store],
                name,
                patchDoc,
                HttpContext.Connection.RemoteIpAddress?.ToString(),
                HttpContext.RequestAborted);
            return modified ? Ok() : BadRequest();
        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (FileNotFoundException ex) {
            return NotFound(ex);
        } catch (SystemException ex) {
            logger.Forbidden(ex, store, name);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (Exception ex) {
            logger.Problem(ex, store, name);
            return Problem(ex.Message);
        }
    }
}

#endif
