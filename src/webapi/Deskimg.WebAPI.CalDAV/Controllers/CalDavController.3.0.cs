// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V3_0

using Microsoft.AspNetCore.Mvc;
using Deskimg.WebAPI.CalDAV.Services;

namespace Deskimg.WebAPI.CalDAV.Controllers.V3;

using Deskimg.WebAPI.CalDAV.Options;

/// <summary>
/// Represents the <code>/api/3.0/caldav/</code> endpoint.
/// </summary>
[ApiVersion(3.0)]
[Route("api/{version:apiVersion}/caldav")]
[ApiController]
public sealed partial class CalDavController(
    ILogger<CalDavController> logger,
    IOptions<AppOptions> appOptions,
    IOptions<AppOptions> appOptions30,
    MongoDbRepository service) : BaseCalDavController(logger, appOptions, service) {

    /// <inheritdoc/>
    [HttpHead($"{{store:regex({ValidRoute})}}/{{name:regex({ValidRoute})}}")]
    public new Task<IActionResult> Head(string store, string name) => base.Head(store, name);

    /// <inheritdoc/>
    [HttpGet($"{{store:regex({ValidRoute})}}/{{name:regex({ValidRoute})}}")]
    [Produces(MediaTypeNames.Text.Calendar, MediaTypeNames.Application.Json)]
    public new Task<IActionResult> Get(string store, string name) => base.Get(store, name);

    /// <inheritdoc/>
    [HttpPut($"{{store:regex({ValidRoute})}}/{{name:regex({ValidRoute})}}")]
    [Consumes(MediaTypeNames.Text.Calendar, MediaTypeNames.Application.Json)]
    public new Task<IActionResult> Put(string store, string name) => base.Put(store, name);

    /// <inheritdoc/>
    [HttpDelete($"{{store:regex({ValidRoute})}}/{{name:regex({ValidRoute})}}")]
    public new Task<IActionResult> Delete(string store, string name) => base.Delete(store, name);

    /// <inheritdoc/>
    [HttpPropFind]
    [Produces(MediaTypeNames.Text.Xml)]
    public new Task<IActionResult> PropFind() => base.PropFind();

    /// <inheritdoc/>
    [HttpPropFind($"{{store:regex({ValidRoute})}}")]
    [Produces(MediaTypeNames.Text.Xml)]
    public new Task<IActionResult> PropFind(string store) => base.PropFind(store);
}

#endif
