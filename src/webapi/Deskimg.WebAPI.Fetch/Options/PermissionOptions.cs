// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA2227 // Collection properties should be read only

namespace Deskimg.WebAPI.Fetch.Options;

/// <summary>
/// Represents the additional appsettings options.
/// </summary>
public sealed class PermissionOptions {

    /// <summary>
    /// Turns the permission feature on or off.
    /// </summary>
    public bool Enabled { get; set; } = true;

    /// <summary>
    /// Gets or sets the list of denied sites.
    /// </summary>
    public ICollection<string>? Deny { get; set; }

    /// <summary>
    /// Gets or sets the list of allowed sites.
    /// </summary>
    public ICollection<string>? Allow { get; set; }
}

#pragma warning restore CA2227 // Collection properties should be read only
