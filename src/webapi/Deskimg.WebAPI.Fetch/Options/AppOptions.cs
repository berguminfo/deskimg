// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Fetch.Options;

/// <summary>
/// Represents the root appsettings options.
/// </summary>
public sealed class AppOptions {

#if V1_0

    /// <summary>
    /// Specifies the <see cref="MessageFormatter"/> output format.
    /// </summary>
    public string OutputFormat { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the cache storage file.
    /// </summary>
    public string Index { get; set; } = "@.json";

    /// <summary>
    /// Gets or sets the cache file commit delay.
    /// </summary>
    public TimeSpan DelayCommit { get; set; } = TimeSpan.FromSeconds(30);

#endif

    /// <summary>
    /// Deny fetch on metered connections.
    /// </summary>
    public bool EnforceMeteredConnection { get; set; } = true;

    /// <summary>
    /// True to enforce direct fetch.
    /// </summary>
    public bool EnforceRefresh { get; set; }

    /// <summary>
    /// Get or sets the cache options.
    /// </summary>
    public HybridCacheEntryOptions? Cache { get; set; }

    /// <summary>
    /// Gets the list of endpoint permissions.
    /// </summary>
    public PermissionOptions Permission { get; set; } = new();

    /// <inheritdoc cref="Deskimg.Extensions.FetchToken.Create(int)"/>
    public int ApiKey { get; set; } = 29;
}
