// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net;
using Microsoft.AspNetCore.StaticFiles;
using Deskimg.WebAPI.Fetch.Options;
using Deskimg.WebAPI.Fetch.Services;

var builder = WebApplication.CreateBuilder(args);

// add services to the container

var mimeMap = builder.Configuration.GetSection("mimeMap").Get<IDictionary<string, string>>();
FileExtensionContentTypeProvider contentTypeProvider = mimeMap is null ? new() : new(mimeMap);

const string CorsPolicyName = "allow-any";
builder.Services.AddCors(options => {
    options.AddPolicy(CorsPolicyName, policy => {
        policy.AllowAnyOrigin();
        policy.AllowAnyMethod();
        policy.AllowAnyHeader();
    });
});
builder.Services.AddSingleton(contentTypeProvider);
builder.Services.AddControllers();
builder.Services.AddProblemDetails();
builder.Services.AddApiVersioning(options => {
    options.ReportApiVersions = true;
}).AddMvc();
builder.Services.AddHttpClient("H3", http => {
    http.DefaultRequestVersion = HttpVersion.Version30;
    http.DefaultVersionPolicy = HttpVersionPolicy.RequestVersionOrLower;
    var timeout = builder.Configuration.GetValue<TimeSpan?>("ConnectTimeout");
    if (timeout is not null) {
        http.Timeout = timeout.Value;
    }
});
builder.Services.Configure<AppOptions>(builder.Configuration);
builder.Services.AddSingleton<NetworkService>();
#if V2_0
builder.Services.AddHybridCache();
#endif

// add API services
#if V1_0
builder.Services.AddSingleton<FileSystemService>();
#endif

// learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();
app.Use((context, next) => {
    context.Response.Headers.AltSvc = $"h3=\":{context.Connection.LocalPort}\"";
    return next(context);
});
string accessControlAllowOrigin = app.Configuration.GetValue(nameof(IHeaderDictionary.AccessControlAllowOrigin), "*");
app.Use((context, next) => {
    context.Response.Headers.AccessControlAllowOrigin = accessControlAllowOrigin;
    context.Response.Headers.Vary = "Origin";
    return next(context);
});
app.Use(async (context, nextMiddleware) => {
    context.Response.Headers["x-system-id"] = $"{nameof(Deskimg.WebAPI.Fetch)}/{ThisAssembly.Git.InformationalVersion}";
    await nextMiddleware();
});

// configure the HTTP request pipeline

if (!app.Environment.IsProduction()) {
    const string name = "Deskimg.WebAPI.Fetch";
    const string url = "../@typespec/openapi3/";
    app.UseSwaggerUI(options => {
        options.DocumentTitle = name;
        options.ConfigObject.Urls = [
#if V2_1
            new() { Url = $"{url}openapi.2.1.yaml", Name = $"{name} (2.1)" },
#endif
#if V2_0
            new() { Url = $"{url}openapi.2.0.yaml", Name = $"{name} (2.0)" },
#endif
#if V1_1
            new() { Url = $"{url}openapi.1.1.yaml", Name = $"{name} (1.1)" },
#endif
#if V1_0
            new() { Url = $"{url}openapi.1.0.yaml", Name = $"{name} (1.0)" },
#endif
        ];
    });
}

#if V1_0
var fetchService1 = app.Services.GetRequiredService<FileSystemService>();
await fetchService1.LoadAsync();
fetchService1.StartCommitTimer();
#endif

app.UseHttpsRedirection();
app.UseCors(CorsPolicyName);
app.UseDefaultFiles();
app.UseStaticFiles(new StaticFileOptions {
    ContentTypeProvider = contentTypeProvider
});
app.MapControllers();

await app.RunAsync();
