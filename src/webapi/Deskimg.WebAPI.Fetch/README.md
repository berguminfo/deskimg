# Deskimg.WebAPI.Fetch

WebAPI service for the `Fetch` proxy.

## Syntax

```sh
Deskimg.WebAPI.Fetch
  [--EnforceMeteredConnection <bool>]
  [--EnforceRefresh <bool>]
  [--ConnectTimeout <TimeSpan>]
  [--OutputFormat <string>]
  [--Index <string>]
  [--DelayCommit <TimeSpan>]
  [--Cache:LocalCacheExpiration <TimeSpan>]
  [--Cache:Expiration <TimeSpan>]
  [--Permission:Enabled <bool>]
  [--Permission:Deny <string[]>]
  [--Permission:Allow <string[]>]
  [--ApiKey <int>]
  [--Logging:LogLevel:Deskimg <LogLevel>]
  [--Logging:LogLevel:System.Net.Http.HttpResponseMessage <LogLevel>]
```

👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)

## Parameters

### --EnforceMeteredConnection

Deny fetch on metered connections.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | True                                                      |
| Required:        | False                                                     |

### --EnforceRefresh

True to enforce direct fetch.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | False                                                     |
| Required:        | False                                                     |

### --ConnectTimeout

Specifies the `HttpClient.Timeout` property.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 00:00:10                                                  |
| Required:        | False                                                     |

### --OutputFormat

**api/1.0:** Specifies the output format for filesystem storage.

The following substitutions are available for use.

- `{Local}`
  - Replaced with the value of `SpecialFolder.LocalApplicationData` + `Deskimg`.
- `{Roaming}`
  - Replaced with the value of `SpecialFolder.ApplicationData` + `Deskimg`.
- `{FileName}`
  - The file name of the fetch source.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --Index

**api/1.0:** Specifies the cache storage file.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Default value:   | @.json                                                    |
| Required:        | False                                                     |

### --DelayCommit

**api/1.0:** Specifies the cache file commit delay.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 00:00:30                                                  |
| Required:        | False                                                     |

### --Cache:LocalCacheExpiration

Specifies the amount of time cache entries expires and should be refreshed.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 12:00:00                                                  |
| Required:        | False                                                     |

### --Cache:Expiration

Specifies the amount of time cache entries should be permanently be removed.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 7.00:00:00                                                |
| Required:        | False                                                     |

### --Permission:Enabled

Turns the permission feature on or off

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | True                                                      |
| Required:        | False                                                     |

### --Permission:Deny

Specifies the list of URI's the fetch service must not fetch.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String[]                                                  |
| Required:        | False                                                     |

### --Permission:Allow

Specifies the list or URI's the fetch service can fetch.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String[]                                                  |
| Required:        | False                                                     |

### --ApiKey

Optionally turn fetch token authentication on or off.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Int32                                                     |
| Default value:   | 29                                                        |
| Required:        | False                                                     |

### --Logging:LogLevel:Deskimg

Specifies the logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Information                                               |
| Required:        | False                                                     |

### --Logging:LogLevel:System.Net.Http.HttpResponseMessage

Specifies the HTTP logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Warning                                                   |
| Required:        | False                                                     |
