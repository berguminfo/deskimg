// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text;

namespace Deskimg.WebAPI.Fetch.Extensions;

/// <summary>
/// Represents console only <see cref="ILogger"/> extensions.
/// </summary>
static class ConsoleLogger {

    const string RequestSymbol = "\u2191 ";
    const string ResponseSymbol = "\u2193 ";

    static string Primary(object? value) => VT.Escape(value, 0x0d6efd);

    static string Success(object? value) => VT.Escape(value, 0x198754);

    static string Danger(object? value) => VT.Escape(value, 0xdc3545);

    static string Keyword(object? value) => VT.Escape(value, 0x00bff9);

    static string RequestUri(object? value) => VT.SGR(VT.SelectGraphicRendition.Underline) + VT.Escape(value, 0xd63384);

    static string Status(object? value) => VT.SGR(VT.SelectGraphicRendition.Bold) + VT.Escape(value, 0xfd7e14);

    static string Name(object? value) => VT.Escape(value, 0x91dacd);

    static string Value(string value) => value;

    static void Request(StringBuilder sb, bool succeeded, params object?[] values) {
        sb.Append(succeeded ? Primary(RequestSymbol) : Danger(RequestSymbol));
        sb.AppendJoin(string.Empty, values);
        sb.AppendLine();
    }

    static void Response(StringBuilder sb, bool succeeded, params object?[] values) {
        sb.Append(succeeded ? Success(ResponseSymbol) : Danger(ResponseSymbol));
        sb.AppendJoin(string.Empty, values);
        sb.AppendLine();
    }

    /// <summary>
    /// Logs the result of fetch operations.
    /// </summary>
    /// <param name="logger">The <see cref="ILogger"/> instance.</param>
    /// <param name="response">The fetch response object.</param>
    /// <param name="logLevel">The target <see cref="LogLevel"/>.</param>
    public static void LogResponse(this ILogger logger, HttpResponseMessage response, LogLevel logLevel = LogLevel.Trace) {
        if (logger.IsEnabled(logLevel)) {
            bool succeeded = response.IsSuccessStatusCode;

            StringBuilder sb = new();
            if (response.RequestMessage != null) {
                HttpRequestMessage request = response.RequestMessage;
                Request(sb, succeeded, Keyword(request.Method), " ", RequestUri(request.RequestUri), " ", Keyword($"HTTP/{request.Version}"));
                foreach (var requestHeader in request.Headers) {
                    string value = string.Join(';', requestHeader.Value);
                    Request(sb, succeeded, Name(requestHeader.Key), ": ", Value(value));
                }
            }

            Response(sb, succeeded, Keyword($"HTTP/{response.Version}"), " ", Status((int)response.StatusCode), " ", Status(response.ReasonPhrase));
            foreach (var responseHeader in response.Headers) {
                string value = string.Join(';', responseHeader.Value);
                Response(sb, succeeded, Name(responseHeader.Key), ": ", Value(value));
            }

            // NOTE: must use console, the current ILogger don't support ANSI escape sequences
            Console.Write(sb.ToString());
        }
    }

    static class VT {
        const string ESC = "\u001b";
        const string CSI = ESC + "[";

        internal static class SelectGraphicRendition {
            public const string Reset = "";
            public const string Bold = "1";
            public const string Italic = "3";
            public const string Underline = "4";
            public const string SelectRgbForeground = "38;2;";
        }

        internal static string SGR(string n, params object[] parameters) =>
            CSI + n + string.Join(";", parameters) + "m";

        internal static string Escape(object? value, uint color) {
            var r = color >> 16 & 0xff;
            var g = color >> 8 & 0xff;
            var b = color & 0xff;
            return
                SGR(SelectGraphicRendition.SelectRgbForeground, r, g, b) +
                value +
                SGR(SelectGraphicRendition.Reset);
        }
    }
}
