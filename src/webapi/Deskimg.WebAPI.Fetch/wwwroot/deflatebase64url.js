// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

import { base64_encode, base64url_encode, base64_decode } from "./base64.js";

/**
 * Encodes supplied data into Base64 and replaces any URL encodable characters into
 * non-URL encodable characters.
 *
 * @param {String} data Data to be encoded.
 * @return {String} Base64 encoded string modified with non-URL encodable characters.
 */
export async function encode(data, options = {}) {
    const textEncoder = new TextEncoderStream();
    const deflateStream = textEncoder.readable.pipeThrough(new CompressionStream("deflate"));

    // UTF-8 encode and write it into the deflate stream
    const writer = textEncoder.writable.getWriter();
    await writer.write(data);
    writer.close();

    // read all compressed chunks and find the total size
    // first chunk probably is the ZLIB header [120, 156]
    // last chunk prepended with the ADLER-32 checksum
    const chunkBuffer = [];
    let totalSize = 0;
    let result = {};
    const reader = deflateStream.getReader();
    while ((result = await reader.read()).done == false) {
        chunkBuffer.push(result.value);
        totalSize += result.value.byteLength;
    }

    // merge all chunks
    const output = new Uint8Array(totalSize);
    let offset = 0;
    for (const chunk of chunkBuffer) {
        output.set(chunk, offset);
        offset += chunk.length;
    }

    if (options && options.base64) {
        return base64_encode(output);
    } else {
        return base64url_encode(output);
    }

}

/**
* Decodes supplied string by replacing the non-URL encodable characters with URL
* encodable characters and then decodes the Base64 string.
*
* @param {String} text The string to be decoded.
* @return {String} The decoded data.
*/
export async function decode(text) {
    const stream = new ReadableStream({
        start(controller) {
            const chunk = new Uint8Array(base64_decode(text));
            controller.enqueue(chunk);
        },
        pull(controller) {
            controller.close();
        },
        stop() {
        }
    });
    const deflateStream = stream.pipeThrough(new DecompressionStream("deflate"));

    // read all decompressed chunks
    // NOTE: using chunk buffer in case the readable stream has to slice the data
    const chunkBuffer = [];
    let totalSize = 0;
    let result = {};
    const reader = deflateStream.getReader();
    while ((result = await reader.read()).done == false) {
        chunkBuffer.push(result.value);
        totalSize += result.value.byteLength;
    }

    // merge all chunks
    const output = new Uint8Array(totalSize);
    let offset = 0;
    for (const chunk of chunkBuffer) {
        output.set(chunk, offset);
        offset += chunk.length;
    }

    return new TextDecoder().decode(output);
}
