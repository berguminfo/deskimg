// JavaScript base64/base64url encoder/decoder Source Code
// Encode and decode text to/from base64 and base64url, as per RFC-4648.
// This source code is in the public domain.
// You may use, share, modify it freely, without any conditions or restrictions.

const BASE64_DICT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const BASE64URL_DICT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
const BASE64_PADDING = '=';

// Internal helper to encode data to base64 using specified dictionary.
function base64_encode_data(data, dict, padding) {
    const dst = [];
    let i;
    for (i = 0; i <= data.length - 3; i += 3) {
        dst.push(dict.charAt(data[i] >>> 2));
        dst.push(dict.charAt(((data[i] & 3) << 4) | (data[i + 1] >>> 4)));
        dst.push(dict.charAt(((data[i + 1] & 15) << 2) | (data[i + 2] >>> 6)));
        dst.push(dict.charAt(data[i + 2] & 63));
    }

    if (data.length % 3 == 2) {
        dst.push(dict.charAt(data[i] >>> 2));
        dst.push(dict.charAt(((data[i] & 3) << 4) | (data[i + 1] >>> 4)));
        dst.push(dict.charAt(((data[i + 1] & 15) << 2)));
        if (padding) {
            dst.push(BASE64_PADDING);
        }
    }
    else if (data.length % 3 == 1) {
        dst.push(dict.charAt(data[i] >>> 2));
        dst.push(dict.charAt(((data[i] & 3) << 4)));
        if (padding) {
            dst.push(BASE64_PADDING);
            dst.push(BASE64_PADDING);
        }
    }

    return dst.join("");
}

/**
 * Encodes supplied data into Base64 and replaces any URL encodable characters into
 * non-URL encodable characters.
 *
 * @param {ArrayLike} data Data to be encoded.
 * @return {String} Base64 encoded string modified with non-URL encodable characters.
 */
export function base64_encode(data) {
    return base64_encode_data(data, BASE64_DICT, true);
}

/**
 * Encodes supplied data into Base64Url and replaces any URL encodable characters into
 * non-URL encodable characters.
 *
 * @param {ArrayLike} data Data to be encoded.
 * @return {String} Base64 encoded string modified with non-URL encodable characters.
 */
export function base64url_encode(data) {
    return base64_encode_data(data, BASE64URL_DICT, false);
}

// Internal helper to translate a base64 character to its integer index.
function base64_charIndex(c) {
    if (c == "+") return 62;
    if (c == "/") return 63;
    return BASE64URL_DICT.indexOf(c);
}

/**
* Decodes supplied string by replacing the non-URL encodable characters with URL
* encodable characters and then decodes the Base64 string.
*
* @param {String} text The string to be decoded.
* @return {ArrayLike} The decoded data.
 */
export function base64_decode(text) {
    switch (text.length % 4) {
        case 3:
            text += BASE64_PADDING;
            break;
        case 2:
            text += BASE64_PADDING;
            text += BASE64_PADDING;
            break;
    }

    const dst = [];
    let i, a, b, c, d;

    for (i = 0; i < text.length - 3; i += 4) {
        a = base64_charIndex(text.charAt(i + 0));
        b = base64_charIndex(text.charAt(i + 1));
        c = base64_charIndex(text.charAt(i + 2));
        d = base64_charIndex(text.charAt(i + 3));

        dst.push((a << 2) | (b >>> 4));
        if (text.charAt(i + 2) != BASE64_PADDING)
            dst.push(((b << 4) & 0xF0) | ((c >>> 2) & 0x0F));
        if (text.charAt(i + 3) != BASE64_PADDING)
            dst.push(((c << 6) & 0xC0) | d);
    }

    return dst;
}
