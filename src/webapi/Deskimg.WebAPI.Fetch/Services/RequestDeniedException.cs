// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// Represents the <see cref="Exception"/> thrown if the request uri is denied.
/// </summary>
/// <remarks>
/// The configuration path <c>Fetch:Allow</c> specifies witch URI's is allowed to be fetched.
/// </remarks>
public class RequestDeniedException : Exception {

    /// <inheritdoc/>
    public RequestDeniedException() { }

    /// <inheritdoc/>
    public RequestDeniedException(string? message) : base(message) { }

    /// <inheritdoc/>
    public RequestDeniedException(string? message, Exception? innerException) : base(message, innerException) { }
}
