// Deskimg.Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// Represents the abstract Fetch service.
/// </summary>
public abstract class AbstractFetchService {

    /// <summary>
    /// Creates a SHA identifier given the specified URI.
    /// </summary>
    /// <param name="uri">The URI.</param>
    /// <returns>The identifier.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    protected static string CreateKeyFromUri(Uri uri) {
        // NOTE: sync with ConfigurationExtensions
        // NOTE: use URI escaping for Unicode support
        byte[] bytes = Encoding.UTF8.GetBytes(uri.AbsoluteUri);
        return Base64UrlTextEncoder.Encode(
#if SHA1
            SHA1.HashData(bytes)
#else
            SHA256.HashData(bytes)
#endif
        );
    }

    /// <summary>
    /// Creates a request message intended to fetch a remote resource.
    /// </summary>
    /// <param name="method">The HTTP method (verb) to request.</param>
    /// <param name="requestUri">The URI of the remote resource to fetch.</param>
    /// <returns>The request message.</returns>
    protected static HttpRequestMessage CreateHttpRequestMessage(HttpMethod method, Uri requestUri) {
        HttpRequestMessage request = new(method, requestUri);
        request.Headers.Host = requestUri.Host;
        request.Headers.Accept.Add(new("*/*"));
        request.Headers.UserAgent.Clear();
        // NOTE: the User-Agent header is required by some endpoints
        request.Headers.UserAgent.Add(new(
            "kestrela",
            $"{ThisAssembly.Git.BaseVersion.Major}.{ThisAssembly.Git.BaseVersion.Minor}"));
        return request;
    }

    /// <summary>
    /// Gets all the fetched items in cache.
    /// </summary>
    /// <returns>The item collection.</returns>
    public abstract IEnumerable<KeyValuePair<string, FetchEntry>>? Index();

    /// <summary>
    /// Fetches the <paramref name="base64url"/> using the HEAD method.
    /// </summary>
    /// <param name="base64url">The resource to fetch.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <exception cref="HttpRequestException">A problem fetching the resource occurred.</exception>
    /// <returns>The fetch result.</returns>
    public abstract Task<FetchEntry> HeadAsync(string base64url, CancellationToken cancellationToken = default);

    /// <summary>
    /// Fetches the <paramref name="base64url"/> using the GET method.
    /// </summary>
    /// <param name="base64url">The resource to fetch.</param>
    /// <param name="headers">The fetch headers.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <exception cref="FileNotFoundException">The file cannot be found. The file must already exist in these modes.</exception>
    /// <exception cref="IOException">An I/O error occurred.</exception>
    /// <exception cref="HttpRequestException">A problem fetching the resource occurred.</exception>
    /// <returns>The fetch result.</returns>
    public abstract Task<(FetchEntry, FileInfo)> GetAsync(string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default);

    /// <summary>
    /// Fetches the <paramref name="base64url"/> using the PROPFIND method.
    /// </summary>
    /// <param name="base64url">The resource to fetch.</param>
    /// <param name="headers">The fetch headers.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <exception cref="FileNotFoundException">The file cannot be found. The file must already exist in these modes.</exception>
    /// <exception cref="IOException">An I/O error occurred.</exception>
    /// <exception cref="HttpRequestException">A problem fetching the resource occurred.</exception>
    /// <returns>The fetch result.</returns>
    public abstract Task<(FetchEntry, FileInfo)> PropFindAsync(string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default);
}
