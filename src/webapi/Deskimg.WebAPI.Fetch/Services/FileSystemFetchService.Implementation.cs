// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_0

using System.Runtime.CompilerServices;
using System.Net;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace Deskimg.WebAPI.Fetch.Services;

using Deskimg.Extensions;
using Deskimg.WebAPI.Fetch;
using Deskimg.WebAPI.Fetch.Extensions;

/// <summary>
/// Represents a storage based fetch service.
/// </summary>
partial class FileSystemService {

    const string ForwardHeader = "X--";

    static readonly string s_roamingPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
        nameof(Deskimg));
    static readonly string s_localPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        nameof(Deskimg));

    IList<Regex>? _fetchAllowedCollection;

    /// <summary>
    /// Gets the requested resource permission.
    /// </summary>
    /// <param name="requestUri">The resource.</param>
    /// <returns>The configured permission.</returns>
    bool IsFetchAllowed(Uri requestUri) {
        if (options.Permission.Enabled) {
            _fetchAllowedCollection ??= (
                from item in options.Permission?.Allow ?? Enumerable.Empty<string>()
                select new Regex($"^{Regex.Escape(item)}", RegexOptions.IgnoreCase | RegexOptions.Compiled)
            ).ToList();
            return _fetchAllowedCollection.Any(x => x.IsMatch(requestUri.AbsoluteUri));
        } else {
            return true;
        }
    }

    /// <summary>
    /// Gets the physical path of the given file.
    /// </summary>
    /// <param name="fileName">The name of the file.</param>
    /// <returns>The path.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    FileInfo GetPhysicalPath(string fileName) =>
        new(MessageFormatter.Format(options.OutputFormat, new {
            Roaming = s_roamingPath,
            Local = s_localPath,
            FileName = fileName
        }));

    /// <inheritdoc/>
    public override async Task<FetchEntry> HeadAsync(string base64url, CancellationToken cancellationToken = default) {
        ArgumentNullException.ThrowIfNull(_cache);
        ArgumentNullException.ThrowIfNull(base64url, nameof(base64url));

        Uri requestUri = new(DeflateBase64Url.Decode(base64url));
        if (!IsFetchAllowed(requestUri)) {
            throw new RequestDeniedException(requestUri.AbsoluteUri);
        }

        string key = CreateKeyFromUri(requestUri);
        FetchEntry? entry = _cache.TryGetValue(key, out var item) ? item : null;
        if (!options.EnforceRefresh && entry is not null && entry.State == FetchState.Ready) {

            // fetch from cache result
            logger.FetchFromCache(requestUri, entry.ExpiresAtTime.ToLocalTime());
            return entry;
        }

        // throw if on metered connection, controller should catch the error
        if (options.EnforceMeteredConnection && network.IsInternetOnMeteredConnection) {
            throw new MeteredConnectionException(requestUri.AbsolutePath);
        }

        // fetch from source
        using HttpRequestMessage request = CreateHttpRequestMessage(HttpMethod.Head, requestUri);
        logger.FetchStarting(requestUri);

        using HttpResponseMessage response = await http.SendAsync(request, cancellationToken: cancellationToken);
        consoleLogger.LogResponse(response);

        response.EnsureSuccessStatusCode();

        // return a new instance on successfully response
        entry = new();
        entry.Update(response);
        return entry;
    }

    /// <summary>
    /// Fetches the <paramref name="base64url"/> using the <paramref name="method"/> method.
    /// </summary>
    /// <param name="method">The HTTP method.</param>
    /// <param name="base64url">The resource to fetch.</param>
    /// <param name="headers">The fetch headers.</param>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <exception cref="RequestDeniedException">The requested resource has been denied access. Only allowed resources can be actively granted access.</exception>
    /// <exception cref="MeteredConnectionException">The current Internet interface is up but configured to be a metered connection.</exception>
    /// <exception cref="FileNotFoundException">The file cannot be found. The file must already exist in these modes.</exception>
    /// <exception cref="IOException">An I/O error occurred.</exception>
    /// <exception cref="HttpRequestException">A problem fetching the resource occurred.</exception>
    /// <returns>The fetch result.</returns>
    async Task<(FetchEntry, FileInfo)> FetchAsync(HttpMethod method, string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default) {
        ArgumentNullException.ThrowIfNull(_cache);
        ArgumentNullException.ThrowIfNull(base64url, nameof(base64url));

        Uri requestUri = new(DeflateBase64Url.Decode(base64url));
        if (!IsFetchAllowed(requestUri)) {
            throw new RequestDeniedException(requestUri.AbsoluteUri);
        }

        string key = CreateKeyFromUri(requestUri);
        FetchEntry entry = _cache.GetOrAdd(key, new FetchEntry());
        FileInfo physicalFile = GetPhysicalPath(key);

        // give up early if the configured output path is invalid
        if (physicalFile.Directory is null) {
            throw new InvalidOperationException($"Invalid path: '{physicalFile}");
        }

#if UPDATE_STATE_LOCK
        lock (_updateStateLock) {
#endif

        if (!options.EnforceRefresh &&
            entry.State == FetchState.Ready &&
            DateTimeOffset.UtcNow < entry.ExpiresAtTime) {

            // fetch from cache result
            logger.FetchFromCache(requestUri, entry.ExpiresAtTime.ToLocalTime());
            return (entry, physicalFile);

        } else {

            // ensure any TryGet gets the new state
            // about #define UPDATE_STATE_LOCK:
            //   - accepts this short period of multiple thread gets the same HasExpiered item
            //   - the next thread will only overwrite the same resource or fail with an Exception

            entry.State = FetchState.Interactive;
        }

#if UPDATE_STATE_LOCK
        }
#endif

        // throw on metered connection, expect controller to catch
        if (options.EnforceMeteredConnection && network.IsInternetOnMeteredConnection) {
            throw new MeteredConnectionException(requestUri.AbsolutePath);
        }

        try {

            // fetch remote resource
            using HttpRequestMessage request = CreateHttpRequestMessage(method, requestUri);
            request.Headers.IfModifiedSince = entry.LastModified;
            request.Headers.IfNoneMatch.TryParseAdd(entry.ETag);
            foreach (var forward in headers.Where(x => x.Key.StartsWith(ForwardHeader, StringComparison.OrdinalIgnoreCase))) {
                string forwardKey = forward.Key[ForwardHeader.Length..];
                foreach (var forwardValue in forward.Value) {
                    request.Headers.Add(forwardKey, forwardValue);
                }
            }

            logger.FetchStarting(requestUri);

            using HttpResponseMessage response = await http.SendAsync(request, cancellationToken);
            consoleLogger.LogResponse(response);

            if (response.StatusCode == HttpStatusCode.NotModified) {

                // resource has not modified, update the cache and commit
                entry.State = FetchState.Ready;
                logger.UnmodifiedFetch(requestUri, entry.ExpiresAtTime.ToLocalTime());
            } else {
                response.EnsureSuccessStatusCode();

                // check if directory can be created
                if (!physicalFile.Directory.Exists) {
                    physicalFile.Directory.Create();
                }

                // create a temporary file, and copy the buffer
                string tempPath = physicalFile.FullName + ".tmp";
                using (var stream = File.Create(tempPath)) {
                    long length = response.Content.Headers.ContentLength ?? long.MinValue;
                    if (length > 0) {
                        stream.SetLength(length);
                    }
                    await response.Content.CopyToAsync(stream, cancellationToken);
                }

                // set content-length header to file length
                // used by SetResponseMessage
                response.Content.Headers.ContentLength = new FileInfo(tempPath).Length;

                // replace temporary and update state
                if (!physicalFile.Exists) {
                    // file must exist and closed before calling File.Replace
                    using (physicalFile.Create()) { }
                }
                File.Replace(tempPath, physicalFile.FullName, null, true);
                entry.State = FetchState.Ready;
            }

            // always update item properties
            CacheControlHeaderValue? cacheControl = null;
            if (CacheControlHeaderValue.TryParse(headers.CacheControl, out var value)) {
                cacheControl = value;
            }
            entry.Update(response, options.Cache, cacheControl);

            // commit the new state
            // no need to take care of errors, the item is already unavailable or expired.
            Interlocked.Increment(ref _commitCounter);
            logger.CompletedFetch(requestUri, entry.ExpiresAtTime.ToLocalTime(), entry.ExpiresAtTime.ToUniversalTime() - DateTimeOffset.UtcNow);
            return (entry, physicalFile);

        } catch (Exception) {

            // network may be down but try send previous version
            if (physicalFile.Exists) {
                logger.InteractiveFetch(requestUri, entry.AbsoluteExpiration.ToLocalTime());
                return (entry, physicalFile);
            }

            // record error state
            entry.State = FetchState.Error;

            // last resort, just retry forever (maybe the server wakes up sometime)
            throw;
        }
    }

    /// <inheritdoc/>
    public override Task<(FetchEntry, FileInfo)> GetAsync(string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default) =>
        FetchAsync(HttpMethod.Get, base64url, headers, cancellationToken);

    /// <inheritdoc/>
    public override Task<(FetchEntry, FileInfo)> PropFindAsync(string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default) =>
        FetchAsync(new("PROPFIND"), base64url, headers, cancellationToken);
}

#endif
