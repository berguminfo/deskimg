// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// Represents the <see cref="Exception"/> thrown if the current connection is metered.
/// </summary>
public class MeteredConnectionException : Exception {

    /// <inheritdoc/>
    public MeteredConnectionException() { }

    /// <inheritdoc/>
    public MeteredConnectionException(string? message) : base(message) { }

    /// <inheritdoc/>
    public MeteredConnectionException(string? message, Exception? innerException) : base(message, innerException) { }
}
