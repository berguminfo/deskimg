// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace Deskimg.WebAPI.Fetch.Services;

partial class FetchEntry {

    /// <summary>
    /// Sets properties from <paramref name="response"/> properties.
    /// </summary>
    /// <param name="response">The HTTP response instance.</param>
    public void Update(HttpResponseMessage response) {
        Host = response.RequestMessage?.RequestUri?.Host;

        // 'Content-Type:' ∨ application/octet-stream
        var contentType = response.Content.Headers.ContentType;
        ContentType = contentType?.ToString() ?? MediaTypeNames.Application.Octet;

        // 'Content-Disposition: filename' ∨ GetHashCode
        string? fileName = response.Content.Headers.ContentDisposition?.FileName;
        if (string.IsNullOrWhiteSpace(fileName)) {
            fileName = null;
        }
        FileName = s_escapePattern.Replace(fileName ?? GetFileName(ContentType, response.RequestMessage?.RequestUri), "_");

        // 'Last-Modified:' ∨ NULL (might as well be File.GetLastWriteTime)
        LastModified = response.Content.Headers.LastModified;

        // 'ETag:' ∨ NULL
        ETag = response.Headers.ETag?.ToString();

        // the actual download size
        ContentLength = response.Content.Headers.ContentLength ?? 0;
    }

    /// <summary>
    /// Sets properties from <paramref name="response"/> properties.
    /// </summary>
    /// <param name="response">The HTTP response.</param>
    /// <param name="options">The fetch entry options to apply.</param>
    /// <param name="cacheControl">Optionally override default age.</param>
    public void Update(HttpResponseMessage response, HybridCacheEntryOptions? options, CacheControlHeaderValue? cacheControl) {

        // update response headers
        Update(response);

        TimeSpan ttl =
            cacheControl?.MaxAge ??
            response.Headers.CacheControl?.MaxAge ??
            options?.LocalCacheExpiration ??
            TimeSpan.Zero;

        // prevent items to be in cache forever
        if (ttl > options?.LocalCacheExpiration) {
            ttl = options.LocalCacheExpiration ?? TimeSpan.Zero;
        }

        // set expiration
        ExpiresAtTime = LastModified?.Add(ttl) ?? DateTimeOffset.UtcNow;

        // prevent items to expire imminently
        if (ExpiresAtTime < DateTime.UtcNow) {
            ExpiresAtTime = DateTime.UtcNow.Add(ttl);
        }

        // set absolute expiration
        AbsoluteExpiration = ExpiresAtTime.Add(options?.Expiration ?? TimeSpan.Zero);
    }

    static string GetFileName(string? contentType, Uri? input) {
        string absolutePath = input?.AbsolutePath ?? "/";
        int index = absolutePath.LastIndexOf('/');
        if (index < 0) {
            return string.Empty;
        }

        // file name should be the last string after '/' or "index".
        string fileName = absolutePath.Substring(index + 1);
        if (string.IsNullOrWhiteSpace(fileName)) {
            fileName = "index";
        }

        string extension = Path.GetExtension(fileName);
        if (string.IsNullOrWhiteSpace(extension) && contentType != null) {
            if (s_extensionMapping.TryGetValue(contentType, out string? value)) {

                // append the extension from the content type mapping
                fileName += value;
            } else {
                Match match = s_mediaTypePattern.Match(contentType);
                if (match.Success) {

                    // append the extension from the content type
                    fileName += "." + match.Groups[1].Value;
                }
            }
        }

        return fileName;
    }

    // pattern to escape filename characters
    static readonly Regex s_escapePattern = GetEscapePattern();

    [GeneratedRegex(@"\/:""\*\?<>\|")]
    private static partial Regex GetEscapePattern();

    // pattern to fall-back on in case s_mimeMapping missing extension:
    //   - by default only the first word counts
    //   - i.e. 'application/rss+xml; charset=utf-8' should catch the 'rss' word
    static readonly Regex s_mediaTypePattern = GetMediaTypePattern();

    [GeneratedRegex(@"^[^/]+/(\w+)")]
    private static partial Regex GetMediaTypePattern();

    // some known media type extensions
    static readonly Dictionary<string, string> s_extensionMapping = new() {
        [MediaTypeNames.Application.Json] = ".json",
        [MediaTypeNames.Application.Xml] = ".xml",
        [MediaTypeNames.Application.Rss] = ".rss",
        [MediaTypeNames.Image.Jpeg] = ".jpg",
        [MediaTypeNames.Image.Png] = ".png",
        [MediaTypeNames.Text.Plain] = ".txt"
    };
}
