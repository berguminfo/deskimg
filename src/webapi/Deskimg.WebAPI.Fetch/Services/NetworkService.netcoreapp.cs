// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// This class exposes functionality of NetworkInformation through a singleton.
/// </summary>
public class NetworkService {

    /// <summary>
    /// Gets a value indicating whether internet is available across all connections.
    /// </summary>
    /// <returns>True if internet can be reached.</returns>
    public bool IsInternetAvailable { get; private set; } = true;

    /// <summary>
    /// Gets a value indicating whether if the current internet connection is metered.
    /// </summary>
    /// <returns>True if internet is on metered connection.</returns>
    public bool IsInternetOnMeteredConnection { get; private set; }
}
