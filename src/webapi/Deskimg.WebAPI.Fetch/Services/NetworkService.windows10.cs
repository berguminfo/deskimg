// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Runtime.Versioning;
using Windows.Networking.Connectivity;

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// This class exposes functionality of NetworkInformation through a singleton.
/// </summary>
/// <remarks>
/// <see href="https://github.com/CommunityToolkit/WindowsCommunityToolkit">Original source.</see>
/// <para/>
/// <see href="https://opensource.org/licenses/MIT">The MIT license.</see>
/// </remarks>
[SupportedOSPlatform("windows10.0.10240.0")]
public class NetworkService {

    static readonly bool s_hasWindowsNetworking = OperatingSystem.IsWindowsVersionAtLeast(10, 0, 10240, 0);

    readonly Lock _updateLock = new();

    /// <summary>
    /// Gets a value indicating whether internet is available across all connections.
    /// </summary>
    /// <returns>True if internet can be reached.</returns>
    public bool IsInternetAvailable { get; private set; } = true;

    /// <summary>
    /// Gets a value indicating whether if the current internet connection is metered.
    /// </summary>
    public bool IsInternetOnMeteredConnection { get; private set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="NetworkService"/> class.
    /// </summary>
    public NetworkService() {
        if (s_hasWindowsNetworking) {
            UpdateConnectionInformation();
            NetworkInformation.NetworkStatusChanged += OnNetworkStatusChanged;
        }
    }


    /// <summary>
    /// Finalizes an instance of the <see cref="NetworkService"/> class.
    /// </summary>
    ~NetworkService() {
        if (s_hasWindowsNetworking) {
            NetworkInformation.NetworkStatusChanged -= OnNetworkStatusChanged;
        }
    }

    /// <summary>
    /// Checks the current connection information.
    /// </summary>
    public void UpdateConnectionInformation() {
        if (!s_hasWindowsNetworking) {
            return;
        }

        lock (_updateLock) {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile == null) {
                IsInternetAvailable = false;
                return;
            }

            var connectivityLevel = profile.GetNetworkConnectivityLevel();
            switch (connectivityLevel) {
                case NetworkConnectivityLevel.None:
                case NetworkConnectivityLevel.LocalAccess:
                    IsInternetAvailable = false;
                    break;
                default:
                    IsInternetAvailable = true;
                    break;
            }

            var connectionCost = profile.GetConnectionCost();
            IsInternetOnMeteredConnection = connectionCost != null && connectionCost.NetworkCostType != NetworkCostType.Unrestricted;
        }
    }

    /// <summary>
    /// Invokes <see cref="UpdateConnectionInformation"/> when the current network status changes.
    /// </summary>
    void OnNetworkStatusChanged(object sender) {
        UpdateConnectionInformation();
    }
}
