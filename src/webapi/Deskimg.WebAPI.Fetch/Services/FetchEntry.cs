// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json.Serialization;

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// Represents the fetch states.
/// </summary>
public enum FetchState {

    /// <summary>
    /// The item has unknown state.
    /// </summary>
    Initial,

    /// <summary>
    /// The item is about to be updated.
    /// </summary>
    Interactive,

    /// <summary>
    /// The item is ready to be served.
    /// </summary>
    Ready,

    /// <summary>
    /// The item has errors.
    /// </summary>
    Error
}

/// <summary>
/// Represents a fetched entity.
/// </summary>
public sealed partial class FetchEntry {

    /// <summary>
    /// Gets or sets the <see cref="FetchState"/>.
    /// </summary>
    public FetchState State { get; set; } = FetchState.Initial;

    /// <summary>
    /// Gets or sets the time of expiration.
    /// </summary>
    [JsonPropertyName("Expires")]
    public DateTimeOffset ExpiresAtTime { get; set; } = DateTimeOffset.MinValue;

    /// <summary>
    /// Gets or sets the time of absolute expiration.
    /// At this time the entry will be deleted.
    /// </summary>
    [JsonPropertyName("Stale")]
    public DateTimeOffset AbsoluteExpiration { get; set; } = DateTimeOffset.MinValue;

    /// <summary>
    /// Gets or sets the "Content-Type" header.
    /// </summary>
    public string? ContentType { get; set; }


    /// <summary>
    /// Gets or sets the origin host name.
    /// </summary>
    public string? Host { get; set; }

    /// <summary>
    /// Gets or sets the origin filename.
    /// </summary>
    public string? FileName { get; set; }

    /// <summary>
    /// Gets or sets the origin time of last modification.
    /// </summary>
    public DateTimeOffset? LastModified { get; set; }

    /// <summary>
    /// Gets or sets the origin ETag.
    /// </summary>
    public string? ETag { get; set; }

    /// <summary>
    /// Gets or sets the resource content length.
    /// </summary>
    public long? ContentLength { get; set; }
}
