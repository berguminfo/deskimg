// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_0

using System.Collections.Concurrent;
using System.Text.Json;

namespace Deskimg.WebAPI.Fetch.Services;

using Deskimg.WebAPI.Fetch;

using Deskimg.WebAPI.Fetch.Options;

/// <summary>
/// Represents a file system fetch service.
/// </summary>
public sealed partial class FileSystemService(
    ILogger<FileSystemService> logger,
    ILogger<HttpResponseMessage> consoleLogger,
    IOptions<AppOptions> appOptions,
    HttpClient http,
    NetworkService network) : AbstractFetchService, IDisposable {

    readonly AppOptions options = appOptions.Value;

    ConcurrentDictionary<string, FetchEntry>? _cache;
    Task? _commitTimer;
    CancellationTokenSource? _cancellationTokenSource;
    uint _commitCounter = uint.MinValue;
#if UPDATE_STATE_LOCK
    readonly object _updateStateLock = new();
#endif

    internal void CreateCache() {
        logger.NewCache(GetPhysicalPath(options.Index).FullName);
        _cache = new();
    }

    /// <inheritdoc/>
    public void Dispose() {
        StopCommitTimer();
    }

    /// <inheritdoc/>
    public override IEnumerable<KeyValuePair<string, FetchEntry>>? Index() => _cache;

    /// <summary>
    /// Loads the cache JSON file.
    /// </summary>
    /// <param name="cancellationToken">Optional. The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The <see cref="IAsyncResult"/>.</returns>
    public async Task LoadAsync(CancellationToken cancellationToken = default) {
        FileInfo physicalPath = GetPhysicalPath(options.Index);
        if (physicalPath.Exists) {
            try {
                using var stream = physicalPath.OpenRead();
                _cache = await JsonSerializer.DeserializeAsync<ConcurrentDictionary<string, FetchEntry>>(
                    stream, cancellationToken: cancellationToken);
            } catch (JsonException ex) {
                // prefer logging the error and continue
                logger.DeserializeJsonFailed(ex, physicalPath.FullName, ex.LineNumber, ex.BytePositionInLine);
            } catch (SystemException ex) {
                // prefer logging the error and continue
                logger.LoadCacheFailed(ex, physicalPath.FullName, ex.Data);
            }
        }

        if (_cache is null) {
            logger.NewCache(physicalPath.FullName);
            _cache = new();
        } else {
            logger.LoadCacheCompleted(physicalPath.FullName, _cache.Count);
        }
    }

    /// <summary>
    /// Starts the commit timer.
    /// </summary>
    public void StartCommitTimer() {
        _cancellationTokenSource = new();
        _commitTimer = Task.Run(async () => {
            uint counter = _commitCounter;
            while (!_cancellationTokenSource.Token.IsCancellationRequested) {
                try {
                    await Task.Delay(options.DelayCommit, _cancellationTokenSource.Token);

                    // commit queue has changed, save the new state
                    if (counter != _commitCounter) {
                        counter = _commitCounter;
                        CleanUp();
                        await CommitAsync(_cancellationTokenSource.Token);
                    }
                } catch (OperationCanceledException) { }
            }
        }, _cancellationTokenSource.Token);
    }

    /// <summary>
    /// Stops the commit timer.
    /// </summary>
    public void StopCommitTimer() {
        if (_cancellationTokenSource is not null) {
            _cancellationTokenSource.Cancel();
            try {
                _cancellationTokenSource.Dispose();
            } catch (InvalidOperationException) { }
        }
        if (_commitTimer is not null) {
            try {
                _commitTimer.Dispose();
            } catch (InvalidOperationException) { }
        }
    }

    async Task CommitAsync(CancellationToken token = default) {
        FileInfo physicalPath = GetPhysicalPath(options.Index);
        try {
            // this method should only be used inside the commit timer (except tests)
            // hence no file access violation can occur
            using var stream = physicalPath.Create();
            await JsonSerializer.SerializeAsync(stream, _cache, cancellationToken: token);
            logger.CommitCompleted(physicalPath.FullName, _cache?.Count ?? 0);

        } catch (NotSupportedException ex) {
            // prefer logging the error and continue
            logger.SerializeJsonFailed(ex, physicalPath.FullName);

        } catch (SystemException ex) {
            // prefer logging the error and continue
            logger.CommitFailed(ex, physicalPath.FullName, ex.Data);
        }
    }

    void CleanUp() {
        if (_cache is null) {
            // prefer silent notification
            return;
        }

        FileInfo physicalPath = GetPhysicalPath(options.Index);
        if (!physicalPath.Exists) {
            logger.CleanUpSkipped(physicalPath.FullName);
            return;
        }

        var removed =
            from item in _cache
            where item.Value.AbsoluteExpiration < DateTime.UtcNow
            select item;
        foreach (var item in removed) {
            string path = Path.Join(physicalPath.DirectoryName, item.Key);
            try {
                if (File.Exists(path)) {
                    File.Delete(path);
                    logger.ExpiredItemDeleted(path);
                }
                if (!_cache.Remove(item.Key, out var _)) {
                    logger.CleanUpFailed(item.Key);
                }
            } catch (SystemException ex) {
                logger.DeleteItemFailed(ex, path);
            }
        }
    }
}

#endif
