// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V2_0

namespace Deskimg.WebAPI.Fetch.Services;

/// <summary>
/// Represents a hybrid cache fetch service.
/// </summary>
public sealed class HybridCacheService(
    ILogger<HybridCacheService> logger,
    ILogger<HttpResponseMessage> consoleLogger,
    IOptions<FetchOptions> fetchOptions,
    HttpClient http,
    NetworkService network,
    HybridCache cache) : AbstractFetchService {

    /// <inheritdoc/>
    public override IEnumerable<KeyValuePair<string, FetchEntry>>? Index() {
        throw new NotImplementedException();
    }

    /// <inheritdoc/>
    public override Task<FetchEntry> HeadAsync(string base64url, CancellationToken cancellationToken = default) {
        throw new NotImplementedException();
    }

    /// <inheritdoc/>
    public override Task<(FetchEntry, FileInfo)> GetAsync(string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default) {
        throw new NotImplementedException();
    }

    /// <inheritdoc/>
    public override Task<(FetchEntry, FileInfo)> PropFindAsync(string base64url, IHeaderDictionary headers, CancellationToken cancellationToken = default) {
        throw new NotImplementedException();
    }
}

#endif
