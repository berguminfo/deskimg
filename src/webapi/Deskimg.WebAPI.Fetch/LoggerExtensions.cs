// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Collections;
using System.Net;

namespace Deskimg.WebAPI.Fetch;

static partial class LoggerExtensions {

    // global

    [LoggerMessage(
        EventId = 1,
        Level = LogLevel.Error,
        Message = "Error fetching the resource at '{Input}'. See exception details for more information."
    )]
    public static partial void Problem(this ILogger logger, Exception ex, string input);

    [LoggerMessage(
        EventId = 2,
        Level = LogLevel.Warning,
        Message = "Prevented to fetch the resource at '{Input}'. Internet connected on metered connection."
    )]
    public static partial void OnMeteredConnection(this ILogger logger, string input);

    [LoggerMessage(
        EventId = 3,
        Level = LogLevel.Error,
        Message = "Error fetching the resource at '{Input}', got status code: {Status}."
    )]
    public static partial void HeadFailed(this ILogger logger, Exception ex, string input, HttpStatusCode? status);

    [LoggerMessage(
        EventId = 4,
        Level = LogLevel.Error,
        Message = "Error fetching the resource at '{Input}', got status code: {Status}."
    )]
    public static partial void GetFailed(this ILogger logger, Exception ex, string input, HttpStatusCode? status);

    [LoggerMessage(
        EventId = 403,
        Level = LogLevel.Information,
        Message = "Not allowed to fetch the resource at '{Input}'."
    )]
    public static partial void FetchDenied(this ILogger logger, string input);

    // 100: FetchService.LoadAsync

    [LoggerMessage(
        EventId = 100,
        Level = LogLevel.Warning,
        Message = "Error deserialize cache store at '{Path}' ({Line}:{Position}). See exception details for more information."
    )]
    public static partial void DeserializeJsonFailed(this ILogger logger, Exception ex, string path, long? line, long? position);

    [LoggerMessage(
        EventId = 101,
        Level = LogLevel.Warning,
        Message = "Error loading cache store at '{Path}'. See exception details for more information. Data: {Data}."
    )]
    public static partial void LoadCacheFailed(this ILogger logger, Exception ex, string path, IDictionary data);

    [LoggerMessage(
        EventId = 102,
        Level = LogLevel.Warning,
        Message = "Starting with a new cache store at '{Path}'."
    )]
    public static partial void NewCache(this ILogger logger, string path);

    [LoggerMessage(
        EventId = 103,
        Level = LogLevel.Information,
        Message = "Successfully loaded the cache store at '{Path}' having {Count} items."
    )]
    public static partial void LoadCacheCompleted(this ILogger logger, string path, int count);

    // 110: FetchService.CommitAsync

    [LoggerMessage(
        EventId = 110,
        Level = LogLevel.Warning,
        Message = "Error serialize cache store at '{Path}'. See exception details for more information."
    )]
    public static partial void SerializeJsonFailed(this ILogger logger, Exception ex, string path);

    [LoggerMessage(
        EventId = 111,
        Level = LogLevel.Warning,
        Message = "Error save cache store at '{Path}'. See exception details for more information. Data: {Data}."
    )]
    public static partial void CommitFailed(this ILogger logger, Exception ex, string path, IDictionary data);

    [LoggerMessage(
        EventId = 112,
        Level = LogLevel.Information,
        Message = "Successfully saved the cache store at '{Path}' having {Count} items."
    )]
    public static partial void CommitCompleted(this ILogger logger, string path, int count);

    // 120: FetchService.CleanUp

    [LoggerMessage(
        EventId = 120,
        Level = LogLevel.Debug,
        Message = "No such cache store at '{Path}'. Continues."
    )]
    public static partial void CleanUpSkipped(this ILogger logger, string path);

    [LoggerMessage(
        EventId = 122,
        Level = LogLevel.Trace,
        Message = "Deleted the expired cache item '{Path}'."
    )]
    public static partial void ExpiredItemDeleted(this ILogger logger, string path);

    [LoggerMessage(
        EventId = 123,
        Level = LogLevel.Warning,
        Message = "Error remove the cache item '{Key}'. Trying again on next run."
    )]
    public static partial void CleanUpFailed(this ILogger logger, string key);


    [LoggerMessage(
        EventId = 124,
        Level = LogLevel.Warning,
        Message = "Error delete the expired cache item '{Path}'. See exception details for more information."
    )]
    public static partial void DeleteItemFailed(this ILogger logger, Exception ex, string path);

    // 130: FetchService.GetAsync

    [LoggerMessage(
        EventId = 130,
        Level = LogLevel.Information,
        Message = "Fetch from cache '{Uri}'. Expires: {Expires}."
    )]
    public static partial void FetchFromCache(this ILogger logger, Uri uri, DateTimeOffset expires);

    [LoggerMessage(
        EventId = 131,
        Level = LogLevel.Debug,
        Message = "Start fetching the resource at '{Uri}'."
    )]
    public static partial void FetchStarting(this ILogger logger, Uri uri);


    [LoggerMessage(
        EventId = 132,
        Level = LogLevel.Information,
        Message = "Unmodified fetch for the resource at '{Uri}'. Expires: {expires}."
    )]
    public static partial void UnmodifiedFetch(this ILogger logger, Uri uri, DateTimeOffset expires);

    [LoggerMessage(
        EventId = 133,
        Level = LogLevel.Information,
        Message = "Completed fetch for the resource at '{Uri}'. Expires: {Expires}. Age: {Age}."
    )]
    public static partial void CompletedFetch(this ILogger logger, Uri uri, DateTimeOffset expires, TimeSpan age);

    [LoggerMessage(
        EventId = 134,
        Level = LogLevel.Information,
        Message = "Interactive fetch for the resource at '{Uri}'. Expiry: {Expiry}."
    )]
    public static partial void InteractiveFetch(this ILogger logger, Uri uri, DateTimeOffset expiry);
}
