// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_1

using Microsoft.AspNetCore.Mvc;

namespace Deskimg.WebAPI.Fetch.Controllers.V1;

/// <summary>
/// Represents the '/api/1.1/fetch' endpoints.
/// </summary>
partial class FetchController {

    /// <inheritdoc/>
    [ApiVersion(1.1)]
    [HttpGet]
    public new IActionResult Index() => base.Index();
}

#endif
