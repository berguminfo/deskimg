// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Fetch.Controllers;

static class StatusCodesInternal {
    public const int Status425TooEarly = 425;
    public const int Status509MeteredConnection = 509;
}
