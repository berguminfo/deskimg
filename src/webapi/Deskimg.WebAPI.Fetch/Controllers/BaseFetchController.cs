// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Deskimg.WebAPI.Fetch.Services;

namespace Deskimg.WebAPI.Fetch.Controllers;

using Deskimg.Extensions;
using Deskimg.WebAPI.Fetch.Options;

/// <summary>
/// Represents the abstract fetch controller.
/// </summary>
public class BaseFetchController(
    ILogger logger,
    IOptions<AppOptions> appOptions,
    AbstractFetchService service) : Controller {

    readonly AppOptions options = appOptions.Value;

    /// <summary>
    /// Specifies the valid route regular expression.
    /// </summary>
    internal const string ValidRoute = """^[[\w-]]+$""";

    void SetResponseHeaders(FetchEntry entry) {
        Response.Headers["x-fetch-host"] = entry.Host ?? string.Empty;
        Response.Headers["x-fetch-name"] = entry.FileName ?? string.Empty;
        Response.Headers["x-fetch-expire"] = entry.AbsoluteExpiration.ToString("r");
        Response.Headers.Expires = entry.ExpiresAtTime.ToString("r");
        Response.Headers.ContentType = entry.ContentType;
        Response.Headers.ContentLength = entry.ContentLength;
        if (entry.LastModified is not null) {
            Response.Headers.LastModified = entry.LastModified.Value.ToString("r");
        }
        if (entry.ETag is not null) {
            Response.Headers.ETag = entry.ETag;
        }
    }

    /// <inheritdoc cref="AbstractFetchService.PropFindAsync"/>
    protected IActionResult Index() {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        var result = service.Index();
        return result is not null ? Ok(result) : NotFound();
    }

    /// <summary>
    /// Handles 'OPTIONS' requests.
    /// </summary>
    protected IActionResult Options(string base64url) {
        Response.Headers.Allow = "OPTIONS, HEAD, GET, PROPFIND";
        Response.Headers["Public"] = Response.Headers.Allow;
        Response.Headers["DAV"] = "1";
        return Ok();
    }

    /// <inheritdoc cref="AbstractFetchService.HeadAsync"/>
    protected async Task<IActionResult> Head(string base64url) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            var result = await service.HeadAsync(base64url, HttpContext.RequestAborted);
            if (result is null) {
                return NotFound();
            }

            SetResponseHeaders(result);
            return Ok();

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (RequestDeniedException ex) {
            logger.FetchDenied(ex.Message);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (MeteredConnectionException ex) {
            logger.OnMeteredConnection(ex.Message);
            return StatusCode(StatusCodesInternal.Status509MeteredConnection);
        } catch (HttpRequestException ex) {
            logger.HeadFailed(ex, DeflateBase64Url.Decode(base64url), ex.StatusCode);
            return ex.StatusCode == HttpStatusCode.NotFound ?
                NotFound() : Problem(ex.Message, statusCode: (int?)ex.StatusCode);
        } catch (Exception ex) {
            logger.Problem(ex, DeflateBase64Url.Decode(base64url));
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Handles 'GET {base64url}' requests.
    /// </summary>
    /// <param name="base64url">The fetch URI.</param>
    /// <returns>The <see cref="IActionResult"/>.</returns>
    protected async Task<IActionResult> Get(string base64url) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            (FetchEntry entry, FileInfo physicalFile) = await service.GetAsync(base64url, Request.Headers, HttpContext.RequestAborted);
            if (entry.State != FetchState.Ready) {
                return NotFound();
            }

            SetResponseHeaders(entry);
            TimeSpan age = DateTime.UtcNow - physicalFile.LastWriteTimeUtc;
            Response.Headers.Age = Math.Abs(age.TotalSeconds).ToString("F0", CultureInfo.InvariantCulture);
            if (!physicalFile.Exists) {
                return StatusCode(StatusCodesInternal.Status425TooEarly);
            }

            // NOTE: ActionResult sets Content-Length
            return PhysicalFile(physicalFile.FullName, entry.ContentType ?? MediaTypeNames.Application.Octet);

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (RequestDeniedException ex) {
            logger.FetchDenied(ex.Message);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (MeteredConnectionException ex) {
            logger.OnMeteredConnection(ex.Message);
            return StatusCode(StatusCodesInternal.Status509MeteredConnection);
        } catch (HttpRequestException ex) {
            logger.GetFailed(ex, DeflateBase64Url.Decode(base64url), ex.StatusCode);
            return ex.StatusCode == HttpStatusCode.NotFound ?
                NotFound() : Problem(ex.Message, statusCode: (int?)ex.StatusCode);
        } catch (Exception ex) {
            logger.Problem(ex, DeflateBase64Url.Decode(base64url));
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Handles 'PROPFIND {base64url}' requests.
    /// </summary>
    /// <param name="base64url">The fetch URI.</param>
    /// <returns>The <see cref="IActionResult"/>.</returns>
    protected async Task<IActionResult> PropFind(string base64url) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        try {
            (FetchEntry entry, FileInfo physicalFile) = await service.PropFindAsync(base64url, Request.Headers, HttpContext.RequestAborted);
            if (physicalFile is null) {
                return NotFound();
            }

            SetResponseHeaders(entry);
            TimeSpan age = DateTime.UtcNow - physicalFile.LastWriteTimeUtc;
            Response.Headers.Age = Math.Abs(age.TotalSeconds).ToString("F0", CultureInfo.InvariantCulture);
            if (!physicalFile.Exists) {
                return StatusCode(StatusCodesInternal.Status425TooEarly);
            }

            // NOTE: ActionResult sets Content-Length
            return PhysicalFile(physicalFile.FullName, entry.ContentType ?? MediaTypeNames.Application.Octet);

        } catch (OperationCanceledException) {
            return StatusCode(StatusCodes.Status408RequestTimeout);
        } catch (RequestDeniedException ex) {
            logger.FetchDenied(ex.Message);
            return Problem(ex.Message, statusCode: StatusCodes.Status403Forbidden);
        } catch (MeteredConnectionException ex) {
            logger.OnMeteredConnection(ex.Message);
            return StatusCode(StatusCodesInternal.Status509MeteredConnection);
        } catch (HttpRequestException ex) {
            logger.GetFailed(ex, DeflateBase64Url.Decode(base64url), ex.StatusCode);
            return ex.StatusCode == HttpStatusCode.NotFound ?
                NotFound() : Problem(ex.Message, statusCode: (int?)ex.StatusCode);
        } catch (Exception ex) {
            logger.Problem(ex, DeflateBase64Url.Decode(base64url));
            return Problem(ex.Message);
        }
    }
}
