// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_0

using Microsoft.AspNetCore.Mvc;
using Deskimg.WebAPI.Fetch.Services;

namespace Deskimg.WebAPI.Fetch.Controllers.V1;

using Deskimg.WebAPI.Fetch.Options;

/// <summary>
/// Represents the '/api/1.0/fetch' endpoints.
/// </summary>
[ApiVersion(1.0)]
[Route("api/{version:apiVersion}/[controller]")]
[ApiController]
public sealed partial class FetchController(
    ILogger<FetchController> logger,
    IOptions<AppOptions> appOptions,
    FileSystemService service) : BaseFetchController(logger, appOptions, service) {

    /// <inheritdoc/>
    [HttpOptions($"{{base64url:regex({ValidRoute})}}")]
    public new IActionResult Options(string base64url) => base.Options(base64url);

    /// <inheritdoc/>
    [HttpHead($"{{base64url:regex({ValidRoute})}}")]
    public new Task<IActionResult> Head(string base64url) => base.Head(base64url);

    /// <inheritdoc/>
    [HttpGet($"{{base64url:regex({ValidRoute})}}")]
    public new Task<IActionResult> Get(string base64url) => base.Get(base64url);

    /// <inheritdoc/>
    [HttpPropFind($"{{base64url:regex({ValidRoute})}}")]
    public new Task<IActionResult> PropFind(string base64url) => base.PropFind(base64url);
}

#endif
