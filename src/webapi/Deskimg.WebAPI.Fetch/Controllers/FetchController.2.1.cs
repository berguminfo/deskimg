// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V2_1

using Microsoft.AspNetCore.Mvc;

namespace Deskimg.WebAPI.Fetch.Controllers.V2;

/// <summary>
/// Represents the '/api/2.1/fetch' endpoints.
/// </summary>
partial class FetchController {

    /// <inheritdoc/>
    [ApiVersion(2.1)]
    [HttpGet]
    public new IActionResult Index() => base.Index();
}

#endif
