// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Sets.Options;

/// <summary>
/// Represents the root appsettings options.
/// </summary>
public sealed class AppOptions {

    /// <inheritdoc cref="Deskimg.Extensions.FetchToken.Create(int)"/>
    public int ApiKey { get; set; } = 29;
}
