// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.WebAPI.Sets;

static partial class LoggerExtensions {

    [LoggerMessage(
        EventId = 1,
        Level = LogLevel.Error,
        Message = "Error fetching the resource at '{Input}'. See exception details for more information."
    )]
    public static partial void Problem(this ILogger logger, Exception ex, string input);

    [LoggerMessage(
        EventId = 2,
        Level = LogLevel.Trace,
        Message = "GET: '{Input}'."
    )]
    public static partial void TraceGet(this ILogger logger, string input);

    [LoggerMessage(
        EventId = 3,
        Level = LogLevel.Information,
        Message = "PUT: '{Input}'."
    )]
    public static partial void TracePut(this ILogger logger, string input);

    [LoggerMessage(
        EventId = 4,
        Level = LogLevel.Information,
        Message = "DELETE: '{Input}'."
    )]
    public static partial void TraceDelete(this ILogger logger, string input);

    [LoggerMessage(
        EventId = 403,
        Level = LogLevel.Debug,
        Message = "Forbidden: Input = '{Input}'. See exception details for more information."
    )]
    public static partial void Forbidden(this ILogger logger, Exception ex, string input);

    [LoggerMessage(
        EventId = 404,
        Level = LogLevel.Debug,
        Message = "NotFound: Input = '{Input}'."
    )]
    public static partial void NotFound(this ILogger logger, string input);

}
