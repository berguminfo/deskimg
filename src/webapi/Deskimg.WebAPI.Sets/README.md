# Deskimg.WebAPI.Sets

WebAPI service for the default configuration sets.

## Syntax

```sh
Deskimg.WebAPI.Sets
  [--ApiKey <int>]
  [--Logging:LogLevel:Deskimg <LogLevel>]
```

👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)

## Parameters

### --ApiKey

Optionally turn fetch token authentication on or off.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Int32                                                     |
| Default value:   | 29                                                        |
| Required:        | False                                                     |

### --Logging:LogLevel:Deskimg

Specifies the logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Information                                               |
| Required:        | False                                                     |
