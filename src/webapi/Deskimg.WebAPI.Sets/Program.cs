// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.StaticFiles;
using Deskimg.WebAPI.Sets.Options;

var builder = WebApplication.CreateBuilder(args);

// add services to the container

var mimeMap = builder.Configuration.GetSection("mimeMap").Get<IDictionary<string, string>>();
FileExtensionContentTypeProvider contentTypeProvider = mimeMap is null ? new() : new(mimeMap);

const string CorsPolicyName = "allow-any";
builder.Services.AddCors(options => {
    options.AddPolicy(CorsPolicyName, policy => {
        policy.AllowAnyOrigin();
        policy.AllowAnyMethod();
        policy.AllowAnyHeader();
    });
});
builder.Services.AddControllers();
builder.Services.AddProblemDetails();
builder.Services.AddApiVersioning(options => {
    options.ReportApiVersions = true;
}).AddMvc();
builder.Services.Configure<AppOptions>(builder.Configuration);

// learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();
app.Use((context, next) => {
    context.Response.Headers.AltSvc = $"h3=\":{context.Connection.LocalPort}\"";
    return next(context);
});
app.Use(async (context, nextMiddleware) => {
    context.Response.Headers["x-system-id"] = $"{nameof(Deskimg.WebAPI.Sets)}/{ThisAssembly.Git.InformationalVersion}";
    await nextMiddleware();
});

// configure the HTTP request pipeline

if (!app.Environment.IsProduction()) {
    const string name = "Deskimg.WebAPI.Sets";
    const string url = "../@typespec/openapi3/";
    app.UseSwaggerUI(options => {
        options.DocumentTitle = name;
        options.ConfigObject.Urls = [
#if V1_0
            new() { Url = $"{url}openapi.1.0.yaml", Name = $"{name} (1.0)" },
#endif
        ];
    });
}

app.UseHttpsRedirection();
app.UseCors(CorsPolicyName);
app.UseDefaultFiles();
app.UseStaticFiles(new StaticFileOptions {
    ContentTypeProvider = contentTypeProvider
});
app.MapControllers();

await app.RunAsync();
