// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if V1_0

using System.Globalization;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace Deskimg.WebAPI.Sets.Controllers.V1;

using Deskimg.Extensions;
using Deskimg.WebAPI.Sets.Options;

/// <summary>
/// Represents the '/api/1.0/sets' endpoints.
/// </summary>
[ApiVersion(1.0)]
[Route("api/{version:apiVersion}/[controller]")]
[ApiController]
public class SetsController(
    ILogger<SetsController> logger,
    IOptions<AppOptions> appOptions) : Controller {

    readonly AppOptions options = appOptions.Value;

    /// <summary>
    /// Specifies the valid route regular expression.
    /// </summary>
    protected const string ValidRoute = """^\w+$""";

    static readonly Assembly s_resourceAssembly = Assembly.GetExecutingAssembly();

    /// <summary>
    /// Handles 'GET /id' requests.
    /// </summary>
    /// <param name="id">The set identifier.</param>
    /// <returns>The <see cref="IActionResult"/> response.</returns>
    [HttpGet($"{{id:regex({ValidRoute})}}")]
    public IActionResult Get(string id) {
        if (options.ApiKey > 0 && !Request.HasValidFetchToken()) {
            return Unauthorized();
        }

        string resourceName = $"Deskimg.WebAPI.Sets.sets.{id}.json";

        try {
            FileInfo assemblyInfo = new(s_resourceAssembly.Location);
            Stream? resourceStream = s_resourceAssembly.GetManifestResourceStream(resourceName);
            if (resourceStream is null) {
                logger.NotFound(resourceName);
                return NotFound();
            }

            logger.TraceGet(resourceName);
            DateTimeOffset lastModified = new(assemblyInfo.LastWriteTimeUtc, TimeSpan.Zero);
            EntityTagHeaderValue entityTag = new($@"""{lastModified.Ticks.ToString("x", CultureInfo.InvariantCulture)}""");
            if (Request.Headers.IfNoneMatch == entityTag) {
                return StatusCode(StatusCodes.Status304NotModified);
            } else {
                return File(resourceStream, MediaTypeNames.Application.Json, lastModified, entityTag);
            }

        } catch (FileNotFoundException) {
            logger.NotFound(resourceName);
            return NotFound();
        } catch (Exception ex) {
            logger.Problem(ex, resourceName);
            return Problem(ex.Message);
        }
    }
}

#endif
