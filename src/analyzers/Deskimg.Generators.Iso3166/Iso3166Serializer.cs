// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Xml.Linq;

namespace Deskimg.Generators.Iso3166;

struct Iso3166Entry {

    public string Name { get; set; }

    public string TwoLetterISORegionName { get; set; }

    public string ThreeLetterISORegionName { get; set; }

    public int NumericCode { get; set; }
}

static class Iso3166Serializer {

    public static IEnumerable<Iso3166Entry> Deserialize(Stream stream) {
        var document = XDocument.Load(stream);
        return
            from element in document.Descendants("iso_3166_entry")
            let name = element.Attribute("name")
            let twoLetterISORegionName = element.Attribute("alpha_2_code")
            let threeLetterISORegionName = element.Attribute("alpha_3_code")
            let numericCode = element.Attribute("numeric_code")
            where name is not null &&
                  twoLetterISORegionName is not null &&
                  threeLetterISORegionName is not null &&
                  numericCode is not null
            select new Iso3166Entry {
                Name = name.Value,
                TwoLetterISORegionName = twoLetterISORegionName.Value,
                ThreeLetterISORegionName = threeLetterISORegionName.Value,
                NumericCode = int.Parse(numericCode.Value, CultureInfo.InvariantCulture)
            };
    }
}
