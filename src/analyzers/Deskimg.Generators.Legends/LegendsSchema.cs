// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Text.Json;
using System.Text.Json.Serialization;

namespace Deskimg.Generators.Legends;

[JsonConverter(typeof(JsonStringEnumConverter<LegendVariant>))]
internal enum LegendVariant {
    Day,
    Night,
    PolarTwilight
}

internal class LegendItem {

    public static readonly JsonNamingPolicy NamingPolicy = JsonNamingPolicy.SnakeCaseLower;

    [JsonPropertyName("old_id")]
    [JsonNumberHandling(JsonNumberHandling.AllowReadingFromString)]
    public int NumericId { get; set; }

    [JsonPropertyName("desc_nb")]
    public string? NB { get; set; }

    [JsonPropertyName("desc_nn")]
    public string? NN { get; set; }

    [JsonPropertyName("desc_en")]
    public string? EN { get; set; }

    [JsonPropertyName("variants")]
    public LegendVariant[]? Variants { get; set; }
}
