### New Rules

Rule ID | Category | Severity | Notes
--------|----------|----------|--------------------
GEN1001 |  Design  |  Warning | GEN1001_MissingMarkerAttribute, [Documentation](GEN1001_MissingMarkerAttribute)