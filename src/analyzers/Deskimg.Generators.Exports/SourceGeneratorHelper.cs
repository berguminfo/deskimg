// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Generators.Exports;

static class SourceGeneratorHelper {

    public const string ExportAttributeName = "Deskimg.Composition.ExportAttribute";
    public const string AssemblyExportsGeneratedAttributeName = "Deskimg.Composition.AssemblyExportsGeneratedAttribute";

    public static string EmitExports(IList<ExportsGenerator.ExportEntityToGenerate> collection, CancellationToken cancellationToken) {
        StringWriter sw = new();
        using IndentedTextWriter writer = new(sw);
        writer.Indent += 2;

        foreach (var item in collection) {
            cancellationToken.ThrowIfCancellationRequested();
            writer.WriteLine($@"[(typeof({item.ContractType}), ""{item.ContractName}"")] = typeof({item.ImplementorType}),");
        }

        return sw.ToString();
    }
}
