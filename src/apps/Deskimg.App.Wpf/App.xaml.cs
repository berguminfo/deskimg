// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Diagnostics;
using System.Windows;
using Microsoft.Extensions.Configuration;
using Microsoft.Win32;

namespace Deskimg.App.Wpf;

/// <summary>
/// Interaction logic for App.xaml.
/// </summary>
partial class App : Application {

    internal IConfiguration Configuration { get; init; }

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public App() {
        CheckWebView2Runtime();
        Configuration = CreateConfiguration();
    }

    /// <inheritdoc/>
    protected override void OnStartup(StartupEventArgs e) {
        if (Configuration["Control"] is string message) {
            PipeHelper.Send(message);
            Environment.Exit(0);
        }
    }

    static void CheckWebView2Runtime() {
        if (OperatingSystem.IsWindows()) {
            string key;
            RegistryKey? registryKey;
            key = Environment.Is64BitOperatingSystem ?
                @"SOFTWARE\WOW6432Node\Microsoft\EdgeUpdate\Clients\{F3017226-FE2A-4295-8BDF-00C3A9A7E4C5}" :
                @"SOFTWARE\Microsoft\EdgeUpdate\Clients\{F3017226-FE2A-4295-8BDF-00C3A9A7E4C5}";
            registryKey = Registry.LocalMachine.OpenSubKey(key);
            if (registryKey is null) {
                key = @"Software\Microsoft\EdgeUpdate\Clients\{F3017226-FE2A-4295-8BDF-00C3A9A7E4C5}";
                registryKey = Registry.CurrentUser.OpenSubKey(key);
            }

            if (registryKey is null) {
                ProcessStartInfo startInfo = new() {
                    FileName = "MicrosoftEdgeWebview2Setup.exe",
                    Arguments = "/install",
                    WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory,
                    UseShellExecute = false
                };
                try {
                    Process? process = Process.Start(startInfo);
                    process?.WaitForExit();
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message, nameof(Deskimg), MessageBoxButton.OK, MessageBoxImage.Error);
                }
            } else {
                registryKey.Close();
            }
        } else {
            MessageBox.Show($"Operation system is unsupported.", nameof(Deskimg), MessageBoxButton.OK, MessageBoxImage.Error);
            Application.Current.Shutdown();
        }
    }

    static IConfiguration CreateConfiguration() {
        ConfigurationBuilder builder = new();
        builder.SetBasePath(AppDomain.CurrentDomain.BaseDirectory);
        builder.AddJsonFile("appsettings.json", optional: true);
        string environmentName =
            Environment.GetEnvironmentVariable("DESKIMG_ENVIRONMENT") ??
            Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ??
#if DEBUG
            "Development";
#else
            "Production";
#endif
        builder.AddJsonFile($"appsettings.{environmentName}.json", optional: true);
        builder.AddEnvironmentVariables();
        builder.AddCommandLine(Environment.GetCommandLineArgs());

        return builder.Build();
    }
}

