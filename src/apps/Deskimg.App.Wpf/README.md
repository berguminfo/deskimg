# Deskimg.App.Wpf

Application to host and view the desktop image using `WPF` and `WebView2`.

💡 To override the default environment use the `DESKIMG_ENVIRONMENT` environment variable.

## Syntax

```sh
Deskimg.App.Wpf
  [--Mode <DisplayMode>]
  [--Control <ControlMessage>]
  [--PreserveUserData <bool>]
  [--UserDataFolder <string>]
  [--Language <string>]
  [--PortRange:MinInclusive <ushort>]
  [--PortRange:MaxInclusive <ushort>]
  [--Permissions:UnknownPermission <PermissionState>]
  [--Permissions:Geolocation <PermissionState>]
  [--Permissions:Notifications <PermissionState>]
  [--Dbug:TabVisibility <Visibility>]
  [--Dbug:ToggleAnyTab <bool>]
  [--Dbug:AreDevToolsEnabled <bool>]
```

👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)

## Parameters

### --Mode

Start the application in the given display mode. Use `--Control <string>` to send messages to the this instance.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | DisplayMode                                               |
| Accepted values: | Window, WindowMaximized, Kiosk, Wallpaper                 |
| Default value:   | Window                                                    |
| Required:        | False                                                     |

### --Control

Sends a control message to previous started instances.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | ControlMessage                                            |
| Accepted values: | detach                                                    |
| Required:        | False                                                     |

### --PreserveUserData

Preserves the browser UserData folder between sessions.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | False                                                     |
| Required:        | False                                                     |

### --UserDataFolder

Specifies the path to the folder storing the UserData.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | False                                                     |

### --Language

Optionally specify the browser language.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | False                                                     |

### --PortRange:MinInclusive

Specifies the minimum TCP port number for the hosted services.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | UInt16                                                    |
| Default value:   | 15000                                                     |
| Required:        | False                                                     |

### --PortRange:MaxInclusive

Spcifies the maximum TCP port number for the hosted services.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | UInt16                                                    |
| Default value:   | 15099                                                     |
| Required:        | False                                                     |

### --Permissions:UnknownPermission

Specifies the permission state for unknown permissions.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | PermissionState                                           |
| Accepted values: | Default, Allow, Deny                                      |
| Default value:   | Default                                                   |
| Required:        | False                                                     |

### --Permissions:Geolocation

Specifies the permission state for the gelocation service.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | PermissionState                                           |
| Accepted values: | Default, Allow, Deny                                      |
| Default value:   | Allow                                                     |
| Required:        | False                                                     |

### --Permissions:Notifications

Specifies the permission state the the notifications service.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | PermissionState                                           |
| Accepted values: | Default, Allow, Deny                                      |
| Default value:   | Allow                                                     |
| Required:        | False                                                     |

### --Dbug:TabVisibility

This debugging option may be used to reveal the tab panel.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Visibility                                                |
| Accepted values: | Visible, Hidden, Collapsed                                |
| Default value:   | Collapsed                                                 |
| Required:        | False                                                     |

### --Dbug:ToggleAnyTab

This debugging option may be used to toggle into the logging tab.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | False                                                     |
| Required:        | False                                                     |

### --Dbug:AreDevToolsEnabled

This debugging options may be used to enable the `F12` development tools.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | False                                                     |
| Required:        | False                                                     |
