// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Windows;

namespace Deskimg.App.Wpf.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
internal sealed class DebugOptions {

    /// <summary>
    /// Gets or sets the tabs visibility.
    /// </summary>
    public Visibility TabVisibility { get; set; } = Visibility.Collapsed;

    /// <summary>
    /// Gets or sets the toggle any tab options.
    /// </summary>
    public bool ToggleAnyTab { get; set; }

    /// <summary>
    /// Gets or sets DevTools on or off.
    /// </summary>
    public bool AreDevToolsEnabled { get; set; }
}
