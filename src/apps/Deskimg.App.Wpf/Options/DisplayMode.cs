// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.App.Wpf.Options;

/// <summary>
/// Represents the application display modes.
/// </summary>
internal enum DisplayMode {
    Window,
    WindowMaximized,
    Kiosk,
    Wallpaper
}
