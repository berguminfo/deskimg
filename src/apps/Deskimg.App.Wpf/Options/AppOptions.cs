// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.Web.WebView2.Core;

namespace Deskimg.App.Wpf.Options;

/// <summary>
/// Represents the root application settings.
/// </summary>
internal sealed class AppOptions {

    /// <summary>
    /// Gets or sets the application display mode.
    /// </summary>
    public DisplayMode Mode { get; set; }

    /// <summary>
    /// Gets or sets the user data <see cref="MessageFormatter"/> pattern.
    /// </summary>
    public string UserDataFolder { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the preservation of user data.
    /// </summary>
    public bool PreserveUserData { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="DebugOptions"/> options.
    /// </summary>
    public DebugOptions Dbug { get; set; } = new();

    /// <summary>
    /// Gets or sets the browser language, or current culture if not specified.
    /// </summary>
    public string? Language { get; set; }

    /// <summary>
    /// Gets or sets the browser permissions.
    /// </summary>
    public IReadOnlyDictionary<CoreWebView2PermissionKind, CoreWebView2PermissionState>? Permissions { get; set; }
}
