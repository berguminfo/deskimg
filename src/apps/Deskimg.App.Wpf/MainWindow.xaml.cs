// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if DEBUG
//#define DEBUG_ENDPOINTS
#endif

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using Microsoft.Extensions.Configuration;
using Microsoft.Web.WebView2.Core;
using Microsoft.Web.WebView2.Wpf;

namespace Deskimg.App.Wpf;

using Deskimg.App.Wpf.Options;

/// <summary>
/// Interaction logic for MainWindow.xaml.
/// </summary>
partial class MainWindow : CustomWindow_WPF.CustomWindow, IDisposable {

    const string ConsoleTabName = "consoleTab";
    const string WebViewTabName = "wvTab";

    static readonly string s_roamingPath = Path.Join(
    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
    nameof(Deskimg));
    static readonly string s_localPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        nameof(Deskimg));

    readonly RichTextBox _consoleTextBox = new() {
        IsReadOnly = true,
        VerticalScrollBarVisibility = ScrollBarVisibility.Visible
    };

#if WINDOWS
    readonly ProcessJobTracker _jobTracker = new();
    readonly ProgmanWorker _progmanWorker = new();
#endif

    AppOptions _options = new();
    ObservableCollection<TabItem> _webView2Tabs = [];
    PipeHelper? _pipeHelper;

    /// <summary>
    /// Gets or sets the web view tabs.
    /// </summary>
    public ObservableCollection<TabItem> WebView2Tabs => _webView2Tabs;

    /// <summary>
    /// Initializes a new instance of this class.
    /// </summary>
    public MainWindow() {
        InitializeComponent();
        RestoreWindowPosition();
    }

    #region IDisposable

    readonly Lock disposeLock = new();
    bool disposed;

    /// <inheritdoc/>
    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <see cref="Dispose()"/>
    protected virtual void Dispose(bool disposing) {
        if (disposing) {
            lock (disposeLock) {
                if (!disposed) {
#if WINDOWS
                    _jobTracker.Dispose();
#endif
                }

                disposed = true;
            }
        }
    }

    #endregion

    /// <inheritdoc/>
    protected override void OnInitialized(EventArgs e) {
        base.OnInitialized(e);

        // use F11 as kiosk mode gesture
        KioskExitGesture = new CustomWindow_WPF.Utils.KioskExitGesture {
            Key = Key.F11
        };

        // initialize configuration and options
        IConfiguration configuration = ((App)Application.Current).Configuration;
        _options = configuration.Get<AppOptions>() ?? new();
        EndpointFormatter endpointFormatter = new(configuration.GetSection("PortRange").Get<PortRange>());

        // override saved maximized display mode
        if (_options.Mode == DisplayMode.Window && Settings.Default.Maximized) {
            _options.Mode = DisplayMode.WindowMaximized;
        }

        // set the display mode from _options.Mode
        // NOTE: wallpaper mode is postponed until ContentRendered in order for the WindowChrome class has invoked DwmExtendFrameIntoClientArea (WindowChromeWorker:864)
        if (_options.Mode != DisplayMode.Wallpaper) {
            SetDisplayMode();
        }

        // start hosting services and assign endpoints
        var services = configuration.GetSection("Services").Get<IReadOnlyList<ServiceDescriptor>>();
        if (services is not null) {
            StartServices(services, endpointFormatter);
        }

        // add wv2 tabs
#if DEBUG_ENDPOINTS
        AddTab(new Uri("https://localhost:5000/?nav=✓&🌐=corp&🌐=308&🌐=@"), "5000");
        AddTab(new Uri("https://localhost:5004"), "5004");
#else
        foreach (var item in configuration.GetSection("Urls").GetChildren()) {
            string requestUri = endpointFormatter.Format(item.Get<string>());
            if (Uri.TryCreate(requestUri, UriKind.Absolute, out var uri)) {
                AddTab(uri, uri.Port.ToString(CultureInfo.InvariantCulture));
            }
        }
#endif

        // add console tab
        _consoleTextBox.Foreground = TitleBarForeground;
        _consoleTextBox.Background = TitleBarBackground;
        _webView2Tabs.Add(new TabItem {
            Name = ConsoleTabName,
            Visibility = _options.Dbug.TabVisibility,
            Header = new HeaderedContentControl {
                Content = "Console"
            },
            Content = _consoleTextBox
        });
    }

    /// <inheritdoc/>
    protected override void OnContentRendered(EventArgs e) {
        if (_options.Mode == DisplayMode.Wallpaper) {
            SetDisplayMode();
        }
    }

    /// <inheritdoc/>
    protected override void OnClosing(CancelEventArgs e) {
        if (_options.Mode == DisplayMode.Kiosk) {
            e.Cancel = true;
        } else {
            SaveSettings();

            foreach (var _ in Enumerable.Range(0, _webView2Tabs.Count)) {
                RemoveTab(0);
            }

            base.OnClosing(e);
        }
    }

    /// <inheritdoc/>
    protected override void OnKeyUp(KeyEventArgs e) {
        if (e.Key == Key.F11) {
            e.Handled = true;

            DisplayMode mode = e.KeyboardDevice.Modifiers switch {
                ModifierKeys.Control => _options.Mode == DisplayMode.Wallpaper ?
                    DisplayMode.Window : DisplayMode.Wallpaper,
                _ => _options.Mode == DisplayMode.Kiosk ?
                    DisplayMode.Window : DisplayMode.Kiosk
            };

            SetDisplayMode(mode);
        }

        base.OnKeyUp(e);
    }

    void SaveSettings() {
        // only save state if not in Kiosk, Wallpaper or Minimized mode
        bool saveSettings =
            WindowState != WindowState.Minimized && (
                _options.Mode == DisplayMode.Window ||
                _options.Mode == DisplayMode.WindowMaximized
            );
        if (saveSettings) {
            Settings.Default.Maximized = WindowState == WindowState.Maximized;
            if (WindowState == WindowState.Normal) {
                Settings.Default.Left = Left;
                Settings.Default.Top = Top;
                Settings.Default.Width = Width;
                Settings.Default.Height = Height;
            }

            Settings.Default.Save();
        }
    }

    void RestoreWindowPosition() {

        // shrink the window down to fit in the current desktop
        var width = Settings.Default.Width;
        if (width > SystemParameters.VirtualScreenWidth) {
            width = SystemParameters.VirtualScreenWidth;
        }
        Width = width;
        var height = Settings.Default.Height;
        if (height > SystemParameters.VirtualScreenHeight) {
            height = SystemParameters.VirtualScreenHeight;
        }
        Height = height;

        // moves the window onto the desktop if it is out of view
        var left = Settings.Default.Left;
        if (left + width > SystemParameters.VirtualScreenWidth) {
            left = SystemParameters.VirtualScreenWidth - width;
        }
        if (left < SystemParameters.VirtualScreenLeft) {
            left = SystemParameters.VirtualScreenLeft;
        }
        Left = left;
        var top = Settings.Default.Top;
        if (top + height > SystemParameters.VirtualScreenHeight) {
            top = SystemParameters.VirtualScreenHeight - height;
        }
        if (top < SystemParameters.VirtualScreenTop) {
            top = SystemParameters.VirtualScreenTop;
        }
        Top = top;
    }


    void SetDisplayMode(DisplayMode? mode = null) {
        if (mode is DisplayMode modeValue) {
            // if nothing to do return
            if (modeValue == _options.Mode) {
                return;
            }

            // switch mode
            _options.Mode = modeValue;
        }

        switch (_options.Mode) {

            case DisplayMode.Window:
                KioskMode = false;
                ShowInTaskbar = true;
                WindowState = WindowState.Normal;
                BorderThickness = new(1);
                break;

            case DisplayMode.WindowMaximized:
                KioskMode = false;
                ShowInTaskbar = true;
                WindowState = WindowState.Maximized;
                BorderThickness = new(0);
                break;

            case DisplayMode.Kiosk:
                KioskMode = true;
                ShowInTaskbar = true;
                BorderThickness = new(0);
                break;

            case DisplayMode.Wallpaper:
                KioskMode = true;
                WindowState = WindowState.Normal; // revert maximized state
                ShowInTaskbar = false;
                BorderThickness = new(0);
                break;
        }

#if WINDOWS
        // attach to desktop worker, now any input will be discontinued
        // use the name pipe to send control messages
        if (_options.Mode == DisplayMode.Wallpaper) {
            try {
                WindowInteropHelper helper = new(this);
                _progmanWorker.Attach(helper.Handle);
            } catch (Win32Exception) {
            }
        }
#endif

        // open the name pipe to send control messages
        if ((_options.Mode is DisplayMode.Kiosk or DisplayMode.Wallpaper) && _pipeHelper is null) {
            _pipeHelper = new();
            _pipeHelper.OnDetach += PipeHelper_OnDetach;

            _pipeHelper.ListenAsync().ConfigureAwait(false);
        }
    }

    void PipeHelper_OnDetach(object? sender, EventArgs e) {
        // NOTE: this event comes from another thread, dispatch back to my thread
        Dispatcher.Invoke(() => {
            if (_options.Mode is DisplayMode.Kiosk or DisplayMode.Wallpaper) {

                // if in wallpaper mode, detach from the desktop worker to restore the background
                if (_options.Mode == DisplayMode.Wallpaper) {
#if WINDOWS
                    try {
                        WindowInteropHelper helper = new(this);
                        _progmanWorker.Detach(helper.Handle);
                    } catch (Win32Exception) {
                    }
#endif
                }

                // go back to window mode
                SetDisplayMode(DisplayMode.Window);
            }
        });
    }

    void AddTab(Uri uri, string headerText) {

        // create new instance, setting a custom UserDataFolder
        string userDataFolder = string.IsNullOrWhiteSpace(_options.UserDataFolder) ?
            // just create a temporary folder, PreserveUserData should be false
            Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("n")) :
            // create a optional Roaming or Local folder, PreserveUserData may be true to store application wide settings (like login tokens)
            MessageFormatter.Format(_options.UserDataFolder, new {
                Roaming = s_roamingPath,
                Local = s_localPath,
            });
        WebView2 wv = new() {
            DefaultBackgroundColor = Color.FromArgb(0x21, 0x25, 0x29),
            CreationProperties = new() {
                AdditionalBrowserArguments = "--autoplay-policy=no-user-gesture-required",
                Language = !string.IsNullOrWhiteSpace(_options.Language) ?
                    _options.Language : CultureInfo.CurrentCulture.Name,
                UserDataFolder = userDataFolder
            }
        };
        wv.CoreWebView2InitializationCompleted += WebView2_CoreWebView2InitializationCompleted;

        // add TabItem
        tabControl.Background = TitleBarBackground;
        _webView2Tabs.Add(new TabItem {
            Name = WebViewTabName,
            Visibility = _options.Dbug.TabVisibility,
            Header = new HeaderedContentControl {
                Content = headerText
            },
            Content = wv
        });

        // navigate
        wv.Source = uri;
    }

    void WebView2_CoreWebView2InitializationCompleted(object? sender, CoreWebView2InitializationCompletedEventArgs e) {
        if (e.IsSuccess && sender is WebView2 wv) {

            // disable core features
            wv.CoreWebView2.Settings.AreDefaultContextMenusEnabled = false;
            wv.CoreWebView2.Settings.AreDefaultScriptDialogsEnabled = false;
            wv.CoreWebView2.Settings.IsStatusBarEnabled = false;
            wv.CoreWebView2.Settings.AreHostObjectsAllowed = false;

            // optional features
            wv.CoreWebView2.Settings.AreDevToolsEnabled = _options.Dbug.AreDevToolsEnabled;

            // enable required features
            wv.CoreWebView2.Settings.IsScriptEnabled = true;
            wv.CoreWebView2.PermissionRequested += CoreWebView2_PermissionRequested;
            wv.CoreWebView2.Settings.IsWebMessageEnabled = true;
            wv.CoreWebView2.WebMessageReceived += CoreWebView2_WebMessageReceived;
        }
    }

#pragma warning disable IDE1006 // Naming Styles

    sealed record PostInteropMessageResult(MessageBoxResult message);

#pragma warning restore IDE1006 // Naming Styles

    void CoreWebView2_WebMessageReceived(object? sender, CoreWebView2WebMessageReceivedEventArgs e) {
        try {
            var result = JsonSerializer.Deserialize<PostInteropMessageResult>(e.WebMessageAsJson)?.message;
            switch (result) {

                // OK should reload first tab and toggle tab (possible to console tab)
                case MessageBoxResult.OK:
                    WebView2? wv = ((TabItem)tabControl.Items[0]).Content as WebView2;
                    wv?.Reload();
                    ToggleButton_Clicked(this, new());
                    break;

                // cancel should toggle tab (possible to console tab)
                case MessageBoxResult.Cancel:
                    ToggleButton_Clicked(this, new());
                    break;
            }
        } catch (JsonException) {
        }
    }

    void CoreWebView2_PermissionRequested(object? sender, CoreWebView2PermissionRequestedEventArgs e) {
        e.SavesInProfile = true;
        if (_options.Permissions is not null) {
            CoreWebView2PermissionState permissionState;
            if (_options.Permissions.TryGetValue(e.PermissionKind, out permissionState)) {
                e.State = permissionState;
                e.Handled = true;
            } else
            if (_options.Permissions.TryGetValue(CoreWebView2PermissionKind.UnknownPermission, out permissionState)) {
                e.State = permissionState;
                e.Handled = true;
            }
        }
    }

    void RemoveTab(int index) {
        if (index >= 0 && index < _webView2Tabs.Count) {
            if (_webView2Tabs[index].Content is WebView2 wv && wv.CoreWebView2 is not null) {

                // unsubscribe from event(s)
                wv.CoreWebView2InitializationCompleted -= WebView2_CoreWebView2InitializationCompleted;
                wv.CoreWebView2.PermissionRequested -= CoreWebView2_PermissionRequested;
                wv.CoreWebView2.WebMessageReceived -= CoreWebView2_WebMessageReceived;

                // get process
                Process? wvProcess = null;
                try {
                    wvProcess = Process.GetProcessById((int)wv.CoreWebView2.BrowserProcessId);
                } catch (ArgumentException) {
                    // browser process already terminated
                }

                // store user data folder before disposing the process
                string userDataFolder = wv.CoreWebView2.Environment.UserDataFolder;
                try {
                    wv.Dispose();
                } finally {
                }

                // wait for WebView2 process to exit
                if (wvProcess is not null && !wvProcess.HasExited) {
                    wvProcess.WaitForExit(250);
                }

                // for security purposes, delete userDataFolder
                if (!_options.PreserveUserData && Directory.Exists(userDataFolder)) {
                    Directory.Delete(userDataFolder, true);
                }
            }

            _webView2Tabs.RemoveAt(index);
        }
    }

    void ToggleButton_Clicked(object sender, RoutedEventArgs e) {
        int selectedIndex = tabControl.SelectedIndex;
        do {
            selectedIndex++;
            if (selectedIndex >= tabControl.Items.Count) {
                selectedIndex = 0;
            }
        } while (
            !_options.Dbug.ToggleAnyTab &&
            tabControl.Items.Count > 1 &&
            ((TabItem)tabControl.Items[selectedIndex]).Name == ConsoleTabName);

        tabControl.SelectedIndex = selectedIndex;
    }

    void StartServices(IEnumerable<ServiceDescriptor> services, EndpointFormatter endpointFormatter) {
        try {
            var servicesToStart =
                from item in services
                where item.StartType == ServiceStartMode.Automatic
                select item.GetProcessStartInfo(endpointFormatter);
            foreach (var startupInfo in servicesToStart) {
                startupInfo.RedirectStandardOutput = true;
                startupInfo.RedirectStandardError = true;
                Process? process = Process.Start(startupInfo);
                if (process is not null && !process.HasExited) {
#if WINDOWS
                    _jobTracker.Add(process.Handle);
#endif
                    string fileName = Path.GetFileName(startupInfo.FileName);
                    process.ErrorDataReceived += (sender, e) =>
                        Process_DataReceived(sender, e, fileName);
                    process.OutputDataReceived += (sender, e) =>
                        Process_DataReceived(sender, e, fileName);
                    process.EnableRaisingEvents = true;
                    process.BeginErrorReadLine();
                    process.BeginOutputReadLine();
                }
            }
        } catch (Exception ex) {
            MessageBox.Show(this, ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

    void Process_DataReceived(object _, DataReceivedEventArgs e, string fileName) {
        if (!string.IsNullOrWhiteSpace(e.Data)) {
            Application.Current.Dispatcher.BeginInvoke(delegate () {
                Paragraph para = new(new Run($"{DateTime.Now:yyyy-MM-dd hh:mm:ss.ffff} : {fileName} : {e.Data}")) {
                    Margin = new Thickness(0)
                };
                _consoleTextBox.Document.Blocks.Add(para);
            });
        }
    }
}
