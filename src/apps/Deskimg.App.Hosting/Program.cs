// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.StaticFiles;

var builder = WebApplication.CreateBuilder(args);
string path = Path.Join(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json");
builder.Configuration.AddJsonFile(path);

var app = builder.Build();
app.Use((context, next) => {
    context.Response.Headers.AltSvc = $"h3=\":{context.Connection.LocalPort}\"";
    return next(context);
});
app.Use(async (context, nextMiddleware) => {
    // NOTE: don't use x-system-id (reserved for API services)
    context.Response.Headers["X-Powered-By"] = $"Deskimg/{ThisAssembly.Git.InformationalVersion}";
    await nextMiddleware();
});

// don't cache, we're on localhost
app.Use(async (httpContext, next) => {
    httpContext.Response.Headers.CacheControl = "no-cache";
    await next();
});

// Blazor-Environment: specifies witch /appsettings.*.json file to download
// NOTE: /hostsettings.json overrides appsettings.
app.Use(async (context, nextMiddleware) => {
    context.Response.Headers["Blazor-Environment"] = app.Environment.EnvironmentName;
    await nextMiddleware();
});

// add static files
app.UseDefaultFiles();
var mimeMap = builder.Configuration.GetSection("mimeMap").Get<IDictionary<string, string>>();
FileExtensionContentTypeProvider contentTypeProvider = mimeMap is null ? new() : new(mimeMap);
app.UseStaticFiles(new StaticFileOptions {
    ContentTypeProvider = contentTypeProvider,
    ServeUnknownFileTypes = true
});
app.UseHttpsRedirection();
if (app.Environment.IsDevelopment()) {
    app.UseDeveloperExceptionPage();
}

// add fallback endpoints
app.MapFallbackToFile("/-/{controller}/{action?}", "/index.html");

// add response handler for '/hosting.json'
Dictionary<string, IReadOnlyDictionary<string, string>?> hostingResponse = new() {
    ["Session"] = builder.Configuration.GetSection("Session").Get<IReadOnlyDictionary<string, string>>(),
    ["Endpoint"] = builder.Configuration.GetSection("Endpoint").Get<IReadOnlyDictionary<string, string>>(),
};
app.MapGet("/hosting.json", () => hostingResponse);

await app.RunAsync();
