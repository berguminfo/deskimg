// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Diagnostics;

namespace Deskimg.App;

/// <summary>
/// <see href="https://learn.microsoft.com/en-us/dotnet/api/system.serviceprocess.servicetype"/>
/// </summary>
public enum ServiceStartMode {
    ///
    None,
    /// 
    Automatic = 2,
    /// 
    Manual,
    /// 
    Disabled
}

/// <summary>
/// Represents a service description.
/// </summary>
public sealed class ServiceDescriptor {

    /// <summary>
    /// Gets or sets the start description.
    /// </summary>
    public ServiceStartMode StartType { get; set; }

    /// <inheritdoc cref="ProcessStartInfo.FileName"/>
    public string? FileName { get; set; }

    /// <inheritdoc cref="ProcessStartInfo.Environment"/>
    public IReadOnlyDictionary<string, string>? Environment { get; set; }

    /// <inheritdoc cref="ProcessStartInfo.ArgumentList"/>
    public IReadOnlyList<string>? Arguments { get; set; }

    /// <summary>
    /// Gets a <see cref="ProcessStartInfo"/> object from this description.
    /// </summary>
    /// <param name="endpointFormatter">The formatter.</param>
    /// <returns>The start info object.</returns>
    public ProcessStartInfo? GetProcessStartInfo(EndpointFormatter endpointFormatter) {
        if (string.IsNullOrWhiteSpace(FileName)) {
            return null;
        }

        FileInfo fileInfo = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, endpointFormatter.Format(FileName)));
        ProcessStartInfo startInfo = new() {
            UseShellExecute = false,
            WindowStyle = ProcessWindowStyle.Hidden,
            FileName = fileInfo.FullName,
            WorkingDirectory = fileInfo.DirectoryName
        };
        if (Environment is not null) {
            foreach (var item in Environment) {
                startInfo.Environment[item.Key] = endpointFormatter.Format(item.Value);
            }
        }
        if (Arguments is not null) {
            foreach (var item in Arguments) {
                startInfo.ArgumentList.Add(endpointFormatter.Format(item));
            }
        }

        return startInfo;
    }
}
