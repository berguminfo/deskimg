// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Diagnostics;
using System.Globalization;
using System.IO.Pipes;
using System.Runtime.CompilerServices;

namespace Deskimg.App.Wpf;

/// <summary>
/// Represents a helper class for the named pipe sending and receiving control messages.
/// </summary>
public sealed class PipeHelper {

    const int ConnectTimeout = 500;

    /// <summary>
    /// Invoked each time a detach message has been received. 
    /// </summary>
    public event EventHandler? OnDetach;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static string GetPipeName(int id) =>
        string.Format(CultureInfo.InvariantCulture, "deskimg_{0}", id);

    /// <summary>
    /// Listening on the name pipe for request messages.
    /// </summary>
    /// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is None.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    public async Task ListenAsync(CancellationToken cancellationToken = default) {
        using NamedPipeServerStream pipeServer = new(GetPipeName(Environment.ProcessId), PipeDirection.In);
        using StreamReader reader = new(pipeServer);
        do {
            try {
                await pipeServer.WaitForConnectionAsync(cancellationToken);
                var message = await reader.ReadLineAsync(cancellationToken);
                if (message == "detach") {
                    OnDetach?.Invoke(this, new());
                }
            } catch (IOException) {
                // pipe is broken or disconnected.
            } finally {
                if (pipeServer.IsConnected) {
                    pipeServer.Disconnect();
                }
            }
        } while (true);
    }

    /// <summary>
    /// Sends the given message on the named pipe.
    /// </summary>
    /// <param name="message">The message to send.</param>
    public static void Send(string message) {
        var thisProcess = Process.GetCurrentProcess();
        foreach (var otherProcess in Process.GetProcessesByName(thisProcess.ProcessName).Where(x => x.Id != thisProcess.Id)) {
            using NamedPipeClientStream pipeClient = new(".", GetPipeName(otherProcess.Id), PipeDirection.Out);
            try {
                pipeClient.Connect(ConnectTimeout);
                using StreamWriter writer = new(pipeClient);
                writer.WriteLine(message);
            } catch (TimeoutException) {
            }
        }
    }
}
