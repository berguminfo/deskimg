// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// loosely inspired on https://github.com/Francesco149/weebp

#if WINDOWS

using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using Windows.Win32.Foundation;
using static Windows.Win32.PInvoke;
using Windows.Win32.UI.WindowsAndMessaging;

namespace Deskimg.App;

/// <summary>
/// Represents a class providing a method to attach and detach to the progman worker window.
/// </summary>
[SupportedOSPlatform("windows5.0")]
public class ProgmanWorker {

    const string ProgmanClass = "Progman";
    static readonly string WorkerClass = Environment.Is64BitOperatingSystem ? "WorkerW" : "WorkerA";

    // styles blacklist taken from https://github.com/Codeusa/Borderless-Gaming/
    // blob/2fef4ccc121412f215cd7f185c4351fd634cab8b/BorderlessGaming.Logic/
    // Windows/Manipulation.cs#L70
    const WINDOW_STYLE BorderlessWindowStyle =
        WINDOW_STYLE.WS_CAPTION |
        WINDOW_STYLE.WS_THICKFRAME |
        WINDOW_STYLE.WS_SYSMENU |
        WINDOW_STYLE.WS_MAXIMIZEBOX |
        WINDOW_STYLE.WS_MINIMIZEBOX;

    const WINDOW_EX_STYLE BorderlessWindowExStyle =
        WINDOW_EX_STYLE.WS_EX_DLGMODALFRAME |
        WINDOW_EX_STYLE.WS_EX_COMPOSITED |
        WINDOW_EX_STYLE.WS_EX_WINDOWEDGE |
        WINDOW_EX_STYLE.WS_EX_CLIENTEDGE |
        WINDOW_EX_STYLE.WS_EX_LAYERED |
        WINDOW_EX_STYLE.WS_EX_STATICEDGE |
        //WINDOW_EX_STYLE.WS_EX_TOOLWINDOW |
        WINDOW_EX_STYLE.WS_EX_APPWINDOW;

    const SET_WINDOW_POS_FLAGS SetWindowPosFlags =
        SET_WINDOW_POS_FLAGS.SWP_FRAMECHANGED |
        SET_WINDOW_POS_FLAGS.SWP_NOACTIVATE |
        SET_WINDOW_POS_FLAGS.SWP_SHOWWINDOW;

    RECT _lastPosition;
    WINDOW_STYLE _lastStyle;
    WINDOW_EX_STYLE _lastExStyle;

    /// <summary>
    /// Enumerate all top most desktop windows for a instance of WorkerW.
    /// </summary>
    /// <returns>The HWND handle or NULL if not found.</returns>
    static HWND FindWallpaperWindow() {
        HWND wnd = HWND.Null;
        do {
            // find the next window
            wnd = FindWindowEx(HWND.Null, wnd, WorkerClass, null);

            // try get the window extended style
            Marshal.SetLastSystemError(0);
            int result = GetWindowLong(wnd, WINDOW_LONG_PTR_INDEX.GWL_EXSTYLE);
            int error = Marshal.GetLastSystemError();
            if (!(result == 0 && error != 0)) {
                var style = (WINDOW_EX_STYLE)result;

                // if the window has NOACTIVATE and TRANSPARENT the window should be a desktop WorkerW window (all other may be widgets and icons)
                if (style.HasFlag(WINDOW_EX_STYLE.WS_EX_NOACTIVATE) && style.HasFlag(WINDOW_EX_STYLE.WS_EX_TRANSPARENT)) {
                    break;
                }
            }
        } while (!wnd.IsNull);

        return wnd;
    }

    /// <summary>
    /// Spawn a wallpaper window if it doesn't already exists and return the handle.
    /// </summary>
    /// <returns>The wallpaper <see cref="HWND"/> handle.</returns>
    /// <exception cref="Win32Exception"/>
    static HWND GetWallpaperWindow() {

        // scanning for the Progman window to possible open a new worker window
        HWND progman = FindWindow(ProgmanClass, null);
        if (progman.IsNull) {
            throw new Win32Exception(Marshal.GetLastWin32Error());
        }

        // spawning wallpaper
        // this is basically all the magic. it's an undocumented window message that
        // forces windows to spawn a window with class "WorkerW" behind desktop icons
        SendMessage(progman, 0x052C, 0xD, 0);
        SendMessage(progman, 0x052C, 0xD, 1);

        // initial search for the windows
        HWND wallpaper = FindWallpaperWindow();
        if (wallpaper.IsNull) {

            // couldn't spawn WorkerW window, trying old method
            SendMessage(progman, 0x052C, 0, 0);
            wallpaper = FindWallpaperWindow();
        }

        // windows 7 with aero is almost the same as windows 10, except that we
        // have to hide the WorkerW window and render to Progman child windows
        // instead
        if (!wallpaper.IsNull && !OperatingSystem.IsWindowsVersionAtLeast(6, 2)) {
            // detected windows 7, hiding worker window
            ShowWindow(wallpaper, SHOW_WINDOW_CMD.SW_HIDE);
            wallpaper = progman;
        }

        if (wallpaper.IsNull) {
            // couldn't spawn window behind icons, falling back to Progman
            wallpaper = progman;
        }

        return wallpaper;
    }

    /// <summary>
    /// <see cref="SetWindowLong(HWND, WINDOW_LONG_PTR_INDEX, int)"/>
    /// </summary>
    /// <param name="wnd">A handle to the window and, indirectly, the class to which the window belongs.</param>
    /// <param name="index">The zero-based offset to the value to be set.</param>
    /// <param name="clear">The replacement bits to clear (not and).</param>
    /// <param name="assign">The replacement bits to assign (or).</param>
    /// <returns></returns>
    /// <exception cref="Win32Exception"/>
    static uint UpdateWindowLong(HWND wnd, WINDOW_LONG_PTR_INDEX index, uint clear = 0, uint assign = 0) {
        int result, error;

        Marshal.SetLastSystemError(0);
        result = GetWindowLong(wnd, index);
        error = Marshal.GetLastSystemError();
        if (result == 0 && error != 0) {
            throw new Win32Exception(error);
        }

        uint newValue = (uint)result;
        newValue &= ~clear;
        newValue |= assign;
        Marshal.SetLastSystemError(0);
        result = SetWindowLong(wnd, index, (int)newValue);
        error = Marshal.GetLastSystemError();
        if (result == 0 && error != 0) {
            throw new Win32Exception(error);
        }

        return (uint)result;
    }

    /// <summary>
    /// Embeds the constructor window reference into the wallpaper window.
    /// </summary>
    /// <param name="handle">The window handle of the main window.</param>
    /// <exception cref="Win32Exception"/>
    public void Attach(nint handle) {
        HWND wallpaper = GetWallpaperWindow();
        HWND wnd = (HWND)handle;
        if (wnd.IsNull) {
            return;
        }

        // if already attached return
        if (IsChild(wallpaper, wnd)) {
            return;
        }

        // set border less style
        _lastStyle = (WINDOW_STYLE)UpdateWindowLong(wnd, WINDOW_LONG_PTR_INDEX.GWL_STYLE,
            // toggle POPUP and CHILD bits
            clear: (uint)(BorderlessWindowStyle | WINDOW_STYLE.WS_POPUP),
            assign: (uint)WINDOW_STYLE.WS_CHILD);
        _lastExStyle = (WINDOW_EX_STYLE)UpdateWindowLong(wnd, WINDOW_LONG_PTR_INDEX.GWL_EXSTYLE,
            clear: (uint)BorderlessWindowExStyle,
            // hide window from ALT+TAB
            assign: (uint)WINDOW_EX_STYLE.WS_EX_TOOLWINDOW);

        // window retains screen coordinates so we need to adjust them
        RECT r;
        if (GetWindowRect(wnd, out _lastPosition) && GetWindowRect(wallpaper, out r)) {
            if (!SetWindowPos(wnd, HWND.Null, 0, 0, r.Width, r.Height, SetWindowPosFlags)) {
                throw new Win32Exception(Marshal.GetLastSystemError());
            }
        }

        // try to push our window behind icons (and possible loose input event)
        if (SetParent(wnd, wallpaper).IsNull) {
            throw new Win32Exception(Marshal.GetLastWin32Error());
        }
    }

    /// <summary>
    /// Pops the constructor window out of the wallpaper window and attempts to restore the old style.
    /// </summary>
    /// <param name="handle">The window handle of the main window.</param>
    /// <exception cref="Win32Exception"/>
    public void Detach(nint handle) {
        HWND wallpaper = GetWallpaperWindow();
        HWND wnd = (HWND)handle;
        if (wnd.IsNull) {
            return;
        }

        // if already detached return
        if (!IsChild(wallpaper, wnd)) {
            return;
        }

        // try detach from worker
        if (SetParent(wnd, HWND.Null).IsNull) {
            throw new Win32Exception(Marshal.GetLastSystemError());
        }

        UpdateWindowLong(wnd, WINDOW_LONG_PTR_INDEX.GWL_STYLE,
            clear: uint.MaxValue,
            assign: (uint)_lastStyle);
        UpdateWindowLong(wnd, WINDOW_LONG_PTR_INDEX.GWL_EXSTYLE,
            clear: uint.MaxValue,
            assign: (uint)_lastExStyle);

        if (!SetWindowPos(wnd, HWND.Null, _lastPosition.X, _lastPosition.Y, _lastPosition.Width, _lastPosition.Height, SetWindowPosFlags)) {
            throw new Win32Exception(Marshal.GetLastSystemError());
        }

        InvalidateRect(wallpaper, _lastPosition, true);
        GetWallpaperWindow(); // can sometimes fix leftover unrefreshed portions
    }
}

#endif
