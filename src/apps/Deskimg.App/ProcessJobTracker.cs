// Copyright (c) Andrew Arnott. All rights reserved.
// Licensed under the MIT license.

/* This is a derivative from multiple answers on https://stackoverflow.com/questions/3342941/kill-child-process-when-parent-process-is-killed */

#if WINDOWS

using System.ComponentModel;
using System.Runtime.Versioning;
using Microsoft.Win32.SafeHandles;
using Windows.Win32.System.JobObjects;
using static Windows.Win32.PInvoke;

namespace Deskimg.App;

/// <summary>
/// Allows processes to be automatically killed if this parent process unexpectedly quits
/// (or when an instance of this class is disposed).
/// </summary>
/// <remarks>
/// This "just works" on Windows 8.
/// To support Windows Vista or Windows 7 requires an app.manifest with specific content
/// <see href="https://stackoverflow.com/a/9507862/46926">as described here</see>.
/// </remarks>
[SupportedOSPlatform("windows5.1.2600")]
public sealed class ProcessJobTracker : IDisposable {

    bool _disposed;
    readonly Lock _disposeLock = new();

    /// <summary>
    /// The job handle.
    /// </summary>
    /// <remarks>
    /// Closing this handle would close all tracked processes. So we don't do it in this process
    /// so that it happens automatically when our process exits.
    /// </remarks>
    readonly SafeFileHandle? jobHandle;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessJobTracker"/> class.
    /// </summary>
    public unsafe ProcessJobTracker() {

        // The job name is optional (and can be null) but it helps with diagnostics.
        // If it's not null, it has to be unique. Use SysInternals' Handle command-line
        // utility: handle -a ChildProcessTracker
        string jobName = nameof(ProcessJobTracker) + Environment.ProcessId;
        jobHandle = CreateJobObject(null, jobName);

        var extendedInfo = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION {
            BasicLimitInformation = new JOBOBJECT_BASIC_LIMIT_INFORMATION {
                LimitFlags = JOB_OBJECT_LIMIT.JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE,
            },
        };

        if (!SetInformationJobObject(jobHandle, JOBOBJECTINFOCLASS.JobObjectExtendedLimitInformation, &extendedInfo, (uint)sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION))) {
            throw new Win32Exception();
        }
    }

    /// <summary>
    /// Ensures a given process is killed when the current process exits.
    /// </summary>
    /// <param name="h">The process handle whose lifetime should never exceed the lifetime of the current process.</param>
    public void Add(nint h) {
        using var processHandle = new SafeFileHandle(h, ownsHandle: false);
        bool success = AssignProcessToJobObject(jobHandle, processHandle);
        if (!success) {
            throw new Win32Exception();
        }
    }

    /// <summary>
    /// Kills all processes previously tracked with <see cref="Add"/> by closing the Windows Job.
    /// </summary>
    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    void Dispose(bool disposing) {
        if (disposing) {
            lock (_disposeLock) {
                if (!_disposed) {
                    jobHandle?.Dispose();
                }

                _disposed = true;
            }
        }
    }
}

#endif
