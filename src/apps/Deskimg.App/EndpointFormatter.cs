// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA5394 // Do not use insecure randomness

using System.Globalization;
using System.Net.NetworkInformation;
using Jeffijoe.MessageFormat;

namespace Deskimg.App;

/// <summary>
/// Represents a port range.
/// </summary>
/// <param name="MinInclusive">The minimum inclusive port number.</param>
/// <param name="MaxInclusive">The maximum inclusive port number.</param>
public record PortRange(ushort MinInclusive = 49152, ushort MaxInclusive = 65535);

/// <summary>
/// Represents a <see cref="MessageFormatter"/> for endpoints.
/// </summary>
public class EndpointFormatter {

    readonly MessageFormatter _formatter;

    static readonly Dictionary<string, object?> s_args = new() {
        ["Port"] = null, // assign a random port
        ["Platform"] = OperatingSystem.IsWindows() ? nameof(OperatingSystem.IsWindows) : string.Empty
    };

    /// <summary>
    /// Initializes a new instance of the <see cref="EndpointFormatter"/> class.
    /// </summary>
    /// <param name="portRange">The requested port range.</param>
    /// <param name="assignedPorts">The list of assigned ports.</param>
    /// <param name="reservedPorts">The list reserved ports.</param>
    public EndpointFormatter(PortRange? portRange = null, IDictionary<string, int>? assignedPorts = null, HashSet<int>? reservedPorts = null) {
        _formatter = new(customValueFormatter: CreateCustomValueFormatter(portRange, assignedPorts, reservedPorts));
    }

    static CustomValueFormatters CreateCustomValueFormatter(PortRange? portRange, IDictionary<string, int>? assignedPorts, HashSet<int>? reservedPorts) {
        if (portRange is null) {
            portRange = new();
        }
        if (assignedPorts is null) {
            assignedPorts = new Dictionary<string, int>();
        }
        if (reservedPorts is null) {
            reservedPorts = (
                from item in IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners()
                where portRange.MinInclusive <= item.Port && item.Port <= portRange.MaxInclusive
                select item.Port
            ).ToHashSet();
        }

        // remove existing assignments witch has been reserved
        if (assignedPorts.Count > 0) {
            foreach (var reservedPort in reservedPorts) {
                var item = assignedPorts.FirstOrDefault(x => x.Value == reservedPort);
                if (item.Key is not null) {
                    assignedPorts.Remove(item.Key);
                }
            }
        }

        Random random = new();
        return new CustomValueFormatters {
            Number = (CultureInfo cultureInfo, object? value, string? style, out string? formatted) => {
                if (value is null && !string.IsNullOrEmpty(style)) {
                    if (!assignedPorts.TryGetValue(style, out int assignedPort)) {

                        // don't try to assign a new random port if all available is assigned
                        if (reservedPorts.Count > portRange.MaxInclusive - portRange.MinInclusive) {
                            throw new ArgumentOutOfRangeException(nameof(portRange));
                        }

                        // find a unassigned random port
                        do {
                            assignedPort = random.Next(portRange.MinInclusive, portRange.MaxInclusive);
                        } while (reservedPorts.Contains(assignedPort));

                        // add the port to the list of reserved ports
                        reservedPorts.Add(assignedPort);

                        // store service port alias
                        assignedPorts[style] = assignedPort;
                    }

                    formatted = assignedPort.ToString(cultureInfo);
                    return true;
                } else {
                    formatted = null;
                    return false;
                }
            }
        };
    }

    /// <summary>
    /// Formats the <see href="https://github.com/messageformat/messageformat"/> pattern.
    /// </summary>
    /// <remarks>
    /// List of endpoint format patterns:
    /// <para>
    /// The "Port" number substitution returns the already assigned port
    /// or a random port for the service.
    /// </para>
    /// <para>
    /// The "Platform" select substitution equals "IsWindows" if
    /// <see cref="OperatingSystem.IsWindows()"/> returns true.
    /// </para>
    /// </remarks>
    /// <example>
    /// <list type="bullet">
    /// <item><c>{Port, number, fetch}</c></item>
    /// <item><c>{Platform, select, IsWindows{net9.0-windows10.0.17763.0} other{net9.0}}</c></item>
    /// </list>
    /// </example>
    /// <param name="pattern">The message pattern.</param>
    /// <returns>The formatted <see cref="string"/>.</returns>
    /// <exception cref="ArgumentOutOfRangeException">All available ports has been assigned.</exception>
    public string Format(string? pattern) => _formatter.FormatMessage(pattern ?? string.Empty, s_args);

    /// <summary>
    /// Formats the <see href="https://github.com/messageformat/messageformat"/> pattern.
    /// </summary>
    /// <remarks>
    /// <seealso cref="Format(string)"/>
    /// </remarks>
    /// <param name="pattern">The message format pattern.</param>
    /// <param name="args">The message format pattern arguments.</param>
    /// <returns>The formatted <see cref="string"/>.</returns>
    public string Format(string? pattern, IReadOnlyDictionary<string, object?> args) =>
        _formatter.FormatMessage(pattern ?? string.Empty, args);
}

#pragma warning restore CA5394 // Do not use insecure randomness
