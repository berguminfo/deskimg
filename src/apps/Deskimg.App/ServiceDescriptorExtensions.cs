// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Diagnostics;

namespace Deskimg.App;

/// <summary>
/// Represents extensions methods for <see cref="ServiceDescriptor"/> classes.
/// </summary>
public static class ServiceDescriptionExtensions {

    /// <summary>
    /// Gets a <see cref="IEnumerator{ProcessStartInfo}"/> enumerator.
    /// </summary>
    /// <param name="descriptions">The list of descriptions.</param>
    /// <param name="endpointFormatter">The endpoint formatter.</param>
    /// <returns>The enumerator.</returns>
    public static IEnumerable<ProcessStartInfo> GetProcessStartInfoEnumerator(
        this IEnumerable<ServiceDescriptor> descriptions, EndpointFormatter endpointFormatter) =>
        from item in descriptions
        let startInfo = item.GetProcessStartInfo(endpointFormatter)
        where startInfo is not null
        select startInfo;
}
