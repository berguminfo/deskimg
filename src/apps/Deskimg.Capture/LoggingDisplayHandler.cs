// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using CefSharp;
using CefSharp.Handler;

namespace Deskimg.Capture;

#pragma warning disable CA1848 // Use the LoggerMessage delegates, don't apply for console output
#pragma warning disable CA2254 // Template should be a static expression, don't apply for console output

sealed class LoggingDisplayHandler(ILogger logger) : DisplayHandler {

    protected override bool OnConsoleMessage(IWebBrowser browser, ConsoleMessageEventArgs e) {

        // The message event uses '\n' as line separator.
        // But the console may use some other line separator.
        string message = string.Join(Environment.NewLine, e.Message.Split('\n'));
        logger.Log((LogLevel)e.Level, message);

        return true; // stop the message from being output to the console
    }
}

#pragma warning restore CA1848 // Use the LoggerMessage delegates
#pragma warning restore CA2254 // Template should be a static expression
