// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Capture;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

static partial class LoggerExtensions {

    // 100: CaptureWorker.ExecuteAsync

    [LoggerMessage(
        EventId = 101,
        Level = LogLevel.Error,
        Message = "Error execute capture operation. See exception details for more information."
    )]
    public static partial void ExecuteFailed(this ILogger logger, Exception ex);

    [LoggerMessage(
        EventId = 102,
        Level = LogLevel.Information,
        Message = "Scheduling repeat in '{Interval}'."
    )]
    public static partial void SchedulerStarting(this ILogger logger, TimeSpan interval);

    // 110: CaptureWorker.ExecuteOnce

    [LoggerMessage(
        EventId = 110,
        Level = LogLevel.Critical,
        Message = "Error initialize Cef. Enable Cef debugging for more information."
    )]
    public static partial void InitializeCefFailed(this ILogger logger);

    [LoggerMessage(
        EventId = 111,
        Level = LogLevel.Debug,
        Message = "Trying invoke capture for '{Get}'."
    )]
    public static partial void GetStarting(this ILogger logger, string? get);

    [LoggerMessage(
        EventId = 112,
        Level = LogLevel.Warning,
        Message = "Error capture the resource at '{Get}'. See exception details for more information."
    )]
    public static partial void CaptureFailed(this ILogger logger, Exception ex, string? get);

    // 120: CaptureWorker.CaptureAsync

    [LoggerMessage(
        EventId = 120,
        Level = LogLevel.Information,
        Message = "Missing the 'OutputFormat' setting, skipping this item."
    )]
    public static partial void GetItemSkipped(this ILogger logger);

    [LoggerMessage(
        EventId = 121,
        Level = LogLevel.Warning,
        Message = "The URI '{Get}' is invalid, skipping this item."
    )]
    public static partial void GetItemSkipped(this ILogger logger, string? get);

    [LoggerMessage(
        EventId = 122,
        Level = LogLevel.Debug,
        Message = "Waiting for render complete at '{When}'. Timeout: '{Timeout}'."
    )]
    public static partial void CaptureWaiting(this ILogger logger, DateTime when, TimeSpan timeout);

    [LoggerMessage(
        EventId = 123,
        Level = LogLevel.Information,
        Message = "Saved to '{Path}' having {Length} bytes."
    )]
    public static partial void CaptureCompleted(this ILogger logger, string path, long length);

    [LoggerMessage(
        EventId = 124,
        Level = LogLevel.Warning,
        Message = "Skipping capture. No OnAfterRender message was received and the '--CaptureOnAfterRender true' argument is set. This may indicates a failed Blazor rendering."
    )]
    public static partial void CaptureSkipped(this ILogger logger);

    [LoggerMessage(
        EventId = 125,
        Level = LogLevel.Information,
        Message = "{When}: Capture start."
    )]
    public static partial void CaptureStart(this ILogger logger, DateTime when);

    [LoggerMessage(
        EventId = 128,
        Level = LogLevel.Warning,
        Message = "Received unknown message {Message}."
    )]
    public static partial void UnknownMessage(this ILogger logger, object message);

    [LoggerMessage(
        EventId = 213,
        Level = LogLevel.Trace,
        Message = "{Counter}: Elapsed: {Elapsed}ms. Remaining: {Remaining}ms. SpinWait: {Delay}ms."
    )]
    public static partial void SpinWait(this ILogger logger, int counter, long elapsed, long remaining, long delay);

    [LoggerMessage(
        EventId = 321,
        Level = LogLevel.Trace,
        Message = "{Counter}: Elapsed: {Elapsed}ms. Remaining: {Remaining}ms: OnAfterRender: {Message}."
    )]
    public static partial void OnAfterRender(this ILogger logger, int counter, long elapsed, long remaining, object message);
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
