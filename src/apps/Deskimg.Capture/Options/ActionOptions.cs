// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Capture.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
public sealed class ActionOptions {

    /// <summary>
    /// Specifies the URI to capture.
    /// </summary>
    public required string Get { get; set; }

    /// <summary>
    /// Specifies the output filename.
    /// </summary>
    public required string Put { get; set; }
}
