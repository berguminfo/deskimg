// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Capture.Options;

/// <summary>
/// Represents the root appsettings options.
/// </summary>
public sealed class AppOptions {

    /// <summary>
    /// True to capture on the OnAfterRender event, otherwise capture on timeout.
    /// </summary>
    public bool RequireOnAfterRender { get; set; }

    /// <summary>
    /// Specifies the <see cref="MessageFormatter"/> output format.
    /// </summary>
    public required string OutputFormat { get; set; }

    /// <summary>
    /// Gets or sets the capture delay after last OnAfterRender event.
    /// </summary>
    public TimeSpan Wait { get; set; }

    /// <summary>
    /// Gets or sets the timeout for each capture.
    /// </summary>
    public TimeSpan Timeout { get; set; }

    /// <summary>
    /// Gets or sets the spin delay.
    /// </summary>
    public TimeSpan SpinDelay { get; set; } = TimeSpan.FromMilliseconds(250);

    /// <summary>
    /// Gets or sets the browser language, or current culture if not specified.
    /// </summary>
    public string? Language { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="ScheduleOptions"/> options.
    /// </summary>
    public ScheduleOptions? Schedule { get; set; }

    /// <summary>
    /// Gets or sets the capture view port.
    /// </summary>
    public ViewPortOptions? ViewPort { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="CefOptions"/> options.
    /// </summary>
    public CefOptions? Cef { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="ActionOptions"/> options.
    /// </summary>
    public IReadOnlyList<ActionOptions>? Capture { get; set; }
}
