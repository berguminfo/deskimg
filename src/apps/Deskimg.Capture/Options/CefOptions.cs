// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using CefSharp;

namespace Deskimg.Capture.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
public sealed class CefOptions {

    /// <summary>
    /// Gets or sets the logging level.
    /// </summary>
    public LogSeverity LogLevel { get; set; } = LogSeverity.Warning;

    /// <summary>
    /// Gets or sets the <see cref="MessageFormatter"/> cache path pattern.
    /// </summary>
    public string? CachePath { get; set; }

    /// <summary>
    /// Gets or sets delay timeout to wait for browser to close.
    /// </summary>
    public TimeSpan? WaitForBrowsersToCloseTimeout { get; set; }
}
