// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Drawing;

namespace Deskimg.Capture.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
public sealed class ViewPortOptions {

    /// <summary>
    /// Gets or sets the view port width.
    /// </summary>
    public int Width { get; set; } = 1920;

    /// <summary>
    /// Gets or sets the view port height.
    /// </summary>
    public int Height { get; set; } = 1080;

    /// <summary>
    /// Gets or sets the view port scale factor.
    /// </summary>
    public double Scale { get; set; } = 2.0;

    /// <summary>
    /// Gets a <see cref="Size"/> object representation.
    /// </summary>
    /// <returns>The object creation result.</returns>
    public Size ToSize() => new(Width, Height);

    /// <see cref="ToSize"/>
    public static implicit operator Size(ViewPortOptions value) => value.ToSize();
}
