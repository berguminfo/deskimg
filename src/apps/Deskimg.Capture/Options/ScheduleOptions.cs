// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Capture.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
public sealed class ScheduleOptions {

    /// <summary>
    /// Gets or sets the scheduled repeating.
    /// </summary>
    public TimeSpan? Repeat { get; set; }

    /// <summary>
    /// Gets or sets the scheduled duration.
    /// </summary>
    public TimeSpan? Duration { get; set; }
}
