param (
    $OutputPath = (Join-Path $PSScriptRoot "obj"),
    $Get = 'https://localhost:5000/',
    $Put,
    $Set = ('corp', '408'),
    $Nav = $true,
    $Lang = 'en-GB',
    $Toi = "$(Get-Date -f "yyyy-MM-dd")T22:20:00",
    $Geo = 'ZZZZ',
    $RequireOnAfterRender = 'true',
    $Wait = '00:00:03',
    $TotalTimeout = '00:00:30',
    $LoggingLevel = 'Trace'
)

if (!$Put) {
    Get-Content "$PSScriptRoot/obj/x64/Debug/net9.0/win-x64/GitInfo.cache" | % {
        if ($_ -match '(\w+)=(\d+)') {
            if ('GitSemVerMajor' -eq $Matches.1) {
                $GitSemVerMajor = $Matches.2
            }
            if ('GitSemVerMinor' -eq $Matches.1) {
                $GitSemVerMinor = $Matches.2
            }
        }
    }
    $Put = "release-$GitSemVerMajor.$GitSemVerMinor.webp"
}

$QueryString = $Set | % { "🌐=$_" }
if ($Nav) {
    $QueryString += 'nav=✓'
}
$QueryString +=
    "lang=$Lang",
    "t=$Toi",
    "geo=$Geo"
$Parameters =
    "OutputFormat=$OutputPath/{FileName}",
    "Capture:0:Get=$($Get)?$($QueryString -join '&')",
    "Capture:0:Put=$Put",
    "RequireOnAfterRender=$RequireOnAfterRender",
    "Wait=$Wait",
    "Timeout=$TotalTimeout",
    "Logging:LogLevel:Default=Critical",
    "Logging:LogLevel:Deskimg.Capture=$LoggingLevel",
    "Logging:LogLevel:Deskimg.Console=Critical"

echo "Deskimg.Capture"
echo $Parameters
dotnet run $Parameters
