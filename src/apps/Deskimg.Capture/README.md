# Deskimg.Capture

Tool to capture a snapshot of web pages.

## Syntax

```sh
Deskimg.Capture
  [--Capture:0:Get <string>]
  [--Capture:0:Put <string>]
  [--ViewPort:Width <uint>]
  [--ViewPort:Height <uint>]
  [--ViewPort:Scale <double>]
  [--Wait <TimeSpan>]
  [--Timeout <TimeSpan>]
  [--RequireOnAfterRender <bool>]
  [--Language <string>]
  [--Cef:LogLevel <LogSeverity>]
  [--Cef:WaitForBrowsersToCloseTimeout <TimeSpan>]
  [--OutputFormat <string>]
  [--Logging:LogLevel:Deskimg <LogLevel>]
  [--Logging:LogLevel:Console <LogLevel>]
  [--export <string>]
```

👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)

## Parameters

### --Capture:0:Get

Specifies the source URL to fetch.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --Capture:0:Put

Specifes the destination file name.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --ViewPort:Width

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | UInt32                                                    |
| Required:        | False                                                     |

### --ViewPort:Height

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | UInt32                                                    |
| Required:        | False                                                     |

### --ViewPort:Scale

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Double                                                    |
| Required:        | False                                                     |

### --Wait

Specifies the capture delay after last OnAfterRender event.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 00:00:03                                                  |
| Required:        | False                                                     |

### --Timeout

Specifies the timeout for each capture.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 00:00:30                                                  |
| Required:        | False                                                     |

### --RequireOnAfterRender

True to capture listening on `OnAfterRender` events, otherwise capture on timeout.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | Boolean                                                   |
| Default value:   | True                                                      |
| Required:        | False                                                     |

### --Language

Specifies the the browser language, or current culture if not specified.
Used to control the formation date, time and localized strings.
Expected value is the two letter ISO code of the language.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | False                                                     |

### --Cef:LogLevel

Specifies the browser logging log serverity.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogSeverity                                               |
| Accepted values: | Disable, Fatal, Error, Warning, Info, Verbose, Default    |
| Default value:   | Warning                                                   |
| Required:        | False                                                     |

### --Cef:WaitForBrowsersToCloseTimeout

Specifies the delay timeout to wait for browser to close.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | TimeSpan                                                  |
| Default value:   | 00:00:30                                                  |
| Required:        | False                                                     |

### --OutputFormat

Specifies the output format for fetch operations.

The following substitutions are available for use.

- `{Local}`
  - Replaced with the value of `SpecialFolder.LocalApplicationData` + `Deskimg`.
- `{Roaming}`
  - Replaced with the value of `SpecialFolder.ApplicationData` + `Deskimg`.
- `{FileName}`
  - The file name of the fetch source.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --Logging:LogLevel:Deskimg

Specifies the logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Information                                               |
| Required:        | False                                                     |

### --Logging:LogLevel:Console

Specifies the `F12 console` logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Warning                                                   |
| Required:        | False                                                     |

### --export

Generates a `Scheduled Tasks` or `crontab` sample to a file.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | False                                                     |
