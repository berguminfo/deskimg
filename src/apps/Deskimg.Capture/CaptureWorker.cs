// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Diagnostics;
using System.Globalization;
using Microsoft.Extensions.Options;
using CefSharp;
using CefSharp.DevTools.Page;
using CefSharp.OffScreen;

namespace Deskimg.Capture;

using Deskimg.Capture.Options;

/// <summary>
/// Represents the capture service worker.
/// </summary>
sealed class CaptureWorker(
    ILogger<CaptureWorker> logger,
    ILogger<ChromiumWebBrowser> consoleLogger,
    IOptions<AppOptions> appOptions,
    IHostApplicationLifetime lifetime) : BackgroundService {

    readonly AppOptions options = appOptions.Value;

    readonly ManualResetEventSlim _afterRenderEvent = new();
    readonly Stopwatch _afterRenderStopwatch = new();
    int _afterRenderCounter;

    static readonly string s_roamingPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
        nameof(Deskimg));
    static readonly string s_localPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        nameof(Deskimg));

    /// <inheritdoc cref="BackgroundService.Dispose"/>
    public override void Dispose() {
        base.Dispose();
        _afterRenderEvent.Dispose();
    }

    /// <inheritdoc/>
    public override Task StartAsync(CancellationToken cancellationToken) {
        Cef.EnableWaitForBrowsersToClose();
        return base.StartAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public override Task StopAsync(CancellationToken cancellationToken) {
        lifetime.StopApplication();
        return base.StopAsync(cancellationToken);
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
        while (!stoppingToken.IsCancellationRequested) {
            try {
                ExecuteOnce(stoppingToken);
            } catch (Exception ex) {
                logger.ExecuteFailed(ex);
            }

            // HACK: capture repeat: Execute must keep CefHost alive in the context thread.
            lifetime.StopApplication();

            if (options.Schedule?.Repeat is not null) {
                await Task.Delay(options.Schedule.Repeat.Value, stoppingToken);
            } else {
                lifetime.StopApplication();
            }
        }
    }

    static string GetCachePath() => Path.Join(AppContext.BaseDirectory, "User Data", "Default");

    void ExecuteOnce(CancellationToken cancellationToken) {

        // Console app doesn't have a message loop which we need as Cef.Initialize/Cef.Shutdown must be called on the same
        // thread. We use a super simple SynchronizationContext implementation from
        // https://devblogs.microsoft.com/pfxteam/await-synchronizationcontext-and-console-apps/
        // Continuations will happen on the main thread
        // The Nito.AsyncEx.Context Nuget package has a more advanced implementation
        // https://github.com/StephenCleary/AsyncEx/blob/8a73d0467d40ca41f9f9cf827c7a35702243abb8/doc/AsyncContext.md#console-example-using-asynccontext

        AsyncContext.Run(async delegate {

            // Initialize Cef using the provided CefSettings.
            string locale = options.Language ?? CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            if (locale == "en") {
                locale = CultureInfo.CurrentCulture.Name;
            }
            bool success = await Cef.InitializeAsync(new CefSettings {
                LogSeverity = options.Cef?.LogLevel ?? LogSeverity.Default,
                Locale = locale,

                // The location where cache data will be stored on disk. If empty an in-memory cache will be used for some features and a temporary disk cache for others.
                // HTML5 databases such as localStorage will only persist across sessions if a cache path is specified.
                CachePath = options.Cef?.CachePath ?? GetCachePath()
            });

            if (!success) {

                // OBS! Stop here, can't continue without Cef
                logger.InitializeCefFailed();
                lifetime.StopApplication();
                return;
            }

            if (options.Capture is not null) {
                foreach (var captureOptions in options.Capture.Distinct()) {
                    try {
                        logger.GetStarting(captureOptions.Get);
                        await CaptureAsync(captureOptions, cancellationToken);
                    } catch (Exception ex) {
                        // log and continue with the next 'Get' item
                        logger.CaptureFailed(ex, captureOptions.Get);
                    }
                }
            }

            // Wait until the browser has finished closing (which by default happens on a different thread).
            // Cef.EnableWaitForBrowsersToClose(); must be called before Cef.Initialize to enable this feature
            // See https://github.com/cefsharp/CefSharp/issues/3047 for details
            TimeSpan timeout = options.Cef?.WaitForBrowsersToCloseTimeout ?? TimeSpan.FromSeconds(30);
            Cef.WaitForBrowsersToClose((int)timeout.TotalMilliseconds);

            // Clean up Chromium objects.  You need to call this in your application otherwise
            // you will get a crash when closing.
            Cef.Shutdown();
        });
    }

    async Task CaptureAsync(ActionOptions captureOptions, CancellationToken cancellationToken) {

        // find capture source
        if (!Uri.TryCreate(captureOptions.Get, UriKind.Absolute, out Uri? source)) {
            logger.GetItemSkipped(captureOptions.Get);
            return;
        }

        // find capture destination path
        string destination = Path.GetFullPath(
            MessageFormatter.Format(options.OutputFormat, new {
                Roaming = s_roamingPath,
                Local = s_localPath,
                FileName = captureOptions.Put
            }));
        string? destinationDirectory = Path.GetDirectoryName(destination);
        if (destinationDirectory is not null) {
            Directory.CreateDirectory(destinationDirectory);
        }

        // RequestContext can be shared between browser instances and allows for custom settings
        // e.g. CachePath
        using BrowserSettings browserSettings = new() {
            // Reduce rendering speed to one frame per second so it's easier to take screen shots
            WindowlessFrameRate = 1
        };
        RequestContextSettings requestContextSettings = new();
        using RequestContext requestContext = new(requestContextSettings);
        using ChromiumWebBrowser browser = new(source.ToString());
        browser.DisplayHandler = new LoggingDisplayHandler(consoleLogger);
        browser.JavascriptMessageReceived += Browser_JavascriptMessageReceived;
        await browser.WaitForInitialLoadAsync();

        if (options.ViewPort is not null) {
            browser.Size = options.ViewPort;
            browser.SetZoomLevel(options.ViewPort.Scale);
        }

        // wait for browser to finish rendering
        if (options.RequireOnAfterRender) {

            // start the stopwatch
            _afterRenderStopwatch.Reset();

            // wait for the firstRender event
            logger.CaptureWaiting(DateTime.Now, options.Timeout);
            if (!_afterRenderEvent.Wait(options.Timeout, cancellationToken)) {

                // got the wait for render complete timeout,
                // avoid capture in case nothing has been rendered
                logger.CaptureSkipped();
                return;
            }

            // spin until all renderes has completed
            _afterRenderStopwatch.Restart();
            do {
                // 1: Elapsed: 10ms. Remaining: 990ms. SpinWait: 100ms.
                logger.SpinWait(
                    _afterRenderCounter,
                    _afterRenderStopwatch.ElapsedMilliseconds,
                    (long)(options.Wait - _afterRenderStopwatch.Elapsed).TotalMilliseconds,
                    (long)options.SpinDelay.TotalMilliseconds);

                await Task.Delay(options.SpinDelay, cancellationToken);
            } while (_afterRenderStopwatch.Elapsed < options.Wait);

            // stop the stopwatch, not needed anymore
            _afterRenderStopwatch.Stop();
        } else {

            // optimistically guess rendering completes at a timeout
            logger.CaptureWaiting(DateTime.Now, options.Wait);
            await Task.Delay(options.Wait, cancellationToken);
        }

        // hopefully rendering has completed, start capturing the pixels
        logger.CaptureStart(DateTime.Now);

        // wait for the screen-shot to be taken,
        // if one exists ignore it, wait for a new one to make sure we have the most up to date
        CaptureScreenshotFormat? format = Path.GetExtension(destination)?.ToUpperInvariant() switch {
            ".JPG" or ".JPEG" => CaptureScreenshotFormat.Jpeg,
            ".PNG" => CaptureScreenshotFormat.Png,
            ".WEBP" => CaptureScreenshotFormat.Webp,
            _ => null
        };

        byte[] screenshot = await browser.CaptureScreenshotAsync(format);

        // Make a file to save it to
        await File.WriteAllBytesAsync(destination, screenshot, cancellationToken);
        logger.CaptureCompleted(destination, screenshot.Length);
    }

    const string SessionOnAfterRender = "Session_OnAfterRender";

    void Browser_JavascriptMessageReceived(object? sender, JavascriptMessageReceivedEventArgs e) {
        if (e.Message is string message && message.StartsWith(SessionOnAfterRender, StringComparison.Ordinal)) {
            Interlocked.Increment(ref _afterRenderCounter);

            // 1: Elapsed: 10ms. Remaining: 990ms. OnAfterRender: Background.
            logger.OnAfterRender(
                _afterRenderCounter,
                _afterRenderStopwatch.ElapsedMilliseconds,
                (long)(options.Wait - _afterRenderStopwatch.Elapsed).TotalMilliseconds,
                message[(SessionOnAfterRender.Length + 1)..]);

            // restart the stopwatch
            _afterRenderStopwatch.Restart();

            // the blazor boot has completed, signal the wait handler
            _afterRenderEvent.Set();
        } else {
            logger.UnknownMessage(e.Message);
        }
    }
}
