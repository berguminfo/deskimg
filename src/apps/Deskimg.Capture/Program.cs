// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Deskimg.Capture;
using Deskimg.Capture.Options;

if (!await CommonCommands.IsExportCommand(args)) {
    var builder = Host.CreateApplicationBuilder(args);
    builder.Logging.AddSimpleConsole(config => {
        config.SingleLine = true;
    });
    builder.Services.Configure<AppOptions>(builder.Configuration);
    builder.Services.AddHostedService<CaptureWorker>();

    Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
    await builder.Build().RunAsync();
}
