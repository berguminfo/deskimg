// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Collections.Concurrent;

namespace Deskimg.Capture;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

// https://devblogs.microsoft.com/pfxteam/await-synchronizationcontext-and-console-apps/
sealed class SingleThreadSynchronizationContext : SynchronizationContext, IDisposable {

    readonly BlockingCollection<KeyValuePair<SendOrPostCallback, object>> _queue = [];

    public void Dispose() {
        _queue.Dispose();
    }

    public override void Post(SendOrPostCallback d, object? state) {
        if (state is not null) {
           _queue.Add(new KeyValuePair<SendOrPostCallback, object>(d, state));
        }
    }

    public void RunOnCurrentThread() {
        while (_queue.TryTake(out var workItem, Timeout.Infinite)) {
            workItem.Key(workItem.Value);
        }
    }

    public void Complete() {
        _queue.CompleteAdding();
    }
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
