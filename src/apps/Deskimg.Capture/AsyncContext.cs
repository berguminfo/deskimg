// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Capture;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

// https://devblogs.microsoft.com/pfxteam/await-synchronizationcontext-and-console-apps/
static class AsyncContext {

    public static void Run(Func<Task> func) {
        var prevCtx = SynchronizationContext.Current;

        try {
            using SingleThreadSynchronizationContext syncCtx = new();
            SynchronizationContext.SetSynchronizationContext(syncCtx);

            var t = func();
            t.ContinueWith(delegate {
                syncCtx.Complete();
            }, TaskScheduler.Default);

            syncCtx.RunOnCurrentThread();

            t.GetAwaiter().GetResult();
        } finally {
            SynchronizationContext.SetSynchronizationContext(prevCtx);
        }
    }
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
