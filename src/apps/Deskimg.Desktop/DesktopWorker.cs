// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using System.Globalization;

namespace Deskimg.Desktop;

using Deskimg.Desktop.Options;

/// <summary>
/// Represents the desktop service worker.
/// </summary>
public sealed class DesktopWorker(
    ILogger<DesktopWorker> logger,
    IOptions<AppOptions> appOptions,
    IServiceProvider sp,
    CompositionService compositions,
    IHostApplicationLifetime lifetime) : BackgroundService {

    readonly AppOptions options = appOptions.Value;

    const string TimeStampFormat = "MMdd_HHmmss";

    static readonly string s_roamingPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
        nameof(Deskimg));
    static readonly string s_localPath = Path.Join(
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        nameof(Deskimg));

    /// <inheritdoc/>
    public override Task StopAsync(CancellationToken cancellationToken) {
        lifetime.StopApplication();
        return base.StopAsync(cancellationToken);
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
        while (!stoppingToken.IsCancellationRequested) {
            try {
                await FetchAsync(stoppingToken);
            } catch (OperationCanceledException) {
            } catch (SetWallpaperException<int> ex) {
                logger.SetWallpaperFailed(ex, ex.Status);
            } catch (SetWallpaperException<bool> ex) {
                logger.SetWallpaperFailed(ex, ex.Status);
            } catch (Exception ex) {
                logger.SetWallpaperFailed(ex.Message);
            }

            if (options.Schedule?.Repeat is not null) {
                await Task.Delay(options.Schedule.Repeat.Value, stoppingToken);
            } else {
                lifetime.StopApplication();
            }
        }
    }

    async Task FetchAsync(CancellationToken cancellationToken) {
        // downloaded images should go in the same directory
        DateTime now = DateTime.Now;
        string timestamp = $"{now.Year - 2020}{now.ToString(TimeStampFormat, CultureInfo.CurrentCulture)}";

        if (options.Fetch is not null) {
            foreach (var fetchOptions in options.Fetch.Distinct()) {
                if (!string.IsNullOrEmpty(fetchOptions.Get)) {
                    DirectoryInfo? wallpaperDirectory = await GetAsync(fetchOptions.Get, timestamp, cancellationToken);
                    if (wallpaperDirectory is not null) {
                        if (options.MaxArchive > 0) {
                            Clean(wallpaperDirectory);
                        }

                        if (!string.IsNullOrEmpty(fetchOptions.Put)) {
                            await PutAsync(fetchOptions.Put, wallpaperDirectory, cancellationToken);
                        }
                    }
                }
            }
        }
    }

    ValueTask<DirectoryInfo?> GetAsync(string getValue, string timestamp, CancellationToken cancellationToken) {
        DirectoryInfo? result = null;

        bool isRemote =
            getValue.StartsWith($"{Uri.UriSchemeHttps}{Uri.SchemeDelimiter}", StringComparison.OrdinalIgnoreCase) ||
            getValue.StartsWith($"{Uri.UriSchemeHttp}{Uri.SchemeDelimiter}", StringComparison.OrdinalIgnoreCase);
        if (!isRemote) {
            var source = MessageFormatter.Format(getValue, new {
                Roaming = s_roamingPath,
                Local = s_localPath,
            });
            if (!File.Exists(source)) {
                logger.GetSourceNotFound(source);
            } else {
                string output = MessageFormatter.Format(options.OutputFormat, new {
                    Roaming = s_roamingPath,
                    Local = s_localPath,
                    TimeStamp = timestamp,
                    FileName = Path.GetFileName(source)
                });

                string destination = Path.GetFullPath(output);
                result = new DirectoryInfo(Path.GetDirectoryName(destination) ?? destination);
                if (!result.Exists) {
                    result.Create();
                }

                File.Copy(source, destination, true);
                logger.GetCompleted(source, destination);
            }
        }
#if NULL
        if (Uri.TryCreate(getValue, UriKind.Absolute, out Uri? requestUri)) {
                string path = Helper.NormalizePath(string.Format(OutputFormat,
                    DateTime.Now, Path.GetFileName(requestUri.LocalPath)), CreateDirectory);
                using Stream? source = await Http.GetStreamAsync(requestUri, cancellationToken);
                using Stream? destination = File.Create(path, 0x1000, FileOptions.Asynchronous);
                await source.CopyToAsync(destination, cancellationToken);
                paths.Add(path);
        }
#endif

        return ValueTask.FromResult(result);
    }

    void Clean(DirectoryInfo wallpaperDirectory) {
        var include = (from item in wallpaperDirectory.Parent?.GetFileSystemInfos()
                       orderby item.Name descending
                       select item).Skip(options.MaxArchive);
        foreach (var item in include) {
            logger.CleanUpStarting(item.FullName);

            if (item.Attributes.HasFlag(FileAttributes.Directory)) {
                Directory.Delete(item.FullName, true);
            } else {
                item.Delete();
            }
        }
    }

    async Task PutAsync(string putValue, DirectoryInfo wallpaperDirectory, CancellationToken cancellationToken) {
        if (options.SetDesktop is not null &&
            options.SetDesktop.TryGetValue(putValue, out var setDesktopOptions) &&
            setDesktopOptions.Provider is not null) {
            Type? type = compositions.GetExport<IDesktopProvider>(setDesktopOptions.Provider);

            if (type is not null) {
                var provider = sp.GetService(type) as IDesktopProvider;
                if (provider is not null) {
                    await provider.SetWallpaperAsync(setDesktopOptions, wallpaperDirectory, cancellationToken);
                    logger.PutCompleted(wallpaperDirectory.FullName, type.Name);
                }
            } else {
                logger.ProviderNotFound(putValue);
            }
        }
    }
}
