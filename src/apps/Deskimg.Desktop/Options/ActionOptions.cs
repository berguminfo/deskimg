// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Desktop.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
public sealed class ActionOptions {

    /// <summary>
    /// Specifies th source image <see cref="MessageFormatter"/> pattern.
    /// </summary>
    public required string Get { get; set; }

    /// <summary>
    /// Specifies the reference to a <see cref="SetDesktopOptions"/> setting.
    /// </summary>
    public required string Put { get; set; }
}
