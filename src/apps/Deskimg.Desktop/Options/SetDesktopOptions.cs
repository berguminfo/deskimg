// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Desktop.Options;

/// <summary>
/// Represents additional appsettings options.
/// </summary>
public sealed class SetDesktopOptions {

    /// <summary>
    /// Specifies the <see cref="IDesktopProvider"/>.
    /// </summary>
    public required string Provider { get; set; }

    /// <summary>
    /// Additional property to the provider.
    /// </summary>
    public string? Command { get; set; }

    /// <summary>
    /// Additional property to the provider.
    /// </summary>
    public int? Desktop { get; set; }

    /// <summary>
    /// Additional property to the provider.
    /// </summary>
    public string? Position { get; set; }
}
