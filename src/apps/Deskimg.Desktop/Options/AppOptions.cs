// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Desktop.Options;

/// <summary>
/// Represents the appsettings options.
/// </summary>
public sealed class AppOptions {

    /// <summary>
    /// Specifies the <see cref="MessageFormatter"/> output format.
    /// </summary>
    public required string OutputFormat { get; set; }

    /// <summary>
    /// Specifies the maximum number of archived files.
    /// </summary>
    public int MaxArchive { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="ScheduleOptions"/> options.
    /// </summary>
    public ScheduleOptions? Schedule { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="SetDesktopOptions"/> options.
    /// </summary>
    public IReadOnlyDictionary<string, SetDesktopOptions>? SetDesktop { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="ActionOptions"/> options.
    /// </summary>
    public IReadOnlyList<ActionOptions>? Fetch { get; set; }
}
