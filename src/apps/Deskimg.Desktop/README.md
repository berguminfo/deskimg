# Deskimg.Desktop

Tool to set the desktop wallpaper.

## Syntax

```sh
Deskimg.Desktop
  [--Fetch:0:Get <string>]
  [--Fetch:0:Put <string>]
  [--SetDesktop:Exec:Command <string>]
  [--OutputFormat <string>]
  [--MaxArchive <uint>]
  [--Logging:LogLevel:Deskimg <LogLevel>]
  [--export <string>]
```

👉 [Command-line arguments manual](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-9.0#command-line-arguments)

## Examples

### Example 1: Set the desktop wallpaper to a windows desktop

```sh
Deskimg.Desktop --Fetch:0:Put ShobjN --Fetch:0:Get wallpaper.jpg
```

### Example 2: Set the desktop wallpaper to a GNOME desktop

```sh
dotnet Deskimg.Desktop.dll --Fetch:0:Put Exec --SetDesktop:Exec:Command tools/gsettings-wallpaper.sh --Fetch:0:Get wallpaper.jpg
```

## Parameters

### --Fetch:0:Get

Specifies the source file to fetch.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --Fetch:0:Put

Specifies the method used to set the desktop wallpaper. This can be any subkey under the `SetDesktop` section in `appsettings.json`. By default, the following desktop types are available.


- `Exec`
  - Sets the desktop wallpaper using an external script or application.
- `Shobj0`
  - Sets the first windows desktop wallpaper.
- `ShobjN`
  - Sets the windows desktop wallpaper.
- `User32`
  - Sets the windows desktop wallpaper (legacy).

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --SetDesktop:Exec:Command

Specifies the command to execute to set the desktop wallpaper.

💡 The `tools/` directory contains a few well known tools.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --OutputFormat

Specifies the output format for fetch operations.

The following substitutions are available for use.

- `{Local}`
  - Replaced with the value of `SpecialFolder.LocalApplicationData` + `Deskimg`.
- `{Roaming}`
  - Replaced with the value of `SpecialFolder.ApplicationData` + `Deskimg`.
- `{TimeStamp}`
  - Replaced with the current time in a `MMdd_HHmmss` format.
- `{FileName}`
  - The file name of the fetch source.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | True                                                      |

### --MaxArchive

Specifies the maximum number of images to archive.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | UInt32                                                    |
| Default value:   | 1                                                         |
| Required:        | False                                                     |

### --Logging:LogLevel:Deskimg

Specifies the logging log level.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | LogLevel                                                  |
| Accepted values: | None, Critical, Error, Warning, Information, Debug, Trace |
| Default value:   | Warning                                                   |
| Required:        | False                                                     |

### --export

Generates a `Scheduled Tasks` or `crontab` sample to a file.

|                  |                                                           |
| ---------------- | --------------------------------------------------------- |
| Type:            | String                                                    |
| Required:        | False                                                     |
