#!/usr/bin/env sh
gconftool-2 --type string --set /desktop/gnome/background/picture_filename "$1"
gconftool-2 --type string --set /desktop/gnome/background/picture_options spanned
