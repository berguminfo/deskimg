#!/usr/bin/env sh
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-path -s $1
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-style -s 3
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-show -s true
xfdesktop --reload
