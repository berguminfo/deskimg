#!/usr/bin/env sh
gsettings set org.gnome.desktop.background picture-uri "file://$1"
gsettings set org.gnome.desktop.background picture-options spanned
