#!/usr/bin/env sh
gsettings set org.gnome.desktop.screensaver picture-uri "file://$1"
gsettings set org.gnome.desktop.screensaver picture-options spanned
