// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using Deskimg.Desktop;
using Deskimg.Desktop.Options;

if (!await CommonCommands.IsExportCommand(args)) {
    var builder = Host.CreateApplicationBuilder(args);
    builder.Services.Configure<AppOptions>(builder.Configuration);
    builder.Services.AddHostedService<DesktopWorker>();
    builder.Services.AddCompositions(new DesktopExports());

    Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
    await builder.Build().RunAsync();
}
