// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Desktop;

[AssemblyExportsGenerated]
partial class DesktopExports : IAssemblyExports {

    /// <inheritdoc cref="IAssemblyExports.GetExports"/>
    public partial IImmutableDictionary<(Type, string), Type> GetExports();
}
