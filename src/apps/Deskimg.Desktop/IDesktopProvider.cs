// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Desktop;

using Deskimg.Desktop.Options;

/// <summary>
/// Represents the desktop services.
/// </summary>
public interface IDesktopProvider {

    /// <summary>
    /// Sets the desktop wallpaper.
    /// </summary>
    /// <param name="options">The desktop options.</param>
    /// <param name="wallpaperDirectory">The wallpaper directory to set.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    Task SetWallpaperAsync(SetDesktopOptions options, DirectoryInfo wallpaperDirectory, CancellationToken cancellationToken = default);
}
