// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Deskimg.Desktop;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

public static partial class LoggerExtensions {

    // 100: DesktopWorker.ExecuteAsync

    [LoggerMessage(
        EventId = 101,
        Level = LogLevel.Error,
        Message = "Failed to set wallpaper, got status code {Status}. See exception details for more information."
    )]
    public static partial void SetWallpaperFailed(this ILogger logger, Exception ex, int status);

    [LoggerMessage(
        EventId = 102,
        Level = LogLevel.Error,
        Message = "Failed to set wallpaper, got status flag {Status}. See exception details for more information."
    )]
    public static partial void SetWallpaperFailed(this ILogger logger, Exception ex, bool status);

    [LoggerMessage(
        EventId = 103,
        Level = LogLevel.Error,
        Message = "{Message}"
    )]
    public static partial void SetWallpaperFailed(this ILogger logger, string message);

    // 110: DesktopWorker.FetchAsync

    // 120: DesktopWorker.GetAsync

    [LoggerMessage(
        EventId = 120,
        Level = LogLevel.Warning,
        Message = "Missing the 'OutputFormat' setting, skipping this item."
    )]
    public static partial void GetItemSkipped(this ILogger logger);

    [LoggerMessage(
        EventId = 121,
        Level = LogLevel.Warning,
        Message = "Source file not found '{Source}', skipping this item."
    )]
    public static partial void GetSourceNotFound(this ILogger logger, string source);

    [LoggerMessage(
        EventId = 122,
        Level = LogLevel.Debug,
        Message = "Succeeded copy the file from '{Source}' to '{Destination}'."
    )]
    public static partial void GetCompleted(this ILogger logger, string source, string destination);

    // 130: DesktopWorker.Clean

    [LoggerMessage(
        EventId = 130,
        Level = LogLevel.Debug,
        Message = "Trying cleanup the directory at '{Path}'."
    )]
    public static partial void CleanUpStarting(this ILogger logger, string path);

    // 140: DesktopWorker.PutAsync

    [LoggerMessage(
        EventId = 140,
        Level = LogLevel.Information,
        Message = "Succeeded setting desktop image '{Path}' using '{Provider}'."
    )]
    public static partial void PutCompleted(this ILogger logger, string path, string provider);

    [LoggerMessage(
        EventId = 141,
        Level = LogLevel.Warning,
        Message = "Skipping unknown provider: '{Provider}'."
    )]
    public static partial void ProviderNotFound(this ILogger logger, string provider);
}

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
