// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if WINDOWS || true

using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using Windows.Win32;
using Windows.Win32.UI.WindowsAndMessaging;

namespace Deskimg.Desktop.Providers;

using Deskimg.Desktop.Options;

/// <summary>
/// Provider to set the desktop wallpaper using the SystemParametersInfo API.
/// </summary>
/// <remarks>
/// The image should be a BMP image.
/// </remarks>
[Export<IDesktopProvider>(nameof(User32))]
[SupportedOSPlatform("windows5.0")]
public class User32 : IDesktopProvider {

    /// <inheritdoc/>
    public unsafe Task SetWallpaperAsync(SetDesktopOptions options, DirectoryInfo wallpaperDirectory, CancellationToken cancellationToken = default) {
        FileInfo? image = wallpaperDirectory.GetFiles().FirstOrDefault();
        if (image is not null) {
            nint buffer = Marshal.StringToHGlobalAuto(image.FullName);
            try {
                bool succeeded = PInvoke.SystemParametersInfo(
                    SYSTEM_PARAMETERS_INFO_ACTION.SPI_SETDESKWALLPAPER,
                    0,
                    buffer.ToPointer(), // unsafe
                    SYSTEM_PARAMETERS_INFO_UPDATE_FLAGS.SPIF_UPDATEINIFILE | SYSTEM_PARAMETERS_INFO_UPDATE_FLAGS.SPIF_SENDCHANGE);
                if (!succeeded) {
                    throw new SetWallpaperException<int>("SystemParametersInfo", Marshal.GetHRForLastWin32Error());
                }
            } finally {
                Marshal.FreeHGlobal(buffer);
            }
        }

        return Task.CompletedTask;
    }
}

#endif
