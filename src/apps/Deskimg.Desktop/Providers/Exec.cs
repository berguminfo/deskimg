// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

using System.Diagnostics;

namespace Deskimg.Desktop.Providers;

using Deskimg.Desktop.Options;

/// <summary>
/// Provider to set the desktop wallpaper using some external process.
/// </summary>
[Export<IDesktopProvider>(nameof(Exec))]
public class Exec : IDesktopProvider {

    /// <inheritdoc/>
    public async Task SetWallpaperAsync(SetDesktopOptions options, DirectoryInfo wallpaperDirectory, CancellationToken cancellationToken = default) {
        if (string.IsNullOrWhiteSpace(options.Command)) {
            throw new InvalidOperationException();
        }

        ProcessStartInfo startInfo = new() {
            FileName = options.Command,
            Arguments = wallpaperDirectory.FullName,
            UseShellExecute = false
        };
        var process = Process.Start(startInfo);
        if (process == null) {
            throw new InvalidOperationException(nameof(Exec));
        }

        if (!process.HasExited) {
            await process.WaitForExitAsync(cancellationToken);
        }

        if (process.ExitCode != 0) {
            throw new SetWallpaperException<int>("Process.ExitCode: {ExitCode}", process.ExitCode);
        }
    }
}
