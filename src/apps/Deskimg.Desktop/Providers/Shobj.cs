// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if WINDOWS8_0_OR_GREATER || true

using System.Runtime.Versioning;
using Windows.Win32;
using Windows.Win32.Foundation;
using Windows.Win32.UI.Shell;

namespace Deskimg.Desktop.Providers;

using Deskimg.Desktop.Options;

/// <summary>
/// Provider to set the desktop wallpaper using the IDesktopWallpaper API.
///
/// Supported on Windows 8 or greater.
/// </summary>
/// <remarks>
/// The image path must not exceed MAX_PATH wchar_t bytes.
/// </remarks>
[Export<IDesktopProvider>(nameof(Shobj))]
[SupportedOSPlatform("windows8.0")]
public class Shobj : IDesktopProvider {

    /// <inheritdoc/>
    public Task SetWallpaperAsync(SetDesktopOptions options, DirectoryInfo wallpaperDirectory, CancellationToken cancellationToken = default) {
        var desktopWallpaper = (IDesktopWallpaper)new DesktopWallpaper();
        if (options.Desktop is null or >= 0) {
            FileInfo? image = wallpaperDirectory.GetFiles().FirstOrDefault();
            if (image == null) {

                // prefer silent notification
                return Task.CompletedTask;
            }

            // get the monitor and set wallpaper
            string? monitorId = null;
            if (options.Desktop is not null) {
                desktopWallpaper.GetMonitorDevicePathAt((uint)options.Desktop, out PWSTR monitorIdNative);
                monitorId = monitorIdNative.ToString();
            }

            string wallpaper = @"\\?\" + image.FullName;
            desktopWallpaper.SetWallpaper(monitorId, wallpaper);

#if ENABLE_SET_POSITION

            // Position, default DWPOS_SPAN.
            var position = DESKTOP_WALLPAPER_POSITION.DWPOS_SPAN;
            if (Enum.TryParse(options.Position, out DESKTOP_WALLPAPER_POSITION positionEnum)) {
                position = positionEnum;
            } else {
                if (int.TryParse(options.Position, out int positionNumber)) {
                    position = (DESKTOP_WALLPAPER_POSITION)positionNumber;
                }
            }

            pDesktopWallpaper.SetPosition(position);
#endif
        } else {
            HRESULT hr;

            // Create pShellItem.
            hr = PInvoke.SHCreateItemFromParsingName(wallpaperDirectory.FullName, null, typeof(IShellItem).GUID, out object pShellItem);
            hr.ThrowOnFailure();

            // Create pShellItemArray.
            hr = PInvoke.SHCreateShellItemArrayFromShellItem((IShellItem)pShellItem, typeof(IShellItemArray).GUID, out object pShellItemArray);
            hr.ThrowOnFailure();

            // Set wallpaper.
            desktopWallpaper.SetSlideshow((IShellItemArray)pShellItemArray);

            // TODO: pDesktopWallpaper.SetSlideshowOptions(DESKTOP_SLIDESHOW_OPTIONS.DSO_SHUFFLEIMAGES, (uint)TimeSpan.FromHours(1).TotalMilliseconds);
        }

        return Task.CompletedTask;
    }
}

#endif
