// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#if WINDOWS10_0_17763_0_OR_GREATER

using System.Runtime.Versioning;
using Windows.Storage;
using Windows.System.UserProfile;

namespace Deskimg.Desktop.Providers;

using Deskimg.Desktop.Options;

/// <summary>
/// Provider to set the desktop wallpaper using the UserProfilePersonalizationSettings API.
///
/// Supported on Windows 10.0.10240.0.
/// </summary>
/// <remarks>
/// The image path must be located somewhere in the isolated storage.
/// </remarks>
[Export<IDesktopProvider>(nameof(UWP))]
[SupportedOSPlatform("windows10.0.10240.0")]
public class UWP : IDesktopProvider {

    /// <inheritdoc/>
    public async Task SetWallpaperAsync(SetDesktopOptions options, DirectoryInfo wallpaperDirectory, CancellationToken cancellationToken = default) {
        FileInfo? image = wallpaperDirectory.GetFiles().FirstOrDefault();
        if (image == null) {
            // Prefer silent notification.
            return;
        }

        if (UserProfilePersonalizationSettings.IsSupported()) {
            var imageFile = await StorageFile.GetFileFromPathAsync(image.FullName);
            if (!imageFile.IsAvailable) {
                throw new SetWallpaperException<bool>($"GetFileFromPathAsync: IsAvailable is false. Path: '{image.FullName}'", false);
            } else {
                if (options.Desktop < 0) {
                    if (!await UserProfilePersonalizationSettings.Current.TrySetLockScreenImageAsync(imageFile)) {
                        throw new SetWallpaperException<bool>($"TrySetLockScreenImage: returned E_FAIL. Path: '{image.FullName}'", false);
                    }
                } else {
                    if (!await UserProfilePersonalizationSettings.Current.TrySetWallpaperImageAsync(imageFile)) {
                        throw new SetWallpaperException<bool>($"TrySetWallpaperImage: returned E_FAIL. Path: '{image.FullName}'.", false);
                    }
                }
            }
        }
    }
}

#endif
