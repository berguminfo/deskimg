// Deskimg. Copyright © BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

#pragma warning disable CA1515 // Exceptions should be public

using System.Globalization;

namespace Deskimg.Desktop;

/// <summary>
/// Represents the exception throw on SetWallpaper critical failure.
/// </summary>
public class SetWallpaperException : Exception {

    /// <inheritdoc/>
    public SetWallpaperException() { }

    /// <inheritdoc/>
    public SetWallpaperException(string? message) : base(message) { }

    /// <inheritdoc/>
    public SetWallpaperException(string? message, Exception? innerException) : base(message, innerException) { }
}

/// <inheritdoc/>
public class SetWallpaperException<T> : SetWallpaperException where T : struct {

    /// <summary>
    /// Gets or sets the operation status.
    /// </summary>
    public T Status { get; set; }

    /// <inheritdoc/>
    public SetWallpaperException() { }

    /// <inheritdoc/>
    public SetWallpaperException(string? message) : base(message) { }

    /// <inheritdoc/>
    public SetWallpaperException(string? message, Exception? innerException) : base(message, innerException) { }

    /// <inheritdoc cref="SetWallpaperException.SetWallpaperException(string?)"/>
    public SetWallpaperException(string? message, T status) : base(message) => Status = status;

    /// <inheritdoc/>
    public override string ToString() {
        return string.Format(CultureInfo.CurrentCulture, Message, Status);
    }
}

#pragma warning restore CA1515 // Exceptions should be public
