# Changelog

## 7.6.2 (2025-02-18)

### Bug fixes

🐛 Fixed cache expiration.

🐛 Fixed display of headers time in local timezone.

## 7.6.1 (2025-01-13)

### Miscellaneous improvements

🛠️ Replaced fetch permissions with the `fetch-token` feature.

### Bug fixes

🐛 __App.Wpf:__ Fixed sandbox startup, reporting certificate error.

🐛 __Meteorology:__ Fixed recent update to the `cap:eventCode` elements.

🐛 __Meteorology:__ Fixed NVE messageformat strings.

🐛 __Meteorology:__ Fixed Yr severity mapping.

## 7.6.0 (2024-11-13)

<img src="docs/assets/images/release-7.6.webp" width="600"/>

### New actions

⚡ Requires `net-9.0` to take advantage of new `C#v13` features.

✨ __App.Wpf:__ Added a <kbd>CTRL</kbd> + <kbd>F11</kbd> shortcut to display in kiosk mode attached to the desktop wallpaper behind icons.

✨ __App.Wpf:__ Added `--mode kiosk` and `--mode wallpaper` command options to startup in the specified display mode.

✨ __App.Wpf:__ Added `--control detach` command option to detach from the desktop wallpaper.

⚡ __Background:__ Added support for video playback (applies to `--bgmode Dom`).

⚡ __Background:__ Updated the theme selector to switch between any `data-bs-theme` given the background image luminance.

⚡ __Background:__ Added sample `highcontrast` and `vid` layout sets.

⚡ __CalDAV:__ Added `MongoDB` storage option as the `/api/3.0/caldav` and `/api/3.1/caldav` endpoints.

⚡ __Calendar:__ Added support for the `CalDAV` protocol.

⚡ __Partial:__ Added support for markup content like `HTML` and `SVG`.

⚡ __Settings:__ Added a `InitialValue` configuration option for newly added components.

### Miscellaneous improvements

🛠️ __Calendar:__ Replaced `IntervalInf` with `rangeStart` and `rangeLength` parameters.

🛠️ __Calendar:__ Refactored the symbol mapping construct to be deterministic.

🛠️ __Desktop:__ Defined the `UWP` provider to be an experimental feature.

🛠️ __Inline, Partial:__ Added `WITHOUT_MARKUP`, `WITH_MARKUP` defines to include or exclude markup rendering.

🛠️ __Meteorology:__ Replaced the previous v4.3.0 `List<T>` based readings with individual `struct` constructs.

🛠️ __Meteorology:__ Updated the cap:1.2 parser to be in sync with the remote dataset.

### Bug fixes

🐛 __Background:__ Fixed fetching the `bg=⟦xs:anyURI⟧` session specified wallpaper.

## v7.5.1 (2024-10-08)

### Bug fixes

🐛 Workaround [runtime/#99030](https://github.com/dotnet/runtime/issues/99030).
   - The NonBacktracking source generated `Regex` object may assert using the AOT compiler.

🐛 __Meteorology:__ Updated the `metalerts` 1.1 API to version 2.0.

🐛 __Settings:__ Fixed uncleared bind value when adding components.

🐛 __Settings:__ Fixed `Background` expanding properties for the `Preview` page.

🐛 __Specs:__ Fixed the offline build.

## v7.5.0 (2024-04-29)

<img src="docs/assets/images/release-7.5.webp" width="600"/>

### New actions

⚡ Added `xs:duration` parsing to the `t=⟦xs:dateTime⟧` parameter.

⚡ Added [GeoURI](http://tools.ietf.org/html/rfc5870) parsing to the `geo=⟦xs:string⟧` parameter.

⚡ Added `DataType` annotations.

⚡ Added the `Partial` component in order to embed web resources.

⚡ __Plugins:__ Added `Foundation.Ref` the foundation for the plugins experiense.

⚡ __Plugins:__ Added a sample `Desklet.QRCode` plugin.

⚡ __Settings:__ Added a modify layout tab.

⚡ __Settings:__ Added `datalist` suggestions to `input` elements using annotation hints.

⚡ __Settings:__ Added string translations.

⚡ __Settings:__ Added `Move Up`, `Move Down` and `Remove` actions.

### Miscellaneous improvements

🛠️ __Atom:__ Added a hyperlink icon to open the article.

🛠️ __Navigation:__ Replaced the dropdown design with datalist suggestions and responsive containers.

🛠️ __Settings:__ Changed the hosting protocol to be H2 compatible.

### Bug fixes

🐛 __App.Wpf:__ Fixed ArgumentException on system termination.

🐛 __Background:__ Fixed the background composition during polar twilight transitions.

🐛 __Epg:__ Fixed the ordering of channels.

🐛 __Epg:__ Fixed the `epg.allente.no` fetch from cache validation.

## v7.4.1 (2024-03-20)

### Bug fixes

🐛 Changed the `traffic-info.vegvesen.no` endpoint to be in sync with the recent server update.

## v7.4.0 (2024-03-14)

<img src="docs/assets/images/release-7.4.webp" width="600"/>

### New actions

⚡ New components `Deskimg.App.Wpf` and `Deskimg.App.Hosting` to view and host the application.

⚡ __Settings:__ New components to manage application settings.

⚡ __Capture:__ Added new option `--export` to generate a `Task Scheduler` task.

⚡ __Desktop:__ Added new option `--export` to generate a `Task Scheduler` task.

⚡ __Background:__ Added `🌐=img` and `img=⟦xs:string⟧` background wallpaper options.

⚡ __Container:__ New component to render child components.

⚡ __Navigation:__ Added a geographic location map picker.

⚡ __Navigation:__ Added viewport clipping selector buttons.

### Miscellaneous improvements

🛠️ Updated text rendering components to include CSS styles.

🛠️ __Capture:__ Delay capture on last `AfterRender` event.

🛠️ __Navigation:__ Let the `Now` button set the update clock to be a realtime clock.

🛠️ __Location:__ Updated status icons to have indicator colors.

### Bug fixes

🐛 __Meteorology:__ Fixed the `yr.no` forecast provider `Toi` filtering.

## v7.3.0 (2024-01-13)

<img src="docs/assets/images/release-7.3.webp" width="600"/>

### New actions

⚡ Split Blazor APIs into `Blazor`, `Components`, `Settings` and `WebAssembly` libraries.

⚡ Added translations for `da-DK`, `fi-FI`, `sv-FI` and `sv-SE`.

⚡ Added `cw` and `ch` query parameters to specify clipping boundary (capture mode).

⚡ __Alert:__ Added new provider `traffic-info.vegvesen.no`.

⚡ __Alert:__ Added optional `Notify` parameter to display [Notifications API](https://notifications.spec.whatwg.org/) end user notifications.

⚡ __Audio:__ New components to play audio resources.

⚡ __Background:__ Added optional `Blob` and `Css` rendering modes.

⚡ __Epg:__ Added new provider `epg.allente.no`.

⚡ __Inline:__ New component to render Text and HTML content.

⚡ __Meteorology:__ Added additional weather symbol themes `310`, `dark`, `light` and `shadows`.

⚡ __Video:__ New component to play video resources.

### Performance improvements

🔥 Replaced all JSON serialization with source generator.

🔥 Replaced `RegexOptions.Compile` constructs with the source generator.

🔥 Replaced `Composition` reflection constructs with source generator.

🔥 Replaced exported types with generic `ExportAttribute`.

🔥 Replaced `IConfiguration` references with `IOptions<T>`.

🔥 __libnova:__ Replaced `webpack` bundling with `esbuild`.

### Miscellaneous improvements

🛠️ Remove duplicate execution of first rendering.

🛠️ Component titles is optional, replaced with `DisplayXXX` parameters.

🛠️ __Meteorology:__ Improved weather symbol selection to be specified in manifests.

### Bug fixes

🐛 Fixed the chrome warning message *setTimeout violates timing constrain* by using `IAsyncEventHandler`.

🐛 Fixed a regression when trying to update the geographical coordinates from the GPS receiver.

🐛 __Location:__ Fixed polar day/night to begin or end at -0.25° (`STANDARD_ALTITUDE_SUN_UPPER_LIMB`).

🐛 __Meteorology:__ Fixed METAR token mismatch.

🐛 __Meteorology:__ Fixed METAR polar night weather symbol selection.

## v7.2.1 (2023-10-10)

### Miscellaneous improvements

🛠️ __Background:__ Updated background theme (308).

### Bug fixes

🐛 __Sets:__ Fixed invalid JSON.

## v7.2.0 (2023-08-03)

<img src="docs/assets/images/release-7.2.webp" width="600"/>

### New actions

⚡ Split Web APIs into `Fetch`, `Sets`, `CalDAV` services.

⚡ __Navigation:__ Add search capability in `nav=✓` mode.

⚡ __Sets:__ Support merging multiple sets into one.

### Miscellaneous improvements

🛠️ Merge bash and ps1 scripts into one source script.

🛠️ Replace `GitVersion` with `GitInfo`.

### Bug fixes

🐛 __Calendar:__ Fixed rendering on the day of polar twilight.

## v7.1.3 (2023-07-19)

### Bug fixes

🐛 __Calendar:__ Fixed incorrect `RiseSetState` algorithm.

## v7.1.2 (2023-07-02)

### Bug fixes

🐛 __Calendar:__ Disable historic events.

## v7.1.1 (2023-06-13)

### Bug fixes

🐛 __Image:__ Fixed 'fetch camera image', very long expiration.

🐛 __Meteorology:__ Fixed METAR temperature parser.

## v7.1.0 (2023-06-02)

<img src="docs/assets/images/release-7.1.webp" width="600"/>

### New actions

⚡ __Navigation:__ Added optional `🌐=⟦xs:string⟧` query string.

⚡ __Atom:__ Added `atom10` feed support.

⚡ __Meteorology:__ Added `avalanche.nve.no` provider.

⚡ __Meteorology:__ Added `flood.nve.no` provider.

⚡ __Meteorology:__ Added `landslide.nve.no` provider.

⚡ __Meteorology:__ Added `ultraviolet index` for clear sky weather condition.

⚡ __Fetch:__ Add `PROPFIND` fetch support.

⚡ __Fetch:__ Initial work on `CalDAV` provider.

### Miscellaneous improvements

🛠️ Change relative days intervals to be `-…`, `-𝑥`, `+`, `+𝑥`, `…`.

🛠️ Replaced `SwaggerGen` with `TypeSpec (Cadl)`.

🛠️ Replaced Iso3166 `TextTemplate` with simpler `XSL-Transform`.

🛠️ __Calendar:__ Updated Calendar APIs to resolve a number of old issues.

🛠️ __Calendar:__ Updated holiday calendars, add more and historic national events.

🛠️ __Meteorology:__ Align CAP schema with the provider expiration recommendation.

🛠️ __Fetch:__ Restricted requests to only accept allowed endpoints.

### Bug fixes

🐛 __Meteorology:__ Fixed DateTime arithmetics range overflows.

### Other changes

❌ Remove Location-Measure. Unused and clutters the repository.

❌ ShobjDesktop: Disable set position, users should use system UI instead.

## v7.0.4 (2023-02-01)

### Miscellaneous improvements

🛠️ Upgraded bootstrap to version 5.3.0-alpha1.

🛠️ Updated status images to use tabler-icons theme.

🛠️ __Capture:__ Upgraded Cef to version 109.1.110.

🛠️ __libnova:__ Upgraded libnova to version 7.7.2.

### Bug fixes

🐛 Fixed `CompressionStream` exception (firefox-108.0).

🐛 __Calendar:__ Fixed polar summer and winter indicator for southern latitudes.

🐛 __Calendar:__ Fixed calendar to react on location change.

🐛 __Calendar:__ Fixed lunar phase hunt granularity.

🐛 __Meteorology:__ Fixed forecast continuation exception.

## v7.0.3 (2022-12-13)

### Miscellaneous improvements

🛠️ __Capture:__ Upgraded Cef to version 107.0.5304.110.

### Bug fixes

🐛 __Capture:__ Fixed Capture zooming setting.

🐛 __Meteorology:__ Fixed METAR temperature tokenizer.

## v7.0.2 (2022-11-25)

### New actions

⚡ __Calendar:__ Added new provider `PolarTwilight`.

⚡ __Meteorology:__ Added forecast continuation.

⚡ __Epg:__ Added EPG continuation.

⚡ __Location:__ Added Tide Level continuation.

### Bug fixes

🐛 Fixed moon RiseSet timezone mismatch.

## v7.0.1 (2022-11-02)

### Bug fixes

🐛 Merged hotfix/tests.

## v7.0.0 (2022-11-01)

<img src="docs/assets/images/release-7.0.webp" width="600"/>

### New actions

⚡ Replaced complex cache `handlers` and `journals` with simple `fetch` service.

⚡ Replaced complex configuration `XHTML`, `XLINK` documents with simple `JSON` configuration.

⚡ Replaced complex `Layouts`, `PostCSS`, `D2D`, `SVG` with `HTML` layout.

⚡ Replaced integrated `Rendering` logic with standalone `Deskimg.Capture.exe` component.

⚡ Replaced integrated `SetDesktop` logic with standalone `Deskimg.Desktop.exe` component.

⚡ __Alert:__ Added severity colors.

⚡ __Alert:__ Added `alerting.yr.no` provider.

⚡ __Epg:__ Added `epg.nrk.no` provider.

⚡ __Meteorology:__ Added gust wind speed.

⚡ __Location:__ Added `astronomy-bundle.js` for better accuracy.

⚡ __Location:__ Replaced karttverket `TXT` format with `REST` API.

### Miscellaneous improvements

🛠️ __Atom:__ Don't overwrite encapsulated image.

🛠️ __Calendar:__ Improve visualization.

🛠️ __Epg:__ Use interface reference instead of copy-by-value.

### Bug fixes

🐛 __Meteorology:__ Fixed `METAR` unknown weather conditions.

### Other changes

❌ Removed `Bond` never really used for anything but `JSON` serializing.

❌ Removed complex settings `XML`, `Detectors`, `Options`.

❌ Removed remotes, partly replaced with `nuget` packages.

❌ __Alert:__ Removed `NVE` providers.

❌ __Background:__ Removed lockscreen rules.

❌ __Epg:__ Removed `XMLTV` provider (shutdown).

❌ __Image:__ Removed unused SVG and filter feature.

❌ __Location:__ Removed `glyphmisc.ttf`.

❌ __Location:__ Removed `sun.c`, `moontool.c` and variants.

❌ __Location:__ Removed `libnova.c` (incorrect calculations).

❌ __Meteorology:__ Removed composition background.

❌ __Ticker:__ Removed (shutdown).

## v6.3.0-alpha (halted 2019-03-13)

### Planned features

✏️ DX: Upgrade to SharpDX-4.0.0, and enable D2D1_SVG.

✏️ HTTP/2.0

✏️ Width, Height: May use percent unit in addition to px. IE. Width = MeasureUnit (50, "%").

✏️ Layout: Use flows to position desklets. (DeskletA.X = Surface.50% + DeskletB.Width - Surface.10%)

✏️ New feature — GPS: Resolve o=meteorology,ou=yr.no to nearest known station.

✏️ SaveAsync to ICacheItem, remove Deskimg_ prefix.

✏️ Remove Wait(), use await.

✏️ D2D1_SVG

✏️ TimeTableDesklet.

✏️ Windows: ManagementDesklet.

✏️ ImageDesklet: Video Capture.

✏️ ImageDesklet: SVG rasterization.

✏️ CSS: Get style from id, class, style.

✏️ CSS: postcss replaces less.

✏️ Drawing: images don't align to top.

✏️ Get scale factor for monitor.

✏️ Roboto-Glyphmisc-Regular.

✏️ DWriteFontCollection

✏️ PangoFontCollection

## v6.2.4 (2017-11-20)

### Miscellaneous improvements

🛠️ Update background images.

### Bug fixes

🐛 libnova.amd64 build.

## v6.2.3 (2017-08-02)

### Bug fixes

🐛 Resolve cachedb storage.

## v6.2.2 (2017-06-15)

### Bug fixes

🐛 xmltv.se domain change.

## v6.2.1 (2017-06-10)

### Bug fixes

🐛 Set Dialog-Warning fill color.

## v6.2.0 (2017-06-02)

<img src="docs/assets/images/release-6.2.webp" width="600"/>

### New actions

⚡ Option to choose output format. See `--output-format`.

⚡ Option to print scheduled task. See `--scheduled-task`.

⚡ Option to disable settings probes. See `--disable-probe`.

⚡ DX: Add rendering features (outline-text, WIC-bitmap, stroke-style).

⚡ SVG: serve currentColor.

⚡ Info: MessageFormat: {now, date, @format} and {assembly, attribute, @type, @property}

### Miscellaneous improvements

🛠️ Switch from global to session cancellation.

🛠️ Deskimg--help.bat: Enable colorized output.

🛠️ Set default background image.

🛠️ DPI-aware: Provide manifest to disable automatic scale.

🛠️ Geolocation: Identify timezone by TZDB ID.

🛠️ mono-4.8: Enable TLSv1.2.

### Bug fixes

🐛 Serialize to standard output.

🐛 DX: Preserve images aspect ratio.

🐛 Fix SVG images foreground color given background luminance.

🐛 Don't require surface dimension, should be optional.

🐛 Dst: Format output using Session.Culture instead of CurrentUICulture.

🐛 Fix GradientStops alignments.

## v6.1.0 (2017-01-13)

<img src="docs/assets/images/release-6.1.webp" width="600"/>

### New actions

⚡ Options to choose probe. See `--network-probe`,
`--high-contrast-probe`.

⚡ Option to exclude low speed networks. See `--min-network-speed`.

⚡ Meteorology: Fetch localized resources (yr.no, nve.no).

### Miscellaneous improvements

🛠️ Performance: Geolocation: Replace filesystem store with a Bond serialized object.

🛠️ Performance: Remove disposal of the composition catalog.

🛠️ Pipeline: Hybrid bond / xml deserialization.

🛠️ Location: Per item rise and set icons.

🛠️ Removed daemon mode.

### Bug fixes

🐛 Calendar: Fixed alignment and padding errors.

🐛 Caching: Fixed FileNotFoundException on failure.

🐛 Fixed `--margin=` validation.

🐛 Fixed relative `--output=` paths (mono).

🐛 Fixed `--at=` parse rules dd.MM.yyyy, MM/dd/yyyy.

🐛 Fixed surface size calculation for multi monitor.

## v6.0.2 (2017-01-10)

### Bug fixes

🐛 Fixed moontool3 timeout.

## v6.0.1 (2016-12-20)

### Miscellaneous improvements

🛠️ Move cache and output files from roaming to local AppData.

### Bug fixes

🐛 Composition: Remove native dlls from the composition index.

## v6.0.0 (2016-12-07)

<img src="docs/assets/images/release-6.0.webp" width="600"/>

### New actions

⚡ Initialization: Load work descriptions from remote location.

⚡ DirectX: Rendering backend for Windows.

⚡ `--monitor-index=`: Sets wallpaper to specific monitor.

⚡ `--desktop=windows-lockscreen`: Sets lockscreen wallpaper (Windows 10).

### Miscellaneous improvements

🛠️ Atom: Display content text if summary text is empty.

🛠️ Meteorology: Change the METAR download URI.

🛠️ Desktop: Add desktop name to output filename.

🛠️ EPG: Indicate ongoing programmes.

### Bug fixes

🐛 Libnova: Fix win32 marshalling.

🐛 Charts: Fix regression on network failure.

🐛 WeatherStatements: Fix thumbnail scaling.

🐛 CSS: Fix linear-gradient parser.

## v5.2.1 (2016-06-02)

### Bug fixes

🐛 Fixed white-space collector.

## v5.2.0 (2016-05-25)

### New actions

⚡ NveFloodStatements: Flood forecast warnings from NVE.

### Bug fixes

🐛 Meteorology: Fixed regression on network failure.

## v5.1.4 (2016-05-19)

### Miscellaneous improvements

🛠️ Epg: Draw now line with stroke-dot-dot stroke-style.

### Bug fixes

🐛 Calendar: Fixed holiday category and rise/set events

🐛 Location: Draw section icons without details.

## v5.1.3 (2016-05-06)

### Bug fixes

🐛 Set rounding behavior of colors to round() (the human eyes distinguish darker colors much better).

## v5.1.2 (2016-05-05)

### Bug fixes

🐛 Decode HTML entities.

## v5.1.1 (2016-04-04)

### Bug fixes

🐛 Sharing violation on `--update-cache`.

🐛 TLS certificates: Trusted list, and revoked list.

🐛 Disable unused features: ProxyLookup, RequestCaching, Expect100Continue.

🐛 Increase max connections to 16.

🐛 Epg: More efficient date-time parser.

## v5.1.0 (2016-03-21)

<img src="docs/assets/images/release-5.1.webp" width="600"/>

### New actions

⚡ JSON and XML serialization to file or console.

⚡ Pipeline: meta elements may set session properties.

⚡ Cairo: Output formats: BMP, JPEG, PNG, SVG, TIFF, WEBP.

⚡ `--desktop=feh` for the i3 desktop.

⚡ NveAvalancheStatements: Avalanche forecast warnings from NVE.

⚡ HTTP digest and NTLM authentication.

⚡ InfoDesklet.

### Miscellaneous improvements

🛠️ Render icons with glyphs instead of images.

🛠️ Cache: allow at most one instance at the time.

🛠️ Epg: Separate channels and programmes cache fetch policy.

### Bug fixes

🐛 Disable REUSE_HTTP_CLIENT.

🐛 Handle HTTP/503 error codes.

🐛 Wind icon: Outline shadow effects.

🐛 HashString: Let base36 encoder use BigInteger and revert back to
SHA1.

🐛 Epg: Handle server meltdown, cancel pending tasks.

🐛 Epg: Try datalist.xml.gz first, then channels.xml.gz.

## v5.0.0 (2016-01-23)

<img src="docs/assets/images/release-5.0.webp" width="600"/>

### New actions

⚡ Pipeline: Replace xml pipeline files with html layout files.

⚡ Pipeline: Enable loading source content from stdin.

⚡ Rendering: CSS styles.

⚡ `--style` and `--stylesheet` options to add additional CSS styles.

⚡ Yr: Provide observations in addition to forecasts.

⚡ Progress: Add `--progress` option to show download progress.

### Miscellaneous improvements

🛠️ Swap images column and calendar column.

🛠️ Calendar: Format day of week to left, day to right of date cell.

🛠️ Holiday: Reddish outline, colorless text.

🛠️ Upgraded libraries: Bond-4.0.1, Json.NET-8.0.2, ExCSS-2.0.6.

🛠️ Use Planetary.JulianDay to find day numbers.

🛠️ Epg: Parallel channel and programmes parser, merge result with
SelectMany.

🛠️ Cache: Replace CacheItem.Location with ICacheItemContent.

🛠️ Cache: Use resource stream as CacheItem.Content, remove write to
disk.

🛠️ Switch to NLog based logging.

### Bug fixes

🐛 Epg: Use local time to calculate number of days to fetch.

🐛 Meteorology: Catch FormatException when the XML missing date-time node(s).

🐛 Yr: Forecast: only choose elements ending after start.

🐛 Yr: Prefer extended symbol (forecast). Secondary simple symbol
(observation).

🐛 Metar: Fix casting DateTimeOffset to BondedZonedDateTime.

🐛 Cairo: Set clip region to prevent overdrawing sibling desklets.

🐛 Lock and synchronize console writes.

## v4.4.1 (2015-12-04)

### Bug fixes

🐛 Metar: Correctly convert UTC time to local time.

🐛 Metar: Sort weather conditions by least match first.

🐛 Yr: Use int as weather condition key.

## v4.4.0 (2015-12-03)

<img src="docs/assets/images/release-4.4.webp" width="600"/>

### New actions

⚡ Add option `--disable-desklet=` to disable misbehaving desklets.

⚡ Scalable moon icons.

### Miscellaneous improvements

🛠️ Version: Set version from `AssemblyVersion` attribute, `git tag`
and `get rev-list`.

🛠️ Re-factoring internals in response to the WeekCalendar project.

🛠️ C#v6 / net-4.5: Dropped older C#/net-4.0 ifdefs.

🛠️ SVG: Replaced all png icons with svg (moon phase, weather-unknown).

🛠️ Log: Set more readable colors on the console.

🛠️ SHA256Base36: Generate 48 characters hash value.

### Bug fixes

🐛 Meteorology: Use the resolved geoposition.

🐛 EpgView: Avoid NullPointerException.

🐛 Fix XmlSerialization issues with the referencesource code in mono-4.2.

🐛 WindImage: round to nearest integer.

## v4.3.3 (2015-11-13)

### Bug fixes

🐛 Meteorology: WundergroundProvider: Fix incorrect wind speed unit.

## v4.3.2 (2015-10-19)

### Bug fixes

🐛 Meteorology: yr.no: parse zoned date-time at leniently.

## v4.3.1 (2015-10-07)

### Bug fixes

🐛 Atom: Ignore 503 (Service Temporarily Unavailable) HTTP responses.

## v4.3.0 (2015-10-04)

<img src="docs/assets/images/release-4.3.webp" width="600"/>

### New actions

⚡ Rendering: Replaced per-platform views with IRenderingSource.

⚡ Meteorology: Replaces MetarDesklet and MeteogramDesklet. Added support for wunderground.com stations.

⚡ WeatherStatements: Replaces MeteogramDesklet / YrForecasts.

⚡ Meteorology: Scaleable weather condition icons.

⚡ Pipeline: XML deserialize desklets using Bond schemes. Required for xs:duration (TimeSpan), map (Dictionary).

### Miscellaneous improvements

🛠️ Rendering: GPU optimization, replace `double` with `float`.

🛠️ Epg: Replace HeaderItems with Width to specify horizontal distribution.

🛠️ Images: Renders at most one image or one composition of two. Pipeline controls flow direction.

## v4.2.0 (2015-07-21)

<img src="docs/assets/images/release-4.2.webp" width="600"/>

### New actions

⚡ Detection: HasNetwork detected from Windows.Forms or from file system (/sys).

⚡ Detection: HighContrast detected from `IDesktop` lookup (win32, windows, gsettings, gsettings-dbus).

⚡ Desktop: By default use `--desktop=auto`. See Settings.Dynamic for detection rules.

⚡ Caching: DataContract serialization replaced with Bond serialization (XML or JSON).

⚡ Calendar: Serialization friendly reoccurred types.

⚡ Desktop: DBusDesktop: replacing gsettings executable process.

⚡ Desktop: WindowsDesktop (NTDDI_WIN8) replacing legacy P/Invoke SystemParametersInfo.

⚡ Geolocation: Resolve position from `Default (?).geoposition.xml` or `--device-geolocator --longitude=? --latitude=? --altitude=?`

⚡ Extras: OBE charts.

### Miscellaneous improvements

🛠️ Views: Source code compatible views (`--renderer=Cairo`, `--renderer=Drawing`).

🛠️ HighContrast: Execute BackgroundDesklet first, enforcing luminance settings before any desklet executes.

🛠️ Splitting Exec: Exec contains (abstract) types required by Desklets. Framework contains types required to render desklets.

## v4.1.2 (2015-05-26)

### Bug fixes

🐛 Desktop: Apply `set wallpaper` only when path changed or with --force-set-wallpaper. Fixing gsettings issues.

## v4.1.1 (2015-05-15)

### Bug fixes

🐛 Calendar: Tidal: Update for recent API changes.

## v4.1.0 (2015-04-22)

<img src="docs/assets/images/release-4.1.webp" width="600"/>

### New actions

⚡ Design-4.1: Column arranged.

⚡ App.config: Vary by Configuration and TargetFrameworkVersion.
Atom: View first image attachment.

⚡ Caching: Http async (net-4.5).

⚡ Accept-Encoding: gzip, deflate. If-None-Match, If-Modified-Since.

⚡ Calendar: Span summary text over more than one line then it can't fit.

⚡ Calendar: PolarSummer, PolarWinter.

⚡ Image: Xptr: XML Pointer Language specification. [W3C http://www.w3.org/TR/xptr-framework/].

⚡ Image: Cairo: Filter effects: SVG 1.1 filters.

⚡ Image: Cache each stage of production.

### Bug fixes

🐛 Atom: Workaround RFC822 date-time in RSS.

## v4.0.1 (2014-11-08)

### New actions

⚡ Image: Support for the XML Pointer Language specification.

## v4.0.0 (2014-10-29)

<img src="docs/assets/images/release-4.0.webp" width="600"/>

### New actions

⚡ Exec: HighContrast icons.

⚡ Exec: Daemon mode.

⚡ Background: Compositions: Daylight%. HighContrast. Lockscreen.

⚡ Calendar: Calendar systems. For more information execute with --calendar= argument.

⚡ EPG: Localized DST shift.

⚡ Metar: Scalable icons.

⚡ Metar: Lunar phase indicator.

### Miscellaneous improvements

🛠️ Atom: Localized format. Current time zone. Clip on last.

🛠️ Background: AspectRatio. Fixed or Infinite.

🛠️ Calendar: Localized format. Current time zone. Clip on last.

🛠️ Calendar: Dst: Geoposition time zone.

🛠️ Metar: Geoposition time zone.

🛠️ Location: Calendar subset back-end.

🛠️ Location: Localized time. RiseSet: Geoposition time zone.

🛠️ Location: Tidal: CET time zone.

### Bug fixes

🐛 Cancellation: Fixed for all.

## v3.0.0 (2014-07-28)

### New actions

⚡ New name Deskimg.

⚡ Scalable: All icons and plotting uses scalable vector graphics. TODO: Drawing: Exception for gradients.

⚡ Locale: In code localization.

### Miscellaneous improvements

🛠️ Image: Skip images having to low luminance (camera out of order).

## v2.9.0 (2014-05-19)

<img src="docs/assets/images/release-2.9.webp" width="600"/>

### New actions

⚡ Rotate: Record previous productions.

### Bug fixes

🐛 Cancellation fixes.

🐛 EPG: Fix empty include.

## v2.8.1 (2014-04-22)

### Bug fixes

🐛 Fix Geolocation incompatibility.

## v2.8.0 (2014-04-14)

### New actions

⚡ Image: Hatch overlay.

### Miscellaneous improvements

🛠️ Epg: Rule lines on both programme start and programme stop.

🛠️ `--date`, `--days` preserve current time.

## v2.7.4 (2014-02-22)

### Bug fixes

🐛 Calendar: Sunday should have red text.

## v2.7.3 (2014-02-21)

### Miscellaneous improvements

🛠️ Calendar: Indicate now with frame fill.

## v2.7.2 (2014-02-19)

### Bug fixes

🐛 Win32: LinearGradient throws OutOfMemoryException in case width and height equals 0.

🐛 ImageDesklet: Add dimension in thumbnail name.

## v2.7.1 (2014-02-18)

### Miscellaneous improvements

🛠️ Epg: Replace Moontool3.GetJulianDay with SunRiseSet.DaysSince2000Jan0.

## v2.7.0 (2014-02-06)

### Miscellaneous improvements

🛠️ XML: Simpler node names.

🛠️ Win32: Drawing renderer as default renderer.

## v2.6.6 (2014-01-28)

### Miscellaneous improvements

🛠️ Atom: Highlights.

## v2.6.5 (2014-01-22)

### Miscellaneous improvements

🛠️ Drawing views.

🛠️ Win32 fixes.

## v2.6.4 (2014-01-18)

### Miscellaneous improvements

🛠️ Geolocation: Alternative time-zones (POSIX / Win32).

## v2.6.3 (2014-01-16)

### New actions

⚡ Lunar Rise/Set.

### Bug fixes

🐛 Fix: Replace Moontool2 with Moontool3. Required to pass lunar phases tests.

## v2.6.2 (2014-01-06)

### New actions

⚡ ContainerDesklet.

## v2.6.1 (2013-12-09)

### Miscellaneous improvements

🛠️ Epg: Channel icons as files.

## v2.6.0 (2013-12-03)

<img src="docs/assets/images/release-2.6.webp" width="600"/>

### New actions

⚡ Common: New graphical design.

### Miscellaneous improvements

🛠️ Common: Global cancellation. Press CTRL+C to cancel.

🛠️ BackgroundDesklet: DayImage and NightImage as container of ImageDesklet.

## v2.5.3 (2013-11-10)

### Bug fixes

🐛 Drawing: Fixed Metar and Atom drawing.

## v2.5.2 (2013-11-08)

### Miscellaneous improvements

🛠️ Units of Measure.

## v2.5.1 (2013-11-07)

### Miscellaneous improvements

🛠️ Atom: Skip empty items.

🛠️ MetarView: Wind and Humidity.

## v2.5.0 (2013-11-03)

<img src="docs/assets/images/release-2.5.webp" width="600"/>

### New actions

⚡ Layout: Flow elements control layout.

⚡ Desklets: Image, Metar, Tidal.

### Miscellaneous improvements

🛠️ Geoposition: Georesolution.

## v2.4.11 (2013-10-28)

### Bug fixes

🐛 Various issues fixed (async, serialization, performance). Runs ~3100 ms down from ~8000 ms.

## v2.4.10 (2013-10-27)

### Miscellaneous improvements

🛠️ `--days` option.

### Bug fixes

🐛 Calendar: Fix incorrect daylight change condition.

🐛 Desktop: Fix incorrect output extension.

## v2.4.9 (2013-10-25)

### Miscellaneous improvements

🛠️ Style: Dynamical theme based on background luminance.

## v2.4.8 (2013-10-17)

### Miscellaneous improvements

🛠️ Settings: Now as real-time property.

## v2.4.7 (2013-10-05)

### Bug fixes

🐛 EPG: 3 times speedup (fix-slow-epg).

## v2.4.6 (2013-10-03)

### Miscellaneous improvements

🛠️ Desktop: ExecuteDesktop replaces Gsettings, Gconf, Xsetbg.

## v2.4.5 (2013-09-29)

### Bug fixes

🐛 bash: DNS lookup error should set `--without-network` option.

## v2.4.4 (2013-09-05)

### Miscellaneous improvements

🛠️ BackgroundDesklet: Preserve system wallpaper.

## v2.4.3 (2013-09-03)

### New actions

⚡ XML serialization of cachedb.

## v2.4.2 (2013-06-08)

### Bug fixes

🐛 Epg: Check gzip signature before usage.

## v2.4.1 (2013-05-16)

### Miscellaneous improvements

🛠️ Cache policy.

## v2.4.0 (2013-05-14)

<img src="docs/assets/images/release-2.4.webp" width="600"/>

### New actions

⚡ BackgroundDesklet: Replaces ImageDesklet and OpacityDesklet.

⚡ BackgroundDesklet: Option to use separate day and night image.

## v2.3.5 (2013-04-22)

### Bug fixes

🐛 Expire invalid downloads.

## v2.3.4 (2013-03-30)

### New actions

⚡ Calendar: DaylightChanges.

## v2.3.3 (2013-03-26)

### Bug fixes

🐛 Calendar: Break when reaching max height.

## v2.3.2 (2013-03-25)

### Miscellaneous improvements

🛠️ Background scaling.

## v2.3.1 (2013-03-24)

### Miscellaneous improvements

🛠️ Add `--margin` argument.

## v2.3.0 (2013-03-23)

### Bug fixes

🐛 Cairo/Pango rendering. Fixing outlining glitches, wrapping issues.

## v2.2.6 (2013-03-19)

### Miscellaneous improvements

🛠️ Background: Use png instead of jpg.

## v2.2.5 (2013-03-14)

<img src="docs/assets/images/release-2.2.5.2.webp" width="600"/>

### Miscellaneous improvements

🛠️ Use outline drawing date column.

## v2.2.4 (2013-03-11)

### Miscellaneous improvements

🛠️ Unlocked CacheManager.

## v2.2.3 (2013-03-10)

### Bug fixes

🐛 Epg: Fix now line overdrawn by similar channel.

## v2.2.2 (2013-03-10)

### Miscellaneous improvements

🛠️ Epg: Now line.

## v2.2.1 (2013-03-05)

### Miscellaneous improvements

🛠️ Calendar: Now renderer.

## v2.2.0 (2013-02-28)

<img src="docs/assets/images/release-2.2.0.8.webp" width="600"/>

### Miscellaneous improvements

🛠️ Use git for version control.

🛠️ Async framework.

🛠️ CacheManager replaces WorkPath.GetFile and ResourceExtensions.

🛠️ Category.Icon replaces IconCollection.

🛠️ Remove FileInfo from WorkPath.

🛠️ IEnumerableAppointment.SubsetAsync replaces IEnumerableAppointment.Subset.

🛠️ Epg: Channel sorting intermixing channels from all available grabbers.

🛠️ Location: Tidal predictions.

🛠️ Calendar: New view.

## v2.1.0 (2013-02-23)

### Miscellaneous improvements

🛠️ Build re-factory:

🛠️ Assembly split.

🛠️ Code analysis.

🛠️ Desklet split (Model / Views / Controller).

🛠️ Styles: Removed System.Drawing dependencies.

## v2.0.0 (2013-02-12)

### New actions

⚡ .4 work merged back to master branch.

⚡ Desklets: Atom.

⚡ Calendars: FI holidays.

⚡ Epg: Viasat.

⚡ Calendar uses DateTime instead of DateTimeOffset for All Day Events.

⚡ IndexComposition replaces Assembly reflection.

⚡ ScreenSize replaces WorkingArea.

⚡ EPG: Hide programmes having only one letter. Fix XBearing on wrapped lines.

⚡ Meteogram: OBS forecast detection.

## v1.0 (2011-12-29)

<img src="docs/assets/images/release-1.0.webp" width="600"/>

### Initial work on

✏️ GDI+ rendering.

✏️ Desklets: Calendar, Epg, Image, Location, Opacity.

✏️ Desktops: Gconf, Gsettings, Win32.

✏️ Calendars: File, DK, GB, IE, NO, SE.

✏️ Epg: Canal+ (Cmore).

✏️ 2010-04-07: Initial commmit.
